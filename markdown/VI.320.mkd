395, 29. παραγήραμα II 394, 20; 491, 15. mente defectus per aetatem uel
a recto ordine et quasi a lera aberrret V 627, 57 (*Isid.* X 78).
delerum (?) γελοῖος III 334, 32; 512, 52. *Cf. Haupt Op.* III 535.

**Delisit** *cf.* deleo.

**Deliteo** λανθάνω II 358, 39. ἀποκρύπτομαι II 238, 7. **delitere**
latere V 406, 34.

**Delitesco** διαλανθάνω II 272, 11. ἀποκρύπτομαι II 238, 7.
ὑποστέλλομαι τὸ διαλανθάνω II 467, 52. **delitescit** latet IV 49, 31;
225, 21; 505, 33. **dilitescunt** ἀποκρύπτονται, λανθάνουσιν II 50, 6.
**delitescere** diu latere IV 225, 23; 328, 58. moram facere IV 505, 32;
V 283, 50; 407, 16. **delituit** ἔλαθεν II 42, 10. latuit IV 49, 10;
225, 20; 328, 59; V 285, 38 (dilutuit). latuit, oblituit IV 49, 30; 505,
31. oblituit, latuit V 284, 27; 407, 2. **dilutuit** latuit IV 54, 33
(*cf. Loewe Prodr.* 429).

**Delius** Apollo IV 52, 13. Apollo ab insula Delo V 285, 15.

**Delliones** (duelliones *Vulc.*) δηταρται (διακατάρατοι Vulc.
κατάρατοι *c.* δηιῶται *h.* ἀντάρται *H.*), ἀκαθοσίωτοι τύραννοι καὶ
τυραννίδες (τυραννιδος *cod. corr. Vulc.*) II 47, 31. *V.* debellio,
cluellio, perduellio.

**Delmatica** *v.* dalmatica.

**Delocatio** ἐκτόπισις II 293, 11.

**Deloco** ἐκτοπίζω II 293, 12.

**Deloricatum** laceratum V 285, 29 (*v.* diloricare).

**Delos** nomen insulae III 492, 56; 515, 40. insula IV 52, 17.

**Delphica** (*vel* delfica *scil.* mensa) δελφική III 197, 17; 321, 24.
**deluca** μενιστέριον II 42, 20 (*corr. c h, Meursius*: *v.* abacus).
**delfica** mensa est quae fiebat aput Delfus ciuitatem V 633, 44 (*cf.
Friedlaender Iuvenal. p.* 106). **delfica** ciuitas. Lucanus (V 74):
'Delphica Thebanae', et haec Thebanae ciuitas V 188, 36.

**Delphinus** (*pro quo fere ubique* delfinus *libri habent*) δελφίς II
268, 2. δελφίν II 42, 4; III 17, 20; 241, 49 (*sign. caeli*).
δελφῖνος III 89, 38; 293, 36; 318, 9; 355, 56; 436, 31. *V.* simones.

**Delta** quattuor III 514, 5.

**Delticus** (*a* δέλτος?) litteratus, doctor V 596, 62. est litteratus
V 618, 23. litteratus, doctus V 633, 37.

**Deluare** decaluare V 543, 33 (Deglubare? Dealbare decalicare *H.*
Delēuare *Buech.*).

**Delubrum** καθίδρυμα II 335, 17; III 301, 35. ἵδρυμα καὶ καθίδρυμα III
238, 32. ἵδρυμα II 330, 58. ἀφίδρυμα II 253, 16. καθίδρυμα, ξόανον,
ἀνάθημα II 42, 5. ξόανον II 378, 19. templum idolorum IV 53, 30.
simulacrum uel timphanum (templum, fanum?) IV 53, 16. templum, puluinus
(*vel* puluinar) uel quicquid in idolum colitur IV 328, 60. **dilubrum**
templi locus ubi est simulacrum IV 331, 18. templum IV 54, 36.
**delubra** καθιδρύματα III 170, 46. fana, templa IV 437, 41. templa IV
48, 16. templa idolorum, quia in ingressu lacus aquae fiebant, ubi se
sacerdotes sacrificaturi purificabant, et a diluendo, id est lauando
delubra dicta sunt IV 227, 32; V 284, 40/41 (in ingr. ipso; *cf. Serv.
in Aen.* II 225; IV 56; *Isid.* XV 4, 9; *Diff.* 407). templum, ara
idolorum IV 225, 27. templa idolorum aut auaritia (ara? *contam.*? *v.*
lucrum) IV 502, 44. templa idolorum V 284, 8; 407, 21. templa deorum
(*Euseb. eccl. hist.* XI 28) V 421, 29 = 430, 10. dicuntur templa,
nihilominus et simulacra ab eo quod dolantur (*schol. Gronov. Div. in
Caec.* 3) V 659, 3. statuae V 407, 57. sacrificia V 407, 11.

**Deluca** *v.* delphica.

**Deludo** διαπαίζω II 273, 15; III 134, 12. καταπαίζω II 342, 48.
**deludis** διαπαίζεις III 134, 13. **deludit** διαπαίζει III 134, 14.
**delusi** διέπαιξα III 134, 16. **delusit** διέπαιξεν II 42, 25.
circumuenit IV 225, 28; 504, 46; V 283, 39 (deluxit). *V.* pelusit.

**Delumentum** thuachl (*AS.*) V 356, 26. đhuehl (*AS.*) V 405, 29.
*Cf.* sapo.

**Deluo** ἀποκλύζω II 237, 43. καταπλὐνω II 343, 4. **deluit** ἀποκλύζει
II 42, 19. lauit IV 48, 50; 225, 17; V 496, 18. **deruit** eluit IV 329,
32. *V.* diluo, deruit. *Cf. Festus Pauli p.* 73, 15.

**Delurcor** *v.* delargitior.

**Delusor** διαπαίκτης III 134, 15.

**Demagis** σφοδρῶς II 42, 29. uehementer V 627, 59. uehimens (!) V 285,
30. ualde magis V 640, 36 (*Non.* 98, 16). *Cf. Festus Pauli p.* 71, 9
(*ubi* nimis *Dacier, Thewrewk de Ponor*); *Loewe Prodr.* 332;
*Buecheler Mus. Rhen.* XXXVII 523.

**Demanauerint** καταδράμωσιν II 42, 30.

**Demando** παρακατατίθημι II 395, 3; III 156, 5.

**Demanduco** corrodo *gloss. Arab. p.* 703, 55.

**Demeio** ἀφουρῶ II 253, 50; III 248, 69. ἐξουρῶ II 304, 27.

**Demendatio** commendando (*ubi* commendatio *b, Loewe*) II 576, 39.

**Demens** ἀπονενοημένος III 333, 64; 373, 9. ἀνόητος, ἄφρων II 42, 34.
ἄφρων II 254, 5; III 334, 4. ἄνους II 228, 36.
