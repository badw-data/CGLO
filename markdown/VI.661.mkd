**Lupa** ἑταίρα καὶ λύκαινα II 125, 18. λύκαινα III 361, 77; 431, 15;
499, 11. dicta est meretrix IV 535, 29. meretrix V 308, 43; 528, 7.
**lupam** meretricem a rapacitate uel a libidine huius (ejus *G P*)
animalis, unde et lupanar dicitur *Plac.* V 29, 38 = V 82, 8 (unde et
lupa *sine* dicitur: *reliqua absumpta sunt*) = V 114, 30 (*item*).
**lupas** meretrices V 219, 26. meretrices dicunt[ur] IV 111, 18. *Cf.
Serv. in Aen.* I 273; III 647; *Isid.* X 163; XVIII 42, 2.

**Lupa** σαλιβάριον II 429, 39. *Cf. Bluemner 'Maximaltarif' p.* 130.

**Lupa ceruaria** belbes (?) III 554, 65. fellenia III 590, 45; 624, 10
(fellenea). *Cf.* belbe id est fellenis III 608, 66. belbae id est
fellonis III 587, 70. fellena id est billi III 612, 17. felleni id est
bilbi III 624, 30; 590, 65 (fellem *cod.: nisi* fel = bilis *subest*);
belbe id est III 537, 3. *V. index Cass. Fel. p.* 225 (belua); *Theod.
Prisc. p.* 499. *V.* lupicuda.

**Lupae nutricis** Remo enim et Romulo lupa traditur mammas dedisse IV
450, 45 (*Verg. Aen.* I 275).

**Lupal** (?) lupanar IV 362, 19; V 544, 21; 602, 62.

**Lupana** (lupa *d e*) meretrix IV 362, 22 (*Arch.* VIII *p.* 9; 145;
IX *p.* 5). *V.* scortus.

**Lupanar** πορνεῖον III 306, 46. domus meretricis IV 535, 30. habitatio
meretricum IV 256, 7. statio meretricis V 309, 7. ubi meretrices
habitant[ur] V 309, 11. locus ubi scortum est uel prostibulum siue ubi
meretrices consistunt V 219, 21. locus turpis IV 111, 12; 535, 27. locus
turpis meretricum IV 362, 23. taberna V 369, 44 (lapanas *vel* lasanas:
*an* capanna?). cella meretricis V 114, 20. domus meretricum uel
theatrum II 586, 55 (*cf. Isid.* XVIII 42, 2). **lupanaria** πορνεῖα III
455, 18. cellulae meretricum IV 112, 22; V 219, 24. cellae meretricum IV
256, 19. loca in quibus meretrices sunt IV 362, 24. locus (!) in quibus
meretrices habitant V 528, 8 (*cf. Serv. in Aen.* III 647). *V.* lupa.

**Lupanaria** πόρνη II 413, 54. meretrix V 308, 53. mulier IV 535, 28.
lupa dicta est meretrix, de qua hoc uocabulum sumtum est V 219, 23. lupa
dicta est, de qua hoc uocabulum est sumtum V 114, 19 (*cf. Serv. in
Aen.* I 273; *GR L.* II 222, 5). ludibriosa meretrix, de qua hoc
uocabulum sumtum est V 219, 25.

**Lupanaria mulier** enim lupa dicta est meretrix V 219, 22. enim lupa
dicta est meretrix, de qua hoc uocabulum IV 111, 13 (de qua permanet hoc
uoc. *a*).

**Lupanarium** πορνεῖον II 413, 57 (*ex plurali* lupanaria /actum?).

**Luparia herba** (= λυκοκτόνον) unde lupi moriuntur III 568, 19; 592,
1; 613, 50; 625, 45.

**Lupata** frena duriora inaequalium et asperrimorum dentium ad domandos
equos lupata dicuntur V 114, 18. **lupatis** frenis IV 111, 21; 535, 26.
frenis. Lucanus (V 549): lurida pallens V 219, 27 (*glossa contaminata:
v.* luridus). *Cf.* **lupatus** frenum Sarracenorum V 621, 1. *Cf.
Isid.* XX 16, 2.

**Lupellus** (*piscis*) spatgangitus (σπαταγγίτης *aut* σπατάγγιος
*Buech. coll. Ath.* III 91^b^.) III 186, 49.

**Lupercal** Παν\<ε\>ῖον II 393, 24. ἱερὸν Πανός II 331, 22; III 238,
46. templum Panos V 415, 41 (*de lib. rot.*); 426, 13 (phani: *de lib.
rot.*). fuit Romae sub monte Palatino spelunca in qua de capro luebatur,
id est sacrificabatur, unde **lupercal** quidam dictum putant, alii,
quod illic Remum et Romulum lupa nutrierit; alii, quod et Virgilius,
locum esse hunc sacratum Pan\<i\> deo Arcadiae, cui etiam mons Lycaeus
in Arcadia consecratŭs est, et dictus Lycaeus quod lupos non sinat in
oues saeuire V 659, 30 (*cf. Serv. in Aen.* VIII 343). sic appellatur
locus ubi Pan deus colitur, cui sollemnia ludicra celebrantur, quae
Luperca\<lia\> uocantur *Plac.* V 82, 9. **Lupercalia** Λύκεια III 171,
47 (lupanaria); 239, 36. gentilium cultus, quod mares colunt IV 111, 22;
535, 31; V 219, 28. gentilium cultus marti (?) V 219, 29. sacra Panis,
quia ipse dicitur dedisse responsa, ut coirent lupi et hirci V 309, 5.
gentium cultura, id est sacra Panis, quia ipse dicitur dedisse responsa,
ut coirent lupi et hirci IV 256, 20. ipsa sacra (*de lib. rot.; v.*
lupercus) V 415, 39; 426, 11. *V.* ergo Euander.

**Lupercus** sacerdos qui deum (deos *cod.*) Arcadum sacrato more
celebra[n]t V 464, 13. sacerdos is qui deos Arcaclum sacra fani (Fauni
*Buech.*) celebrat IV 415, 22. sacerdos V 506, 53; 544, 22. **luperci**
pastores qui sacra Incubi nudi colebant IV 256, 9. sacerdotes lupercales
V 415, 37 (*de lib. rot. = Isid. de nat. rer.* IV 4). lupercales
sacerdotes V 426, 10 (*item*).

**Lupi ceu** quasi lupi *Plac.* V 82, 10 (*Verg. Aen.* II 355).

**Lupicuda** fellenis (= φλόμος?) III 590, 57. filonis III 612, 15; 624,
22. pi\<s\>caturia III 594, 6; 616, 1; 627, 57. flomus **lupicuda** siue
piscatoria III 546, 33.
