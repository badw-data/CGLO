**Calabricus** *v.* allius agrestis.

**Calabris** uentis siccis IV 214, 41; 491, 46; V 493, 41; 595, 19. *Cf.
Lucan.* V 379. *V.* flabrum.

**Calabri uersus** obsceni V 626, 32; 595, 61. **calabri** obsceni
foetore V 275, 5. **calabrum** genus uersuum malorum quasi colobon uel
iocularium (*ubi* colobon *et* calabri *miro modo confunduntur: cf.
Nettleship 'Journ. of Phil.'* XIX 117) IV 30, 1.

**Calacte** ciuitas V 550, 7.

**Calamaucus** (calomacus *cod.*) haeth (*AS.*, haett = *Hut*) V 353,
54. *Cf.* cidarim, galerus, pileus, scirpus. *V. Ducange sub*
camelaucum, *Arch.* VI 113.

**Calamaula** canna de qua canitur IV 30, 11; 213, 43; V 173, 37; 493,
38.

**Calamaularius** ipse qui de canna canit IV 214, 3.

**Calameos** (Καλαμαῖος) Perint\<h\>inorum lingua Ianuarius mensis
dicitur V 173, 38. *V.* menses.

**Calamiscus** καλαμίσκος III 207, 60. **calamiscos** calamos IV 215, 26
(= *Eucher. instr. p.* 149, 8).

**Calamistrat** παραγκιστροῖ (ita *ce.* -τρον *cod.*) καὶ εἰς τὸ οὖλον
(αυλον *cod.*) συντίθησιν τὴν κόμην II 96, 18.

**Calamistratorium** *v.* acus.

**Calamistratus** capillosus, compositus uel crispus (crispatus *abd*)
IV 215, 30 (*cf. Serv. in Aen.* XII, 100). comptus ad uanitate\<m\> V
274, 36. a calamistro, icl est aco (!) ferreo in calami similitudine
facto, in quo crines obtorquentur, ut crispi sint, quem in cinere
calefacere solent, qui capillos crispant, ut calamistrati sint V 626, 30
(= *Isid.* X 57; XX 13, 4).

**Calamistrum** compti[o] capilli uel crispi V 493, 33. est pecten uel
acus capitis ferreus a quo crines torqueantur V 616, 48 (*cf. Isid.* XX
13, 4).

**Calamitas** δυστυχία II 96, 21; 282, 20. συμφορά II 96, 11; 443, 18;
III 440, 49; 503, 49 (simphoria). κακοδαιμονία II 336, 39. φθορά II 471,
12. miseria, aerumna, aduersa V 550, 2. infelicitas, miseria IV 315, 31.
**calamitas** quod calamus (*Non.*?) V 650, 3. *Cf. Serv. in Georg.* I
151 (robigo … genus uitii est quo culmi pereunt, quod a rusticanis
calamitas dicitur) *ex Donato: cf. Donat. in Eun.* I 1, 34; *Hec. prol.*
I 2. clades **calamitates** casus συμφοραί, δυστυχίαι II 101, 31. *Cf.*
grando.

**Calamitosus** δυστυχής II 96, 10; 282, 19. ἄθλιος III 125, 32.
κακοδαίμων II 336, 38. miser, infelix II 571, 24. periculosus V 444, 23.
**calamitos\<i\>** at\<t\>riti V 650, 4 (*cf. Non.* 33, 26).

**Calamiza** est qui post messores spicas colligit V 616, 45.
(calamista? calamizans? *cf. Ducange*).

**Calamizo** cum calamis canto V 616, 46. **calamizare** laeta cantare
IV 490, 37; V 272, 58; 355, 27; 626, 31.

**Calamus** κάλαμος III 261, 43; 277, 48. **calamum** κάλαμος III 327,
54; 527, 39. δόναξ, κάλαμος **calamus** harundo III 301, 10. canna IV
*praef.* XLII. fragmites (phragmitis *Plin.* XXXII 141) III 580, 52.
**calamum** κάλαμον III 340, 53; 440, 50. **calamos** trochiscos
(calamostro iscos *codd.*) uel cariscos quasi in nucis modum deformatos
V 173, 39 (*contam.*?). **calamis** tubis IV 29, 21; 492, 6. *V.*
ingenti calamo, cariscus.

**Calamus agrestis** (*vel potius* calamo agreste) κάλαμος ἄγριος III
537, 71.

**Calata** uocata V 563, 61; 564, 12.

**Calata comitia** ἀρχ[ι]αιρέσια δὶς τοῦ ἔτους γινόμενα [επισκοπις
επιγλυφεις] II 96, 15 (*v.* cala). *Cf. Gai.* II 101. **calata**
ἀρχ[ι]αιρεσίων ἑορταί II 95, 35. *Cf.* caltudia.

**Calathus** κάλαθος III 263, 25. τάλαρος II 451, 20; 263, 24. cophinus
uel canistrus (!) V 444, 22. canistrum uel qua[s]sillum V 493, 36
(*cf. Festus Pauli p.* 47, 6; *Serv. in Ecl.* II 45; *Isid.* XIX 29, 3).
poculi genus V 173, 42. **calatum** canistrum id est cartallum IV 31, 7;
31, 20; 492, 49; V 173, 41; 653, 46 (*= Iuvenal.* II 54). **caladi**
similitudo liliorum V 275, 30. **calati** cyathi, scyphi V 173, 40.
**calathis** canistris IV 213, 28.

**Calatiae** ἀναιδεῖς, αἰσχροί (αισχρος *cod.*) ἀπὸ τοῦ θερμαίνεσθαι II
96, 12 (ὀξέως θερμ. *Vulcanius, contam.*?).

**Calator** κλήτωρ II 96, 1. minister sacrorum IV 214, 1; V 275, 1; 595,
34; 63; 616, 44. uocator, minister sacrorum V 563, 66. clamator V 495,
72. **calatores** ἐκβιβασταὶ ἱερέων II 96, 3. δοῦλοι δημόσιοι, περίπολοι
II 95, 42. *Cf. Festus Pauli p.* 38, 13. *V.* nomenclator.

**Calatris** fluuius ethorum(?) V 275, 20. Crathis?

**Calatrum** (?) stercus V 493, 35 (caenum?).

**Calauatarius** σχοινοβάτης III 172, 44. *Scr.* calobatarius. *Cf.*
grallator.

**Calautica** (cac. *cod. corr. Vulc.*) εἶδος ζώνης II 95, 14.
**caldica** (?) uuefl (*AS.*) V 354, 43 (*ubi* calantica *Oehler*). *V.
Hessels* C 467 (cladica); *Wright-Wuelcker* 364, 24 (*quamquam dubitari
potest num recte huc referatur*). *Cf. Non. p.* 537, 2; *Serv. in Aen.*
IX 613.

**Calbae** κόσμια II 95, 43 (*ubi* calbea *alii*). armillae quibus
milites (quae militibus *G*) ob uirtutem donantur *Plac.*
