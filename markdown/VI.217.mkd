492, 50; III 442, 7. δεξαμενή II 268, 14 (*cf.* III 246, 25); III 200,
27; 357, 55; 365, 38.

**Cistifer** *v.* uicorium.

**Cistophori** cyneris (denarii *H.*) V 565, 32 (cyst. *cod.*).
argenti V 565, 33.

**Cistula** sporta V 349, 20.

**Citate** ἐντρεχῶς II 101, 22. cursim IV 318, 43; V 276, 47,

**Citatim** κατὰ τάχος II 344, 30. cursim IV 33, 57; 494, 46.

**Citatio** κλῆσις II 350, 47. ἀνάκλησις III 442, 8; 484, 53. uocatio II
573, 10; IV 318, 44.

**Citatorium** κλητικόν II 350, 50.

**Citato tramite** cursu ueloci IV 218, 35. *Cf.* cito tramite.

**Citatum aeris** mobilitatem aeris IV 494, 51; V 276, 41.

**Citatus** ὠκύς III 373, 55. agilis in lingua IV 494, 50. *V.* citatus
in lingua.

**Citatus in lingua** agilis in lingua V 446, 3.

**Citaxus** similis taxo IV 35, 8; 218, 45; 495, 39; V 593, 34 (= ceu
taxus).

**Cit certamen** excitat pugnam uel commonet *Plac.* V 12, 11 = V 55,
14.

**Citerior** ἐνδότερος II 101, 21. exterior (interior?) IV 219, 15. qui
extra est IV 318, 46; V 540, 14 (*cf.* IV *p.* XLIII). intraneus,
intimus II 573, 14. **citeriorem** ulteriorem V 418, 42 (*Euseb. eccl.
hist.* V 9) = 427, 12. **citeriore** interiore IV 34, 57; 494, 42.
ulteriore V 276, 26.

**Citerius** propius IV 494, 43. quasi propius IV 35, 10. exterius (?)
IV 219, 14. ulterius IV 318, 47. exterius, ulterius IV 33, 56; 494, 41;
V 276, 46.

**Citerum** (*vel* citierum) aliquid exinde ut cithara (*vel* citiararum
*sine* ut) IV 35, 9; 495, 40 (Cythera insula: aliquid exinde Cytheriacum
*Nettleship 'Journ. of Phil.'* XIX 117. Citerum *et* ut citra *tutatur
Buech.*).

**Cithaeron** mons apud Thebas IV 437, 2 (*Verg. Aen.* IV 303 *et
Serv.*). mons Thebanorum V 277, 25.

**Cithara** κιθάρα II 349, 28. chelys IV 318, 48. *V.* citerum.

**Citharoedus** κιθαρῳδός III 172, 52; 239, 69. **citharoedi** κιθαρῳδοί
III 10, 44; 302, 36; 371, 74; 525, 23.

**Citimum** citra omnia V 446, 4; *cf.* IV *p.* XLIII (*Loewe GL. N.*
83). citra omnia, proximum IV 218, 49; V 277, 18. contra (citra?) omnia,
proximum IV 34, 36. citra omnium (*scr.* omnia) IV 318, 49. **cituma**
proxima V 639, 8 (= *Non.* 85, 15). *Cf. praef. Anthol.* V *p.* V. *V.*
connum, citro.

**Citius tempus quam oratio deficiet** V 660, 41 (*Cic. pro Rosc. Am.*
89).

**Cito** ταχέως II 452, 17. ταχύ III 442, 10. ὀξέως II 384, 41.
propediem IV 318, 51. statim, continuo, exinde *Plac.* V 55, 17. citius
ταχέως III 442, 11. ταχύτερον II 452, 23. τάχιον II 452, 16. θᾶττον II
326, 41. prius *Plac.* V 55, 16. maturius IV 318, 50.

**Cito** καλῶ II 337, 61. **citant** uocant, accersunt V 276, 17.
**citarier** celeriter moneri *Plac.* V 14, 43 = V 55, 15.

**Cito coctum** *v.* damasonium.

**Cito tramite** celeri cursu IV 434, 12 = 433, 34 (*Verg. Aen.* V
610). *V.* citato tr.

**Citra** ἐκτός II 293, 15. δίχα, χωρίς, ἐκτός II 101, 19. χωρίς II 479,
49. ἄνευ II 226, 16. extra IV 34, 24; 40; 219, 7. ultra V 351, 10.
extra, ultra IV 318, 52; IV *p.* XLII. **citra te** sine te IV 34, 44.
**citra uos** infra (= intra?) uos V 494, 44. **citra** bihina (*AS.*) V
425, 21 (*Cassian. inst.* V 36, 2). *V.* cis.

**Citreum** θύϊνον II 101, 20. **citrium** κίτριον II 349, 59.
**cetreum** (*vel* citrium) κίτριον III 191, 66; 403, 70; 428, 53; 556,
36 (ceitrin ·i· citru); 621, 13 (*item*). κίτρινον III 264, 47. ἑσπερίς
III 26, 22; 358, 75; 545, 71. ἑσπέριον III 442, 9 (citrum); 477, 41
(*item*). **cidrium** (*vel* citrium *vel* cedrium) poma cedri III 588,
31, poma de cedro III 609, 19 (*cf.* erporis \<ἑσπερίς?\> poma cedri
III 562, 69). erporis III 590, 21; 611, 45; 623, 57. erbiscis (*vel*
erpiscis?) III 562, 36; 590, 21; 611, 44; 623, 56. *Cf.* epredim idest
**cedru** III 538, 42 = 546, 10 (ἑσπέριον citreum? *aliter Schmidt
Herm.* XVIII *p.* 538). **citreae** sunt spondae ubi antiqui noctibus
scribebant V 616, 33 (*cf. schol. Pers.* I 52). *V.* pirum.

**Citreus** pomerius IV 219, 19 (*ubi* citrarius pomarius *Warren, non
recte*); V 494, 47; 565, 34.

**Citro** proximum IV 219, 5 (citrum prox, *abcd:* citimum?). huc ad nos
IV 219, 25; 494, 44. huc ad nos, **ultro** (ultra *codd.*) a nobis ad
alia IV 35, 21. me praesente V 446, 2. *V.* ultro.

**Citropodes** *v.* chytropodes.

**Citroque** et ulterius IV 219, 13.

**Citrus** citri folia III 544, 59. cit[e]reus (?) V 639, 13 (= *Non.*
86, 7). *V.* citreum.

**Citus** ταχύς II 452, 22. festinus, uelox IV 33, 62. festinans,
properans IV 318, 53. citatus, uelox, festinus IV 494, 49. uelox seu
citatus IV *p.* XLII. celer, uelox IV 434, 13 (*Verg. Aen.* I 301; XI
462). **citum** IV *p.* XLII. **citam** uelocem IV 34, 19; 218, 33; 494,
48. **citi** festinantes IV 34, 18; 494, 47.
