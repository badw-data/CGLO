cisorum uel uictoriae IV 398, 21. spolia punitorum aut uictoria IV 575,
30. spolia punitorum V 397, 48; *gloss. Werth. Gallée* 354; 356 (*v.*
*suppl.*). triumphorum tituli, luna (= Triuia) IV 186, 45; 575, 29 (*cf.
Arch.* IX 439). **tropia** (*vel* tropea) signa uel sigbeacn (*vel*
beanc, *AS.*) V 396, 9. signa V 428, 59 = V 419, 71 (*Euseb. eccl.
hist.* II 25). **tropea** uictoria. Virgilius (*Georg.* III 32): et dua
(ua *codd.*) rapta manu diuerso \<ex\> hoste tropaea V 251, 18. *V.*
triumphus.

**Tropica** splendida II 595, 60. **propicon** moralium V 379, 23.

**Tropice** moraliter IV 293, 32.

**Tropologia** moralis intellegentia IV 293, 27 = 575, 45 (*Eucher.
instr. p.* 161, 10).

**Tropus** locutionis modus *Plac.* V 102, 17. sonus IV 425, 17; V 395,
24. sonus, cantus V 487, 21. mensura dictionis et sonus IV 293, 31.
mensura dictionis IV 187, 1; 575, 32; V 334, 1.

**Trorsus** fortis II 595, 59 (trasus = θρασύς? torosus?). *V.*
trusulus.

**Tros\<s\>eus** equester, adulescens V 631, 58. **trosseos** equestres
V 487, 24; 517, 34. *V.* trossuli, equester.

**Trossuli** equites Romani con (cum *b*) equis puplicis IV 293, 29.
**trossoli** equi, equites IV 575, 47. equites V 395, 27 (trossulae);
487, 23 (trorsuli); 631, 59 (trosoli). *Cf. Festus Paulip.* 367, 20.
*V.* trusulus, equester.

**Tru[u]am** trullam V 651, 50 (*Non.* 19, 10).

**Trublium** (τρύβλιον) parapside IV 398, 32. **triblia** (*vel*
triplia) lebil (*AS.*) V 396, 16. *Cf. Loewe Prodr.* 276. 'tribliam
*legitur in vita S. Malonis c.* 24' (*Schlutter*).

**Trubuci** *v.* tribaci.

**Trucidatio** σφαγή III 209, 18.

**Trucido** σφάζω II 449, 11. **trucidat** κατασφάζει, μιαιφονεῖ,
ἀναιρεῖ II 202, 26. occidit IV 425, 8. iugulat, occidit, necat, uerberat
IV 398, 26, interficit IV 294, 11. **trucidant** interimunt IV 467, 39
(*Verg. Aen.* II 494?). **trucidari** ἀναιρεθῆναι II 202, 25.

**Trucis** *v.* trachis.

**Truclionis** (? a torculo?) grana racemi (*vel* -mis) III 578, 74;
595, 59. **truclioni** grana racemi[s], id est botrum III 629, 56.

**Tructa** τρώκτης III 355, 63. *Cf. Is.* XII 6, 6.

**Truculentus** δύσκολος II 281, 50 (trucel.). ἐμβριθής II 295, 55.
ἰταμός, ὠμός II 202, 27. τετρωμένος (cruentus *e*) II 454, 36. ἐλεύθερος
II 202, 28. ammodum pessimus IV 467, 40 (*gl. Verg.*?). saeuus, iratus
V 334, 4. saeuus uel iratus aut inmitis aut ferox IV 187, 15; 576, 26.
ferox, inmitis (*vel* inimicus) IV 294, 7. uehementissimus, saeuus,
iratus IV 398, 27. uehementissimus V 487, 26. **truculentum** ἰταμόν II
202, 30.

**Trudes ferratae** fustes V 397, 54. *Cf. Isid.* XVIII 7, 3; *Verg.*
*Aen.* V 208.

**Trudo** ὠθῶ II 481, 55; III 81, 31; 165, 39. **trudis** includis IV
576, 20. **trudit** ὠθεῖ, ἐρείδει II 202, 29. cludit IV 186, 19; 576,
16. inpingit, recludit IV 294, 13. inpingit, recludit, expellit IV 398,
28. propellit uel inpellit IV 187, 11. inpulit (*v.* trusus), inpingit,
recludit, abscondit V 487, 27. exclusit (!), excludit, expellit uel
clausit (!) V 334, 2. inclusit [alias processit] V 334, 3. **trusit**
expulit IV 576, 18. **truditur** mittitur IV 187, 10. inpingitur in
custodia IV 398, 29. in custodia inpellitur IV 294, 14. excluditur,
noue, ut Donatus *lib. gloss.* (*cf. Goetz 'der lib. gloss.' p.* 278;
*Serv. in Georg.* II 31). inpellitur IV 576, 19.

**Truelum** (*cf.* τρυήλη) diabete, id est circinum V 487, 30.
**truele** circinum V 517, 38.

**Truere** *v.* struo.

**Trufulus** (*vel* trif.) feluspraeici (*vel* felospraeci, *AS. =
vielgesprächig*) V 396, 39 (friuolus?).

**Trulla** ζωμάλιστρος II 323, 1; III 20, 54. ζωμάρυστρος III 92, 28.
ζωμάρυστον III 420, 24 (trubla). ζωμάρυσις III 321, 57. ζωμήρυσ\<ις\>
III 198, 4 (trulle). ὑποχεύς (-χυσις?) III 368, 6. κοτύλη (κοιυταλη
*cod. em. Boucherie*) III 321, 58. τρούλλα[ς] II 460, 13. panna, cacha
(*cf.* cazza *paulo post et Diez* I), id est ferrum unde parietes
liniunt (!) II 595, 49. cazza ferrea V 517, 41. quod trudat, id est
cludat cal\<c\>e uel luto lapides V 582, 15 (*Isid.* XIX 18, 3). crucae
(*AS.*) V 396, 3. scofl (*AS.*) V 396, 54.

**Trulla aurea** id est sonus uentris ex parte obscena diuitis V 655, 9
(*Iuvenal.* III 108). *V.* tublia.

**Trulla lignea** ζωμήρυσις II 202, 33.

**Trullaria** ζωμάρυστρον II 202, 41.

**Trulleum** et **gutum** (guttum *e*) et **aquiminale** χέρνιβον,
ξεστόν (ξέστην *d.* χερνιβόκεστρον *Mau*) II 202, 31. **trulleum**
χέρνιβον II 476, 52; III 324, 62. polybrum in quo manus perluuntur, quod
in sinistra tenetur et aliud uas cum aqua in dextera V 655, 10 (*Non.*
547, 3).

**Truma** (*et* pulex) ψύλλα II 481, 22. *V.* struma.

**Trumbi** genus iudicii V 487, 29; 517, 39 (*obscura*).

**Trumen** τρυχο̄ς τρῑχινοσε̄ν II 202, 32 (τρῦχος, τρυχινόεν? τρύφος
ταριχινόν *Vulc.* turben τροχός, τροχοέν τι *Martinius*).
