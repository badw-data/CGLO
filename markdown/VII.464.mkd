**ἀποθήκη** cella, horreum. *Cf.* apotheca, hypotheca.

**ἀποθηριῶ** effero 2.

**ἀπόθητος** inamabilis.

**ἀποθλίβω** elido.

**ἀποθνήσκω** decedo, emorior, morior, -θνήσκοντες morituri, -θανών
defunctus, interceptus.

**\*ἀποίγω (?)** *cf.* resero.

**ἀποίητος** inhabilis.

**ἀποικία** colonia.

**ἀποικισμός** deportatio.

**ἄποικος** colonus, elimes.

**ἀποιστέον** perferendum.

**\*ἀποιωνίζομαι** abominor, -ισθείς inauguratus.

**ἀποκαθαίρω** expurgo.

**ἀποκαθέζομαι** resideo, -εται absidet.

**ἀποκαθήμενος** reses, sessilis.

**\*ἀποκάθισμα** residuatio.

**ἀποκαθίστημι** *et* **ἀποκαθιστῶ** exhibeo, redintegro, remancipo,
repraesento, resarcio, restituo, -εἰς ἀρχαῖον antico, -εἰς ἀκέραιον in
integrum restituo.

**ἀποκαίω** νεκροῖς νεκύσια parento.

**ἀποκαλύπτω** detego, evelo, revelo.

**ἀποκάλυψις** retectio, revelatio. *Cf.* apocalypsis.

**ἀποκαλῶ:** -οῦμαι devoco.

**ἀποκάμνω:** -καμώγ defectus, defessus, lassus.

**ἀποκάμπτω** degenero, deverto.

**ἀποκατάστασις** redhibitio, reditus, reductio (*v. s. v.* diructio),
restitutio, ἡ εἰς ὁλόκληρον ἀ. in integrum restitutio, redintegratio.

**ἀποκαταστατικός** *cf.* apocatasticus.

**\*ἀποκατάσχεσις** abstentatio.

**ἀποκατέχω** detin(e)o. *Cf.* decadus.

**ἀπόκαυμα** torres, ustilatio.

**ἀπόκειμαι:** -κειται repono (repositum est), -κείμενος conditus,
repositus, -ον reconditum, repositorium.

**ἀποκείρω** detondo, -κεκαρμένα detonsa.

**ἀποκένωσις** exinanitas, vacuitas.

**ἀποκεφαλίζω** decollo, ὁ -ων speculator.

**ἀποκεφαλισμός:** εἰς -όν ad decollandum.

**ἀποκήρυξις** abdicatio.

**ἀποκηρύσσω** abdĭco, abrogo, depreco (depreconum).

**ἀποκίνησις** emotio, remo[vea]tio.

**ἀποκινῶ** amoveo, dimoveo, removeo, semoto, summoveo, -ηθείς amotus,
emotus.

**ἀποκλείω** abdo, praecludo, recludo.

**ἀποκληρονόμος** exheres, -ον ποιῶ exheredem facio.

**ἀπόκληρος** exsors. *Cf.* integer.

**ἀποκλήρωσις** delegatio, deputatio.

**ἀποκλιθείς** explosus.

**ἀποκλύζω** abluo, deluo.

**ἀποκλῶ** defrango, perfringo, praefringo, refrango.

**ἀποκοιμῶμαι** abdormio.

**ἀπόκοιτος** discretus lecto, emanens, emansio, -μνηστήρ decubis, -
λῃστής emansor.

**ἀποκοιτῶ** amaneo. *Cf.* abscindo.

**\*ἀποκολακεύω** eblandio.

**ἀποκομίζω** asporto, aufero, refero, -ισθείς latus 2.

**\*ἀποκομιστικός** ablativus.

**ἀποκοπή** abscidio, apocope, desectio, praesectio, recisus 2.,
-ξύλου detruncatio, -χρέους decisio, decoctio, -αὶ χρεῶν tabulae
novae.

**ἀπόκοπος** castratus, eviratus, ὁ ἐκτμηθεὶς τοῦ αἰδοίου eviriatus.

**ἀποκόπτω** decido, praecido, recido, succido, -ει abscidit,
ἀποκεκομμένον abscisum.

**ἀποκοσμῶ** dedecoro, deorno, emundo.

**\*ἀποκόψιμος** succidaneus.

**ἀποκρέμαμαι** dependo.

**ἀποκρεμνῶντες** ταῖς δοκοῖς βάρη phalangarius (ii).

**ἀπόκρημνος** abruptus, praeruptus (τόπος), -ον praeceps (*neutr.*),
-α prorupia.

**ἀπόκριμα** responsum.

**ἀποκρίνω** abiudico, -ομαι respondeo.

**ἀπόκρισις** pronuntiatio, responsio, responsum, - ἡ διὰ λόγων
elogium. *Cf.* apocrusis.

**ἀπόκροτος** perfidus, rigidus.

**ἀπόκρουσις** *cf.* apocrusis (*expl.* depulsio), - σελήνης
interlunium.

**ἀποκρούω** *et* **ἀποκρούομαι** propulso.

**ἀποκρύπτω** abdo, abscondo (*cf.* abscedo), occulto, recondo, -ει
ementitur, -ομαι deliteo, delitesco, ἀποκεκρυμμένος abditus,
absconsus, -ον abstrusum, reconditum.

**ἀπόκρυφος** abditus, absconsus, arcanus, dubius, obscurus, occultus,
secretus, -ον absconditum, abstrusum. *Cf.* apocrypha.

**ἀπόκρυψις** latitatio.

**ἀποκτείνω** *et* **ἀποκτιννύω** occido, perimo, ἀποκτιννύω πληγαῖς
elido.

**ἀποκύησις** puerperium.

**ἀποκυλίω** *cf.* devoluta.

**ἀποκυρῶ** abrogo.

**ἀποκύρωσις** abrogatio.

**ἀποκυῶ** conixo, enitor, enixo, -ήσασα conixus (-a), enixus (-a).

**ἀποκωλῦσαι** diutare.

**ἀποκωφῶ** exsurdo.

**ἀπολακτίζει** aspellit.

**ἀπολαμβάνω** consequor, deprehendo, recipio, recupero, ἀπέλαβεν
accepto sibi fecit.

**ἀπόλαυσις** fruitio.
