**Interfectio** ἀναίρεσις III 127, 22.

**Interfector** φονεύς III 452, 59.

**Interfectus** occisus, extinctus IV 356, 42.

**Interfemus** μεσομήριον II 368, 16. **interfemora** μεσομήρια III 176,
23; 249, 3.

**Interfiat** interficit *Plac.* V 78, 23 (interficiatur *Deuerling. cf.
Lucr.* III 872).

**Interficio** διακόπτω II 271, 55. κατασφάζω II 344, 21. ἀναιρῶ III
127, 18. **interficis** ἀναιρεῖς III 127, 19. **inter­ficit** ἀναιρεῖ II
89, 54; III 127, 20. **interfic\<i\>to** interrumpe *Plac.* V 29, 7 = V
78, 24 (*Apul. Metam.* XI 24 *contulit Deuerling. an* interfato?).
**interficere** ἀναιρεῖν II 89, 55. **interfecit** ἀνεῖλεν II 89, 53;
III 127, 21. *Cf.* **interficere** ἀναιρεθῆναι III 127, 22.

**Interflues** μεθόρια II 89, 56; 366, 28.

**Interfor** διαλαλῶ III 452, 60; 483, 33. **interfatur** interloquitur
IV 99, 10; V 461, 32. **interfabor** interlocor IV 99, 11 (-ar *a*);
251, 35; V 461, 41.

**Interfusa** frequentius recurrens *Plac.* V 78, 25.

**Intergeries paries** τοῖχος ὀ δόο κτήσεις διορίζων II 89, 57
(intergeriuus *c*). **intergerus** (! -iuus *b e*) μεσότοιχον II 523, 4.
*Cf. Festus Pauli p.* 110, 21.

**Intergit** ἀπομάσσει II 89, 16.

**Inter haec** dum hoc \<geritur\> V 304, 63.

**Interibi** interea uel interim *Plac.* V 28, 25 = 78, 26; 571, 5. pro
interea IV 98, 50; V 461, 34.

**Intericit** σχετλιάζει II 90, 7.

**Interiectio** σχετλιασμός II 90, 5; 450, 4; 495, 2. \<σ\>χετλιασμός
III 328, 31. *Cf.* III 328, 30. παρεμβολή[ς] III 328, 29. est motus
uocis affectu\<m\> animi demonstrans *Plac.* V 78, 28.

**Interiectum** σχετλιασμός II 500, 34; III 452, 61 (σχεδιασμός); 483,
52 (*item*).

**Interiectus** interpositus IV 90, 16. **interiectum** inclusum, situm
IV 526, 37.

**Interim** ἐν τῷ μεταξύ II 300, 56. ἐν τοσούτῳ II 90, 8; 300, 46. ἔσθ'
ὅτε (*v.* interdum), ἐν τῷ μεταξύ II 89, 12. τέως II 454, 53.
**inter\<im\>** tantisper IV 356, 37 (*corr. Hildebrand*). *V.*
tantisper.

**Interimentum** *v.* intertrimentum.

**Interimo** (-emo *cod. corr. a e*) σφάζω II 449, 11. **interimit**
interfecit, occisit (!) IV 356, 46. **interemerit** ἀνέλοι II 89, 48.

**Interionis** *v.* coloquinthida.

**Interior** ἐσώτερος II 315, 46. ἐνδότερος II 298, 19. ἄδυτος III 238,
42 (*vetusta*?). domesticus IV 356, 47. **interius** ἐσώτερον II 315,
47; III 141, 11. ἐσωτέρω II 315, 48. ἐνδοτέρω II 89, 59 ἐνδότερον II
298, 20. introrsus IV 356, 50. **interiori** ἐνδοτέρῳ II 298, 21.
**interiora** secreta IV 356, 49. *V.* intimus.

**Interitus** ἀναίρεσις II 90, 2. ἀπώλεια II 243, 35; 488, 52 (ἀπωλη);
511, 49; 538, 28. ἀπώλεια, ἀναίρεσις II 550, 40. **interitum** ἀπώλεια
II 90, 3. **interitus** ἀφανισμός II 252, 28. διαφθορά II 275, 37.
ὄλεθρος II 381, 45. pessum, pestilentia IV 357, 2. **interitus** dictus
quasi interueniens V 552, 52 (*cf. Serv. in Georg.* IV 226; *Aen.* V
735). **interitu** ἀπωλείας II 90, 1.

**Interlectio** ὁ στοχασμός II 519, 6 (intellectio?).

**Interlectus** *v.* interlitus, interpolatus.

**Inter legendum** in lectione IV 98, 44.

**Interlinitus** *v.* interlitus.

**Interlitus** intercessio (= intercisio) uerbi, quando inter se
oblitterantur IV 356, 51 (*cf.* intentando). interlinitus uel intercisio
uerbi, quando inter se oblitera\<n\>tur V 523, 3; 571, 6. intercisio
uerbi IV 252, 10; V 541, 11.

**Interlitus** interlinitus IV 99, 23; 100, 12; 251, 23; 526, 27; V 213,
3. interlinitus uel ab eo quod est interlinor V 303, 62. **interlitam**
bismiridae (*vel* bismirida, *AS.*) V 366, 48. *Cf. Landgraf. Arch.* IX
387.

**Interlocutio** ἀπόφασις II 242, 22. διαλαλία II 272, 8; III 452, 62;
475, 58; 492, 7. indicium IV 252, 6. **interlocutiones** διαλαλίαι III
482, 46.

**Interloquo** (interloco *cod.* -locor *e*) διαλαλῶ II 272, 9.
**interlocutus** διελάλησεν II 90, 15.

**Interluceo** παραφαίνω II 397, 7.

**Interlucesco** διαφαίνω II 275, 27.

**Interluctat** (*vel* inluctat) reluctat, inpedit IV 356, 52 (*cf.*
interpellantem: *unde* interpellat *H.*).

**Interlunium** μεσοσέληνον II 368, 22. ἀπόκρουσις σελήνης II 90, 13.
interstitio lunae IV 356, 53. inter primam et nouissimam lunam IV 251,
47; V 304, 21. *Cf. Isid.* III 54.

**Interluo** διακλύζω II 271, 47. **interluit** perfundit IV 100, 40.
interlabitur V 552, 60 (*cf. Serv. in Aen.* III 419).

**Intermetium** τὸ μεταξὺ τῶν καμπτρῶν (καμπτήρων *d e*) II 90, 9.

**Intermina** insuperabilis aut termino carens IV 99, 19; V 213, 6
(insep.); 461, 43.

**Intermina** internuntia, abiecta (*vel* obiecta), mediatrix IV 99, 28;
251, 40 (interminia *codd.* innuntia *vel* inter nuptias *iidem*); V
461, 49 (*ubi* intermedia *Hilde-*
