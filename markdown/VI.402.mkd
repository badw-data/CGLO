**Etrusca** Τυρρηνή II 63, 7. **Etrusci** (*vel* Erusci) Tusci IV 65,
24; 233, 15; V 453, 53; 454, 6; 498, 65. Eruli seu Tusci V 453, 57
(*cf.* V 498, 65). Eruli seu caterua V 568, 17 *V.* Eruli.

**Etruscum aurum** insigne libertatis, quod diuites in auro habebant,
pauperes in loro, bullam dicit, quam Etruscis oriundus Tarquinius
inuenit V 656, 23 (*Iuvenal.* V 164).

**Etsi** καὶ εἰ II 335, 56. licet quae (*vel* quaeue, *ex* IV 383, 38?)
uel quamquam IV 337, 20.

**E\<t te\> populariter** (epopulariter *G.* expopularint *R.*
expopulariter *b in marg.*) **iactas** et te apud populum uel more
populi (populum uulga os *R.* populum more *vel* populum *G.* populi in
uulgus *Buech.*) iactas *Plac.* V 20, 18 = V 65, 8. *Ad. Plauti Rud.*
927 *refert Buech.* (ut liberes expopulariter te *scribens*).

**Et tu** καὶ σύ III 342, 21.

**Et ualde cupio** *v.* cupio ualde.

**Et ueni mecum** καὶ ἐλθὲ μετ' ἐμοῦ III 524, 16.

**Etymologia** ἐτυμολογία II 316, 11. paratum uerbum IV 233, 20.
proprietas V 358, 29. origo uocabulorum uel proprietas gloss. *Werth.
Gallée* 340.

**Eu** uox respondentis IV 63, 48; V 195, 22; 542, 19. laudantis est uox
IV 233, 37. interiectio laudantis IV 337, 21. **\<h\>eu** interiectio
dolentis est, **eu** uox respondentis, **\<h\>eus** uero uox clamantis
(*cf.* aIV 63, 47) est V 195, 7 + 8. \<h\>eu interiectio dolentis,
\<h\>eusuox clamantis V 195, 20 + 21. *V. Isid. Diff.* 197. *Cf.* heu,
euphonia.

**Euantes** furientes, bacchantes V 499, 10. bacchantes a Libero qui
**Euan** dictus est V 551, 56. **euantes** fugentes (*h. e.* furentes)
IV 233, 44 (*Verg. Aen.* VI 517).

**Euax** εὖγε II 316, 25.

**Eubo** (? exudo *Oehler.* euaporo *Semlerus.* sebo *Buech.*) oleo IV
514, 50; *Scal.* V 598, 19.

**Euboicorum** graece genus maiorum taIentorum V 358, 2. *Cf. Festus
Pauli p.* 78, 14; *Oros.* IV 11, 2.

**Eucharistia** bona gratia, quia charis graece gratia est II *praef.*
XIV. gratia (*= Eucher. instr. p.* 160, 17) *c* IV 65, 49; IV 233, 51.
**eucharistias** gratiarum actiones V 358, 30.

**Euchochous** orationem fundens IV 337, 42.

**Eudoxia** claritas IV 410, 47; V 358, 3. **eudoxa** bona gloria, quia
doxa gloria est II *praef. p.* XIV.

**Eudulia** bonum seruitium V 358, 55.

**Euge** εὖγε II 316, 25. aduerbium euphonia laudantis uel hortantis
*Plac.* V 18, 14 = V 66, 3. laudatio IV 440, 42. laudatio seu beate *a
post* IV 63, 48. laudatio siue salutatio IV 66, 27. laudatio siue
saluatio (!) V 542, 13. bene IV 515, 31; V 291, 12; 542, 12. gaude V
357, 15. uox laetitiae uel ualde V 542, 17. laetus, alibi ua, sed hoc
duas partes Donatus (*cf. GR. L.* IV 146, 29) siue in bona siue in mala
IV 66, 23 (duobus partibus *a*); V 454, 9 (duabus partibus). sermo
blandientis est uel uox congratulantis seu collaudantis id est bene
gaude V 568, 36. *V.* euphonia.

**Eugenia** nobilitas V 358, 56; IV 233, 23 (*ubi* humelitas *praeter
c*).

**Eugenis** nobilis IV 233, 21; V 291, 19 (*v.* euidet). **eugenes**
nobiles V 358, 4.

**Eugium** media pars \<inter naturalia muliebria\> V 641, 24 (*Non.*
107, 26).

**Eulogias** salutationes V 413, 18 (*reg. Bened.* 54, 4).
benedictiones [**exarchus** patric\<ius\>] *gloss. Werth. Gallée* 340
(*v. suppl.*). *V.* euphonia.

**Eulogium** \<duorum locutio\> II 578, 49 (*suppl. b, male*). *V.*
elogium.

**Eu[li]logus** uersiculus V 499, 17. **eu[li]logi** uersiculi IV
233, 35. **eulogi** uersiculi IV 515, 30; V 291, 11. *Nescio quomodo*
elogium *subest.*

**Eulum** uinum V 291, 25 (Euhium?).

**Eumenides** Εὐμενίδες III 237, 29. Furiae IV 440, 44. **Eumenidum**
Furiarum IV 62, 40; 66, 15; 231, 50; 233, 50. *Cf.* **Eumenidus**
furiosus, iratus V 499, 16 (**Eumenidum** Furiarum, Irarum?).

**E\<u\>morphius** p̄p̄ (= proprium) V 423, 39 (*Gregor. dial.* IV 35).

**E\<u\>morphos** graece pulcher *gloss. Werth. Gallée* 339 (*v.*
*suppl.*).

**Eum\<p\>se** eum ipse uel eum ipsum V 454, 11. eum V 634, 13.
**eamsi** eum ipse (?) V 452, 33. **eapse** ea ipse V 568, 1. **emsoe**
(eopse? *v.* et ipse) eo ipse V 193, 18.

**Eundi** παριέναι II 63, 28.

**Euntis** ire uolentis IV 66, 5. **euntes** ire uolentes IV 440, 46
(*Verg. Aen.* II 111).

**Eunuchizati** qui ab hominibus eunuchi facti sunt V 195, 14 (*Roensch
It.* 249).

**Eunuchus** Herculaneus IV 440, 45. **eunuchus** et **spado** unum est
V 300, 21. *V.* herculaneus.

**Eunum** (= εὐνοῶν) beneuolus V 416, 63 (*Hieron. in Matth.* 5, 25).

**Euochias** delicias IV 233, 36.

**Euoe** ἐπίφθεγμα εἰς Διόνυσον II 312, 43.

**Eupatorium** *v.* argemonia, marrubium.

**Euphonia** uocis sonus suauis *Plac.* V 19, 2 = V 66, 2. sonoritas,
bonus sonus; eu quippe in compositione nominum siue uerborum bonum
sonat, ut euge, bene,
