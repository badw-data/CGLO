IV 508, 49. diligenter aut ex toto IV 508, 46. clare seu diligenter V
454, 12. absolute siue certe uel exquisite V 291, 48. ex toto IV 70, 4;
V 454, 13. integre, diligenter IV 234, 5. inquisitiue V 422, 36 (*de
Clemente*). **exarnusim** lusio tabulae V 499, 28 (*cf.* datatim).
*Cf. Festus Pauli p.* 80, 19.

**Exanclandi** peragendi *Plac.* V 66, 9; IV 70, 14; 72, 3; 509, 20; V
454, 16; 634, 22.

**Exanclata** exhausta et quasi exangulata, id est per angulos quoque
(quosque *Kettner*) exquisita *Plac.* V 21, 29 = V 66, 12.

**Exanclatio** ἐκπλήρωσις, ἀκρίβεια III 447, 42; 480, 25.

**Exanclo** (exanthlo *cod.*) exinanio, examino, exhaur\<i\>o, euacuo V
568, 43. **exanclat** ἐξαντλεῖ, δαπανᾷ III 447, 41; 480, 24. ἀρύεται II
63, 48. euacuat V 499, 30. **exanclare** exhaurire ex graeco ueniens,
quod quidem uerbum Plautus, saecularis poeta comicus, posuit ita (*om.
R*) in Sticho (*v.* 272 *sqq.*). ne iste edepol uini poculo pauxillulo
saepe exanclaui\<t\> *Plac.* V 20, 2 = V 66, 10 (*cf. Festus Pauli p.*
80, 13). exanimare uel exinanire *Plac.* 21, 5 = V 66, 11. exhaurire IV
235, 61. exhaurire, egere\<re\> V 454, 18; 499, 25. perpeti V 641, 23 (=
*Non.* 107, 21). **exanclasti** exhausti (!) IV 69, 24; V 454, 15; 634,
24 (exhausisti: *Apul. Met.* I 16). **exanclauerint** pertulerint V 195,
25. *V.* antlia.

**Exanimatio** ἀθυμία II 219, 57.

**Exanimatus** ἄψυχος II 255, 6. timore percussus, turbatus IV 69, 33
(*Ter. Andr.* 131). perterritus *a* IV 71, 49. **exanimatum** ἄψυχον II
63, 49. perturbatum V 533, 26 (*Ter. Andr.* 342).

**Exanimis** ἄπνους II 235, 39, **exanimus** νεκρός II 63, 50.
**exanimis** sine anima IV 69, 11; 234, 42; 509, 18. mortuus,
exanimatus, timens V 551, 63 (*v. Isid. Diff.* 196). mortuus,
**exanimis** autem et **exanimus** dicimus sicut unianimus et unianimis,
inermus et inermis et hoc nostro arbitrio subiacet V 195, 26.
**exanime** mortuum *a* IV 71, 3. **exanimem** sine anima IV 440, 50.
*Cf. Serv. in Aen.* I 484; IV 672; *Donat. in Hec.* III 3, 4.

**Ex animo** libenter IV 69, 40; V 533, 42 (*Ter. Ad.* 72).

**Exanimo** ἐκπνέω II 292, 17. ψυχορραγῶ II 481, 30, **exanimat**
occidit IV 68, 35; *a* IV 71, 3; 509, 19. occidit, interficit IV 337,
53. *V.* examino.

**Exapla** *v.* hexapla.

**Ex aqua** ἐξ ὕδατος III 183, 59; 516, 29. ἀφ' ὕδατος III 314, 35.

**Ex aqua madidam** ἐξ ὐδατος τακερόν III 287, 61/62 = 658, 18.

**Ex arca** ὑπὲρ ὠφελείας III 480, 33 (*v.* e re).

**Exarchus** *v.* eulogias.

**Exardeo** ἐκκαίω II 290, 24 (exardo *cod.*). ἐκκαίομαι II 290, 25.
**exarsit** increbuit IV 411, 5 (*Verg. Aen.* V 172?).

**Exaresco** ἀποξηραίνω II 239, 28. **exaruit** siccauit IV 337, 55.

**Exaro** ἐξαροτριῶ II 302, 11.

**Exarticulatio** ἐξάρθρημα III 363, 55.

**Exasperans** ad amaritudinem prouocans V 568, 41 (*cf. Vulg. Ez.* 2,
8).

**Exasperatus** ἠγριωμένος III 143, 52. inritatus IV 71, 50.
**exaspera[s]ti** ἠγριωμένον III 143, 53.

**Exaspero** ἐκτραχύνω II 293, 21. ἐξαγριῶ II 301, 20. **exasperat**
exulcerat IV 337, 56. *V.* exacerbo.

**Ex asse** ex uno siue ex omnibus *gloss. Werth. Gallée* 340 (*v.*
*suppl.*).

**Ex asse heres esto** ex integro, ex omni patrimonio V 292, 46. *Cf.
Loewe GL. N.* 127.

**Exauctoro** ὑπεραυθεντῶ II 463, 61. **exauctoriat** auctoritatem
tollit dimicantis V 292, 61. **exauctorauit** giheldae (giheendae
*gloss. Corp. Christi: AS.*) V 357, 59.

**Exaudibilis** *v.* facilis.

**Exaudiens** ἐπηκοος II 63, 51; 306, 53.

**Exaudio** εἰσακούω II 286, 49. ἐπακούω II 305, 25. **exaudi**
εἰσάκουσον II 286, 47.

**Excaecatio** ἐκτύφλωσις II 293, 33.

**Excaecatus** obtunsus IV 337, 58.

**Excaeco** ἐκτυφλῶ II 293, 32; III 114, 52 = 643, 24. **excaecauit**
obtenebrauit aut ualde caecauit: ex enim pro ualde ponitur V 195, 29.

**Excalcias** ὑπολύεις III 405, 69. **excalcia** ὑπόλυσον II 466, 54;
III 6, 64 (exculcia!); 405, 68; 447, 43. **excalciate** ὑπολύσατε III
405, 72. **excalciaui me** ὑπελυσάμην III 405, 71. **excalciasti**
ὑπελύσω III 405, 70. **excalcior** ὑπολύομαι II 466, 53. *Cf.* III 287,
3 = 657.

**Excalo** elicio *gloss. lat. arab. p.* 704, 29.

**Excandesco** ἀγανακτῶ II 215, 44. ζέω ἐπὶ ὀργῇ II 322, 11.
**excandebat** exardebat seu candidum fiebat V 454, 20. **excanduit** in
iracundiam (*vel* -a) exiliuit IV 67, 4; 235, 52; 509, 4; V 292, 33
(*Iuv.* X 327). *V.* explano.

**Excandiscentia** iracundia frequentia V 640, 72 (*Non.* 103, 13.
frequens *Luc. Mueller.* feruentia *H.*).

**Excanet** praecanet IV 70, 27; 509, 5; V 195, 27; 454, 22; 26. *V.*
excauet.

**Excantare** excludere, subripere V 640, 62 (*Non.* 102, 7. *v.*
excludo, ecanto).
