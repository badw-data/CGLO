κατωφερής, καταγύναιος II 177, 23. **salaces** κατωφερεῖς II 177, 12.

**Salebra** uia inaequalis IV 166, 25. uia inaequalis, id est lapidosa V
242, 15. loca lutosa IV 166, 9; 281, 18; 29; 388, 32; V 330, 19.
**salebrae** ἐν ταῖς ὁδοῖς ἀνωμαλίαι καὶ βόθυνοι II 177, 26 (-ab-).
thuerhfyri [(*AS.*)]{.smallcaps} V 388, 25. *Cf.* **salibra** matassa (=
mataxa?) V 578, 25 (*contam.*?).

**Salebrosus** asper IV 166, 8; 281, 17; 564, 6; V 330, 35; 388, 24.

**Sale conditum** ἁλασάρτυτον III 184, 13; 255, 1. ἁλιστόν III 288, 19 =
658, 18. σαλλάκωνα III 288, 18 = 658, 18. *Cf.* **sale conditus** quod
supra (?) IV 166, 6; 564, 5. *Cf. Bluemner 'Maximalt.'* 72.

**Salentinus** Calabriensis IV 281, 32. *V.* Sallentinum.

**Salgamarius** παντοπώλης II 393, 48. ἁλμαιοπώλης III 307, 13.

**Saliares cenae** sumptuosae quae fiunt \*\*\* V 513, 11. **saliares
cenas** sumptuosas quae fiunt a \<Sa\>liis V 481, 12. quae fiunt a
Saliis IV 166, 33. *Cf. Fest. p.* 329, 9.

**Salibra** *v.* salebra.

**Salicis semen** *v.* salix.

**Salictum** ἰτεών II 333, 59; 527, 45; III 264, 32 (salicetum.
*unde?*). uirgultum IV 166, 17 (salctum); 564, 16. **salectus** locus in
quo salices sunt V 146, 14. **salictum** locus uel genus quod cito
[de]crescit V 389, 54 (*Serv. in Ecl.* I 54). **salicta** ubi salices
nascuntur V 390, 20. salices. Virgilius (*Georg.* II 13: *cf. Serv.*):
populus et glauca canentia fronde salicta V 146, 8; 242, 16. *V.*
saliuncula.

**Salii Collini** Κουρῆτες III 302, 10. **Collini** Κουρῆτες III 10, 15;
83, 69.

**Salii Palatini** Κορύβαντες III 10, 14; 83, 68; 302, 9.

**Salinae** ἁλαί II 177, 24 (*ubi* ὰλεῖαι *c.* ἁλικαί *GR. L.* I 33,
23); 224, 36. sunt sterilitates, quia sal si ceciderit super terram
sterilem facit V 624, 6.

**Salinum** ἁλιά III 324, 56 (*inter ar­gentea*). *V.* catinus.

**Salio** πηδῶ II 407, 20. **silio** πηδῶ III 153, 4. **salit** ἅλλεται
II 177, 33. ἅλλεται, πηδᾷ, ὕδωρ ῥεῖ II 177, 40. **siliere** silierunt.
legitur et saluere, ut rapuere, ad eiusdem regulam uerbi V 150, 5
(*Verg. Georg.* II 384).

**Salio** ταριχεύω III 160, 16. **sallio** uel **sallo** sale condio uel
condo V 513, 10.

**Salisatores** quacumque membrorum parte cum salierint diuinant V 513,
13 (*Isid.* VIII 9, 29). *Cf. gl. lat. arab. p.* 451 *Seyb.*

**Salis[s]atio** παλμός II 177, 37. *V.* sagatio, saltio.

**Salitor** ταριχευτής II 177, 39; 41.

**Salitores** πηδηταί, οἱ ἁλματισταί III 240, 52 (*unde*?).
**silitores** πηδηταί III 173, 23. *Adde* **silitor** πηδητής III 153,
5. *V.* salio 1.

**Salitum** *v.* sartus.

**Saliunca** ἀγριόροδον, λευκόροδον II 177, 28. ἀγριόρροδον III 266, 33.
celtica III 556, 58. uirgulti [flore] genus V 481, 10 (*cf. Serv. in
Ecl.* V 17). *V.* saliuncula.

**Saliuncula** ἀγριόροδον II 217, 24. ἀνεμώνη II 225, 51. ἁλμυρίς III
256, 35 (*unde*?). celtica III 621, 24. colita (= celtica) id est fasce
galicos (*v.* nardus celtica) siue **saliuncula** III 556, 59 = colita
id est fasce zalicos id est **saliunca** III 621, 25. **saliuncula**
salices, quae uelociter saliunt, id est crescunt IV 388, 33 (*v.*
salictum, *cf. Is.* XVII 7, 47). salices ab eo quod uelociter crescunt
IV 166, 11; V 146, 12. **saliuncus** salices qui uelociter crescunt V
394, 25. **saliunculae** salices sunt qui uelociter saliunt, id est
crescunt V 330, 37. *V.* salix lenta.

**Salius** ἱερεὺς ὁ τὰ διοπετῆ ὀπλα θεραπεύων, ὑμνῳδός, πρύτανις II 177,
36 (salior *cod. corr. d*). ἱερεὺς ὁ τὰ διοπετῆ ὅπλα θεραπεύων II 177,
43. ἱερεὺς Καίσαρος II 331, 13; III 237, 68. ἱέραξ dialis, **salius**,
accipiter, auis III 238, 1 (*contam.: unde?*). εἶδος ἱερέως II 330, 55.
ἱερέως εἶδος II 331, 15. sacerdos Ionis uel armiger Caesaris II 592, 17.
**Salii** qui circa aras saliebant tripudiantes V 556, 59. *Cf. Serv. in
Aen.* VIII 285; 663. **Saliorum** strionum (= histrionum) V 513, 9.
**Salarium** salariorum, strionum V 578, 24 (salarium *et* Saliarium
*confusa* ?). **Salios** nobiles V 654, 20 (*Iuvenal.* VI 604). *V.*
capedo, saltator.

**Saliua** σίελος II 177, 38; 431, 27; 520, 23; 541, 41; III 11, 48/49;
247, 53; 349, 42; 394, 47 (*GR. L.* I 553, 11). σίελον III 310, 58.
σίαλος II 431, 14 (siliba); 493, 64; III 85, 15; 175, 42.

**Saliua latina** V 664, 46.

**Salix** ἰτέα II 177, 34; 333, 58; III 264, 31; 358, 28 (salice); 66;
397, 17 (salice); 418, 68; 428, 68; 496, 24; 547, 6; 565, 2 (salix
arbor); 583, 56 (*item*). salch (*vel* salh, *AS.*) V 389, 34. *Cf.*
**semen de salice** ἰτέας καρπός III 539, 49. **salicis semen** ἰτέας
καρπός III 565, 45. **cortex salicis** ipsicis (? σίδια? ὑποκυστίς?) III
546, 74; 583, 40 (ipsius). **salices** ab eo quod uelociter crescunt IV
564, 7. *Cf. Isid.* XVII 7, 47. *V.* in salicibus, salictum, saliunca.

**Salix graeca** ἄγνος δένδρον II 177, 44.
