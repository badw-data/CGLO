-οντα τῷ πράγματι meditata, ἡρμοσμένος aptus.

**\*ἁρμολόγησις** compaginatio, compagitus (*cf. p.* 436).

**\*ἁρμολόγος** coagmentarius.

**ἁρμονία** *cf.* harmonia, - ἡ τῆς κιθάρας series, -μελῶν ἀνθρώπου
compago.

**ἁρμός** commissura, compages, internodium, - μελῶν artus 2.

**ἅρμοσις** aptatio, compaginatio.

**ἁρμοστης** aptator, -γάμου sponsus.

**ἁρμοστικός** sponsalis.

**ἁρμοστός** aptus, habilis, -ή desponsata, sponsus (-a), νύμφη -ή
sponsa sponsata.

**\*ἅρμοστρα** (*plur.*) sponsalia.

**ἀρνακίς** (τὸ προβάτων δέρμα) ovina pellis, pellis lanata.

**ἀρνειός** agnus.

**ἄρνειος** (ἄρνιος): **ἄρνειον** *et* **ἄρνεια** agnina (*v. p.*
434), agnina caro, -ον κρέας agnina.

**ἄρνειον** (-νιον) lapadia, nymphaea, plantago.

**ἄρνες** agna (-ae), agnus (-i).

**ἄρνησις** infitiae, -γλώσσης infitiatio.

**\*ἀρνησίχωλος** claudaster.

**\*ἀρνητής** infitiator, negator.

**ἀρνίον** agna, agnus.

**ἀρνόγλωσσον** *et* **ἀρνόγλωσσα** lingua vervecina, plantago.

**ἀρνός** *cf.* hagnos arnus.

**ἀρνοῦμαι** diffiteor, infitior (*cf. p.* 437), nego, recuso,
-ούμενοι infitentes.

**\*ἀρνών** ὁ τόπος agnile.

**ἀροτήρ** arator.

**ἀρότης** arator.

**ἀροτρίασις** aratio, aratura. *Cf.* cyma.

**ἀροτριῶ** aro. *V.* ἀγρός.

**ἄροτρον** aratrum. *Cf.* ἐχέτλη, ῥυμός.

**ἄρουρα** arvum, defrensa, rus, seges, - τεθερισμένη *et* θερισθεῖσα
defrensa.

**ἀρουραῖος** arvalis. *Cf.* μῦς.

**\*αρουσια** *vel* **αρυσια** eluvies, illuvies.

**ἁρπαγή** clarigatio, duellio, ereptio, praeda, praeda et rapina,
rapacitas, rapina, μετὰ -ῆς raptim.

**ἁρπαγίμως** raptim.

**ἁρπάζω** eripio, praedor, rapio, rapto, -ομαι rapio, ἡρπαγμένος
raptus (*cf.* ἡρπαγμένως), ἁρπαγείς raptus.

**ἁρπακτικός** rapidus.

**ἅρπαξ** hemerocoetus, procax, rapax, raptor.

**ἁρπεδόνη** compedes (-pes), laqueus, pedica.

**\*ἁρπεδόνιον** pediculus *sim.*

**ἁρπεδών** pediculus *sim.*, -όνες pedani.

**ἅρπη** falx messoria, ossifragus. *Cf.* aripus, harpa, harpis.

**ἅρπυια** (*Eins.*) harpyia.

**ἀρραβών** arra (*cf. etiam* masculinus).

**ἀρραγής** inviolatus.

**ἀρρενικός** *vel* **ἀρσενικός** masculinus, -όν auripigmentum, -ὸν
γένος masculino genere.

**ἀρρενογονία** virilis sexus. *Cf.* ἀγχιστής, συγγένεια, συγγενής.

**ἀρσενοκοίτης** masculorum concubitor.

**ἄρρηκτος** irrumpibilis, -οι artus (-iores).

**ἄρρην** *et* **ἄρσην** mas, masculus. *Cf.* λίβανος, μαζός, φθορά.

**ἄρρητος** inauditus, infandus, nefastus.

**ἀρρυθμία** abnormitas, enormitas.

**ἄρρυθμος** abnormis, enormis.

**ἀρρωστία** aegrimonium, aegritudo, infirmitas, languitas,
languitudo, morbus.

**ἄρρωστος** aeger, aegrotus.

**ἀρρωστῶ** aegroto, imbecillor, langueo, languesco.

**ἀρσεν-** *v.* ἀρρεν-.

**ἄρσις** accentus. *Cf* arsis.

**ἀρτεμίδιον** dictamnus.

**Ἄρτεμις** Diana, -φωσφόρος Diana lucifera.

**Ἀρτεμίσιος** Artemesios. *Cf.* menses.

**ἀρτεμισία** artemisia, febrifugia, origanum.

**ἀρτέμων** (πλοίου) artemo. *Cf* phaseolus.

**ἀρτηρία** arteria, vena vitalis.

**\*ἀρτηρίασις** *cf* arteriasis (*expl.* raucitudo vel asperitas in
faucibus).

**ἀρτηριοτομίαι** *cf.* arteriotomiae.

**ἄρτι** modo, noviter, nuper, ὡς ἄρτι δύναμαι quomodo possum (quomodo
modo possum).

**ἀρτιγένειος** prima barba.

**ἄρτιος** integer, ὁ τέλειος ἀριθμός integer, perfectus.

**ἀρτίως** modo, tantum quod.

**\*ἀρτοεψητής** furnus.

**ἀρτοθήκη** panarium.

**ἀρτοκοπεῖον** *et* **ἀρτοκόπιον** pistrinum *sim.*

**ἀρτοκοπικός** pistriniensis, -όν cerealia arma (*ib.* -ὸν ἐργαλεῖον
cerealium).

**ἀρτοκόπος** pistor, pristarius.

**ἀρτόκρεας** visceratio.

**ἀρτοποιεῖον** panificium.

**ἀρτοποιία** panificium.

**ἀρτοποιός** panificus, pistor, pistrix.

**ἀρτόπτης** *cf* artopta.

**ἀρτοπώλης** panarius.

**ἄρτος** panis, -γυρίτης pollina, pollinaceus, - καθαρός siliginium.
*Cf.* ἐντυπή, φλοκτίς, χυδαῖος.

**ἀρτοφόρον** canuam (cannula), panarium.

**ἄρτυμα** condimentum.

**ἄρτυσις** conditura.

**ἀρτυτόν** conditum. *Cf.* ἅλας.

**ἀρτύω** condio, condito, ἠρτυμένον conditum.

**ἀρτῶ:** ἤρτημαι dependo, pendeo, pendo, -μένος incertus, suspensus.
