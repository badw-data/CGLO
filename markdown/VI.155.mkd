**Buccans** garriens IV 602, 29.

**Buccella** ψωμός III 81, 27. ψωμίν III 572, 68. **buccilla** ψωμός III
7, 39; 467, 50. **bucilla** ψωμός III 440, 41. **buccellae** ψωμία III
164, 60. **bucellae** ψωμία III 340, 25; 440, 40. **bucellas** tortelli
minuti III 598, 3 (tortelli *a* torta, *genus placentae significant*).
**bucellae** (bucellat) sicca et extenuata corpora V 563, 55.
**buccellum** ψωμίον II 481, 42.

**Buccellarius** *v.* assecula, galearii, cerbarii, parasitus, scurra.

**Buccellatarius** *v.* parasituli.

**Buccellum** *v.* buccella.

**Buccidine** *v.* buceriae.

**Bucco** garrulus, quod ceteros oris loquacitate, non sensu exsuperat
(= *Isid.* X 30) IV 594, 39; 602, 28. garrulus, quod ceteros oris
loquacitate non sensu superet, rusticus, stultus V 592, 6 (*cf. Loewe
Prodr.* 82). sacerdos rusticus (sacerdos *suspectum, cf. Arch.* II 344)
V 493, 19. *V.* eggones. **buccones** παράσιτοι, βουκκίωνες II 31, 37.
stulti, rustici *cd post* IV 26, 33; 213, 2; 314, 49; 490, 3; 586, 25;
594, 49; 601, 39; V 172, 16; 48; 272, 5; 35; 349, 11; 404, 25; 443, 57;
632, 41. **baccunis** rustici[s], stulti[s] V 170, 17. **batinius**
rusticus, agricolanus V 591, 53 (*cf. Loewe Prodr.* 68). **bucones**
garruli, stulti, rustici V 563, 44. *Cf. Loewe Prodr.* 265. *V.*
rusticus, bacuceas.

**Buccones** *v.* bolona.

**Buccosus** γνάθων III 330, 51; 495, 63 (gnatus); 512, 45. *Cf. Funck
Arch.* VIII 372.

**Buccula** ὁ ψωμός (ψωμίον *be*) II 517, 39. παραγναθίς II 31, 32.
umbo, randbaeg (*AS.*) II 570, 29. ὀμφαλός III 368, 48. εἶδος ἀγγείου
(boccola: *ubi* botiola *d*) II 30, 43. bucc (*vel* bua *vel* buuc,
*AS.*?) V 349, 14; 404, 29. **baculus** (*vel* buc.) rondbaeg (*vel*
*recentiore forma* randbeag, *AS.*) V 348, 1. **buculus** rondbaeg
(*AS.*) V 404, 14. *Cf.* ancyla, umbo.

**Buceriae** armenta pecuaria IV 314, 47; 594, 44. armenta IV 27, 7;
212, 48; V 272, 37; 349, 13; 404, 28. *Huc refero* **buccidine** armenta
V 171, 15. **bucerias** bouum greges V 638, 54 (= *Non.* 80, 26). *V.*
bucerum.

**Bucerum** γέννημα βοός (βιος *cod.: corr. g*) II 31, 33. pecus bubulum
*cd post* IV 26, 35; 212, 47; 314, 46; 490, 7; 594, 43; 602, 26; V 171,
13; 272, 39; 349, 10; 404, 23; 443, 54. genus bubulum (buturum *vel*
butyrum *in lemm.*) IV 315, 4; 395, 23. **bucera** boues V 443, 55; 563,
48. *V.* bacerus. *Cf. Festus Pauli* 30, 3.

**Buceta** pascua IV 212, 45; 490, 1; 586, 28; 593, 38; 594, 45
(bucita); V 272, 47; 592, 8 (bucula: *cf. Loewe Prodr.* 82); 65. pascua
bouis IV 602, 25. loca bouum V 171, 14. pascua ubi cientur boues
*Scaliger* V 592, 47 (bucita: *ex Osberno p.* 80^b^: *cf. Loewe Prodr.*
74).

**Bucidae** qui boues caedunt V 171, 16. *Cf. Loewe Prodr.* 267 (*Plaut.
Most.* 884). *V.* buceriae, bucula.

**Bucina** βουκανιστήριον II 259, 9 (boc.); 496, 37. begir (*AS.*) V
347, 42. **bucina** erit tuba qua[si] signum dat bucinator, bucinus
ipse canor *Plac.* V 50, 18 (*= GR. L.* VII 99, 16; 265, 8: *ubi recte*
qua). *V.* bucinum.

**Bucinaria** uoces IV 314, 48 (bucen.); 594, 47; V 592, 9 (bucina tubae
uoces *coll. Isid.* XVIII 4, 1 *Oehler Iahnii Annal. suppl.* XIII 235).
**bucenariae** uoces V 171, 17; 443, 59. *An* bucinariae uoces *amisso
interpretamento? V.* bucinae uox *Verg. Aen.* VII 519. *Cf. Loewe
Prodr.* 82.

**Bucinator** βουκανιστής II 259, 8.

**Bucino** βυκανίζω (!) II 260, 43. βουκινίζω III 129, 21. **bucinas**
βουκινίζεις III 129, 22. **bucinat** βουκινίζει III 129, 23.

**Bucinum** βουκάνη II 31, 34. σάλπιγξ II 429, 41. βουκανιστήριον
(bocino) II 259, 9. clangor bucinae V 172, 47; 563, 43. *V.* tuba. *Cf.
Festus Pauli p.* 30, 9, *Isid.* XVIII 4, 1. *V.* bucina.

**Bucinus** κῆρυξ θαλάσσιος (*piscis*) II 349, 17. **bucini** κήρυκες
III 396, 36; 355, 27; 436, 73. *Cf. Plin. N. H.* IX 130.

**Bucitum** (?) seotu (= *Sitze uel Ställe, AS.*) V 404, 32. *V.*
buceta.

**Bucolia**, bualia, id est stabula boum V 563, 51. **bolia** stabula
bouum V 403, 43. **bolium** (bolio) stabula bouum (*vel* bonum) V 349,
6. *Nescio an huc spectet* bulum bucolium pastoris IV 314, 53; 595, 4; V
543, 17 (*unde pendet Sca­liger* V 592, 16: bullum baculum pastoris: *cf.
Loewe Prodr.* 83). **bolum** stabulum V 272, 18. *Cf. Hildebrand p.* 37.
Baculum pastorum est agolum (*cf. Festus Pauli p.* 29, 14). bacillum
pastorale baculum *Ducange.*

**Bucula** δάμαλις, deminutiue βοῦς II 31, 35. βούδιον, βουκόλιον II 31,
36. βούδιον, βουθύτης (*an* bucida *subest?*) II 31, 29. δάμαλις II 266,
19. μοσχάς II 373, 27. uitula, cucaelf (*AS.*) II 570, 27. uacca IV 27,
3; 314, 45; 594, 46; 602, 24; V 404, 24; 443, 56. uacca diminutiue IV
197, 4; 489, 32; 580, 33; V 336, 56. uaccula IV 212, 40. uacca uel
uaccula diminutiue IV 594, 36. uascula siue uaccula IV 602, 27. iuuenca,
uitula IV 489, 31. **bacula** iuuencula aut uitula *post* IV 26, 26
*cd. Cf.* **baccula** bouis femina
