τικόν cella, cellarium, pennarium, penus (penum), proma,
promptuarium, - τὸ τοῦ δημοσίου aerarium, fiscus, - Καίσαρος fiscus.

**ταμίευσις** fiscatio, proscriptio.

**ταμιεύω** fisco, proscribo.

**ταμιοῦχος** aerarius, cellariaria, cellarius, confiscator, penarius,
promentarius.

**\*ταμίσιον** coagulum.

**ταμιῶ:** ταμιοῖ confiscat.

**\*τανισια** epinia.

**ταξεώτης** *v.* ταξιώτης.

**ταξίαρχος** centurio, evocatus, manipularius, ordinarius, -οι ἱππέων
alaris (-es).

**τάξις** acies, antes (-εις), classis, cohors, legio, officium, ordo
(*v. etiam* ordior telam), series, tenor, - δουλική *v.* χρεωστώ, -
ἐσχάτη *v.* ὁ πολίτης, - ἱππική ala, turma, - πολεμική cuneum,
exercitus, tagma, - στρατιωτική cohors, collegium, legio, - ὁ στίχος
ordo, - ἡ τοῦ ἄρχοντος apparatio, officium, - πράγματος ἢ λόγου series,
τάξιν ἀντὶ τοῦ δύναμιν vicem, ἐκτὸς τῆς τάξεως extra ordinem, ἐν τάξει
vicem (vice), κατὰ τάξιν ex ordine. *Cf.* προκόπτω, σπεῖρα.

**ταξιώτης** (= ταξεώτης) apparitor, officialis (*cf.* taxiota).

**ταπεινός** humilis, iners, -ά improcerata.

**ταπεινότης** humilitas.

**ταπεινοφρονῶ** humiliter sapio.

**ταπεινοφροσύνη** humilitas, humilitatio.

**ταπεινόφρων** humiliter sapiens.

**ταπεινῶ** humilio, τεταπεινωμένος submissus.

**ταπείνωμα** humilitudo.

**ταπεινῶς** humiliter.

**ταπείνωσις** humilitatio, humilitudo. **τάπης** tapeta *sim.*

**τάπις** tapeta (tapedus).

**ταρακτικός** turbidus.

**ταράσσω** perturbo, turbo, turbulento, τετάραγμαι turgeo, τεταραγμένος
conturbatus, turbatus, turbulentus.

**ταραχή** confusio, tumultus, turba, turbatio, -ὴν ποιοῦντες bovinator
(-res).

**τάραχος** turbatio.

**ταραχώδης** asper, turbatus, turbulentus.

**ταρίχευσις** salsatio.

**ταριχευτής** salitor.

**ταριχεύω** salio 2., salso, - νεκρόν pollingo; τεταριχευμένος
salsatus, -ον allec, *cf.* ὀψάριον.

**ταριχηρόν** praesalsus (-um), praesultum.

**ταρίχιον** salsamen, salsus (-um).

**ταριχοπώλης** salsamentarius.

**ταριχοπώλιον** *et* **ταριχοπωλεῖον** salsamentarium.

**τάριχος** salsamentum, salsus (*et* -um), *cf.* tarichus (*expl.*
salsus), εἰς τὸν -ον in salsum.

**\*ταρσίς** menta.

**ταρσός** planta, ratis, talus, - τοῦ ποδός planta. *Cf.* plango.

**Τάρταρος** *et* -ον Tartarus.

**τάσις** tenor, tentigo, tentio. *Cf.* menta.

**τάσσω** *v.* τάττω.

**\*τατικόν** terribilis (territibile = tentib.).

**τάττω** ordino, τάττομαι θεῷ τι *v.* εὐχή, τέταγμαι constituo,
τεταγμένος ordinatus. *Cf.* τεταγμένως.

**\*ταυράριοι** pĭla (-ae).

**ταύρειος** culleus (*cf.* ἀσκός), -ον chelidonia, -α taurina. *Cf.*
χολή.

**ταυροβόλοι** culpatores.

**ταυροκαθάπται** pĭla (-ae).

**\*ταυρόκολλον** gluten taurinum.

**ταῦρος** taurus, - ἁγνός (*sic Buech.*) camuri (-ris), - δαμάλη
iuvencus. *Cf.* αἷμα, θυσία.

**ταύτη:** - κἀκεῖσε hac illac, αὐτῇ - ipse (ea ipsa). *Cf. etiam s. v.*
hic, iste.

**ταυτολογία** stummia, *cf.* tautologia (*expl.* repetitio sermonis bis
vel ter).

**ταυτότης** identitas.

**ταφή** humatio, sepultura, Ὀσίριος - caput canis.

**τάφος** bustum, fovea (foveus), humus, rogus, sepulcrum (*cf.*
taphos), sepultus 1., tumulus.

**\*ταφροβολῶ** fossam duco.

**τάφρος** fossa, fossatum, saepes *sim.*, scrobis, vallum.

**\*τάφρωσις** vallatus 1.

**τάχα** fere, forsitan, fortasse.

**ταχέως** celeriter, cito (*et* citius), mature, ociter, propediem,
tempus (tempore), volociter, - πάνυ praemature, - ποιοῦμαι arripio,
ταχύτερον celeriter, cito (-ius), mature (-ius), ociter (ocius), velox
(velocius), τάχιον cito (citius), θᾶττον cito (citius), mature (-ius),
ociter (ocius), velox (velocius), ταχύτατα celeriter (celerissime,
celerrime, quam celeriter), velox (veloqua), ταχυτάτως celeriter (-ius).

**τάχος:** διὰ τάχους quam celerrime, ἐν τάχει celeriter (quam
celerissime), in continenti, quam cito, quam maturius, κατὰ τάχος
citatim.

**ταχυδρόμος** cursor, exercipes.

**ταχυθάνατος** decrepitus.

**ταχύνω** accelero, celero, maturo.

**ταχύπους** alipes.

**ταχύς** celer, citus, praeceps, rapidus, velox, ταχύ cito, τὴν
ταχίστην actutum,
