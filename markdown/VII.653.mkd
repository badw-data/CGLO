**συνωρίς** *et* **ξυνωρίς** biga.

**\*συοβοσκός** subulcus.

**συοφορβός** porcarius, porcinus, subulcus.

**συριγγίας** *cf.* syringias.

**συρίγγιον:** - ἐκ καλάμων canna, - τὸ ἕλκος II, 448, 54 *s. interpr.*

**συριγμός** sibilus.

**σῦριγξ** *et* - ὁ αὐλός *et* - οὐρήθρας fistula.

**συρίζω** sibilo *sim.*, -ων singultans.

**Σύριος:** θεὰ Χυρία dea Syria.

**συρίσκος** *cf.* pultarium.

**συριστής** fistulator, sibilator.

**σύρμα** *cf.* syrma (*expl.* dictio longa).

**συρράπτω** consuo, suto, συνερραμμένον *vel* συνρερ. consertum,
consutum, correptum, sutum.

**συρράσσω** confligo.

**συρρέει** confluit.

**συρρήγνυται** ἡ χάλαζα grandino (-at).

**σύρρηξις** *cf.* syrrexis (*expl.* ruptio apostemae). *Cf.* siresis?

**συρρήσσω** confligo, -ων singultans.

**σύρροια** compluvium.

**σύρρους** compluvium.

**σύρσις** tractio, tractus.

**Σύρτιες** (*Eins.*) Syrtes.

**συρφετός** rutuba.

**σύρω** traho.

**σῦς** sus, σῦν ἄγριον *cf.* dentatus, σῦς χοῖρος *fort. scrib. s. v.*
syulis *pro* ἰσχυρός (suilis ὗς, χοῖρος *Buech.*). *Cf.* γρόμφαινα,
θρίξ, θυσία.

**συσκέπτομαι** delibero.

**συσκέπω** cooperio.

**συσκευάζω** construo, -ομαι concinno, συσκευασθείς infuscatus.

**συσκευαστής** factionarius.

**συσκευή** concinnatio, factio, illictum.

**συσκηνία** contubernium.

**συσκήνιον** contubernium.

**σύσκηνος** collega, contubernalis, contubernarius.

**\*συσκηνοτρόφος (?)** mangosus.

**συσκιάζω** obumbro, opaco, συσκιασθείς *cf.* infuscatus.

**\*συσκίασις** obumbratio.

**σύσκιος** amoenus, opacus.

**συσπείρω** consero 1.

**\*συσπιλοῖ** commaculat.

**συσπῶ** contraho, -ᾷ pandat.

**συσσείω** concito, concutio.

**συσσίτιον** στρατιωτῶν contubernium.

**σύσσιτος** concibus.

**συσσωρεύω** coacervo, confosso, συνσεσωρευμένον congestum.

**συστάδην** comminus, ἡ - μάχη con gressio.

**συσταθμία** compensatio.

**\*συνστάνει** constituo (-it).

**σύστασις** commendatio, οὐδεμία - nulla comprobatio.

**συστάτης** capreolus.

**συστατικός:** -ή commendativa, -όν minervalicius (-um), -ά
commendaticiae litterae.

**συστέλλω** contraho, corripio, συνεσταλμένον correptivum.

**σύστημα** caterva, collegium, contubernium, cuneum, curia, factio,
globus, sacramentum, - ἀνδρῶν contubernium.

**\*συστοιβάζω** constipo.

**συστολή** correptio, correptus.

**συστρατεύομαι** commilito 2.

**συστρατιώτης** comes, commiles, commilito 1.

**συστρέφω** colligo 1., conglomero, converto, glomero, verso,
συνεστραμμένος II, 445, 38 *s. interpr.*, διάλεκτος *vel* ὁμιλία
συνεστραμμένη tortiloquiqm.

**συστροφἡ** caterva, globus, - ἀνέμου procella, turbo 1., ἐπὶ ὄχλου
caterva.

**\*σύστροφος (?)** vertigo.

**συσφίγγω** artat, coarto, constringo, depresso, συνεσφιγμένος
constrictus, obstrictus.

**συσφραγίζω** consigno.

**συσχηματίζω** *et* -ομαι conformo.

**\*συσχίζω** conscindo.

**σῦφαρ** *cf.* tunica serpentis.

**συφεός** hara, suile, volutabrum.

**συφεών** hara.

**συχνός** spissus.

**συχνῶς** assidue, crebro.

**συψέλιον** *v.* συμψέλιον.

**σφαγεύς** occisor, sicarius.

**σφαγή** caedes, internecio, iugulum, nex, occisio, trucidatio.

**σφαγιάζω** macto.

**σφαγίδιον** iugulatorium.

**σφάγιον** μαγείρου culter.

**σφαγίς** culter *sim.*, occisorium.

**σφάζω** caedo, interimo, iugulo, neco, occido, trucido, σφαγείς
caesus.

**σφαῖρα** globus, pila 1. *et* 2., sphaera.

**σφαιρίον** pila 2.

**σφαιριστήριον** *cf.* sphaeristerium (*expl.* pilatorum lusus, *cf.*
sphaeristae s. *f.*).

**σφαιριστής** pilarius, *cf.* sphaeristae (*expl.* ioculatores qui
onagro interposito omnigenos dabant ludos).

**σφαιροειδής** globosus, pilarius.

**\*σφαιροπαίκτης** pilarius, II, 449, 15 *s. interpr.*

**σφαιροποιός** pilarius.

**σφαίρωμα** lumbus.

**σφάκτης** iugulator, percussor, sicarius.

**σφάλαξ** talpa, vepres (verris). *V.* ἀσφ.

**σφαλερός** titubans.

**σφάλλω** decipio, -ομαι fallo (-or), titubo, trepido, εἰ μὴ -ομαι
nimirum.

**σφάλμα** vacillatio, vitium.

**σφεῖς:** σφίσιν αὐτοῖς sibimet, σφᾶς αὐτούς semet ipsum (-os).
