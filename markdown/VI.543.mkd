(*Non.* 125, 27). sordes uel squalor aut humor IV 94, 49. morbus uel
sordes V 211, 41. morbus, sordes, squalor IV 249, 46. inundatio IV 530,
17. **illuuies secundarum** hama (*AS.*), in quo fit paruulus V 365, 36.
**inluuie** ἀλουσίᾳ II 83, 49. incuria, sordibus *Plac.* V 29, 1 = V 77,
26.

**Illyricos (inl.) sinus** Illyriae regio Adriatici maris IV 447, 8
(*Verg. Aen.* I 243).

**Ilua** Syrorum lingua September mensis dicitur *lib. gl. V.* menses.

**Ilul** *v.* Elul.

**Imaginarius** εἰκονικός II 77, 26. supposita persona V 629, 21.
**imaginarii** εἰκονικοί II 77, 19. *V.* funus imag.

**Imaginatio** εἰκονογραφία II 285, 41.

**Imaginatur (anima tua *cod.*) quod cupit** V 660, 2 (*GR. L.* VII
426, 13).

**Imaginifer** προτόμορφος (*scr.* προτομοφόρος) II 77, 28. προτομοφόροι
III 298, 30 (*de militia*).

**Imaginor** εἰκονίζω II 285, 40. κατεικονίζω II 345, 29. φαντάζομαι II
470, 5.

**Imago** εἰκών, εἴδωλον II 77, 20. εἰκών II 285, 49; III 4, 31; 278,
47; 353, 42; 450, 71; 469, 69; 496, 20. φαντασία II 470, 6 (*cf. Serv.
in Aen.* VIII 557). iconisma, pinace (!) IV 350, 50. similitudo V 109,
12 (= *Non.* 329, 8). uultus IV 525, 48. **imagines** εἰκόνες II 77, 25;
III 10, 1; 83, 55; 170, 48; 196, 28; 238, 60; 267, 56; 301, 60; 362, 15;
408, 56; 517, 51. *V.* iconisma. *De script.* ymago, immago *cf. W.
Heraeus Arch.* XI 63.

**Imaguncula** similitudo modica, quasi imago in gemma isculpta (*vel*
sculpta) *Plac.* V 76, 7 (*cf. Arch.* IX 596).

**Imbacchatus** (inb.) ἀβάκχευτος II 215, 1.

**Imbecillis** (inb.) ἀσθενής II 247, 30; III 14, 9; 86, 80; 329, 70;
509, 34. infirmus, aegrotus, inluctuabilis (inel. *a b c.* inbellis
*Buech.*) IV 357, 24. languidus uel debilis IV 90, 23. languidus V 303,
34. **inbecillum** ἀσθενές II 247, 32. **inbecilles** (*vel* imb.)
debiles IV 246, 29. infirmi, flebiles V 413, 52 (*reg. Bened.* 35,
5?). *V.* becilli.

**Imbecillitas** (inb.) ἀσθένεια II 247, 29. ἀσθένεια, νόσος II 79, 16;
29. **imbecillitatem** ἀσθένειαν II 79, 14.

**Imbecillo (inb.) ingenio** V 662, 34.

**Imbecillor** (inb.) ἀρρωστῶ II 245, 59. ἀσθενῶ II 247, 34. *Cf. GR.
L.* VII 431, 10.

**Imbellis** (inb.) ἀπόλεμος II 238, 29. ἀπόλεμος, ἀσθενής II 79, 13.
**imbelle** inualidum IV 448, 22 (*Verg. Aen.* II 544). debile IV 101,
17. **inbelles** qui pugnare non possunt IV 91, 3; 247, 47; 351, 24;
525, 64; V 301, 64. qui pug­nam ignorabant *Plac.* V 76, 10 (*cf. Verg.
Georg.* III 265).

**Imber** ὄμβρος II 77, 31; 382, 58; III 169, 2; 294, 23; 347, 35; 425,
44; 469, 70. ἐπιομβρία III 244, 62. sophos (ὄμβρος? ζόφος?) II 536, 30.
pluuia II 582, 22; IV 89, 34; 102, 16; 412, 50; 527, 20. solita pluuia V
301, 47. **imbres** ὄμβροι II 77, 32. pluuiae IV 246, 28. V nimietas.

**Imberbis** (*vel* inb.) ἀγένειος II 79, 17; 25; III 181, 8; 252, 44;
329, 60; 487, 40. **inbarbis** ἀγένειος II 79, 24; 216, 17; III 507, 8.
sine barba II 584, 22; IV 413, 5. nondum barbatus IV 92, 34.
**inberbis** sine barba IV 526, 8; V 302, 30. **inberbes** ἀγένειοι II
79, 18. *V.* impubes, ephebus, lēuis.

**Imbibo** (inb.) καταπίνω ὕδωρ II 342, 62.

**Imbitit** *v.* inuideo.

**Imboio** (inb.) κλοιὸν περιτίθημι II 350, 64.

**Imbrex** καλυπτήρ II 77, 33; 34 (impers). **imbrix** καλύπτης (?) II
337, 57. **imbrex** σωλήν II 450, 29. summum tecti cacumen quod subter
cauum, desuper acutum tecto omni superponitur: dictus imbrex quod arceat
imbres II *p.* XIII. tegula incuruata super ripam (riuum *Loewe*) II
582, 25. **imbrices** (*vel* inb.) καλυπτῆρες II 78, 40 (imperges); 76,
57 (iambrices *cod. v.* ambrices, imbrico); 79, 26; III 19, 49; 91,
42/41; 190, 27; 268, 46; 313, 33; 365, 13; 408, 63. πλήμνη (? plimne
*cod.*) **infrices** III 195, 56. **imbrices** qui ponuntur super
tegulas contra imbrem V 301, 44; 635, 17. **ymbrices** tegulae eo quod
accipiant imbres V 255, 25. *Cf. Is.* XIX 10, 15.

**Imbricitur** ὀμβροῦται, βρέχεται II 77, 30.

**Imbrico** (*vel* inb.) καλυπτηρίζω II 337, 58 (καλυπτηριαζω); III 451,
17; 483, 13. **iambricat** (*cf.* imbrex) σκέπει, καλύπτει II 76, 59.

**Imbrietas** *v.* nimietas.

**Imbrutus** (inb.) inprudens, sine sensu, sine intellegentia V 210, 3.
*Cf. Fulg.* 168, 14 *ed. H.*

**Imbulus** ab ambulando, ambulatorium *Scal.* V 601, 46 (*Osb. p.*
294). *Cf. Isid.* XV 2, 26.

**Imbumentis** (inb.) documentis V 210, 4. *Cf. Fulg.* 9, 5 *ed. H.*

**Imbuo** (*vel* inb.) βάπτω II 255, 52. ἀπάρχομαι II 233, 35. ἐμβιβάζω
II 295, 51. μυῶ II 374, 38. **imbuit** (*vel* inb.) ἐνβάπτει, ἐμβιβάζει
εἰς ἐπιστήμην II 79, 27. ενεττυψεν (ἐνέβαψεν *Vulc.* ἐνέστυψεν *Salm.*)
II 79, 31. initiat IV 89, 37; 90, 21; 102, 19; V 301, 46 (*cf. Non.*
324, 20). inseruit (instruit?), informat IV 530, 22.
