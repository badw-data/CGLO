*Epinal. post* V 363, 26 (caluuaer *est* ῾*Milchbrei'*).

**Galmilla** (*vel* gamilla) limmolegn (*vel* liim molegn, *AS.*) V 363,
30. *Cf. Corp. Christ.* (*Hessels*) II 22. *V.* galba.

**Galmum** *v.* galba.

**Galucis** (?) χλωροστρουθίον II 31, 57 (galba *vel* galua *Vulc. v.*
galbus).

**Galzinia** *v.* auruginosus.

**Gamalihel** retributio dei IV 521, 15; 595, 38. *Cf. Onom, sacra p.*
13, 16.

**Gamba** *v.* crus, suffragines.

**Gamenon** (= Γαμηλιώv) tenerum (Athenaeorum?) lingua Iulius mensis
dicitur *lib. gloss. V.* menses.

**Gamma** γάμμα[τα] III 323, 58. tres III 511, 61.

**Gammari** *v.* gabbarus, cammarus (III 437, 27 *legitur* gammari).

**Gammus** πλατύκερως II 409, 2; III 431, 37 (*ubi* camurus *proponitur
apud Labbaeum. at cf.* gamo *Hisp.*).

**Gamus** (*vel* -os) nuptiae IV 240, 55; 521, 18; 587, 2; 595, 35; 603,
15; V 105, 30; 297, 38; 363, 50.

**Ganea** taberna IV 81, 11; 240, 44; 521, 21; V 105, 33; 299, 12
(granea); 458, 32. tabern[ac]ula IV 586, 2. taberna uel popina IV 346,
2; 595, 36.

**Ganearius** ἀκόλαστος ὁ ἐξώλης II 222, 62. ἀσελγής II 247, 23. ἄσωτος
II 249, 30. *Cf.* **ganearum** (ganearium? gannitus?) meretricum risio V
502, 14.

**Ganeo** καπηλοδύτης II 338, 37. ταβερνοδύτης III 336, 1. *Cf.*
ταβερνοδύτης **ganeo, sauinario** (*ubi* saginarius *Boucherie, non
recte: v.* sabinario) III 336, 2. **ganeo** λίχνος, καταφαγᾶς,
λωποδύτης, λιμβός, λαίμαργος II 32, 15 (*Cic. in Cat.* II 4, 7).
λίχνος II 31, 55. λάγνος II 357, 63. ὁ ἐν τοῖς καπηλείοις III 178, 47;
250, 78. πορνοδύτης II 413, 61. fornicator II 581, 16. tabernio IV 81,
10; V 458, 31 (*Ter. Heaut.* 1034). gulosus, popinator IV 521, 19.
gulosus, propinator IV 81, 24; V 204, 22; 363, 48. **graneo** golosus,
propugnator (!) V 206, 1. **ganeo** gulosus, tabernio, propinator IV
240, 43; 586, 1 (glutto *add. c d*). gulosus, popinator, tabernio (hasta
uel iaculum lingua gallica: *v.* gaesum) IV 595, 37. gulosus, popinator,
tabernio IV 346, 4. gulosus, popinator, ponpator (*varia lectio*?) uel
tabernio V 297, 36. tabernarius V 204, 21; 364, 22 (*Epin. post* V 362,
46). tabernium atabernari (tabernio aut tabernarius?) IV 521, 20.
comestor, glutto V 502, 13. gulosus V 363, 41. luxuriosus uel
tabernarius IV 603, 12 (*cf. Schlee schol. Ter. p.* 45). ab eo quod
gan\<e\>um dicitur, proprie receptaculum luxuriosum sub terra IV 81, 31
(*cf. Isid.* X 114); V 458, 35. tabernio, deuorator, consummator (=
consumptor?) V 204, 24. tabernareo, teneor (tabernarius, leno?) V 204,
23. **ganeon** siue **ganeo** glutto aut comestor V 105, 32 (comesor);
204, 25 (comesor); 458, 33; IV 81, 12. **ganeo** guleo (glutto? gulo?),
gurgis (= gurges), charybdis, caenus uel continens (caenosus,
incontinens? *Schoell*) V 458, 30. **ganeones** a ganeis V 642, 20
(*Non.* 119, 6). propinarii, ubi (qui *W. Heraeus 'Spr. des Petr.'* 29)
et agaguli V 297, 47. *Cf. Landgraf. Arch.* IX 379. *V.* gulosus,
ganimen.

**Ganeo** καπηλεύω II 338, 36. **ganeor** propinor (*cf.* ganeo
*subst.*) V 458, 29.

**Ganeosus** tabernarius, gulosus, propinator V 502, 12 (*v.* ganeus).

**Ganeum** πορνεῖον II 413, 57. ἀσωτεία II 249, 31. popinam latebrosam V
534, 42 (*Ter. Ad.* 359). **gania** fornices subterranea, graecum est
IV 603, 13. loca subterranea ubi luxuriae fiunt V 502, 16. **ganzia**
(*cf.* ganza *Ital.*) occulta loca et subterranea V 614, 29. **gangia**
(!) occulta loca et subterranea: gangagia (ἀνάγαια *Buech.*) Graeci
uocant *lib. gloss. Cf. Isid.* X 114; *Festus Pauli p.* 96, 9; *Donat.
in Ad.* III 3, 5.

**Ganeus** luxuriosus IV 81, 14; 240, 47; 521, 22; 586, 4; V 204, 26;
458, 34. **ganei** lenones V 204, 20.

**Gangalize\<i\>s** titillas V 204, 27.

**Gangaridum** Virgilius (*Georg.* III 27): solidoque elefanto
Gangaridum faciam uictorisque arma Quirini V 204, 28.

**Gangis** fluuius Indiae IV 412, 5; 595, 34; 603, 10. **Gantes** nomen
fluuii IV 81, 27 (*Verg. Georg.* II 137; *Aen.* IX 31).

**Gangraena** cancer V 642, 8 (*Non.* 117, 17). **cancrena** carnis (!)
putrida uel cutis III 599, 7. serpitionis (= ἕρπης?) uulnera,
**cancrena** III 606, 5. *V.* steatoma, cancer.

**Ganimen** (?) tabernarius IV 241, 2; 587, 4; V 458, 36; 502, 17.
Ganymedes *H.*

**Ganit** λαγνεύει II 32, 16 (*cf. Iuvenal.* VI 64). **cannit** σκυζᾷ
II 434, 11. **gan­nit** σκυζᾷ (σκύζει *e*) II 32, 24.

**Gannat** χλευάζει II 32, 22 (sann.?).

**Gannator** χλευαστής II 32, 23 (sann.?).

**Gannio** ορυωμαι (ὠρύομαι?) II 387, 33. **gannit** muttit IV 81, 13
(*Ter. Ad.* 556); 240, 45; 521, 44; 586, 6; V 204, 31. latrat uel
inridet IV 346, 5; 595, 40; 603, 14 (inridit). **ganniunt** cantant V
204, 32. **gannire** auium murmur, animalium IV 603, 11. uulpium est
sicut latrare canum V 204, 30. *V.* baubant, muttio, uulpes.
