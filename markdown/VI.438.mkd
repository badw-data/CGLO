IV 237, 34. summitas, nobilitas generis IV 516, 41. pinnaculum templi IV
341, 31. maiestate (? maiestas, templi *Buech.*), pi\<nn\>aculum IV 516,
40. **fastigia** excelsae sedes IV 73, 14; 74, 42 (excelsa). summae
partes aedificiorum IV 73, 25 (*cf. Serv. in Verg. Aen.* I 342). capita,
culmina, summa montium IV 442, 1 (*Verg. Aen.* II 302?). excelsa sedis
aut pars summa par\<ie\>tis IV 516, 42. nunc operis summitates, alibi
ima significant, ut 'forsitan et scrobibus quae sint fastigia quaeras'
(*Verg. Georg.* II 288). V 568, 52 (*cf. Serv. ad Aen.* I 58; II 758).
summa parietis aedificatio, alibi summae partes aedificiorum V 198, 36.
de imis scrobium dicta, ait Lucanus (IV 296): ad inrigui premit\<ur\>
fastigia campi V 199, 1. uerba summatim exposita IV 411, 26 (*Verg.*
*Aen.* I 342).

**Fastilis** *v.* fastidilis.

**Fastiscitur** *v.* fatescit.

**Fastuosus** fastidiosus, superbus II 579, 21. **fastu\<osu\>s**
superbus, elatus IV 74, 40 (fastus superbia, elatio *Nett­leship*
*'Journ. of Phil.'* XIX 122. *Cf. Osb.* 245, *ubi* fastus superbia,
elatio *se excipiunt*).

**Fastus** (?) deceptor, suasor V 500, 22. fascinator? *V.* fascinum.

**Fastus** felix, faustus IV 341, 35 (*cf. Festus Pauli p.* 87, 19 *et
v.* faustus). *Huc rettulit* **faustus** festus IV 236, 34 Warren. *Cf.*
**fastu\<s\>** iustus II 579, 17. *V.* faustus.

**Fastus** ὑπερηφανία II 464, 17; 488, 29. ἐξουθενία II 511, 18.
superbia: et est quartae declinationis *Plac.* V 23, 11 (quarta
declinatio) = V 68, 24. fastidium, superbia IV 442, 2 (*cf. Serv. in
Aen.* III 326); 73, 22 (superbiam *vel* superbia). inflatio uel timor
(tumor?) V 424, 50 (*Cassian. inst.* V 10). superbia V 425, 3
(*Cassian. inst.* V 10). contemptus V 293, 27; 361, 54. superbia,
contemptator aut contemptor (*ex* superbia, contemptus *enata*, *cf.
bc*) IV 516, 33. superbus, contemptor IV 236, 37 (*eodem modo
progenita: cf. tamen* fastuosus). ἐπισκύνιον, ἡμερολόγιον (*contam.*) II
70, 46. superbia uel liber (*item*) V 362, 16. **fastu** superbia II 70,
44 (*Horat. ep.* II 2, 93). superbia uerborum V 294, 17.

**Fastus** et **fasti** libri ubi sunt nomina consulum V 293, 56; 568,
53; IV 237, 27 (libri sunt). libri ubi nomina consulum scribuntur IV
341, 34. **fastus fasti** libri V 552, 7. **fastes** libri V 360, 5.
**fastus** in quibus sunt nomina consulum V 360, 27. libri consolator
(*h. e.* consulatus?) IV 74, 45. consulatus (*vel* libri cons.) IV 73,
50. **fasti** libri quos con­sules habebant V 198, 35. **fastorum libri**
sunt in quibus reges et consules scribuntur V 568, 54. **fastibus**
libris (*cf. Euseb. eccl. hist.* I 5 *ubi* fascibus *editur*) V 419, 47;
428, 31. **fastis** libellis (*vel* -us) ubi nomina consulum
inscribuntur. Lucanus (V 5): noua nomina fastis V 199, 3. *An huc
pertinet* **fastum** deuote uel quod magistratus legunt (leguntur *b*)
IV 411, 28 (*glossa truncata?*): *quae talis est in lib. gloss.*:
**fatus** deuotae laudes Caesarum quod magistratus legunt? *nisi* fatus
*praestabit. Cf. Isid.* VI 8, 8.

**Fastus dies** ἐπιτηδεία ἡμέρα II 311, 53. *Cf.* ἐπιτηδεία ἡμέρα
nefastus dies, **fastus dies** III 243, 72. **fasti dies** in quibus ius
fatur (!) V 568, 56. *Cf. Is.* VI 18, 1.

**Fata** (*partic.*) *v.* fatum.

**Fatalis** μοιρίδιος II 372, 39. ex fato contingens II 579, 45.
mortifera V 552, 6 (*cf. Serv. in Aen.* II 237). **fatale** μόρσιμον II
70, 57. εἱμαρμένον II 286, 5. locus unguenti II 579, 48 (*male versa*).
paludio (Palladium? Palladis *Loewe GL. N.* 154) *Leid.* 67 E. *Cf.
Verg. Aen.* II 165.

**Fatalis dies** ἐμπρόθεσμος ἡμέρα II 296, 50.

**Fatali sorte** ἰδίῳ θανάτῳ III 448, 23; 481, 16.

**Fataliter** μοιριδίως II 372, 40.

**Fateor** ὁμολογῶ II 383, 28; III 406, 13; 448, 24; 501, 26. confiteor
IV 237, 39. **fateris** ὁμολογεῖς III 406, 14. **fatetur** ὁμολογεῖ II
70, 58; III 406, 15. confitetur IV 341, 39. **fatemur** ὁμολογοῦμεν III
406, 16. **fatere** ὁμολόγησον III 406, 12. **fatemini** ὁμολογήσατε III
406, 19. **fateri** confiteri IV 74, 15. **fassus est** ὡμολόγησεν III
406, 17. **fassi sumus** ὡμολογήσαμεν III 406, 20. **fassi sunt**
ὡμολόγησαν III 406, 18. *V.* confiteor.

**Fatescente** soluente V 199, 5. *V.* fatescit.

**Fatescit** (*vel* fatiscit) κοποῖ II 71, 1. descit (*vel* dehiscit),
aperitur uel aperit IV 341, 37. aperitur IV 236, 28. resoluitur V 293,
14; IV 516, 16. dissoluitur, euanescit IV 72, 42; 74, 16; 47; V 199, 6
(*Verg. Georg.* II 249?). frangit, dissoluit V 455, 61; 500, 26.
soluit, contriuit (*de libro rot. = Isid, de nat. rer.* VII 3) V 415,
53; 425, 18. dissoluitur, diuiditur V 199, 12. **fatescunt** (*vel*
fatiscunt) soluuntur IV 442, 3 (*Verg. Aen.* I 123; IX 809). feriendo
dissipant IV 72, 36. feriendo dissipantur IV 237, 19. feriendo dissipant
uel dissoluunt (*vel* dissoluuntur) IV 341, 38. feriendo dissipant,
deficiunt, dissoluuntur, dissipantur IV 516, 15. feriendo dissipantur ac
diuiduntur V 199, 7. feriendo dissipantur ac dissoluuntur uel
