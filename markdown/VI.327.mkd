**Depudo** ἀπαναισχυντῶ II 232, 53. Depudoro *Quicherat.*

**Depugis** κατάπυγος, καταπύγων II 44, 3. **depygis** denaticata, sine
natibus, nam pyga extrema pars corporis *Pap.*

**Depugno** μονομαχῶ II 373, 6.

**Depulpo** ἀποσαρκῶ II 240, 30.

**Depulsio** (def.) correctio II 576, 34. *V.* defautio.

**Depulsitasse** ἀπωθηκέναι II 44, 9.

**Depulso** ἀπωθῶ II 243, 34.

**Depulsor** ἀλεξίκακος (*vel* ἀν.) II 224, 47; III 289, 63; 445, 43;
480, 10; 507, 58.

**Depulsus** delictus IV 329, 27. *V.* delicum.

**Depultus** pro depulsus dicitur *Pap.* (*auct. ad Her.* IV l0, 15).

**Depungo** καταστίζω II 344, 7 (*Pers.* VI 79).

**Deputat** despoliat, detrahit, sed proprie 'ad purum redigit', et ex
puro clarum accipiemus, ex claro manifestum *Plac.* V 16, 9 (manif. ex
cl.) = 61, 19 (deportat *codd. corr. Kettner*). abdicet IV 50, 44.
**deputor** ἀφορίζομαι III 445, 44; 479, 19. **deputatur** ἀμφιβάλλεται
II 44, 11 (disp.?). ἀναφέρεται, κατατέτακται II 44, 6. **deputantur**
ἀναφέρονται, ἀπονέμονται II 44, 28. *Consulto non distinxi. Cf.*
colluco, deporto.

**Deputatio** ἀπονέμησις, ἀποκλήρωσις II 44, 10.

**Deputatus** ἀπονεμηθείς II 239, 16. ἀπονενεμημένος II 239, 21.
κατα[τε]ταχθείς, ἀπονεμηθείς II 44, 7. *V.* defectio.

**De quarte** εἰς τετάρτην III 296, 15; 517, 12. *Cf. GR. L.* I *p.* 81,
30, *Loewe Prodr.* 206.

**De quarto de quarta** περὶ τετάρτης II 405, 15.

**Dequerere** querelas ferre *gloss. Sal. = Mai VI* 520.

**De quinte** εἰς πέμπτην III 296, 16; 517, 13.

**De quocumque** περὶ οὑτινος δήποτε II 403, 46.

**Derado** καταξύω II 342, 46. παραξύω II 395, 48. περιξύω II 403, 31.

**Derarat** (deriget *H.*) torpet, frigidum est *Scal.* V 596, 66.

**Derbitas** impetigines III 599, 32. similis ersipelatas III 599, 39
(erysipelati?). erpitas (herpetas?) III 600, 30. zernas III 607, 6.
iomias (?) III 601, 44. *Cf. Is.* IV 8, 5; 6; *Cass. Fel. ind.* 205.
*V.* impetigo.

**Derectarius** θυρεπανοίκτης, ἐπεισπηδητής, κατάρατος II 44, 53. ὁ εἰς
τὰς ἀλλοτρίας ἕνεκεν τὸν κλέψαι εἰσερχόμενος οἰκίας II 44, 35. certus
(dictus *Nettleship 'Contr.'* 431) latro in aliena domo furans II 576,
24. **derectarii** (def. *cod.*) εἰσπηδησιῶνες Lib. de officio
proconsulis II 40, 46. *Cf. Rudorff 'Abh. d. Berl. Ac.'* 1865 *p.*
[279. *V.* directarius.

**Derectum** apertum (= detectum *Nett­leship* '*Journ. of Phil.'* XIX
119) uel rectius ordinatum IV 51, 39. rectius ordinatum V 405, 13.
rectius ordinatum uel rectius missum (*cf. mettre*) IV 503, 21. *Cf.*
decretum.

**Derelictus** καταλελειμμένος II 44, 37. dimissus IV 227, 33; 505, 4.
solus, dimissus IV 48, 39. **derelicta** καταλελειμμένη II 44, 36. *Cf.
Plaut. Truc.* 867.

**Derelinquo** ἐγκαταλιμπάνω II 283, 46. **dereliquerit** \<in\> futuro
sine n scribimus *Plac.* V 17, 7 = V 61, 21 (in *add. Mai*). *Cf.
Roensch Coll. phil. p.* 228.

**Derepente** subito *Plac.* V 16, 21 = V 61, 22. *Cf. Non.* 517, 10.

**Dereptum** *v.* direptus.

**Derideo** καταγελῶ II 340, 16; III 76, 33; 150, 11. χλευάζω II 477, 21;
III 81, 7; 163, 63. **derides** καταγελᾷς III 150, 12. χλευάζεις III
163, 64. **deridet** καταγελᾷ III 150, 13. χλευάζει III 163, 65.
**deride** καταγέλασον III 150, 14. χλεύασον III 163, 66. **derisi**
κατεγέλασα III 150, 15. **derisus es** κατεγελάσθης III 150, 17.

**Derideo adseculam** καταγελῶ τοῦ παρασίτου, κόλακος II 44, 38.

**Deridiculus** καταγέλαστος II 340, 14.

**Derigenda** ἐξαποσταλτέα II 44, 50.

**Derigeo** ἀποψύχω II 242, 52. **diriguo** ἀποπηγνύω II 239, 45.
**diriguit** (*vel* deriguit) ἀπεπάγη II 44, 40. obstipuit IV 51, 32
(*Verg. Aen.* III 308); 56, 9; 226, 25; 437, 51; 505, 2; 506, 14; V
189, 18; 628, 5. hebuit, obstipuit V 286, 57. stupefactus est V 189, 19.
rigidus et frigidus est IV 230, 1 (*Verg. Aen.* III 260). **derigeor**
ἀποπήσσομαι II 239, 47. *Cf.* **diriguere** pro stupore pallescere
(*Euseb. eccl. hist.* III 6) V 418, 24 = 426, 66. pallescere V 356, 29;
408, 3.

**Derigo** ἀπευθύνω II 234, 48. κατιθύνω II 346, 13. **derigit** rectum
facit IV 329, 29. **derigire** ἐξαποστεῖλαι (*ubi* derigere *e*) II 44,
48. *V.* dirigo.

**Deriguere (dir. *cod.*) oculi[s]** stupuere obtuto (!) V 287,
23. **deriguerunt oculi[s]** stupuerunt V 496, 72. *Cf. Verg. Aen.*
VII 447.

**Deripio** ἀφαιροῦμαι II 252, 21; III 445, 45; 479, 58. **deripit**
ὑφαρπάζει II 44, 51 (deriper *cod.*). **deripere** ἀφαρπάσαι II 44, 52.
*V.* diripit.

**Derisor** ἐμπαίκτης III 373, 2. καταγελαστής III 150, 16. *V.*
derisus.

**Derisoria** *v.* tabula derisoria.

**Derisus** καταγελασθείς (καταγελεστης *cod. unde* derisor *c*; *corr.
e.* καταγέλαστος?)
