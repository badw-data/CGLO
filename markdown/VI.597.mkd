**Interruscus** scorcia (*cf. Diez* I scorza) mediana III 591, 41. *V.*
cortex mediana.

**Intersaepio** περιφράσσω II 406, 1.

**Intersaeptum** διάφραγμα II 275, 45; III 262, 17.

**Interscapulum** μετάφρενον II 89, 24 (intercapulum); 90, 34; 369, 52;
III 247, 73; 311, 50; 530, 18. **interscapilium** μετάφρενον III 500,
21. **inter scapulas** μετάφρενον III 86, 39; 175, 70.

**Inter se** ἐν ἀλλήλοις II 297, 24. inuicem V 536, 22 (*Ter. Ad.*
828; *GR. L.* III 178, 6).

**Intersecta** (intersepta *H.*) interclusa IV 251, 30; 252, 4
(interrecta).

**Inter se disgladiantur** V 662, 33.

**Interspondium** *v.* interpondium.

**Interstat** interest *Plac.* V 29, 15 = V 78, 29. **interstitit**
διέστη II 90, 37.

**Interstinximus** interposuimus IV 99, 17; V 213, 9.

**Interstitium** (!) interstantiam V 213, 10.

**Interstitio** διάφορον II 90, 35. **interstatio** interpositio,
dubitatio, intermissio V 571, 16.

**Interstitium** διάστημα, διάστασις II 90, 33. παράλληλον III 452, 70;
482, 14. spatium uel interuallum IV 527, 6.

**Intersum** πάρειμι II 90, 38. παρατυγχάνω II 397, 4. interuenio IV
356, 36. **interest** διαφέρει II 89, 50; 275, 30. distat, differt IV
356, 45. **interesse** παρεῖναι II 397, 35. accumbere IV 448, 11 (*v.*
accumbit *et Verg. Aen.* I 79). **inter\<ero\>** παρέσομαι II 398, 19
(*suppl. e*). **interfui** παρήμην II 89, 52. **interfuit** παρῆν II 89,
51. παρέτυχεν II 90, 32 (interruit *cod. corr. e*); 562, 34. medius fuit
IV 526, 44. *V.* refero, nihil interest.

**Interterminat** *v.* interminat.

**Intertortuosus** *v.* amfractum.

**Intertrigo** παράψησις II 561, 40. **intertrigines** μεσομήρια III
311, 63; 530, 21. ὑπομήρια III 176, 24; 249, 4.

**Intertrimentum** ἀφουσία II 504, 4. ἀφουσία χαλκοῦ II 253, 51.
**intetrimentum** si aliqua species in medio teratur IV 252, 5; V 523,
5; 571, 11 (interimentum). **intertrimento** damno IV 98, 52 (*Ter.*
*Heaut* 448); V 461, 35. **intertrimenta** ἀπουσίαι II 90, 36.

**Interturbat** molestus est V 535, 42 (*Ter. Andr.* 663).

**Interula** tunica interior IV 98, 49; V 461, 33. est camisia V 620,
21.

**Inter utrumque** ἐπ' ἀμφότερον II 305, 32.

**Interuallum** διάστημα τόπου II 90, 42. διάστημα II 274, 28 (*GR. L.*
III 43, 3). spatium IV 356, 38. medium inter fossam et murum *Scal.* V
602, 31. **interballum** (*contam.*?) προσχήματι, προσποιήσει II 89, 20.
*V.* longo interuallo, ex interuallo.

**Interuenio** διαιτῶ II 271, 30. μεσιτεύω II 368, 11. παρατυγχάνω II
397, 4. **interuenit** βοηθεῖ, συνέρχεται, μεσάζει II 89, 13. ἐμφέρεται
II 296, 61. superuenit IV 98, 51. **interueniebat** παρεγένετο III 103,
50. **interueniebant** παρεγίνοντο III 50, 44.

**Interuentio** μεσητία (= μεσιτεία) II 89, 14.

**Interuentor** διαιτητής II 271, 28. ἐπιγνώμων II 307, 36.

**Interuentus** παρένθεσις II 398, 6. παρουσία, ἄφιξις II 90, 41.
παρέντευξις II 488, 53. ἡ παράταξις II 511, 48 (eparazaxis *cod. corr. a
b e.* ἡ παράτασις *b in marg.*). **interuentum** (nom.) παρεντυχία II
398, 11. **interuentu** τῇ μεσιτεία III 452, 71; 482, 16. þingungae
(*AS.*) V 366, 44 (*cf. Oros.* III 23, 66). rogatione, intercessione V
553, 27.

**Interuulsa** *v.* interpolatus.

**Intestabilis** ἀμαρτύρητος III 452, 72; 475, 61 (ἀμάρτητος). ἀδιάθετoς
II 218, 34. ἀπηγορευμένος, ἄτιμος, μὴ προσδεκτέος εἰς μαρτυρίαν II 90,
43. sine testimonio II 584, 19. sine fraude (fide *Hildebrand cum d e*),
sine testimonio IV 357, 11. sine fide testium V 301, 61; 367, 57. sine
fide et testimonio V 635, 14. sine fide testium, sine fide et
testimonium (!) IV 525, 62. *Cf. Plaut. Curc.* 30. **intestabile** sine
fide (*vel* fede) testium IV 251, 33. *V.* intemperans, inaestimabilis.

**Intestatus** ἀδιάθετος II 218, 34; III 129, 10; 452, 73; 475, 60; 482,
18. mo­riturus (*vel* mortuus) qui testamentum non facit II 583, 17.
**intestati** ἀδιάθετοι III 129, 11. *V.* ex intestato.

**Intestinale** edron (ἑδρικόν *Buech.*) III 207, 61.

**Intestinarius** τορνευτής III 307, 49 (ἐντορνευτής *Hagenprogr. Bern.*
1877 *p.* 14).

**Intestinum** thearm (*AS.*) V 365, 41. **intestina** ἔγκατα, ἐμφύλια
(*v.* intestinus) II 90, 39. ἔγκατα (singularia non habet) II 283, 43
(*GR. L. I* 550, 6). ἔντερα (singularia non habet) II 300, 27. ἔντερα
III 13, 1; 86, 11 (intestine); 176, 51 (sentine *vel* stentine); 248,
47; 311, 31; 350, 5 (stentinae *forma vulgari*); 395, 10 (stenta); 518,
20; 562, 11. interania IV 357, 9. *Cf.* **stentinis intestinis** minoris
et maioris (?) III 605, 38. *V.* ileus, insetie, *W. Heraeus 'Spr. des
Petr.'* 41. Intestina, intestinae, stentinae, sentinae *bonae formae, ex
parte vulgares.*

**Intestinus** ἐμφύλιος II 297, 3. conprouincialis II 583, 15.
**intestinum**
