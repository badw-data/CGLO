sollemnis dies festa dicitur quae magno anniuersario celebratur, ex quo
et **lemniscae** dicuntur coronae quae de fasciis fiebant V 621, 5.

**Lemosinator** *v.* adulator.

**Lemunculus** *v.* lembunculus.

**Lemures** νυκτερινοὶ δαίμονες II 120, 43 (*cf. Hor. epi.* II 2, 209;
*GR. L.* I 32, 23; 548, 30) daemones IV 107, 6. laruae nocturnae V 656,
14 (*Non.* 135, 14). laruae V 643, 68 (*Non.* 135, 14). umbrae
sugillatorum V 216, 18. **lemorum** laruarum *Plac.* V 29, 37 = V 80, 16
= V 112, 34.

**Lemuria[m]** dies festus laetitiae (Larentiae *Baehrens*) V 370, 25.
**lamuriliae** ἑορταὶ τῆς αστιας (*ubi* Ἀττίας *cd.* Ἑστίας *Salmas.*
῾Εκάτης *et antea* Hecatae *Nettleship Arch.* VI 150. ἀγχιστείας
*Buech.*) II 120, 44. *Cf. Loewe Prodr.* 193. **limulla** (=
lemulia, lemuria) larua *Scal.* V 603, 53. **limuriae** laruae
[lingurrit, *v.* ligurrio] V 370, 40.

**Lena** nutritor meretricum (*ex graeco male versa*) II 586, 10. **lena
uitiorum** seductrix IV 254, 21; 533, 25; V 505, 55 (lena uitiorum
seductrix uitiorum).

**Lenaeus** Liber pater ab eo (leno?) in quo uuae premuntur, quia poetae
dicunt quod ipse inuenisset uinum V 112, 15; 216, 21. *Cf. Serv. in
Georg.* II 4.

**Lendina** (=lens) hnitu (*AS.*) V 369, 22.

**Lendis, lendix** *v.* lens 1.

**Leneos** pro iniciorum (= Perinthiorum) lingua Martius mensis dicitur
V 216, 20. *V.* menses.

**Leni agmine** placido cursu IV 449, 40 (*Verg. Aen.* II 782).

**Lenifico** ἁπαλύνω III 127, 30. **lenificat** blande loquitur V 553,
57.

**Lenificus** dulcis, lenis *cod. Bern.* A 91 (*Loewe Prodr.* 161).

**Lenimen** προσηγορία II 122, 12 (παρηγορία *c e*).

**Lenimentum** πραότης II 415, 10. species quae lenitur lima IV 106, 16;
533, 15; V 307, 13; 370, 32 (lermentum …); 505, 56 (cum lima): species
(= *Spezerei*) quae lenit ut lima \<ita V 307, 13; 370, 32\> *Landgraf.
Arch.* IX 389: *cf. W. Heraeus Arch.* X 512. *Explicatur* lenimentum
(*fuerat* linimentum).

**Lenio** καταπραύνω II 343, 13. **leno** (lenio *a*) πραύνω II 415, 17.
**lenit** mitigat IV 414, 51. placat, blandit IV 254, 28. mitigauit IV
107, 32. **linire** mulcere, mitigare, πραύνειν II 123, 40. **lenire**
placare IV 107, 11 (*Verg. Aen.* IV 393).

**Lenis** πρᾶος II 122, 13; 415, 9. προσηνής ἀρρενικῶς II 421, 35.
tranquillus IV 414, 50. limpidus uel quod graue non est, mansuetus IV
360, 16 (*v.* lēuis). placidus V 112, 16. **lenus** mitis, benignus V
307, 23. **lene** ἁπαλόν III 127, 31. **lenius** πραότερον II 122, 11.
**lenissimus** amoenissimus IV 533, 41. *V.* leuis, linens.

**Leni sanguine** *v.* leui s.

**Lenis crepitans** lenis sonans IV 449, 41 (*Verg. Aen.* III 70).

**Lenitas** πραότης II 122, 10; 415, 10. patientia IV 107, 8.
indulgentia V 536, 46 (*Ter. Andr.* 175). *V.* lenitas.

**Lenito** πραΰνω II 415, 17.

**Lenitudinem** pro lenitatem V 643, 39 (*Non.* 132, 2).

**Lenitus** (lin. *codd.*) sedatus, placatus IV 361, 17.

**Leno** λήνω III 367, 2. πορνοβοσκός II 122, 14; 413, 58; III 252, 8;
309, 10. μαστροπός II 365, 16; III 271, 40. μαυλιστής III 179, 61; 201,
37. meretricum nutritor II 586, 19. qui puellas comparat in prostibulo V
370, 26; 307, 21 (puellam . . inpraesto). *Cf.* V 307, 28. seductor et
praepositus meretricum IV 254, 26. consciarius meretricis (*Euseb. eccl.
hist.* VI 5) V 430, 46. lecator, mediator *Scal.* V 602, 66 (*Osb.*
324). **lenones** conciliatores IV 106, 42 (*Isid.* X 63; 160).
conciliatores meretricum V 368, 12. nani, fornicatores V 112, 20.
domestici, adsecula\<e\> IV 360, 19. unde lenocinia, id est carnalia
blandimenta V 430, 47 (*Euseb, eccl. hist.* VI 5). uenenosi (?) uel
suasores V 368, 8. **lenonibus** conciliatoribus mulierum (*Euseb. eccl.
hist.* VI 5) V 421, 63; 430, 50.

**Lenocinantem** suadentem IV 107, 40. **lenocinante** suadente V 216,
22. **lenocinantes** conciliantes IV 533, 33; V 216, 23; 306, 4.
conciliantes uel seducentes IV 360, 17. **lenocinantibus** male
blandientibus uel adsentientibus IV 107, 25; V 216, 24. *V.* lucinantes.

**Lenocinat** conciliat IV 533, 34; V 306, 58. **lenocinatur** turpi
adulatione famulatur IV 106, 18; V 112, 17; 463, 10; 523, 34
(ambulatione); 603, 6. **lenoci­nari** est cum turpitudine seruire V 659,
21 (*schol. Gronov. in Div. in Caecil.* 48).

**Lenocinator** πορνοβοσκός II 413, 58. πορνοκόπος II 413, 62. προαγωγός
ὁ πορνοβοσκός II 416, 5. καλλωπιστής II 337, 38.

**Lenocinium** πορνοβοσκία, καλλωπισμός II 122, 15. πορνεία II 531, 2.
πορνοβοσκεῖον II 413, 59. καλλωπισμός II 337, 37. nutrimentum meretricum
(*male ex graeco versa*) II 586, 12. uxoris meretricatio mariti consensu
IV 106, 19;
