**Cortina** Δελφικὸς τρίπους Ἀπόλλωνος II 116, 39. tripes (*vel* tripus)
Apollinis II 573, 39. τρίπους II 459, 39; 517, 52. ἔμβασις III 353, 76;
494, 24. **curtina** (*cf. GR. L.* V 575, 7) responsum IV 224, 27. locus
unde oraculum dabatur, dicta a corio Pythonis serpentis uel quasi
certina, quod certa inde responsa dabantur, uel quia cor uatis ibi
tenebatur V 550, 29. *Cf. Serv. in Aen.* III 92. *V.* aulaeum, contila.

**Corus** εὖρος III 295, 22; 517, 4. ἐργάτης (*scr.* ἀργέστης) III 172,
14/13; 245, 41. κῶρος III 84, 60. *Cf. Anthol. ep.* 1177 (chorus). *V.*
clores.

**Corus** modii X (*vel* XXX; = *Eucher. instr. p.* 159, 2) IV 436, 5
(*cf.* κόρος). XXX modios habet V 351, 51 (chorus). *Cf. Isid.* XVI 26,
17.

**Coruscans** *v.* Iouis coruscans.

**Coruscatio** ἀστραπή III 169, 14; 245, 5. fulgor IV 436, 2. splendor
IV 224, 23. fulgor uel splendor IV 500, 17. uibrat, hiems (uibrat *est*
coruscat: *recentius est quod in d e exstat* uibrans: *unde corrie quae
Loewe dixit Prodr. p.* 47) IV 325, 45; V 594, 59. **coruscationis**
fulgoris V 448, 19. **scoruscatio** ἀστραπή III 347, 16; 393, 19; 418,
11. *Cf. Loewe Prodr.* 356; *GR. L.* IV 198, 32.

**Corusco** κραδαίνω II 354, 38. **coruscat** ἀστράπτει III 169, 15.
**coriscabat** (caristabat *codd.*) cassidatus aut galeatus fulgebat V
175, 21 (*cf.* cristatus; contam.?).

**Coruscum** ἀστραπή III 9, 63; 425, 61.

**Coruscus** ἀστραποειδής II 248, 49. splendidus IV 38, 24. **coruscum**
crispum IV 224, 21. **corusco** micanti aut relucenti IV 500, 18; 47,
16. **coruscae** fulgentes IV 436, 3 (*Verg. Aen.* II 172 *et Serv.*).
fulminis coruscantis IV 46, 8 (*Verg. Aen.* II 172). **coriscae**
(*GR. L.* IV 198, 32) crispae aut resplendentes IV 37, 31. **coruscis**
fulgentibus IV 436, 4 (*Verg. Aen.* I 164?); 44, 32. *V.* curustus.

**Coruinus** κοράκινος II 116, 48.

**Coruus** κόραξ II 116, 47; 353, 37; III 19, 67; 148, 11; 188, 31; 258,
17; 319, 88; 360, 19; 48; 397, 36; 403, 67; 404, 2; 435, 47; 468, 34;
497, 81. **caluus** (*h. e.* coluus) κόραξ III 621, 16. κόραξ **caluus**
uel **coruus** III 556, 41. **coruus** κορακίσκος (*ferramentum*) III
369, 4. **coruos** feminini generis dicit esse Donatus V 185, 25. *V.*
nocturnus coruus.

**Coruus** κορακῖνος (*piscis*) III 318, 24; 355, 68; 436, 42; 526, 55.

**Coruus agrestis** ebius (*cf.* αἴθυια) III 562, 72.

**Coruus maritimus** ebius (*cf.* αἴθυια) III 589, 69; 611, 30; 623, 33.
mercoris (?) pullos III 603, 3. *V.* mergus, mergulus.

**Corybanta** lunaticus V 654, 1 (*Iuvenal.* V 25). **Cory[m]bantes**
Κουρῆτες οἵ περὶ τὴν Ῥέαν II 354, 23. **Corybantes** ludentes cum
generibus organum (!) V 494, 48. id est qui fecerunt sonitum V 423, 4
(*Clem. Rom. rec.* X 18).

**Corylus** λεπτοκάρυον, id est nucleus V 449, 1 (*v.* contus). arbor
abellanus (!) V 278, 52. haesl (*AS.*) V 353, 15. **corulus** ποντικέον
II 518, 1. ποντικέα II 116, 53 (*v.* coronea). **corylum** λεπτοκάρυον
II 359, 38. **corylos** auellanas IV 224, 14. auellanas uel nuces IV 44,
46; 500, 3 (*cf. Serv. Georg.* II 65).

**Corymbata** nauis IV 45, 8; 224, 22; 497, 44; V 185, 30; 593, 39
(corymbo ornata *suppl. De-Vit*).

**Corymbus** ἕλιξ ἀμπέλου II 295, 15. ἕλιξ III 265, 3. ἀκρεμών II 223,
43. ἀκρεμών, καρφίον III 263, 48. ὅρμος **culimbos** (*ubi* corymbos
*David*) III 434, 12. **corymbus** cacumen IV 38, 2; V 278, 35. est quae
in uite nascitur V 617, 36. leactrocas (*AS.*) V 353, 27. **corymbi**
bacae hederae IV 496, 31 (*Verg. Ecl.* III 39). hederarum bacae IV 45,
14. bacae in hedera V 278, 36. hederae poma V 495, 45. **corymbis**
nauibus V 353, 60 (*cf.* corymbata). *V.* corineos.

**Coryti** (corici *cod.*) iacula breuia in modum sagittarum quae
faretris aut inserta scuto gestantur uel gladius V 639, 2 (*Non.* 556,
21). *V.* goruthus.

**Corythus** *v.* coritus.

**Cos** ἀκόνη II 223, 10; 507, 7 (cos cotis); 537, 56; 550, 11 (cos
cotis); III 270, 19; 321, 59. ἀκόνη, ὑποδερμίς II 117, 19. **cotis**
ἀκόνη III 29, 58; 92, 43. **cutis** ἀκόνη III 198, 8; 321, 60; 506, 25.
**cotis** ὀργή, ἀκόνη, ὑποδερμίς II 102, 42 (*ubi* \*\*\*κότος ὀργή,
**cos** ἀ κόνη *et* **cutis** ἐπιδερμίς *Nettleship Arch.* VI 150. *v.*
cotei). acuendi petra II 574, 18. **cox** huetistan (*AS.*) V 354, 37.
**cotis** dignitas corporis. Virgilius (*Ecl.* VIII 43): 'nunc scio quid
sit amor: nudis in cotibus' (*ubi aut* nudis *aut* duris *libri: non
recte distinxit et* cutibus *intellexit glossator*) V 186, 5. *V.*
cautes, cote Cretica, cotei.

**Cosanas** κέφαλος III 318, 45; 526, 60 (*piscis*!). Cosanus (*a* Cosa)
*H.*

**Cosmus** calcarium feminis Romanis V 566, 21. *Cosmus Martialis et
Iuvenalis tangi videtur Buechelero* (caldarium).

**Cossam** diuinam (*vel* diuinans) IV 497, 43; 224, 19 (corsam); V 278,
20: *ubi* cosam *vocem Hebraicam latere putant cum De-Vitio Warren p.*
151, 588 *et Roensch 'Coll. phil.' p.* 300 *sq.*
