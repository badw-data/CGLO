**\*ζωμάλιστρον** *v.* ζωμάρυστρον.

**ζωμάρυσις** *v.* ζωμήρυσις.

**ζωμάρυστρος** (*et* **ζωμαρυστάρυστον** *et* **ζωμαρυστάρυστρον** *et*
**ζωμαρυστάλιστρον**) cattia, trulla, trullaria.

**\*ζωμευτής (?)** iuscellarius.

**ζωμευτόν** ius 2.

**ζωμήρυσις** *et* **ζωμάρυσις** popia, trulla, trulla lignea.

**\*ζωμίτης (?)** *cf.* iuscellarius.

**ζωμοποιός** iuscellarius.

**ζωμός** ius 2., *cf.* zomos, - παχύς tuccetum, διὰ -οῦ iuscellatas.
*Cf. etiam* κατάχυμα, ψιλήπλευρον.

**ζωνάρι(ο)ν** cingillum.

**ζώνη** arcus, balteum, calautica, cinctus 1., cingulum, semicinctium,
zona, - οὐρανία plaga. *Cf.* ἄζωστος.

**ζωνίον** cingillum.

**ζώννυμι** *et* **ζώννύω** accingo, cingo, praecingo; ἐζωσμένος cinctus
2., -οι ἐν πολέμῳ endo procinctu.

**ζώντως** vivide.

**ζῶον** animal, - ἄγριον *v.* παγίς, - ἄλογον animal, muta bestia
(*cf.* μασθός, μῖξις, φθειράριος), - λογικόν animal, - τετράποδον *v.*
ἀγωγή. *Cf.* vita.

**ζωοποιῶ** vivifico.

**ζωός** incolumis, vivus.

**\*Ζωπύριον** *cf.* azofirion.

**ζώσιμος** victurus, vitalis.

**ζῶσις** cinctus 1.

**ζῶσμα** cinctus 1. *Cf.* ζῶμα.

**ζωστήρ** balteum, cingulum (*et* -a). *De glossa* cinctor *v.* ζώστης.

**\*ζωστήριον** cingulum.

**\*ζώστης** cinctor (*v. p.* 436).

**ζωστός** *et* **ζωστόν** praecincta, subarmalis (-le), -ή
praecinctus 1. (-a).

**\*ζώστρα** *cf.* ζῶστρον.

**\*ζωστρίς** (*Eins.*) cingulum.

**ζῶστρον:** -α (*an* ζώστρα?) cinctura, cingulum (gingla).

**ζωτικός** vitalis, vivalis, vividus.

**ἤ** an, aut, quam, ve, vel, - οὐ necne, - τί quidve. *Cf.* ἦ, ἤτοι,
πρίν.

**ἦ** = ἆρα an, anne. *Cf.* ἤ.

**ἥβη** iuventa, inventas, iuventus, pubertas, pubes.

**ἡβῶ** adolesco, -ῶν puber.

**ἡγεμονία** ducatus, imperium, regnum.

**ἡγεμονικός** praesidalis, principalis. *Cf.* θρόνος.

**ἡγεμών** consularis, ducator, dux, praeses, princeps.

**ἡγητορία:** περὶ -ας actus quidam.

**ἡγήτωρ** *cf.* agitator.

**ἡγοῦμαι** duco, dux sum, existimo, opinor, pendo, reor, - σε πολλοῦ
pendo magni te, - εὐπρόσδεκτον acceptum habeo , ἡγούμενος *v.*
ἐπίσταθμος, τάγμα.

**ἡδέως** grato, iucunde, libenter, suaviter.

**ἤδη** etiam, iam, iamdiu, - ἐκ πολλοῦ *cf.*
iamdiu, - πάλαι iamdudum, iampridem, - πρότερον iampridem, - πρῶτον
iamprimum, - τε tandem, - ἤμην iam eram, καὶ - iamque. *Cf.* ἐντεῦθεν.

**ἥδομαι** *v.* ἥδω.

**ἡδονή** libido, suavitas, voluptas.

**ἡδονικώτατος** medullitus.

**ἧδος** delectatio.

**\*ἡδύανθος (?)** viola.

**ἡδύβιος** voluptarius.

**ἡδυλόγος** scurra.

**ἡδύνοντες (?)** *cf.* ethinones (*expl.* delectabiles).

**ἡδύοσμον** *et* **ἡδύοσμος** menta, menta nigra, menta sicca,
procinacia. *Cf.* luma.

**ἡδυπάθεια** libido.

**ἡδυπαθής** lascivus, libidinosus, voluptarius.

**ἡδύς** bellus, dulcis, glucidatus, iucundus, suavis, -ύ ἐστι libet,
ὅπως -ύ quilibet (quol.), ὅπερ -ύ quilibet (quodl.), ἡδύτερον libenter
(-tius).

**\*ἡδύστομος** (*Eins.*) iocosus.

**ἡδύτης** dulcedo, iucunditas, lepos, suavitas.

**ἥδω** delecto, iuvo, -ομαι delecto (-or), libenter habeo, -όμενος
gaudens, libens.

**ἠθικός** moralis, subultaneus. *Cf.* ethica (*expl.* moralis, ars
moralis, proprietas).

**ἠθμός** colum, qualus.

**ηθος** habitus animi, mos, ἐν φιλοσόφων (? φιλοσόφῳ *Buech.*) ἤθει
secta (-ae).

**ἠίθεος** puber, pubes.

**ἥκιστα** minime.

**ἥκω** venio (-ni).

**ἠλακάτη** *et* **ἠλεκάτη** colucla, colus (*cf. etiam* culus 1.).

**ἠλεκάτη** *v.* ἠλακάτη.

**ἡλέκτρινα** sucina.

**ἤλεκτρον** gumen, lacrima, sucinum. *Cf.* electrum.

**ἡλιάζω** apricor (-o), insolo, -ομαι apricor, insolo (-or), soliculor,
-όμενος apricus.

**ἡλιαστήριον** solarium.

**\*ἡλιαστής** lutor.
