**Obiurgans** *v.* obuncans.

**Obiurg\<at\>io** ἐπίπληξις II 310, 21 (*suppl. e*).

**Obiurgo** ἐπιτιμῶ II 312, 11. **obiurgat** oppugnat, castigat,
increpat IV 263, 10. obpugnat V 314, 54. castigat, culpat IV 546, 45.
increpat IV 130, 39; V 469, 35. increpat, corrigit, monet IV 370, 7. ad
uicem litigandi (*ita uel* litiganti *uel* -tem *codd.*) redarguit IV
128, 4; 546, 17; V 123, 23. **obiurgabit** increpauit IV 131, 11; V 469,
36 (obiurgauit). **obiurgetur** increpetur, culpetur V 414, 15 (*reg.
Bened.* 23, 7). *V.* castigo.

**Oblatio** προσφορά II 136, 5 (*cf. margo*); 423, 32; III 239, 5.
προσαγωγή II 420, 22. προσκομιδή II 421, 59. ἀπαρχή, προσφορά III 170,
60 (-onis *uel* -es). ἐπομβρία **oblatio** imbrietas III 425, 51 (*v.*
imbrietas).

**Oblator** συντελεστής II 136, 10.

**Oblatrantem** musitantem, murmurantem V 469, 39.

**Oblatro** κατυλάσσω (*i. e.* καθ-) II 346, 41. **oblatrat** obmurmurat
IV 417, 45. obstrepit IV 128, 6; 543, 11. murmurat V 544, 54. murmurat,
obstrepit V 469, 38.

**Oblatum** προσενεχθέν II 136, 6. **oblato** offerto IV 128, 5 (*v.*
officium); 543, 10; V 123, 36; 469, 41 (*cf. Verg. Aen.* XII 109).
**oblata** ostensa, in palam producta IV 455, 28 (*Verg. Aen.* I 450).
offerta IV 263, 16.

**Oblectamentum** παρηγορία II 398, 25. **oblectamento** delectamento V
636, 11.

**Oblectatio** παρηγορία II 398, 25. τέρψις II 453, 34. ἀπάτη, τέρψις II
136, 7.

**Oblectatoria** θελκτήρια II 327, 17.

**Oblecto** παρηγορῶ II 398, 26. delecto, consolor V 537, 66 (*cf. Ter.
Ad.* 49). **oblectat** παρηγορεῖ (consolatur) II 562, 43. blandit V 315,
25. delectat uel blanditur IV 370, 9. obicit, delectat IV 127, 40 (*v.*
obiectat). delectatione (uel -em) infundit IV 128, 43; 263, 15; 543, 40;
V 123, 35; 469, 43. **oblectant (!)** delectantur, gaudent V 555, 9.
**oblactare** reficere IV 129, 52; V 469, 46; 508, 2. **oblectatur**
(obiect. *codd.*) amicum (animum?) pascit IV 127, 47; 543, 39.
**oblectatur** quasi cum lacte, cum fraude, ut Terentius (*Andr.* 648):
nisi me lactasses amantem: unde et oblectare dictum est *Plac.* V 88, 17
(*v.* lactas). *Cf. Isid.* X 199. **oblectari** delectari IV 370, 8.

**Oblegat** obligat, obcaecat V 471, 39. *V.* obrogare, obligo.

**Oblegatum** iniunctum, mandatum *Plac.* V 36, 27 = V 88, 18 = V 125,
4.

**Oblicare** *v.* obliquo.

**Oblidantur** συνθλα\<σ\>θῶσιν II 136, 26 (*suppl. e*).

**Obligamentum** lybb (*uel* libb, *AS.*) V 376, 15.

**Obligantes** circummittentes IV 370, 12; V 469, 49; 630, 16 (*quod
mutare non ausim*: *cf.* obligo).

**Obligatio** ἐνοχή II 299, 60. ἐποχή III 458, 15; 486, 38. ὑποθήκη II
466, 21. ἐνοχή, ὑποθήκη II 136, 8.

**Obligatorium** ἐνοχοποιόν II 299, 62.

**Obligatura** κατάδεσμος II 340, 38.

**Obligatus** ἐνδεδεμένος, ἔνοχος, ὑπόχρεος II 136, 12 (ὑπόχρεως *e*).
ἔνοχος II 299, 63. ἐνεχόμενος II 298, 50. ἔνοχος, ἐναγόμενος . . .
φεύγων III 276, 60 (*unde*?). culpabilis II 588, 20. **obligata**
ὐποκειμένη II 136, 13. oblita (oblimata oblita?) V 376, 10.
**obligatam** ὑποτεθεῖσαν II 136, 14.

**Obligatus** ἐνοχή II 299, 60. ὑποθήκη II 466, 21.

**Obligo** ὑπεύθυνον ποιῶ II 465, 10. ὑποτίθημι ἐπὶ ἐνοχῆς ἢ ἐνεχύρου II
468, 19. **obligat** obumbrat IV 455, 29 (obumbrat *Verg. Aen.* XI 223).
nectit IV 370, 13. circummittit IV 128, 44 (oblegat *ad*, *recte*?);
544, 43; V 227, 32 (oblegat *codd.*); 469, 44 (*ubi* circumnectit
*Nettleship 'Journ. of Phil.'* XIX 191). **obligare** ὐποθέσθαι II 136,
11. **obligauerunt** ὑπέθεντο II 136, 9. *V.* oblegat.

**Oblimat** limpidat IV 128, 26; 263, 14 (obliminat *uel* oblimpidat);
370, 14; 545, 18; V 227, 33; 315, 35; 376, 37; 606, 26 (obilat); 630,
17. **olimat** limpidat IV 132, 19; 264, 34; 371, 43; V 229, 6; 316, 3;
471, 35; 36. **oblimet** claudat, opturat (!) V 573, 48 (*cf. schol. in
Verg. Georg.* III 136; *Lachm. in Lucr.* VI 840).

**O\<b\>limata** ueterata V 544, 57. *V.* obligatus. *Cf.* **olitinata**
ueterata, antiqua IV 371, 44 (*ubi* olitana *Landgraf Arch.* IX 404.
obliterata?).

**Oblimet sulcos** obducat, id est limo repleat *Plac.* V 88, 20 (*cf.
Verg. Georg.* III 136).

**Obliuire** linare *Plac.* V 36, 9 = V 88, 21 = V 124, 54 (*ubi*
liniare *Deuerling*, linire *Maius*).

**Oblino** (obligo *cod. corr. e*: *nisi* oblito *corrigendum*) ἐνχρίω II
136, 15. **oblinat** ἐπιχρίει (-ίῃ?) II 136, 19. **oblinere** ἐγχρίειν
II 136, 17. **obleuit** obliniuit *Plac.* V 36, 6 = V 88, 19 (oblinuit)
= V 124, 53.

**Oblique** adcliue uel recte (non r. *Hildebr.* tecte *Schoell*) IV
370, 15. *V.* accliuis.

**Obliquiloquus** λοξίας II 362, 41 (obloquis *cod. corr. Vulc.*).

**Obliquitas** πλαγιότης II 408, 31.

**Obliquo** πλαγιάζω II 408, 28. **obliquat** conuertit, inflectit
(transuersus uadit *add. d c f*) IV 455, 30 (*Verg. Aen.* V 16). 
