IV 481, 2. **altia** alites, aues V 490, 45. **altilium** σιτευτῶν II
15, 33 (*cf. Loewe Prodr.* 327).

**Altilitate** ab alendo, id est ipsa res quae alitur *Plac.* V 4, 2 = V
46, 16 (*ubi non recte* altili dote *Koch*).

**Altissimo otio** longissima quiete IV 15, 24 *cum libro gloss.* (*cf.
Loewe Prodr.* XIV). largissimo quies (!) V 632, 29 (apissimo).
largissima V 167, 30. largissimo IV 19, 7; V 167, 32; 440, 34 (*ubique*
apissimo). **apissimo diu** largissimo V 167, 31. *Cf. Loewe Prodr.*
375, *Landgraf Arch.* IX 362, *Ott Fleckeiseni Ann.* CXVII 426
(amplissimo otio largissima quiete).

**Altitudo** ὕψος II 469, 30; III 161, 49; 328, 44; 340, 14; 439, 24.
βάθος III 205, 2. βάθος, ὕψος II 15, 32.

**Alto** βαθύνω II 255, 25. ὑψῶ II 469, 31.

**Alto a sanguine** (alta s. *vel* alto s. *libri*) ex diuino genere IV
429, 19 (= *Verg. Aen.* IV 230; V 45; VI 500). *V.* alea.

**Altocinctus** superius cinctus II 565, 53. *V.* alticinctus.

**Alto maris** pelago *a post* IV 15, 20.

**Altor** τροφεύς II 460, 14. σιτευτάρις, τροφεύς II 15, 34. nutritor IV
15, 33. nutritor, educator IV 307, 40. **altores** cultores V 264, 9;
344, 57. *V.* alitor.

**Altrinsecus** ἑτέρωθεν II 315, 59. non ex alto, sed ex altera parte
*Plac.* V 3, 6 = V 46, 19. hinc et inde uel desuper, sicut extrinsecus
dicimus ab eo quod est exterius (quod extrinsecus *R:* quod extra est
*cod. Ambros.*) et intrinsecus ab eo quod intus est (est intus *R*)
*Plac.* V 5, 26 = V 46, 18 = V *praef.* XVI. **altrinsecus** ex altera
parte *Plac.* V 7, 31 = V 46, 17 (alterutra pars). ex utraque parte IV
480, 46; V 264, 45; 345, 17. dextra laeuaque IV 15, 1. hinc atque inde
IV 308, 5. dextra laeuaque aut abinuicem IV 480, 48. abinuicem, hinc et
inde IV 206, 2. on ba halbe (*AS.*) V 340, 20.

**Altriplicem** duplicem, dolosum *Plac.* V 7, 45 = V 46, 20 (= V 48,
25); *unde Scaliger* V 591, 51.

**Altriplicitas** *v.* biplicitas.

**Altrix** θρέπτειρα II 329, 9. τιτθή II 14, 57. nutrix IV 14, 30; 206,
19; V 263, 67; 165, 19 (alecris). nutrix [hebraeorum] *a post* IV
15, 16. nutrix, mater seu genetrix V 439, 1. **altricem** nutricem IV
205, 44; 481, 4; V 165, 30; 438, 58. nutricem, educatricem IV 429, 20 (=
*Verg. Aen.* III 273). **altrices** nutrices uel educatrices IV 14,
22; 481, 6.

**Altruncat** *v.* auerrunco *et* alieno.

**Altum dolorem** infixum IV 15, 8; 485, 9 (*Verg. Aen.* I 209).

**Altus** ὑψηλός II 15, 35; 469, 23; III 80, 8. βαθύς II 255, 22.
celsus, excelsus, eminens IV 308, 7. **altum** ὑψηλόν II 469, 22; III 6,
72; 161, 48; 340, 13; 439, 22. βυθός II 260, 42; 499, 43; 543, 26. βάθος
II 255, 20. βαθύ II 255, 24. mare uel caelum IV 485, 8; V 263, 65; 345,
6. mare uel praeclarum aut caelum IV 14, 28. **alto** nunc mari, alias
caelo IV 429, 18 (*Verg. Aen.* I 5, *saepius*). **altius** ὑψηλότερον
II 15, 36. diligentius IV 479, 25. **altissimus** ὑψηλότατος II 469, 25.
ὕψιστος *be post* II 15, 35; 469, 29; III 439, 23. *Cf.* περίχωρον
**altus** (actus *H.*) III 199, 30.

**Altus** nutritus, pastus IV 15, 20. nutritus IV 205, 40 (aletus); 486,
20. **alitos** nutritos *Plac.* V 6, 16 = V 46, 10 (alitus nutritus
pastus).

**Alucinatio** lucis alienatio IV 15, 9; 205, 27; 485, 33. *Cf. Festus
Pauli p.* 24, 14.

**Alucinor** id est somnio, uerbosor uel nugor V 615, 3. **halucinares**
nugares IV 85, 18. **alucinatur** ἀλύει II 15, 43. **halucinari**
oberrare (ab.?) V 642, 39 (*Non.* 121, 19). *V.* futura alucinatus, V
*praef.* V. *Cf. Martian. Cap. p.* 167 (*Eyssenh.*); *Wessner Comm.
Ien.* VI 2, 100, 11; 126.

**Alumen** στυπτηρία II 15, 38; 439, 29; 506, 23; 529, 5; 546, 46; III
195, 1; 273, 56; 547, 19; 587, 18; 606, 11; 607, 44; 617, 8. σχιστή III
629, 50 (scitis); 595, 51 (alum scistis). *Cf.* σχιστή (scistis)
στυπτηρία III 606, 6. locus ubi tanantur coria (*cf.* aluta) II 566, 14.
*V.* aqua stypterizusa.

**Alumen album** III 543, 1.

**Alumen liquidum** στυπτηρία ὑγρά (isteptirias nigras *cod.*) III 539,
65; 565, 53. λιπαρά III 568, 27 (liparis). alumen lipari **alumen
liquidus** III 597, 45. lipari **alumen liquidus** III 602, 43.

**Alumen scissum** III 542, 30. στυπτηρία σχιστή (istipteriascistis *et*
scissus) III 565, 54. στυπτηρία III 595, 12; 629, 4. **alumen scisum**
id est bulum scysum (*cf.* bolum tusum *Moore Arch.* X 268) III 580, 22.
*Cf.* **nauistum** id est στυπτηρία (= scissum) III 570, 26. στυπτηρία
ἀγρία (ὑγρά?) id est humida III 586, 13; 606, 12 (stipterion). **alumen
stipum** (scissum *Cuiacius*) στυπτηρία σχιστή II 15, 37.

**Alumnus** θρεπτός II 329, 8; III 28, 46; 182, 11; 253, 56; 374, 67;
467, 18. τρόφιμος III 304, 24 (*Serv. in Aen.* XI 33). τρόφιμος ὁ
τραφεὶς ὑπό τινος II 460, 16. οἰκοτραφής, τρόφιμος, θρεπτός II 15, 41.
nutritus *a post* IV 15, 32.
