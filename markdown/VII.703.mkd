**lininryhae** (A, linninryhae E) villa *s. v.* villosus.

**listan vel thres** limbum *s. v.* limbus.

**lithircadae** promulserit (*Oros.* V, 17, 12 permulserit *confert
Schlutter*).

**loca** (= locc?) floccus.

**locaer vel sceaba** runcina.

**locar** *v.* bruun.

**locc** *v.* loca, **loccas** antiae.

**loda** *v.* lotha.

**loergae** *v.* lorg.

**logdor** cacomichanus.

**loh *vel* dal** (*utrumque potest esse palaeotheod.*) baratrum.

**lorg** colus 2., **lorg, couel** colum *s. v.* colus 2., **loergae**
amites.

**lotha** lacerna (*v.* haecilae vel lotha *s. v.* haecilae); lodix.
**loda** sagulum.

**luad** *v.* lath.

**ludgaet: dorh ludgaet** per pseudothyrum.

**lundlaga** renunculus.

**luus** peducla *s. v.* peduculus.

**lybb** (E, libb A) obligamentum.

**lybisnę** dilaturas *s. v.* phylacteria (*an* ligaturas?)

**lynisas** axedones V, 338, 21, *quod ad­dendum est in Thes. s. v.*
axedo.

**lypbcorn** cartamo *s. v.* cardamum.

**lytisna** concedam *s. v.* concedo.

**lytlae sneglas** cocleae *s. v.* cochlea.

**maanful** (E, meinfol A *palaeotheod.*) infandum *s. v.* infandus.

**maefuldur** *v.* mapuldur.

**maerh** lucanica.

**maest** arbor.

**maesttun** (E, mestum A) saginabant *s. v.* sagino.

**maethlae: in maethlae** (E, in medlae A) in curia.

**maettoc** *v.* mettoc.

**maffa** (*et* naffa) omentum (*saxonicum putat Kluge*).

**malscrung** pressicium (praestigium *coni. Steinmeyer*).

**malt** bratium.

**mand** qualus; cophinus E *post* V, 352, 34 (*adde in Thes.*);
**mand** (E, mondi A, *instr. sing.*) corben *s. v.* corbis, **mond**
corbis.

**manu brystae** (*et* manu biriste) iuba.

**mapuldur** (E, maefuldur A) acerabulus.

**masae** parrula.

**mattae** spiathio *s. v.* psiathium V, 417, 28 (*saxonicum putat
Kluge*).

**mearcisern** *v.* mearisern.

**meard** *v.* mearth.

**mearisern** (E, merisaen A, mearcisern *Kluge*) cauterium.

**mearth** (E, meard A) furunculus 2.; **merth** catta; felis *s. v.*
feles.

**medlae** *v.* maethlae.

**meeg** contribulus (= contribulis).

**meeli** (E, meelu A) alveum *s. v.* alveus.

**meelu** *v.* meeli.

**megsibbi** affectui *s. v.* affectus.

**meldadum vel roactum** (A, wroegdun C) deferuntur *s. v.* defero.

**men** *v.* meu.

**menescillingas** (E, meniscillingas A) lunulis *s. v.* lunulae.

**mengio** (mengia *coni. Kluge*) margo.

**meniscillingas** *v.* menescillingas.

**meniu** dilectum *s. v.* dilectus 2.

**meodomlice** dignitosa *s. v.* dignitosus.

**mera** *v.* merae.

**merae** (A, mera E) incuba *s. v.* incubo.

**meresuin** bacarius.

**mere uueard** (E, mere uuard A) percrebuit *s. v.* percrebrescit.

**meri** stagnum. *V.* staeg vel meri.

**merici** (E, meru A) apio *s. v.* apium.

**merisaen** *v.* mearisern.

**merisc** (*et* **merix** *in eodem* A) calmetum (= calametum).

**merix** *v.* merisc.

**merth** *v.* mearth.

**meru** *v.* merici.

**merze** merx.

**mestum** *v.* maesttun.

**metrap** bolide*s. V.* sundgerd in scipe vel metrap.

**metticas** *v.* mettoc.

**mettoc** (A, maettoc E) tridens, **mettocas** (E *bis,* metocas A
*altero loco*) ligonas *sim.* (= ligones), **mettocas** (E, metticas A)
rastros *s. v.* raster.

**meu** (A, men E) laris *s. v.* larus.

**micel** *v.* suiđe micel.

**mich** *v.* mycg.

**mid nedlae** (A, miđ naeđlae E) **asiuuid** pictus acu.

**middil: ođ middil** pube tenus.

**mihi** *v.* mycg.

**inilc** *v.* suur milc.

**milcapuldr** *v.* milscapuldr.

**milcid** (A, milciþ E) morgit *s. v.* mulget.

**milscapuldr** (E, milcapuldr A) melarium.

**milti** splenis *s. v.* splen, **milti** (A, multi E) lien.

**mintę** peguIium *s. v.* puleium.

**misbyrd** abortus 2.

**misthagch** (i. e. misthah) degeneraverat *s. v.* degenerat.

**mistil** viscus *s. v.* viscum.

**miđ** *v.* mid nedlae asiuuid.
