στάκις II 9, 25. ἑκάστοτε II 288, 40. plerumque, frequenter IV 305, 13.
*Cf.* **adsiduae** multitudo (?) IV 11, 3; **at si de** μὰ τὸν (!)
κεφαλήν σου: ita enim Graeci iurant (*ubi* adsidue *W. Heraeus Arch.* IX
594) V 652, 32 (*cf. schol. Iuvenal.* VI 16).

**Assiduitas** (ads.) continuatio IV 305, 14.

**Assiduus** (ads.) συνεχής II 445, 51; III 372, 51. συνήθης III 177,
25; 249, 67. ἐπίμονος III 331, 48; 519, 36. παράμονος III 332, 35.
dicebatur apud antiquos qui assibus adaeratibus expensum (*scr.* ad
aerarii expensam) conferendis et in negotiis puplicis frequens erat V
561, 53 (*cf. GR. L.* I 75, 8; *Isid.* X 17). **adsidua** πυκνή II 426,
3. **assiduos** capite censos, qui nihil praeter prolem dare poterant
(nihil dare poterant prolem *R: ubi* nisi prolem *c*^2^ *b*^2^), unde et
proletarii dicti sunt et assidui milites ab (et ab *R*) assiduitate
officii *Plac.* V 7, 7 = V 45, 1. *Cf. Festus Pauli p.* 9, 9 et 226, 13.

**Assignatio** (ads.) ἀπονέμησις II 239, 19.

**Assignato** (ads.) tradito uel deputato (*reg. Bened.* 42, 15) V
412, 42.

**Assigno** (ads.) ἀπονέμω II 239, 17. ἐπισφραγίζω II 311, 29.
παρασημειοῦμαι II 396, 15. παρατίθημί τι φυλαχθησόμενον II 396, 46.
προσκυρῶ II 422, 12. **adsignat** προσαγγέλλει II 9, 31. restituit IV
305, 19; 479, 46. restituit uel probat (praebet?) IV 9, 39.

**Assimilatio** (ads.) ἀφομοίωσις II 9, 14.

**Assimilatus** (ads.) similis IV 481, 18.

**Assimilis** (ads.) παρόμοιος II 399, 8. παραπλήσιος II 396, 1. ἐοικυῖα
II 304, 57.

**Assimilo** παρομοιάζω III 155, 52. ἀπεικάζω II 233, 53. **adsimulo**
προσποιοῦμαι II 422, 52.

**Assisto** (ads.) παρίσταμαι II 398, 42; III 154, 10. ἀποσυνιστῶ II
241, 11. συνιστῶ II 446, 45. συνίσταμαι II 446, 43. συνέστηκα II 445,
33. adsistit παρίσταται II 563, 2. παρέστη II 562, 30 (adstitit?). ibi
stetit IV 404, 42. **adstiti** παρέστηκα II 398, 16.

**Asso** ὀπτῶ III 399, 43. **assas** ὀπτᾷς III 399, 44. **assat** ὀπτᾷ
II 22, 5; III 399, 45. **assamus** ὀπτῶμεν III 399, 47. **assa** ὄπτησον
II 22, 6; 386, 2; III 399, 42. **assaui** ὤπτησα III 165, 57; 399, 49.
**assasti** ὤπτησες III 399, 48. **assauimus** ὤπτήσαμεν III 399, 51.
**assauerunt** ὤπτησαν III 399, 50.

**Associo** (ads.) προσοικειοῦμαι II 422, 24. *Cf.* **adsuscitare**
requirere, consociare V 437, 31 (*contam.*).

**Assolet** (ads.) solite, consueuit IV 305, 21. **adsolen\<t\>**
εἰώθασιν II 288, 21. *Cf.* assolite.

**Assolite** (ads.) consuete, solite V 437, 30.

**Assolitum** (ads.) *v.* allositum.

**Assua** *v.* asisua.

**Assuarius** (assarius *b*) qui assat II 568, 29.

**Assubicio** (ads.) προσυποβάλλω II 423, 18.

**Assuefacio** (ads.) προσεθίζω II 420, 55. συνεθίζω II 445, 1.
**adsefacit** adsueuit IV 305, 11.

**Assuefactus** (ads.) ἠθισμένος II 9, 24 (εθισμ.); 323, 50.

**Assuefio** (ads.) ἐθίζομαι II 284, 51.

**Assuesco** (ads.) προσεθίζω II 420, 55. συνεθίζω II 445, 1. ἐθίζω II
284, 52. **adsueuit** εἴθισεν II 9, 30. ads\<u\>efecit, consueuit IV
305, 25.

**Assuetudine** (ads.) consuetudine IV 8, 26; V 163, 26; 632, 9.

**Assuetus** (ads.) ἠθισμένος II 323, 50. συνήθης II 446, 14. consuetus
IV 8, 27.

**Assuetus** (ads.) προσεθισμός II 420, 56.

**Assuit** consuit, annexuit V 492, 6. **ad suissent** (*ita c, Loewe
Prodr. p.* 149. adsuessent *vel* adsuiscent *codd.* adseruissent *et*
adsuessent *Nettleship* ῾*Journ. of Phil.'* XIX *p.* 114) adnexuissent,
consuissent IV 9, 47; 485, 27; V 163, 27.

**Assula** πελέκημα II 521, 34. *V.* hastula.

**Assulatim** \<minutatim\> V 637, 27 (*Non.* 72, 23).

**Assulentes** (adsulentes *R.* adsolentes *G*) adsilientes *Plac.* V 5,
5 = V 45, 2 (*ubi* adsultantes *Deuerling*, adsulientes *Loewe GL. N.*
91).

**Assultibus** (adsalt. *codd.*) saltibus IV 428, 5 (*Verg. Aen.* V
442). *Cf.* **adstultibus** transitus a saliendo V 261, 52.
**adsultibus** [adsumere] saltibus IV 10, 30 (adsumere *novum lemma
est: cf. Nettleship 'Journ. of Phil'* XIX 114); V 163, 28. **adsultum**
subsidium IV 404, 44.

**Assum** (ads.) πάρειμι II 397, 34; 398, 38; 561, 51; III 156, 18; 339,
71; 439, 1. praesens sum IV 305, 26. **ades** (adis *cod.*) παρεισιν
(πάρει *e*) II 7, 1. **adest** πάρεστιν II 9, 26 (*cf.* II 6, 44); III
156, 19. praesto est IV 8, 46 (post); 9, 34, 203, 17; 476, 24.
**adsumus** aduenimus IV 404, 46. **adsis** placidus sis, propitius sis
IV 9, 54; 481, 8. **adsit** παρέστω II 562, 32. praesto sit [iamque
etiam (?)] V 261, 50. **ades** πάρεσο II 398, 17. ueni V 530, 15 (*=
Ter. Andr.* 344). **adesto** succurre IV 10, 19. auxiliare IV 203, 18.
**adeste** adestote, subuenite, succurrite IV 8, 20. **adestote**
succurrite IV 482, 52. **adesse** παρεῖναι II 6, 49; 397, 35; 562, 1.
προσεῖναι II 420, 58. παρεῖναι, ὑπάρχειν II 6, 42. συνηγορεῖν II 7, 3.
**adforet** adesset V 261, 37
