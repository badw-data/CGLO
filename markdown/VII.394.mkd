**Uarietas** ποικιλία II 204, 41 (-εια *cod.*); 411, 22. *V.* sine ulla
uarietate.

**Uarii** πέρκαι (*pisces*) III 436, 47 (*cf. Isid.* XII 6, 6 *et
Koerting s.* uario).

**Uariola** [id est] uolaticas graece dicitur III 630, 26. uolaticas
Graeci dicunt III 596, 17. uolaticas **uarila** passio III 606, 45. *Cf.
Ducange, Koerting.*

**Uario** ποικίλλω II 411, 25. **narior** περκάζω II 406, 17. **uariat**
περκάζει II 406, 18.

**Uarius** ἴονθος, σκαμβός, ποικίλος II 204, 43 (*v.* uarus). ποικίλος
II 411, 24; III 335, 52; 502, 37. παντοδαπής II 393, 44. παντοδαπός III
252, 22 (*unde*?). ἀλφώδης III 180, 54; 206, 51; 253, 11. falla\<x\> IV
425, 34. discolor, mutabilis IV 399, 42. **uaria** ποικίλη II 204, 39.
**uarium** ποικίλον II 411, 23; III 22, 16; 93, 25; 369, 46. multiformem
IV 188, 35. **uarium uarium (?)** ποικιλονχοιμα (ποικιλόχρωμον *Hagen
progr. Bern.* 1877 *p.* 13. ποικίλον χύμα *Buech.*) III 322, 47.
**uaria** ποικίλα III 209, 47. **uarios** diuersos IV 188, 23 (*Verg.*
*Aen.* I 204). *V.* arrius.

**Uari[e]x uarica** κρισσός II 355, 28 (*corr. a*). **uarix**
κρισσός II 541, 64. κιρσός, κρισός III 351, 57. scabies II 596, 15.
omprae (*vel* amprae, *AS.*) V 399, 19. **uarices** κρισσοί II 204, 40;
III 206, 14. κρισσοὶ τὸ πάθος III 296, 47; 524, 60. *Cf.* cisus
(crissus) uena cum inflatione circa ap\<p\>arentia suri (surae?) III
598, 36 (*v.* cirsocele). **uarices** uenas contortas atque plenas V
252, 9. **uaricae** uitia quaedam pedum quae stando nascuntur IV 294,
38; V 518, 7; 527, 26; 582, 28. **uarices** uerrucae (= uaricae) V 334,
32. **uarruces** uarruce (*vel* uarruccae) facit V 400, 23. **uaces**
uenae grossae V 517, 53; 527, 33; 582, 18. *V.* uarica.

**Uartiuus** στρεβλός II 204, 44 (uatiuus *c*, uatinius *Vulc.*).

**Uaruo** *v.* baro.

**Uarus** σκαμβός III 330, 11 (*v.* uarius). **ualus** qui genibus
iunctis ambulat IV 189, 5; 294, 28; 576, 51; V 252, 5; 334, 48; 400, 62
(uaginus); 527, 30. qui pedibus iunctis ambulat IV 399, 38. **uatus**
tortis pedibus IV 294, 44. *Cf. Loewe Prodr. p.* 388: *quamquam
exspectes* uatius *cum Hildebrandio vel* ualgus.

**Uaruassi (?)** de uana dictum V 415, 29.

**Uas** ἔγγυος II 508, 46. fidei iussor V 334, 42. fideiussor IV 189,
12; 576, 43. fideiussor, satisdator IV 399, 46. **uadem** fideiussorem
IV 188, 17; 576, 37. **uades** ἔγγυοι, ἀνάδοχοι II 203, 54. ἐγγυηταί II
283, 30. dicuntur qui legaliter causas agunt V 251, 36. fideiussores IV
188, 20; 294, 23; 576, 38; V 334, 33; 399, 59. fideiussores,
satisdatores IV 399, 14. *V.* fidissimi uades, uates.

**Uas** ἀγγεῖον, σκεῦος II 204, 45. ἀγγεῖον II 216, 1; III 326, 45.
**uas uasum uasa** σκεῦος II 433, 19. **uas** σκεῦος II 508, 53; 517,
11; III 270, 42; 326, 46. **uasa** ἀγγεῖα, σκεύη II 204, 46. σκεύη II
433, 15; III 197, 59; 215, 24 (sceuge) = 231, 2 = 650, 9. σκεῦος II 498,
16. **uascim** (= uasum) uasculum II 596, 17 (*nisi* uascium = uasclum
*subest*). *Cf. GR. L.* VII 112, 3; *W. Heraeus 'Spr. des Petr.'* 42.

**Uasa fictilia** friuola IV 399, 44. *V.* fictilia uasa.

**Uasarium** σκευοθήκη II 433, 17; III 322, 27; 366, 57. κυλικεῖον
(κυλῑκιον *cod.*) II 204, 47. σκευοφόριον III 270, 43 (*unde*?).
σκευόφορος III 197, 36.

**Uasarius** σκευοποιός III 309, 39.

**Uasa tornata** σκεύη \<τε\>τορευμένα (tormema *cod.*) III 93, 52. *V.*
tornatum.

**Uasca** μελητικὸς (!) αὐλός II 204, 54. **uacca** μελετητικὸς αὐλός II
203, 42. *Cf. Solin.* V 19 (52, 4); *Nettleship 'Contr.' p.* 605; *Serv.
in Aen.* XI 737. *Verum iam Salmasius invenit Plin. Ex.* 122 C; 124 A.

**Uascellum** σκευάριον II 433, 14. **uascella** σκεύη III 22, 41. *Cf.
Haupt Op.* II 179, 6.

**Uascilium** paruum uas. Macrob. soluens radiis in aqua subiecta
uascillo (*immo Marbodaeus de heliotropia cap.* 18: Quae solis radiis in
aqua subiecta uasillo *adnotante Arevalo*) *Scal.* V 613, 41.

**Uascularius** σκευοπώλης II 433, 18. uasa uendens II 596, 23.

**Uasculum** discus, fasculum (ferculum *Loewe GL. N.* 108: *v.* discus,
ferculum) IV 399, 45. **fasculum** discum V 599, 26.

**Uascum** inanem, nugatorium *lib. gloss.* (*Mai* VII 585; *adde Osb.
p.* 623 uascus inanis). *Cf.* **uasculam** *in praef. Anthol.* V *p.* V
*et Goetz 'Sitzungsber. der S. Ges. d. W.'* 1896 *p.* 69 *et* uescus.

**Uasifer** σκευοφόρος II 433, 20 (*v.* sarcinator 2).

**Uastatio** λεηλασία II 204, 55. πόρθησις II 413, 42. **uastatione**
πορθήσει II 204, 50.

**Uastator** πορθητής II 413, 43.

**Uaste** ἐξαισίως II 301, 36. μεγάλως II 366, 15.

**Uastescant** (uastissant *cod.*) disserant (= des.), solentur
(desolentur *L. Mueller*) V 648, 55 (*Non.* 185, 7).

**Uasticant** pro uicissitudine reddunt V 488, 29; 518, 8; 527, 31.;
582, 30. \<re\>masticant? uicissant?
