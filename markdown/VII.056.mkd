IV 138, 6; 547, 10; V 129, 50; 384, 18; 542, 50. *V.* cinis patricius.

**Patricus** paternus graece V 129, 59.

**Patrimonium** οὐσία πατρική II 143, 25; 531, 35. οὐσία II 390, 23;
504, 52; III 202, 27; 459, 34. *sine interpr.* IV 374, 29 (hominum
defensatur *interpr. d e. an ad* patronus *spectant*?).

**Patris (Patrius?) amor** paternus affectus IV 547, 6. *Cf. Verg.
Aen.* I 643 *sq.*

**Patrisso** πατρώζω II 399, 55. patris opus exerceo uel similo V
575, 1. **patrissat** patri\[s\] similat V 231, 26. patri (*uel* patris)
similis fit IV 138, 8; 547, 5; V 129, 51; 231, 25; 318, 35; 384, 24.
*Cf. GR. L.* V 54, 29; *Plaut. Ps.* 492; *Loewe Prodr. p.* 271. patrisat
(V 318, 35) tut. *Baehrens.*

**Patrite** (patricius?) uel a patre aut a patria, utrumque
intellegimus: set melius a patria: nam et pater a patria dicitur V 129,
55; *lib. gl.*

**Patritum** patriam (?) V 646, 21 (*Non.* 161, 4).

**Patrius** paternus IV 267, 43; 547, 7. **pat\[o\]rius** a patre
deriuatur IV 418, 20 (*cf. Serv. in Aen.* XII 736). *V.* dii patrii.

**Patrius animus** similis animus IV 418, 35 (*cf. Isid. Diff.* 453).

**Patrocinium** προστασία, βοήθεια II 504, 51. προστασία II 143, 26;
423, 7; 531, 20. πατρωνία II 548, 4. adiutorium V 318, 59.
**patrocinia** intercessio V 548, 43.

**Patrocinor** συνηγορῶ II 446, 9. προΐσταμαι ὅ ἐστιν βοηθῶ II 418, 8.
πατρώζω II 399, 55. πατρωνεύω II 399, 56.

**Patron** patronus V 129, 56; *lib. gl. V.* patronus.

**Patrona** πατρώνισσα II 399, 58; III 276, 66.

**Patronissa** patrona graece V 129, 57.

**Patronus** προστάτης, πάτρων τουτέστιν ὁ ἐλευθερώσας II 143, 27.
προστάτης II 423, 8; III 459, 35; 487, 6. πάτρων II 399, 57; III 304,
55/56; 374, 70. προστάτης, πάτρων III 276, 65. δικολόγος II 277, 37.
βοηθός II 258, 23. δικαιολόγος III 276, 61 (*unde*?). qui clientem
habet, id est qui alicui externo patris affectum impendit II *p.* XII
(*cf. Isid.* X 205). defensor V 538, 39 (*Ter. Ad.* 456: *cf. Don.*). *Cf.*
πρὸς τὸν πάτρωνα **ad patronum** III 50, 63. *V.* fisci patronus.

**Patronus causae** συνήγορος II 143, 28; 446, 7; III 362, 75.

**Patronymica** graecum est, a patre, ut puta (puta *om. G*) si dicas;
Tydides Tydei filius, Aeneius Aeneae filius *Plac.* V 91, 10 = V 130,
17 (*cf. Serv. in Aen.* I 97; X 123; *Isid.* I 6, 22; *GR. L.* IV 537,
7). dicuntur eo quod trahantur a patribus, ut Tydides Tydei filius,
Aeneius Aeneae filius, quamuis et a matribus et a maioribus dicuntur
(*uel* -antur) *Plac.* V 91, 11. *Cf. Serv. in Aen.* V 823; X 545.

**Patros** uitruncus (uitricus *Maius* VII 573). id est maritus matris
V 129, 58. uitricus *gl. lat. arab. p.* 369 *ed. Seyb. V.* patreus.

**Patruelis** ἀνεψιός II 143, 29. ἐξάδελφος III 459, 36; 476, 17. qui ex
patruo tuo progeneratur IV 374, 30. filius patrui uel filia IV 138, 13;
547, 8; V 129, 52; 231, 27. **patrueles** filii fratrum IV 268, 8. de
duobus fratribus nati V 318, 50. *V.* frater patruelis, sorores
patrueles.

**Patruus** θεῖος πρὸς πατρός II 143, 30; 327, 13. θεῖος πρὸς πατρός,
πάτρων (πάτρως?), πατράδελορος III 254, 2. θεῖος III 303, 61; 504, 45;
522, 19. πατρὸς ἀδελφός II 399, 53; III 28, 48. πατράδελφος II 399, 46;
III 181, 48. πατρωός III 253, 50 (*unde*?). πάτρως III 375, 25. frater
patris IV 138, 14; 268, 4; 374, 31; 547, 9; V 129, 53. **patruus**
**magnus** frater aui IV 268, 9; 374, 32. *Cf. Isid.* IX 6, 16; 24; *Serv.
in Georg.* III 55.

**Patule** ἀνεῳγότως II 226, 50. προφανῶς II 424, 27.

**Patuli boues** qui cornua diuersa habent V 555, 29. *Cf. Festus Pauli
p.* 221, 1.

**Patulus** ἀνεῳγώς II 226, 48. ἠνεωγμένος II 325, 18. ἡπλωμένος II 325,
36. **patula** ἀνεῳγυῖα II 226, 49. aperta, expansa IV 267, 45. ampla
aut diffusa IV 136, 24; V 129, 63. **patulum** ἐκπέταλον, συνεστός
(διεστός *c*), διακεχηνός II 143, 31. ἐκπέταλον II 143, 48. amplum,
apertum IV 374, 34. patens, amplum IV 547, 21; V 317, 44. apertum V 381,
39. dicitur quod naturaliter pateat V 129, 62 (*cf. Serv. in Ecl.* I 1;
*Isid. Diff.* 434). quod per se patet V 555, 37. patentem IV 136, 6;
*acd post* IV 138, 18. **patulam** amplam IV 547, 29. amplam uel
diffusam IV 138, 17. **patulae** patentes IV 138, 18; 547, 30; V 129,
64. **patulis** diffusis, apertis, extensis IV 374, 33. apertis IV 138,
24; V 421, 36 = 430, 14 (*Euseb. eccl. hist.* XI 25). patentibus V 317,
31. patentibus aut expansis IV 547, 20.

**Paturum** *v.* paedor.

**Patus** *v.* plautus.

**Paucies** raro (rara *cod.*) V 645, 76 (*Νon.* 157, 14).

**Pauciscent** *v.* rarescunt.

**Paucitas** ὀλιγανδρία II 381, 50.

**Pauco** μικροῦ ἐπίρρημα II 371, 43. ὀλίγιστον ἐπίρρημα II 381, 56.
