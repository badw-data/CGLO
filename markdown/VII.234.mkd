**Saties** κόρος II 353, 48; 496, 9. χορτασία III 80, 75.

**Satietas** κόρος II 353, 48. πλησμονή II 410, 8. χορτασία II 478, 4.
*Cf.* saturatio **satietas** χορτασία τροφῆς II 179, 10.

**Satigeri** *v.* saetigerus.

**Satim** *v.* affatim, saltem.

**Satio** σπορά II 178, 60. σπόρος II 436, 2; 495, 32; III 260, 60.
messis IV 282, 17. **sationes** σπόριμα II 178, 61. *Cf.* sementis.

**Satio** χορτάζω II 478, 3; III 80, 74. σπείρω II 435, 35 (sato?).
ἐμπιμπλῶ II 296, 27. ἐμφοροῦμαι II 296, 62.

**Sationales agros** seminales V 242, 31 (*cf. Serv. ad Georg.* I
*init.*).

**Satio populum tritico** χορτάζω τὸν δῆμον σίτου **sati\<a\>ui (?)** II
179, 1.

**Satipola (?)** cinido (κίναιδος *Buecheler.* sitopola *Schoell*) II
563, 32.

**Satis** ἱκανῶς II 331, 47. ἱκανόν III 75, 58; 146, 37. αὐτάρκως II
251, 27. consequens IV 167, 4; 389, 5; 564, 20; V 330, 23 (competens
*Nettleship 'Journ. of Phil.'* XX 56). multum V 556, 60. **satius**
ἱκανώτερον II 331, 48. ἄμεινον II 179, 8. melius IV 282, 5; 564, 28; V
146, 34; 390, 56. melius uel utilius IV 167, 1. *V.* satius est, sat
est, satis est, satim, satis datis est.

**Satis abundeque** ἱκανῶς καὶ ἐκτενῶς II 179, 3.

**Satisdatio** ἱκανοδοσία II 331, 45; III 276, 53. complementum II 592,
18; IV 564, 19; V 330, 1; 394, 21.

**Satis [datis] est** ἀρκεῖ, πολύ ἐστιν II 179, 4.

**Satisdator** ἱκανοδότης II 179, 2; 331, 46; III 4, 75; 75, 59; 146,
64.

**Satisdictio (?)** νομοθεσία III 276, 30 (*unde* ?).

**Satis est** ἀρκεῖ II 244, 46. ἱκανόν ἐστιν II 331, 44. *V.* sat est,
sat, satis.

**Satisfacio** πληροφορῶ II 409, 53. **satisfaciam** ἱκανοποιήσω III
146, 38.

**Satisfactio** πληροφόρημα II 409, 52. ἀπολογία II 238, 41; III 510,
26.

**Satis niger** taeterrimus IV 389, 6 (*v.* taeter).

**Satis prudens** V 664, 36.

**Satis superque** uberius, exabunda (-de *c e f*) IV 463, 12 (*Verg.*
*Aen.* II 642). satis [super]abundeque V 330, 41 (*om. cod. Werth.
Gallée* 352 *v. suppl.*).

**Satius est** melius est IV 463, 13 (*cf. Verg. Aen.* X 59; *Ecl.* II
14).

**Satiua terra** σπορίμη γῆ II 436, 1; III 261, 1.

**Satiuum** satoris (satorium? sata res *Schoell*) IV 422, 36.

**Sator** σπορεύς II 179, 7; 435, 60; III 260, 61. σπείρων III 303, 22.
γεννητής III 303, 21; 512, 12. seminator IV 282, 12; 463, 14 (*Verg.*
*Aen.* I 254; XI 725); 389, 7; V 330, 49. seminator, pater V 146, 44.
pater uel seminator IV 564, 23. pater et seminator IV 166, 20. *Cf.
Isid.* X 255. cultor *lib. gl.*; IV 289, 5 (sutor *cod.*). arator IV
167, 6; 564, 22. genitor V 146, 43.

**Satrapa** σατράπης II 429, 65. praefectus, qui est pecuniae praeses IV
422, 46 (*Ter. Heaut.* 452). **satrapae** praefecti Persarum IV 282,
7; V 146, 37; 242, 32. praefecti Persarum sunt V 330, 31. purpurati
Persarum IV 166, 36. praefecti Persarum aut Filistinorum (aut F. *om. a
c*) IV 167, 9. praepositi Persarum uel praefecti IV 564, 33.
**satrapes** iudices, honorati IV 389, 8. sapientes V 146, 32. *Cf.
Gell.* X 18, 2.

**Satrix** (satris *lib. gl.* satis *Buech.*) nunc nomen, nunc
participium V 146, 29; *lib. gl. Cf. schol. in Verg. Georg.* I 106.

**Satullum** saturum V 647, 23 (*Non.* 171, 12).

**Satullus sum** κεκόρεσμαι III 463, 15; 478 *adn.* 6. *Cf. Loewe GL.
N.* 228; *Buecheler Arch.* I 103.

**Satum** modium (*vel* modius) semis IV 282, 13. *Cf. Isid.* XVI 26,
11. *Adde* **sata** modius et dimidius V 389, 21. *Cf. Roensch Mus.
Rhen.* XXXI 454. **satum[en]** unum dimidium modium capit IV 564, 53.
*V.* satus.

**Satura** νόμος πολλὰ περιέχων II 179, 9. χορτασία II 478, 4 (*v.*
saties). κόρος II 353, 48. πλησμονή II 410, 8. δίσκος, τάγηνον II 520,
21. *V.* lex per saturam.

**Saturabile** χορταστικόν II 179, 18.

**Saturatim** κατακόρως II 341, 26.

**Saturatio** satietas χορτασία τροφῆς II 179, 10. χορτασία III 163, 54.

**Saturatus** χορτασία II 478, 4.

**Saturatus** κεχορτασμένος II 348, 46.

**Sature** *v.* brittola.

**Satureia** θύμβρα II 179, 13; III 476, 56. *Cf.* θύμβρα **satureia**
cunela III 430, 63. conela θύμβρα ipsa **satureia** II 119, 6 (*v.*
cunila). **satureia** θύμβρα (*vel* θρύμβα) III 16, 46; 88, 69; 186, 5;
265, 54; 317, 37; 359, 61; 463, 16; 472, 67; 522, 40; 629, 70.
**saturegia** θύμβρα III 596, 1. **satureia** colona (*v. supra*) III
589, 24. canola III 609, 55. thumus IV 389, 12. timus III 577, 53.
triabin (= thrymbin, thymbra?) III 630, 21. *Cf. v. Fischer-Benzon p.*
135. *V.* decreti cum, cunila.

**Satureia siluatica** estimo (epithymon?) III 582, 39. epitimo III 545,
44.

**Saturicus** (satricus *vel* satir.) morum descriptor (*vel* morus
discriptis) IV
