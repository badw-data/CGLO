(*cf.* crescione *Italorum*) ortensis III 593, 8. crison domesticus III
614, 56. **nasturcium** id est crisonus domesticus III 626, 66.
**nastorcius** crisonus III 570, 37. cardamomus uel cardamus id est
**nasturcius** siue crissonus ortensis III 581, 34. **nasturcio**
cardamomu III 537, 54. **nasturcio** enismo (ἐρύσιμον) III 545, 47.
erisinus id est cardamomus siue **nasturcius** III 582, 42.
**nasturcium** cressa saxonice V 312, 65. leccressae (*vel* tuuncressa,
*AS.*) V 374, 12. **nasturtium** est bufo (?) V 621, 46. *Cf. Diosc.* II
185. *V.* flos nasturtii, cortex cardamomi, cicer erraticum.

**Nasturcius hortulanus** damassonius III 610, 47. damasomus III 622,
73. **nasturgius** damosonius III 589, 37. *Cf. Dynam.* I 64; *v.*
*Fischer-Benzon p.* 103. *Cf.* damasoma costo ortenso (!) III 631, 59.

**Nasus** ῥώθων, μυκτήρ II 132, 35. ῥίς II 428, 24; 551, 6; III 412, 63.
ῥίν II 428, 13; 538, 55; 551, 6; III 503, 9. ῥίν, ῥώθων II 512, 26.
ῥώθων II 429, 9; III 12, 18; 85, 43; 175, 13; 349, 31; 394, 36; 457, 25;
471, 21; 486, 6. μυκτήρ III 247, 31; 350, 47. **nasum** μυκτήρ III 310,
35; 530, 11. *V.* adunco naso.

**Nasutus** ἐπίρρινος II 310, 38; III 252, 56. ἐπί\<ρ\>ριν\<ος\> III
180, 56; 329, 48; 519, 13. γρυπός II 265, 23. **nasuta** γρυπή II 265,
22. ῥύγχαινα II 428, 53.

**Nata\<bi\>libus** νηκτῶν III 422, 51.

**Natalicium (*vel* -us) munus** praemia natalis IV 122, 20; V 312,
45; 374, 48; 467, 34; 507, 46.

**Natalicius** γενεθλιακός II 262, 15. **natalicium** γενέθλιον III 495,
32. **natalicia** γενέθλια III 10, 31.

**Natalis** γενέθλιος II 132, 36; 262, 14; III 84, 8; 171, 65; 239, 45;
412, 31. **natale** γενέθλιον II 262, 16; III 74, 36; 130, 57 (natalis);
294, 68; 371, 63 (natalis); 512, 6. **natali** γενεθλίῳ III 130, 58.
**natalem** γενέθλιον III 457, 26. **natalia** γενέθλια III 457, 27;
481, 63. *V.* titulos ac natales.

**Natator** κολυμβητής II 352, 40; III 205, 39; 371, 34. **natatores**
κολυμβηταί III 412, 40.

**Natatoria** κολυμβήθρα II 352, 39; III 148, 3. piscina,
baptiste\<rion\> II 587, 59.

**Natibulum** latibulum, absconsorium V 467, 33; 507, 45. *V.*
latibulum.

**Natica** πυγή II 425, 63. *V.* natis, impuges, culus 1. *Cf. Loewe
Prodr.* 409.

**Natina** discordia IV *praef.* XVIII (*Mus. Rhen.* XL *p.* 326).

**Natinare** negotiare IV 367, 24. nego­tia[to]re V 544, 43. negotiari
*Scal.* V 605, 19. *Cf. Loewe Prodr.* 5; *Roensch Coll. phil. p.* 238.

**Natinator** (nitator *cod. inter* nabat *et* natina) seditiosus IV
*praef.* XVIII (*cf. Mus. Rhen.* XL *p.* 326; *Festus p.* 166, 30;
31).

**Natio** γένος, ἔθνος II 132, 42. γένος II 523, 35; III 131, 39. ἔθνος
II 284, 58; 498, 54; 545, 70. γενεά II 262, 11. γέννημα III 74, 33.
**nationes** ἔθνη II. 132, 39. *V.* indoles, per nationes, procul
positas nationes.

**Natis** γλουτός II 263, 30; 493, 46; 519, 44; 541, 13 (*GR. L.* I 553,
7); III 248, 62. **gnatis** ὁ γλουτός II 493, 16. **nates** πυγή (pyge)
III 86, 21; 349, 69; 394, 73; 412, 64; 502, 29. πυγαί II 132, 38; III
13, 11; 311, 57. κῶλα III 248, 55. pugme (πυγαί?) III 176, 13. naticae
IV 367, 23. **natis** et hae **nates**, naticae latinum non est IV 260,
39. **natium** nates hominum dicit, id est femora (*Vulg.* I *Reg.* 5, 6
*et* 12) V 225, 12. *V.* in natem.

**Natiuitas** φύσ[ε]ις II 132, 44 (*corr. e*). γέννησις II 262, 29.
γένεσις III 74, 37; 132, 7; 340, 57; 457, 28; 495, 31. γενεά II 262, 11.
γέννημα II 262, 28. γενεαλογία II 262, 12.

**Natiuum** ἰδιόχρωμον III 322, 59. ἰδιόχρωον III 322, 60. ἰδιόχροον II
330, 46; III 486, 19. τὸ γενικόν II 132, 45. ge­nerale, ingenuum IV 416,
46. genitiuum IV 122, 25; 260, 35 (genet.) naturale, genitiuum IV 540,
26; V 312, 40 (natium natura legentium); IV 122, 32 (*cf.* 31: natio
natura legenet, *v.* nactus). generalem, genetiuum IV 367, 27. ge­nerale,
geniti\<u\>um V 312, 41. **natiua** ἰδιόχρωμα II 132, 41. **natiuis
suis** τοῖς ἰδίοις γένεσιν II 132, 43.

**Natiuus color** naturalis II 587, 60.

**Nato[r]** κολυμβῶ II 352, 42 (*corr. a e*). **nato naro no** νήχομαι
II 376, 33. **nato** κολυμβῶ III 76, 12; 412, 33; 457, 29; 497, 73.
**natas** κολυμβᾷς III 412, 34. **natat** κολυμβᾷ II 132, 37; III 436,
29. **natamus** κολυμβῶμεν III 412, 35. **nata** κολύμβησον III 412, 32.
**natate** κολυμβήσατε III 412, 41. **natare** κολυμβῆσαι III 148, 2.
**natasti** ἐκολύμβησες (!) III 412, 37. **natauit** ἐκολύμβησεν III
412, 36. **natauimus** ἐκολυμβήσαμεν III 412, 38. **natauerunt**
ἐκολύμβησαν III 412, 39.

**Natricem explodit** serpentem excludit et expellit V 656, 32 (*gl.
Iuuenal.*).

**Natrix** βόα ἰχθύος εἶδος II 132, 40 (*ubi* ἔχιος εἶδος *Salmas. ad
Plin. p.* 87). serpens IV 260, 37; 367, 28; 540, 28; V 312, 38; 374, 25;
630, 1. **natrex**
