**Continuanda** iugiter, semper, perpetuo (reg. *Bened.* 41, 8) V 412,
49.

**Continuans** ετερων (ἐνείρων *Vulc.* ἐξ ἑτέρων *h*) II 114, 31.

**Continuatim** συναπτῶς II 444, 16. συνεχῶς II 445, 58. συνημμένως,
συνεχῶς II 114, 15; 41. incessanter V 448, 3. assidue, iugiter, inpensius
V 448, 2. *Cf.* tractimque iugiter **continuatim** II 199, 59.

**Continuatio** συνάφεια II 444, 32. συνέχεια II 445, 53. συνάφεια,
ἀνανέωσις II 114, 16. συνθεσία III 363, 15. ἐνδελεχίζει (continuat?) II
114, 33.

**Continuatis uerbis** V 660, 43 (*cf. GR. L.* VII 427, 22: *ubi*
coronatis).

**Continuatus** ἡνωμένος II 325, 25. συνηνωμένος II 446, 21. congressus
V 281, 55 (continatus?). **continuata** ἡνωμένα II 114, 34.

**Continue** adsidue, iugiter IV 324, 26.

**Continuo** συνάπτω III 443, 69; 483, 73. **continuat**
ἐνδ\<ελ\>εχίζει, συνάπτει, πυκνάζει II 114, 29. **continuat** [quanet
(quam et *e*) **continuate** dicimus] συνάπτει, πυκνάζει II 114, 17
(*cf.* 15). iungit IV 39, 4. frequentat, iungit, geminat IV 496, 46.
**continuant** porregunt IV 497, 11 (conlinuunt); V 279, 45
(conclinunt). **continuare** congeminare IV 496, 47; V 279, 48.
concurrere V 639, 70 (= *Non.* 93, 23). **continuauit** coniunxit V 350,
33. **continuatur** periuratur (perpetuatur? perduratur?), congregatur
IV 223, 10. coniungitur V 281, 4. obturatur (obd.?) V 495, 37. *V.*
continuatio.

**Continuo** statim εὐθέως, ἐξαῦτις II 114, 23. εὐθέως II 317, 19; III
443, 70. ἐξαῦτις III 3, 10. αὐτίκα II 251, 33. παραυτίκα II 397, 5.
παραχρῆμα II 397, 20. illico (*vel* ilico) IV 324, 29. significat
iugiter, tractim, perpetuo, sine fine, sine intermissione, alias statim
*Plac.* V 58, 29 (*cf. Verg. Georg.* I 60).

**Continuus** συναπτός II 444, 15. συνεχής II 445, 51. ferstud (? *AS.
cf. Gallée p.* 348) V 281, 64. **continuis** (-us *Loewe GL. N.* 113)
iugis IV 324, 28. **continua** ἀδιάλειπτος II 114, 32. διηνεκής III 423,
50. **continuum** σύνεγγυς II 444, 55. *V.* continuo.

**Contio** ἐκκλησία II 290, 38; III 443, 1; 484, 54. δημηγορία, ἐκκλησία
II 114, 25. δημηγορία II 269, 27 (*v.* denuntio). 1οcutio ad plebem II
575, 8. conuocatio populi IV 35, 35. conuentus populi IV 222, 9.
conuentus populi uel conuocatio IV 497, 2. conuentus populi uel
conuocatio populi V 447, 21. conuo\<ca\>tio populi uel conuersatus
(conuentus?) IV 324, 33. conuentio populi uel ecclesia, conuentum (!) V
280, 13. conuocatus populus V 540, 9. populi multitudo IV 44, 5; 497,
28. **contione** conuocatione populorum IV 497, 1. *Cf. Festus Pauli p.*
66, 6. *V.* in contione.

**Contio\<nu\>la** est congregatio V 618, 14.

**Contionarius** qui populum adloquitur IV 35, 37; 497, 5 (*add.* uel
conpellat); V 185, 9. qui adloquitur populum V 280, 38; IV 324, 30.

**Contionator** ἐκκλησιαστής *post* II 104, 40; 290, 39; III 520, 11.
δημηγόρος II 269, 26. *Cf.* II 575, 31. dux exercitum adloquens
(concionatur: *quo spectat* adloquitur *in bcd*) IV 35, 36. multitudinis
allocutor V 185, 10. **contionatorem** principem exercitus alloquentem V
185, 11. **contionatores** †errores (ubi non *tam* oratores *quam*
retores *reponi iubet Loewe GL. N.* 113) IV 324, 31; V 448, 7; 540,
16.

**Contionatus** δημηγορία II 510, 10. *Cf.* cocionatura.

**Contionor** ἐκκλησιάζω II 290, 40. δημηγορῶ II 114, 24; 269, 25
(contiono *cod.*). alloquor V 616, 37. **contionatur** ἐκκλησιάζει II
114, 22. ad cateruas fatur, ad turbas loquitur IV 497, 3. congreditur
(*v.* continantur), dum (uel?) in conuentu (*vel* -to) loquitur uel
contestatur IV 324, 32. alloquitur IV 222, 10. declamat uel iudicat IV
497, 4; V 279, 40 (deiudicat). eloquitur in populo V 281, 3.
**contionabantur** adloquebantur V 543, 23. **contionaretur** loqueretur
(*Euseb. eccl. hist.* II 10) V 419, 58 = 428, 44.

**Contiriaca** (contracta?) subcincta V 495, 35.

**Contius** orsius aut adlocutus IV 42, 39; 496, 58 (contionatus orsus
aut adlocutus?).

**Contollo** συνεπαίρω II 445, 24. **contollere** contra tollere V 638,
60 (= *Non.* 81, 36).

**Contor** *v.* contasti.

**Contorpet** stupet IV 324, 34; V 495, 39.

**Contorquet** inmittit et iaculatur, ἐμβάλλει II 114, 30. **contorsit**
emisit IV 45, 13; 500, 32. **contorquetur** in unum torquet (!) V 281,
5.

**Contorsit iaculum** iactauit lanceam IV 408, 43.

**Contortuosa** *v.* uersutia.

**Contostum** κάτοπτον τὸ πάνυ ὠπτημένον II 346, 29.

**Contra** ἀντί III 510, 20. ἐξ ἐναντίας II 302, 58. κατ' ἐναντίον II
345, 32. ἀπέναντι, ἄντικρυς II 114, 51. ἀντικρύ II 229, 51. ὑπ' ἐναντίον
II 463, 49. contrario IV 42, 6. e contrario IV 42, 23. aduersus IV 324,
35. significat aduerbium, ut 'contra faciens' *Plac.* V 58, 31.
