**Aorasia** uel **ausaria** quoddam genus caecitatis, quo quaedam
uidentur et quaedam non V 616, 4.

**Apage** abscede et uade *Plac.* V 4, 20 = V 48, 5; V 652, 36.
prouerbiale, uox aduersantis (auers. Duecli.) illa aduersus quae dicit V
440, 29. prouerbiale, uox aduersantis illa V 561, 27 (= auers.).

**Apage sis** abscede sic (*scr.* sis), sanus sis IV 19, 17; V 652, 37
(Ter. *Eum* 756: *cf. Schlee schol. T. p.* 46). graecum est et
significat dimitte, discede uel cessa V 657, 20 (= *Apul. de d.*
*Socr.* 5).

**Apage te** aperi te IV 19, 27; V 440, 42 (apagite abaetite *Nettleship
'Journ. of Phil'* XIX 115). **apax te** ductu (vel ductam) ἀπὸ τοῦ ἄπαγε
dictum *Plac.* V 6, 30 (*lemmate hausto contaminatione*) = V 48, 6.
*Cf. Kettner Hermae t.* VI 175, *Loewe GL. N.* 132, *qui pro* ductu
*probabiliter* duc te *proponit. Lemma ex* apagesis te *explicandum
vel* apage te.

**Apalum** ἁπαλόν III 315, 11 (*inter escas*). *Cf. W. Meyer-Luebke
'Idg. Anzeiger'* IV p. 61; *Ducange.*

**Apamoinama** Cappadocum lingua Nouember mensis dicitur V 167, 22
(*cf. Ideler* I *p.* 442).

**Apanteterium** ἀπαντητήριον II 233, 4.

**Apapores** ἀνακρεμνάδες III 430, 30 (*de oleribus*). *Cf. Isid.*
XVII 10, 16, *ubi* apopores *legitur* (*in quo vocabulo nescio quo modo*
ἀπ' ὀπώρας *latet*).

**Aparcias** septemtrionalis V 440, 46. septemtrio[n] IV 19, 33. *Cf.*
ἄρκος *pro* ἄρκτος. **aparchius** septentrio graece V 167, 23.

**Aparnesin** negatio V 338, 10.

**Aparthenum** *v.* abartenum.

**Ape** *v.* apud.

**Apegion** *v.* acorus.

**Apeliotes** (apoliten) subsolanus IV 19, 32; V 440, 45.

**Apellaeos** Macedonum lingua December mensis dicitur V 167, 24.

**Apellit** *v.* aspellit.

**Aper** σύαγρος II 21, 47; 439, 38; III 18, 42; 90, 54; 189, 29; 320,
14; 361, 37; 431, 24; 439, 51; 503, 44. κάπρος, σύαγρος, χλούνης,
μονιός, χαυλιόδους III 258, 61. **aprum** σύαγρον III 287, 56 = 658, 18.

**Aperio** ἀνοίγω II 228, 17; III 124, 34; 398, 22. **aperis** ἀνοίγεις
III 124, 35; 398, 23. **aperit** ἀνοίγει II 19, 39; III 124, 36; 398,
25. ostendit IV 207, 8; 477, 31. reuelat IV 309, 52. **aperimus**
ἀνοίγομεν III 124, 37; 398, 24. **aperitis** ἀνοίξατε (!) III 124, 39.
**aperiunt** ἀνοίγουσιν III 398, 26. **aperi** ἄνοιξον III 124, 37; 398,
21. **aperire** ἀνοῖξαι II 228, 20. **aperui** ἤνοιξα III 74, 70.
**aperuit** ἠνέωξεν II 19, 34. **aperuero** ostendero IV 477, 32.
**apertum est** ἠνοίχθη III 398, 27. *Cf.* **aperit** επικειρει II 18,
31 (*ubi sunt qui* appetit ἐπιχειρεῖ *commendent*).

**Aperire montes** ostendere IV 429, 49 (= *Verg. Aen.* III 206).

**Aperit Syrtes** nauigabiles facit inmisso \<mari\> IV 429, 50 (=
*Verg. Aen.* I 146: *cf. Servi*).

**Aperte** φανερῶς II 18, 38; 21, 50; 470, 2. ῥητῶς II 428, 4. σαφῶς II
430, 12. palam, dilucide IV 309, 53.

**Aperticius** aperiens V 346, 5.

**Apertilis** ἀνοικτός II 228, 19. apertus II 567, 15.

**Apertularius** θυρεπανοίκτης II 21, 48. θλάσμ\<α\> ἔχων ἵππος II 21,
49 (οὐλάς *c*). effractor IV 309, 51 (aperc. *codd. plerique*); V 590,
46. *V.* offractor.

**Apertura** ἄνοιξις II 228, 21.

**Apertus** ἀνεῳγώς II 226, 48. ἀνοικτός II 228, 19. ἐμφανής II 296, 57.
κατάδηλος II 340, 42. φανερός II 469, 54. **apertum** ἀνεῳγός II 226,
47. ἐμφανές II 296, 56. δῆλον II 269, 11. φανερόν II 469, 53. euidens,
patens IV 309, 55. **apertis** ἀνοιχθέντων III 508, 21. extensis,
diffusis IV 309, 54. *Cf.* αἴθριος **apertus**, serenus II 220, 25
(*ubi* apricus *e*) et *Festus Pauli p.* 2, 7.

**Apes** amorte V 267, 7 (ape amoue? a peste a morte *H.*).

**Apet** ἀποσοβεῖ II 21, 39; 46. **ape** κώλυσον II 21, 41. *Cf.*
*Festus Pauli p.* 22, 17. *V.* apse abigere.

**Apex** κεραία II 347, 56; 490, 50; 537, 19. κεραία, γράμματος ἄκρον,
προσῳδία καὶ τὸ ἐπὶ τοῦ πιλίου παράσημον II 19, 27. ἄκρον II 223, 61.
κορυφὴ ἀνθρώπου II 353, 54. summa pars litterarum uel summa pars capitis
IV 19, 22. summitas uerticis uel montis *post* IV 19, 29; V 167, 25.
cacumen aut summitas IV 20, 9. repaia (*h. e.* κεραία: *cf. Loewe
GL. N.* 110), summa pars cuiuscumque rei IV 310, 7. summa potestas uel
cacumen IV 19, 6; V 167, 26. summa pars flammae IV 429, 51 (*Verg.*
*Aen.* II 683). inproprie uirga pileo feminis (*h. e.* flaminis)
eminens IV 430, 1 (*Verg. Aen.* X 270; *cf. Servius*). uertex aut
summa potestas uel cacumen; interdum distinctionis nota IV 476, 4.
interdum distinctionis nota, interdum summa \<pars\> capitis uel
cuiuslibet rei V 267, 5. distinctio notae aut summa pars teli uel
cuiuscumque rei IV 207, 7. interdum distinctionis nota aut summa pars
capitis V 167, 27 interdum distinctionis
