**Fulmino** κεραννῶ II 348, 13; III 245, 10.

**Fultare** (?) pulchrare uel adorare V 296, 44 (**fucare** *et*
adornare *Hildebrand*).

**Fultus** ὀχυρωμένος (!) II 391, 17. auxiliatus IV 240, 5. sustentatus
IV 521, 4; V 552, 18 (*Verg. Ecl.* VI 53). **frutus** adiutus V 501,
53. **fulta** sustentata IV 79, 24. *V.* fulcitus, fuluus.

**Fuluida** rubea IV 78, 53; 520, 54; V 458, 7. *Cf. Arch.* III 135.
*V.* fuluus.

**Fuluo (fulgo *codd.*) tegmine** flaua pelle IV 443, 27 (*Verg.*
*Aen.* I 275).

**Fuluus** ξανθός II 74, 16. falbus (= flauus?), bellus (= heluus: *v.*
flauus) IV 345, 23 (*Loewe Prodr.* 422). est inter nigrum et rubicundum
V 619, 25. rubus (rufus?) V 297, 5. rufus IV 78, 48; 520, 49; V 105, 7.
**fulua** rufa IV 240, 19 (rubra *vel* rubea *Warren*). **fuluum**
rubicundum IV 240, 10; 520, 53. rubeum uel mundissimum ut aurum rufum
seu flauum V 458, 5. **fultum** rubeum uel mundissimum (*ubi* fultum
munitissimum *Nettleship 'Journ. of Phil.'* XIX 124) IV 80, 27.
**fuluum** rubeum uel flauum, aurum rubum (rufum?) V 296, 56. **folium**
flauum, rubeum V 457, 33. **fuluo** flauo aut rubeo IV 78, 46; 520, 48;
V 458, 6. flauo V 295, 62. *V.* fulgus.

**Fuma** terra IV 240, 21; 519, 64; V 296, 50; 501, 62 (*de* humus
*cogitavit Loewe Prodr.* 426, rura *Schlutter Arch.* X 192, funda retia
*coll. Serv. Georg.* I 141 *Warren: cf. Lindsay* 336, *ut alia mittam*).

**Fumantem** ad \<fumi\> similitudinem surgentem V 203, 27.

**Fumarium** καπνοδοχεῖον II 338, 40; III 353, 72. καπνοδόχη III 19, 55;
91, 48; 313, 60. καπνοδόχος III 365, 61; 245, 23. κάπνη II 530, 16.
καπνία II 503, 59. *Cf.* fimirium.

**Fumata** καπνιστά III 184, 21. fumo plena V 203, 28.

**Fumator** incensi peritus II 581, 7.

**Fumea** fumosa V 105, 8 (*Verg. Aen.* VI 593).

**Fumidus** igneus IV 79, 49; V 203, 29. **fumida** fumosa IV 240, 17; V
501, 63 (*Verg. Aen.* VII 76).

**Fumigata** καπνιστά III 254, 66 (*gl. vetusta ?*).

**Fumigo** καπνίζω II 338, 39; III 76, 5; 150, 68. nebula turbo V 501,
61.

**Fumo** καπνίζω II 338, 39. **fum\<o\>** τύφομαι II 461, 14.

**Fumosus** καπνώδης II 338, 43.

**Fumus** καπνός II 74, 17; 338, 41 (singulariter tantum declinabitur);
III 76, 6; 150, 69; 245, 22; 449, 41; 469, 32; 496, 58. *V.* sine fumo.

**Funaie** λαμπάδιον II 74, 19. **funalium** ἐλλύχνιον II 295, 36.
**funalia** stuppea candelabra V 502, 2. stuppe uel candelabre V 458,
12. lignalia (lychn.?) V 203, 30. in modum funalium candelae intortae V
203, 31. candelae in modum funium intortae V 203, 32. sunt cerei V 619,
44. candelae uel uncini ad (ellychnia, *Buech.*) cereos V 634, 54. *Cf.
Serv. in Aen.* I 727.

**Funambulus** σχοινοβάτης II 450, 17. σκαλοβάτης II 432, 31. καλοβάτης,
σχοινοβάτης II 337, 39. νευροβάτης, σχοινοβάτης III 240, 13 (funiamb.).
**funabulum** (!) νευροβάτης III 172, 43. **funambuli** καλοβάται II 74,
18 (*cf. mrg.*). *V.* schoenobatos.

**Functa** κτέρεα III 449, 46. **functe** ισερια (= κτέρεα) III 481, 44
(funera *Cuiacius*). funesta καίρια *Buech.* funale κηρία *H.*

**Functio** συντέλεια III 449, 42; 481, 31. λειτουργία II 361, 40.
λειτουργία[ς], τέλεσμα II 74, 21. exsolutio tributorum IV 79, 28; 240,
29; 345, 29; 520, 5; V 297, 21; 458, 9; 501, 64. **functione**
ministerio IV 240, 28. **functionum** λειτουργιῶν, εἰσφορῶν II 74, 22.

**Functoria** transitoria IV 519, 62 (fructoria); V 203, 37; 296, 30
(frunctoria); 43 (frinctoria). *Cf. Loewe Prodr.* 386, def., perf.

**Functurus** fruiturus IV 520, 6. *V.* fructurus.

**Functus** τυχών II 74, 20. usus [uel] ministerio IV 80, 14.
ministrans IV 240, 18; 520, 8. gerens, agens, usus uel consecutus IV 79,
2. usus uel ministerium (*cf. Serv. in Aen.* VI 83 *et Ter. Heaut.*
580), seruiens IV 345, 30. agens, gerens IV 345, 31. liberatus V 360,
14.

**Functus curribus (?)** agens V 297, 4.

**Funda** σφενδόνη II 74, 23; 449, 24. fundibalum IV 79, 51; V 203, 33
(*Verg. Georg.* I 141). retia, linea seu fundibalum V 458, 11. retia,
linea seu fundibula V 501, 65. retia, linea et fundib\<al\>us V 297, 30.
*V.* fuma, iaculum, neutrale.

**Fundamen** fundamentum IV 519, 56. *V.* basis.

**Fundamentum** θεμέλιος *post* II 74, 6; II 327, 22; 500, 25; 526, 23;
543, 47; III 19, 29; 91, 21; 190, 18; 406, 69; 449, 43. θεμέλιον III
268, 28. **fundamenta** θεμέλιοι III 312, 35; 522, 33. θεμέλια III 364,
78; 504, 44.

**Fundanus** rusticus qui fundum colit V 502, 1. rusticus qui fundos
colit IV 240, 34. **fundana** (*ex* -nas: *scr.* fundanus) qui pensiones
colit V 502, 3. *V.* fundus.

**Fundat humo** deicit (!) terrae IV 443, 30 (*cf. Verg. Aen.* I 193).
prosternat, occidat IV 79, 29; 519, 57.
