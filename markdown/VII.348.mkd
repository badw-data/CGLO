**Theoreticen** et **practicen** contemplatiuum et actiuum V 417, 15
(*cf. Hieron. in Matth.* 21, 1 θεωρητικὸν καὶ ἐργαστικόν).

**Theoria** consideratio (*vel* contemplatio) IV 291, 9. **theoriae**
(theorice *cod.*) supernus intellectus V 417, 54 (*Cassian. inst.* XI
18). *V.* in theoria.

**Theorias** (theoricus *Mai* VII 583) ad diuinitatem extendens IV 183,
19 (tendians *cod. Vat.* 3321: *cf. Landgraf Arch.* IX 438). **tebrias**
ad diuinitatem IV 573, 21 (extendens *add. b.*). *Cf.* **theorica**
intellectus ad diuinitatem se extendens *Papias.*

**Theorica** contemplatiua V 395, 32.

**Theorisma** diligens uerborum conscriptor (-ptio?) IV 574, 16.

**Theos** deus est, **atheus** (a theo *R*) dicitur qui caret deo
*Plac.* V 42, 18 = V 101, 26 (caret deum). **theos** deus V 157, 1.

**Theosebia** (*vel* theus.) sapientia IV 291, 12.

**Theotecnus** (theotegnes *cod.*) dei filius III 504, 62.

**Theriaca** (thyriacae *codd.*) medicinae ignitae V 395, 26 (*cf.*
*Aldhelm. de laud. virg.* XIII *p.* 14, 9?).

**Therion:** graece θηρίον, latine fera, inde theriaca, quod feraliter
occidit V 526, 25 (*GR. L.* VII 292, 13).

**T\<h\>eristrum** mauortium, quod (*vel* quo) mulieres Arabiae utuntur
IV 291, 31 (*cf. Isid.* XIX 25, 6). mauortium IV 574, 11. mafortia V
398, 19 (teretrum *cod.*). mauortia V 545, 51 (theretrum). ligatio
capitis V 395, 25. amictum V 486, 11. uestis suptilis et pulcra uel
facciolus (*cf. Diez* II^a^ fazzuola) V 516, 40. *Cf. Arch.* IX *p.*
437; *Roensch Coll. phil. p.* 282.

**Thermae** θερμαί III 306, 13; 522, 25. γυμνάσιον III 132, 23; 196, 47.
**tenuas** calores V 395, 13. pro calore quod calor \<ther\>mas (θερμόν)
dicitur *gloss. Werth. Gallée* 343 (*v. suppl.*). id est casa ampla,
rotunda uel quadrata, habens deforis fornaces XII ad calificandum domum,
istus (*scr.* intus) pauimentum de marmore constructum et piscinas XI,
contra hanc (?) fornaciorem (fornacem maiorem?), ut per plumbeas
fistulas accipiant aquam, quod quaedam calidae sunt ualde, quaedam
frigidae *gloss. Werth. Gallée* 344 (*v. suppl.*). *Cf.* **thermon**
calda graece (*sc.* ὕδωρ aqua) V 157, 3. 'στο δημόσιον **ad thermas**
III 216, 25 = 231, 54 = 651, 10. *V.* balneum.

**Thermodontem** flumen Cappadociae V 581, 18.

**Thermopylae** cryptae balneariae (*vertit* θερμοπωλεῖον) V 516, 50;
581, 19. **Termopilas** faestin (*vel* festin = *Festung, AS.*) uel
anstigan (*vel* anstiga, *AS.*) V 397, 22 (*cf. Oros.* IV 20, 20). *V.*
teritigo.

**Thesaurus** *v.* thensaurum.

**Thesis** positio graece *Plac.* V 101, 27.

**Thesmophoria** legislatio *cod. Epin. post* V 395, 16.

**Thessali** *v.* Graecus.

**Thetis (Tethys?) et Clymene** nymphae sunt IV 424, 45.

**Thex (?)** secutor V 527, 5. Threx? (*cf. schol. in Hor. sat.* II 6,
44).

**Thia** uel **amita** soror patris V 395, 12. **thia tia** soror patris
aut matris V 581, 31. **thia** matertera V 396, 14 = 418, 57 = 427, 25
(? *Euseb. eccl. hist.* X 6 θείᾳ). graece, amita latine [senex pater he
dicuntur extranea] *gloss. Werth. Gallée* 344 (*v. suppl.*).

**Thiasus** chorus Liberi patris IV 291, 52; V 581, 26. **thiasum**
sacrum V 581, 27. sacra IV 291, 53. **thiasis** laudes uirginum IV 184,
17; 292, 2; 397, 12; 574, 19; V 395, 56 (*gl. Werth. Gallée* 345).
**thiasos** saltationes, choreas V 581, 34. *Cf. Serv. in Ecl.* V 30;
*schol. Bern. in* eundem locum (thiasi coetus uirginum).

**Thinx** (*Bruckner 'Spr. d. Longob.' p.* 212) donatio V 581, 36.

**Tholus** tectum de petris sine ligno V 422, 54 (*Clem. Rom. rec.* VIII
18). **tholum** fastigium templi rotundi IV 292, 46 (*cf. Isid.* XI 1,
111). culmen tecti IV 185, 9; 575, 6; V 516, 59. signum rotundum quae
(!) super culmen domus ponitur IV 292, 44. **tolus** est camberata domus
(*cf. Landgraf Arch.* IX 437) uel locus quo trabes coeunt V 624, 22
(*cf. Serv. in Aen.* IX 406; *Isid.* XIX 19, 6; XI 1, 111). summum in
nauali malo fastigium, ad quod ueli funes trahuntur, quamuis tolus
rotunditas illa quae in rotundis contignationibus media ponitur et in
papilionibus eminet[ur] dici possit II *p.* XIII.

**Thomas** abyssus uel geminus IV 293, 2 (*cf. Isid.* VII 9, 16).

**T\<h\>oracata acies** cum toracibus ambulantes: toraces enim dicuntur
genera munimentorum quae portant *gloss. Werth. Gallée* 356 (*v.*
*suppl.*).

**Thoracicla** sculptae imagines V 396, 1 (*cf. Aldhelm. de laud.
virginit.* XXXVIII *p.* 51). sculpta imago pectoralis usque ad pectus in
auro uel in quolibet, deriuata a torace, quod est pectus V 624, 27.

**Thoragium** *v.* choragium (IV 397, 9 etc.).

**Thorax** (*vel* torax) θώραξ III 311, 24; 522, 31. facies IV 424, 48;
V 545, 52. lorica IV 185, 11; 293, 19; 575, 7. militare munimentum IV
185, 41; V 486, 33. militare munimentum, hoc est lorica IV
