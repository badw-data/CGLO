109, 51/53 = 639, 5; 165, 1; 402, 65. **computate** ψηψίσατε III 402,
72. **com­putare** ψηφίσαι III 165, 3. **computaui** ἐψήφισα III 402, 71.
**computasti** ἐψήφισες (!) III 402, 70. **computauimus** ἐψηφίσαμεν III
402, 74.

**Computus** ψῆφος III 81, 11; 164, 66; 402, 75. consiliis (calculus?
computis?) V 495, 11. *V.* calculus.

**Con** σύν II 106, 32; 443, 33.

**Conabulum** *v.* cunabulum.

**Conadunare** *v.* coaduno.

**Conamen** ἐγχείρημα II 284, 18. **conamine** (*vel* -na) librorum
incipientia V 182, 16.

**Conatus** ἐγχείρησις II 284, 19. ἐπιχείρησις II 312, 60; 488, 11; 510,
28; 537, 29; 549, 44; III 442, 81. uoluntas V 280, 9. uoluptas (!) IV
497, 32. notus (motus *b*), impetus, temptatus IV 497, 33. temptatus,
adgressus IV 223, 7.

**Conatus** ἐπιχειρήσας II 106, 33,

**Conatus ibi dare bracchia collo** uirtute magna amplexa retinere
(uirtute magna dare brachia amplexa tenere *codd. cf. Verg. Aen.* II
792; VI 700) V 182, 17.

**Concabisto** concludito V 182, 18 (concaueato? concapito *H.*
conclauato *Buech.*).

**Concacasti** κατέχεσες(!) III 402, 62. **concacauit** κατέχεσεν III
402, 63. *Cf.* **concacius** enchesten (concacatum συγχεσθέν?) III 402,
64.

**Concado** *v.* concido.

**Concaleo** *v.* conculco.

**Concalfacere** pro califacere V 639, 61 (*Non.* 92, 14).

**Concal\<l\>uit** incal\<l\>uit V 639, 43 (*Non.* 90, 1). *V.*
congeluit.

**Concaluit** exardescit V 627, 25.

**Concambiat** *v.* cambio.

**Concameratio** fornix, transuolutio V 182, 20. *Cf. AHD. GL.* III 376.

**Concameratum** curuum, quasi conuexum (conuersum *codd.*) seu
inclinatum, ad modum circuli flexum *Plac.* V 57, 2.

**Concapito, concapsit** *v.* concipio.

**Concaptiuus** συναιχμάλωτος II 106, 48.

**Concauae** [poples] ἀγκύλαι III 13, 20 (*om. ab*).

**Concedens** adquiescens, migrans IV 43, 15.

**Concedentia** συγχώρησις II 441, 6.

**Concedo** συγχωρῶ II 441, 7. παραχωρῶ II 397, 25. **concedit**
συγχωρεῖ, παραχωρεῖ II 106, 52. **concede** transi IV 41, 8 (*Ter.*
*Eun.* 706). **concedite** εἴξετε (!) III 147, 24. **concedam** lytisna
(*AS., beinahe*) V 350, 49. **concesserim**, arectae (*AS.*) V 350, 54.
**concessum est** συνκεχώρηται II 106, 40.

**Concelebro** συνεορτάζω II 445, 22. **concelebrat** συνεορτάζει II
106, 55.

**Concena** σύνδειπνος II 444, 38. **concenae** σύνδειπνοι II 106, 41.
*Cf. Loewe Prodr.* 330. *V.* conuiua.

**Concenturiat** instruit, ordinat: dictum a centurionibus, qui milites
ordinant *Plac.* V 15, 8 = V 57, 3. *Cf. Plaut. Trin.* 1002.

**Concentus** συνῳδία II 448, 43; 488, 6 (conuentus); 537, 35.
συν\<ῳδία\>, ᾠδῆς σύγκρασις, σύνψαλμα II 106, 49 (*suppl. Buech.*). ἡ
συμφωνία ἢ συνῳδία II 549, 50. συμφωνία II 443, 23. σύνψαλμα II 106, 49
(*mrg.*); 448, 40; 491, 13 (concantus). simul cantus IV 44, 10; 498, 41.
multorum cantus V 280, 58. simul se colligentes IV 321, 19 (conuentus
*Loewe GL. N.* 114). simul recolligunt (simul se colligunt?) V 540, 12.
uocis est, **conuentus** corporis V 182, 24. **coucentum** quod hinc et
inde canitur IV 496, 53; V 279, 38. organa IV 43, 28. *V.* concitus.

**Concepta** συγκειμένη II 440, 11. **conceptum** ὑποδεχθέν II 106, 39.
**concepta** νενομισμένα, εἰλημμένα II 106, 56. *V.* conceptus.

**Conceptio** συνθήκη II 446, 32. ἀνδροληψία II 225, 20. σύλληψις
γυναικός II 441, 32. *V.* mentis conceptio.

**Concepto** silamo III 576, 29 (conceptus σύλλημψις?).

**Conceptus** σύλλημψις II 510, 34. σύνλημψις, αἵρεσις καὶ συλληφθείς II
106, 50. syllemses II *p.* XXXVII.

**Concerno** συνθεωρῶ II 446, 31.

**Concerto** συναγωνίζομαι II 443, 40. **concertat** συναγωνίζεται II
106, 51. contendit, confligit IV 321, 20. *V.* confligo.

**Concessa** συνηγμένα II 106, 54 (= II 109, 15). *V.* congestum.

**Concessio** συγχώρησις II 441, 6. *Cf.* **concessius** συγχώρησις II
106, 53 (concessio *e.* concessus?). est cum reus non id quod factum est
defendit, sed ut ignoscatur postulat, quod nos ad paenitentes probabimus
pertinere V 182, 25 (*Isid.* II 5, 6).

**Concessurum** *v.* contersurum.

**Concessus** *v.* concessio.

**Concessus a deo** missus a deo IV 435, 2.

**Concha** (*vel* conca) κόγχη II 107, 16; 351, 32; III 22, 52; 70, 5 =
637, 2 (cuncha); 93, 63 (*v.* colum); 203, 31; 318, 30; 325, 30; 376,
62; 497, 65; 526, 59. κόχλος II 354, 37. genus uasis II 574, 36. coclea
IV 496, 57; V 276, 39; 279, 46; 543, 22 (clocea); 566, 37; 627, 26.
bucit (= bucina) V 446, 67. ostrea animal in ponto quo tinguitur purpura
V 566, 13. dicitur a Graecis
