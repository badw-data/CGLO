massagiune (*Diosc. lat.* de ecino terreno ·i· nassaione *confert
Stadler*) III 538, 51.

**Ericius** ἐχῖνος II 62, 35; 92, 52 (iricius); 321, 36 (*item*); III
18, 51; 90, 64; 189, 37; 259, 30; 320, 46; 361, 65 (ir.); 431, 54; 494,
9; 518, 52; 520, 8; 562, 23. *Cf.* systrix (ὕστριξ?) **ericio** III 189,
39. **ericii** idem et echini V 194, 24 (*Is.* XII 6, 57).

**Erictat** (*vel* -tit) custodit V 194, 25 (seruat?).

**Eridanus** Ἠριδανός III 241, 54. fluuius IV 65, 6; 410, 37; 511, 48.
qui et Padus, fluuius Galliae cisalpinae (*cf. Serv. in Aen.* VI 659; X
189; *Georg.* I 482; IV 371; *Isid.* XIII 21, 26) V 551, 50. **Eridanus
fluuius** Ἠριδανὸς ποταμός III 293, 39; 516, 65. *V.* eurus.

**Erigo** ὀρθῶ II 386, 43; III 78, 32; 151, 45. ἀνορθῶ II 228, 31.
ὑπορθῶ II 467, 36. **elego** ἀφιδρύω II 253, 15 (*ubi* eligo *a*, erigo
*e.* eloco *H.*). **erigis** ὀρθοῖς III 151, 46. **erigit** ὀρθοῖ III
151, 47; 447, 21. adtollit IV 336, 47. **eregi** (!) ὤρθωσα III 81, 38.
*V.* frigeo.

**Eriles lectos** dominorum lectus V 194, 30.

**Erilis** δεσποτικός II 268, 49. δεσπόσυνος II 268, 50. dominicus II
578, 48 (dominicalis *b*); V 194, 31. filius dominicus V 194, 27
(erile), dominicus, nonus (?) V 194, 29. **erile** δεσποτικόν II 62, 36;
535, 56 (*cf. post* II 68, 36). dominicum IV 64, 41; 511, 52; V 108, 22;
194, 26; 459, 27. dominicum, id est ut ualde ut uis (?) V 453, 49.
**eriles** domini\<ci?\> V 194, 28. *V.* eruli.

**Erilius** inferius V 498, 59 (Erebus infernus? uilius? exilius
*Buech.*).

**Erimio** hindbrere (= hindberie, *AS.*) V 357, 35. *V.* acinus.

**Erimit** apparat V 498, 60 (erigit?).

**Erinaceus** χοιρογρύλ\<λ\>ιος ἤτοι ἐχῖ­νος χερσαῖος II 477, 45.
χοιρόγρυλ\<λ\>ος III 431, 44 (*add. David*). **irinaceus** κτεὶς
κναφικός II 355, 60. *Cf. GR. L.* V 578, 7. **herenacis** aliud genus
lepor\<is\> V 300, 22 (*cf. Salmas. Plin. ex.* 391).

**Erinys** furia [egit excludit] IV 440, 37 (*Verg. Aen.* II 337).
furia V 290, 45. phuria IV 512, 34. furia, ira magna IV 232, 48.
furia[e] IV 410, 39. **Erunis** furiae †recuse V 453, 55. furiis V
498, 66. **Erinae** Ἐριννύες III 237, 31 (*fortasse novicia*).

**Eripio** ἀφαρπάζω II 252, 33. ῥύομαι II 428, 61. **eripit** ἁρπάζει II
64, 37. abripit, proripit IV 336, 48. **eripe** ῥῦσαι II 429, 4.
**eripuit** abstulit IV 336, 49.

**Eritio** *v.* famulatio. **Eritium** *v.* era.

**Eritudo** δεσποτεία II 268, 48; III 447, 22; 480, 67. dominatio II
578, 47; *Plac.* V 21, 3 = V 65, 23. *Cf. Festus Pauli p.* 83, 1 *et
praef.* V *p. V.*

**Ermana** *v.* aerumna.

**Ermeos** Bithyniensium lingua Nouember mensis dicitur V 194, 36. *V.*
menses.

**Ermius** Tucorem (*id e.* Tuscorum) lingua Augustus mensis dicitur V
194, 37. *V.* menses.

**Ero** *v.* aero, culleus, eruum.

**Erodion** animal IV 514, 12; V 290, 33 auis maior qui (!) etiam
aquilam prendit V 498, 63. **horodius** uualhhaebuc (*vel* uualhhebuc,
*AS.*) V 365, 11. *Cf. AHD. GL.* I 496, 34. *V.* ardea.

**Erogatio** ἐξοδιασμός II 303, 46; 492, 68. ἐξοδιασμός, ἀνάλωμα II 494,
62. ἔξοδος τὸ ἀνάλωμα II 303, 50. ἀνάλωμα II 545, 30. dispensatio *b
ante* II 578, 48. *V.* agape.

**Erogator** *v.* prorogator.

**Erogitat** exquirit V 498, 62.

**Erogo** ἐξοδιάζω II 303, 47. δαπανῶ **erolo** (erogo?) III 134, 35.
**erogare** ἐξοδιάζειν II 62, 40. *V.* tripertit.

**Erola** tapspleus(?) III 578, 66.

**Erotema** (-eria *cod.*) graece interrogatio V 359, 27.

**Erpica** egdae (*AS.*) V 359, 47 (*v.* hirpices; *AHD. GL.* III 273,
28).

**Erpicarius** egderi (*AS.*) V 359, 48 (*cf. AHD. GL.* III 719, 58).

**Errabundus** errans V 640, 71 (*Non.* 103, 10). *Cf. Serv. in Ecl.* VI
58.

**Errans** πλανήτης II 408, 40. gens (uagans? egens?) V 290, 41.
**errantem** feras uestigantem IV 440, 31 (*Verg. Aen.* VII 493).
uagantem IV 65, 13. **errantes** pascentes IV 440, 35 (*Verg. Aen.* I
185).

**Errantem lunam** uices mutantem uel quod inter planetas sit, id est
errantem (-tes?) IV 440, 32 (*Verg. Aen.* I 742).

**Errata** errores, ut illud: et sua errata emendent V 194, 11.

**Erraticus** ἀλήτης II 224, 66. πλάνος II 408, 41. **erratici**
πλανῆται II 408, 39. **erraticae** erroneae V 194, 12. περὶ τῶν ζ
ἀστέρων πλανητῶν de septem (!) stellis **erraticis** III 292, 46 + 47.

**Erro** πλανῶ II 408, 42. πλανῶμαι III 154, 46. πλάζομαι II 408, 32.
**erras** πλανᾶσαι III 154, 47. **errat** πλανᾶται II 62, 41. **error**
πλανῶμαι II 408, 43. ῥέμβομαι II 427, 44. *Cf. Hagen Grad. ad cr.* 13.

**Erroneus** ἀλήτης II 224, 66. πλάνος II 408, 41. **erronei** errantes
IV 65, 17; V 628, 39. **erranei** errantes IV 508, 50; V 634, 8; 598, 64
(*cf. Arch.* III 132; *Fulg.* 141, 20 *Helm*).

**Erronicus** πλάνος III 335, 49.

**Error** πλάνη II 408, 38; 489, 71; 510, 51; 536, 8; 538, 3; 550, 16;
III
