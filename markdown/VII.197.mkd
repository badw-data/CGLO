**Remorator** παρελκυστής II 397, 44.

**Remoratrix** βραδεῖα II 259, 51. παρελκύστρα (!) II 397, 45. *V.*
remeligines. *Cf. Loewe Prodr.* 263.

**Remordet** sollicitat, hoc est cruciat IV 163, 50. sollicitat, laedit
IV 461, 49 (*Verg. Aen.* I 261; VII 402). laedit uel occultanter
conscientiam pungit IV 386, 10.

**Remoror** βραδύνω II 259, 53. **remoro** (-or *a*) παρέλκω II 397, 46.
**remorer** inpediam V 539, 14 (*Ter. Andr.* 739). **remorata est**
ἐπέσχεν II 172, 7.

**Remorum rasor** κωποξύστης III 308, 28; 498, 6; 525, 52.

**Remo[uea]tio** ἀποκίνησις II 237, 36 (*corr. a e: nisi forma
vulgaris sub est*).

**Remotum** sublatum IV 562, 54. absconsum, latens IV 386, 11. *sine
interpr. Scal.* V 609, 9. **remota** ablata V 414, 32 (*reg. Bened.*
39, 12). framadoenre (*dat. sing., AS.*) V 387, 12. **remotae** de medio
ablatae V 142, 56. iterum positas (! *contam. cum* repostas?) IV 562,
55. **remotius** longius V 328, 52. **remotiora** secretiora IV 278, 58;
562, 53. *Cf.* [remodera] **remotiora** secretiora V 512, 44 (rege
modera *H.*).

**Remoueo** ἀποκινῶ II 237, 37. παρακινῶ II 395, 11. **remoue**
ἀπόστησον II 172, 8. remobuit *v.* reuellit.

**Rempha** Lucifer uel iubar V 386, 55 (*cf. Roensch Mus. Rhen.* XXXI
462).

**Remugit** clamat IV 278, 57. **remu­giunt** [rapidus aestus feruens]
V 240, 23.

**Remulcant** (remulcunt *codd.*) cum scapha nauem ducunt IV 279, 1; V
577, 44. cum scapha ad (?) nauem perducunt V 512, 49. quid sit
**remulcare** V 651, 23 (*cf. Non.* 57, 20; *Loewe GL. N.* 169;
*Nettleship 'Contr.' p.* 574; *Stowasser Arch.* I 440).

**Remulcens** replacans IV 162, 28 (replicans *a: cf. Verg. Aen.* XI
812). replacans IV 561, 51; V 479, 22. rem placans V 240, 24.
repropitians V 240, 25. reducens V 240, 26 (*cf.* **remulgens** reducens
V 240, 28). **remolens** reducens V 479, 16; 512, 45. replicans uel
reducens V 142, 52.

**Remulceri** replicari V 240, 27.

**Remulco** scapha V 577, 43. *Cf. Festus Pauli p.* 279, 1.

**Remulus** *v.* Remolus.

**Remuneratio** ἀντίδωρον II 229, 32.

**Remunero** ἀνταμείβομαι II 172, 10; 228, 45. **remunerat** ἀντιτιμᾷ II
172, 9.

**Remunerare humanitatem tuam** V 664, 22.

**Remus** κώπη II 172, 11; 357, 49; 489, 32; 513, 12; 539, 12; 551, 35;
III 354, 45; 355, 5; 396, 19; 472, 40; 497, 74. est lorum quod continet
tubam V 623, 11. **remi** κῶπαι III 29, 22; 205, 10; 297, 8; 434, 8;
524, 62. tonsae IV 386, 7. *V.* remorum rasor. *v.*

**Ren** *in hac gl. est:* **rien** et **ren** singulare, renum II
*praef. p.* XII. **rien rienis** νεφρός II 376, 2 (ren renis *e*).
**rien** lendino (*AS.*) V 386, 49. **renis** νεφρός II 501, 62; III
570, 12. **renes** νεφρός II 172, 14. **renis** νεφροί II 172, 15.
**renes** νεφροί III 13, 17; 86, 25; 176, 54; 248, 2; 311, 40; 349, 67;
394, 71; 416, 36. *Cf. GR. L.* I 38, 7; 541, 24.

**Renale** περίζωμα III 193, 24 (*v.* uentralis). *Cf. Isid.* XIX 22,
25.

**Renascentia** recidiua IV 386, 12. *Cf.* III 292, 19 (**noluit
renasci** ἔθελεν ἀναγεννᾶσθαι).

**Renatio** παλιγγενεσία II 392, 48.

**Rendis** *v.* reses.

**Renego** *v.* renuo.

**Reniculus** lendibreda saxonice V 329, 7.

**Renidenti uultu** interdum laeto et hilari, interdum splendenti, item
florenti, si dicamus: terra renidenti *Plac.* V 40, 2 = V 97, 1=V 144,
7.

**Renidet** redolet IV 279, 6; 386, 14. olet V 328, 9; 46. splendet aut
olet (*vel* tollet) IV 162, 32. splendet aut [c]olet IV 561, 52
(*corr. b*). **renidit** splendit IV 162, 30; V 142, 58. **reniduit**
ἐμειδίασεν II 172, 13. *V.* renitet.

**Renimentum** *v.* reuimentum.

**Renĭtenti** fulgenti IV 422, 2.

**Renītentibus** resistentibus IV 279, 5.

**Renitet** (-det *e*) μειδιᾷ, ἀντιλάμπει II 172, 12. *V.* renidet.
**renitet** resplendet IV 279, 2.

**Renitiosus** νεφριτικός II 172, 16 (reniosus?).

**Renitor** ἀντεξεργάζομαι II 228, 58. ἀντεπερείδομαι II 228, 63.
ἀντιβιάζομαι II 229, 12. **renititur** e contra conatur IV 386, 15.
**renitite** reclinate V 327, 47. **rennitere** (rennuere?) recusare V
143, 1.

**Renium ualitudo** νεφρῖτις III 363, 43.

**Rennuo** *v.* renuo.

**Renocendi (?)** bos siluester V 388, 6. *Cf.* rhinoceron.

**Renodo** διαλύω II 272, 33. ἐκλύω II 291, 37.

**Renones** uestes de pellibus V 387, 34; 512, 51.

**Renosus** νεφριτικός II 376, 1; III 206, 55 (nefricon). **rienosus**
νεφριτικός (*cf. margo*) II 174, 43.

**Renouat** redintegrat, restaurat IV 386, 16. *V.* rem famil. *etc.*

**Renouatiuum** παραγωγόν II 394, 28 (denominatum *e.* denominatiuum
*vel* declinatiuum *ap. Labb.*).
