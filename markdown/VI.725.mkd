**Nanctus occasione\<m\>** adeptus IV 122, 9. adeptus uel poti\<t\>us V
312, 36.

**Nandi** natandi *Plac.* V 35, 3 = V 86, 6 = V 119, 17; IV 122, 6; 540,
12. **nando** natando IV 260, 21; V 225, 5; 312, 60.

**Nanfurae** quod medici naptam (napton *vel* naptan *G*) uocant *Plac.*
V 33, 33 = V 86, 5 (namf.). *Cf. Keller 'Volkset.' p.* 99, *Festus p.*
169, 22.

**Nantes** natantes IV 122, 7; 454, 1 (*Verg. Aen.* I 118); V 312, 43.
nantes duos IV 540, 13. natantes aut nauigantes V 119, 15. **nantes**
[natae filiae] natantes IV 122, 26. *Cf. Verg. Aen.* I 118.

**Nanus** νᾶνος III 180, 60. νάννος III 253, 2. uel **pumilio** duerg
(*AS.*) V 374, 38. *V.* humiliamanus, burichus. *Cf. Loewe Prodr.* 355.

**Naophylax** templi custus graece V 119, 11.

**Naos** templum V 119, 12.

**Napaeae** deae florum agrestium, sicut Naiadas (*vel* -des) Veneris,
Oreades Dianae, ut Donatus V 225, 6 (*Verg. Ge­org.* IV 535). *V.*
nympha.

**Napta** purgamenta lini uel cuiuslibet rei. Sallustius scribit in
istoriis (*vel* storiis: *cf. Maurenbr.* IV 61) quod nabta genus sit
fomitis aput Persas, quo uel maxime nutriuntur incendia: alii ossa
oliuarum qui (!) proiciuntur cum amurca arefacta naptam appellari
putant, unde et Graece πυρὴν[η] dicitur ab eo quod [est] πῦρ, id est
ignem nutriat V 225, 7. *Cf. Hieron. in Dan.* III 46. est genus fomitis
apud Persas quo incendia aluntur V 621, 44. genus fomitis apud Persas
quo uel maxime nutriantur incendia V 312, 59. genus fomenti (!), id est
tyndir (*AS.*) V 374, 31. blaecteru (*AS.*) V 374, 14 (*AHD. GL.* I 656,
16). *Cf. GR. L.* IV 22, 22.

**Napus** collis [nam graece enim] si\<l\>ua nemus II 587, 62 + 63
(*cf. v. d. Vliet Arch.* IX *p.* 303).

**Napus** βουνιάς II 132, 24 (βουνεας *cod.*); 259, 29; 373, 30
(μουνιάς); III 359, 47; 490, 67. boinion (βούνιον?) III 554, 26; 618,
54. rapus III 575, 33. **napi** βουνιάδες III 16, 29; 88, 51; 317, 3;
359, 19; 397, 62; 413, 2; 430, 42; 511, 33. **napos** βουνιάδες III 185,
43. *Cf.* gugilis (γογγυλίς) id est rapa, id est **napo maiore** III
539, 25. b \*\* iades isilia III 543, 74 (*ubi* id est selinum
*Schmidt*). **napi** naep (*AS.*) V 374, 44. *V.* apii semen, rapa; *v.*
*Fischer-Benzon p.* 112.

**Napy** νᾶπυ III 266, 7.

**Nar** ῥώθων II 429, 9; 541, 14 (*GR. L.* I 42, 12). ῥιν II 428, 13.
ῥίς II 428, 24. **naris** ῥίν III 247, 32 (*unde?*). **nares** ῥῖνες,
ῥώθωνες II 132, 25. ῥῖνες III 350, 48; 471, 20. μυκτῆρες (singularia non
habet) II 373, 55. μυκτῆρες III 175, 14. *V.* aduncis n., emunctae n.,
ad nares.

**Nar** ὄνομα ποταμοῦ II 508, 11. fluuius Narniae qui coniungitur Tiberi
V 554, 54 (*cf. Serv. in Aen.* VII 517). generis neutrius, nomen est
fluuii: nam de naso haec naris, huius naris dicitur, plurali haec
narices V 573, 16 (*GR. L.* II 222, 11; IV 15, 29 *sq. Arch.* IV 129).

**Nar albus** fluuius Tusciae. Virgilius (*Aen.* VII 517: *cf. Serv.*):
sulfurea Nar albus aqua V 225, 8.

**Narcissinus:** *cf.* milo (μύρον?) **narcissino** id est oleus
**narcissinus** III 569, 69. *V.* oleus n.

**Narcissus** νάρκισσος τὸ ἄνθος II 375, 5. νάρκισσος III 192, 32; 301,
18; 531, 9. bulbus agrestis III 593, 11; 626, 69 (bulbis). bulbus III
570, 4; 608, 55. *Cf.* bulbus **narcissus agrestis** III 587, 43. bulbus
[s]emeticon id est **narcissum** III 553, 60; 618, 9. **narcissu** id
est bulbum narcissum, uultu sementitio (bulbus emeticos?), cinoglosa III
541, 9. **narcissus** emeticus III 570, 35. id est uua ragias III 596,
27; 630, 35. ema anabrago (*cf. Pseudap.*) id est **narcissus** III 561,
49. **narcissus** authonoes (autogenes *Pseudap.*) III 550, 40. eptone
III 561, 48 (*v. Pseudap.*). flores purpureos (*vel* puero) IV 122,
23; V 467, 37 (*cf. Serv. in Georg.* IV 160). **narcissum** genus herbae
odoriferae purpureae V 119, 8. *V.* lilius siluaticus, solsequia,
squilla, ros maior. *Cf. Diosc.* IV 158; *Pseudap.* 56; *v.*
*Fischer-Benzon p.* 38.

**Narcoti\<c\>a** medicamentum somniferum III 603, 24.

**Nardo rustico** baccara III 570, 20. *V.* baccar. *Cf. v.
Fischer-Benzon p.* 56.

**Nardostachyos** (nardus cius *codd.*) spica nardi III 548, 17. spica
nardus III 585, 7 (nardus tacius). spigugus (spicus?) III 593, 16.
spicus III 570, 1; 614, 58. spigus III 627, 5. id est nardi spica nigra
III 541, 6.

**Nardum pisticum** nardum fidelem, id est sine fraude IV 260, 40 (*cf.
Hieron. praef. in XII prophet.; Eucher. instr. p.* 148, 18). chrisma
sine inpostura *Scal.* V 605, 39.

**Nardum spicatum** species nardi in modum spicae infusa conficitur V
374, 33. ab eo quod species ipsa nardi in modum spicae sit, quae infusa
conficitur V 225, 9 (= *Eucher. instr. p.* 148, 19).

**Nardus** genus odoris optimi IV 122, 11 (nardum *a c d*). arbor V 374,
30.
