**ἐνακόσια** nongenta.

**\*ἐνάκρατον (?)** *v.* ἄκρατος (ἕν ἄκρατον).

**ἐναλείφω** inungo.

**ἐναλλαγή** mutatio.

**ἐναλλάξ** mutuo.

**ἐναλλάσσω** cambio, commuto, demuto, immuto, muto.

**ἐνάμαρτος** mendosus, -ον perperus (perperam; ἀνάμαρτον *cod.*, *quod*
ἐνάμ. *corrigendum videtur*).

**ἐναμάρτως** perperam.

**ἐναμείβω** immuto.

**ἔναμμα** filum.

**ἐναντίος** adversarius, adversus, contrarius, eniber, sinister, -οι
advores, -ον τὸ ἀντικρύ ante conspectum, ἐξ -ας contra, obviam (*cf.*
μέρος), ἐκ τοῦ -ου e contrario, ex diverso, versa vice, ἐκ τῶν -ων
ultro, ultro citroque, κατ' *et* ὑπ' ἐναντίον adversus, contra.

**ἐναντιότης** contrarietas, refragatio.

**ἐναντιοῦμαι** adversor, committo, obsisto, obvio, occurro, officio,
reclamo, refragor, reluctor, resisto.

**ἐναούγκιον** dodrans.

**ἐναπόγραφος** ascripticius, capite census.

**ἐναργής** evidens, liquidus, manifestus, praesens. *Cf.* impigrabile.

**\*ἔναργος** *cf.* impedio 2.

**ἐναργῶς** evidenter.

**\*ἐναρδευτής** irrigator.

**\*ἐναρδεύω** irrigo, -ευθέντος irrigato. *Cf.* irriguus (-gilo).

**ἐνάρετος** eximius, industrius, nobilis, strenuus.

**ἔναρθρος** articulatus.

**\*ἐναρκτικός** inchoativus.

**ἐναρμοσθείς** impactus.

**ἐνάρχομαι** inchoo.

**ἑνάς** unitas.

**ἐνατενίζω** intueor.

**ἔνατος** nonus, -ον novies. *Cf.* ἔννατος.

**ἔναυλος** alveus, aulon.

**ἐνδαψιλεύομαι** indulgeo, largior.

**ἐνδεής** egens, egenus, egestuosus, indigens, indigus, -έστερος
detentator.

**ἔνδεια** abstinentia, egestas, inopia, penuria. *Cf.* paenitentia.

**ἐνδεικνύω** demonstro, ostendo.

**ἐνδείκτης** delator, ostentator.

**ἔνδειξις** ostentatio.

**\*ἔνδειστρον** (*Eins.*) cerniculum.

**\*ἑνδεκαούγκιον** deunx.

**ἑνδεκάτη** undecima.

**ἐνδελεχείᾳ** assidue.

**ἐνδελεχής** indefessus.

**ἐνδελεχίζω** continuo 1. *Cf.* continuatio.

**ἐνδελεχῶς** frequenter.

**ἔνδεμα** alligamentum.

**ἔνδεσμος** ligatura, materia, nodus, volumen.

**ἐνδεσμῶ** innexo, innodo, - ἐν παλαίστρᾳ implico, nexo.

**ἐνδέχομαι:** -εται convenio (-it), licet, -όμενος conveniens.

**ἐνδέω** desum, -ομαι egeo.

**ἐνδέω:** ἐνδεδεμένος illigatus, innexus, innodatus, obligatus, - ὑπὲρ
φόρου δήμου praediator.

**ἐνδιαιτῶμαι** immoror.

**\*ἐνδιαπεπερονημένος (?)** transfixus.

**ἐνδιατρίβω** immoror.

**ἐνδίδωμι** differo.

**\*ἐνδινευτής** tergiversator.

**ἔνδοθεν** intrinsecus, intus. *Cf.* ἔξωθεν.

**ἐνδοιάζω** dubito.

**ἐνδομενία** *et* **ἐνδομενίαι** lautia, supellex, - ξυλίνη supellex lignea.

**ἐνδόμυχον** latebrosus (-um), penetralium.

**ἔνδον** indu, intro, intus, penetralis (-e), penitus. *Cf.* penes.

**ἔνδοξος** adoriosus, almus, clarus, gloriosus, honoratus, illustris,
inclytus, nobilis, praeclarus.

**ἐνδόξως** clare, gloriosus (-e), II, 298, 18 *sine interpr.*

**ἐνδότερος** citerior, interior, intimus, ulterior, -ον *cf.*
endopicus, εἰς τὸ -ον introrsus; ἐνδότατος intimus (*cf.* inclytus),
penetralis, -α ancita.

**ἐνδοτέρω** interior (-us), penitus. *Cf.* endopicus.

**ἐνδοχεῖον** *cf.* horreum.

**ἐνδράνεια** efficacia.

**ἐνδρανής** sollers.

**ἐνδρομίς** gausapa. *Cf.* caligo, endromida.

**ἔνδυμα** indumentum, palla, pallium, stola, vestimentum, vestitus, -
συγκλητικόν praetexta.

**ἐνδυναμοῦμαι** praevaleo.

**ἐνδύνω** penetro.

**ἐνδύω** induo, vestio, ἐνδεδυμένον vestitum.

**ἐνεάζω** stupeo.

**ἐνέδρα** cavillatio, deceptio *s. v.* devoro, fraus, insidia,
insidiatio, obsessio, obsessus (-um), subsessum, sutela, -αι illecebra
(-ae).

**ἐνεδρευτής** deceptor, insidiator, seductor.

**ἐνεδρευτικός** insidiosus.

**ἐνεδρεύω** colloco, decipio, illecebro, insidio (*et* -or), obsideo.

**ἐνείλημα** involucrum, involumen.

**ἐνειλῶ** implico, involvo, ἐνειλημένον involutum.

**ἔνειμι** insum, ἐνών ingenitus.

**ἐνείργω** *cf.* impedio.

**ἐνείρω** insero, insuo, -ων *cf.* continuans.

**\*ενεις** endopesciscas.
