fluuius inferni IV 476, 46. **Acherontes** Ἅιδης III 290, 58
(acnerotes).

**Achilles** Pelei filius III 506, 13.

**Achiuus** Graecus V 260, 28 (achus). **Achiuis** Graecis IV 6, 8; 427,
33. *V.* Conon.

**Achor** conturbatio V 338, 55 (*cf. Roensch Mus. Rhen.* XXX 455).

**Achoras** ἰχώρ (icar) III 597, 6: *cf. Cassii Felicis ed. Rose ind.*
p. 199 *s. v.* ἀχώρ.

**Acia** ῥάμμα II 13, 43; 427, 24; 521, 18; III 21, 49; 93, 5; 323, 68;
369, 26; 467, 33 (cf. III 270, 16). *Cf. Roensch Coll. phil.* 166. *V.*
acies.

**Aciamen** *v.* aconita.

**Aciare** *v.* aciarium.

**Aciarium** στόμωμα III 325, 39 (acc.); II 13, 12 (acc. *cod.* στόμωμα
*g.* στόματα *cod.*) ferrum durum IV 6, 22 (aciare *a*); V 162, 2
(aciare *codd.*). **accearium** steli (*AS.*) V 340, 17. *V.* denticulum
aciarium.

**Aciarium** ῥαφιδοθήκη II 13, 44; 427, 37. βελονοθήκη II 257, 9. *V.*
acuclarium.

**Acida** est cura (*cf.* ἀκίς) V 615, 48. *Cf.* acidalia.

**Acidalia** (*v.* acida) hinc et Venus dicitur, quod acidas inmittat
hominibus V 615, 49 (*Serv. in Aen.* I 720).

**Acidit (?)** ὀξίζει II 384, 43 (acidit, acescit, acetat).

**Acidus** ὀξύπικρος II 384, 55. ὀξώδης (oxides) III 93, 29 (*de
colore*). ab acrore IV 202, 51; 303, 16; V 260, 42; 343, 43 (acie *quod
sequitur pertinet ad* acie turba 42: *cf. Nettleship 'Journ. of Phil.'*
XVII 120). *V.* acumen.

**Aciem gladii** uim gladii IV 478, 40; V 260, 19.

**Acieris** genus securis V 590, 63 (acieres). *Cf. Festus Pauli p.*
10, 1. **acceres** ἀξίνη ἱεροφάντου, ὡς Πλαῦτος II 13, 9 (= *inc. fr.*
LXXVI). **acerlis** securis quam flamines seu pontifices habebant IV
202, 39; V 436, 15 (acersu --- aut --- habent). **acceries** genus
securis IV 404, 15. *Cf. Loewe Prodr.* 257, *Buecheler Mus. Rhen.* XLVI
233, *C. Hofmann Arch.* II 275, *Dammann Comm. Ien.* V 41, *Ind. Ien.
a.* 1885/1886 p. VII.

**Acies** αἰχμή II 221, 39. ἀκμὴ σιδήρου II 222, 53. ἀκμή, αἰχμή, τάξις,
ὀξυδορκία, ὀξύτης, ῥάμμα (*v.* acia) II 13, 48. βολὴ ὀφθαλμοῦ II 258,
37. acumen oculorum uel gladii IV 6, 4; 17; 7, 12. εἴλη II 285, 52.
παράταξις II 533, 52; III 208, 32. παράταξις ἐν πολέμῳ II 396, 34.
φάλαγξ II 469, 48. ordo uel prima pars exercitus IV 303, 17. extensa
militum impugnatio IV 5, 33; 303, 18; V 260, 37. turba V 259, 12; 343,
37 (abicies *utroque loco*). turba exercitus uel acumen gladii uel
intuitionem IV 478, 39. et ordo militum et oculorum uisus et ferri
acumen V 260, 45. plura significat: aut enim oculorum est, ut 'huc
geminas nunc flecte acies' (*Verg. Aen.* VI 788) aut exercitus ut
'hinc acies atque hinc acies \<ad\>stare latinas' (*Aen.* IX 550) aut
ferri ut 'stat ferri acies mucrone corusco' (*Aen.* II 333) V 162, 5.
**acia** ala IV 303, 15; V 590, 38 (axilla *Arev.*). **aciem**
ὀξύτητα, ἀκμήν II 13, 15. oculorum aut uim ferri IV 202, 47. **acie**
turba IV 202, 46 (*cf.* acidus). **acies** acumina gladiorum IV 404, 17.
*Cf. GR. L.* I 156, 10; 322, 35; V 36, 1.

**Acies animi** ὀξύτης φρενῶν II 385, 3.

**Acies ferri** ὀξύτης σιδήρου II 385, 2.

**Acies oculorum** ὀξυδορκία II 384, 50.

**Acilli** id est grana uel semina uuarum *glossa Ambros. apud Loewium
Prodr. 432* (arilli *cod.* acini *Buecheler.*).

**Acinaces** pugio Part\<h\>icus IV 477, 33 (*cf. Porph. in Hor. carm.*
I 27, 5); V 260, 59; 625, 18. gladii V 339, 26. *V.* canacem.

**Acinari** tricari, in paruo morari IV 480, 38; V 590, 28; 260, 58
(mora arit *cod.* aginari *Vossius*; apinari *Casaub.*).

**Acinaticium** ὀξῶδες II 385, 5.

**Acinus** ῥώξ II 13, 46; 429, 17; III 575, 10. γίγαρτον II 542, 10;
514, 31. βότρυς, ὄμφαξ III 427, 67 (*v.* botrus). folliculus botri II
564, 12. hic **acinus** et hi **acini** generis masculini IV 203, 6.
**acinum** (*neutr.*) ῥώξ III 27, 10. hindberiae (*AS.*) V 340, 38.
**acina** ῥώξ II 429, 17 (*cf.* III 192, 43, *ubi* roia *magis ad* ῥώξ
*quam ad* ῥοία *spectare putaverim*). **acini** ῥῶγες II 13, 45. *Huc
refero* **acinestafile** gargarion III 597, 34 *h. e.* acinus σταφυλή,
γαργαρεών. acinus *est quae alibi* uua *dicitur* (*Zäpfchen*). *V.*
flores de acina, erimio.

**Acinus uuae** ὀμφακῖτις III 548, 27.

**Acipenser** (accipenser *R.* accipienser *G*) genus piscis est raro
inuentum, id est nobile *Plac.* V 4, 17 = V 44, 6 (*cf. Macrob. Sat.*
III 16, 4). **aquipenser** εἶδος ἰχθύος II 18, 44 (= *Festus Pauli* 22,
13). **aquippense** γαλοιος (γαλεός?) III 318, 5; 513, 6 (galios).
ἀκιπήσιν **accipiens** III 186, 50 (*cf. Athen.* VII p. 294 f ἀκκιπ.).

**Aciscularius** λατόμος II 13, 47.

**Acisculus** (ascic.) asciola, dolabra V 590, 25. **accisulus** (*corr.
b in marg.*) axedonis (ἀξηδονίς?) II 514, 32. **acisculum** σκαφίον ἤτοι
ὄρυξ, κηπουρικόν II 432, 49. malliolum structorium IV 481, 40. quod
habent [in]structores, quasi malleolus est ad caedendos lapides V 260,
43; 343, 53. **acisclum** μυλοκόπον III 23, 23. **ciscillus**
