mus, nobilis uel extremus IV 395, 9. magnus V 153, 54. maximus IV 180,
12. **summum** ὑψηλόν II 469, 22. ἀνώτατον II 231, 35. ἄκρον II 223, 61.
postremum, nouissimum IV 289, 7. **summo** alto V 153, 55. **summi**
excelsi IV 179, 4. **summa** ἄκρα II 223, 31. ἄκρα, ἔσχατα III 427, 9.
**summ[ici]a** ἄκρα III 430, 49 (? *inter olera*). *V.* dii summi,
summa, per summum.

**Summus lictor** *v.* lictor s.

**Summus sacerdos** ἀρχιερεύς III 464, 74. pontifex IV 395, 10.

**Sumo** δέχομαι, λαμβάνω II 192, 24. δέχομαι II 269, 4. λαμβάνω II 358,
23. **sumit** λαμβάνει II 192, 32. accipit IV 179, 19; 570, 47; V 153,
52. **sumunt** λαμβάνουσιν II 192, 22. **sume** δέξαι, λαβέ II 192, 36.
**sumite** accipite IV 570, 48. capite V 153, 51. accipite uel arripite
IV 179, 22. **sumere** implere, (impendere *Hild.*), bibere IV 395, 4.
**sumam** accipiam IV 180, 45. **sumpsit** accepit IV 179, 21; *cod.
Monac. v. suppl.* recepit IV 424, 18.

**Sumpta** annona familiaris V 484, 60 (sumptus?).

**Sumptis** acceptis V 153, 59.

**Sumptio** λῆμμα *b e post* II 194, 27.

**Sumptuarius** ἐπαναλωτής II 305, 43. largus II 594, 29. qui erogat
sumptus IV 179, 26; 289, 15; 570, 52; V 610, 47; *cod. Monac. v. suppl.*

**Sumptuosa res** V 664, 32.

**Sumptuosus** δαπανηρός II 266, 34, III 178, 34; 250, 68. δάπανος III
334, 20. ἄσωτος II 249, 30. πολυδάπανος II 412, 35. **sumptuosius**
δαπανηρότερον II 266, 35.

**Sumptu publico** adiutorio regali V 420, 10 = 428, 72 (*Euseb. eccl.
hist.* III 6).

**Sumptus** ἀνάλωμα, δαπάνη II 192, 29. ἀνάλωμα II 516, 56. δαπάνη II
266, 33; 489, 49; 513, 53.

**Sunt mihi** habeo IV 177, 47.

**Suo** hypome (= ἠπῶμαι) III 144, 32. ῥάπτω III 475, 42. **suo suis**
coso V 514, 39 (*v.* cuso). **suit** ῥάπτει, ἠπᾶται II 192, 18.
**suere** consuere, id est cusire IV 288, 37; 389, 35 (sciere *codd.*);
V 484, 46 (est considere). *V.* cuso.

**Suo cum corde** cum animo suo V 545, 44. *Cf. Verg. Aen.* VI 185.

**Suo Marte** τῇ ἰδίῳ δυνάμει ἐπὶ πράγματος ἤτοι δίκης … 'et is suo
Marte percurrat' (*cf. Cic. Philipp.* II 95) II 455, 1.

**Suo nomine** ἰδίῳ ὀνόματι III 476, 28.

**Suopte** sua uoluntate V 484, 61 (*cf.* summa ope). suo ipsius
[torridum] IV 177, 36 (*contam. cum* siccum?). suimet ipsius,
**meopte** meimet ipsius, **tuopte** tuimet ipsius V 526, 18. *Cf.
Festus p.* 310, 8.

**Suo robore** sui firmitate V 154, 2.

**Suouetaurilia** sacra sunt de tribus animalibus, de sue, oue, tauro IV
289, 56; V 393, 33 (et tauro); *cod. Monac. v. suppl. Cf. Serv. in Aen.*
IX 624; *GR. L.* I 108, 27; *Loewe Prodr.* 379. *V.* solitaurilia.

**Supat** iacit, unde dissipat dis\<i\>cit, obstat (obsipat *Buech.*)
obicit et insipat hoc est inicit V 514, 44; 526, 17. iacit unde
dissipat, hoc est inicit, discit, obstat, obicit, insipat V 580, 17.
*Cf. Fest. Pauli p.* 311, 6.

**Supellex** ἐνδομενία II 192, 40 (supp.). **supplex** ἐνδομενία
(singulariter tantum declinabitur) II 298, 13 (*GR. L.* I 32, 7 etc.).
**supellex** ἐνδομενία II 490, 24; III 20, 35; 92, 9 (superlex: *cf. GR.
L.* IV 198, 13); 196, 66 (supp.); 269, 29; 320, 62. οἰκοσκευή II 380,
37. duo sunt genera (g̃. *cod.*). rustica et urbana: et secundum artem
haec suppellectilis declinabitur, non suppellex, quia grammatici negant
a nominatiuo singulari genitiuum sin­gularem duabus syllabis esse
(posse?) crescere V 154, 3 (*cf. GR. L.* II 325, 1 *etc.*).
**suppellex** ornatus uel facultas V 154, 4. **suplex** omnia utensilia
domus IV 571, 4 (*corr. b*). **supex** omnia utensilia domi IV 179,
35. **suppelles** facultas IV 181, 4, **supellectile** κατασκευή II 533,
15. res mobilis IV 289, 17. utensilia IV 571, 9; V 529, 34. dicitur omne
strumentum (instr. *G.* stram. *P*) et ornatus domus *Plac.* V 42, 12 =
V 101, 1 (ornamentum) = V 155, 11 (superl.). **supellectilia** ἐνδομενία
III 274, 41.

**Supellex lignea** ἐνδομενία ξυλίνη III 365, 77.

**Super** ἄνω II 231, 26. ἐπάνω II 305, 56. ἐπί II 307, 4. ἐπὶ πλέον II
310, 18. περὶ II 402, 4. ὑπέρ II 463, 53. ὕπερθεν II 193, 30; 464, 24.
insuper V 154, 14. **super[o]** ualde V 647, 4 (*Non.* 169, 19).

**Supera** nauis V 391, 40; 611, 29 (*ubi* suppara uela nauis *Vulc.,*
supparum pars nauis *Nettleship 'Journ. of Phil.'* XVII 123, myoparo
nauis *Helmreich Arch.* VII 275. superaria uestis?).

**Superabile** ὑπερ[ε]σχετικόν II 193, 35.

**Superabundans** (-hab- *cod.*) indigeries per habundantiam frugum V
389, 11. V indigeries.

**Superabundo** ὑπερπλεονάζω II 464, 42. ὑπερπερισσεύω II 464, 44.

**Superaedifico** ἐποικοδομῶ II 313, 18.

**Super aethera** super aera IV 569, 57.
