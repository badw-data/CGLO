ἔκφρων II 293, 49. infelix IV 52, 48. dementicus IV 504, 38. sine mente,
insanus V 532, 65 (*Ter. Andr.* 469). **dementes** sine mente IV
49, 1. amentes V 406, 43.

**Demensum** ἀπομέτρημα II 42, 32. *V.* demessum, de dimenso tuo.

**Dementat** ἀφρονεῖ, ἀπορεῖ, καταγινώσκει II 42, 57. ἐξίστησιν
ἰδιωτικῶς II 42, 35.

**Dementatam** stultam factam V 567, 2.

**Dementatio** insania II 576, 42.

**Dementia** ἄνοια II 228, 15. ἀπόνοια, ἄνοια II 42, 36. παράνοια II
395, 44. uecordia, insania, stultitia IV 437, 42 (*Verg. Aen.* V 465).
insania, amentia IV 226, 33. insania IV 504, 37.

**Dementicastis** obliuioni tradidistis *gloss. Werth. Gallée* 337 (*v.*
*suppli*).

**Dementicium** ἐντροπή II 503, 27 (uerecundia *in marg.*).
deuerticlum ἐκτ. *H.*

**Dementicus** insanus, amenticus IV 226, 31 (*v.* demens).
**dementicos** *v.* apodos.

**Demergo** καταβυθίζω II 340, 7. καταποντίζω II 343, 9. **demergit**
καταποντίζει II 42, 33. **demersit** correpsit IV 329, 2.

**Demersio** καταβυθισμός II 340, 8.

**Demessum** (demensum *codd.*) incisum IV 49, 42; 53, 31; 505, 25. *Cf.
Verg. Aen.* XI 68.

**Demetior** ἀπομετρῶ II 239, 4. **demensus** mensurauit IV 50, 43; 504,
20; V 284, 9; 406, 56.

**Demeto** ἀποθερίζω II 236, 54. **demeta\<m\>** praecidam IV 227, 19.

**Demetreos** Bithyniensium lingua September mensis dicitur V 188, 40.
*V.* menses.

**Demicare** διαπληκτίζεσθαι II 42, 49. *V.* dimico.

**Demiculus** σκυφίον μικρόν II 42, 37.

**Demigratio** μετάβασις II 42, 51.

**Demigro** μεταβαίνω II 368, 37. μεταναστεύω II 369, 20. **demigrat**
μετ\<αχω\>ρεῖ, μετοικεῖ II 42, 52. emigrat, detrudit IV 502, 38.
**demigrasti** παρέβης II 42, 53 (*Cic. in Cat.* 119). **demigrauit**
μετῷκησεν II 42, 50.

**Demingo** ἐξουρῶ II 304, 27.

**Deminuo** ἀπομειῶ II 239, 10. ἐλαττῶ III 140, 51. καταλεπτύνω II 341,
49. **deminuit** ἐλαττοῖ, ἠλάττωσεν II 42, 38. **deminuuntur**
ἐλαττοῦνται II 42, 45.

**Deminuti** minuati V 496, 21. deminuatum *cf.* deriuatum.

**Deminutio** μείωσις II 372, 16; 535, 10. ὑποκορισμός II 466, 38; 494,
60. ὑποκορισμός, μείωσις II 42, 40. ὑφαίρεσις II 468, 59; 469, 9.
ἀφουσία II 253, 52. ἐλάττωσις II 294, 24. *V.* diminutio, capitis
deminutio.

**Deminutio lunae** μείωσις σελήνης III 293, 63.

**Deminutiue** ὑποκοριστικῶς II 466, 39. *Cf. Loewe Prodr.* 413.

**Deminutus** ἔκλειψις ἡλίου ἢ σελήνης II 291, 31. ἐλάττωσις II 294, 24.

**Demiratus** diu miratus, ualde miratus V 189, 1.

**Demiror** despicio IV 503, 41; V 449, 42; 496, 22. dispicio IV 226,
52. despicior IV 49, 37.

**Demissa** ἀποβληθέντα II 42, 39. *V.* dimissus.

**Demissio** ἄφεσις II 252, 49.

**Demissus adulescens** V 661, 40 = pudore praeditus V 663, 73.

**Demitto** καταπέμπω II 342, 54. καθίημι II 335, 21. ἀφίημι II 253, 18.
ἀποστέλλω II 240, 53. ἐξαφίημι II 302, 26. χαλῶ σχοῖνον ἢ ἄλλο τι II
475, 12. **demitte** ἄφες II 252, 48. ἔασον II 283, 1 (dimitte?).
indulge, relaxa IV 437, 43 (*Verg. Aen.* V 692; *Georg.* IV 542).
**demisit** καθῆκεν, ἐχάλασεν, καθεῖλκεν, ἀπεβάλετο II 42, 41. *V.*
dimitto.

**Demo** ἀφαιρῶ II 252, 20. ἀφαιροῦμαι II 252, 21. ὑφαιροῦμαι II 468,
58. **demit** ἀφαιρεῖ, ὑφαιρεῖ II 42, 42. tollit IV 49, 3; V 286, 54;
407, 13. **demet** tollet IV 226, 54. detrahit aut minuit IV 49, 38; 53,
40 (minuet). **demit** de\<mi\>nuit, deducit, detrahit uel minuit IV
503, 2. deme ὕφελε II 42, 31; 469, 10. **demere** tollere IV 329, 3; V
405, 61. **dempsi** sustuli V 533, 12 (*Ter. Ad.* 736). deduxi,
deminui IV 49, 41. **dempsit** ἀφείλετο II 252, 42. ἔτεμεν, τέμνει
(τεμνῃ?) II 42, 60. tollit (= tulit?), deleuit IV 227, 6. *Cf.*
**diemat** (demat?) dempserit V 356, 33; 408, 12. *V.* ungues demo.

**Democraticus** *v.* ratio populorum.

**Demoenio** τειχίζω II 452, 36.

**Demolior** καταβάλλω οἰκοδομήν II 339, 40. καταστρέφω II 344, 12.
**demolit** καθαιρεῖ II 42, 43. **demolitur** destruit IV 329, 4.
exterminatur IV 50, 47; 226, 42; V 286, 10; 408, 59 (*v.* dimolitur).
**demolire** dissipare IV 52, 10. **dimoliri** (*scr.* dem.) diruere V
640, 11 (*Non.* 95, 22). *Cf.* dimolitur, diruo.

**Demolitio** καθαίρεσις οἰκοδομῆς II 334, 28. distructio II 576, 43
(destr. *b*).

**Demonicus** plebis uictor III 491, 77.

**Demonstrandi** δηλωτικοί II 269, 18.

**Demonstratio** ἀπόδειξις II 236, 20. **demonstratione** δηλώσει II 42,
47.

**Demonstratiuum** δεικτικόν II 267, 8.

**Demonstro** ἀποδεικνύω II 236, 13. ἐπιδεικνύω II 307, 45. ἐνδεικνύω II
297, 54. ὑποδείκνυμι II 465, 47. **demonstrat** ἐπιδεικνύει II 42, 46. 
