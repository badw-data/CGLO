**dedocere** de doctrina euacuare IV 227, 45.

**Dedolo** ἀποπελεκῶ II 239, 41. **dedolat** ἀποπελεκᾷ II 39, 54. dolat
IV 437, 20.

**Deducit genus** κατάγει τὸ γένος III 524, 17.

**Deduco** ἀπάγω II 232, 30. κατάγω II 340, 30. καταφέρω II 344, 53.
κατασπῶ II 343, 57. κατακομίζω II 341, 22. ὁδηγῶ II 379, 10. **deducit**
καθέλκει, καθαιρεῖ II 40, 9. παρεκτείνει II 562, 13. καταπίνει II 40, 20
(deductis *cod.*) defert IV 328, 6. **deducunt** e terra deducunt in
mare IV 437, 19 (*Verg. Aen.* III 71; IV 398; *cf. Serv. in Aen.* I
551). **deducas** διαγάγοις II 40, 2; 40, 16. **deduc** δίαξον II 40, 5.
detrahe, minue, persequere IV 50, 15. trahe, minue IV 504, 33.
**deducere** θέσθαι II 40, 18. separare IV 437, 18 (*Verg. Aen.* II
800). deponere [inpellunt uel ruinam faciunt: *cf. Nettleship 'Journ.
of Phil.'* XIX *p.* 119, *qui* deruunt *lemma inserit*] IV 52, 55.
componere V 653, 20 (*Iuvenal.* VII 54). **deducor** κατάγομαι II 340,
25. **deducuntur** κατάγονται II 40, 19. **deducatur** διαχθείη II 40,
6. **deducantur** διαχθῶσιν II 40, 3.

**Deductio** ἀφέλκυσις II 518, 26. διαγωνισμός (διαγωγισμός?) II 540, 30
(did. ?). διαγωνισμὸς ἡ διαγωγή II 552, 55. disputatio uel demptio IV
50, 17. disputatio IV 502, 37. *V.* diductio.

**Deductis capillis** ἁπλόθριξ III 329, 54; 55.

**Deductor** καθοδηγός II 335, 27. **deductores** καθοδηγοί, ἀρχηγέται
II 40, 4.

**Deductus** delatus, deuectus, deportatus IV 328, 7. **deductum** tenue
uel gracile aut subtile IV 52, 54 (*Verg. Ecl.* VI 5). **deductis**
ὐφαιρεθέντων καὶ κατασχεθέντων, ὑφεξῃρημένων II 40, 1. ὑπολογηθέντων II
40, 17. παρασπασθέντων II 40, 21.

**De exitu animae** *v.* defixiezodo.

**De experientia dei** peridoy cratoros asporios (περὶ τοῦ κράτορος\<?\>
ἐμπειρίας?) V 377, 47. *Cf.* catonperenmatossia.

**Defaecatum** liquidum, purum IV 328, 8; V 633, 49. liquidum, purum,
extersum IV 48, 45; 227, 31; V 284, 26. liquidum, purum, extersum uel
purificatum IV 505, 8. liquidum *gl. Werth. Gallée* 337 (*cf. suppl.*).
purificatum V 283, 45. a faece purgatum V 285, 21. **defaecatior**
purior, lucidior V 567, 11. **defectior** putior (= purior) *gloss.
Werth. p.* 337 *Gallée* (*p. suppl.*).

**Defaecatum uinum** purificatum V 356, 56; 405, 52.

**Defaeco** (defecto *cod.*) διυλίζω II 279, 12. **defectat** διυλίζει,
διηθεῖ II 40, 25 (*cf.* faeco *ubi* fecto *cod.*). **defaecare** est
decolare et res quondam mixtas a faecibus segregare *Plac.* V 18, 8 = V
60, 27 (*cf.* V *praef.* XVI, *ubi* quasdam commixtas). *Cf. Non.* 454,
22.

**Defatigat** flagellat (fatigat?), lassat IV 49, 18; 505, 6.
**delitigat** fatigat, lassat IV 52, 40. **defatiget** (*vel* defit.)
fatiget V 356, 15; 405, 24 (defit.); 426, 18 (*vit. Anton. interpr.
Euagr.* 15). suenceth (*AS.*) V 405, 25.

**Defatigatio** καταπόνησις II 343, 6.

**Defatigatus** καταπεπονημένος II 342, 57. **defatigati** κεκοπωμένοι
II 40, 35.

**Defautio** ἀποτροπή, ψόγος II 40, 24 (dissuasio *De-Vit*). *V.*
depulsio.

**Defectio** λιποθυμία III 363, 34. ἔκλυσις, ἀτονια II 40, 37. ἔκλειψις
III 492, 78. ἔκλειψις, ἀπονεμηθείς, ἀφορισθείς (*contam. cf.* deputatus,
definit.) II 40, 26. ἡλίου ἔκλειψις II 324, 11. ἔκλειψις ἡλίου ἢ σελήνης
II 291, 31.

**Defectio lunae** ἔκλειψις σελήνης III 293, 62; 516, 68.

**Defectio solis uel lunae** ἔκλειψις III 520, 18.

**Defectura** aspringendi (*AS. partic. praes.*) V 356, 40; 405, 38.

**Defectus** ἀγωνία II 537, 57. ἀτονία II 250, 18; 550, 12. ἔκλειψις II
291, 30. ἔκλειψις ἡλίου ἢ σελήνης II 291, 31. pluraliter: defectus solis
uarios lunaeque labores V 188, 15 (*Verg. Georg.* II 478).

**Defectus** (*et* **defessus**) ἀποκαμών II 237, 19. καταπεπονημένος II
342, 57. ἐξατονήσας II 40, 38. *V.* defessus.

**Defendiculum** *v.* tuitio.

**Defendo** ἐκδικῶ II 289, 34. συνδικῶ II 444, 48. ἀπολογοῦμαι II 238,
42. **defendo[r]** tueor (= *Non.* 277, 24) IV 437, 21 (*Verg. Ecl.*
VII 6). **defendit** excusat, uindicat IV 49, 21. **defendant**
ἐκδικήσωσιν III 212, 21 = 228, 19 = 648, 4. **defendere** pro alio satis
facere IV 48, 46. **defenditur** distenditur (diff.?) V 407, 27.

**Defenerauit** ditauit IV 225, 44; V 627, 55. ditauit, donauit V 406,
14.

**Defensa** λήιον (*h. e.* defrensa), ἐκδικηθέν II 40, 29.

**Defensaculum** *v.* latibulum, tuitio.

**Defensat** defendit V 633, 39.

**Defensio** ἐκδικία II 289, 30; III 445, 23; 479, 69. ἐκδίκησις II 289,
29; 535, 18. uindicta, ultio IV 328, 9. **in defensionem** εἰς ἐκδικίαν
III 65, 18. *V.* defensum.

**Defensor** ἔκδικος II 40, 28, 289, 31; 535, 17; III 445, 24; 480, 9.
σύνδικος II 444, 47. **defensores** custodes, praesides V 410, 40 (*cf.*
*can. conc. Afric.* 75).
