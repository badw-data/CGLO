**Terebellus** nabfogar (*vel* naboger, *AS. pro* nabugar) V 396, 41.

**Terebinthus** κλῆθρον, τερέβινθος III 264, 50 (*unde*?).
**therebintum** τερέβινθος III 191, 65. *V.* tetterebinthos, resina
tereb., cera.

**Terebra** τρύπανον II 197, 6 (terebrum); 460, 41; 498, 10; 524, 50;
546, 32; III 23, 37; 204, 22 (τρύπανος); 325, 54; 368, 60; 505, 14 (*GR.
L.* I 553, 27). **tenebra** τρύπαρον III 79, 49. *V.* furfuraculum.

**Terebrantes** perforantes V 420, 60 = 429, 40 (*Euseb. eccl. hist.* VI
31). borendę (*AS.*) *gloss. Werth. Gallée* 343 (*v. suppl.*).

**Terebrati** *v.* effossa.

**Terebrator** τρυπητής II 460, 44.

**Terebro** κατατρυπῶ II 344, 43. **terebrat** τρυπᾷ II 197, 28.
pertundit, forat IV 291, 26. **tenebra\<t\>** τρυπᾷ III 160, 56.
**terebramus** perforamus in rotundo V 157, 4 (*Verg. Aen.* III 635).
**terebra** τρύπησον II 460, 43. **terebrare** perforare IV 183, 43.

**Teredo** uermis in ligno IV 182, 49; 291, 30; 396, 46; 574, 7; V 398,
37; 581, 3; 637, 3; *gloss. Werth. Gallée* 354. *v. suppl.* termes V
485, 56. **teredio** uermis in ligno V 157, 11. *Cf. Isid.* XII 5, 10;
18; *Arch.* V 86. *V.* cossus 1, tarmus.

**Terentiani (?)** follares (?) V 516, 38.

**Teres** στρογγύλος II 439, 1. περιφερής II 405, 32. στρογγύλον II 543,
15; 546, 33. **teretum** στρογγύλον (teretem?) II 439, 2. **teres**
rotundus V 617, 37. \<ro\>tundus *gloss. Werth. Gallée* 354 (*v.
suppl.*). rotundum V 395, 17. rotunditas *a post* IV 396, 45. sinuurbul
(*AS.*) V 397, 39. **teretes** rotundi IV 182, 32; *a c post* IV 183,
23; *gloss. Werth. Gallée* 353 (*v. suppl.*); IV 573, 34.
rotundi[tas] V 485, 57. **terti** rotundi IV 182, 36. **terreti**
rotundi (*vel* -dae) IV 573, 33. **terelis** teretis, rotundi[s] IV
396, 47 (*v.* tereti trunci). *V.* tersus.

**Teretes lacerti** pulchra simul et fortia brachia IV 183, 23; 573, 35.

**Tereti gemma** rotunda gemma IV 291, 27 (*Verg. Aen.* V 313).

**Tereti (?) trunci** (*v.* teres) rotundi IV 291, 16. *Cf. Verg. Aen.*
VI 207.

**Terga do** εἰς φυγὴν δίδωμι II 287, 60.

**Terga uertentem** dorsum dantem IV 573, 31.

**Tergeminus** τρίδυμος II 458, 62. **tergeminam** triplicem IV 291, 18
(*Verg. Aen.* IV 511).

**Tergeo** καταμάσσω III 376, 71. ἀπομάσσω II 238, 58. **tergo**
καταμάσσω II 342, 12; III 70, 17 = 637, 2. **tergit** καταμάσσει II 197,
7. **tersi** κατέμαξα III 70, 18 = 637, 2.

**Tergilla** siue **tergillum** φορίνη, χοίρου δέρμα II 197, 30.

**Tergimenta** σήκωμα (? *v.* temperamentum) II 197, 23.

**Terginum** ὠμοβύρσινον II 197, 3; 42.

**Tergiuersatio** περίκαμψις, φυγοδικία II 197, 21 (*cf.* 22). φυγοδικία
II 473, 40. fuga II 595, 18. **tergiuersatione** μελλησμῷ II 197, 20.

**Tergiuersator** περικάμπτης II 403, 5. καταστροφεύς II 344, 14. δόλιος
II 280, 7. ἐνδινευτής III 334, 42; 519, 62. dorsiuersator II 595, 25
(*v.* tergiuersor). contradictor V 485, 55. calumniosus et callidus
*gloss. Werth. Gallée* 354 (*v. suppl.*). dicitur quod animum quasi
tergum uertat huc illuc, ut non facile \<qualis\> sit intellegatur IV
184, 15 (*suppl. a*). *Cf. Isid.* X 271.

**Tergiuersor** φυγοδικῶ II 473, 41. περικάμπτω II 403, 6. νωτοστροφῶ II
377, 46. ἀγνωμονῶ II 216, 51. **tergiuersatur** περικάμπτει II 197, 11.
φυγοδικεῖ III 465, 30. ἐθελοκακεῖ, περικάμπτει II 197, 26. eludit aut
frustratur IV 183, 18; V 157, 10; 248, 17. eludit IV 573, 25. moratur
uel tempus noluit IV 396, 49. dorsiuersatur IV 573, 26 (*v.*
tergiuersator). **tergiuersari** φυγοδικεῖν II 197, 8. fugire (*vel*
fugere) et dicta mutare IV 291, 20 (*Non.* 41, 22).

**Tergonarius** *v.* cerdonius.

**Tergus** et **tergum** βύρσα II 548, 55. δορά II 280, 16. **tergum
tergus** βύρσα II 260, 45. **tergus** νῶτος II 377, 45; III 311, 47.
δορά, νῶτος II 506, 55. δέρρις, δέρμα III 273, 7 *et* 8 (*unde*?). βύρσα
III 273, 5. **terqui** (tergus *e*) νῶτος, δορὰ ζῷων II 197, 9.
**tergus** terga, coria IV 183, 21 (*cf. Serv. in Aen.* I 211; VII 94).
terga, \<c\>oria IV 573, 28. dorsum V 485, 54. tergora, pelles et coria
IV 291, 22. terga, dorsa, coria V 248, 19. persecutio (pro scuto
*Nettleship 'Journ. of Phil.'* XX 186 *coll. Serv. in Aen.* X 717, *vix
recte*) IV 183, 44. **tergum** νῶτον οὐδετέρως II 377, 44. νῶτος II 501,
38; 528, 5; 544, 49 (*GR. L.* I 554, 4). δέρμα II 268, 23. terga, dorsa
IV 183, 20; 573, 27. dorsum V 248, 18. **tegus** ἡμίχοιρον II 325, 10.
ἡμίτομον χοίρου II 325, 6. medius porcus II 595, 7. **terga** νῶτα III
433, 50. dorsa IV 466, 34 (*Verg. Aen.* II 57?). dorsa hominum sunt,
terga animalium *gloss. Werth. Gallée* 344 (*v. suppl.*). posterior
pars V 157, 7. **tergora** coria IV 573, 29. pelles IV 466, 35 (*Verg.*
*Aen.* I 211); 182, 39. **terga** scapula, posteriora hominum IV 396,
48. scapulae uel posteriora IV 182, 31. fuga
