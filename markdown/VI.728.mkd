serpens IV 122, 29. **natrices** \<a\> natando V 651, 7 (*Non.* 65, 23).

**Natura** φύσις II 132, 48; 474, 10; III 13, 31; 177, 1; 180, 9; 252,
25; 278, 67; 279, 34; 328, 40; 563, 32. πρόσθεμα III 13, 9; 86, 19; 351,
45; 457, 30; 475, 5. corpus IV 367, 29. corpus, genitura V 548, 33 (*cf.
b* IV 367, 29). corpus, ingenium, genitura IV 122, 24; 540, 25; V 312,
39; 605, 20. *V.* nitura, turam, de natura loquor.

**Naturae ratio** φυσιολογία II 132, 47 (*cf. margo*).

**Naturalis** φυσικός II 474, 8. αὐτοφυής II 252, 5. νόθος III 181, 58.
**naturalem** genetiuum IV 367, 30. *V.* a naturali sapore.

**Naturalis circuitus** φυσικὸς περίδρομος III 384, 47.

**Natus** παῖς ὁ υἱός II 392, 26. υἱός II 462, 34. τεχθείς II 454, 45.
πεφυκώς II 407, 7. γεννηθείς II 262, 35. **natus filius** τὸ τέκνον III
254, 12. **natum natus** τέκνον II 452, 43. nutritus IV 540, 24.
**nata** θυγάτηρ, γνησία II 329, 40; III 254, 13. θυγάτηρ II 329, 39.
filia uel generata IV 367, 26. **nati** τέκνα III 181, 34; 303, 35.
**nati** et **natos** (-us *cod.*) τέκνα II 132, 46. **natae nati**
filiae, filii IV 454, 4 (*gl. Verg.*). **natorum filii** filii
filiorum V 467, 36. **natos** filios V 119, 28. *V.* sublatus, domi
natus, gnatus, gnata, sine natis, nantes.

**Nauarchus** ναύαρχος III 27, 46; 298, 52; 53 (nauchus, *forma
vulgari*); V 605, 41 (nauregus nauaretius). nomen nauigantis IV 367, 31;
416, 47. nauis (*vel* naui) magister IV 122, 4; 260, 19; V 225, 14; 15
(nauiargus); 312, 52. nauis princeps *Scal.* V 605, 40 (nauricus: *an*
naupigus?). princeps nauis V 312, 57.

**Nauchus** *v.* nauarchus.

**Nauci** pro nihilo uel fabae granum cum se aperit uel putamen nucis V
507, 48 (*cf. GR. L.* I 207, 6; II 204, 13). quasi nihilum siue
putridum uel putamen est nucis (ricis *cod.*) V 573, 20. est purgamentum
nucis V 621, 41. *V.* flocci et nauci. *Cf. Festus p.* 166, 6.

**Nauclerus** ναύκληρος III 205, 36.

**Naucupes** ad eundum acutum habens pedem *Scal.* V 605, 56 (*scr.*
acupes). *Cf. Osb. p.* 385; *Loewe Prodr.* 49.

**Nauficus** *v.* naupicus.

**Naufragium** ναυάγιον II 375, 9; III 77, 49; 457, 31. **naustagium**
naufragium (naufrag. nauifr.? nauagium *Hildebrand*) IV 367, 37.
**naustragium** nau\<i\>fragium V 544, 44 (*cf. Pseudacr. ad Hor. carm.*
I 16, 10).

**Naufragus** *v.* nauifragus.

**Naulum** ναῦλον II 375, 12. *V.* uectura.

**Naumachia** nauale certamen III 500, 56. bellum nauale II 587, 61. hic
forum signat romanum, qui (ubi?) pro rostri\<s\> dicitur, eo quod rostra
nauium Carthaginiensium in bello capta ibi ostentui posita erant uel
naumachia, id est pugna nauium V 573, 19. **naumachiam** bellum nauale
IV 416, 48. **naumachium** pugna naualis V 374, 47. locus naualis
exercitationis V 374, 8. *V.* aumatium.

**Naupicus** (= naupegus) nauis factor II 587, 65; V 630, 3.
**nauficus** nauis factor *Scal.* V 605, 42. **naupeus** nauium magister
(*ubi* fabricator *a b c*) IV 260, 33.

**Nauregus, nauricus** *v.* nauarchus.

**Naus\<e\>atio** uomitus uel uulatung (*vel* -ting, *AS.*) V 373, 42.

**Nausia cum febre** peridiosis III 604, 21.

**Nausiam** uomitum V 119, 10.

**Nausio** ναυσιῶ II 375, 16.

**Naustologus** nauis dispositor V 524, 11. **naustologi** sunt mercedes
quae dantur nautis propter regimen nauis uel mercatores V 621, 45.
**nastologis** (!) mercedes quae dantur nautis propter regimen nauis
(*Clem. Rom. ep. ad Iacob.*) V 423, 10. **naustologis** allocutoribus
uel exhortatoribus V 573, 17.

**Nauta** ναύτης III 77, 54; 354, 53; 412, 73; 457, 37; 471, 22.
**na\<u\>ta[m]** ναύτης III 396, 27. **nautae** ναῦται III 434, 21.
\<Inter\> **nautam** et **nauitam** haec distinctio est: **nauita**
poeticum est: nam rectum est **nauta**, sed causa metri a poetis una
littera addita est *Plac.* V 86, 11 = 119, 34 (*om.* Inter --- est: unde
causa metri a poetis additur littera). *Cf. Isid. Diff.* 390; XIX 1, 5.

**Nautea** (nauteo *cod. corr. d*) ὀσμή βύρσης II 132, 53. aqua corii
(coriis *Deuel.*) foetida, in qua coria macerantur *Plac.* V 33, 32
(corium maceratur) = V 86, 12 (corium f.) = V 119, 29 (*item*). *Cf.
Festus p.* 165, 27; *Loewe Prodr.* 285.

**Nautibellum** pugnam maritimam facere IV 367, 36 (nauale bellum facere
p. m. facere?).

**Nauale** νεώριον III 434, 3. **naualia** νεώρια II 132, 13; III 297,
10; 354, 33; 412, 70; 500, 71; 531, 6. ὁρμὸς ὁ πρὸς καταγωγὴν νεὼς
ἐπιτήδειος τόπος II 387, 4. locus in quo e mari naues subducuntur IV
454, 5 (*Verg. Aen.* IV 593). **noualia** loca in quibus e mari naues
eiciuntur IV 126, 21; V 227, 4; 468, 38 (*ubi* educuntur *Nettleship
'Journ. of Phil.'* XIX 190). **naualia** loca ubi naues fabricantur,
quod et textrinum appellatur V
