**κηρί(ο)ν** cera, favus, - ἡ μελικηρίς favus. *Cf.* cerion.

**\*κηρίφυλλον** cirifolium, χαιρέφυλλον cerfolium.

**κηρίων** candela.

**κηρόπισσον** malta.

**κηροπλάστης** cerarius, pictor sigillarius.

**\*κηροπώλης** cerarius.

**κηρός** cera. *Cf.* λύω, τήκω.

**κηροῦχος** *cf.* ceruchi.

**\*κηροφόρον** cerostatarium.

**κήρυγμα** praedicatio.

**(κηρύκειον)** κηρύκιον *et* - ὃ βαστάζουσιν οἱ πρέσβεις caduceus.
*Cf.* κῆρυξ.

**κηρύκιον** murex.

**\*κηρυκιοφόρος** caducifer.

**κῆρυξ** bucinus, curio, fetialis, murex, praeco, tubicen, - ὁ περὶ
εἰρήνης ἀποστελλόμενος κηρύκιον φέρων caduceator, - θαλάσσιος bucinus,
murex. *Cf.* caenax, cereacas, ῥάβδος.

**κήρυξις** praeconium.

**κηρύσσω** praecono, praedĭco, -ων praedicans.

**κηρῶ** cero.

**κήρωμα** ceroma.

**κηρωτός:** -όν cerotum, fascimentum, -ή cerota. *Cf.* οἴσυπος.

**κῆτος** balaena, belua, belua marina, pistrix. *Cf.* cetus 1.

**κητώεις** grandis.

**Κηφεύς** Cepheus.

**κηφήν** fucus. *Cf.* κέντρον.

**\*κιβάριος** cibarius, -ον cibarium.

**κιβώτιον** arcula.

**κιβωτός** arca, arcula, capsa, riscus, - μεγάλη riscus.

**κιγκλίς** carcer.

**κίδαρις** tiara. *Cf.* cidarim.

**κιθάρα** cithara, fides (-is). *Cf.* ἁρμονία.

**κιθαριστής** fidicen.

**κιθαρίστρια** fidicina.

**κιθαρῳδός** citharoedus, fidicen.

**Κικέρωνος** Ciceronis.

**κίκιννος** *cf.* cincinnus.

**\*κικκάμη** noctua.

**\*κικύμη** *cf.* caecuma (*expl.* noctua).

**κιλίκιον** *cf.* cilicion, cilicium.

**Κιμμερικὸν** πέλαγος (*Eins.*) Cimbricum mare

**κιμωλία** creta argentea, creta cimolia, creta sarda, serpyllum,
striga. *Cf.* collyrium.

**κίναιδος** eviratus, *cf.* cinaedi, -ων θεά Cicinnia. *Cf.* satipola
*et* κινδιδος II, 349, 33 *s. interpr.*

**κινάρα** (= κυν.) cardo silvester, carduus, carex, III, 264, 60 *s.*
*interpr.*

**\*κινάριον** *et* **κυνάριον** cardo 2., carduus.

**\*κινδιδος** *v.* κίναιδος.

**κινδυνευτέας** periclitandae.

**κινδυνεύω** periclitor (*et* -o).

**κίνδυνος** discrimen, exitium, periculum.

**κίνημα** motus 1.

**κίνησις** commotio, concitatio, gestus, motio, motus 1., nutatio, -
γῆς terrae motus, - δικαστική commotio, -ψυχῆς scrupulus.

**κινητός** mobilis.

**κιννάβαρι** *et* **κιννάβαρις** minium.

**κίνναμος** cinnamus.

**κιννάμωμον** amomum, cassia, cinnamomum, -ος III, 429, 24 *s.
interpr.*

**κινῶ** cieo, moveo, - ἐπὶ τοῦ ἐνάγω experior (-o), κινοῦμαι
consternor, κινούμενος mobilis, κινηθεῖσα ὅ ἐστι ῥιφεῖσα incita,
κεκινημένος agitatus, mobilis.

**κιονίς** cartilago, columella, columna, uva, - ἡ τοῦ ἀνθρώπου
columella, uva, - ἤτοι ἐπιστύλιον columella.

**\*κιονοπλος** columna.

**\*κιρκέας** *et* **κιρκίας** circius *sim.*

**\*κίρκειον** mandragora.

**κίρκι(ο)ς** circius.

**κίρκος** falco.

**κιρνῶ** misceo. *Cf.* κεράννυμι.

**κιρρός** rubeus, *cf.* cirrhon (*expl.* obridium aurum).

**κιρσοκήλη** *cf.* cirsocele (*expl.* venae vel nervorum extollentia
*etc.*).

**κιρσός** varix. *Cf.* κρισσός.

**κισηρίζω** pumico.

**κισήρι(ο)ν** pumex.

**κίσηρις** pumex.

**κίσσα** τὸ ὄρνεον pica.

**κίσσαρος** hedera.

**\*κισσεύς** cisseus. *Cf.* gumen hederae.

**κισσός** cissus, hedera, salvia, -ὸν μελανόν hedera nigra. *Cf.* gumen
hederae, χυλός.

**κισσόφυλλον** cyctaminus, opulus.

**κισσῶ** ὅ ἐστιν ἐπιθυμῶ gestio 2.

**κίστη** arca, cista.

**κίστος** mercurialis.

**κίτρινον** citreum.

**\*κίτριον** citreum.

**κίχλα** *et* **κίχλη** turdus.

**κιχρῶ** commodo, mutuo, -ῶμαι commodo (-or), mutuo 2. (-or), κέχρημαι
commodo.

**κιχώριον** (*vel* -όριον) *et* -α ambuvia, intubus, lactuca agrestis.
*Cf.* solsequia.

**κίων** ὁ *et* ἡ columna. *Cf.* μύδρος.

**κλαγγὴ** σάλπιγγος clangor.

**\*κλάδευμα** putamen.

**κλάδευσις** amputatio.

**κλαδευτήριον** putatoria falx, putatorium, runcilio.

**\*κλαδευτής** frondator, putator.

**κλαδεύω** amputo, defrudo, puto 1. *et* 2., - ἀμπέλους sarpo,
κεκλαδευμένος putatus.

**κλαδίον** ramulus, vitis.

**κλαδοειδής** ramosus.
