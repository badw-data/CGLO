**Lumbrix** σκώληξ II 434, 37. V lumbricus.

**Lumbrus** ἕλμις, κέστρος II 125, 9 (*ubi* Iumbricus *a*).

**Lumbus** ὀσφύς II 388, 35; 488, 63; III 248, 38; 470, 61; 571, 17. ψύα
II 481, 16. ἰσχίον, ψυάδιν II 515, 50. σφαίρωμα II 449, 16; 26. uertebra
II 586, 50 (*v.* uertebra). **lumba** ἰσχίον II 333, 39. **lubia**
lateres ubi cinguntur II 586, 47. **lumbi** ὀσφύες, ἰσχία II 125, 6.
ὀσφύες III 12, 52; 86, 4; 176, 8; 311, 54. ἰσχία III 349, 66; 409, 61.
ψύαι III 86, 5 (psoeae); 311, 55. sacmia (= σάγμια?) III 394, 70. νεφροί
III 248, 2 **lumbos** τὴν ὀσφύν II 125, 7.

**Lumen** αὐγὴ φωτὸς καὶ φῶς II 125, 8. φῶς II 474, 25; III 162, 31;
168, 60; 278, 43; 340, 22; 455, 15. φάος III 69, 50 = 637, 1; III 376,
51. † peton III 216, 35 = 651, 10 (ὑπαίθρων *Christ.* διὰ τοὺ πτεροῦ
*Krumbacher.* δ. τὸ ὀπτόν *Buech.*). ὅρασις II 506, 41. **lumina**
ὀφθαλμοί (pluraliter declinabitur, sed Vergilius singulariter dixit
\<*Aen.* III 663\>: 'luminis effossi fluuidum lauit inde cruorem') II
390, 50 (*cf. GR. L.* I 328, 10). ὄμματα II 383, 8; III 247, 21; 455,
16. ὁρατικὰ ὄμματα (*ex* 11) II 125, 12; III 174, 67; 571, 13. ὅρασις III
350, 33. oculi IV 111, 19; 256, 10. oculi [deuoratorum glutturum] V
572, 11 (*v.* lurco). **luminibus** φωσίν II 125, 14. *V.* in lumine.

**Lumen iuuentae** aetatis decus IV 450, 40 (*Verg. Aen.* I 590).

**Lumentum** *v.* lomentum.

**Luminaria** φωστῆρες III 425, 2. διαφανῆ φωτιστήρια λυχνικὰ (λυχνίδια
*Volkm.*) καὶ φῶτα II 125, 13.

**Luminarium** φωταγωγός II 474, 28.

**Luminarius** retiarius IV 112, 18 (linarius?).

**Lumine lustro** oculis circuminspicio V 114, 17 (*Verg. Aen.* II
754). *Cf.* IV 112, 3 + 4: **lumine lustro** lumine praespicio
[lustratio]: *ubi libri* lumen lustro, lumine praespicio lustratio
*exhibent*; **lu­mine lustro** lumine prospicio IV 112, 17. *Cf.
Nettleship 'Journ. of Phil.'* XIX 187.

**Lumine torno** truci, terribili IV 450, 42 (*Verg. Aen.* III 677.
*cf. Hagen Grad. p.* 58). diro aut (*vel* ac) truculento uultu IV 111,
10; 256, 26; V 114, 16.

**Lumino** φωτίζω II 474, 30.

**Luminosum** clarum V 643, 41 (*Non.* 132, 19).

**Luna** σελήνη II 125, 15; 430, 29; III 242, 20; 409, 52; 425, 5; 455,
17; 503, 24; 558, 12; 622, 30. μήνη III 499, 75. **Luna** Σελήνη III 8,
67; 83, 18; 168, 40; 348, 26; 393, 46. Cynthia, Phoeba IV 362, 21. Titan
III 521, 31 (Titania?). *Cf.* σελήνη **luna** foengus (φέγγος? Phoebe?)
III 72, 33. **lunae** σελήνης III 292, 50. *V.* noua luna, deminutio
lunae, deliquium lunae, defectio lunae, menstrua l., menstruosa l.,
surgere luna.

**Lunae cursum** σεληνοδρόμια III 426, 67.

**Luna (? *vertit* Σελήνη) mater Liberi patris** Σεμέλη μήτηρ τοῦ
Διονύσου III 291, 32.

**Luna pernocte** quae lucet tota nocte IV 415, 18.

**Luna plena** πανσέληνος II 393, 35. πληροσέληνον II 409, 51; III 293,
64; 425, 6. **luna prima** (?) πληροσέληνον III 169, 56.

**Luna semenstris** luna medii mensis V 635, 50; 603, 35. uel medii
mensis IV 112, 6.

**Lunaticus** σεληνιακός II 125, 16; 430, 30; III 206, 9. daemoniacus
III 602, 37. **lunatica** quod fit plenilunio IV 111, 11; 535, 25; V
219, 20; 603, 40 (fluit *pro* fit *Ianssonius ab Almeloveen*).
**lunatici** (?) quod fit plenilunio, hoc plenum (splenicum? *Buech.*)
dicitur V 309, 6. *V.* maenomenus.

**Lunatis peltis** scutis Amazonum immodum (!) lunae circumcisis IV 450,
43 (*Verg. Aen.* I 490; XI 663: *cf. Serv.*).

**Lunulae** ornamenta mulierum in lunae specie\<m\> factae V 572, 19/18.
**lunulis** menescillingas (*vel* meniscillingas, *AS.*) V 368, 32 (*cf.
AHD. GL.* I 589, 10). *Cf. Is.* XIX 31, 17.

**Luo** κλύζω II 351, 7. ἀποτιννύω II 241, 41. πλύνω II 410, 28.
**luis** persoluis poenas IV 256, 13; V 572, 7. **luit** patitur IV 415,
17. abluit IV 450, 44 (*Verg. Aen.* XI 849: luet). soluit, lauat uel
expendit IV 111, 8; 535, 22. soluit sicut lutio V 219, 18. dat, lauat,
commissa persoluit uel mortalitas V 308, 44 (*v.* lues). commissa
persoluit IV 535, 23; 111, 44. persoluit IV 362, 18. **luitis**
persoluitis V 572, 8. **luam** excipiam (expiem *Nettleship 'Journ. of
Phil.'* XIX 187) IV 111, 31. **luere** διαλοῦσαι II 124, 54 (*ubi*
διαλῦσαι *Vulc.* lue aera διάλυσαι *idem cum c*). **lues** solues
*Plac.* V 30, 4 = V 82, 6 = V 114, 28 (*cf. Serv. in Aen.* I 136; XI
842). **luet** τίσει II 125, 1 (lucet *cod.*). **luetis** persoluetis IV
111, 39; 450, 39 (*cf. Aen.* I 136); V 528, 2. poenas persoluitis IV
415, 23. **lui** ἐρυσάμην II 125, 3. **luit** ἀπέτισεν, ῥύεται (!) II
125, 4. **luerunt** absoluerunt ἀπὸ τοῦ λύειν IV 112, 10; V 464, 12.
**luebatur** sacrificabatur V 554, 19. *V.* reluo, luet poenam.

**Luocuntuli** *v.* lucunculus.
