**fidelem** caram, certam, stabilem IV 342, 43. **fideles** πιστάς III
153, 47. **fidelissimus** πιστότατος III 332, 39. *Cf.* III 332, 38.
*V.* pistos. *V. Don. in Phorm.* I 2, 26; *GR. L.* VII 97, 9.

**Fidelitatis** fides V 641, 38 (*Non.* 109, 28).

**Fideliter** πιστῶς II 408, 22. πιστικῶς III 153, 48; 339, 62.

**Fidem excessit** V 662, 9.

**Fidem implorat** V 662, 10 (Ter. *Ad.* 489?).

**Fidem soluo, fidem rumpo** ἀγνωμονῶ II 216, 51.

**Fidens** πεποιθώς II 401, 48. confidens IV 76, 28 (*Verg. Aen.* II
61).

**Fides** πίστις *post* II 71, 58; III 9, 28; 153, 46; 168, 38; 291, 24;
424, 45; 448, 53; 469, 10. fiducia IV 342, 44. ueritas IV 442, 23. dicta
quod dictum fiat V 650, 44 (*Non.* 24, 11). *V.* bona fide, mala fide,
cana fides, sine fide.

**Fides** χορδὴ ἡ νευρά II 477, 60. χορδαὶ λύρας (singularia non habet)
II 477, 58. νεῦρα τὰ τῆς κιθάρας II 375, 63. **fidis** λύρα, κιθάρα,
χορδαί II 71, 58. **fides** citharae IV 76, 34. cordae in cithara IV 76,
48 (fidae). **fede** (*vel* -ę) chordae in cithara V 456, 16. cordae in
cithara uel cenosae (*ad* foedae?) V 500, 41. **fidibus** cordis. fides
autem dictae quod fidem sibi seruent nec alterius sonos imitentur V 200,
17. cordis IV 76, 24; 442, 24 (*Verg. Aen.* VI 120: *cf. Serv. et GR.
L.* II 106, 1; 242, 4; IV 21, 24). cordis citharae IV 238, 39. filis
citharae, id est chordae V 294, 48.

**Fidibus scire** fidicinam esse V 534, 30 (*Ter. Eum* 133).

**Fidicen** κιθαριστής II 349, 29. κιθαρῳδός II 349, 30. λυριστής II 71,
57; 501, 58. qui cum cithara canit, a fidibus dictum V 361, 34.

**Fidicina** ψάλτρια ἡ κιθαρίστρια II 480, 5. citharistria IV 76, 50
(*Ter. Eun.* 457); 238, 48 (citharista); V 295, 6 (graece *add.*); 456,
17.

**Fidicinarius** λυριστής II 363, 17.

**Fidicino** λυρίζω II 363, 16.

**Fidicinus** citharoedus IV 76, 46.

**Fidicula** (*vel* fiducula) λύρα III 170, 5; 241, 42. βασάνου γένος II
256, 10. organa II 580, 15. citharoedus (*ubi* citharoeda Warren *cum
Hildebrando*) IV 76, 12; 238, 43; 519, 17; V 200, 18; 294, 57; 362, 39;
501, 4. citharam antiqui fidiculam uel fidicem (*cf. supra* citharoedus,
fidem *Arev.*) nominarunt, quia tam concinunt inter se cordae eius quam
bene conueniat inter quos fides sit V 200, 19 (*cf. Isid.* III 21, 4).
citharoedus uel ge­nus tormenti IV 342, 45. genera tor­mentorum [fuas
*Ampl. om. Epin. v.* fuam] V 361, 36. genera tormentorum sicut lamminae
IV 238, 45. genus tormentorum V 199, 23 (foed.). genus tormentorum sicut
[profetontide] lamminea (-ae?) V 456, 53. genus tormentorum sicut
lamminea V 501, 2. genus tormentorum IV 75, 51; V 200, 21. **fidiculae**
ὄνυχες οἱ εὶς τὰς βασάνους II 384, 34. cordae IV 238, 52. cordae
citharae IV 76, 11 (fiduc.); 342, 46; 519, 16; V 200, 20; 294, 55; 361,
42; 456, 55 (fiduculae); 501, 3 (*item*). sunt ungulae quibus torquentur
\<rei\> in eculeo adpensi (ad persas *R*) *Plac.* V 23, 15 = V 69, 17
(*cf. Isid.* V 27, 20 *unde* rei *adscivit Hildebrand p.* 143 *a*).
catenae V 362, 38 (fiduc.); 295, 1. *V.* admotis fidiculis.

**Fidicularius** φορτιαφορος (φορμιγγοποιός?) III 309, 55 (*lacunam
statuit Hagen progr. Bern. p.* 12). *Cf.* 56.

**Fidiculina** ὄργανον βασανιστήριον II 386, 21 (fidicula *e*).

**Fidifragus** refraga fidei IV 76, 33; V 456, 57. **fidifragi**
refragae fidei V 501, 5 (*quod ex* **foedifragus** refragus foederis
*factum censet Landgraf Arch.* IX 378. refragator *H*).

**Fidilla** *v.* fitilla.

**Fidissimi uades** ἐγγυηταί III 448, 50; 481, 37.

**Fidius** *v.* diuus filius, me dius f.

**Fido** πιστεύω II 408, 18. πέποιθα II 401, 47. θαρρῶ II 326, 33.
confido IV 342, 47. fidit confidit IV 442, 26 (*Verg. Aen.* V 69).

**Fiducia** ὑποθήκη II 466, 21; III 202, 55 (pudicicia *cod.*). παρρησία
καταχρηστικῶς II 562, 44. παρρησία II 399, 22. πεποίθησις, ὑποθήκη,
ἐνέχυρον II 71, 63. πεποίθησις II 401, 46. **fiduciam** ἄδειαν II 72, 2.
*V.* confidentia.

**Fiducialiter ago** παρρησιάζομαι II 399, 23. *Cf.* IV 361, 33.

**Fiduciarius** ὑποθηκιμαῖος II 466, 22. qui rem aliquam fiduciatam
accipit IV 76, 14 (pecunie *add. c*); 238, 34 (fiducia *cod. Sangall.
ubi* fiduciam *Warren*); 519, 10 (fidia accipiat); V 456, 56. possessor
V 362, 40; 628, 66. possessor, qui possessionem occupat pigneris nomine
per quanta libet et (quantumlibet?) tempus, sum\<mam\> pecuniae V 294,
60.

**Fiduciat** ὑποτίθεται II 71, 62.

**Fiduciatus** ὑποτιθέμενος II 71, 61.

**Fidunculus** πιστός II 71, 64 (*ubi* fidaculus *d*). *Cf.* fidustus.

**Fidus** πιστὸς φίλος II 72, 1 (*v.* fidelis). fidelis, credulus,
fretus (certus? *an con­tam. cum* fisus?) IV 342, 49. amicus uel fidelis
V 294, 56. amicus, fidelis, certus
