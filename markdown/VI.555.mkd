**Inburuclum** *v.* inuolucrum.

**Incaelatus** ἄγλυφος II 216, 37. ἀτόρνευτος II 250, 23.

**Incaen-** *v.* **encaen-.**

**Incaestum** *v.* incestum.

**Incaesus** ἀτύπτητος II 250, 33. ἄπληγος II 235, 18. ἄδαρτος II 218,
13. non falgellatus (flag. *b*) II 583, 8.

**Incaluit** ualde ferbuit IV 90, 30; V 303, 59.

**In Campania** mons Vesuuius qui cotidie ignem exhalat V 570, 40.

**In canali** scolasticus de foro V 503, 52 (*v.* inforare). **in
canalibus** in angustis locis V 365, 24. locus in fines (!) Africanorum
V 305, 12 (Canariae ins.?).

**Incandidus** ἀλεύκαντος II 224, 52.

**Incanigenia** primigenia V 210, 9.

**Incantatio** ἐπῳδή III 451, 18.

**Incantator** ἐπῳδός III 271, 32; 433, 17.

**Incanto** ἐπᾴδω II 305, 8.

**Incanus** σπαρτοπόλιος II 435, 24. sine canitie, sparsicanus,
aliquatenus canus II 582, 46. **incana** iuuenalia V 210, 8 (*Verg.*
*Aen.* VI 809).

**Incapabilis** ἀχώρητος II 254, 54 (incapabius *cod. corr. a e*).

**In capessendo** (*vel* capis-) in accipiendo IV 93, 12; 525, 32; V
302, 29 (in capiscendo).

**Incapitalis** ἀκέφαλος II 222, 39.

**Incapitatum** sine capite V 210, 10.

**Incapito** ἐπάρχομαι III 451, 19. ἐπάρχομαι, id est capitationes facio
III 483, 20.

**In carectam** (!) in locum palustri (palustre?) V 570, 38. *V.*
carectum.

**Incassum** sine causa, frustra IV 413, 6. sine causa IV 100, 38
(*Verg. Aen.* III 345). in uacuum, inane, sine causa IV 525, 30. in
uacuum V 302, 44. superuacuum, inane et sine causa IV 351, 25. inane ac
\<su\>peruacuum IV 91, 46 (superuacuum *iam Nettleship 'Journ. of.
Phil.'* XIX 126). superuacuum, inane IV 247, 51. infructuosum, inaniter
IV 92, 40. *Cf. Arch.* II 13; 14; 22.

**Incastratura** coniunctio uel conglutinatio V 620, 14. *V.* ancon,
anconiscos.

**Incastus** *v.* incestus *adiect.*

**Incaute** ἀπερισκέπτως II 234, 42.

**Incautus** ἀφύλακτος II 254, 8; III 334, 9. inprouidus, incustoditus
IV 351, 26. **incauta** nescia uel inprouida IV 101, 28. **incautum**
securum IV 92, 43 (incatum); 446, 6. inscium IV 525, 52 (*Verg. Aen.*
III 332?).

**Incauillatione** inderisione V 210, 6. *Cf. Festus Pauli p.* 107, 16.

**Incauillatur** inderidetur V 210, 7. *Cf. Arch.* IV 80.

**Incedens** ambulans IV 91, 15.

**Incedo** πρόειμι II 418, 5. προβαίνω II 416, 21. ἐπιβαίνω II 307, 8.
**incedit** ingreditur IV 351, 30. ambulat, praecedit (*vel* proc.) IV
248, 2. **incessi** inuasi IV 100, 33. **incessit** ingressus est IV 90,
41 (*Verg. Aen.* I 497). inuasit V 535, 47 (*Ter. Andr.* 730).
incurrit V 302, 49; (*de Euseb.*) V 421, 66; 430, 53. incucurrit (*vel*
incurrit) IV 526, 19. incurauit (incursauit? *v.* incursant, incusauit?
*v.* incessunt) IV 351, 37. **incesse­rat** intrauerat IV 91, 20.

**Incelatum** ἄκρυπτον II 224, 11.

**Incelebratum** (inlecebrarum *cod.*) in desertum V 301, 59 (in inc.?).

**Incelebre** desertum IV 92, 38; 247, 53 (deserto); V 210, 11; 522, 39.
desertum, desolatum IV 351, 28. desertum nec nominatum V 503, 64.
**inlecebrum** in desertum IV 525, 60 (in inc.?).

**Incenatus** ἄδειπνος II 218, 15. non adhuc cenans II 583, 14 (cenatus
*Loewe*).

**Incendebat fulgor** inluminabat IV 446, 9 (*Verg. Aen.* V 88).

**Incendiaria** πρηστήρ III 433, 8.

**Incendiarius** ἐμπρηστής II 296, 49. impiristis III 179, 37
(ἐμπυριστής?). ἐμπυριστής III 251, 62 (*vetusta*?). incensor ignis II
583, 28 (igniarius *Loewe*; *cf. Gallée* 360). *V.* incendo.

**Incendium** ἐμπρησμός II 296, 48; 500, 35; 526, 41; 536, 41.
ἐμπυρισμός II 544, 5; III 139, 45. καύσων III 558, 10; 622, 29. ur IV
351, 31 (*Roensch Mus. Rhen.* XXX 751). **incendia** clades, aerumna IV
446, 7 (*gl. Verg.*).

**Incendium facio** ἐμπυρίζω II 296, 54. ἐμπρήζω II 296, 47. ἐμπιμπρῶ II
296, 28.

**Incendo** ἐμπρήζω II 296, 47. ἐμπυ­ρίζω II 296, 54; III 139, 42; 341,
9; 451, 21. ἐμπιμπρῶ II 296, 28. ἐπιθύω III 171, 14; 238, 71.
**incendis** ἐμπυρίζεις III 139, 43. **incendit** ἐμπυρίζει III 139, 44.
excitauit IV 100, 45 (*Verg. Aen.* IV 197). **incendere** adolere IV
446, 8 (*Verg. Aen.* IV 360). *Cf.* **incendistis** ἐμπυριστής III
139, 46 (*ubi a* incendiarius ἐμπυριστής). *Cf.* exuro.

**Incenis** Plaut. Casina (438): incenem ex aedibus *Scal.* V 601, 70
(*= Osb. p.* 115; *cf. Loewe Prodr. p.* 51).

**In cenoleis** *v.* in coenobiis.

**Incensare** (-ere *a c*) inritare IV 351, 29.

**Incensio** κατάφλεξις II 344, 59.

**Incensor** ἐπιθύτης III 238, 72.

**Incensum** θυμίαμα II 329, 54; III 145, 32; 273, 37; 301, 51; 338, 47;
451, 22; 522, 10. θυμίαμα, λίβανος καὶ
