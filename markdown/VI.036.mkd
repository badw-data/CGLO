(*cf. Loewe GL. N.* 8). uitium ferri, erodens ferrum (er. f. *om. R*),
ab erodendo dicta, non ab aeramento (non ab aer. *om. G*) *Plac.* V 20,
3 = V 65, 22 (*sub* E *littera*). *Cf. Isid.* XVI 21, 5. rost (*AS.*) V
359, 60. **aerugine** ἰός III 539, 38. ἰάριον III 539, 61 = 565, 51
(*cf.* ἰάριον id est ἰός III 566, 29 = ἰός ἰάριον (ἰάριν) III 591, 66).
glaucium (*cf.* uiola: ἰός *et* ἴον *confusa*) id est **aerugine** III
624, 74.

**Aerugo aeraminis** ἰὸς χαλκοῦ (aerugine) III 539, 39; 565, 5.

**Aerugo campana** (*vel* aerugine) ἰός siue ἰάριν III 546, 75; 583, 41.
ἰάριν III 547, 16; 566, 55; 583, 35; 591, 43; 612, 68; 625, 14.
ὑποκυστίς (hipoquistide) III 546, 59.

**Aerugo cupri** (aerugine cipri) ἰὸς κύπρου III 539, 41; 565, 7.
**aerugo de cupro** λεπίδες χαλκοῦ III 566, 70.

**Aerugo ferri** (*vel* aerugine) ἰὸς σιδήρου III 565, 6; 20; 583, 37;
589, 71; 611, 31; 623, 32. λεπίδες στομώματος (*h. e.* aciei) III 568, 1
(*cf.* III 566, 71 *squ.*); 584, 18. μελαντηρία III 568, 30.

**Aerugo rasa** (*vel* aerugine) ξυστοῦ ἰοῦ III 539, 40 (situ *cod.
corr. Stadler*). μελαντηρία III 584, 44.

**Aerumna** ταλαιπωρία II 12, 26; 451, 17. δυστυχία II 282, 20. ἄτη II
250, 2. miseria IV 65, 31; 233, 4; V 546, 12. calamitas IV 515, 23; V
290, 36. **ermana** calamitas (*cf. Caper GR. L.* VII 109, 10: erumna,
non eramna) IV 65, 7; 232, 49; 509, 7. miseria uel calamitas IV 86, 9;
306, 16; V 262, 42. calamitas, miseria uel labor aut infelicitas IV 12,
14. miseria, calamitas, labor, infelicitas, clades IV 475, 39. calamitas
uel miseria aut labor IV 65, 1. labor uel miseria IV 11, 31. labor,
inopia IV 204, 22. clades uel labor V 300, 13. pestilentia, calamitas IV
337, 7. **erumnis** luctus (?*cf. Pers.* I 78) IV 515, 24; V 290, 44.
*V.* mulcantem aerumnas.

**Aerumnosus** μοχθηρός II 373, 34. ἀθλιώτατος II 62, 53. ἀτηρός II 250,
4. miser, querulus II 565, 16. **aerumnus** σκληρός II 433, 41
(aerumnosus *a*). infortunus (*nisi* aerumnis infortuniis *subest*) V
344, 33. **aerumnuosum** miserrimum V 453, 54.

**Aeruscans** aes minutum (*adde sequentem glossam* accurate contrahens
[construens *R*]: *v. sub* accurate: *ubi* colligens *addidit O.
Mueller*) *Plac.* V 7, 32 + 33 = V 45, 19 + V 43, 18. *Cf. Festus Pauli
p.* 24, 7; *E. Ludwig Fleckeiseni Ann.* 1879 *p.* 768. *V.* accurate.

**Aes** χαλκός II 502, 8; 507, 4; 525, 34; 543, 24. χαλκός hoc **aes**,
aeris, plurale aera II 475, 5 (*GR. L.* I 34, 20, *alibi*). **aes**
χαλκός III 23, 5; 93, 70; 163, 57; 203, 48; 325, 6; 368, 18. κέρμα II
348, 25; III 202, 31; 274, 14. ἀργύριον II 244, 6. φόλλις II 472, 44.
aeramen IV 11, 42 (aere *codd.* aeramentum *abc*). aeramentum IV 204,
23; 306, 17; 474, 14 (aere); V 437, 56. **aes** aeris V 346, 43.
**aera** χάλκεια III 368, 19 (aerea?).

**Aes alienum** δάνειον II 266, 25. χρέος; δάνειον II 12, 35. χρέος II
478, 23; III 336, 56/57 (aes asensum: *corr. Boucherie*). **aes
\<alienum\>** pecunia feneraticia IV 474, 13. **aes alienum** dubiae uel
(debitae?) pecuniae V 262, 62; 63. debitum uel pecunium (!) V 344, 34.
**aere alieno** gaebuli (*AS.*) V 342, 14. *V.* aere alieno onustus et
aggrauatus, aere alieno uacillat, alienum aes.

**Aes cauum** tuba \<a\>ut quicquid cauum aeris V 262, 61. **aere cauo**
tuba IV 405, 12. pro aeris caui (tuba *de*) IV 428, 26 (= Verg. *Aen.*
III 240. 286). *Hinc emenda:* **aerectatio** tuba V 344, 31.

**Aesculapius** Ἀσκληπιός II 247, 54; III 8, 45; 82, 75; 167, 42; 236,
45; 290, 50; 400, 31; 509, 24. nomen proprium cuiusdam medici II 565,
15. **Hesculapius**, Asclepius, medicinae inuentor *Plac.* V 25, 11 = V
74, 2 = V 108, 37.

**Aesculator** χαλκολόγος II 12, 36; 475, 2. aeris elector (*ubi*
conlector *Loewe GL. N.* 9) II 565, 23. aeruscator *Cuiacius apud
Philoxenum collato* aeruscans: *at cf.* aesculor.

**Aesculetum** φηγών II 525, 29.

**Aesculor** χαλκολογῶ II 475, 3. *Cf. GR. L.* VII 430, 4.

**Aesculus** εἶδος δένδρου, φηγὸς βαλανοφόρος II 12, 31. φηγός II 470,
41. uorax II 565, 8 (*male versa: cf. Loewe GL. N.* 8). arboris nomen
est (*ita libri*) glandariae, ab esca dicta, quod ante usum frumenti
haec arbor uictum mortalibus praebebat *Plac.* V 4, 8 = V 45, 20. genus
arboris IV 12, 10; 204, 17; 474, 17; V 262, 38. genus herbae IV 306, 18.
boecae (*AS.*) V 339, 7. ab edendo, beccae [enfatibus] V 359, 35
(*del. Oehler: an* fagus ?). **aesculum** genus arboris IV 12, 25; 476,
40. *Cf. Isid.* XVII 7, 28.

**Aestas** θέρος, θερεία II 12, 30. θέρος II 327, 64; 499, 33; 521, 36;
544, 66; III 75, 43; 83, 28; 242, 46; 261, 27; 294, 32; 427, 30; 504,
36; 521, 60. unum de his temporibus IV 306, 19.

**Aestas prima** adulta, praeceps V 551, 54; 552, 1. *Cf. Serv. in Aen.*
I 430.

**Aestate noua** uerno tempore IV 428, 29 (= *Verg. Aen.* I 430); 475,
26; V 262, 68.
