apertio terrae IV 349, 2. fissura, uorago IV 244, 46. patefactio IV 349,
3. profundus (!) IV 86, 50. **hiatibus** profunditatibus IV 86, 52
(*Verg. Aen.* VI 576).

**Hiatus terrae** patefactio terrae IV 86, 14; 524, 13; V 300, 42.
**terrae hiatus** χάσμα III 473, 58.

**Hibera** Σπανή II 435, 12.

**Hiberas** Stygias, infernales [suasorias blandientes] V 503, 29
(inferas?).

**Hiberi** Spani V 300, 53. gens in Ponto et latrones Hispani V 552, 36
(*cf. Serv. in Georg.* III 408).

**Hiberia** Hispania IV 524, 21. **Hiber** Hispania ab Ibero flumine
dicta V 503, 18 (*Serv. in Aen.* XI 913; *cf. Isid.* XIV 4, 28).
**Hiberia** Spania IV 87, 15; 27; 244, 14. spungia IV 86, 23. *V.*
Hispania.

**Hibernaculum** hiemale II 581, 53.

**Hibernalis** χειμερινή II 476, 16.

**Hiberna loca** calida propter hiemem facta IV 349, 6; 244, 42 (*Serv.*
*in Aen.* IV 143).

**Hibcrnatio** παραχείμασις III 157, 6. παραχειμασία III 450, 28; 482,
9.

**Hiberni chori** uenti tempestates concitantes IV 445, 1 (*Verg.*
*Aen.* V 126).

**Hiberno** παραχειμάζω II 397, 17; III 157, 2. χειμάζω III 80, 70.
**hibernas** παραχειμάζεις III 157, 3. **hibernat** παραχειμάζει II 68,
43; 561, 35; III 157, 4. hiemat IV 349, 5. **hibernare** παραχειμάσαι
III 157, 5.

**Hiberno sidere** hiemis tempore IV 86, 22; 524, 23 (*Verg. Aen.* IV
309).

**Hibernum** παραχειμασία II 397, 18; 503, 64; 547, 42. *V.* hibernus.

**Hibernum tempus** χειμερινὸς καιρός III 295, 51; 524, 58.

**Hibernus** χειμερινός II 476, 18. ut tempus aut mensis IV 349, 7. ut
mensis V 300, 45. **hibernum** χειμερινόν II 476, 19. ut tempus V 300,
46. χειμερινή II 530, 40 (*substant.*). φύσημα III 294, 1. **hiberna**
gelida, frigida IV 444, 42 (*Verg. Aen.* I 266). ut tempora et castra
militum ubi hiemant dicimus, ut hibernis castris V 300, 49. tempora uel
castra militum ubi hiemant; IV 349, 4. **hibernas** hibernas magnas et
turbidas, hoc Vergilius (*cf. Georg.* IV 235). Plautus enim: increpui
\<hi\>bernum (*Rud. prol.* 69) V 207, 38. **hibernis** hiemalibus V
415, 55 (*lib. rot.* = *Isid, de nat. rer.* VII 2); V 425, 19 (*item*).

**Hibiscum** (*cf. Pseudap.* 39) ἀλθαία βοτάνη ἤτοι ῥίζα II 225, 1.
ἀλθαία III 301, 22; 507, 24. μαλάχη αγρία II 364, 32. ὀξύσχοινος II 384,
61. biscopuuyrt (*AS.*) V 364, 55. **hibiscus** ἀγριομαλάχηII 217, 23.
ὀξύσχοινος II 384, 61. ἀλθαία III 487, 48. **euiscus** uel **ibiscus**
ἀλθαία III 549, 30. **hiuiscus** ἀλθαία III 565, 9. **euiscus** ἀλθαία
III 587, 36; 590, 27; 608, 9; 611, 50; 623, 62. **euiscus** iscus
(hibiscus?) III 591, 65, **euisco** ἀλθαία III 631, 4. **hibisco**
ἀλθαία (alfea *codd.*) V 459, 45; 503, 24. herba mollis IV 524, 16.
**euisca** ἀλθαία III 543, 15. **euiscu** ἀγρι*\<*ο\>μαλάχη III 552, 51.
ἀλθαία ῥίζα III 552, 50. **euiscus** dentrolimolum (anadendromalachen
*Pseudap.*) III 560, 32. mola ceratica (malache cretice *Pseudap.*) III
568, 63. onsinaca (osyriacam *Pseudap.* 41) III 571, 47. siccophilla
(*cf. Pseudap.*) III 577, 3. **ibiscum** est genus frondis, in arboribus
nascitur, unde hieme animalibus datur cibus V 209, 16. **ibiscus** genus
uirgulti Virgilius (*Ecl.* X 71): et gracili fiscellam texit ibisco V
209, 17. genus uirgulti quod pastores pro flagello utuntur V 209, 18
(*Verg. Ecl.* II 30). *V.* althaea, malua.

**Hic** οὗτος, ἐνταῦθα (*contam.*) II 68, 39. οὗτος II 390, 32. ὅδε III
81, 32; 407, 49; 450, 29. illic, ut 'hic cursus fuit' V 570, 3 (*Verg.*
*Aen.* I 534). **haec** αὕτη II 251, 31. ταῦτα, αὕτη II 68, 4. **hoc**
τοῦτο *post* II 69, 40 (*cf.* **hoc ho** τοῦτο, ταύτῃ II 69, 6); 457,
49. idem unum uel statim (ilico?) IV 524, 48. **hocce** τουτοΐ II 457,
52. **hoce** hoc autem IV 349, 28. **huius** τούτου II 69, 32; 457, 54.
**huiusce** τουτου[τ]ί II 69, 33. **huiusque** huius uero IV 349, 55.
**hunc** τοῦτον II 69, 39; III 79, 61. *Cf.* **hanc** sta\<m\> IV 85,
27. **hac** ταύτῃ II 452, 6. **hi** οὗτοι II 68, 38; 390, 33. **hisce**
antique pro hi V 534, 56 (*Ter. Eun.* 269). **hae** ταῦται (!) II 67,
45. **haec** stae V 108, 7. ταῦτα II 67, 44; 452, 5; III 407, 48. **ha**
ταῦτα II 67, 43. **hea** ipsa uel dicenda V 459, 31. **harum** τούτων II
458, 4. starum V 299, 53. **hisce** istis IV 412, 26. **hosce** hos
autem V 364, 36. **osce** os est: sed Romani inuenerunt syllabam V 317,
16. **hosce** hos uero IV 412, 30. modo (?) IV 88, 6. **has** stas IV
85, 1. **hasce** has autem IV 85, 29; 242, 46; 348, 27. **his** istis V
109, 23. **hisce** istis V 208, 8. *V.* ad haec, ad hoc, ab his, ob hoc,
super haec.

**Hic** ἐνταῦθα II 68, 39; 300, 16. ὧδε II 481, 48; III 7, 42; 450, 30.
ἐνθάδε II 299, 8. istic (*vel* stic) IV 524, 7; V 300, 23. *Cf.* **si
hic est** III 515, 63.

**Hic adque hic** ἔνθα καὶ ἔνθα II 299, 9.

**Hic cursus** haec nauigatio IV 444, 32 (*Verg. Aen.* I 534). *Cf.*
hic.

**Hic habet minus inter h et i folia quattuor quae excisa fuerunt de
exemplari:** *de his cf. Loewe Prodr. p.* 10.
