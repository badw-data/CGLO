**κατασκοπῶ** contemplor (-o), exploro, impetro, speculor.

**κατασπεύδω** urgeo.

**κατασπορά** seminatio.

**κατασπουδάζω** circumvenio.

**κατασπῶ** deduco, detraho.

**κατασταγμός** destillatio.

**καταστάζω** destillo, liquor.

**καταστακτική (?)** batrachion.

**κατασταλτική** *cf.* fomentatio, sumentano.

**κατάστασις** elogium, status.

**καταστέλλω** compesco, placo, sedo.

**\*καταστέρισις** constellatio.

**κατάστημα** statura, status.

**καταστίζω** depungo, - ἐπὶ ὕβρεως denoto, κατεστιγμένος sedatus.

**κατάστικτος** destinctus. *Cf.* ἰχθύς.

**καταστολή** II, 344, 10 *s. interpr.*

**καταστρέφω** demolior, destruo, diruo, everto, - ει deruit, -ομαι
deleo. *Cf.* grumat.

**καταστροφεύς** eversor, tergiversator.

**καταστροφή** destructio, excidium, subversio. *Cf.* catastrophon
(*expl.* conversationem).

**κατάστρωμα** forus.

**καταστρωννύω** prosterno, sterno, κατεστρωμένος prostratus.

**κατάστρωσις** stratura.

**\*καταστρωτής** scansor.

**κατασύρω** detraho.

**κατασφαγή** internecio, occisio.

**κατασφάζω** interficio, perimo, trucido, -σφαγείς interemptus.

**κατασφραγίζω** consigno, designo, obsigno.

**κατασχάζω** *cf. s. v.* phlebotomum.

**κατάσχεσις** deprehensio, retentio.

**κατασχίζω** conscindo, descindo, scarifo, -ει scalpellat.

**\*κατατακτικῶς** ordinate.

**καταταράσσω** diverbero.

**κατατάσσω** *et* **κατατάττω** deputo, digero, dispono, imputo,
ordino, -ταχθείς deputatus.

**κατατήκω** afficio, liquefacio, -ομαι tabesco.

**κατατίθημι** depono, profiteor, -εμαι condo, prosequor, -εμαι εἰς
μνήμην condo.

**κατατομή** concisio, decisio.

**κατατρέχω** commeo, decurro, emano, καταδράμωσιν demanaverint,
κατατρέχων decurrens.

**κατατρίβω** attribo, detero, infrico.

**κατατρυπῶ** penetro, terebro.

**κατατρύχω** affligo.

**κατατρώγω** comedo 1.

**κατατυγχάνω** consequor.

**καταυγάζω** illumino.

**καταφαγᾶς** cibicida, comestor, comisator, devorator, ganeo, helluo,
nepos, vorax, -άδες gulatores.

**\*καταφάγεται** devoro (devorabit).

**καταφανής** conspicuus, perspicuus.

**καταφερής** obscenus. *Cf.* κατωφερής.

**καταφέρω** deduco, defero, deicio, depono, depromo, deveho, perfero,
-ομαι defero; κατενεχθείς delatus, -ὲν ἐπὶ τοῦ πεσόντος deiectus
(-um), - ἐπὶ τοῦ κτηθέντος delatus (-um), -εῖσα κληρονομια delatus (-a).

**καταφεύγω** confugio, decurro.

**\*κατάφημος** famosus.

**καταφιλῶ** basio, osculor.

**\*καταφιμῶ** conticisco.

**καταφλέγω** aduro, conflagro, exuro, inflammo, oburo.

**κατάφλεξις** combustio, incensio.

**καταφλυαρῶ** obgannio.

**καταφορά** invectivus (-a), procella, - τῶν ἀνέμων procella.

**καταφορικός** invectivus.

**κατάφορος** proclivus.

**κατάφρακτος** cataphracta, cf.cataphractus (*expl.* eques munitus,
equus loricatus).

**καταφρόνησις** contemptio, contemptus, contumacia, despectio.

**καταφρονητής** contemptor, contumax, detrectator, spernibilis.

**καταφρονῶ** asperno, contemno, despicio, sperno, supersedeo, temno,
-ηθείς contemptus, despero (-at), spernatus, spretus.

**καταφυγή** confugium, effugium, perfugium, refugium, subterfugiam.

**καταφύγιον** perfugium, refugium.

**καταφυρῶ** decoquo.

**καταφύτευσις** complantatio.

**καταφυτεύω** complanto, -ομαι consero.

**καταχαράσσω** cancello, scarifo.

**καταχέζω:** κατέχεσες concacasti.

**καταχέω** perfundo.

**καταχθόνιος** infernus, -οι inferus (-i), -α inferilia. *Cf.* δαίμων, θεός.

**κατάχρεος** feneus.

**κατάχρησις** abusio, usurpatio.

**καταχρηστικῶς** abusive, vulgo.

**καταχρίω** oblito, -κεχριμένος oblĭtus.

**καταχρῶμαι** abutor, usurpo, κατακέχρημαι abutor (-o, *v. p.* 434).

**κατάχυμα** perfusio, - ζωμοῦ tucca.

**\*καταχύννω** perfundo.

**καταχωννύω** obruo.

**καταψεύδομαι** commentior.

**\*καταψεύστης** commentor 2.

**καταψηφίζομαι** decerno.

**καταψήχω** demulceo, frico, mulceo, permulceo, κατέψηξα demunxi.

**κατάψυξις** perfrictio.
