246, 38. ἀρχὴ πράγματος II 246, 45. ἀρχὴ βιβλίου ἢ χάρτου ἢ ἄλλου τινός
II 246, 46. προοίμιον II 66, 29; 419, 34. δίασμα III 323, 71. initium IV
66, 37; 72, 4 (= *Non.* 30, 22). initium, prin­cipium sermonum IV 339, 1.
**exor\<d\>iae** nascentiae IV 235, 1; 514, 2. **exordiis**
\<pr\>incipi\<i\>s. proprie exordium est quod in causa facit dicturus
orator V 196, 31 (*cf. Hagen Grad. ad cr.* 79).

**Exordium negotii** V 661, 66.

**Exorior** ἀνατέλλω III 128, 67. **exoritur** exsurgit IV 71, 16
(*Verg. Aen.* II 313). **exoriare** exoriaris, nascaris IV 441, 19
(*Verg. Aen.* IV 625: *Hagen Grad. ad cr.* 46). nascere IV 71, 32
(*cf. Funck Comm. Woelffl. p.* 46). **exor\<i\>etur** ἐπαναστήσεται,
ἐγερθήσεται, ὑψωθήσεται II 65, 29. **exorta est** ἀνεφάνη II 66, 60
(extorta *cod.; cf. adnot. criti*); 226, 37. *Cf.* **exorere** exsurgere
IV 69, 43 (exsurgis? *cf. Ter. Hec.* 213).

**Exormis** inmanis IV 235, 56. **exorius** inmanis IV 411, 11.
**exornis** immanis V 597, 61; 598, 52. *an* exnormis = enormis? *Cf.
Quicherat add. lex. l.* 97.

**Exornatus** ualde ornatus IV 66, 50; 513, 31; V 292, 30 (*Ter. Eun.*
683). **exornatum** deformatum IV 68, 28 (*Ter. Heaut.* 950). *Cf. Is.*
X 82.

**Exorno** διακοσμῶ II 272, 1. ἐκκοσμῶ II 291, 5. **exornare** perornare
(pro ornare?) V 641, 11 (*Non.* 105, 20).

**Exoro** δυσωπῶ II 282, 36. ἐξευμενίζομαι II 303, 13. ἐξιλεοῦμαι II
303, 35. **exoro te** optineo te IV 69, 34. inpetro V 533, 31 (*Ter.*
*Andr.* 592). **exorat** ἐξιλάσκεται, δυσωπεῖ, ἐνεύχεται (*ubi*
ἐξεύχεται *Vulc.* ἐντεύχεται *Ducange*) II 65, 24 (*Verg. Aen.* III
370). supplicatur IV 339, 3. **exorare** cum impetratione [ex]orare V
292, 50. **exoramur** παρακαλούμεθα, δυσωπούμεθα II 65, 26.

**Exorsus** incipit loqui IV 234, 8. loqui coepit IV 71, 1. incipiens
loqui IV 70, 35 (exorsum). locutus IV 71, 25; 513, 53; V 291, 37.
initians IV 339, 2. **exorsum** ἀπόρρητον (exosum?) II 240, 24.

**Exortus** natus IV 71, 3; 234, 52. abortus IV 339, 8. **exorta**
ἀρξαμένη II 245, 38 (exorsa?). nata IV 69, 47 (*Ter. Hec.* 632). nata
[benedicit] V 454, 36. **exortum** paruum, uacuum uel ab initio IV
339, 7 (*v.* exparta). **exortam** natam, procreatam IV 441, 13. *V.*
exorior.

**Exos** sine ossa (!) V 640, 69 (*Non.* 103, 6).

**Ex ossibus** ex genere IV 71, 33 (*prae­cedit* exoriare); 514, 24
(*Verg. Aen.* IV 625).

**Exosso** ἐξοσταΐζω (ἐξοστεΐζω *e*) II 304, 18. **exossiat** ἐξοσταΐζει
(ἐξοστεΐζει *e*) II 65, 25.

**Exosus** μεμισημένος II 367, 33. odio habitus IV 67, 14; 339, 9
(exosum), odiosus IV 234, 40. odio habitus aut odiosus IV 514, 25; V
292, 14 (uel). qui aliquem odit, odiosus IV 339, 10 (*Isid. Hiff.* 182).
*Cf. Verg. Aen.* V 687. **exosum** odibilem IV 67, 51. odibile IV 514,
26 (*cf. a c* IV 67, 51). **exoticum** odibilem (*contam.?*) IV 69, 2;
V 196, 33.

**Exoticus** alienus V 196, 34. peregrinus V 196, 35. **exoticum**
peregrinum V 641, 25 (*Non.* 108, 1; 540, 18). nomen est graecum, id est
peregrinum, de foris ueniens. dicimus enim 'exoticum uinum', 'exoticam
uestem', 'exoticum mancipium' *Plac.* V 19, 12 = V 67, 12. *Cf. Is.* XIX
22, 21. **exotica** forinseca *cod. Leid.* 67 F^5^ (*cf. Loewe Prodr.*
429). *V.* exosus.

**Expalmo** ῥαπίζω II 427, 31 (*Roensch Coll. phil.* 26). ἐξαλαπίζω II
301, 42. **expalmare** depellere (dealapare *H.*) *Seal.* V 599, 15
(*Osb. p.* 199). **expalmauit** ἐράπισεν III 67, 16.

**Expalpare** elicere V 641, 1 (*Non.* 104, 7).

**Expando** ἀνοίγω II 228, 17. ἐξανοίγω II 301, 50. **expande** ἅπλωσον
II 235, 37.

**Expapillato** nudato V 640, 68 (*v.* excapillato *et* effafilato,
*Loewe Prodr.* 269; *Funck Arch.* IV 85).

**Exparta** partu uacua IV 68, 34; V 598, 3. ex (*om. b c*) partu uacua
IV 508, 45. **expartu** partu uacua V 291, 61. **exparta** parua seu
uacua V 454, 37. **exarta** uacua uel parua IV 337, 54. **exparta**
partu uacuata V 196, 37. **experita** parua uacua IV 236, 19.
**exparata** euacuata V 196, 36. **expartam** partis uacuam V 597, 64.
*Cf.* epartua; *Hagen Grad. ad cr.* 92, *Landgraf Arch.* IX 376. *V.*
exortus.

**Ex parte** ἐκ μέρους II 291, 44; III 141, 27.

**Expatesco** ἀναφαίνω III 447, 62; 480, 59.

**Expatro** ἐκκρίνω ἐπι συνουσίᾳ II 291, 8. εσσινω (ἐκκινῶ?) III 480,
56. **expatrauit** ἐπετέλεσεν II 65, 30 (*Catull.* 29, 17).

**Expauesco** ἐκπλήττομαι II 292, 14. θροοῦμαι II 329, 31. πτοοῦμαι II
425, 44. φοβοῦμαι II 472, 35. **expabllit** *v.* exsto, extabescit.

**Expectorat** extra pectus eicit IV 235, 17 (*cf. Festus Pauli p.* 80,
7; *Non.* 16, 1). **expectorare** extra pectus iacet (?) V 499, 51. de
consilio deicere V 650, 40 (*Non.* 16, 1).
