**conuicti** confutati, coniuncti IV 325, 12 (*h. e.* conuincti). *V.*
confutatus.

**Conuictus** συμβίωσις II 441, 55; 444, 36; 488, 4.

**Conuincens** oberstelendi (*vel* obaerstaelendi; *AS.*) V 350, 37.

**Conuincibile** est quod euidenti ratione conuincitur, sicut fecit
Cicero pro Milone (29, 79): eius igitur mortis sedetis ultores, cuius
uitam si putetis per uos restitui posse, nolitis (*Isid.* II 9, 10) V
185, 18. *Cf. Cassiod. de rhet.* 13.

**Conuinco** ἐκνικῶ II 291, 54. ἐλέγχω II 294, 42. **conuincit**
ἀπελέγχει, νικᾷ II 115, 35. ἀπελέγχει, νικᾷ, ἐκνικᾷ II 115, 37.
redarguit, arguit, coarguit IV 325, 11. **conuicisse** (*immo*
conciuisse: *cf. Ter. Hec.* 313) concitauisse IV 42, 32.

**Conuinctus** συνδέσμιος II 444, 40.

**Conuinna** genus uehiculi IV 42, 36; 498, 38; V 185, 17 (est *add.*).
*V.* couinnus. *Cf. Nettleship 'Contr.' p.* 424; *schol. Lucani* I 426.

**Conuinxit** et **conuincit** συμπλέκεται, συνζευγνύει II 115, 47. *V.*
conecto.

**Conuitat** συνερεται (*ubi* conbitat συνέρχεται *Vulc.* comitat
*Buech.*) II 116, 1.

**Conuiua** συμβιωτής II 441, 56. συμπότης II 443, 9. σύνδειπνος II 444,
38 (conciua *cod. cf.* concena). συνέστιος II 445, 36.

**Conuiuatio** et **conuiuium** συμπόσιον II 116, 10.

**Conuiuator** *v.* conuictor.

**Conuiuax** ὁμόβιος II 383, 10 (conuiua *Ducange*).

**Conuiuiones** *v.* combibiones.

**Conuiuium** συμπόσιον II 443, 8; III 172, 3; 239, 53. συμποσία (!) II
503, 12. συμβίωσις II 441, 55. ἀλογία III 489, 62 (*v.* alogia).
συνεστίασις II 445, 35. a conbibendo (!) II 574, 41. a conuiuando V 650,
14 (= *Non.* 42, 8). *V.* conuiuatio.

**Conuiuo** συνζῶ II 446, 6; III 159, 32. συμβιῶ II 441, 54.
συνευωχοῦμαι, συνεστιῶμαι II 116, 12. **conuiuor** συνεστιῶμαι II 445,
37. **conuiuit** συνζῇ, συνεστιᾶται II 116, 11.

**Conuocatio** σύγκλητος III 503, 38.

**Conuocatus** contio, conuentus IV 325, 16.

**Conuoco** συγκαλῶ II 440, 6. **conuocat** et **conclamat** et
**concitat** συνκαλεῖ II 116, 13. **conuocat** multos in unum colligit
IV 45, 25. **conuocari** locari (uocari?) IV 325, 15.

**Conuolo** καθίπταμαι II 335, 24.

**Conuoluens** inuoluens IV 46, 7. *Cf. Verg. Georg.* III 426.

**Conuulsae undis** fluctibus laceratae IV 435, 1 (*Verg. Aen.* I
383). *V.* conuulsus.

**Conuulsione** separatione IV 500, 41.

**Conuulsus** simul euulsus uel percussus IV 321, 18. **conuulsa**
eradicata IV 222, 1. eruta, praesens (percussa?) IV 45, 27. dissoluta
uel percussa IV 434, 51. diuulsa, dissipata, disrupta, euersa IV 40, 3.
dissipata, disrupta, euersa, separata, confracta IV 500, 40.
**comuulsae** fluctibus laceratae IV 36, 41 (*v.* conu. undis).

**Cooperculum** (*vel* cop.) πῶμα II 426, 60; III 24, 11; 326, 38; 403,
60. πωμάτιον II 426, 61. opertorium uasis II 573, 40.

**Cooperimentum** *v.* operculum.

**Cooperio** ἐπισκεπάζω II 310, 53. ἐπικαλύπτω II 308, 28. καλύπτω,
σκεπάζω II 116, 19. περικαλύπτω II 403, 3. συνσκέπω II 447, 32.
**coperior** περιβάλλω ἱματίῳ II 402, 8 (cooperio *e*). **cooperit**
περικαλύπτει III 156, 71. amictat, tegit IV 325, 17. **cooperi[o]**
περικάλυψον III 156, 70.

**Cooperor** συνεργάζομαι II 445, 27.

**Coopertorium** (*vel* cop.) σκέπασμα τὸ περιβόλαιον II 433, 6.
ἀναβόλαιον III 269, 39. περιβόλαιον III 21, 42; 92, 76; 403, 53.
περίβλημα III 156, 72. πῶμα III 194, 2/3; 215, 29 = 650, 9. indumentum
*b ante* II 574, 45. teges, amictum IV 325, 18. *Cf.* teges,
**coopertoria** ἀναβόλαια III 197, 8.

**Coopto** συνεύχομαι II 445, 46. **cooptat** εἰσποιεῖ II 116, 17.
**cooptarunt** εἰσεποιήσαντο II 116, 18.

**Coorior** διεγείρομαι II 276, 21. **cooritur** simul nascitur IV 44,
17; 220, 7; 325, 19. **cohorta** ἐξηγέρθη II 103, 4. **coorta est**
commota est IV 497, 41; V 278, 19 (*Verg. Aen.* I 148).
**cohorta[ta] est** nata est V 278, 63. *V.* crocitur.

**Coortis** natis IV 44, 45. *Cf. Verg. Aen.* X 405.

**Copadium** τέμαχος II 453, 13. κοπτόν III 183, 48. particulam carnis V
185, 20. **copadia** κοπάδια III 14, 45; 87, 35; 288, 62 = 659, 20; 314,
48; 444, 11; 477, 56. τεμάχια III 219, 3 = 234, 1 = 653, 11. κοπτά III
183, 47; 254, 64. particulae carnis quod Graeci κοπάδια dicunt V 185,
19.

**Copertit** *praef. anthol.* V *p.* V (*cf. 'Sitzungsber. d. S. G. d.
W.'* 1896 *p.* 68).

**Copeum** (*vel* clopeum) ornamentum IV 498, 4 (*cf.* clupeum *sub*
calbae).

**Cophinus** (*vel* cof.) κόφινος III 357, 54. corbis V 653, 50 (=
*Iuvenal.* III 14). [s]qualum IV 319, 47. **cophinos** corbes (corbę
*cod.*) V 495, 43. *V.* qualus.

**Copi** copiosi (*scr.* copioso) V 638, 76 (*= Non.* 84, 19).
