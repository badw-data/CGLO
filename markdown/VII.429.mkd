IV 580, 3; V 336, 40. sorbet IV 296, 43. bibit ίν 195, 27. **uorent**
gluttiant IV 195, 32 (*Ter. Eun.* 339). *V.* urgueo.

**Uorri** edaces IV 195, 41; 580, 4. holeres (helluones?), edaces V 336,
38. elones, edaces V 400, 59. edaces, elones IV 403, 7. *Cf. Loewe
Prodr. p.* 430. uorrus *ex* βορος *ducit Roensch 'Litt. Centralbl.'*
1877 *p.* 697, *cum alii pro* uorus *dictum velint.*

**Uortigo** *v.* uertigo.

**Uortumnus** *v.* Uertumnus.

**Uos** ὑμεῖς II 462, 55; III 6, 66; 80, 11. ὑμᾶς II 211, 32; 462, 53.
**uobis** ὑμῖν II 211, 11; 462, 59. **uestrum** ὑμῶν II 462, 64. *V.*
apud me.

**Uosmet** uosmet ipsos IV 195, 42; 426, 37; 580, 5. uos ipsos IV 296,
51; 470, 35 (*Verg. Aen.* I 207).

**Uosmet ipsos** ὑμᾶς αὐτούς II 462, 54.

**Uoti compos** consentaneus uotis [uel compositus uoti] IV 195, 46.
uictor IV 580, 13. uoto ornatus V 398, 44. memor explendi uoti (expl̃i de
u͠o *codd.*) IV 297, 1. **uicti compos** uictoriam uoti\<s\> adeptus IV
192, 47; V 518, 43. **uoti compos** christiana V 422, 20 (*Euseb. eccl.
hist.* X 8 *ubi* regina uoti compos *legitur*).

**Uotificantibus** dona libantibus IV 470, 36 (*gl. Verg.*?); V 612, 56.

**Uoti reus** obligatus uoto IV 470, 37 (*Verg. Aen.* V 237).

**Uotis in Capitolio rite nuncupatis** uotis in Capitolio solito more
nuncupatis *Plac.* V 103, 9 (*cf.* 8) = IV 426, 42. *Cf.* **uotis
nuncupatis** quod occurrit iam dedicatum uotum IV 426, 41 =
**uotissimum** quod iam dedicatum est uoto (uotu *codd. multi*) in
Capitolio solito *Plac.* V 103, 8 (uotiuum *Maius*). *Cf.* uotiuus *et
Liv.* XXII 1, 6.

**Uotis omnibus** desideriis cunctis IV 470, 38 (*gl. Verg.*?).

**Uotitum** deuotum, religiosum V 648, 35 (*Non.* 45, 4).

**Uotiuus** εὐκταῖος II 317, 59. **uotiuum** immolatum IV 195, 26; V
336, 29. immolatiuum IV 580, 8; V 255, 4. quod promissum est uotis [in
Capitolio rite nuncupatis, quia in Capitolio promittebantur sacra] V
336, 43 (*v.* uotis in Cap. r. n.). quod promissum est uotis IV 195, 43;
403, 8; 580, 7; V 255, 5. *Cf.* **botitium** uotum fit V 403, 42
(uotiuum? uoticium?). **botium** uotum fit V 349, 5. **uotiua** promissa
*a post* IV 195, 43.

**Uotorum nuptialium** desiderabilium scilicet V 255, 6.

**Uotum** εὐχή II 211, 33; 320, 37; 506, 11; 548, 63; III 10, 18; 83,
72; 140, 16; 171, 4/3; 239, 14; 294, 55; 466, 66; 517, 3 (*GR. L.* I
554, 32). εὐχή otatte tetisteo II 533, 5 (uota τελεστικά *vel* τελετικά
*b in marg.* ὃ τάττεται τις θεῷ *H.*). uoluntate promissum *a c post* IV
195, 43; 580, 6. **uoto** προθέσει[ς], σκοπῷ II 211, 34. *V.* in uota,
ex uoto.

**Uoueo** ἀνατίθημι, consecro II 211, 35. καθοσιῶ II 335, 37. κατ' εὐχὴν
ἐπαγ­γέλλομαι II 345, 55. εὔχομαι II 320, 38. καθιερῶ II 335, 18.
consecro, dedico V 559, 19. **uouet** obtat, promittit, considerandum
sane est, quia qui uouet, suum [suum] proprium uouere debet, id est
promittere *Plac.* V 103, 10. promittit IV 195, 44; 296, 53; 402, 57;
580, 9; V 336, 35. uotum promittit IV 195, 29; 403, 2. **uouit**
promisit IV 195, 45; 580, 10. **uouere** εὔχεσθαι II 211, 36.

**Uox** φωνή II *p.* XXXVII; 211, 37; 474, 21; 508, 48; III 11, 43; 80,
27; 85, 9; 163, 3; 174, 39; 328, 51; 348, 61; 349, 13; 394, 18; 466, 67;
474, 38; 563, 33. **uocem** φωνήν III 163, 4. *V.* aduoce, uocum
nouitas.

**Uox reciproca** antiphona III 510, 17. *V.* antiphona.

**Uubalia** *v.* burbalia. *Cf. W. Heraeus 'Spr. des Petr'* 41.

**Uulbos** lapationes (lapathio *ut* porrio?) V 519, 55 (bulbos?
uoluos?).

**Uulc\<an\>al** ipsius dei locus II 597, 44.

**Uulcanalia** Ἡφαίστια II 326, 3 (uulcania singularia non habet: *cf.
GR. L.* I 34, 10); III 10, 24/23; 83, 78; 294, 61; *cf.* 371, 57.
festiuitas erat paganorum, quod Graeci efestia uocauerunt V 255, 12.

**Uulcanus** Ἥφαιστος II 212, 39; 326, 2; III 167, 37; 236, 39; 289, 56
(φεστος); 343, 54; 348, 15; 393, 35; 467, 4; 494, 10. ignis IV 470, 46
(*Verg. Aen.* V 662? *cf. Isid.* XIX 6, 2). Mulcifer IV 403, 38
(ulcanus *libri praeter bd. cf. Loewe Prodr.* 421). *V.* Ceres.

**Uuldar** (*vel* -dac, *non plana*) uetustas sola V 399, 35.

**Uulga** sinuosum V 648, 70 (*Non.* 187, 17). *De* uurga *cf. Loewe GL.
N.* 78; *praef. Anthol.* V *p.* V. *V.* bulga.

**Uulgagine** (*vel* bulg.) asaro III 542, 22; 631, 16; *cf.* III 543,
69. **uulgaginos** asarus III 580, 13. **radix bulgaginis** (*vel*
uulg.) asaro III 586, 28; 607, 20; 616, 29 (bulgagine). asaru id est
bacca siue **uulgagine** III 549, 2. *Cf. von Fischer-Benzon p.* 56.

**Uulgari** huni (*AS.*) V 423, 38 (*Gregor. dial.* IV 26, *ubi*
bulgar *in ed. cf. AHD GL.* II 244, 19).
