*tutatur Funck Arch.* VII 492). **praepropera** fraehraedae (*AS.* =
praepropere) V 378, 12 (*cf. Oros.* V 5, 7).

**Praepulchre** εὐμόρφως III 437, 54.

**Praeputium** ἀκροβυστία II 157, 9; 223, 55; 505, 1; III 13, 10; 86,
20; 248, 58. *V.* insigne.

**Praereptum** ante raptum V 323, 28.

**Praeripio** προαρπάζω II 416, 15. **praeripit** ante rapit IV 269, 39;
379, 23. **praeripere** ante rapere IV 379, 22. *V.* proripio.

**Praerogans** ante inpendens IV 153, 35; 269, 31; 379, 24; 551, 27; V
135, 43; 322, 62.

**Praerogatio** τὸ προνόμιον II 524, 11.

**Praerogatiua** πρόλημμα II 157, 14. πρόλημψις καλοῦ ἢ κακοῦ βίου ἢ
πράγματος II 418, 50. πρόκριμα II 418, 36; 497, 51. προτίμησις II 424,
8. προκοπή II 418, 34. προνόμιον II 419, 27. excellentia meritorum IV
154, 35; 269, 43; 552, 3; V 135, 42; 235, 13. excellentia V 424, 55
(*Cassian. Inst.* VIII 1). gratia praemissa IV 379, 25. beneficium ante
oblatum, quod in se (quod si?) reddideris iam non est praerogatiua IV
552, 2; 154, 26; V 235, 14. priuilegium V 417, 65 (*Cassian. Inst.* XII
4, 1). optima IV 556, 56 (pror.); V 322, 53. beneficium ante donatum V
235, 12. praeducium (praeiudicium?) facientia uerba V 323, 20.
**praerogatiuum** beneficium ante oblatum IV 268, 46. beneficium ante
datum IV 379, 26. suffragium IV 379, 27. **praerogatiuas** πλεονεξίας II
157, 15.

**Praerogatiuam animi tui habeo** V 663, 64 (= **liquet mihi de animo
tuo** V 662, 59; *cf. Ind. Ien.* 1888 *p.* VII).

**Praerogator** dispensator *Scal.* V 609, 2 (*Osb.* 484). *V.*
progator.

**Praerogatum** praefixum IV 379, 28. **praerogatam** donatam V 323, 29.
*V.* prorogatum.

**Praerogo** προκριματίζω II 418, 37.

**Praerupta loca** ἀπόκρημνοι τόποι II 157, 17.

**Praeruptus** απόκρημνος II 237, 59. praeceps IV 155, 4. uehemens IV
156, 29. diuisus V 322, 54. **praeruptum** προραγέν II 420, 12. altum IV
459, 14 (*Verg. Aen.* I 246 *ubi cf. Serv.*). excissae rupes V 135,
44. haengiclif (*AS.*) *gloss. Werth. Gallée* 342 (*v. suppl. ad*
praerupium?). **praerupto** eleuato, alto IV 269, 48 (*cf. Serv. in
Aen.* I 105). **praerupta** ἀπόκρημνα, ἀπερρωγότα II 157, 18.
**praerupta** (-ae?) stegelrae (*uel* staegilrae, *AS.*, *gen. uel dat.
sing.*) V 378, 29. **praeruptis** excissae rupes extremae IV 155, 36;
552, 18. *Cf.* perruptus.

**Praeruunt** *v.* pror.

**Praes** ἔγγυος, ἀνάδοχος II 157, 19 ἔγγυος II 508, 20 (prex precis
*cod. cf. GR. L.* I 546, 23). fideiussor IV 269, 32; *gloss. Werth.
Gallée* 364 (*v. suppl.*). ab eo quod est prae\<si\>des, id est
fideiussores *acd post* IV 154, 23. ab eo quod est praesides uel iudices
uel fideiussores V 323, 12. praedis, fideiussor, possessio (!) uel qui
habet praedia V 556, 20. **praes** et **uas** byrgea (*AS.*) V 380, 56.
**praedem** fideiussorem IV 156, 12; 552, 31. **praedes** fideiussores
IV 269, 37; *a post* IV 151, 13; V 135, 28; 379, 46. uades, id est
fideiussores IV 379, 9. fideiussores dicuntur et uades V 322, 51.

**Praesagaciores** ingeniosiores V 575, 46.

**Praesaga mens** V 663, 39 (*Verg. Aen.* X 843).

**Praesagio** *v.* praesago.

**Praesagium** signum quod antea dicitur (*uel* deus), postea uenit IV
268, 41. quod ante dicit\<ur\>, postea uenit IV 551, 23. diuinatio, quod
antea nuntiatum est V 475, 4. diuinatio, quod ante nuntiat\<ur\> IV 379,
29. diuinatio futurorum IV 269, 2. praescientia IV 551, 24. indicium
futurorum uel sign\[or\]um IV 156, 2; 551, 26. signum V 476, 61 (pros.).
uaticinium, diuinatio \[de\] futura adnuntians V 136, 2. **praesagia**
futura V 136, 4. *Cf.* **praepagmen** praediuinatio uel uaticinatio V
322, 44.

**Praesago** προμαντεύομαι II 158, 1; 419, 6. προμηνύω II 419, 12; III
238, 14. προθεσπίζω II 417, 57. προγιγνώσκω II 416, 43. **praesago**
praesagas praesagare facit et est primae coniugationis;
**praesag\<i\>o** praesagire id est futura dicere tertiae coniugationis
sunt productae *Plac.* V 94, 10 + 11 = V 136, 3; *cf.* V *praef.* XVIII.
**praesagat** diuinat aut futura adnuntiat IV 153, 34; 551, 21. diuinat
V 575, 50. **praesagare** praedicare V 475, 5. **praesagauit** futura
nuntiauit V 325, 12 (pros. *coeli*), futurum enarrauit, praesagio ante
cognito dixerat V 475, 6 + 7.

**Praesago** (= -us?) propheta qui antea praedicat\[ur\] V 475, 8.

**Praesagus** praescius futurorum IV 156, 3; 551, 25. praescius IV 268,
39. praescius uel ante inuestigans IV 379, 30. praescius futurorum uel
inante inuestigans V 475, 3. **praesaga** πρόμαντις II 157, 20; 419, 5.
praediuina II 590, 33. praediuina aut futura praesagiens (praesa ignem
*cod.*) uaticinatio IV 551, 22. futura \[uel\] praediuina V 322, 55.
**praesagum** prouidum IV 156, 41;
