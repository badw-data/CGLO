**Lutina** πήλωμα II 545, 54 (*GR. L.* I 33, 16).

**Luto** πλύνω II 410, 28.

**Lutor** πλύτης II 410, 31 (lutus *cod.*); III 455, 23; 485, 35. πλυτήρ
II 410, 32. ἡλιαστής III 367, 32.

**Lutosus** πηλώδης II 407, 28. **lutuosus** *v.* lutulentus.

**Lutrus** otr (*vel* octur, *AS.*) V 369, 6.

**Lutta** *v.* aluta.

**Lutulentassit** lutulentum fecerit *Plac.* V 30, 10 (-sset ---
fecisset) = V 82, 13 (*ubi* luc- luc- *Deuerling*: *cf. Loewe GL. N.* 89).

**Lutulentus** πηλώδης II 407, 28. lutosus (*vel* lutuosus) IV 362, 39.

**Lutum** πηλός II 125, 34; 407, 27; 500, 49; 526, 48; 544, 13; III 19,
40; 70, 9 = 637, 2; 91, 33; 193, 63/64; 270, 50; 312, 40; 365, 3; 376,
65; 502, 17; 573, 6 (lutus). cenum IV 362, 31. *V.* caenum.

**Lutus** πλύσις II 410, 30 (*ubi* lautus *a*, lotus *e*).

**Lutus** λουσάμενος II 362, 49. **lutum** πεπλυμένον II 401, 44. mundum
V 368, 33 (lutum *vel* lautum). *V.* lotus, lautus.

**Lux** φῶς II 125, 36; 474, 25; 499, 20; 507, 44; 523, 16; 540, 73;
545, 51; III 69, 49 = 637, 1; 347, 26; 376, 51; 393, 27; 455, 15; 470,
64. phoos III 409, 70. φῶς, ἄνθρωπος (= φώς) II 553, 34. φέγγος III 162,
30. αὐγή II 250, 38; III 244, 10. φωτισμός III 455, 24. splendor,
claritas IV 451, 4 (*gl. Verg.*). salus V 114, 34. *V.* luce uigilo,
ante lucem, limus.

**Luxati\<o\>** euersio II 586, 51. *V.* laxatio.

**Luxo** ἀσωτεύομαι (*v. sub* lustro) II 249, 32. **luxatur**
ἀνασάσσει (ἀνασπᾶται *cum c vel* ανασείεται Vulc.) II 125, 37.

**Lux ultima** nouissimus dies V 114, 35 (*Verg. Aen.* II 668).

**Luxum** uulsum (uuls. lux. *cod.*), loco motum V 648, 37; 651, 63
(*Non.* 55, 11). **luxis** luxatis, quassatis *a post* IV 111, 42.

**Luxum** στρέμμα II 125, 38; 438, 56. στρέμμα ἢ αὐγισμός (λυγισμός *g*)
ἢ ἀσωτία II 125, 40 (*v.* luxus *et* lux). luxatio pedum IV 362, 32.
*V.* laxatio.

**Luxuria** ἀσωτία II 125, 39; 249, 31; 538, 32; 550, 44; III 489, 56.
**luxuries** ἀσωτία III 455, 26. **luxuria** (*vel* -oria) λαγνεία II
357, 62; III 338, 77; 455, 25. ἐξώλεια II 304, 46. Ἀφροδίτη III 509, 37.
ἀγερωχία III 551, 7. comessationes, potatus (nepotatus *H.*) IV 362, 16.
unde **luxoria** dicta V 651, 62 (*Non.* 55, 12). **luxuriem**
ubertatem. Vergilius (*Georg.* I 112: *cf. Serv.*) luxuriem segetum
tenera depascit in herba V 114, 32; 220, 4.

**Luxurior** (-orior *codd.*) ἀσωτεύομαι II 249, 32. τρυφῶ II 460, 52.
**luxorio** ἐντρυφῶ II 300, 51.

**Luxuriosus** (*vel* -oriosus) ἄσωτος II 249, 30; III 251, 3; 373, 79;
470, 65; 510, 39. ἀσελγής II 247, 23. qui cum meretricibus conuiuia
facit IV 362, 33.

**Luxus** ἀσωτία II 249, 31; 488, 61 (limitus *cod. corr. a e* libitus
*H*); 512, 8; 538, 32; 550, 44. ἀσέλγεια II 247, 25. luxuria IV 111, 15
(luxuriosus: *an* laxus? *cf.* lexa); 535, 46; V 308, 50. luxoria uel
lasciuia (*Euseb. eccl. hist.* III 23) V 420, 19 = 429, 2. pompa regia
et luxuria IV 256, 25; V 114, 33. **luxum** luxuria IV 451, 5 (*Verg.*
*Aen.* I 637). **luxu** luxuria IV 111, 20. *V.* luxum.

**Lyaeus** Liber pater sic dicitur quod matrem suam morte liberauerit IV
451, 10 (*Verg. Aen.* IV 58). uinum, Bacchum graece V 308, 22.
**Lyaeum** uinum IV 255, 13; V 523, 29. *V.* laticem L.

**Lycaeus** mons fri\<gi\>dus. Vergilius (*Ecl.* X 15): et gelidi
fleuerunt saxa Lycaei V 113, 8.

**Lychinium** est quod de candela uel lucerna emungitur, ut melius
ardeat V 621, 11 (licinium *cod.*). **lucinium** stuppa lucernae V 464,
2.

**Lychnia** candelabrum V 506, 61.

**Lychnicum** graece ubi ponitur lucerna V 308, 9 (lychnuchum *Buech.*).

**Lychnis** λυχνίς III 324, 34.

**Lychnus** lucerna V 506, 62. **lichin\<us\>** lucerna V 528, 10.
**lychni** lucernae IV 362, 34. **lycini** candelae, lucérnae IV 451, 9
(*Verg. Aen.* I 726). **licini** candelae V 217, 32. lucernae IV 534,
39; V 217, 33; 603, 7. **lichinii** lucer­nae V 307, 39. **lucinii**
lucernae V 464, 4. **lichinae** candelae IV 255, 25; V 571, 54.
**lichini** candelae, lucernae uel cicindilia V 635, 41; 603, 23
(licini). **licini** candelae uel cecindilae lucernae IV 534, 40.
cicendelia lucernae V 218, 1. **lucinis** lucinii V 464, 1 (lychni
lucini?). **ligni** lucini IV 415, 1. *Cf. Ritschl Op.* II 479.

**Lycii** gens Asiae IV 451, 8 (*Verg. Aen.* I 113).

**Lycisca** canis ex lupo et cane natus V 370, 2. *Cf. Isid.* XII 2, 28.

**Lycium** *in his est glossis:* **licion** licio III 547, 29.
**licius** id est compositus et est bonus ad oculos lacrimosos III 584,
10. **licio** id est pimenta orientalis III 592, 6; 625, 53. id est
pigmenta orientalis III 613, 34. acontontironta (pyxacanthon Chironiam
*Plin.* XXIV 125) id est **liciu** III 552, 67. *V.* trifolium
domesticum.
