**Ὀρθώσιος** Stator. *Cf.* Ianus.

**\*ὁριαῖος (?)** *v.* ὁραῖος.

**ὀριγανίς** origanum.

**ὀρίγανον** artemisia, cardo silvester, cunila, nepeta montana,
nitilla, origanum.

**ὁρίζω** censeo 2., cerno, constituo, decerno, finio, instituo, statuo,
taxo, termino, - ἐπὶ πράγματι censeo 2., constituo, instituo, statuo, -
ἐπὶ τόπου ἤτοι χώρας finio, limito, termino, ὁρισθείς *cet.*
constitutus, praestituto, ὡρισμένος *cet.* certus, constitutus, finitus,
praefinitus, statutum, ἡμέρα ὁρισθεῖσα *et* ὡρισμένη *v.* ἡμέρα.

**ὀρίνεται** rucluatur.

**ὅριον** confinis, finis, terminus.

**ὅριος:** - θεός Terminus. *Cf* ἑορτή.

**ὅρισμα** institutum.

**ὁρισμός** definitio, sponsio.

**ὁριστικός** finitivus, indicativus.

**ὀρίχαλκον** *et* **αὐρίχαλκον** (= ὀρείχ.) aurichalcum.

**ὁρκίζω** adiuro, periuro.

**ὅρκιον** foedus. *Cf.* σύγχυσις.

**ὅρκος** deiuramentum, foedus, iuramentum, iuratio, iusiurandum,
religio, - *et* - στρατιωτικός sacramentum.

**ὁρκῶ** adiuro, iuro.

**ὁρκωμοσία** iuratio, iusiurandum.

**ὁρκωμόσιον** iuramentum, iusiurandum, sacramentum.

**ὁρμαθός** series. *Cf.* sertum (steria).

**ὄρμενον** cyma.

**ὁρμή** impetus, petitio.

**ὁρμηδόν** impes (impete).

**ὅρμημα** impetus.

**ὁρμιά** linea, saeta, - ἡ τοῦ ἀγκίστρου hamus, linum. *Cf.* gruma.

**\*Ὅρμιος** Portunus.

**ὅρμος** accessus, corymbus, statio, - *et* - γυναικεῖος monile, - ὁ
πρὸς καταγωγὴν νεὼς ἐπιτήδειος τόπος navale (-ia).

**ὁρμῶ** egredior, pergo, peto, proficiscor, - ἐπὶ τοῦ ἐπέρχομαι impetum
facio, peto, ruo, ὁρμώμενος ὁ ὢν ἀπό τινος πόλεως ortus.

**ὁρμῶ** ἐπὶ λιμένος deveho, stationem habeo, statuo.

**ὄρνεον** ales, avis, - βασιλικόν immusulus, - θαλάσσιον fulica, -
μαντευτικόν inebra, - οἰωνοσκοπικόν emusculus, γένος *vel* εἶδος ὀρνέου
ambustandus, buteo, immusulus, passa, strix (striga). *Cf* εὔλαλος,
πτερόν, ῥάμφος.

**ὀρνεοπώλης** aviarius.

**ὀρνεοσκοπία** augurium, auspicium.

**ὀρνεοσκόπος** augur, auguralis, auspicalis, hariolus, haruspex. *Cf.*
μάντις.

**ὀρνεοσκοπῶ** auguror, auspicor.

**ὀρνεοτρόφος** altiliarius.

**ὀρνίθεια** gallinacia, gallinacia caro.

**ὀρνίθι(ο)ν** gallina, pullus.

**\*ὀρνιθοκρίτης** coniector.

**ὀρνιθομαντεία** *cf* ornithomantia.

**ὀρνιθοτροφεῖον** aviarium.

**ὄρνις** ales, avis, gallina, gallinacia, pullus, ὄρνιθες anxilites;
ὄρνις ἀγρία gallina rustica, rusticola (= -cula?), - βασιλικός
immusulus, - ἡ κατ' οἶκον gallina, - πονηρά eniber. *Cf.* ἄκρος,
κοιλήπατα, κροκκῶ, νεφέλη.

**\*ὀρνόλη:** εἰς -ην in urceolum.

**ὀρνύφιον** *cf.* βασιλίσκος, γλαῦξ.

**ὄροβος** ervum *sim.*, orobium, vicia domestica, vicia silvatica.

**ὀρόδαμνος** virgultum.

**ὁροθεσία** pagus, terminatio.

**ὁροθέσιον** ab limite, finis, limes (*cf. etiam* limen), terminatio,
terminus, trames, -α terminalia.

**ὁροθέτης** terminator.

**ὀρὸς** γάλακτος (τῶν βοῶν) serum.

**ὄρος** collis, mons; ὄρη Γαλλιῶν Alpes, - ἑπτά septimontium, - ὑψηλά
Alpes. *Cf.* ἄκρος, ἐξοχή, κορυφή.

**ὅρος** confinis, constitutio, constitutus (-um), definitio, finis,
limes, modus, statutum, terminatio, terminus, - ἐπὶ δίκης ἢ πράγματος
definitio, lex, sponsio, terminatio, - ἐπὶ χώρας finis, limitum,
terminatio, terminus, διχόθεν - amterminus, τούτῳ τῷ ὄρῳ ea lege. *Cf.*
διάκρισις, λίθος, τίθημι.

**ὀροφή** camera, culmen, fastigium, lacunar, laquearium, pergula.

**ὄροφος** culmen.

**ὀρόφωμα** culmen, lacunar.

**ὀρόφωσις** lacunarium, laquearium.

**ὄρπηγες** tam arae.

**ὅρριον** *et* **ὥρριον** horreum.

**ὀρρωδῶ** ὅ ἐστι φοβοῦμαι horresco.

**\*ὀρτυγία** sellia.

**ὀρτυγομήτρα** *cf.* ortygometra (*expl.* coturnix).

**ὄρτυξ** coturnix.

**\*ὀρυγεύς** fossorium.

**ὄρυγμα** carrecta, fossa, fossura.

**ὄρυζα** oryza. *Cf.* oniza.

**ὀρύζι(ο)ν** *cf.* oniza.

**ὀρύκτης** fossor.

**ὀρυμαγδός** conglobatio.

**ὄρυξ** acisculus, dolabra, fossorium, ligo, sarculum, upupa (obpupa),
ὄρυγες θήλεια ἐργαλεῖα subulo (sublones). *Cf.* arcumterebra (*p.* 434).

**ὀρύσσω** *et* -ττω fodio, itero, ὠρυγμένος defossus.

**ὀρυῶμαι** *v.* ὠρυῶμαι.

**ὀρφανία** orbitas.
