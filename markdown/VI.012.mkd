XIX 3, 2). *Cf.* **achatto** bonum uelum III 506, 12 (= acation).

**Acatus** nauicula uel arca IV 5, 43. nauicula uel arca[rius] IV 471,
38. *Huc refero* κάραβος **acutus** (= acatus) III 205, 27.

**Accano** (adc. *cod.*) προσᾴδω II 420, 23.

**Accede ad ignem hunc** hoc est ad amorem tuum (diuinum *codd.*) V 436,
13 (id est); V 531, 6 (= *Ter. Eun.* 85).

**Accedo** προσέρχομαι II 421, 10; III 154, 22. προσπελάζω II 422, 41.
προσχωρῶ II 423, 48. πλησιάζω III 438, 5. **accedis** προσέρχεσαι(!) III
154, 23. **accedit** προσέρχεται III 154, 24. ἀπέρχεται II 12, 53 (*cf.
Weymann Arch.* VIII 294. ἐπέρχεται *c*); II 555, 28 (abscedit *e*).
ingreditur IV 302, 41. **accedunt (?)** mala V 260, 23. **accede**
πρόσελθε II 420, 63; III 154, 25. **accedere** προσιέναι II 12, 38.
**accessit** προσῆλθεν II 13, 5. **accesserim** προσέλθοιμι II 13, 4.
**accesseritis** πεισθῆτε II 13, 13.

**Acceia** ἀσκαλάφη III 360, 67. ἀσκαλώπη (ασκαλωπν *cod.* ἀσκαλώπας
*Boucherie*) III 319, 24. **accia** et **acceia** (accela *cod.*)
ἀσκαλάφη II 13, 16. **accega** (acega) holthana (*AS.*) V 340, 9. *V.*
ascalaufe, ascella.

**Accelerans** hoc est celeriter exequens IV 444, 14 (*cf. Verg. Aen.*
IX 505).

**Accelero** ταχύνω III 160, 15. ἐπιταχύνω II 311, 40 (adc.).
**accelerat** adpropinquat, celeriter uenit IV 7, 11. celeriter uenit
(*vel* adu.), properat IV 302, 42. adpropiat (*vel* adpropriat) uel
festinat IV 8, 1.

**Accendo** ἅπτω II 243, 27. ἐκκαίω II 290, 24. ἐξάπτω II 302, 2.
**accendit** ἐξάπτει, ἅπτει II 12, 44. **accende** ἅψον II 255, 5.
**accendere** ἐξάπτειν, ἅπτειν II 12, 39. **accendatur** ἐκκαυθήσεται
(accendetur *e*) II 12, 55.

**Accensi** qui magistratibus praesto sunt aut militibus tributi V 436,
16 (tributa *codd.*). deputati, ab acciendo dicti V 649, 24 (=
*Non.* 59, 1).

**Accensio** *v.* lucern. accensio, accessio.

**Accensor** εἰσαγωγεύς, εἰσηγητής, βούλαρχος II 12, 43.

**Accensus** ἐξαφθείς. II 12, 40. **accensi** irati uel concitati IV 6,
14. incitati, inritati IV 302, 43. irati, incitati, concitati IV 473, 6.
irati uel conciti V 260, 25.

**Accensus** ἔκκαυσις II 290, 29. *V.* accentus.

**Accento** τονίζω II 457, 10.

**Accentus** προσῳδία II 12, 42; 423, 51; 508, 56; 487, 10. προσῳδία,
ἄρσις II 537, 2. τόνος II 457, 11. est sonus productus *Plac.* V 44, 1.
acutus sonus in uerbis IV 477, 7. uox acuta siue producta IV 302, 44.
uox alta siue producta V 260, 40; 343, 47. ratio metrica correptiua (?),
id est uox V 436, 7. ratio metrica IV 404, 26. sonus uocis correptae uel
productae IV 202, 29. sonus, inflammatio uel uociferatio (*contam. cf.*
accensus) IV 5, 40. **accentu** impetu IV 7, 3. *V.* aduentus.

**Acceptabitis** acceptus εὐπρόσδεκτος II 319, 11.

**Acceptabulum** *v.* acetabulum.

**Acceptatio** *v.* acceptilatio.

**Acceptator** auctor IV 473, 8; V 260, 29; 343, 55. auctor, conscriptor
IV 202, 33. actor?

**Acceptilatio** ἀπολοιπογραφία II 238, 43. **acceptilatio** et
**acceptatio** ἄποχον γραμμάτιον II 13, 2 (*cf.* ἄποχον γραμμάτιov
**acceptatio** II 555, 30). καταλογισμός **acceptilatio**, inputatio
(inpulatio *cod.*) II 341, 60.

**Acceptio** λῆμψις II 360, 22. 

**Acceptor** λημπτής II 360, 21. ἱέραξ III 17, 40 (περδιξ *codd.*); 89,
66; 319, 20; 360, 33. ἱέραξ, δοχεύς II 12, 57. auis et homo rapiens
(sapiens *cod. Amplon.*) II 564, 16. **acceptorem** accipitrem *Plac.* V
44, 2. *Cf. GR. L.* I 98, 9; VII 107, 8.

**Acceptorem salutis** qui salutatus est *Plac.* V 7, 8 = V 44, 3 (*quam
glossam non recte explicat Roensch 'Beitr.'* III 75).

**Accepto sibi fecit** ἀπέλαβεν, ἑαυτῷ ἐλογίσατο II 12, 41. ἀπέλαβεν,
ἑαυτῷ ε (*h. e.* ἐ\<λογίσατο\>) 555, 27. *Cf. Roensch 'Beitr.'* III 2.

**Accepto tulit** παρεδέξατο, ἀσμένως ἤνεγκεν II 13, 6 (*cf.* παρεδέξατο
accepto tuli\<t\>, recepit II 561, 43). ἐλογίσατο II 295, 38.
παρεδέξατο, ὅ ἐστιν ἐλογίσατο II 397, 30. recepit IV 302, 46. *Cf.
Roensch Coll. phil.* 170. **accepto ferri** λογίζεσθαι II 361, 59.

**Acceptum facio** παραδέχομαι ἐπὶ δαπάνης II 394, 31. **acceptum
fecit** προσεδέξατο II 12, 50.

**Acceptum habeo** εὐπρόσδεκτον ἡγοῦμαι II 319, 12. ἀποδέχομαι probo,
**acceptum habeo**, comprobo II 236, 25.

**Acceptus** ἀποδεκτέος II 236, 15. εὐπρόσδεκτος II 319, 11. προσφιλής,
προσηνής, λημφθείς II 12, 49. προσηνής III 372, 47. **acceptum**
κεχαρισμένον II 12, 45. ἀπόδεκτον, λῆμμα II 12, 54; 555, 29. λῆμμα II
360, 20. gratum IV 7, 16. gratum, iucundum IV 404, 14. accersum
(acgersum *libri plures*) IV 302, 47.

**Acceptus** εἴσοδος ἡ λῆψις II 287, 19.

**Accersibiles** euocati V 436, 8.
