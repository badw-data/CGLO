294: *v. infra*) IV 148, 3. **profligit (?)** collegit V 379, 11.
**profligant (?)** ἀπαιτήσωσιν II 160, 47. **profligare** celerius
consummare aut perficere uel coadunare IV 148, 23. celerius
co\<nsum\>mare aut coadunare IV 555, 53. exigere, prouidere V 325, 31.
prouidere V 137, 40. **profligauit** erogauit IV 148, 24; V 324, 35;
476, 20. extricauit IV 380, 60.

**Proflixit** prostrauit IV 275, 10; 380, 61.

**Proflo** ἐκφυσῶ II 293, 50 (proflauo *cod. corr. a e*).
**proflabat** dormiens V 325, 23 (*cf. Verg. Aen.* IX 326).

**Profluens** ἐκκεχυμένος III 334, 38; 519, 58.

**Profluens oratione** V 663, 40.

**Profluit** προρέει II 420, 13. multum fluit IV 275, 11; 381, 1.

**Profluuies** naturalium fines (fluores *Buech.*) V 476, 21; 511, 13.
*Cf.* **profluuies** sordis effusio *Mai* VII 575. *V.* proluuies.

**Pro fratre potior** *v.* Et pro fr. p.

**Profugatus** *v.* angiportus.

**Profugatus** fugitiuus V 324, 21.

**Profugus** φυγὰς τῆς ἑαυτοῦ πατρίδος II 160, 52. φυγάς III 461, 20;
472, 12. longe fugatus IV 420, 38; V 137, 37. longe fugatus, exul IV
275, 8. porro fugatus IV 459, 45 (*Verg. Aen.* I 2; X 720: *cf. Isid.*
X 215). porro fugatus uel fugitiuus uel de patria pulsus IV 555, 55.
porro fugatus uel fugitiuus IV 148, 25. profugens (*uel* porro fugiens)
IV 381, 2. de patria pulsus V 323, 35. **profuga** fugitiua V 137, 42.
*Cf. Loewe GL. N.* 205.

**Profundissima pax** dicitur iugis, continua, diuturna (diurna *P*),
plena per omnes (peremnis *Deuerling*) *Plac.* V 94, 27 = V 138, 36 (per
omnes *scil.* prouincias; *Mamert. paneg.* II 14 *confert Buech.*).

**Profundo** ἐκχέω II 293, 55. προχέω II 424, 41. **profundat**
prodigat, multum eroget V 538, 36 (*Ter. Ad.* 134).

**Profundus** ἀπέραντος II 234, 33. **profundum** ἄβυσσος II 215, 24;
504, 65. βύθιον, βαθύ, ἄβυσσον II 160, 51. βυθός II 260, 42; III 29, 35;
205, 1; 434, 58. βάθος II 255, 20. βαθύ II 255, 23. ἀπέραντον II 234,
34. altum IV 459, 46 (*Verg. Aen.* I 58). altum uel demersum IV 148,
29; 555, 59. uel sursum uel deorsum dicitur, ut (*Verg. Georg.* IV
222): 'caelumque profundum' V 137, 34 (*cf. Serv. in Aen.* I 58; II 758;
*Isid.* XIV 9, 4). porro habens fundum IV 381, 3. **profundo** alto IV
274, 42. *Cf. Festus p.* 229, 22; *GR. L.* I 414, 9; *Arch.* VII 529.

**Profusius** *v.* profusus.

**Profusor** περιχύτης II 406, 10 (perf. *e*). προσχύτης II 423, 46. *V.*
perfusor.

**Profusus** ἄσωτος II 249, 30 (profusor?). sumptuosus IV 557, 30; V
324, 3; 384, 3. prodigus IV 381, 4. largus IV 275, 5; 557, 29. humanus
uel in uoluntatibus (!) luxuriosus V 325, 4. **profusa** ἡ δαψιλής II
160, 50. **profusis** (*uel* -fussis) genyctfullum (*AS.*) V 377, 53
(*cf. Aldhelm. de laud. uirginit.* XLVII). **profusius** διακεχυμένως II
271, 42 (perfusius *e*). abundantius V 419, 53 = V 428, 39 (*Euseb. eccl.
hist.* II 3).

**Profuturum** ὠφέλιμον II 483, 1. **profutura** συνοίσοντα, ὠφελήσοντα,
προεσόμενα II 160, 53.

**Progastor** (proc. *codd.*) uentrosus V 511, 9; 525, 28; 576, 41.

**Progener** ἐγγόνης ἀνήρ II 283, 22; III 253, 47.

**Progenerantur** τίκτονται II 455, 31.

**Progeniculo** γουνοῦμαι II 161, 1. **progeniculatur** γονυπετεῖ II
161, 3. **progenicultor** procedens publicat V 325, 19 (procidens
supplicat *H.*).

**Progenies** ἀπογονή II 161, 2; 236, 3; III 254, 11. γονή II 264, 34.
γενεά II 262, 11. posteritas II 590, 31. genus IV 459, 47 (*Verg.
Aen.* I 250?). proles IV 381, 5. suboles IV 150, 49. **progeniem**
posteritatem V 323, 36. subolem *Plac.* V 95, 1. *V.* priuigna, propago.

**Progenies tortuosae** filii praui et amari V 476, 23; 511, 14.

**Progenitore\<s\>** proaui\[a\] V 137, 43. **progenitores** aui uel
proaui IV 148, 32; 150, 24; V 476, 24; 511, 15. aui uel proagnati IV
556, 2. primatum uindicans (? uindicantes *cod. Pal. ex corr.*
progenitor est?) V 236, 11. ataui quasi porro generantes V 236, 12.

**Progenitus** γεννηθείς II 262, 35. τεχθείς II 454, 45.

**Progigno** προγεννῶ II 416, 41. προτίκτω II 424, 5.

**Prognare** celeriter uel fortiter V 476, 22. *Cf. Festus Pauli p.* 95,
11 (prognaue, praegnaue?).

**Prognatus** ante natus *gloss. Werth. Gallée* 363 (*v. suppl.*).
**prognate** ante genite V 137, 44 (prognonte genitae *codd.*).

**Pro\<g\>nosin** intellegentiae species III 604, 20.

**Pro\<g\>nostica** praeuisio aegritudinis III 604, 27 (*cf. Isid.* IV
10, 2).

**Programma** proconscriptio *gl. Werth. Gallée* 342 (*v. suppl.*).

**Progredior** προβαίνω II 416, 21. **progreditur** προεξέρχεται II 417,
33. procedit IV 148, 33; 556, 4 (*Verg. Aen.* IV 136)

**Progressus** indeptus (?) IV 150, 17
