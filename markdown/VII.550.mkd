**κάμπτω** curvo, flecto, ἔκαμψεν ircuit (= circuit), καμπτόμενον
curvatum, καμφθείς tortus.

**καμπύλος** curvus, uncus. *V.* βακτηρία.

**\*καμπυλοσαλπιστής** cornicen.

**\*κάμψη** ebulus.

**κάμψις** flexus, - ὁδοῦ amfractus, flexura.

**καναβ-** *v.* κανναβ-.

**\*κανάκη** *cf.* canacem (*expl.* gladium).

**κανάλης** cloaca (*ubi* κανάλης *restituendum ex codd.*).

**καναλίσκος** cloax.

**κανδῆλα** cicindela, lampas.

**κάνδυς** *cf.* candys (*expl.* vestis regia).

**κανθαρίς** cantharis.

**κάνθαρος** ascopa, crabro, crater, scarabaeus, zimzario.

**κανθήλιον** clitella, semuncia.

**κανθός** angulus, - τὸ ἐπίσωτρον canthus, - ὀφθαλμοῦ angulus,
canthus, - τροχοῦ (urus = vitus), κανθοί hircus (-i), palpebra (-ae).

**κανίσκιον** canistellum, canistrum.

**\*κανίσκος** (*Eins.*) fiscella.

**\*κανναβάριος** stupparius.

**κάνναβις** cannabis, filum, stuppa.

**κάνναβος** cannabis (-us).

**κανονίζω** regulor.

**κανονικῶς** regulariter.

**κανοῦν** canistellum, canistrum, fiscella, III, 263, 28 *s.*
*interpr.*

**κανών** casus, norma, pertica, regula, *cf.* canon (*expl.* regula,
consuetudo, rectum), - λιθοξόου amussis; κανόνες οἱ ἐν θυρίσι ὀβελίσκοι
clatri, - γερδιακοί scapus (-i); κατὰ κανόνα ὀρθωθέν, ἰθυνθέν,
γραμμισθέν directus (-um). *Cf.* patella.

**Καπετώλειος** Capetoleos. *Cf* menses.

**Καπετώλιον** *et* **Καπιτώλιον** Capitolium.

**καπηλεῖον** caupona, cauponicula, cauponium, popa, popina, taberna, -
πολυτελές popina, ὁ ἐν τοῖς -οις ganeo 1.

**καπηλεύω** ganeo 2.

**\*καπηλογείτων** attubernalis.

**καπηλοδύτης** ganeo 1., sabinarius, tabernarius.

**κάπηλος** caupo,cauponarius,tabernarius.

**Καπιτωλ-** *v. etiam* Καπετωλ-.

**Καπιτωλῖνος** Capitolinus. *Cf.* Ζεύς.

**κάπνη** fumarium.

**\*καπνία** fumarium.

**καπνίζω** fumigo, fumo, δοκάριον κεκαπνισμένον insuasum.

**καπνίσματα** suffimenta.

**καπνιστά** fumata, fumigata.

**\*καπνοδοχεῖον** fumarium.

**καπνοδόχη** fumarium.

**\*καπνοδόχος** fumarium.

**καπνός** fumus.

**καπνοῦχος** tubulus.

**καπνώδης** fumosus.

**\*καπνωτήρια** altar (-ria).

**\*καππάρα (?)** portulaca.

**καππάριον** capparis.

**κάππαρις** capparis, lappa Herculina. *Cf.* peucedanum, φλοιός.

**κάπρειον** verrina.

**καπρίολος** τὸ ἔρεισμα τῆς στέγης furcilla.

**\*καπρος** ructus (-um).

**κάπρος** aper, verris.

**καπρῶ** prurio, -ᾷ surit.

**καπτρο-** *v. etiam* καμπτρο-.

**\*κάπτρον** (= κάμπτρον) capsa.

**καπύριον** crustula, crustulum, crustum.

**\*κάπων** gallus castratus.

**κάραβος** acatus, locusta, locusta marina. *Cf* carabus (*expl.*
navicula *sim.*).

**καραδοκῶ** aucupor, exspecto.

**κάραξ** *cf* χάραξ.

**καρακάλλιον** cuculla.

**καρδαμίνη** nasturcium, sisymbrium.

**καρδαμίς** cardamum.

**κάρδαμον** *et* **κάρδαμος** cardamum, nasturcium.

**καρδάμωμον** *et* **καρδάμωμος** cicer (siser *Stadler*) erraticum,
eruca, nasturcium. *Cf.* ἄνθος.

**\*καρδάνη** nasturcium.

**καρδία** cor, - δένδρου arboracea, arborata, - σχίνου lentisci coma, -
ἱλαρά festa corda. *Cf.* λοβός.

**καρδιακός** cardiacus, -ά pulsus cordis. *V.* κάλυξ, πόνος.

**\*καρδιότης** praecordia.

**κάρηνον** *v.* κάροινον.

**καρίς** cammarus, gabbarus, squilla, -ίδες locustulae, -ίδες θαλάσσιαι
squilla (-ae).

**καρκίνηθρον** polygonus.

**καρκίνος** cancer, forceps, forfex, - χαλκέως forfex. *Cf*
suffragines.

**καρκίνωμα** cancer.

**Καρνεῖος** Carneos. *Cf* menses.

**(κάροινον)** κάρηνον caroenum.

**κάρος** sopor.

**\*καρουχάριος** carrucharius (carucharius *cod.*, *v. p.* 436),
cisiarius, mulio.

**\*καρούχιον** raeda.

**\*καροφόρος** soporiferus.

**\*καρπακος** *cf* carpacus (*expl.* pistor).

**καρπεία** *v.* καρπία.

**καρπήσιον** ebuli semen.

**καρπία** (= -εία) ususfructus. *Cf.* χρῆσις.

**καρπίζομαι** ἐπὶ ἐλευθερίᾳ asserit (-or).

**καρπισμός** assertio, vindicta.

**καρπιστής** assertor, vindex.

**\*καρπιστία** assertio, -αι vindiciae.

**\*καρπιστικόν** liberale iudicium. *Cf.* liberalis.

**καρποβάλσαμον** carpobalsamum.

**καρπόδεσμος** articulare, articularius, fasciola, geminiscus (=
lemniscus), -οις περιειλημμένος lemniscatus.
