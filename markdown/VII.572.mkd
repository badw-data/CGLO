**\*λιγνον** (= λέγνον *Buech.*) bandum.

**λιγνύς** fuligo lucernae (*cf. p.* 437).

**λιθάζω** lapido.

**λιθάργυρος** *et* **λιθάργυρον** lithargyrum, spuma argentea, spuma
argenti. *Cf.* summa argenti.

**λιθαρίδιον** gemma, lapillus.

**λιθάριον** gemma, lapillus.

**\*λιθάσβεστος** lapis calcinus.

**λιθαστής** lapidarius, lapidator.

**λιθέα** (= -εία) lapidamen.

**λιθίασις** calculositas.

**λίθινος** lapideus, -ον saxeum. *Cf.* κορμός, πῶρος.

**λιθιῶν** calculosus, - ῶντας calculus (calculorum causa), dysuruntas.

**λιθοβόλος** ballista, fundibalus, lapidator.

**λιθοβολῶ** lapido.

**λιθογλύπτης** lapidum sculptor.

**λιθογλύφος** lapidum sculptor, marmorarius, sculptor.

**\*λιθοξοεῖον** lapidicina.

**λιθοξόος** lapidarius, lapidicinarius, marmorarius. *V.* κανών.

**λιθοπρίστης** lapidarius, serrarius lapidarius.

**λίθος** gemma, glarea, lapis, rupes, - αἱματώδης petra sanguinaria, -
μυλίτης lapis molarius, silex, - ὁραῖος terminalis lapis, - πολύτιμος
lapis pretiosus, - πυρώδης silex, - τίμιος pretiosa gemma, pretiosus
lapis, - Φρύγιος lapis de Phrygia, λίθοι τραχεῖς διακοπτόμενοι iantilia,
λίθοι τῶν ὅρων *sim.* grumus (-i), διὰ λίθων *cf.* bacatum. *Cf.* tofus,
γλύφω, συμβολή, σωρός.

**λιθόσπερμον** milium cervinum. *Cf.* saxifraga (semen -ae).

**λιθόστρωτον** lapide stratum.

**\*λιθοτομεῖον** lapidicina.

**λιθοτομία:** εἰς -ας in lautumias.

**λιθουργός** lapidarius. *Cf.* μοχλός.

**λιθοφόρος** phalangarius.

**λιθώδης** calculosus, lapidosa, scrupeus, - τόπος glarea.

**λικμητήρ** ventilabrum.

**λικμητήριον** ventilabrum, ventilamentum.

**λικμητής** ventilator.

**\*λικμητρίς** ventilabrum.

**λικμῶ** ventilo.

**\*λικνάριον** vannulus.

**\*λικνίζω** vanno (*et* -io).

**λίκνον** cunabulum, vannus, III, 263, 6 *s. interpr.*

**λίκτης** *vel* λείκτης lambo (-bitor), linctor.

**λιμβός** ganeo, glutto, ligurrio 1., ligurrus.

**\*λιμενάρχης** portitor.

**λιμήν** portus. *Cf.* προσορμῶ, τέλος.

**λίμιτον** limes. *Cf.* limitum.

**λιμνάζω** stagno.

**λιμναῖος** stagnatilis. *Cf.* βδέλλα.

**λίμνη** columbium (*cf. p.* 436), lacus, laecorus, palus, stagnum.
*Cf.* limen 2.

**λιμνήσιον** centauria.

**λιμνώδης** paluster, -ῶδες lacunarius (-um). *Cf.* centauria.

**\*λιμ(ν)οκαρίδες (?)** cammariunculi.

**\*λιμόξηρος** famelicus.

**\*λιμοξήρως** famelice.

**λιμός** fames. *Cf.* βούλιμος.

**λινάριον** *cf.* linaria.

**λινόζωστις** *et* -ον *sim.* linozotissin, mercurialis (*et* herba
mercurialis), - βοτάνη II, 361, 16 *s. interpr.*

**\*λινόμαλον** *et* **λινόμαλλον** mantela (mantile).

**λίνον** lineum, linum, - κυνηγετικόν plaga 1. *et* 2., plagula, rete
*sim.*

**λινοπλόκος** linarius, retiarius.

**λινοποιός** linarius.

**λινόσπερμον** lini semen.

**\*λινόστροφον** (βοτάνη) marrubium.

**λινουργός** linio, lintearius.

**λινοῦς:** -οῦν linteum.

**λινουφιον** lintio.

**\*λινοϋφίων (?)** *cf.* lintio.

**\*λινοϋφός** linitextor, linteator, persicarius, λίνυφος lintes.

**\*λινοῦχος** retiarius.

**\*λίνυφος** *v.* λινοϋφός.

**λιπαίνω:** λελιπαμμένον ἢ τὸ ἀπὸ σιδήρου πῖπτον ὡς σπινθῆρες strictura
(-ae).

**λιπαρός** crassus, grossus 1., obesus, pastus, pinguis. *Cf* εὐεκτός,
κόπαιον, στυπτηρία.

**λιπέρνης** pupillus.

**λιπογάλακτος** lacticularius, lacticulosus.

**λιπόδερμος** *et* **λειπόδερμος** curtus, recutitus, verpus.

**λιποθυμία** *et* **λειποθυμία** defectio, lassitudo.

**λιποθυμῶ** deficio. *Cf.* ἀνακτῶμαι.

**λίπος** abdomen, adeps (*cf. p.* 434), ingluvies, pinguamen, pinguedo,
sebum, unctus (-um), - (κρέως) ἄνευ σαρκός arvina, - ἐν ᾧ τοὺς ἄξονας
χρίουσιν axungia. *Cf.* sebes.

**λιποτάκτης** (λειπ.) desertor.

**\*λιπότακτος** (λειπ.) desertor.

**λιποτακτῶ** (λειπ.) desero.

**λιποψυχία** *cf.* lipositio (*expl.* angustia).

**λιποψυχῶ** deficio.

**λιπῶ:** λελιπωμένον unctatum.

**\*λίσφακος** *v.* ἐλελίσφακος.

**\*λίσχος** (= λίσγος) ligo.

**λιτανεία** (*vel* -νία) litatio, precatio, supplicatio.

**λιτανεύω** depreco, lito, precor, supplico.

**λιτή** litatio, precatio, -αί prex (preces; *cf. p.* 438).
