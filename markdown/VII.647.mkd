-ύει convinxit *et* convincit, συνεζευγμένος *cet.* annexus, iugites.

**συζευκτικός** coniunctivus, copulativus, subiunctivus.

**συζήτησις** conquaestio.

**συζητητής** conquisitor.

**συζητῶ** conquisito, disputo, litigo.

**συζυγία** coniunctio, *cf.* synzygia (*expl.* ex variis pedibus veluti
in unum pedem collecta conclusio), - ῥημάτων coniunctivus (-um), - ἐπὶ
ῥήματος τεχνικοῦ coniugatio, - ἐπὶ γάμου coniugium (c. *et* conubium).

**συζύγιος** compar.

**σύζυγος** compar, coniux.

**συζῶ** convivo.

**συκαλίς** *et* **συκαλλίς** ficedula *sim.*

**\*συκαλλός** ficedula.

**συκαμινέα** morum.

**\*συκαμινεών** moretum 2.

**συκάμινα** morum (-a).

**συκάμινος** morum, morus. *Cf.* σμῆγμα.

**\*συκάρι(ο)ν** ficus.

**συκάς** ficus.

**σύκειος** *v.* ξύλον.

**συκεών** ficetum.

**συκῆ** carica, ficus, - ἀγρία caprificus.

**σύκινος** *v.* ξύλον.

**\*συκινόφυλλον** folium ficulneum.

**σῦκον** *et* -ος carica (*v. p.* 436), ficus, -οι oliva (-ae), -α ξηρά
carica (-ae), ficus siccas.

**συκόμορος** *cf.* sycomoros (*expl.* deserti fici fructus, ficus
fatuas).

**συκοφάντης** calumniator, nequior (-issimus), sycophanta (*expl.
etiam* circumventor, impostor), vacillator.

**συκοφαντία** calumnia.

**συκοφαντῶ** calumnior.

**\*συκοφορεῖον** ficarium, fiscella.

**συκοφόρον** fiscella.

**\*συκών** *et* - ὁ τόπος ficetum.

**συκωτόν** ficatum.

**σύλησις** compilatio.

**συλητής** compilator.

**συλλαβή** syllaba, -αί apex (apices, *cf.* ἄκρος), -ῶν affatus
(-ibus).

**συλλαλιά** colloquium.

**συλλαλῶ** colloquor.

**συλλαμβάνω** arripio, comprehendo, concipio, prendo, - ἐπὶ γυναικός
concipio, - ἐπὶ τοῦ βοηθῶ suffrago, - ἐπὶ τοῦ συνέχω comprehendo;
συλλαμβάνομαι comprehendo, opitulor, - ἐπὶ τοῦ συνέχω comprehendo,
opprimo, - ἐπὶ τοῦ βοηθῶ opitulor, suffrago, συλληφθείς conceptus,
deprehensus, - ὁ ζῶντος μὲν τοῦ πατρὸς συλλαμβανόμενος, ἀποθανόντος δὲ
γεννώμενος postumus.

**σύλλεγμα** caterva.

**συλλέγω** collecto, colligo 1., confero, consentio (*et* assentio),
ὠὰ - ova colligo.

**σύλλεκτος** collecticius.

**συλλήβδην** summatim.

**σύλληψις** *vel* -ημψις comprehensio, comprehensus, conceptus, *cf.*
syllempsis, - γυναικός conceptio, concepto, - ἐπὶ τοῦ συνσχεθέντος
comprehensio, deprehensio, - ἐπὶ βοηθείας opitulatio.

**συλλογή** collectio, redactus 2. **συλλογίζομαι** computo; decerno.

**συλλογιμαῖον** ὕδωρ lacus.

**συλλογισμός** computatio, pensatio, *cf.* syllogismus (*expl.*
flexuosa et contorta conclusio, collectio sermonum *sim.*).

**συλλογιστικός** rationalis.

**σύλλογος** collocutio, colloquium.

**συλῶ** compilo (*nota ibi formam* συλοῖ), dispilo, expilo.

**συμβαίνω** accido, attingo, contingo, convenio, emergo, escendo,
evenio, obtingo, συμβαῖνον accidens.

**\*συμβαλλομαχία** concinnatio.

**συμβαλλομάχος** commissor (*et* c. pugnae).

**\*συμβαλλομαχῶ** concinno.

**συμβάλλω** aggredior, cogo, committo, confero, confligo, congero,
congredior, congresso, convenio, lacesso, - ἐν πολέμῳ confligo, consero
2., - μάχην committo, confligo, congredior, συμβάλλομαι expedio,
proficio. *Cf.* συμβλητέον.

**σύμβασις** accidentia.

**συμβαστάζω** conveho.

**συμβιβάζω** arbitror, cogo, confero.

**συμβιβασμός** transactio.

**\*συμβιβαστής** conciliator.

**σύμβιος** coniux.

**συμβιῶ** convivo.

**συμβίωσις** coniugium (c. *et* conubium), convictus, convivium,
matrimonium, -εως χάριν ζευχθείς confarreatus.

**συμβιωτής** convictor *et* convivator, conviva.

**σύμβλημα** accidens, commissio.

**σύμβλησις** commissio.

**συμβλητέον** conferendum.

**συμβόλαιον** instrumentum, sportula.

**συμβολή** commissio, commissum, commissura, congressus 2., proelium,
-αὶ ἄρθρων liniamentum (-a), -ἡ λίθου ἢ ξύλου commissura, - ἐπὶ μάχης
ἤτοι πολέμου confligatio, congressio, proelium, - ἡ συμπλοκή congressus
2., - ἐπὶ συναρμογῆς iunctura. *Cf.* symbola (*expl.* collectio
nummorum).

**σύμβολον** auspicium, collatio, indicium, symbolum (*expl. etiam*
signum sive fides sive pecunia congregata, pactum quod cum deo fit
*etc.*), tessera, -α fascis (-es), tessera.
