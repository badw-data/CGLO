effatur IV 335, 37. **eloquar** ἐξείπω τὸ ἐκλαλήσω II 302, 46 (*Verg.*
*Aen.* III 39?). **eloquere** φράσον II 59, 48.

**Elota** puluerata species uel resoluta III 601, 3.

**Eluceo** ἐκφαίνω II 293, 38. **elucet** praefulgit (*vel* -et) IV 61,
25; 32; 515, 41.

**Elucidum** tenue V 628, 31 (*v.* eiuncidum).

**Elucificare** lucidare V 641, 15 (*Non.* 106, 16).

**Elucratio** redemptio II 578, 16.

**Eluctabile** extinguibile (*vel* expugnabile), id est quod uinci
possit V 192, 37.

**Eluctor** ἐκπαλαίω II 291, 61. καταπαλαίω II 342, 49.

**Elucubra\<n\>tes** euigilantes V 498, 18.

**Elucubrati\<o\>** explicatio V 498, 7.

**Elucubratiuncula** *v.* elucubro.

**Elucubratum** euigilatum V 357, 72. euigilantem (?) V 416, 22 (*de*
*verb. interpr. = Hieron. in Matth. prol.*). uigilia elaboratum IV 61,
22. uigile laboratum (uigilia el.?) IV 514, 39.

**Elucubro** est illumino uel expono, hinc **\<e\>lugubratiuncula** est
expositiuncula uel illuminatio V 619, 6.

**Eludo** διαπαίζω II 273, 15. **eludet** καταπαίζει II 59, 43.
**eludit** saigdę (*AS., scr.* waegde: *cf. Gallée p.* 339) *gloss.*
*Werth.* (*cf. suppl.*). **eludere** circumuenire IV 61, 39; V 498,
15. supplantare V 568, 14. **eluderet** auęgdae (*AS.*) V 357, 39 (*cf.
Oros.* III 1, 6).

**Eluens** lauans IV 61, 23; 514, 57; V 498, 17.

**Elul** in Machabaeorum libros (!) Augustus, qui apud nos mensis sextus
uocatur V 193, 1; 207, 13 (= *Eucher. instr. p.* 153, 11). Ilul et anim
(et ethanim?) Hebraeorum lingua September mensis dicitur *lib. gl.*

**Elumbis** lumbo conuulso V 193, 2 (*Fest. Pauli p.* 76, 13).
**elumbe** dissolutum V 628, 29.

**Elu\<mi\>natio** φωτισμός II 474, 31. inluminatio II 578, 17.

**Eluo** ἐκπλύνω II 292, 16. ἐκκλύζω II 290, 47. **eluit** διυλίζει,
ἐκκλύζει, ἐκπλύνει II 59, 56. **elluit** ὑπερκλύζει, κερδαίνει (?) II
60, 3. **eluit** deluit, delauat IV 335, 41. **eluere** unguere,
emundare V 498, 9. elauit IV 410, 15. **eluitur** purgatur V 289, 15
(*Verg. Aen.* VI 742). exsoluitur, purgatur, expiatur V 498, 11.

**Elurescat** (ei. *cod. corr. Havet Arch.* I 449) pallescat V 640, 60
(*Non.* 101, 32).

**Eluscatio** ἀποτύφλωσις II 242, 17.

**Elusco** ἐκτυφλῶ II 293, 32. μονόφθαλμον ποιῶ II 373, 15.

**Elusio** ἐμπαιγμός II 59, 55.

**Elusus** καταπαιχθείς II 59, 44.

**Elutare** ungere V 498, 19.

**Eluuies** (*vel* elubies) κακοσμία II 336, 61. inmunditia II 578, 19.
liquor IV 335, 40; V 358, 19. lacuna V 628, 30. **elludies** ἀρουσία
(*h. e.* elluuies ἀλουσία: *ita dg*) II 59, 52. **eluies** σελματα
(πέλματα *g.* τέλματα *Vulc.*), ἀνοδία (ἐνόδια *d*) II 59, 54. liquor,
quo aliquid eluitur V 289, 1. **elues** liquor quidam, de quo aliquid
eluitur IV 231, 35; V 522, 2. liquor V 498, 10. **elluuies** liquores
quando ab aliquo funduntur V 498, 16. **eluuies** liquores quando aliquo
funduntur IV 61, 43. **eleuiem** purgationem V 640, 76 (= *Non.* 103,
27). *V.* clunis.

**Eluuio** deluuium V 289, 8. **elubio** diluuium IV 335, 38. **eluuio**
pestilens V 641, 6 (= *Non.* 105, 1). **ellubiones** πόροι, περικλυσμοί
II 59, 53.

**Eluxit** luctum deposuit IV 231, 33; 335, 42; V 498, 12; 522, 3; 568,
15. **eluxerunt** lu\<g\>ere cessarunt V 568, 13. *Cf. GR. L.* VII 121,
23.

**Elyseus** \<para\>disus III 520, 30. **Elysium Elysi** sunt campi aput
inferos, in quibus beatos (uentus *vel* uentis) commanere ait poeta
Virgilius (*Georg.* I 38): quamuis Elysios miretur Graecia campos V 192,
30. *V.* lesia.

**Elysius campus** τὸ Ἠλύσιον πεδίον III 237, 18. **Elysios campos**
beatos IV 335, 30 (*Verg. Georg.* I 38). **Elysios** pagani beatos
nuncupabant campos IV 231, 30; V 289, 14 (nuncupant).

**Em** admiratio IV 62, 3; 231, 45; 509, 31; V 193, 4. admiratio uel
interiectio dolentis, ut 'em cui credidi amice' (? -o?) V 289, 35.
increpatio uel admiratio IV 335, 43. increpatio est, **em** (hem?)
admiratio V 453, 14 + 15. **em** increpatio V 568, 23. ecce V 533, 34
(*Ter. Andr.* 619). **em** et **enim** cum increpas, **en** cum
ostendis V 193, 3. ἔα ἔα he, **em** II 282, 57 (*GR. L.* VII 114, 13).
*Cf. Loewe Prodr.* 423 (*ubi Attonis e Polyptico testimonium
exscribitur*). *V.* hem.

**Emacitas** emendi auiditas IV 62, 21; 42; 231, 43; 509, 29; V 193, 5;
289, 38; 498, 25; 598, 7; 628, 32. macitudo (!) V 193, 6. **emarcitas**
marcitudo (macrit.?) V 193, 7. **emacitas** emendi auiditas, marcitudo V
498, 24. **emacitas** (*vel* emarcitas) marcitudo IV 62, 31. *Neque*
emacitas *a macendo neque* emarcitas *a marcendo factum est, immo*
emacitas *et* macritas *confusae videntur. Vide tamen* emax.

**Emanans** actiue τὸ βλύζον III 278, 34. passiue βλυζόμενον III 278,
35.
