**Sine natis** qui filios non habet II 593, 19.

**Sine nomine** ἀνώνυμος II 231, 33. sine dignitate IV 171, 39; 464, 8
(nom. *vel* num. *cf. Verg. Aen.* II 558; IX 343 *et* nomen).

**Sine nundinatione** ἄνευ πραγματείας II 226, 35.

**Sine palpitatione** procul haesitatione, sine dubio V 482, 12.

**Sine parsimonia** ἀφειδῶς II 252, 41.

**Sine patre** ἀπάτωρ II 233, 42; III 72, 25; 125, 36.

**Sine periculo** ἀκίνδυνος II 222, 42; III 72, 14; 125, 15.

**Sine pice** ἀπίσσωτον II 235, 11.

**Sine planitie** ἀνώμαλος τόπος II 231, 32.

**Sine poena** sine uindicta IV 391, 44.

**Sine principio** ἄναρχος III 278, 26; 423, 21.

**Sine procrastinatione** ἄνευ ἀναβολῆς II 226, 29. ἄνευ μελλησμοῦ II
226, 32. ἀνυπερθέτως II 231, 11.

**Sine querella** ἄνευ μέμψεως II 226, 33.

**Sine recusatione** ἀπαραιτήτως II 233, 13.

**Sine script\<i\>one** (scriptore *e*) ἀνυπόγραφον II 231, 12.

**Sine scriptura** ἄγραφον II 217, 10.

**Sine sensu** ἀνόητος III 551, 10. ἄνους III 473, 17; 488, 47.

**Sine seruo** ἄδουλος II 218, 62; III 73, 5; 125, 42.

**Sine societate** ἀκοινώνητος II 222, 60; III 72, 16; 125, 21.

**Sine spiritu** ἄπνους III 473, 16; 489, 23.

**Sine sponsione** sine uadimonio IV 391, 43.

**Sine strepitu fori** V 664, 50.

**Sine studio** ἀφιλόλογος III 334, 3. ἀφιλόπονος II 184, 40 (πονος *ex*
II 184, 41); III 374, 65.

**Sine substantia** ἀνυπόστατον, τὸ μὴ ὑφεστός II 231, 17.

**Sine sustentatione** ἀσυγκρότητος II 248, 56.

**Sine tempore** ἄχρονος III 278, 24 (*unde*?).

**Sin etiam** εἰ καί II 285, 31.

**Sine ulla uarietate** ἄνευ [πονος *v.* sine studio] ποικιλίας τινὸς
ἤ ἀμφισβητήσεως II 184, 41.

**Sine ullo** ἄνευ τινός II 226, 19. **sine ulla** ἄνευ τινὸς θηλυκῶς II
226, 20.

**Sine umbra** ἄσκιος τόπος II 247, 50.

**Sine uxore** ἀγύναιος II 217, 41; III 73, 2; 125, 38.

**Sine uerbere** ἄπληγος II 235, 18.

**Sine uita** ἄβιος II 215, 14.

**Sine uitio** ἀσινής II 247, 42; III 473, 18. χωρὶς αἰτίας III 31, 22.
*V.* sinarus.

**Singentiana** *v.* acorus.

**Singilariter** singulariter V 332, 49.

**Singillatim** καθ' ἕκαστον II 184, 47. uiritim IV 391, 45. per
singulas uices IV 172, 11; 567, 16. per singulas uices, gradatim,
paulatim V 558, 26.

**Singularis** μονομάχος, ἀπρόκοπος II 184, 44. μονογενής … ut Cicero
(*Cat.* III 15): pro singulari eorum merito II 372, 62. μοναχός II 372,
57; III 463, 74; 500, 51. μονάς II 372, 58. ἑνικός II 299, 32. *sine
interpr.* II 559, 35. **singulare** ἑνικόν II 184, 43; III 24, 60.
**singular** ἑνικόν II 299, 31 (singulare *a*). **in singularis** κατὰ
μόνας III 150, 19.

**Singulariter** ἑνικῶς II 184, 48 (singulatur *cod. corr. c*). summe,
prime, unice IV 423, 38. *V.* singilariter.

**Singulatim** καθ' ἕκαστον II 184, 46; 334, 56. per singulas uices IV
285, 31. singillatim V 647, 58 (*Non.* 176, 6).

**Singulator** ἱππαστης, κέλης II 184, 50. **singulatores** κέλητες III
174, 12. 'Equites singulares' *noti sunt. Cf. Arch.* VIII 386.

**Singulta\<n\>s** sugglutiens *a post* IV 391, 45. **singultantem**
λύζοντα, συρρήσσοντα[ς] II 184, 51 (συρίζοντα *H*.).

**Singul[a]tim** ut loquitur quasi per singultos, si aliquis plorat V
332, 60. *V.* singultus.

**Singultio** λύζω II 185, 1. **singulto** λύζω II 185, 3; 363, 4.
ἀποβλύζω II 235, 54. **singultat** λύζει II 185, 2. halat IV 423, 36.
frequenter subglu[a]ttit V 332, 59 (*item cod. Monac. v. suppl.*).
**singulat** halat, spirat V 482, 14.

**Singultus** λύγξ, ὁ λυγμός II 363, 2. λύγξ II 513, 32; 539, 29; 551,
52. λυγμός II 363, 1; III 602, 29. ploratio hominis IV 172, 2.
sugglutium V 514, 4. iesca (*AS.*) V 392, 36. *Cf.* **sigulus**
sigguttium V 513, 53; **singulus** suggultium IV 172, 46. **singultus**
uiscerum pulsus uel suspirium V 150, 17. **singultum** subglutium IV
285, 25. suggluttium V 482, 15. subgluttum V 558, 22; 579, 18. qui
loquitur per gluttuos IV 172, 10; 567, 15; V 514, 7; 610, 31 (singultim
*ed.: quod tutatur Landgraf Arch.* IX 433 *ad Hor. Sat.* I 6, 56
*referens*). ploratio, ploratum V 631, 37. *V.* sugglutius, *Schuchardt*
II 234.

**Singulum** singularem V 647, 24 (*Non.* 171, 15). **singuli** ἕκαστοι,
καθ' ἕνα II 184, 45. **singulis** καθ' ἕν II 184, 49. **per singula**
καθ' ἕκαστα III 426, 11. *V.* ad singula, a singulo, per singula.

**Singulus quisque** ἕκαστος II 288, 24.

**Sinister** εὐώνυμος, ἐπαρίστερος, ἐναντιος II 185, 4. ἀριστερός II
244, 42; III 172, 27. noxius uel con\<trarius\> uentus
