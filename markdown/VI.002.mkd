**Abaso** infirma domus IV 3, 7; 201, 6; 471, 8; V 259, 24; 343, 22;
583, 3 (abb.). infima domus V 343, 11. infama domus IV 301, 6 *et*
*Scaliger* V 591, 21. infirma domus quasi sine base *Seal.* V 591, 32.
est infirmando in qua si sine base (*h. e.* infirma domus quasi s. b.).
V 615, 33 (*unde pendet Breuiloquus teste Ducangio:* domus infirma uel
infirmi, et dicitur ab a, quod est sine, et basis, id est fundamentum).
*Cf. Gloss. Aelfrici* 185, 21 (*Wright-Wülcker*) **abaso**
infirmatorium, seoccra manna hus; *Abbo Sancti Germ. de bellis Paris,
urb.* III *p.* 803, 55 *ed. Pertz; Goetz Arch.* II 346 et 'Der. *der K.
Sachs. Ges. d.* W.' 1896 *p.* 72. *Videtur igitur* abaso
*'infirmatorium' significare. Glossam Hesychii* Ἀβὰς εὐήθης και ἱερὰ
νόσος παρὰ Ταραντίνοις *confert Diels.*

**Ab astratura** ἀπὸ τῆς ἀναβολικῆς II 3, 7; 554, 24. ab (a) stratura
*Heraldus Aduers.* I 1, *Scaliger.* adstratura *alii teste De-Vit.*

**Ab Ausonio** (ab axonio *cod.*) Vlyxis et Calypsus (Calypsis *cod.*)
filio IV 4, 24 (*cf. Seru. in Aen.* III 171, *Festus Pauli* p. 18, 1).

**Abauia** προ[σ]γόνη, μάμμη II 3, 8. προμάμμη II 533, 31.

**Abauia quinta** προμάμμη II 3, 9.

**Abauunculus** frater auiae *Vatic.* 1471.

**Abauus** προπάππου πατήρ II 3, 10. ἀπόπαππος II 533, 30. pater proaui,
id est auus aui IV 301, 1. aui anus IV 472, 47; V 343, 18. tertius pater
II 563, 41. tritaui pater IV 201, 2. pater aui IV 3, 14. abans auus II
564, 4 (*ubi* abans *ex* abaus *repetit Loewe GL. N.* 4).

**Abba** syrum, graece πατὴρ, latine genitor, in uocatiuo (uocatib;
*cod.*) amittit s sicut Pallas et cetera nomina graeca in as exeuntia *a
ante* IV 3, 1. pater IV 201, 1. pater, syrum (syrus *cod.*) est V 259, 4
(*cf. Roensch Mus. Rhen.* XXXI 456). syrum pater, genitor V 412, 25
(reg. *Bened.* 2, 7). *Cf. Isid.* VII 13, 5. *Huc refero* **abba** πιττα
(*ubi* τέττα *cdg:* πιττάκιον *b album praetoris dici sumens, cf.
Lamprul. Alex. Seu.* 21, 8) II 3, 26.

**Abdecet** non decet V 343, 30; 435, 10.

**Abdens[is]** abscondens V 341, 38.

**Abdicans** contra dicans (*de Cassiano*) V 417, 60.

**Abdicatio** ἀποκήρυξις II 237, 35. **abdicatione** excisione IV 482,
19.

**Abdicatus** refutatus IV 482, 18. **abdicatum** exclusum uel foedatum
IV 5, 23.

**Abdĭco** ἀποκηρύσσω II 237, 34. **abdicat** ἀποκηρύσσει II 3, 13; 554,
27. alienat uel respuit IV 3, 2. a se alienat IV 202, 4. a se alienat
uel respuit IV 482, 20. repudiat, expellit aut alienat IV 4, 20. repulit
(scr. repellit), expellit, alienat IV 301, 7. abominat, denegat,
repudiat IV 482, 17. derogat, detrahit IV 301, 8. abiecit, repellit V
259, 5. exheredat V 343, 28. filium pellit de suo iure, exhereditat IV
404, 2. repudiat, alienat, expellit seu filium a suo exheredat iure V
435, 12. **abdicant** repudiant IV 4, 16. **abdicare** alienare uel
exulem fieri IV 482, 16. abicere (*Cassum. inst.* VII 30; *passim*) V
424, 56. **abdicauit** ἀπεκήρυξεν II 3, 12; 554, 26. bisceridae (*AS.*)
V 341, 1. **abdicasset** ἀποκηρύξει(ί) II 3, 11; 554, 25.

**Abdīco** ἀπαγορεύω II 232, 28. **abdixi** destiti (distiti *ade*) IV
301, 11. **abdixit** ἀπεῖπεν, ἀπηρνησατο II 3, 17. ἀπεῖπεν II 554, 28.
negauit V 343, 29 (abduxit), abnegauit, abnuit IV 482, 21; V 625, 3.
abnuit, negauit V 435, 13. auertii *lib. gloss.* amouit IV 201, 41
(abduxit *ab, recte?*).

**Abditae pecuniae** id est repositae V 435, 11.

**Abditus** ἀπόκρυφος II 238, 9. **abditum** ἀπόκρυφον II 238, 8.
ἀποκεκρυμμένον II 237, 33. absconditum IV 4, 18; 478, 29; V 435, 9
(absconditum *in lemm. cod. Cas.*) absconsum, occultum IV 301, 12. *V.*
ablatum.

**Abdo** ἀποκρύπτω II 3, 15; 238, 6. **abdit** abscondit IV 15, 34
(alidit); 301, 10. **abde** κρύψον II 6, 8. **abdidit** ἀπέκρυψεν,
ἀπέκλεισεν II 3, 14. recepit IV 4, 36. occultauit IV 202, 6. occultauit,
abscondit IV 301, 9. abscondit uel texit IV 4, 10; 38 (abdicit),
abstulit, clausit, abscondidit, texuit (!) IV 478, 28.

**Abdomen** λαπάρα, ὑπογάστριον, ὑποκοίλιον II 3, 16. λαπάρα III 248,
37. ὑπογάστριον III 14, 40; 314, 41. graece \<λαπάρα\>, pinguedo carnium
(graece *om. R*) *Plac.* V 5, 7 = V 43, 3. pinguedo carnis III 487, 4;
506, 5; V 615, 38. ueretrum V 632, 2. **abdumen** λαπάρα II 358, 45;
506, 22; 528, 46; 529, 6; 546, 47. ὑπογάστριον II 9, 42; III 87, 31;
255, 20. ὑπογάστριον, λίπος III 183, 64. ilium II 564, 7. **abdumine**
id est pinguedine V 652, 3 (= *Iuuenal.* II 86).

**Abdomini natus** gulae deditus V 660, 4 + 662, 15 (*cf. Ind. Ien.*
1888 *p.* VII).

**Abdormio** ἀφυπνῶ II 254, 9. **abdormit** ἀποκοιμᾶται III 399, 32.
**abdormiui** ἀπεκοιμήθην III 399, 29. **abdormisti**
