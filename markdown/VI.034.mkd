**Aeque quidquam** nihil V 530, 19 (*= Ter. Andr.* 434: *cf. Donat. et
Schlee schol. Ter. p.* 47); (ae. quicquam) *Plac.* V 7, 23 = V 45, 14.

**Aeque uident** non uident *Plac.* V 6, 36 = V 45, 15.

**Aequidialis** ἰσήμερος II 12, 14. ἰσημερινός II 333, 15. aequinoctium
II 565, 26 (*ubi* aequidiale *Loewe GL. N. p.* 9 *dubitanter coll. Festo
Pauli p.* 24, 5).

**Aequidies** aequinoctium II 565, 29 (aequa d. *Loewe GL. N.* 9).

**Aequi gradum** aequi celere (?) IV 405, 10 (*an* aequigradum aeque?).

**Aequilatium** aequatio IV 405, 11; V 591, 3 (aequilancium *Arev.: nisi
potius* aequilauium *est: cf. Festus Pauli p.* 24, 8, *utpote cum multa
ex Festo in glossas* ab absens *transierint: cf. Ind. Ien. a.* 1893 *p.*
3. *Cf. Scaliger ad Festum*).

**Aequilibris** ἀντίρροπος II 230, 33. aequaliter pensans II 565, 27.
**aequilibrum** ἰσόρροπον II 333, 20.

**Aequilibrium** ἰσοσταθμία II 12, 15. aequa pensatio II 565, 1.

**Aequiloquus** iusta siue recta loquens IV 19, 38. iuste loquens V 490,
33. **aequilocus** est iusta loquens V 615, 16; V 437, 49 (*sine* est);
V 164, 12 (*item*).

**Aequimanus** περιδέξιος II 402, 29. bylipti (*vel* bilypti) saxonice
II 565, 20. uocatur qui utraque manu gladium incunctanter utitur *Plac.*
V 45, 16 (gladio *Isid.* X 21).

**Aequimentum** quid sit V 637, 8 (*= Non.* 69, 17).

**Aequinoctialis** ἰσημερινός III 293, 4.

**Aequinoctium** ἰσημερία II 12, 12; 502, 35; III 146, 66; 169, 50; 293,
68; 341, 52; 447, 15; 492, 79; 496, 22; 516, 69. ἰσημερία, ἰσημέριον III
242, 48. ἰσονύκτιον II 333, 17; III 347, 64; 494, 1.

**Aequinoctium autumni** ἰσημερία φθινοπωρινή III 294, 50; 517, 1.
**aequinoctium autumnale** ἰσημερία φθινοπωρινή III 242, 50. ἰσημερία
μεθοπωρινή II 333, 14.

**Aequinoctium uernum** ἰσημερία ἐαρινή II 333, 13; III 294, 49; 516,
72. **aequ. uernale** ἰσημερία ἐαρινή III 242, 49.

**Aequipero** ἐξισῶ II 303, 43. ἐξισοῦμαι II 303, 40. comparo, aequo IV
12, 20. **aequiperas** ἐξισοῖς II 12, 9. **aequiperat** coaequat IV 11,
25. aequat IV 64, 21; V 290, 30. aequat, adsimilat IV 11, 52; V 263, 6.
aequat, adsimulat uel paria facit IV 306, 7. aequat, id est aequa et
paria facit V 262, 44. pares facit, aequat IV 474, 4. aequalem facit IV
232, 43 (equidem *cod.*). aequalem facit, aequat IV 336, 35.
**aequiperant** aequant, conpensant, similant IV 203, 47. **aequiperet**
aequalem faciat V 262, 65. **aequiperabitur** similabitur V 341, 53.

**Aequis oculis** aequo animo IV 12, 33; 475, 2 (*Verg. Aen.* IV 372).

**Aequitalitas** ἰσορροπία II 12, 11 (*ubi* aequitas *vel* aequalitas
*Vulcanius*).

**Aequitas** δικαιοσύνη II 277, 21; III 291, 37 (*inter deas*); 492, 13;
514, 23. δικαιοσύνη, ἰσότης II 12, 10; 560, 24 (*suppl. Boysen:
recte?*). ἰσότης II 333, 22. iustitia V 546, 17. rectitas, iustitia IV
428, 23. numerus equitum et iustitia (*contam.: cf.* equitatus) IV 64,
23; 514, 43; V 164, 13.

**Aequo** ἰσῶ II 333, 50; III 146, 43. ἰσάζω II 333, 11. non iratus
cognosco *post* V 530, 18 (*nisi adiectivum est*). **aequat**
adsimulat, perpendit, intellegit (*v.* perpendo) IV 306, 6. aequiparat
IV 336, 33. **aequet** aequalem facit (faciat *b*) IV 474, 5.
**aequate** planate V 262, 72. planate, temperate IV 474, 6 (*nisi
adverbium est*). *V.* hostorium.

**Aequo animo** εὐψύχως II 320, 46.

**Aequo discrimine** pari interuallo IV 428, 27 (= Verg. *Aen. V* 154).

**Aequor** πέλαγος II 12, 16; 400, 45; III 245, 59. ἅλα, θάλαττα III
433, 45. mare uel campus V 262, 41. mare siue campus ab aequalitate
dictus V 546, 13. pelagus uel campus V 340, 40. mare uel cauma IV 306, 8
(*contam. cf.* aestus). aequo (ab aequo?) mare IV 12, 15. **aequoris**
maris IV 64, 32. **aequora** maria IV 64, 30; 475, 1. maria, aequalia IV
428, 25. non tantum aquae, sed et campi propter aequalitatem dicti V
164, 14. maria aut campi diffusi ab eo quod aequales sint V 164, 15.
maria uel campi ab aequalitate dicti IV 11, 32; 474, 42. maria ab
aequalitate IV 203, 51. campi IV 204, 1.

**Aequora placat** fluctus maris mitigat IV 64, 33; V 498, 56 (*Verg.*
*Aen.* I 142).

**Aequor \<ar\>andum** nauigandum, eo quod carina sulcum faciat V 164,
16 (Verg. *Aen.* II 780): *inde* aequorare *in glossis arabicolatinis
explicandum.*

**Aequoratus** qui iurat in mare II 565, 19. *Cf. Loewe GL. N.* 8,
*praef. p.* VII.

**Aequore toto** (totum *codd.*) per totum mare IV 474, 43; V 262, 27
(*Verg. Aen.* I 29).

**Aequorius** (*vel* -eus) marinus II 565, 18.

**Aequum censeo** δικαιῶ II 277, 25.

**Aequus** ἴσος II 333, 21; III 250, 4; 332, 14; 523, 60. δίκαιος II
277, 23. clemens, bonus V 530, 56 (= Ter. *Ad.* 837). **aequum** ἴσον,
δίκαιον II 12, 18. iustum IV 11, 24; 64, 29; V 262, 43.
