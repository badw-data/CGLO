incipiam V 376, 18. **ordire** incipere V 651, 10 (*Non.* 39, 22).
**orsus** ἠρξάμην II 325, 43. **est orsus** ἤρξατο II 325, 44. **orsus**
loqui coepit IV 134, 5 (*Verg. Aen.* I 325); V 127, 29. locutus aut
loqui coepit IV 546, 50. *Cf.* **orsus** locutus uel qui loqui coepit IV
372, 51.

**Ordior telam** τάξις, τάγμα, ἀκολουθία, βουλή, ψυκρά (?) II 139, 47
(*h. e.* ordior t \*\*\*: ordo τάξις et *q. s. cf. e*).

**Orditorium** διαστρα (δίασμα?) III 322, 8 (*cf. Funck Arch.* VIII
382).

**Ordium** δίασμα III 209, 56 (orditum?); 323, 70. **ordia** initiamenta
V 471, 56 (*cf. Lucr.* IV 28; *Arch.* VI 433). *V.* orsum, exordium.

**Ordo** τάξις II 451, 32; 490, 33; 512, 37; III 299, 20/19; 413, 29;
458, 55; 504, 9. τάξις, βουλή II 538, 63. τάξις ὁ στίχος II 551, 14
(*v.* ordior telam). τάγμα II 543, 1. βαθμός II 255, 19. βουλὴ ἐπὶ τοῦ
συστήματος II 259, 22. βουλευτήριον II 259, 18. acies IV 372, 42.
**ordines** τάγματα, τάξεις III 279, 7. *V.* curia, extra ordinem.

**Ordo equester** equitum ordo IV 134, 17; V 317, 4. secundum senatum IV
134, 14; V 377, 22.

**Oreae** (oriae *codd.*) freni et habenae, ab ore dicuntur *Plac.* V
37, 2 = V 90, 14 = V 127, 39 (expectat dictum, a matando id est
aiuuamendo *add.*: *v.* omentat). *Cf. Festus Pauli p.* 8, 12; *Fest.*
182, 23. **oreae** frenae (!) V 377, 20. **oleae** frena equi V 126, 9.

**Oreades** nymphae sunt a montibus cognominatae, sicut Dryades ab
arboribus V 229, 26. nymphas montium Oreades pagani dixerunt V 229, 27.
nymphae montanae V 127, 21. nymphae IV 456, 28 (*Verg. Aen.* I 500).
*V.* nympha. *Cf. Festus Pauli p.* 185, 9; *Serv. in Ecl.* X 62.

**Ore fauete** silete IV 134, 31; 456, 27 (*Verg. Aen.* V 71).

**Ore legam** o\[b\]sculando legam IV 456, 29 (*Verg. Aen.* IV 685).
obsculando colligam V 471, 58. **ore lega** obsculandum colligat V 529,
6.

**Oresta (?)** thres (*AS.*) V 376, 6.

**Orestis tragoedia** ubi prosternuntur multi homines in bello uel
filius Agamemnonis *glossa apud Hagenum Grad. ad cr. p.* 97, *GR. L.
suppl.* 236, 11.

**Orexis** delectatio ciborum insatiabilis III 603, 32. commestio et
(comestionis est *Wirz*) desiderium, qui extra naturam uel consuetudinem
amplius comedit V 654, 48 (*Iuvenal.* VI 428).

**Organarius** ὑδραύλης II 462, 9; III 10, 49; 172, 48 (ydraulus); 240,
5; 302, 41; 371, 79; 458, 58; 486, 29. ὀργανάριος II 386, 18. qui utitur
organum V 574, 33. οἱ ὑδραῦλαι **organ\<ari\>i** III 240, 6.

**Organum** ὕδραυλις II 462, 10. **organus** ὕδραυλις II 489, 15.
ὄργανον II 386, 20.

**Orge** occide IV 544, 10; V 316, 70; 376, 29; 606, 18 (urge
*Reinesius. uocem celt. agnoscit Schlutter*).

**Orgia** Διονύσου μυστήρια (singularia non habet) II 278, 22. μυστήρια
Διoνύσου (singularia non habet) II 374, 31. *Cf.* τὰ Διoνύσου μυστήρια
Bacchi **orgia** III 239, 35. **orgia** est graecum, id est sacra quae
per furorem celebrantur, ut Liberi patris et matris deum (*cf. Serv. in
Aen.* IV 302). org\<y\>ia autem (*item GP*) mensura, quod latine
dicitur ulna *Plac.* V 35, 12 = V 90, 13 = V 127, 34 (urna). sacra
Liberi patris IV 134, 32; 456, 30 (*gl. Verg.*); V 658, 7. sacra Liberi
V 555, 21 (*cf. Serv. in Aen.* IV 302; VI 657). mysteria Bacchi V 377,
21. mysteria IV 134, 30; 372, 45; V 317, 3. mysteria, secreta IV 265,
53. initia sacrorum IV 134, 18. mysteria uel nocturna cura (sacra?) uel
initia IV 134, 8; 545, 28. *V.* Bacchus.

**Oriam** dicunt nauicellam modicam piscatoribus (piscatoriam?) V 508,
50; *cf. Non.* 533, 21; *Wessner Comm. Ien.* VI 292, 17; 115. *V.*
horea.

**Oricula** *v.* auricula.

**Oricus** loquax V 630, 26. loquax qui magnum os habet *Scal.* V 606,
15 (*Osb.* 404; oritus *Arevalus*).

**Oridurius** σκληρόστομος II 433, 42 (oridorus *cod.* -durus *a*); III
159, 34; 179, 50; 251, 74. aspere loquens IV 265, 42; V 508, 49. *V.*
aridurus.

**Oriens** ἀνατολὴ καὶ ἀνατέλλων II 139, 49. ἀνατολή II *p.* XXXVII;
490, 44; 512, 35; 554, 19; III 293, 49; 426, 32; 488, 40; 507, 62.
Ὑπερίων II 464, 29; III 170, 3; 241, 26. **orientem** ἀνατολήν III 413,
40. *Cf.* ad orientem, ab oriente.

**Orientalis** ἀνατολικός III 127, 9; 337, 45; 458, 59; 486, 44.
**Orientales** Aonides ('*contam. cum* Aeoae' *Buech.*) III 508, 24.

**Orificio** (orificium *a e*) στόμιον II 438, 22.

**Origanum** ὀρίγανον III 265, 59; 359, 37. ὀρίγανον, ὀριγανίς III 317,
31. **origano** colena III 555, 36 (conila *Pseudapul.* CXXII); 619,
61. collenuo III 622, 43; 558, 50 (origanum). canula galica (cunila
Gallica *Pseudapul.*) III 557, 45; 621, 66 (calica). conicia III 555, 35
(aurig.); 619, 60. conitiae sunt (siue *Schmidt*) colena **origano** III
545, 13. cunitiae id est colena siue **origano** III 537, 19.
**origanus** puleius maior III 593, 27; 627, 16. puleius maior uel
