**Seminatio** σπόρος II 436, 2; 495, 38; III 199, 63; 356, 67. κατασπορά
II 343, 56.

**Seminator** uel pater IV 170, 2 (*cf.* sator).

**Seminator uerborum** σπερμολόγος III 503, 69. *V.* seminiuerbius. *Cf.
Vulg. Act.* 17, 18; *Roensch 'It.' p.* 227.

**Seminatum** ἐσπαρμένον III 260, 58 (*unde*?). **seminata** ἐσπαρμένα
III 199, 32.

**Seminex** ἡμιθανής II 324, 44; III 279, 64. **seminecis** ἡμισφαγής II
325, 4. ἡμίπνικτος II 324, 63. *sine interpr.* II 559, 28. **seminecem**
ἡμιθνῆτα καὶ ἡμιθανῆ II 181, 53. id est prope mortem (prope mortuum
*Landgraf Arch.* IX 425) IV 390, 37. prope mortuum IV 284, 19.
semimortuum V 631, 34.

**Seminiuerbius** seminans uerba *Pap.*

**Semino** σπείρω II 435, 35; III 260, 62.

**Semiorbis** ἡμικύκλιον II 324, 50.

**Semipar** humilior V 513, 38.

**Semipes** ἡμίπους II 324, 65. claudus IV 170, 41; V 149, 1; 481, 53;
513, 34; 636, 63.

**Semiplagium** minus rete dicitur uel adiunctae retibus lineae restes
plagae dicuntur *lib. gloss.*, *Mai* VII 579. *Cf. Isid.* XIX 5, 1
(sympl.).

**Semiputatum** ἡμικλάδευτον II 324, 48.

**Semirutus** ημιχωστος II 325, 12. **semirutus semirutum** ἐρείπιον II
314, 29. κατερρειπωμένον II 345, 42.

**Semis** ἥμισυ, ἡμιασσάριον II 181, 54. ἥμισυ II 325, 3. ἑξαούγκιον II
301, 55. **semissem** ἡμισείας καὶ τὸ ἥμισυ τοῦ νομίσματος II 181, 47.
**semisse** ἡμισείας II 182, 1. *Cf.* **semissem** II 559, 30.

**Semis caput** ἡμικέφαλον III 183, 67.

**Semisicium** (semispathium?) semigladium quasi ad modum dimidiae
spathae IV 283, 46. semigladium in modum dimidiae spatae V 513, 37. *Cf.
Isid.* XVIII 6, 5.

**Semisomnis** ἡμίυπνος II 325, 7. **semisomnos** non plene dormientes V
578, 54.

**Semisphaerium** ἡμισφαίριον III 292, 58.

**Semita** ἀτραπός, ἡμιόδιον II 181, 55. ἀτραπός II 250, 25; 493, 71;
III 196, 21; 306, 33; 395, 61. τρίβος ἡ ὁδός II 458, 53. ἀτραπός, τρίβος
III 267, 43; 353, 69. ὁδὸς στενή II 379, 24. στενὴ ὁδός II 437, 10.
**semita\<e\>** uiae IV 390, 35.

**Semitritum** ἡμιτριβές III 322, 39.

**Semitruncus** *v.* mutilis.

**Semitula** *v.* ruga.

**Semiuncia** οὐγκίας ἥμισυ II 388, 53.

**Semiustus** (semiiustus *codd.*) dimidium [i]ustus IV 390, 34; V
610, 50 (iusti.). **semiustum** ex parte praeflammatum V 244, 9.
semiustu[mu]latum et incensum V 244, 8. medium ustum IV 284, 22.

**Semiuecors** minus a corde V 481, 49. min[im]us a uecordia IV 423,
22. min[im]us a uecorde *Scal.* V 610, 65.

**Semiuir** ἀνδρόγυνος II 225, 19. **semiuiro** effeminato IV 283, 15.

**Semiuiridis** ἡμιθαλής II 324, 43. ἡμίχλωρος II 325, 11.

**Semiuiuus** ἡμίφωνος III 174, 27; 279, 62 (*v.* semiuocalis). medius
uiuus IV 283, 40. pallidus IV 390, 36. **semiuium** ἡμιθανῆ II 182, 2.
*V.* semianimis.

**Semiuocalis** ἡμίφωνος II 325, 8; III 279, 62. **semiuocale** ἡμίφωνον
γράμμα II 325, 9. **semiuocales** ἡμίφωνα III 328, 14; 375, 57. *V.*
semiuiuus.

**Semnion** monasterium IV 169, 34. honestorum conuenticulum V 422, 29 =
431, 20 (*de Euseb.*). est honestorum conuenticulum V 624, 9.

**Semo** *v.* simones.

**Semodius** ἡμιχοίνικον III 366, 36. **semodium** ἡμιμόδιον II 324, 52;
III 322, 20. *V.* satum.

**Semorum** (semotum *cod. corr. Buecheler Mus. Rh.* XII 132: *nisi
contam. est*) sine mora, actutum IV 390, 38. *V.* actutum.

**Semotam** affectam (auectam *Nettleship 'Journ. of Phil.'* XX 58) uel
sequestratam IV 171, 7. **semoti** separati IV 283, 47 (*ubi* -tim-tim
*a b*).

**Semoto** ἀποχωρίζω II 242, 46. ἀποκινῶ II 237, 37.

**Semper** ἀεί, πάντοτε II 181, 57. ἀεί II 219, 8; III 242, 30. πάντοτε
II 393, 51. διὰ παντός II 273, 16. διηνεκῶς II 277, 3. diapsalma III
515, 24 (*cf.* sympsalma).

**Semperuiua** zion (aizoon *Pseudapul.* CXXIII) III 579, 51
(semperuiuum). ezoani III 561, 59. agigon semen **semperuiuo** III 543,
31. **semperuiua** absdios (aizoos?) III 536, 30. *Cf.* abdius id est
azon III 587, 39; 608, 10; 617, 29. **semperuuui** anguyu III 552, 69.
**semperuiua** id est Iouis barba III 591, 45; 625, 16; 629, 8.
**semperuiuo** Iouis barba III 566, 46; 595, 16; 613, 1. barba Iouis id
est **semperuiua** uel herba hyruli (? *v.* erba Hirculi *sub* Iouis
barba) III 617, 68. abdius agria id est uigo (Iouis?) barba uel
**semperuiua** III 580, 9. *Cf.* aizos quod supra III 542, 15; aison id
est III 536, 16. *V.* centaurion, Iouis barba. *Cf. Diosc.* IV 86.

**Sempiterne** semper V 647, 14 (*Non.* 170, 15).

**Sempiterno** διὰ βίου II 270, 17.

**Sempiternus** αἰώνιος II 181, 58; 221, 41. διηνεκής II 277, 1. ἀείδιος
II 219, 9;
