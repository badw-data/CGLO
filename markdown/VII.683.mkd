**χρηστώς** suaviter, utiliter.

**χρῖσις** liniatura, linimentum, oblitus 3.

**χρῖσμα** tectorium. *Cf.* chrisma (*expl.* unctio).

**χρίστης** dealbator, linitor, - ὁ κονιατής tector.

**χριστός** Christus, salvator, unctus, -ά pulvis de tegula cruda.

**χρίω** lino, oblito, uncto, κεχρισμένος oblitus 1.

**χροιά** *et* **χρόα** color.

**\*χρόισις** coloratus 2.

**\*χροϊσμός** coloratura.

**χρονίζω** moror, tardo, -ομαι moror.

**χρονικός:** -ή dracontea. *Cf.* chronicon (*expl.* temporale).

**χρόνιος** diutinus, diuturnus, temporalis, *cf.* chronia (*expl.*
veteris causa). *Cf.* νομή.

**χρονιότης** morbus regius sine febre.

**χρονογράφος** *cf.* chronographum (*expl.* tempora vel scripturis,
seriem).

**χρόνος** aetas *et* aevitas, aevum, tempestas, tempus (*cf.*
chronus), - στρατείας stipendium, - ὁ μεταξὺ ἀλεκτρυοφωνίας conticinium,
ἐνεστώς - instans tempus, ἐπιὼν ὁ μετὰ ταῦτα - posterus, μέλλων -
futurus (-um), posteritas, *cf.* praeteritum tempus, πενταετὴς -
quinquennalis, τετραετὴς - quadriennium, ὀλίγῳ χρόνῳ brevi tempore, ᾧ
δήποτε χρόνῳ quandocumque, χρόνῳ ἐνιαυτοῦ tempore anni, διὰ χρόνου post
tempus. *Cf.* διάστημα, ἐλεύθερος, μῆκος.

**χρυσαλλίς** blatta.

**χρύσαμμος** balluca.

**χρυσάνθεμος** *et* -ον artemisia traganthes, - βοτάνη viola rustica.

**\*χρυσάνθινον** phlomi flores.

**\*χρυσαργύριον** auraria.

**χρυσαττικόν** chrysatticum, *cf.* crissaticum (*expl.* genus quoddam
vini).

**\*χρυσεκλέκτης** aurilegulus.

**χρύσεος** *et* **χρυσοῦς** aureus. *Cf.* νόμισμα.

**\*χρυσεψητής** auricoctor.

**χρύσινος** aureus, solidus, διπλοῦς - ἤτοι ὄβολος dupondium, τέταρτον
-ου scripulus, εἰς -ους ῥ denariis centum.

**χρυσίον** aurum. *Cf.* γήδιον, διανομή.

**χρυσίτης** *et* **χρυσῖτις** mercurialis, millefolium, sandaraca,
spuma argenti.

**χρυσοκάνθαροι** bulli.

**\*χρυσόκανθος (?)** *cf.* chrysocanthos (*expl.* peristola *vel*
periscola).

**χρυσοκόλλα** atriplex *sim.*

**χρυσοκόμος** auricomus.

**χρυσολάχανον** atriplex, holus.

**χρυσόλιθος** *cf.* chrysolithus (*expl.* colorem aureum habet et
stellas).

**χρυσολόγος** (*nomen proprium*?) *cf.* chrysologus (*expl.* aureus
sermo).

**χρυσόμηλα** cydoneum (cidon.), armoniacia.

**\*χρυσονήστρια** aurinetrix.

**\*χρυσόπλευρος** salpa.

**χρυσόπρασος** *cf.* chrysoprasus (*expl.* viridem habet colorem).

**χρυσορύκτης** *v.* χρυσωρύκτης.

**χρυσός** *et* -όν (?) aurum, εἶδος -οῦ absitanum. *Cf.* γήδιον,
δοκιμάζω, ἔλασμα, ἐλάτης, ὑποδέκτης.

**χρυσόσημον** auroclavum.

**χρυσοτελεῖς** auriculae (*at cod. recte* auricolae)

**χρυσουργεῖον** *cf.* auraria.

**χρυσοῦς** *v.* χρύσεος.

**\*χρύσοφος** aurata. *V. sequ.*

**χρύσοφρυς** *et* - ὁ ἰχθύς aurata.

**\*χρυσόχαλκος** aurichalcum.

**χρυσοχοεῖον** aurificina.

**χρυσοχόος** aurifex. *Cf.* ζυγόν.

**\*χρυσοψέλια** periscelis (-ides).

**\*χρυσυποδέκτης** susceptor aurarius.

**χρυσῶ:** χρυσωθείς *et* κεχρυσωμένος auratus. *Cf.* χρυσωτός.

**\*χρυσωρύκτης** *vel* χρυσορ. aurifossor, aurilegulus. *Cf.*
aurifodina.

**\*χρυσωρυχεία** aurifodina.

**χρυσωτής** aurarius, aurator, deaurator, inaurator.

**χρυσωτός** auratus.

**χρώζω** *v.* χρώννυμι.

**χρώμα** color (*cf.* chroma), fucus, - ἀνθινόν floralis color, -
πήλινον luteus (-um), - ὑάλινον ferrugineus (-um).

**χρῶμαι** usito, usurpo, utor, κέχρηται ὀφφικίοις fungatur officiis,
χρησάμενος usus 2.

**χρωματίζω** coloro, κεχρωματισμένος coloratus.

**χρωματικός** colorinus, procax.

**χρωμάτιον** auripigmentum.

**\*χρωματοπώλης** pigmentarius.

**χρώννυμι** *vel* **χρώζω:** κεχρωσμένος coloratus.

**χρώς** color.

**χυδαῖος** cibarius, vulgaris, - ἄνθρωπος gregarius, - ἄρτος cibarius.

**χύδην** passim, permiscue.

**χυλιστά** *cf.* quinista (*expl.* confectio in dynamidia).

**χυλός** sucus, - σαπικός sapa sapae, - θαψίας sucus thapsiae, - κισσοῦ
sucus hederae, διὰ χυλῶν *cf.* fascimentum.

**χύμα** commixtio, confusio.

**χυμός** humor.

**χύννω** fundo 2.

**χύσις** fusio, fusus 2.

**\*χυτήρ** fusorium.

**\*χύτης** fusilarius, fusor (*cf.* chytes).

**χυτόν** fusile, fusillae.

**χύτρα** *vel* εἶδος χύτρας caccabus, fitilla, lepista, - ἱερέων *cf.*
auxilium, -τετρημένη
