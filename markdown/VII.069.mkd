**Percussura** \<κον\>δύλωμα III 599, 40. plaga **percussura** *margo*
II 146, 8.

**Percussus** ἐκπλαγείς, ἀπόπληκτος II 146, 8. plexus, punitus,
conuulsus, iactatus IV 375, 45. **percussi** (-lsi *de*) admiratione
moti IV 375, 44. *V.* non percussus.

**Percutio** παίω II 392, 27. πατάσσω II 399, 40. πλήσσω II 410, 9.
πλήττω II 410, 11. **percutit** πλήσσει, παίει, ῥαπίζει II 146, 23.
ἔπληξεν (= perculit). κρούει II 146, 6. quatit, commonet IV 375, 46.
**percussit** ἐξέπληξεν II 146, 5. *V.* prima peto.

**Per desiderium** κατ' ἐπιθυμίαν II 345, 40.

**Perdicale** morale (muralis *Pseudapul.* LXXX) III 548, 10 (*v.*
parietaria).

**Perdicaria** cionefico (?) III 544, 52.

**Perdicax** *v.* peruicax.

**Perdite** *et* **perditim** *v.* amat perditim.

**Perditio** ἀνάθεμα III 488, 75; 509, 56. ἀπώλεια II 243, 35.

**Perditrix** ὀλέτειρα, ὀλεθρία II 146, 19.

**Perditus** ἐξώλης II 304, 47. ἀπεγνωσμένος II 146, 11; 149, 9.
ἀπεγνωσμένος καὶ ἀπολωλώς II 146, 17 (*Cic. in Cat.* II 4, 7).
ἀπολλύμενος II 238, 39. ἀπολόμενος II 238, 44. **perditissimorum**
ἀπολλυμένων II 146, 15; 149, 8. *V.* tam perditus, conuenticulum
perditorum.

**Perdix** πέρδιξ II 402, 2; III 90, 27; 257, 50; 319, 14; 15 (περνιξ);
361, 12; 435, 39.

**Perdo** ἀπολλύω II 238, 37. ἀπόλλω III 126, 2. perdis ἀπόλλεις III
126, 3. **perdit** ἀπόλλει III 126, 4. a\[d\]mittit IV 375, 47.
**perduint** perdant V 132, 31. **perdidi** ἀπώλεσα III 126, 5.
**perdidisti** ἀπώλεσες (!) III 126, 6.

**Perdocilis** ualde docilis IV 142, 3; 271, 49; 550, 51; V 131, 45;
320, 19. satis docilis IV 375, 48.

**Perduco** διάγω ἐπὶ τῷ διελθεῖν II 270, 34. ἀπάγω II 232, 30.
**perducit** καθιστᾷ, διασώζει, διάγ\[αγ\]ει II 146, 18. *V.* ad exitum
perducunt.

**Perductione** seductione V 473, 3; 575, 40.

**Perductus** ἀχθείς II 146, 16.

**Perduellio** ἀντάρτης II 228, 51. rebellator IV 271, 7; V 473, 6; 510,
6. rebellatio V 320, 10 (*gloss. Werth. Gallée* 340. *v. suppl.*).
eleuatio (!) IV 375, 51. **perduelliones** οἱ κατὰ τοῦ δήμου τοῦ Ῥωμαίων
βουλενόμενοι ἢ κατὰ βασιλέων II 146, 13. rebelliones per quos bella
oriuntur, duellum enim dicitur quasi duorum bellum *Plac.* V 38, 8 = V
92, 2 (rebellationes per quod) = V 132, 43.

**Perduellis** hostis V 319, 22; IV 140, 33 (perduellius). affectus
(affectans *lib. gl.*) tyrannidis V 510, 7; IV 140, 37 (*uel* tyrannidem
*codd.*). inimicus V 529, 18 (*cf. Serv. in Aen.* IV 424). hostis uel
bellator *c post* IV 141, 11; V 131, 43; 232, 21. **perduellem** hostem,
inimicum V 473, 4. **perduelles** hostes IV 375, 50. aduersarii,
inimici, hostes V 473, 5. **perduellibus** rebellionibus IV 271, 39; V
510, 8.

**Perduellium** rebellatio IV 140, 23; 141, 12; V 131, 44. dicitur
bellum eo quod ex utraque parte geritur V 378, 42. dorh gifect (*AS.* =
per duellum) V 378, 19. perduellium est *in not. Tiron.* 91, 56.

**Perduro** ἐπιμένω II 309, 39. διαμένω II 272, 46.

**Peredo** κατεσθίω II 345, 44 (prędo *cod.*).

**Pereger** peregrinus, hospis, aduena IV 375, 53.

**Peregre** ἐπὶ ξένης II 310, 3. aduerbialiter ἐπὶ ξένης, ἀπὸ ξένης, ἐπὶ
ξένην II 146, 24.

**Peregrinatio** ἀποδημία II 146, 31; III 124, 10. ἀποδημία ἐπὶ ξένης II
236, 28. ἐκδημία II 289, 21. ξενία II 377, 54.

**Peregrinatus** ξενία II *praef.* XXXVII, 512, 61.

**Peregrino** ἀποδημῶ III 124, 7. **peregrino peregrinor** ξενιτεύω II
377, 58. **peregrinas** ἀποδημεῖς III 124, 8. **peregrinat** ἀποδημεῖ
III 124, 9. ξενιτεύει III 342, 43; 459, 61. **peregrinatur** ξενιτεύει,
ἀποδημεῖ II 146, 25.

**Peregrinus** παρεπιδήμιος II 146, 33. ἀπόδημος, ξένος, ἐπίξενος II
146, 26 (*cf. margo*). ἀπόδημος II 236, 26; III 124, 11; 337, 37; 459,
60. ξένος II 378, 2; III 505, 18. *Cf.* ξένος hospes, **peregrinus**
unde ξένος II 557, 38. **peregrinus** extraneus, alienus V 555, 52.
long\<e\>. a patria positus quasi alienigena V 131, 49. *Cf.* πρὸς
Ἕλληνας **ad peregrinos** III 53, 1. *V.* lege peregrina, indicium
peregrinum; *Isid.* X 215.

**Peremptorius** διηνεκής II 277, 1. **peremptorium** διηνεκές II 276,
56.

**Peremptus** ἀνῃρημένος II 227, 15. interfectus IV 142, 28; 271, 20.
**perempta** \[per\]leuata V 320, 34. **peremptum** interfectum,
interemptum IV 457, 38 (*Verg. Aen.* VI 163). occisum V 131, 46.
**peremti** occisi V 131, 47.

**Perendie** εἰς τρίτην II 146, 32; 287, 53 (perinde *cod. corr. e*);
III 296, 14; 517, 11. post cras IV 375, 52. pus cras IV 271, 24. post
crastina IV 141, 38; 550, 45; V 131, 48; 232, 22 (pos *uel* post). per
duas noctes V 381, 14 (super *cod. Epin.*). dicitur sicut hodie uel
cras V 575, 44 (*GR. L.* VII 284, 8). die tertia *gloss. Werth. Gallée*
341 (*v. suppl.*).
