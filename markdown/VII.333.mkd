*suppl.* **tantidem** quippe *ibidem.* **tantundem** ipsum *cod. Monac.
v. suppl.*

**Tantillus** modicus IV 181, 23; 572, 16 (-tillus *vel* -tulus); V 156,
2.

**Tantisce (!)** pro tantis V 155, 36.

**Tantisper** ἐν τοσούτῳ, ἐπὶ τοῦ παρόντος II 195, 26. ἐν τοσούτῳ II
300, 46. ἐπὶ τοσοῦτον II 312, 18. ἄχρις τοσούτου II 254, 46. interim IV
290, 9; 396, 5; 572, 17. interim uel ita demum IV 181, 24. interim uel
iterumque (?) V 156, 3. pus suipae (*vel* dus suidae, *AS.*) V 397, 15.

**Tantocius** tanto uelocius IV 181, 40; 290, 10; 396, 6; V 156, 5; 545,
47. tanto uelocius, tanto celeriter V 485, 25. tam citius IV 181, 22;
572, 15 (citus); V 155, 35.

**Tanto magis** τοσούτῳ πλέον II 457, 39. omnino amplius uel eo amplius
IV 572, 35.

**Tanto melior** τοσούτω κρείσσων II 195, 30.

**Tantopere** σπουδαίως II 436, 11.

**Tanto plus** τοσούτῳ πλέον II 457, 39.

**Tantulum** tantundém IV 396, 7; V 580, 30. modicum V 156, 1.
**tantullum** modicum V 545, 48. *V.* tantillus.

**Tantum** μόνον II 373, 7. μόνον, τοσοῦτον II 195, 32.

**Tantummodo** αὐτὸ μόνον, μόνον αὐτόν II 195, 27. μόνον II 373, 7.
quantum et illud IV 181, 25; *cod. Monac. v. suppl.* quantum et (ex
*codd.*) illud uel tantidem IV 396, 8. *V.* tantidem.

**Tantum quod** ἀρτίως II 195, 33.

**Tantundem** τοῦτο II 456, 35. τοσοῦτον II 457, 38. id ipsum V 397, 30.
eadem, similitudo uel simili (similiter *b*) IV 572, 39. similiter (*ita
abd.* aliquando, demum *cod. Sangall.: v.* tandem) IV 290, 22. qui supra
(*quo pertineat in­certum*) IV 181, 29. *Cf.* tantidem.

**Tantus** τηλικοῦτος II 455, 7. τοσοῦτος II 457, 36. **tanta** τοσαύτη
II 457, 33. **tantum** tam magnum IV 181, 37; 466, 7 (*gl. Verg.*).
**tanti** περὶ πολλοῦ II 195, 28. **tanto** τοσούτῳ III 6, 52. **tanti**
τοσοῦτοι καὶ τοσούτου II 195, 34. τοσοῦτοι II 457, 37. tam magni (magna
*cod.*) V 156, 8. **tantae** τοσαῦται II 457, 34. **tanta** τοσαῦτα II
457, 35. *V.* tantisce, tos, in tantum.

**Tanumandatum (?)** humilem V 580, 40 (*cf.* tanis humile mandatum
interpretatur *Papias: quae glossa in libro gl. Hieronymo adscribrtur*).
*Cf. Onom, sacra p.* 51, 3 (Tanais mandans humilia).

**Tapes** ψίεθος II 539, 46; 552, 2.

**Tapeta** τάπης II 502, 5. ryae (*AS.*) V 396, 52. **tapetum** τάπης II
451, 46; 501, 32; 528, 13; III 473, 48. uesti\<s\> stramenti (?) II 594,
66. **tapedus** ταπίς (τάπης?), ψιλή II 195, 35. **tapite** τάπης III
369, 66. *Cf. GR. L.* I 61, 29; IV 129, 25. *V.* tabetum.

**Taphos** sepulcrum graece. epitafium ergo dici potest quidquid in
memoriis scribitur V 155, 24; 247, 19.

**Taprobane** nomen insulae V 580, 35. *Cf. Serv. in Georg.* I 48;
*Isid.* XIV 3, 5; 6, 12.

**Tapsia** *v.* ferula agrestis, f. minor.

**Tara\<ta\>ntara** uox tubae V 558, 45. est uox setaciorum V 624, 34
(*v. Ducange s. h. v., Wright-Wuelcker p.* 615, 18). *Cf. Serv. in
Aen.* IX 501; *GR. L.* II 540, 6; cerniculum.

**Tardarius** *v.* ambro.

**Tardatio** βράδος II 546, 36. tarditas V 545, 49; 580, 39.
**tardatione** tarditate *a post* IV 181, 35.

**Tarde** βραδέως II 195, 38; III 465, 19.

**Tarditas** βραδύτης II 259, 55; III 130, 47; 465, 20. νωχέλ\<ε\>ια II
377, 48.

**Tarditatis** ἐπεχόντων III 305, 57; 517, 70.

**Tarditudine** tarditate V 648, 22 (*Non.* 181, 14).

**Tardo** βραδύνω II 259, 53; III 130, 44. χρονίζω II 478, 54.
**tardas** βραδύνεις III 130, 45. **tardat** βραδύνει II 195, 37; III
130, 46; 465, 17. χρονίζει III 7, 19; 465, 16.

**Tardus** βραδύς II 195, 36; 259, 54; III 130, 42; 178, 8; 250, 43;
340, 43; 420, 16; 465, 18; 473, 49. ὄψιμος III 244, 16 (*unde*?).
**tarde (?)** βραδύ (bradi) III 130, 41. **tardas** seras IV 396, 9.
**tardius** βραδύτερον III 130, 43.

**Tarea** uestis regia aut toga purpurea IV 181, 38; V 247, 28.
**tarica** (*vel* taruca) uestis regia V 398, 32. **talio** uestis regia
uel stola *a post* IV 181, 37. **taruca** uestis regia II 594, 60:
*quibus omnibus* trabea *subesse iam Nettleship 'Journ. of Phil.'* XX
185 *vidit. V.* taurina.

**Tarichus** salsus V 526, 26; 527, 1; 580, 38.

**Tarmus** uermis in carne V 612, 30; 637, 1 (*cf. Isid.* XII 5, 18 *et
AHD. GL.* III 261, 66; *Fest. Pauli p.* 358, 5; *Loewe Prodr.* 288).
*V.* termites.

**Tarpeium limen** id est Capitoli\<n\>um limen V 655, 1 (*Luvenal.* VI
47).

**Tarsi** (*vel* tharsis) exploratores gaudii IV 181, 39 (explorata
gaudia *Mai* VI 547). *Cf. Onom. sacra* 43, 26 (Tharsis exploratio
gaudii).

**Tartareum custodem** canem tricerberum IV 466, 9 (*Verg. Aen.* VI
395).
