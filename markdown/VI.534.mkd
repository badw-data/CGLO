459). **iactor** ῥιπτάζομαι II 428, 20. σκύλλομαι II 434, 20.
**iactatur** ἀλαζονεύεται II 75, 39. ῥιπτάζεται, σκύλλεται II 75, 29.
quod leuissimum est V 659, 18. **iactantur** ῥίπτονται, σκύλλονται II
75, 37. ἀκοντίζονται II 75, 38. **iactemur** uexantes erremur IV 445, 16
(*Verg. Aen.* I 332: uexanter erremus?). **iactari** καυχᾶσθαι II 75,
20.

**Iactuose** iactanter (iactuosae iactantes *Iunius*) V 643, 29 (*Non.*
130, 11).

**Iactura** ἀποβολή II 235, 55. ζημία II 322, 20; 536, 12. ἀποβολή,
βλάβη, ἐκβολή, ζημία II 75, 21. damnum IV 448, 45 (iunct. *codd. cf.
Verg. Aen.* II 646); V 301, 17; IV 245, 26. damnum uel mortalitas IV 88,
24; V 110, 19. detrimentum, damnum IV 525, 10. **iacturis** damnis,
detrimentis, dispendiis IV 350, 9. *V.* coniectura.

**Iacturale** ἐπιζήμιον II 75, 28.

**Iacturarius** qui frequenter patitur iacturam IV 88, 25. qui
frequenter patitur iacturam, id est damnum aut mortalitatem V 522, 46.
qui frequenter patitur damnum aut mortalitatem IV 245, 32. qui
frequenter iacturam patitur, id est damnum V 601, 62. qui saepe patitur
iacturam V 301, 12. *Cf. Loewe GL. N.* 166.

**Iactus** iactatus IV 245, 33. **iectus** percussus IV 350, 31 (*v.*
energumina). **iactus** iactatus uel casus (= iactus *subst.*) IV 525,
14; V 301, 7 (cassus). iacens, iactatus IV 350, 10. **iactum** missum IV
88, 27.

**Iactus** βολή II 258, 36; 488, 46; 538, 24; 550, 37. **iactum** βολή
II 75, 17. **iactus** ῥῖψις II 428, 25; 511, 50. ἐκβολή II 289, 4.
ἔκριψις II 292, 36. ἀκόντισις II 223, 16. boltio (bolus *Loewe: immo =
Bolzen*), sagitta, sciutil (*AS.*) II 582, 8. dispendium, detrimentum V
301, 14. **iactu** tesserarum casu V 536, 20 (*Ter. Ad.* 740). *V.*
iactus.

**Iacula missilia** πεμπόμενα βέλη III 450, 58. *Cf.* iacienda m.

**Iaculares** ἀκοντισταί II 75, 30 (iaculatores ?).

**Iaculatio** ἀκόντισις II 223, 16. βολή II 258, 36. ῥῖψις II 428, 25.

**Iaculator** ἀκοντιστής II 75, 40; 223, 17. **iaculatores** ἀκοντισταί
III 208, 39; 298, 23; 487, 9; 506, 17.

**Iaculatus** ἀκοντίσας II 75, 41 (*cf.* iactatus). **iaculata**
ἀκοντίσασα II 75, 32.

**Iaculor** ἀκοντίζω II 223, 18; III 128, 29. βάλλω II 255, 45. ῥίπτω
ἐπὶ ἀκοντίου II 428, 23. **iaculatur** iactat IV 88, 21. **iaculare**
ἀκόντισαι III 128, 31. **iaculabor** *v.* caculabor. **iaculabo**
sa­gittabo V 110, 22. **iaculatus est** iactauit IV 88, 39. *V.*
contorquet.

**Iaculum** ἀκόντιον II 75, 31; 223, 15; III 259, 53; 299, 12; 352, 64.
βολίς II 258, 39. missile, hasta uel gaesum IV 350, 11. sagitta V 552,
41. genus retis piscatorii qui (!) et funda appellatur uel gladium V
110, 16. genus retis piscatorii qui et funda appellatur V 209, 12 (*cf.
Serv. in Georg.* I 141; *Isid.* XIX 5, 2). ἀμφίβληστρον III 187, 31;
256, 43 (-us). sciutil (*AS.*) II 582, 7. **iaculum** ἀκόντια (!) III
487, 10. **iacula** ἀκόντια II 75, 33. βολίδες II 555, 39; III 367, 66;
490, 64. missilia tela, id est lancea uel sagitta IV 245, 28. arma V
301, 15. *Cf.* **incolum** arma [iactantia] V 460, 12; 503, 56.
**iaculi** missile uel lanceae et sagittae IV 88, 42. **iaculis** hastis
uel missibilibus IV 88, 29. *Cf.* **iaculus** iactus diminutiue II 582,
9. **iaculae** *v.* bina hastilia. *V.* in iaculis *et* euerruclum.

**Iaculus** ἀκοντίας ὁ ὄφις II 223, 13. ακοντίας III 19, 15; 91, 8; 190,
6; 305, 23; 376, 39; 433, 5 (ακοντις); 506, 21. βλητής (βληστής *Buech.
coll. Hesych. s.* βληστάς) II 258, 13 (*cf.* iaculum).

**Iam** λοιπόν, ἤδη II 75, 36 (im). ἤδη II 323, 26; III 4, 52. λοιπόν
ἐπίρρημα II 362, 38. transactum IV 110, 32.

**Iambos** *v.* sestertium.

**Iamdiu** πάλαι II 392, 29. ἤδη, ἐκ πολλοῦ II 323, 27. ἐκ πολλοῦ II
292, 23. olim V 643, 3 (*Non.* 27, 13).

**Iamdudum** ἤδη πάλαι II 75, 34; 323, 28. πάλαι ποτέ II 392, 39. πάλαι
καὶ ἐκ πολλοῦ III 242, 31; 244, 28. quam primum V 552, 39. paulo ante,
quam primo IV 88, 28. iam pridem IV 445, 18; V 110, 24. pridem, paulo
ante V 301, 13. iam ante IV 245, 27. iam pridem, antehoc IV 88, 46; V
110, 29. iam pridem, iam olim IV 525, 43. iam IV 525, 28. iam pridem,
antehoc, iam olim, iam paululum IV 88, 40 (*cf. Serv. in Aen.* I 580).
iam olim V 535, 3 (*Ter. Andr.* 228).

**Iam eram** ἤδη ἤμην III 450, 59.

**Iamiam** etiam IV 525, 27.

**Iam iamque** ἐντεῦθεν ἤδη λοιπόν II 300, 33.

**Iam inde** ἐντεῦθεν ἤδη II 300, 32. *V.* iam olim.

**Iam non licet** οὐκέτι ἔξεστιν II 389, 36.

**Iamnunc** ἐντεῦθεν ἤδη II 300, 32.

**Iam olim** πάλαι ποτέ II 392, 39. iam inde V 535, 64 (*scr*. **iam
inde** iam olim *coll. Ter. Ad.* 41). ante paululum IV 88, 45; V 110,
30.

**Iampridein** πάλαι II 75, 35. ἤδη πρότερον II 323, 29. ἤδη πάλαι II
323, 28. ἐκ πολλοῦ II 292, 23. iam olim IV
