145 29; 241, 26. **ruunt** procidunt aut cadunt IV 165, 26; 563, 47; V
143, 32. **ruat** inruat V 145, 10. **ruit** cecidit V 145, 37 (*Non.*
379, 33). **ruebant** inpellebant IV 462, 40 (*Verg. Aen.* I 35?).
*V.* sol ruit, aere ruebant.

**Rupea** saxosa *a post* IV 165, 40; IV 280, 47; 388, 4; V 545, 32.

**Rupe cauata** spelunca IV 165, 37; 462, 43 (*Verg. Aen.* I 310; III
229).

**Rupes** λίθος II 520, 17. κρημνός, κρημνοὶ καὶ ῥαγάδες II 176, 18.
κρημνός II 355, 44. ἀκροτομία (ἀκρότομα?) III 435, 5. **rups** κρημνός
**rupis** II 508, 30. **rupes** abscissa pars montis IV 422, 32.
**rupem** λίθον III 339, 1 (purum *cod.*). saxum fortem V 329, 54.
**rupes** montes, saxa ingentia IV 462, 44 (*Verg. Aen.* I 162?). saxa
ingentia, montes saxei IV 563, 34. saxa ingentia IV 280, 52; V 145, 31;
329, 32. scopuli uel montes IV 165, 36. **ru\<pi\>bus** montibus V 145,
35. *Cf.* **rupis** praecipitio IV 563, 35. **rure** rupes V 545, 33.
rupes uel petrae IV 388, 5. *V.* sub rupem, sub rupe caua.

**Rupinas** abrupta montium *a post* IV 165, 40; V 609, 52; 636, 48.
*Non recte temptat Schlutter Arch.* X 192. *Cf. Birt Mus. Rhen.* LII
*suppl. p.* 78.

**Rupia** ex utraque parte acuta V 329, 42 (= rumpia). **rupa** ex
utraque parte acuta IV 563, 33; V 609, 36. ex utraque parte cauata IV
165, 17; 280, 53 (rupra *codd.*); V 578, 4 (rubra). *Cf. Isid.* XVIII 6,
3. *V.* romphaea.

**Rupia[m]** ῥαπεύς II 176, 20 (rapium *Salmasius*).

**Rupiat** ῥάπτει ὑπητίῳ (υπητῑω *cod.* ὀπητίῳ *e*; ἠπητιᾷ *d*) II 176,
19.

**Ruplus** στροφεὺς γαλεάγρας καὶ ἀπόψηκτρον (αποψηκτιον *cod.*) II 176,
22. rutlus *pro* rutulus *Salmasius ad. Hist. Aug. p.* 437. *V.*
rotulus.

**Ruppo rusco** κοίαγρος, κώφαγρος II 176, 21 (*obscura omnia*). *V.*
rusco.

**Rupta** *v.* crupta.

**Ruptio** *v.* syrrexis.

**Rupto turbine** abrupta tempestate IV 165, 29. orta tempestate V 329,
49 (*Verg. Aen.* II 416).

**Ruptum** σπάσμα II 435, 26.

**Ruptum** φθαρέν II 176, 23.

**Ruptura** *v.* **intrinscus rupturas** ἐντεροκηλικός III 601, 45.

**Rura** *v.* ruma.

**Ruralem** terrenum IV 281, 4.

**Rurantur** (rurant ui *cod.*) rure agunt V 646, 47 (*Non.* 164, 17).
rurant uitam rure agunt *Landgraf Arch.* IX *p.* 421.

**Rurester** ἀγροικικός II 217, 29.

**Ruricola** ἀγροικικός II 217, 29. γεωργός II 263, 2.

**Rurigena** rure natus IV 165, 33. [a] rure natus V 241, 36. rure
natus, id est in uilla IV 388, 7; V 631, 21. ruri natus IV 280, 56. in
agro natus IV 563, 39. in uilla natus V 480, 50. in rure natus, id est
in uilla V 631, 21. ruri[s] habitans V 241, 37. **rurigenus** rure
natus IV 563, 40; V 329, 34. rure natus [uel pabula quae adponuntur] V
387, 52 (*cf.* 53).

**Ruris opaci** terrae umbrosae. Virgilius (*Georg.* I 156): et ruris
opaci falce premes umbras V 145, 27; 241, 38. *Cf.* **rus pacus**
(opacus *a b*) ager durus IV 280, 57 (*ubi* rus opacum ager umbrosus
*Landgraf Arch.* IX 419, *vix recte*).

**Rursus** πάλιν II 392, 52. αὖθις II 250, 51. ἀνάπαλιν II 176, 25.
rursum uel iterum IV 388, 6. rursum, iteratis (!), iterum V 145, 18.
iterum, denuo IV 280, 55. **rursum** iterum IV 165, 18. *V.* inibi
rursum, haud rursus.

**Rus** ἀγρός (singulariter tantum declinabitur **rus** et est neutrum,
et haec **rura** pluraliter) II 217, 25 (*GR. L.* I 32, 12). ἀγρός II
*praef. p.* XXXVII; 502, 15; 508, 31; 527, 35; 544, 37. ἄρουρα, ἀγρός II
506, 56. ἄρουρα, χωρίον, ἀγρός II 176, 26. χωρίον II 479, 48. ἄρουρα III
261, 2. uilla IV 388, 8. ager seu uilla *a post* IV 165, 40. **rus
ruris** V 387, 37. **ruris** uillae V 411, 50 (*can. concil. Neocaes.*
13). **rure** κατ' ἀγρόν, χωρία, χώρατα (τὰ *ad* χωρία?) II 176, 24
(rure *et* rura *confusa esse vult Vulc.*). **rura** ἄρουραι III 430,
3. numero plurali: singulari uero **rus** dicimus et est generis neutri
*Plac.* V 97, 14 = V 145, 40. agri uel uillae (inculti *add. cod. Sang.*)
IV 281, 3. agri inculti IV 281, 10 (*cf. Serv. in Georg.* II 412). agri
uel uilla IV 165, 20; 563, 36. agros IV 563, 37. **ruribus** terris IV
165, 19; V 329, 39. terris ac montibus (= rupibus) IV 563, 38.

**Rusceus** sordidus IV 388, 9; V 545, 34 (ruseus *ex Papia Hildebrand.*
rusticus *Schuchardt l. sub* ruscus *allato*).

**Ruscidum** *v.* ruscus.

**Ruscinius** mylacinos II 513, 15 (ricinus μυῖα κυνός *c*).

**Rusco** κοίαγρος, κώφαγρος II 176, 27 (*v.* ruppo rusco).

**Ruscus** mirran canihos II 513, 14 (*ubi* μυάκανθος *vel* μυρτάκανθα
*c*, μυρρινάκανθος *e*). **ruscum** χαμαιδάφνη, κυνόσβατον, εἶδος
βοτάνης, γονυπλήξ II 176, 28. **ruscus** βάτος II 256, 36; III 264, 54.
**ruscum** (riscum?) dermatinus (filax *e* 26 *addunt Buech. et H., v.*
filax) II 527, 24. **ruscus** spina longa [i]uncos habens IV 281, 7;
165, 27 (habentem); 563, 48
