IV 100, 4 (infitiae negationes? *cf. a et* infitiae). *V.* sine inuidia.

**Inuidiam mouit** V 662, 21.

**Inuidiosus** φθονερός II 471, 9; III 336, 7. βάσκανος II 256, 31.
ἐπίφθονος II 312, 44. cui inuidetur et qui (mihi *add. cod. Monast.: cf.
Gallée* 360) inuidet II 583, 7. qui inuidiam patit (patitur *d e*) IV
357, 26 (*cf. Isid.* X 134; *Diff.* 300 *etc.*). **inuidiosum** uitabile
V 642, 72 (*Non.* 126, 3).

**Inuidus** φθονερός II 79, 22; III 162, 5; 373, 43. ἐπίφθονος II 312,
44. odiosus aut ab inuidia zelatus IV 100, 2. qui alterius felicitate
(-tem *d e*) inimicatur IV 357, 27. **inuidi** φθονεροί II 79, 20.

**Inuigilantia** *v.* incuria.

**Inuilesco** ἐξευτελίζω II 303, 16.

**Inuilio** εὐτελίζω III 453, 7; 483, 14. *Cf. Arch.* VIII 378.

**Inuilitat** εὐτελίζει II 91, 51.

**Inuincibilis** *v.* ineluctabilis.

**Inuindicatus** *v.* inultus.

**Inuiolabilis** ἄχραντος II 254, 30. **inuiolabile** ἄθραυστον II 91,
49.

**Inuiolatus** ἀβίαστος, ἄφθαρτος II 78, 58. intactus uel incontaminatus
IV 90, 50. integer, intactus IV 252, 28; 531, 15. **inuiolatum**
ἀβίαστον II 215, 13. ἄχραντον II 254, 31. ἀρραγές, ἀδιάφθορον II 78, 59.
integrum, intactum, intemeratum, inlibatum IV 357, 28. inpraesumptum (?)
V 367, 5. **et inuiolatae** καὶ ἀχράντου III 423, 2.

**In uirile** κατὰ ἀναλογίαν II 91, 53.

**Inuisales** *v.* Iouis ales.

**Inuisibilis** ἀόρατος II 232, 19; III 423, 26. ἀθέατος II 219, 34.
**inuisibile** quod uideri non potest IV 357, 29; 531, 32. *V.*
inauspicabilis.

**Inuisice** *v.* infitiae.

**Inuisio** (?) uisio portenti II 583, 44.

**Inuisit** ingreditur IV 252, 22. **inuisunt** inspiciunt IV 252, 26.
**inuisere** requirere uel uidere IV 99, 34 (*Verg. Aen.* VIII 159).
requirere, uidere uel uisitare IV 530, 5. uisitare IV 252, 21. inuisare
V 302, 33 (inuisitare?).

**Inuisor** inuidens IV 414, 22. inuidens seu inuisus V 461, 56. inuidus
IV 252, 23.

**Inuisus** μεμισημένος II 78, 54; 367, 33. μισητός II 371, 62. ἀόρατος,
μισητός II 92, 37. στυγερός II 439, 23. odiosus aut inuidia zelatus
(*v.* inuidus) aut non uisus IV 100, 16. odibilis II 583, 6. odio
habitus IV 530, 38. laath (*vel* lath, *AS.*) V 367, 20. qui non uidetur
IV 252, 15. **inuisa** μεμισημένη II 78, 56. **inuisum** odiosum IV 448,
28 (*Verg. Aen.* I 28). numquam uisum IV 101, 48. luad (*scr.* laad,
*AS.*) V 422, 16 (*Euseb. eccl. hist.* IX 7). **inuisi** μεμισημένοι II
78, 55. inimici felicitatibus uel odiosi *Plac.* V 27, 8 = V 78, 32.
**inuisae** μεμισημέναι II 78, 57. *V.* inuidus.

**Inuisus caelestibus** odiosus diis IV 448, 29 (*Verg. Aen.* I 387).

**Inuitabilis** beneficus IV 94, 14. **inuitabile** προτρεπτικόν II 92,
38. *V.* iuuentabilis.

**Inuitatio** προτροπή II 79, 1.

**Inuitator** κλήτωρ II 350, 51.

**Inuitatus** κλῆσις II 350, 47.

**Inuitatus** ἄκλητος (ininu.? inuoc.?) II 222, 49. εἰσκεκλημένος II
287, 8.

**Inuite** ἀκουσίως II 223, 28.

**Inuitis di\<u\>is** nolentibus diis IV 448, 30 (*Verg. Aen.* II
402).

**Inuito** προτρέπω II 424, 19. **innitor** (-o e) προτρέπομαι II 424,
17. **inuitat** καλεῖ ἐπὶ δεῖπνον, προτρέπεται II 92, 39. προτρέπεται II
79, 21. uocat IV 99, 29; 527, 33 (*Verg. Aen.* V 486). **inuitet**
καλοίη II 92, 40. **inuitaui** ἐκάλεσα III 516, 10.

**Inuitus** ἄκων II 92, 41; 224, 32. ἀκούσιος II 223, 27. nolens IV 414,
21. **inuito** ἄκοντος II 92, 47. **inuitis** nolenti\<bu\>s IV 101, 12
(*corr. a*). **inuitius** ab inuito V 643, 34 (*Non.* 130, 24).

**Inuius** ἄοδος II 232, 13. **inuia** ἀνόδευτος II 228, 9. ἄβατος II
215, 8. **inuium** quod adiri non potest IV 91, 49; 100, 1; 531, 11; V
302, 5. sine uia, quod adiri non potest IV 252, 27. sine uia uel timore
(tramite *Hildebrand.* tenore *Volkm.*) IV 357, 30 (ἄβατον *add. a b*
c). **inuia** aspera, inaccessibilia IV 448, 26 (*cf. Serv. in Aen.* I
537). incerta uel aspera IV 100, 24. sine uia loca IV 528, 43.
difficilia, ubi non est uia IV 252, 14. *V.* in inuio.

**Inuocatio** ἐπίκλησις III 139, 19. ἐπίκλησις θεοῦ II 308, 43.

**Inuocatus** ἄκλητος II 222, 49. **inuocatum** ἀπροσφώνητον II 243, 21.

**Inuoco** ἐπικαλοῦμαι II 308, 27; III 139, 16. **inuocas** ἐπικαλεῖσαι
(!) III 139, 17. **inuocat** ἐπικαλεῖται III 139, 18. in se uocat IV 99,
33 (*Verg. Aen.* VII 140); 529, 14. **inuoca** ἐπικάλεσαι III 139, 20.
**inuocare** ἐπικαλέσασθαι III 139, 21. **inuocaui** ἐπεκαλεσάμην III
139, 22.

**Inuolator** κλέπτης II 350, 28.

**Inuolatus** *v.* abactus.

**Inuolo** κλέπτω II 350, 31; III 76, 47; 147, 45; 453, 9; 483, 15.
**inuolat** κλέπτει II 92, 44; III 475, 38. **inuolant** inuadunt,
arripiunt *Plac.* V 28, 37 = V 78, 33 (*cf. Plaut. Amph.* 245).
**inuolem** inuadam V 536, 45 (*Ter. Eun.* 648). **inuolare** in uola
(*vel* inuolat) id est in manu includit (-dere?) IV 100, 23. in uolam,
id est in manum, includere *Plac.*
