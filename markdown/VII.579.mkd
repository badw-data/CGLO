**μελοποιός** modulator. *Cf.* melopoeus (*expl.* carminis factor,
dulcem compositum), melops.

**μέλος** artus 2., carmen, copula, membrum, *cf.* melos 1. (*expl.*
dulcis sonus vel cantio carminis), - τὸ ᾆσμα modulus, - τὸ σῶμα artus
2., membrum, - τοῦ ἀνθρώπου membra humana, membrum, - ᾠδῆς modulus, κατὰ
μέλος admembratim, membratim, μέλη τὰ ὁλόκληρα artus 2.

**μέλω:** καὶ μελόντων et pertinentibus.

**μελῳδία** modulatio, *cf.* melodia (*expl.* modulatio, dulcedo vocis).

**μελωδός** modulator.

**\*μέλωψ (?)** *cf.* melops (*expl.* dulcis sonus, bonus cantor).

**μεμετρημένως** moderate.

**μεμήκυλα** *v.* μιμαίκυλον.

**μεμοιραμένως** affatim.

**μέμφομαι** accuso, incuso, queror.

**μεμψίμοιρος** querulus.

**μέμψις** incusatio, querela, querimonia (*v. etiam* caerimonia),
questio, questus, - ματαία querela, ἄνευ μέμψεως sine querela.

**μέν** quidem, μὲν οὖν immo, immo quidem (*cf.* ἐγώ, ἐάν, ἐπειδή,
ὁπότε, ὅπως), μὲν οὖν γε immo equidem, immo quin. *Cf.* εἴθε, καί, ὅτε,
ὅτι, σύ.

**μενίς** *v.* μαινίς.

**\*μενιστέριον** *vel* **μηνιστέριον** (= ministerium) abacus, delphica.

**μέντοι** certe, plane, profecto, - γε quŏque, sane (*cf.* εἰ), καὶ -
atque adeo, τοῦτο - hoc tamen, ὅπερ - quod cum, οὕτως - ita tamen.

**μένω** habito, maneo, mansito, sustineo.

**μερίδιον** particula.

**μερίζω** dido, impertio, partio (*et* -or), δίχα *et* διχῶς μερισθείς
bipertitus. *Cf.* κρέας.

**μερικός** particularis.

**μερικῶς** partim.

**μέριμνα** aegritudo, cura, sollicitudo, sonium, scrupulus, -αν ἔχω
sonio (-or).

**μεριμνητικός** sollicitus.

**\*μεριμνοποιῶ** sollicito.

**μεριμνῶ** ango (-or), cogito, curo, satago, sollicito, sonio (*et*
-or), -ῶν suspensus.

**μερίς** portio.

**μερισμός** divisio, partitio.

**μεριστής** partitor.

**μεριστός** immunis.

**μερίτης** particeps.

**μέρος** legatus (-um), pars, portio, ἐξ ἐναντίας - diversa pars, ἓν
ἥμισυ - sescuplum, στενὸν - οἴκου parvum membrum domus, τὸ ἐπιβάλλον -
competens, portio, μέρη λόγου partes orationis, ἀνὰ μέρος alternis,
vicissim, εἰς ποῖον μέρος quorsum, ἐκ μέρους ex parte, partim, portio
(-one), ἐκ παντὸς μέρους omnifariam, ἐν μέρει alterna, in parte,
invicem, partim, vicem, κατὰ μέρος particulatim, partim. *Cf.* ἐπιβάλλω,
μεταδίδωμι, μετοχή.

**μέροψ** *cf.* merops (*expl.* nomen avis, avis apibus inimica).

**μεσάζω** intercedo, intervenio.

**μεσαύλιον** atrium, compluvium, contila.

**μέσαυλον** atrium, compluvium.

**\*μεσεγγύη** sequestratio.

**μεσέγγυος** sequester.

**μεσεγγυῶ** sequestro.

**\*μεσέντερα** interanea, vitalis (-ia).

**μεσῆλιξ** mediae aetatis, medietas.

**μεσημβρία** antica, favonius, media dies, meridianus, meridies, - τὸ
κλίμα II, 368, 9 *s. interpr.*

**μεσημβρίζω** meridio.

**μεσημβρινός** meridialis, meridianus.

**μεσήμβριον** meridies.

**μεσημβριῶ** meridio.

**\*μεσήμερον** medio die.

**μεσιτεία** arbitratus, arbitrium, interventio, interventus,
sequestratio.

**μεσιτεύω** arbitror, intervenio, sequestro.

**μεσίτης** arbiter, arbitrator, mediator, mussitator, sequester,
tollens litem. *Cf.* ἀπόφασις.

**μεσόγειος** mediterraneus, meditullius, τὸ -ον mediterraneum mare,
meditullius (-um).

**\*μεσογεωτικοί** mediterraneus (-i).

**μεσοδάκτυλα** interdigita.

**μεσόδμη** medianus (-um), pergula.

**\*μεσόδοκον** iona (= zona), - *et* -ος trabes *sim.*

**\*μεσομήνιον** idus.

**μεσομήριον** interfemus, intertrigo.

**μεσονύκτιον** intempesta nox, media nox. *Cf.* nox (medium noctis).

**μεσόπλευρον** empyomatia, encatalempsis.

**μεσοπόλιος** II, 368, 19 *s. interpr.*

**\*μεσορανον** II, 368, 20 *s. interpr.*, *cf.* μεσούρανον.

**μέσος** medius, -ον ἡμέρας interdie, interdiu, medius dies, -ον τῶν
μυκτήρων cartilago, \--ον τοῦ ποταμοῦ alveus (*cf.* σκάφη), μέσην ἡμέραν
meridies (-e), θὲς εἰς τὸ μέσον pone in medium, ἐν μέσῳ in medio (*v.
p.* 437). *Cf.* δάκτυλος, χαρακτήρ.

**\*μεσοσέληνον** interlunium.

**μεσόστυλον** intercolumnium.

**μεσότης** medietas.

**μεσότοιχος** medius paries, -ον intergeries paries (intergerus).

**μεσουράνημα** medium caeli.
