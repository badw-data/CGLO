ονασμός II 409, 26. ἐπισώρευμα II 545, 27.

**Cumulatus** γεγεμισμένος II 261, 57. κορυστὸς μόδιος II 353, 52.
largus, beneficus IV 321, 13. **cumulata** superflua siue plena IV 47,
27. **cumulatum** γέμον II 262, 10. μεστόν II 368, 30. μεστόν,
ὑπέρμετρον II 119, 52. **cumul\<at\>um** πεστον (*ubi* πιεστόν *Vulc.*,
μεστόν *alii*) II 118, 60. **cumulatum** auctum et superpositum IV 321,
12. **cumulatius** ὑπέρμεστον III 3, 5. amplius uel uberius IV 47, 11.
uberius, amplius IV 220, 38.

**Cumulo** ἐπισωρεύω II 311, 34. **cumulat** σωρεύει II 119, 1. onerat
IV 47, 34 (*Verg. Aen.* V 532). auget IV 321, 11. **cumulasti**
ἐσώρευσας II 118, 57.

**Cumulus** σωρός II 450, 44; III 444, 56. βουνός II 118, 58. collis,
βουνός, φάραγξ II 119, 43. χῶμα III 261, 25. plenitudo IV 321, 14.
aceruus IV 326, 39. plenitudo aut aceruus IV 46, 22; 499, 41. magnitudo
IV 501, 29. magnitudo uel aceruus V 282, 49. plenitudo aut aceruus uel
collectio IV 47, 6. aquarum multitudo IV 501, 28; V 282, 48 (*v.* in
fluctu pendent). **cumulo** magnitudine abundante, incremento (*Verg.*
*Aen.* I 105; II 498) IV 436, 28. **cumulum** ἐπίμετρον III 429, 62.
augmentum V 551, 12.

**Cum utique** καὶ μάλισθ' ὅτε II 118, 59.

**Cum uisum est** pro cum uisum esset *Plac.* V 12, 23 = 60, 9.

**Cunabulum** σπάργανα (singulare non habet: *cf. GR. L.* I 33, 28; IV
196, 5) II 435, 20. βαβάλιον II 255, 9. λικνον ὃ καλοῦσι βαβαλιστήριον
II 361, 4. uas in quo iacent infantes V 419, 1 = 427, 38 (*cf. Euseb.
eccl. hist.* XI 16, *ubi* incunabulis). **cunabula** σπάργανα II 119, 2;
III 444, 57. initia infantum IV 44, 26. initia uel rudimenta IV 46, 41;
501, 53. initia uel quibus infantes inuoluunt IV 436, 29 (*Verg. Aen.*
III 105). nutrimenta uel cuna\<e\> infantium V 283, 9 (*Serv. in Aen.*
III 105). infantiae cunae, sunt (siue?) panni in quibus infantes
obuoluuntur *Plac.* V 60, 10 (*Verg. Ecl.* IV 23). **cunabulis**
initiis (insignis *cod.*) infantiae IV 326, 40. initiis uel ab infantiis
IV 224, 26. *Cf.* **cuniculum** licentia fandi V 449, 9 (*h. e.*
**cunabulum** initia infantiae: *cf. Isid.* XX 11, 6). *V.* cunae,
cyciminium.

**Cunae** (singularia non habet) βαβαλιστήρια II 255, 10 (*cf. GR. L.*
I 549, 3). βαβάλιον II 545, 19. **cuna** et **cunabu­lum** βαβαλιστήριον
II 119, 3. **cuna** βαβαλιστήριον II 522, 15. **cunae** stratum infantum
IV 326, 41.

**Cunctabunde** haesitanter IV 409, 9.

**Cunctabundus** dubius, suspensus V 283, 12. diffldus (dubius?
diffidens?), haesitator V 448, 44. *V.* confida.

**Cunctanter** difficulter IV 224, 41.

**Cunctanti** repugnanti IV 436, 31. **cunctantem** interrogantem IV
501, 36. cogitantem uel dubitantem IV 47, 25. dum ornatur tardum
(tardam?) uel haesitantem IV 436, 30 (*Verg. Aen.* IV 133).

**Cunctatio** ὑπέρθεσις II 464, 25. μέλλησις, ἀναβολή II 119, 5.
μελλησμός II 119, 14. μεταμελλησμός II 119, 51. μέλλησις II 367, 14.
διαπορία II 273, 40. διαβασταγμός II 270, 11. dubitatio IV 224, 32. *V.*
sine cunctatione.

**Cunctator** διαπορητής II 273, 41. ὀψιμαθής II 391, 26. dubius *b
post* II 576, 6.

**Cunctatus** trepidatus (trepidus *a*) IV 46, 25; 501, 39.

**Cunctor** ὑπερτίθημι II 464, 51. dubito V 551, 13 (*Non.* 252, 23).
**cuncta­tur** dubitat uel remoratur IV 46, 24. dubitat IV 501, 37.
dubitat, haesitat, trepidat, tardat, moratur IV 326, 42. **cunctare**
causare V 187, 16.

**Cunctus** σύμπας II 442, 36. omnis uel uniuersus IV 46, 26; 501, 34.
**cuncti** omnes IV 409, 10. **et cuncti** καὶ πάντες III 424, 58.
**cuncta** πάντα II 119, 4. adunata IV 47, 32 (cumta). omnia, uniuersa
IV 47, 31. **cunctis** omnibus, uniuersis IV 46, 50; 501, 35.

**Cundi** *v.* condy.

**Cunebula** κόνυζα βοτάνη II 119, 12 (cunela *c*, cunicula *Salmas.*).
*Cf. not. Tir.*

**Cuneis stipatus** numerus (numeris *c*) circumdatus IV 501, 15.

**Cuneo** σφηνῶ III 79, 45; 158, 24.

**Cuneum** *et* **cuneus** σφήν II 449, 31. πολεμική τάξις (*vel* τ.
πολ.) II 411, 54; 451, 36. cuneus σφήν III 190, 58; 268, 69; 355, 12;
434, 32. cuneum σφήν II 500, 5. **cuneus** κέρας το ἐν παρατάξει πολέμου
II 347, 66. κερκὶς θεάτρου II 348, 19. θεάτρου κερκὶς ἢ σφὴν ἢ στρατιωτῶν
λόχος II 119, 8. **cuneum** κούνιον τὸ σύστημα; 'densi cuneis se quisque
coactis' (*Verg. Aen.* XII 457) II 354, 20. **cuneus** (cuneum
*Loewe*) uecg (*AS.*) II 575, 55 (= *gloss. Werth. cf. suppl.*).
densus populus IV 46, 42. turba minor IV 47, 10; 501, 16 (minoris).
densus populus, turma (*vel* turba) hominum IV 224, 31. densus populus
uel turba hominum [uel crypta, cumba] IV 326, 43 (*v.* gumba). turba
minoi[is] uel densus populus V 282, 42. densus populus, multas turbas
(!) IV 501, 14. *V.* concius.

**Cuneus theatri** φυλὴ (?) θεάτρου II 119, 11.
