**πρόσχωσις** exaggeratio.

**προσψαύω** attingo.

**προσῳδία** accentus, apex, sonus. *Cf.* adventus.

**προσωθῶ** adigo.

**προσωνυμία** cognomentum.

**προσωπεῖον** larva, oscillum (-a), persona.

**προσωπίδιον** faciale.

**προσωπίς** personacia, - ἄρκτειος prosepis.

**προσωπολη(μ)ψία** personarum acceptio (*cf.* prosopolepsia).

**πρόσωπον** facies, os, persona, vultus, ποίῳ -ῳ frons 2. (qua fronte),
qua persona. *Cf.* conspicillo, ἀγωνίζομαι, φακός.

**προσωποποιία** materiola.

**προσωποποιός** personarius.

**προτακτικός** primitivus, -όν praepositivum.

**προτάσσω** *et* -τάττω antelato, praeficio, - σε τοῦ πραιτωρίου
praeficio te praetorio; προταγείς praelatus; προτεταγμένος praefectus, -
τῆς κοόρτης praefectus cohortis, - τοῦ πραιτωρίου praefectus autem
praetorio, - Ῥώμης τῆς πόλεως praefectus urbis, - τῶν τεχνιτῶν
praefectus fabris.

**προτείνω** porrigo2., praetendo, propono, protendo.

**προτειχίζω** praemunio.

**προτεμένισμα** vestibulum.

**προτέρημα** privilegium.

**πρότερος** prior, superior, -ον ante, antea, prior (-us), ἤδη -ον iam
pridem, μικρῷ - paulo ante. *Cf.* ὕστερος.

**προτίθημι** antelato, antepono, exprimo, praepono, propono, -εμαι
destino. *Cf.* πρόκειμαι.

**προτίκτω** progigno.

**προτίμησις** praerogativa.

**προτιμητέος** praeferendus.

**προτιμῶ** antefero, magnifico, praefero, praemunio, praepono, -ῶμαι
praefero.

**προτομή** effigies, vultus, - εἰκών βασιλέως effigies, ἐν -ῇ πολέμου
στῆναι μετὰ ἐσθῆτος ὑπατικῆς in procinctu.

**\*προτομοφόρος** imaginifer.

**πρότονος** capreolus, rudens, -ον rudens.

**\*προτρεπτής** hortator.

**προτρεπτικός** hortatorius, invitabilis.

**προτρέπω** exhortor, horto, invito, -ομαι, adhortor, exhortor, horto
(-or), instimulo.

**προτρέχω** antecurro, occurro, procurro.

**προτροπή** hortatio, hortatus, invitatio.

**προτρυγητής** autumni sidus.

**προύνεικος** *et* -νικος laciniosus, lascivus.

**προῦπτος** perspicuus, promptus.

**\*προυστελλιον** armo.

**προύχω** *v.* προέχω.

**προφανής** perspicuus, propatulus, ἐκ τοῦ -οῦς in propatulo.

**προφανῶς** debusi deque, patule.

**προφασίζομαι** causor, praetendo.

**πρόφασις** causa, excusatio, materia, occasio, praetextum.

**προφερέστερος** praestans (-antior).

**προφέρω** depromo, exprimo, expromo, infero, praefero, profero, promo,
προοίσων proditurus, προενεχθείς prolatus. *Cf.* προάγω.

**προφήτης** antistes, hariolus, propheta, vates.

**προφθάνω** antecedo, antecello, antecipio, antevenit (-ει),
praeoccupo, praevenio, praevertor, procello.

**προφορά** pronuntiatio.

**προφορικός** eloquens, facundus.

**πρόφορον** patribulum.

**\*προφυὰς** σταφυλῆς racemus.

**προφυλάσσομαι** praecaveo.

**πρόχειλος** labrosus.

**\*προχειράριος** amanuensis.

**προχειρίζω** depromo, expromo, promulgo, προχειρισθείς proditus.

**πρόχειρος** proiectus 2., promptus, εἰς -ον in promptu (-um), ἐν -ῳ ad
manum, in promptu, promptu. *Cf.* prochirum (*expl.* sporta manuensis).

**\*προχειροφόρος** amanuensis.

**προχέω** profundo.

**προχθές** antepridem.

**\*προχία (?):** ἀπὸ -ας ex praecelato.

**προχρεία** antecessus (-um), promotum (= promutuum?), προχρείᾳ *et*
εἰς -αν in antecessum.

**\*προχυτήριον** fusorium.

**προχώρησις** processio, κατὰ -ιν pedetemptim.

**προχωρῶ** procedo, proficio.

**πρόψαλμα** II, 424, 47 *s. interpr.*

**\*πρόψυξις (?)** corpus infrigdatus.

**προώλης** petulans.

**\*πρόωνύμιον** praenomen.

**πρόωρος** praecox.

**πρύμνα** et -η (πλοίου) puppis.

**πρυμνήσιον** retinaculum.

**πρυτανεῖον** II, 424, 51 *s. interpr.*

**πρύτανις** flamen, Salius. *Cf.* ποτήριον.

**\*πρωή**, ἡ (*Eins.*) mane.

**πρώην** ante, nuper, olim.

**πρωθύστερον** praeposterus (-um).

**πρωΐ** *et* **προΐ** mane, matutino, matutinum, ἀπὸ - sub mane.

**πρώιμος** praecox, tempestivus.

**πρωινός** matutinus.

**πρωκτός** culus.

**πρῷρα** prora, *cf.* ancora. *V.* ἄκρος.

**πρωρεύς** proreta.

**πρωτεῖον** *et* **πρωτεῖα** primatus, primitia (-um).
