*Cf. Pseudapul.* 6; *Diosc.* I 27; V 66; *Arch.* X 266.

**Inultus** ἀτιμώρητος II 250, 10. ἀνεκδίκητος II 225, 36. non
uindicatus IV 414, 19. **inultum** inuindicatum IV 252, 11. inpunitum V
535, 39 (*Ter. Andr.* 610). **inulti** non uindicati IV 448, 31
(*Verg. Aen.* II 670); 100, 3; 530, 8.

**In ulua** in herba palustri V 504, 53. in palustri herba V 629, 40.
*Cf. Verg. Aen.* II 135; VI 416.

**In umbilico** in sinu, in medietate V 504, 54.

**Inumbraculum** *v.* lucus.

**Inumbratus** tenebratus V 635, 34.

**Inumbro** ἐπισκιάζω II 310, 58. **inumbratur** occultatur uel
obscuratur IV 99, 32. occultatur IV 252, 17; 530, 4.

**Inuncare** unco \<inuadere et arripere\> V 642, 60 (*Non.* 124, 17).

**Inuncauit manu** adprehendit, uim manu intulit IV 99, 38 (abstulit
*codd. praeter c*); V 213, 21. adprehendit, per uim manus intulit V 213,
20. uim manu intulit V 571, 19.

**Inunctio** ἔγχρισις II 284, 27; III 363, 74.

**Inundata terra** βεβρεγμένη γῆ ἤτοι ποτισθεῖσα II 257, 3. *Cf.*
**inundata** irrigata terra IV 357, 33.

**Inundatio** κατακλυσμός II 92, 46; 341, 17. ἐπίκλυσις II 308, 47.
ἐξυδάτωσις III 426, 63. πλήμμυρα II 409, 45. diluuium IV 357, 32.

**Inundatio fluminis** ἐπιρροή III 453, 8; 483, 1.

**Inundo** ἐπικλύζω II 308, 48. κατακλύζω II 341, 18. **inundat**
ἐπικλύζει II 92, 43. plenum est IV 99, 31; 530, 3. *V.* undo.

**Inungo** ἐγχρίω II 301, 6. ἐναλείφω II 297, 20. **inungere** ἐγχρῖσαι
III 206, 33.

**In unum** εἰς τὸ αὐτό II 287, 40. ἐπὶ τὸ αὐτό II 312, 13. pro simul V
571, 21.

**In urbana militia** *v.* militia.

**In urceolum** εἰς ὀρνόλην III 380, 14.

**Inuro** ἐγκαίω II 283, 39. **inuret** incendit IV 252, 24. **inurere**
infigere (infligere *R*) notas uel insignia (signa *G*) uel maculas, ut
solent equos uarios (*vel* uariis) signis ferro candenti designare uel
alia animalia *Plac.* V 27, 9 = V 78, 35. inprimere IV 100, 14.
**inus\<s\>it** inflammauit V 304, 33. **inusta est** ἐγκέκαυται II 283,
52.

**Inusitate** ἄνευ χρήσεως II 226, 18.

**Inusitatus** ἀκατάχρηστος II 222, 32.

**Inustus** ἐγκεκαυμένος II 283, 50. ἄκαυστος II 222, 36. *Cf. Arch.*
VII 535.

**In \<u\>sum \<h\>abeo** εἰς χρῆσιν ἔχω II 92, 48.

**Inutile ferrum** (-ro *codd.*) quo uti non posse (potest?) uel ipse
\<in\>utilis IV 448, 32 (*Verg. Aen.* II 510: *cf. Serv.*).

**Inutilis** ἄχρηστος III 178, 39; 373, 42; 470, 16; 487, 17. ἀχρήσιμος
II 254, 36. ἀσύμφορος II 249, 1. ἀνωφελής II 231, 37. incommodus IV 357,
34. **inutiles** inbecillae V 535, 15 (*Ter. Andr.* 287).

**Inutiliter** ἀνωφελῶς II 231, 39. ἀσυμφόρως II 249, 2.

**In utrumque** in alterutrum IV 101, 46 (*Verg. Aen.* II 61).

**Inuus** (Siluanus) Πὰν ὁ δαίμων II 393, 14 (*ubi* inus *cod.*; incubus
*e*). \*\*\*\*\* (*v.* incola *qua cum glossa haec est confusa*) Pan
uero est quem pagani deum dicunt uel Incubum (Incibum *R*) appellant,
caprinis pedibus, barbatum, rubicunda facie, in dextra fistulam, in
laeua uirgam tenentem. quem nolunt rerum et totius naturae deum, unde
pagani (Pan *G.* Pana *Maius*) quasi omnia appellant *Plac.* V 27, 11 =
V 90, 22. *Cf. Isid.* VIII 11, 81; *Serv. in Ecl.* II 31; *Aen.* VI
775 (Inuus Pan uero est *Deuerling*). *V.* Ingenis.

**In uacuum** εἰς μάτην II 287, 15.

**In uado** in securo IV 100, 19. facili V 535, 54 (*Ter. Andr.* 845).
*Cf. Schlee Schol. Ter. p.* 45.

**Inuado** ἐφορμῶ II 321, 18. εἰσπηδῶ II 287, 23. ὑπεισέρχομαι βιαίως II
463, 46. **inuadit** ἐφορμᾷ II 79, 10. sermonibus adgreditur IV 414, 20
(*Verg. Aen.* IV 265). superuenit, ingruit IV 357, 18. **inuaserat**
κατειλήφει, ἔφοδον πεποιήκει, εἰσεπήδησεν II 91, 41. **inuallisse**
(*corr. a e*) κατειληφέναι, ἔφοδον πεποιηκέναι II 91, 2.

**Inualetudinariorum** (*vel* -lit-) languentium *Plac.* V 28, 13 = V
78, 30 (langentium).

**Inualidus** ἀδύνατος II 219, 2. ἀνίσχυρος II 228, 3. ἀσθενής II 247,
30. ἄτονος III 329, 69; 507, 16. infirmus IV 252, 18. uitiosus IV 100,
46 (*Verg. Aen.* VI 114). **inualidum** minus fortem IV 525, 47.
**inualida** infirma, inrobusta IV 357, 19.

**In uanum** *v.* frustra.

**Inuasio** ἐφόρμησις II 321, 17. ἔφοδος II 321, 12. *Cf.* **inua**
inuasio IV 100, 15.

**Inuasor** ἐφοδιαστής II 321, 13.

**Inuasum** peruasum IV 99, 35.

**Inuectio** ἐπόχησις II 313, 29. ἐπίπληξις II 91, 42.

**Inuectiuus** καταφορικός II 344, 62. **inuectiua** καταφορά II 344,
61.

**Inuecto** ἐποχοῦμαι II 313, 31.

**Inuectus** inportatus IV 252, 25; 448, 21 (*Verg. Aen.* VI 587).
ingressus IV 531, 8; V 302, 54. *Cf.* **inuectus** ambulat (*contam.*?)
V 303, 15.
