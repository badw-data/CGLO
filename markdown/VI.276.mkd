**Copia** εὐπορία II 319, 3. ἀφθονία II 253, 10. εὐθηνία II 317, 20; III
261, 26. παρρησία (*v.* coram) II 116, 26. περιουσία II 403, 43. πλῆθος
II 522, 9. abundantia uel facultas IV 40, 44; 325, 22; 497, 24.
exuberantia, utilitas IV 325, 23. **copia** aliarum rerum et **copias**
exercitus V 551, 10 (*cf. Serv. in Aen.* II 564). **copiam** facultatem,
oportunitatem IV 40, 43; 497, 23. facultatem V 531, 49 (*Ter. Andr.*
320). **copiae** (singularia non habet) ἐπιτήδεια, τὰ ἀναγκαῖα II 311,
54. πλήθη, πολυπληθία II 116, 24. pluraliter πλῆθος II 545, 26.
**copias** [h]abundantes diuitias IV 497, 25; *cd post* IV 40, 44.
*Cf.* παρατοῦρα **\<cop\>ia** II 397, 2 (*suppl. H.*).

**Copiam sui non praebet** V 660, 37; 661, 26.

**Copiaria** (cociatri *cod. in serie* cop. *corr. c*) μεταβλητική II
116, 30 (cociatrina *Scal. ad Festi* coctiones, *non male.* cociatrix
*Nettleship 'Contr.' p.* 423. cocionatrix *Vulc.*).

**Copidermus** κοπίδερμος II 353, 20. *V.* flagello, casabus.

**Copio\<r\>** (opio *cod.*) εὐπορῶ II 319, 5. **copi\<a\>tur** copiis
utitur V 639, 21 (*= Non.* 87, 3).

**Copiosus** εὔπορος II 319, 4; III 331, 37; 444, 12; 519, 25. εὐγενής
III 331, 36; 493, 50; 519, 24 (generosus *Boucherie*). diues II 576, 2
(cup.). locuples IV 325, 24. **copiosa** ingens IV 435, 46 (*Verg. Aen.*
I 99: *v.* ingens).

**Copis** copiosus, diues V 448, 58. **cope** copiosa V 448, 59.
**cops** est copia V 617, 5. copia V 185, 22. **copsus** copiosus V 185,
23; 617, 6. *Cf. GR. L.* II 321, 25.

**Copo, Copon-** *v.* caupo(n)-

**Copsa** nomen loci V 551, 11 (*scr.* Compsa *vel* Cossa).

**Copsus** *v.* copis.

**Copta** κοπτή III 15, 40; 88, 3; 372, 27.

**Copula** συναφή II 444, 30. χειρόδεσμος II 476, 31. χειροάλυσις II
476, 29. χειραλυσίδιον, μέλος, ἄρθρον χειρός II 116, 25. **copla** ζυγός
III 92, 37. coniunctio IV 38, 4; 219, 33; 325, 26; 496, 41. *V.*
capulus.

**Copulatim** εἱρμῷ II 116, 29. contexte, conflatim IV 325, 27. *V.*
conflatim, contexte.

**Copulatio** δεσμός III 444, 13. δέσις II 268, 45. συναφία II 444, 32.
*V.* coniunctio.

**Copulatiuus** συμπλεκτικός II 442, 55. συναπτικός II 444, 14.
συνζευκτικός II 445, 61.

**Copulatus** συναφή II 444, 30.

**Copulatus** coniunctus IV 35, 46; 496, 40. in uinculis missus V 278,
48. sociatus, coniunctus, **copulatus**, κοινῶν II 185, 27.
**copulatum** συνημμένον II 116, 28.

**Copulo** ζευγνύω II 322, 1. συνάπτω II 444, 17. coniungo IV 219, 32;
325, 29. **copulat** συνζευγνύει, συνάπτει γάμον, σύναψον, σύνπλεξον II
116, 21 (*contam.* copulat *et* copula). **copulare** συνάπτειν II 116,
27. **copuletur** iungatur IV 44, 38.

**Coquester:** *cf. Loewe Prodr.* 291, *ubi* fulinarius: coquus,
coquester *ex Osb. p.* 241 *affertur: adde Hamann* '*Weitere Mittheil.
aus dem Brevil. Benthem.' p.* 14, *Foerster 'Bull. de la Soc. Ramond'*
1898. *V.* coquestrius, quoquestria *Osb. p.* 147.

**Coquina** (*vel ut codex habet* cocina) μαγειρεῖον II 496, 52.
**cucina** μαγειρεῖον II 363, 51. **coquina** μαγειρεῖον III 191, 18;
269, 11. *Cf. Roensch Coll. phil. p.* 199, 236. *V.* carnificina.

**Coquinarius** *v.* coctarius.

**Coquinator** coquus II 574, 11.

**Coquinatum** a coquendo V 639, 11 (*Non.* 85, 28).

**Coquinaturium** μαγειρεῖον III 91, 63. **cocinatorium** III 20, 12;
313, 41; 530, 28.

**Coquo** ἕψω II 321, 42. **coco** ἕψω III 140, 68. **cocet** ἕψει III
140, 70. **cocimus** ἕψομεν III 141, 3. **coquant** ἑψήσωσιν III 114, 72
= 643, 25. **coce** ἕψησον III 141, 1. **cocite** ἑψήσατε III 141, 2.
**coces** ἑψήσεις III 140, 69. **coximus** ἡψήσαμεν III 143, 33.
**coxistis** ἡψήσατε III 143, 37 (hipsisen *codd.*). **coxerunt** ἥψησαν
III 143, 34. **coquantur** ἑψέσθωσαν III 516, 17. **coctum est** ἡψήθη
III 143, 35.

**Coquus** *v.* cocus, lanii coqui.

**Cor** καρδία II 116, 36; 338, 54; 506, 13; 507, 11; 529, 52; 547, 1;
III 13, 5, 86, 15; 176, 48; 248, 39; 311, 36; 349, 58; 394, 62; 403, 20;
444, 15; 526, 6. καρδίαν III 130, 52. **cor hominis** καρ­δίαν III 598,
25. **corde** ex animo *Plac.* V 58, 37. **corda** animos (*vel* -us) IV
40, 42; 224, 11. *V.* sine cor.

**Corae** caluaria V 353, 29 (*vox hebr.: cf. Roensch Mus. Rhen.* XXXI
*p.* 458; *Onom. sacr.* 4, 7).

**Coram** ἐνώπιον II 116, 31; 301, 8. παρρησία, φανερῶς II 562, 45.
palam, praesenti, prae conspectu IV 435, 48. palam, praesens, euidenter,
manifeste IV 498, 29. praesens, palam, ante eum IV 44, 13. praesentibus,
palam IV 325, 30. praesentibus IV 224, 13. *Cf. Serv. Dan. in Aen.* I
595.

**Corax** coruus IV 38, 28; 224, 18. hraebn (*AS.*) V 354, 11. coruus et
mons altis simus inter Calli[o]polim et Nau­-
