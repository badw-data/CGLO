**νεώλκια** navilium (-a).

**νεωλκῶ** πλοῖον navem subduco (νεολκῶ).

**νεώνητος** novicius.

**νεώριον** navale, navalium, navilium.

**νεωστί** novissime, noviter, nuper, recens 2.

**νεωτερίζω** iuvenor, - ίσωσιν iuventent.

**νεωτερικός** iuvenalis, *cf.* neotericus (*expl.* novicius aut neutri
partis, minor), -όν iuvenile.

**νεωτεριστής** factiosus.

**νεωφύλαξ** *v.* ναοφύλαξ.

**νὴ** τόν, ἐπὶ ὅρκου per.

**νηδύς** alvus, uterus, - τοῦ ποταμοῦ alveus, alvus.

**νήθω** neo, νενησμένον netum 1. *Cf.* νήστης.

**νηκτός:** -ῶν natabilibus.

**νῆμα** filum, netum 2., nitum.

**νηνεμία** placidum.

**νήνεμος** serenus.

**νήπιος** infans, parvulus, -ον infans, -ώτατος hirquitallus, parvulus.
*Cf.* κρέας, παιδίον, προσπαίζω.

**νηπιότης** infantia.

**Νηρεΐς** Nereis, Salacia.

**νήριον** rhododendron.

**νηρός:** νηράν frigidus (-am). *Cf.* aqua.

**νησιώτης** insulanus.

**νῆσος** insula, ebulus (-um = Ebusus), - Διονύσου Dia.

**νῆσσα** anas.

**νηστεία** ieiunium, ἡ μετὰ -αν ἡμέρα prandicularius.

**νηστεύω** ieiuno.

**νήστης** ὁ τὸν στήμονα νήθων staminarius.

**νῆστις** ieiunus.

**νήτη** *cf.* nete (*expl.* chorda ultima).

**νητός:** -ή *cf.* neta (*expl.* offa).

**νηφαλέος** ὁ ἀπεχόμενος οἴνου abstemius, sobrius.

**νηφαλεότης** sobrietas.

**νήφω** sobrio, sobrius sum, -ων sobrius, vigilans.

**νήχομαι** nato (*et* naro), no.

**νίκη** ador, anticipatio, antigerio, palma, victoria, -αι laureae.
*Cf.* στοά, φύλλον.

**νικητέας** vincendas.

**νικητής** victor.

**νικηφόρος** victor, victoriosus.

**νικόλαος** *cf.* nicolaus (*expl.* dactylus).

**νικῶ** convinco, evinco, supero, vinco, -ῶν victor.

**νιννίον** pupus.

**νιπτήρ** lavatorium, pelvis.

**νίπτω** lavo, -ομαι lavo, νίψον χεῖρας lava manus, νίψασθε aquam
manibus. *Cf.* nipse podas.

**Νῖσος** *cf.* Nisus 3. (νυσος *cod.*).

**νίτρον** (*cf.* λίτρον) nitrum, vermicarium. *Cf.* ἀφρός.

**νίτρωμα** κεφαλῆς squama.

**νιφάς** nympha.

**νιφετός** nimbus, nix, pruina.

**νίφω:** -ει ninguit, nivet.

**νοερός** intellegens.

**νόημα** clausula, intellectus, sensus. *Cf* noema (*expl.* voluntas).

**νοήμων** intellegens.

**νόησις** intellectio, intellectus.

**νοητικός:** ψυχὴ -ή anima sensitiva.

**νοθεύω** sollicito.

**\*νοθισμοί** illecebra (-ae).

**νόθος** furtivus, naturalis, spurius, *cf.* nothus, - ἀμφημερινός,
τριταῖος, τεταρταῖος nothus amphemerinus, trit, tet.

**νομεύς** opilio, pastor.

**νομή** pabulum, ususfructus, - *et* - ἡ βοσκή pascua, pastio,
pastus, - *et* - ἤτοι κατοχή possessio, - θρεμμάτων pabulum, - σωματικὴ
ἡ παράδοσις incorporatio, - χρονία usurpatio.

**νομίζω** credo, duco, existimo, opinor, puto, video (-or), νεμομισμένα
concepta, ὡς νόμισον ut puta. *Cf.* arbitror.

**νομικός** iuris consultus, iuris prudens, iuris studiosus, tabellio, -
*et* - ὁ νόμους εἰδώς iuris peritus, legis peritus.

**νόμιμος** iuridica sententia, iustus, legitimus, -ον *et* -ον
πολιτικόν ius civile, οὐ -ον incivile. *V.* ἀγωγή, ἐλευθερία.

**νομίμως** legitime.

**νόμιος:** Ἀπόλλων - Vidius, Νομία Pales.

**νόμισμα** moneta, pecunia, solidus. *Cf.* nomisma (*expl.* nummi
percussura, moneta vel forma *sim.*), ἀργυροῦν - argentum signatum,
χρυσοῦν - aurum signatum. *Cf.* ἥμισυ, τόπος *s. f.*

**νομοδιδάσκαλος** legis doctor, legum doctor.

**\*νομοδίφας** legicrepa.

**νομοθεσία** censio, constitutio, iurisdictio, legislatio, promulgatio,
sanctio, satisdictio.

**νομοθέτης** legislator, legum pater, promulgator, -αι iura dictantes.

**νομοθετῶ** constituo, legem facio, leges pono, promulgo, sancio legem,
-ηθείς promulgatus.

**νομομαθής** iuris peritus.

**νόμος** ius 1. *et* 2., lex (*cf.* nomus), - γεωργικός agraria lex, -
πολλὰ περιέχων lex per saturam, satura, -οι ἄγραφοι *cf.* Iupiter, -ῳ
Ἑλλήνων lege peregrina, παρὰ -ους contra leges (*cf.* contra iuris), -ου
χάριν dicis causa, dicis gratia, legis causa. *Cf.* αὐθέντης, εἰσφορά,
ἔμπειρος, κατοχή, κυρῶ, ὑποπίπτω.

**νομός** pagus, quadra (codra).

**νόνναι** nonae.

**νοσερος** aeger, languidus, morbosus.
