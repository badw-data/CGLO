perpetuus V 630, 35. perpetuus (*uel* perpetuo) IV 271, 19. **perpetem**
perpetuum IV 140, 34; 552, 47; V 233, 1. prolixum, continuum IV 271, 45.
perpetuus (?) IV 138, 44. *Cf. Loewe GL. N.* 152. *V.* perpetuus.

**Perpessicius** ὑπομενητικός II 467, 2. qui frequenter aliquid patitur
IV 271, 18. **persicus** qui frequenter aliquid patitur IV 141, 40; 550,
24; V 233, 2; 320, 11; 385, 23 (patitur aliquid). qui frequenter aliud
pandet (!) V 542, 52. **perpessicius** qui semper \[per\]passus est V
232, 42 (*corr. cod. Palat.*). qui ferre potest IV 376, 26.

**Perpesso** ὑπομένω II 467, 3.

**Perpessus** ὑπενεγκώv II 463, 50. παθών II 341, 53. passus IV 376, 20;
550, 23. passus aut sustinuit IV 143, 1; *a post* IV 151, 10.

**Perpetes** *v.* praepes.

**Perpetiendum** tolerandum IV 141, 22.

**Perpetim** (*uel* -tem) perpetualiter V 622, 15. *Cf. Festus p.* 217,
10.

**Perpetior** sustineo V 556, 16. **perpetitur** παρέχει χεῖρα, ἀνέχεται
II 562, 37. ὑπομένει, καρτερεῖ II 147, 53. patitur IV 271, 3. patitur
uel tolerat IV 141, 2; 550, 22. **perpetimur** patimur IV 270, 53.
sustinemus uel conligamur (perplectimur?) IV 376, 22. **perpeti**
καρτερῆσαι II 148, 37.

**Perpetratio** κατόρθωμα II 148, 38. κατόρθωσις καὶ δρασματοπ\<οι\>ία
(ἀτοπία *g*, δρᾶμα, ἀτονία *uel* ἀτοπία *Vulc.*) II 147, 54.

**Perpetratum** peractum, perfectum IV 376, 24. commissum IV 142, 48;
154, 10; V 132, 12. peractum IV 550, 50. **perpetrata** γιγνόμενα II
148, 34.

**Perpetro** περιέρχομαι II 402, 41. **perpetrat** κατορθοῖ II 148, 2.
peragit IV 139, 2; 376, 23; 550, 49. peragit, committit IV 271, 17.
**perpetrare** πεπλημμεληκέναι II 148, 33.

**Perpetuantes** παραμένοντας II 148, 51.

**Perpetuanum** (-arium?) aeternum fiere (febrem?) V 473, 26; 510, 11.

**Perpetuare** coniungere *Plac.* V 37, 10 = V 92, 6 = V 132, 32.
**perpetuassent** perpetuum fecissent V 645, 30 (*Non.* 150, 37).

**Perpetuitas** διηνεκότης III 459, 73. ἀειδιότης II 219, 11; III 242,
34. αἰωνιότης II 221, 43.

**Perpetuo** διηνεκῶς II 277, 3. πάντοτε II 393, 51. διὰ βίου II 270,
17. αἰώνιον III 459, 72.

**Perpetuus** ἀείδιος II 219, 9; III 242, 32. αἰώνιος II 221, 41.
**perpetua** μόνιμος III 423, 52. **perpetuum** διηνεκές II 276, 56; III
459, 72. αἰώνιον, δι᾽ ὅλου, διηνεκές II 147, 51. ἀείδιον II 219, 10; III
242, 33. sempiternum IV 141, 1. **perpetum (!)** perpetuum IV 376, 25.
perpetuum, id est longi temporis V 473, 24. plenum, integrum V 538, 46
(*Ter. Ad.* 972, *ubi* perpetuom *libri*).

**Perplexabile** perplexum V 645, 35 (*Non.* 151, 29).

**Perplexe** σπουδαίως, περιπεπλεγμένως II 147, 56. σκολιῶς II 147, 57;
433, 49. uarie uel dubie IV 140, 13.

**Perplexe loquitur** V 663, 58 (*cf.* dilatat orationem suam V 661,
46).

**Perplexus** σκολιός II 433, 48. inuolutus, timidus (tum.?) IV 271, 4.
inuolutus IV 142, 26 (*v.* perfluxus). **perplexa** perligata IV 272, 15
(perplicata *Hildebrand*); 376, 27; V 319, 54. conligata IV 141, 3.
**perflexa** multis conligata modis IV 140, 31; V 131, 52 (inutilis
conligatur m.).

**Perpluit** ualde pluit IV 419, 20.

**Pe\<r\>politae** per\[c\]limatae V 320, 44.

**Perpotatio** πρόποσις II 147, 52; 420, 8. 

**Perpotiri** perfrui V 320, 33.

**Per pragmaticam formam** per principalia imperia uel negotia V 424, 24
(*de dial.*).

**Per pragmaticum** iussio impe\[ro\]rialis V 320, 59. per
negoti\<at\>ionem *gloss. Werth. Gallée* 341 (*v. suppl.*).

**Perprandierunt** ἀπηρίστησαν III 413, 52.

**Perpressi** ὑπέστειλα III 381, 26.

**Perprocliuus** plus procliue V 510, 12. **perproclius** plus
procline (!) V 473, 28.

**Per pseudothyrum** borh ludgaet (*AS.*) V 378, 24. *Cf. Oros.* VII 6,
17.

**Perpulit** coegit IV 271, 8. coegit, suasit V 538, 27 (*Ter. Andr.*
662).

**Perpungo** παρακεντῶ II 395, 9.

**Perquam** λίαν II 148, 36; 360, 39. κομιδῆ ἀντὶ τοῦ πάνυ II 352, 49.
penitus IV 140, 24. ualde V 538, 40 (*Ter. Ad.* 566). *Cf. GR. L.* I
211, 18. satis, ammodum seu uehemens (!) V 473, 29.

**Perquiro** ἐκζητῶ II 290, 12. perquirit ἐκζητεῖ II 148, 1.

**Perracula** (*uel* perragulum, *sine interpreti*) IV 376, 28. pera
sacculum? *V.* pera.

**Perreptaui** lassus ambulaui V 538, 43 (*Ter. Ad.* 715).

**Perrigere** pergere IV 376, 31 (porrigere? *v.* pergo).

**Perrumpo** διαρήσσω II 273, 55.

**Perruptus** (praer.? *cf. Verg. Aen.* I 105) in altum leuatus V 132,
14.

**Per saecula** per tempora IV 458, 11 (*Verg. Aen.* I 445? XII 826?).

**Perscribo** καταγράφω II 340, 28.
