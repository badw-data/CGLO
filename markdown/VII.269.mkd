**Simulatio** σκῆψις II 184, 26; 433, 29. προσποίησις II 422, 51.
ὑπόκρισις *margo* II 184, 22. tinctio IV 391, 34.

**Simulator** προσποιητής III 335, 57. ὑποκριτής II 466, 44. qui alia
loquitur quam facit IV 391, 36. qui aliud loquitur, aliud cogitat IV
285, 21. qui aliud loquitur, aliud agit V 558, 16.

**Simulatus** ὑποκριτικός II 184, 22. **simulata** fincta (*vel* ficta)
IV 464, 4 (*Verg. Aen.* I 710?); 285, 19; 567, 57.

**Simulo** προσποιοῦμαι II 422, 52. πλάττομαι II 408, 63. **similas**
differis (!) uel confingis [aut premis; *cf. Verg. Aen.* I 209?] IV
172, 33 (*om. a*). **simulat** προσποιεῖται, σκήπτεται II 184, 21.
fingit IV 464, 3 (*Verg. Aen.* I 209); 285, 16; 567, 56; V 558, 10.
filius IV 423, 34 (suboles? fingit? *nisi velut* patrissat filius *lemma
intercidit*). **simulare** fingere scire, quod nescit IV 391, 33 (*cf.
Isid. Diff.* 515).

**Simultas** μικροψυχία II 371, 46 (*cf.* pusillanimitas). δασμός II
266, 40. inimica contrarietas II 593, 16. lis, contentio IV 567, 12; V
332, 41. lis, inimicitiae uel contentio IV 172, 1 (simotus *codd.*).
dolus V 394, 3. dolus aut lis IV 171, 24. dolus, lis occulta IV 285, 20.
dolus, lis occulta, rixa, contentio V 558, 15. lis occulta IV 391, 35.
litis occulta uel inimicitia V 539, 31. **simultatis** dissensionis uel
doli uel litis V 332, 30; *cod. Monac.* (*v. suppl.*) **simultatem**
contentionem V 420, 55 = 429, 35 (*Euseb. eccl. hist.* VI 10).
**simul­tates** ἀπεχθείας, ἔχθρας, ἀντιπολιτείας II 184, 24. rixae,
contentiones IV 285, 18. lites uel dissensiones IV 171, 23. lites aut
contentiones uel dissensiones IV 567, 13.

**Simult\<er\>** similiter V 647, 16 (*Non.* 170, 20).

**Simultum** uermis in cornu arietis (*vel* cornibus arietum) *Scal.* V
609, 67; 611, 2 (*Osb.* 559; *Is.* XII 1, 10). **simulta** est proprie
uermis in capite arietis natus qui dum monetur facit eum repedare
*Papias.*

**Simul uidetur** συνδοκεῖ II 444, 50.

**Simus** σιμός cilto (*h. e.* silo) II 431, 41. deytis (?) III 135, 13.
dicitur naso presso IV 172, 9 (*v.* simae capellae).

**Simussa** *v.* cimussa.

**Simussator** σειρώτης οἴνου ἢ ἄλλου τινὸς ὑγροῦ II 431, 54. *V.*
cimussator.

**Sin** εἰ τὸ ἐάν II 285, 3. si non IV 172, 16; 391, 38; V 332, 53. si
non, sic (si quo minus?) IV 285, 22; 567, 50. si uero uel non V 150, 7.
etiam, uel IV 567, 55. uero, sin autem V 539, 20 (*Ter. Andr.* 165).
quesi (*vel* quesiti) IV 171, 42 (quodsi?). *Cf.* **sino** (si uero?)
quodsi IV 172, 7.

**Sina** mandatum V 389, 40 (*Roensch Mus. Rhen.* XXXI *p.* 463; *On.
s.* 81, 17)

**Sin alias** sin aliter V 414, 46 (*reg. Bened.* 2, 41; 60, 9).

**Sinape** σίνηπι (pluraliter non declinabitur) II 431, 43 (*GR. L.* I
32, 13 *etc*). **sinere** σίνηπι II 184, 27 (sinare *e*, sinape *c*).
**sinapi** σίνηπι III 265, 66. σινάπιον III 359, 59; 503, 40. **sinape**
σινήπιν (?) III 287, 51 = 658, 17 (σίνηπι); 514, 67. **sinapis** σινάπια
III 317, 21. σινεπιν **sanapi** III 430, 67 (σινάπιν *David*).
**sinapis** nape (νᾶπυ) III 570, 17. napius III 593, 18; 614, 60; 627,
7. mulzium III 568, 53. **senape** napeus III 548, 21. **semen senapis**
euatumo III 546, 17 (*ubi* νάπυος *Schmidt: immo* εὔζωμον). **semen
sinapis** bomadis III 553, 43 (napi = buniades). **semen sinapi**
bomadis III 617, 59. **sinapio** cressa (*vel* -ae, *AS.*) V 390, 28.
cressae (*AS.*) V 390, 36. **sinapioues** cressa saxonice, qui in aqua
crescit V 333, 3.

**Sinapidiare** ungere III 605, 19.

**Sinapis albus** eruga III 590, 12; 623, 47 (albis). eruca III 611, 11.

**Sinarus** (σιναρός) uitiosus V 526, 5; 579, 19.

**Sin autem** εἰ δέ II 285, 6. εἰ δή II 285, 10.

**Sincer** *v.* sincerus.

**Sincere** γνησίως, ἀκεραίως, σπουδαίως II 184, 42. γνησίως II 264, 2.
ἁγνῶς ἐπίρρημα II 216, 53. ἀκεραίως II 221, 55 (ἀκαιρεως *cod.*).
εἰλικρινῶς II 285, 60. **sincere sinceriter** καθαρῶς II 334, 48.
**sincerius** γνησίως, ἀκεραίως, σπουδαιὼς II 184, 31.

**Sinceritas** εἰλικρινότης, δικαιοκρισία II 184, 32. εἰλικρίνεια III
363, 20. integritas V 412, 18 (*can. conc. Afric.* 93).

**Sinceriter** *v.* sincere.

**Sincerus** ἁπλοῦς II 184, 2, 8. ἀκέραιος II 221, 52 (ακαιρεος *cod.*).
γνήσιος II 264, 1. ilithios (ἠλίθιος? *contam.*) III 177, 45. εἰλικρινής
III 250, 17. lucide splendidus II 593, 18. **sincerus sinceris**
εἰλικρινής II 285, 59. **sinceris** ἀκέραιος, εἰλικρινής II 184, 29.
εἰλικρινής III 207, 21; 330, 65; 332, 13; 374, 63; 523, 59. ἀκέραιος III
328, 59 (ακαιρεος *cod.*). **sincer** καθαρός II 334, 43 (sincerus *a
e*). **sincer[t]e** ἀβλαβές integrum II 184, 30. **sincera** integra
IV 391, 39; V 414, 55 (*reg. Bened.* 72, 11).

**Sinciput** ἡμικεφάλαιον II 184, 33. ἡμικέφαλον II 324, 47; III 314, 43
(sincipicium). ἡμίκρανον II 324, 49; III 247, 2; 314, 44. medium capud
II 593, 22 (sincipit). dimidium caput IV 171, 27; 285, 35; 567, 39; V
332, 39; 482, 13
