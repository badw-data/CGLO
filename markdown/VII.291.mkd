qui (que *cod.*) non effluit uel genus metalli IV 393, 13. status
aquarum, id est lacum qui non effluit uel genus metalli V 483, 46. staeg
(*AS.*) uel meri (*AS.*) V 392, 40. **stagna** λίμναι III 433, 35.
collectio aquae IV 176, 32. **stagnae** aquae stantes IV 565, 22 (stagna
*b. cf. Serv. in Aen.* I 126). *Cf. Isid.* XIII 19, 9. *V.* ambitae.
*Diversa coniunxi.*

**Stalis** ἀρχός III 13, 13; 351, 54; 464, 41 (colei ὄρχεις *Vulc.*);
475, 8. **stalem** podicem III 604, 12. fincteri (*v.* sphincter) III
605, 36. tenismon III 606, 26 (τεινεσμός). *V.* extalis.

**Stalpa (?)** σταλαγμός II 436, 36; 494, 9.

**Stamen** στήμων II 187, 56; 437, 52 (*GR. L.* I 554, 4); 501, 64; 528,
11; 544, 40; III 21, 50; 93, 6; 209, 54; 270,

**Staminarius** νήστης ὁ τὸν στήμσνα \<νήθων\> II 187, 57 (*add. dg*).

**Stamio** partus *Scal.* V 611, 26 (statio portus *la Cerda*). *Cf.*
**stomo** (*vel* stamo) partus IV 176, 40. **stamo** partus *lib. gl.
V.* statio portus.

**Stamnium** σταμνίον III 325, 18.

**Stas** *v.* hic.

**Stata longa barcarum** *v.* barca. 27; 323, 67; 369, 29.

**Statarium** κονδολύχνιος III 270, 34 (*unde*?). κονδολύχνια
(conclolichinia *cod.*) III 197, 57 (*post* candelabrum).

**Statas** statutas IV 176, 42. statuas (?) V 578, 40. staturas (?) V
483, 48.

**Stata temporis** *v.* breuis.

**Stater** κάμπανος II 338, 8 (*cf.* statera campana *lib. gloss.*)
genus (?) masculini V 152, 16. nummus est et pensat unciam unam IV 282,
37. duo dragmae IV 565, 21. dragmas duas, id est semiuncia V 578, 42.
duo didragma \<h\>abet V 152, 17. duo didragma *Scal.* V 610, 28 (*cf.
Eucher. instr. p.* 158, 12).

**Statera** ζυγός II 187, 58; 494, 13; III 197, 60; 269, 73; 321, 27;
521, 4. ζυγόν III 292, 12; 520, 40. ζύγιον III 143, 1; 338, 25; 464, 42.
ζυγὸς ἐν ῷ σταθμίζομεν II 322, 38. ζυγὸς χρυσοχόου II 541, 48. στατήρ II
520, 35. trutina IV 393, 14. iugum IV 167, 7 (satera). iugum quod est IV
564, 24 (*item*). *Cf. Isid.* XVI 25, 4. *V.* trutina, stater.

**Staterarius** *v.* stationarius.

**Statim** εὐθέως, παραχρῆμα II 187, 59. εὐθέως II 317, 19; III 140, 23;
341, 18. παραχρῆμα II 397, 20. εὐθύς II 317, 26; III 473, 31. αὐτίκα,
εὐθέως III 464, 43. mox, confestim IV 282, 39. continuo, celeriter IV
393, 15. **sartim** uel mox stratim seu certe V 481, 24 (statim mox seu
certe?). *V.* continuo *adv.*

**Statio** σταθμός, συνεδρία καὶ παρεδρία, ὁρμός II 187, 60. στατίων II
436, 46. στάσις ἢ καταμονή II 436, 45. καταμονή II 342, 27. ναύσταθμος
III 245, 58 (*unde*?). στητορ\<ί\>α II 187, 61 (? *add. g.* statorem
στητορα *H.*). διατριβή II 275, 17 (*cf.* remoratio). διαγωγὴ ἡ
ἀναστροφή II 270, 37. ἡ \<στάσις\> III 520, 35. ab stando IV 175, 32;
565, 20. portum *cod. Monac. v. suppl.* (*cf.* stamio). **statia**
(statio?) sollicitudo V 483, 49; 515, 46 (*v.* sistans). **statione**
uigilatione V 417, 64 (*Cassian. inst.* XII 4, 1); *cod. Monac. v.
suppl.* **stationes** χορτοστασίαι III 423, 70.

**Stationarius** ἐπίσταθμος ὁ τῆς καταμονῆς ἡγούμενος II 311, 5.
pensator, staterarius (*male versa*?) II 594, 7.

**Stationem habeo** ὁρμῶ ἐπὶ λιμένος *e* II 387, 8.

**Statiuus** στάσιμος II 436, 43.

**Stator** ὀρθώσιος II 386, 45; III 8, 36; 290, 14. ἐπιστάσιος
**stator**, 'in aedem Iouis **statoris**' II 311, 8 (*Cic. in Cat.* II
12); III 238, 5 (in aede). **statori** ὀρθωσίῳ II 188, 1.

**Statorium** *v.* consistorium, statuarius.

**Statua** ἀνδριάς, στήλη II 188, 2. ἀνδριάς II 225, 13; 494, 14; 520,
38; 541, 38; III 204, 9; 267, 55; 306, 43; 353, 41; 368, 29 (statua
equestris ἀνδριὰς ἔφιππος *adscita* 368, 30 *Vulc.*); 489, 70. σταμε̄να
II 188, 14 (stantia ἱστάμενα *c.* statuta ἱστάμενα *g*, ἑσταμένα
*Vulc.*). **statuae** ἀνδριάντες III 20, 7; 91, 58; 196, 27; 395, 55.
**statuae humanae** ἀνδριάντες III 238, 61 (*unde*?).

**Statuarius** ὀρθοστάτης II 188, 3 (staturium); 386, 34. ἀνδριαντοποιός
II 225, 14; III 307, 1; 367, 13. statuarum opifex II 594, 8.

**Statu liber** ἐλεύθερος χρόνῳ κατὰ διαθήκην ἀφεθείς II 188, 7 (ὅρῳ
*pro* χρόνῳ *Scal.*). **statu[m] libera** (-um *cod.*) ἐπικεκτημένη
(επικεκαμενη *cod. -*πεπαμ*. H.*) τὴν ἐλευθερίαν II 188, 10.

**Statumen** βεβαίωμα II 256, 55. στερ\<έ\>ωμα II 437, 25 (*corr. e*).

**Statuncula** ἀνδριαντάρια III 170, 47. **statunculae** ἀνδριάντια III
238, 62 (*unde*?). *Cf. GR. L.* IV 376, 9; *W. Heraeus 'Spr. des Petr.'*
43. *V.* anta.

**Statuo** ὁρίζω ἐπὶ πράγματι II 386, 48. ὁρμῶ ἐπὶ λιμένος II 387, 8
(*v.* deueho). θεσπίζω II 328, 6. διατυπῶ II 275, 21. **statuit** censet
IV 282, 45; 565, 19. **statuimus** sistimus IV 393, 16. **statuunt**
implunt (*vel* implent) IV 464, 48 (*Verg. Aen.* I 724; VII 147: *cf.*
impleo). **statuit**, ἔστησεν, ὥρισεν, προσ\<έθετο\> II 188, 4 (*suppl.
e ex* 188, 5).
