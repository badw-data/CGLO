**ἀποψηφίζω:** -ομαι abrogo, -οντες refragantes.

**ἄποψις** aspectus, conspectus, prospectus (*et* ἄφοψις), suspectus 2.

**ἀποψύχω** derigeo.

**ἀποψῶ:** -έψησεν abolesco (abolevit).

**ἀππίδιον** *etc. v.* ἀπίδ.

**ἀπραγμάτευτος** innegotiatus.

**ἀπραγμοσύνη** incuria, incuriositas, otium.

**ἀπράγμων** incuriosus, otiosus.

**ἄπρακτος** ignavus, inefficax, intractabilis, otiosus. *V.* ἡμέρα.

**ἀπράκτως** otiose.

**ἀπραξία** desidia, ignavia.

**ἄπρατος** invenalis.

**ἀπρέπεια** dedecus, foeditas.

**ἀπρεπής** dedecor, indecens, indecorus.

**\*ἀπρεπῶ** dedeceo.

**ἀπρεπῶς** dedecore, illicite.

**ἄπροικος** indotata.

**ἀπρόκοπος** inefficax, singularis.

**ἀπρονόητος** improvidus, inconsideratus, inconsultus.

**ἀπρονοήτως** inconsulte.

**ἀπρόοπτος** improvisus.

**ἀπροόρατος** improvidus, improvisus.

**ἀπροοράτως** de improviso, improviso.

**ἀπροσδόκητος** improvisus, inexspectatus, inopinatus, insperatus, ἐξ
-ου de *et* ex improviso, ex inopinato.

**ἀπροσδοκήτως** de improviso, improviso, inopinanter, inopinato, nec
opinanter.

**ἀπρόσιτος** inaccessibilis, intractabilis.

**ἀπρόσκοπος** inconsideratus.

**ἀπροσπέλαστος** improximabilis.

**ἀπρόσφορος** incongruus.

**ἀπροσφώνητος** impronuntiatus, invocatus.

**\*ἀπρόσχωρος** arrogans.

**ἀπρόσωπος** impersonalis, inops.

**ἄπταιστος** impeccatus, irreprehensibilis.

**ἄπτερος** implumis.

**ἀπτόητος** impavidus, interritus, intrepidus.

**ἄπτομαι** affecto, attingo, tango.

**ἅπτω** accendo.

**ἄπτωτος** ὁ ἄκλιτος indeclinabilis (*cf.* aptotus), - ὁ σταθερὸς καὶ
βέβαιος sine casu.

**ἄπυγος** impuges.

**\*ἀπυρετία** sine febre.

**ἀπύρετος** sine febre. *Cf.* apyretus.

**ἄπυρος** *v.* θεῖον.

**ἀπῳδός** *cf.* apodos, - εἰμι abhorreo.

**ἄπωθεν** eminus, longe (*et* -ius), longisecus.

**\*ἀπωθητής** pellator.

**ἀπωθῶ** *vel* **ἀπωθοῦμαι** abstrudo, aspellit (-εῖ), depello,
depulsitasse (-ωθηκέναι), depulso, detestor, detrudo, deturbo, dispulit,
expello, impello, pello, propello, propulso, repello, repudio, retrudo,
ἀπωσθείς repudiatus, repulsus, ἀπωσμένος perculsus. Ἀπώθησεν *legitur s.
v.* abstrudo, dispulit.

**ἀπώλεια** baratrum (*v. p.* 435), exitus, interitus, obitus, occasus,
perditio, pernicies.

**ἀπωμοτικόν** abnutivum.

**\*απωρινον** lappa (*cf.* ἀπαρίνη).

**\*ἀπωρυγισμός** *v.* ἀπορ.

**ἀπῶρυξ** propago, - ἀμπέλου propagatio.

**ἀπωστός (?)** abominandus.

**ἀπωτέρω** ultra (ulterius).

**ἀρὰ** ἠ κατάρα exsecratio, maledictio, -αί dirae. *Cf.* ara.

**ἄρα:** καὶ δὴ - etiamnunc, μὴ - ne forte, μήτι - numnam, numquidnam,
μήτι γε - nempe non, οὐχί - nempe, nonne, οὐ μὴ καὶ - nec non, οὕτως -
itane, sicine. *Cf.* ἆρα, εἰ, καί, ὅπου, πῇ, πότε, ποῦ, τίς.

**ἆρα** an, anne, ne, - γε οὐ nonne, - γε τις quisquamne, - οὐ nempe
ergo, - ποτε ecquando, - τις ecquis, quisquamne, τουτονί - hicine
(huncine), τοῦτο - idne. *Cf.* ἄρα.

**\*ἀραβική** baculus.

**ἄραβος** tintinnus.

**ἀραιόθριξ** tenuis.

**\*ἀραιοπώγων** malibarbius.

**ἀραιός** rarus.

**ἀραιότης** infrequentia, laxitas, raritas.

**ἀραιῶ** laxo.

**ἀραιῶς** rare, rariter.

**ἀραίωσις** laxamentum, laxatio.

**ἄρακος** cicer album.

**ἄραξ** τὸ ὄσπριον arax.

**ἀράσσω** pulso.

**ἀράχνη** aranea (*cf.* rana), araneus. *Cf.* ὕφασμα.

**\*ἀρβήλιον** sicilis (-ila).

**ἄρβηλον** sicilis.

**ἀργαλέος** callidus, taeter.

**ἀργεία** *v.* ἀργία.

**Ἀργεῖος** *cf.* Graecus.

**ἀργεμώνη** aristolochia longa, lappa inversa. *Cf.* argemonia.

**ἀργέστης** corus.

**ἀργεύω** cesso, ferio, pigreo, pigresco.

**ἀργία** (*et* **ἀργεία**) cessatio, otium, pigritas, pigritia,
socordia, vacantia, αἱ - αι (αἱ σχολαί) feriae, vacatio (-ones).

**Ἀργοναύτης** *cf.* Argonautes.

**ἀργός** cessator, lentus, otiosus, piger, segnis, socors, -ἡ γῆ
incultus (-a).

**Ἄργος** (*Eins.*) Argo.

**ἀργύρεος** *et* **ἀργυροῦς** argenteus. *V.* νόμισμα, σκεῦος.
