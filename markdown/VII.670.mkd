**ὑποκριτής** dissimulator, hypocrita, secundarius (*et* subdica),
simulator.

**ὑποκριτικός** simulatus, subdolus.

**ὑποκυρτῶ** obliquo.

**ὑποκυστίς** *v.* ὑποκιστίς.

**ὑπόκωφος** subsurdus.

**ὑπολαμβάνω** aestimo, arbitror, autumo (*v. p.* 435), duco, existimo,
opinor, reor.

**ὑπολείπομαι** II, 466, 47 *s. interpr.*

**ὑπόλευκος** subalbus.

**ὑπολήγουσα** declinatio.

**ὑπολήνιον** laccus vinarius, torcular.

**ὑπόληψις** *et* -λημψις existimatio, opinio.

**ὑπολογίζομαι** reputo.

**\*ὑπολογιμαῖος** subditivus.

**ὑπολογῶ:** -ηθείς deductus.

**ὑπόλοιπος** reliquus, residuus.

**ὑπολύω** decalcio, discalcio, -εις excalcias.

**ὑπομάσχαλον** lateralium.

**\*ὑπόμαυρος** obscurus, sublutulus.

**ὑπομειδιῶ** subrideo.

**ὑπομενητέα** subeunda.

**ὑπομενητικός** patiens, perpessicius, tolerans.

**ὑπομένω** duro, fero, passo, patior, perfero, perpesso, perpetior,
persevero, sustineo, tolero, - τὸν κάματον patiens sum laboris. *Cf.*
ὑπέχω, ὑπομενητέα.

**\*ὑπομήρια** intertrigo (-igines).

**ὑπομιμνήσκω** admoneo, commemoro, comminiscor (-o), convenio, memoro,
moneo, reminisco.

**ὑπόμνημα** actum, commentarius, monimentum, -ατα gestum (-a),
habitus 1. (-a).

**ὑπομνηματίζω** admonefacio, commentor 1.

**ὑπομνηματιστής** commentarius, commentator.

**ὑπομνηματογράφος** actuarius, memor alius.

**ὑπόμνησις** admonitio, admonitus, monitio, monitus 1.

**ὑπομνηστικόν** commentarius (-um), commonitorium (*cf.*
hypomnesticon).

**ὑπομονή** patientia, perseverantia, tolerantia.

**ὑπομονητικός** tolerabilis.

**\*ὑπόμυξις** τὸ μέσον τῶν μυκτήρων cartilago.

**ὑπονέφελον** nubilum.

**ὑπονοητής** suspiciosus.

**ὑπονόθευσις** sollicitatio.

**ὑπονοθευτής** seductor, sollicitator.

**ὑπονοθεύω** illicio, seduco, sollicito.

**ὑπόνοια** opinio, suspicio.

**ὑπόνομος** amara, camera (camara), cloaca, cuniculus, tubus.

**ὑπονόστησις** recessus.

**ὑπονοῶ** conicio, suspicor. *Cf.* trepido.

**ὑπόξανθος** rubeus.

**ὑποξύω** subrado.

**ὑποπαραιτεῖται** prorogat.

**\*ὑποπατριος** filius familias.

**ὑποπείθω** sollicito.

**ὑποπιάζω** *cet. v.* ὑπωπιάζω.

**ὑποπίπτω** committo, succumbo, - τοῖς νόμοις committo (c. in legem).

**\*ὑπόπλευρον** armus.

**\*ὑποπλύνω** sublavo.

**ὑποπόδιον** astraba, scabellum, scamellum *sim.*, subsellium,
suppostorium.

**ὑποπτεύω** suspecto, suspicor.

**\*ὑπόπτιλλος** sublippus.

**ὔποπτος** suspectus, suspiciosus.

**ὑπόπυος:** -ον suppuratio, -ος γίγνομαι *cf.* suppuro.

**ὑπόπυρρος** rufus.

**ὑπορθῶ** erigo, fulcio, subrigo.

**ὑπόρθωμα** fulcimentum, pedamentum.

**ὑπόρθωσις:** εἰς -ιν τιμωρίας ad subrigenda.

**ὑπορράπτω** subsarcio, - οἰκοδομήν resarcio, substruo.

**\*ὑπορραφή** plicatura, resarcinatio, subsutio, -αί sarta tecta,
subructiones.

**ὑπορρέω** suffluo, -ρέον destillans, substillum.

**ὑπορύσσω** effodio, subrugo, subruo.

**\*ὑπορχηστής (?)** (πορκηστης *cod.*) pantomimus.

**\*ὑπόσαρκα** *cf.* hydropicus.

**ὑποσημειοῦμαι** adumbro, designo.

**ὑποσημείωσις** subnotatio.

**ὑπόσιμος** subsimus.

**ὑποσκάζω** vacillo.

**ὑποσκελίζω** supplanto.

**ὑποσκευή** substructio.

**\*ὑποσκότεινος** obscurus, sublutulus.

**\*ὑπόσπευσις (?)** *cf.* astantus.

**ὑπόσπονδος** foederatus.

**ὑποσπῶ** subtraho, subvello.

**ὑποστάζω** substillo.

**ὑπόστασις** bona, facultas, fortuna, substantia, *cf.* hypostasin
(*expl.* quasi faeces), -εις *cf.* hypostases (*expl.* tres personas
subsistentes), -ις ἐλαίου faex (faeces), fraces. *Cf.* hypostasis
(*expl.* constantes, animosi).

**ὑποστατέα** subeunda.

**ὑποστάτης** *cf.* hypostates (*expl.* expositio).

**\*ὑποστεγή** *cf.* casula.

**\*ὑποστέγιον** suggrunda.

**ὑπόστεγον** grunda, suggrunda.

**ὑποστέλλω:** ὑπέστειλα perpressi, ὑποστέλλομαι τὸ διαλανθάνω
delitesco.

**ὑποστήριγμα** fulctrum.

**ὑποστηρίζω** effulcio, perfulcio, subrigo, suffulcio, ὑπεστηριγμένος
effultus.

**ὑποστιγμή** substinctio, *cf.* casula. *Cf.* hypostigme (*expl.* sub
distinctio).
