**Plagula** δίκτυον κυνηγετικόν II 277, 48. κυνηγετικὸν λιθον (λίνον?)
II 356, 54 (plaguram *cod. corr. e*). δίκτυον II 151, 36; 497, 46.
retiaculum II 589, 51 (*Gallée* 361; *cf. suppl.*).

**Plana** ῥυκάνη III 325, 64.

**Planatio** ἀπόρθωσις II 240, 14.

**Plancas** *v.* plaugas.

**Planctio** κοπετός II 495, 27; 520, 1. planctus II 589, 54 (*Gallée*
361; *cf. suppl.*).

**Planctus** κοπετός II 151, 38; 353, 19 (plantus *cod. corr. e*); III
460, 14. θρῆνος III 145, 68. ὀδυρμός III 151, 58/59.

**Plancus** πλατύπους II 151, 39. pedibus latis IV 268, 16; V 630, 42.
**planca** (plancus?) pedibus latis que (= qui) planas et latas habet
plantas, quam (quem?) Vmbri ploton uocant V 473, 62. **plancat** pedibus
latis, qui planas et latas \<habet plantas\> quam (!) Vmbri ploton uocant
IV 144, 37 (*suppl. a*). *Cf. Festus p.* 230, 238; *Loewe Prodr.* 387;
*GL. N.* 143.

**Plane** ὄντως II 151, 49; 384, 31. σαφῶς II 430, 12. σαφῶς καὶ μέντοι
II 151, 40. profecto, σαφῶς, μέντοι, ὄντως, τῷ ὄντι II 151, 57 (*cf.
Hor. sat.* I 3, 66). sane, certe V 414, 20 (*reg. Bened.* 13, 20).
aperte, ordinate V 134, 15.

**Planeta** πλανήτης, πλάνης III 242, 10 (*unde*?). **planetum** (?) a
plano IV 349, 31 (*cf. Serv. in Georg.* I 337 = ἀπὸ πλάνου). *Cf.* hoc
**planctum** a plana V 301, 1. **planetas** stellas, sidera currentia,
quia reliquae stellae caelo fixae sunt V 134, 8. *V.* paenula.

**Plango** κόπτομαι II 353, 30. θρηνῶ II 329, 14. cum pugnum alicui
retollas *Plac.* V 37, 16 = V 93, 9 = V 134, 21 (ante cum *lac. sign.
Deuerling.* **plango \*\* plangas** cum pugno clamorem tollas *adscita
gl. sequenti H.* **plango** cum pugnum alicui rei tollas in plagas,
plangas *Buech.*). *Cf.* tarson plana (= θρηνῶ plango? ταρσός planta?)
III 160, 59.

**Plangor** κοπετός II 353, 19. plangentium uox IV 145, 3; 548, 30
(plagor). ululatus V 321, 46. **plangoribus** κοπετοῖς, θρήνοις II 151,
53. planctibus (plantantibus *codd.*) IV 144, 25; 548, 22 (*Verg. Aen.*
II 487). *V.* placor.

**Planipes** est ioculator V 622, 24. **planipedes** mimi dicti quod
apud antiquos in orchestra adsueuerant exultare idemque saltatores
uocantur IV *praef.* XVIII. *Cf. Mus. Rhen.* XL *p.* 326; *Festus p.*
181 et 326; *GR. L.* I 490, 3; *Iuvenalis ed. Friedlaender p.* 110.

**Planitia** πεδίον III 415, 17. **planities** ὁμαλότης II 382, 55.
campus IV 458, 32 (*Verg. Aen.* XI 527). *V.* sine planitie.

**Plano** ὁμαλίζω II 382, 56. διασαφῶ II 274, 3 (*ex* deplano
*corr.*). σαφηνίζω II 430, 10. *V.* aequo.

**Planopedum** κατώγεων III 190, 66; 269, 5.

**Planos** uel \[a\]stropharius, deceptor, seductor V 525, 17. graece,
latine stropharius, deceptor, seductor V 575, 3. **planum** fallacem ἀπὸ
τοῦ πλανᾶν II 151, 50 (*cf. margo*; *Hor. epi.* I 17, 59).

**Planta** σφυρὸν ποδός II 449, 53. σφυρόν II 151, 41; 497, 45. σφυράς
(?) III 312, 25. σφυρόν, φυτόν II 546, 5. ταρσός, πέλμα, σφυρόν III 249,
17 (*unde*?). ταρσὸς ὁ τοῦ ποδός II 451, 57. ταρσὸς ποδός III 312, 24.
ταρσός II 519, 52; III 176, 34. πτέρνα II 425, 27. φυτόν II 474, 16; III
263, 34. φυτόν, ταρσός II 524, 6. **plantae** φυτά III 429, 18. μόσχοι
τὰ φυτὰ καὶ αἱ φυτεῖαι II 151, 43. μόσχοι II 152, 2 (*cf. margo*). πέλματα
II 151, 56 (*cf. margo*). ταρσοί III 13, 27; 86, 36; 414, 77. *V.*
plango.

**Plantago** ἀρνόγλωσσον εἶδος βοτάνης II 245, 34. ἀρνόγλωσσον II 151,
44; 546, 13; III 265, 64; 360, 3. arnoglossa III 489, 35; 580, 4 (*adde*
septineruia *ex* 3). **plantagine** arnoglossa III 542, 4; 631, 6.
**plantaginis** arnoglosa III 607, 23; 616, 33 (arnaglosa). **plantago**
arneglosa III 549, 31. arnion (*Pseudap. c.* 2; *Diosc.* II 152) III
551, 74. probacion (probation *Pseudap.*, *Diosc.*) III 573, 50.
eptaplereon (heptapleuron *Pseudap.*, *Diosc.*) III 561, 74. patipleoron
(πλατύπλ.) III 574, 8. polineoratursion (polyneuron, thyrsion
*Pseudap.*) III 573, 51. braneosmonos (uraecneumonos *cod. Vrat.
Pseudap.* = οὐρὰ ἰχνεύμ. *Diosc.*) III 618, 15 (plantagine); 553, 65.
taruidolopius (tarbelodathion *Diosc.* tarbidolopius *cod. Vrat.*
*Pseudap.*) III 578, 16. tirica (thicaricam *Pseudap.* thesarican
*Diosc.*) III 578, 17. tetarion III 578, 18. aster III 551, 75 (*cf.
Pseudap.* asaer *cod. Vratisl.*). porno III 572, 5 (*cf. Pseudap.*).
*Cf.* porno id est **plantago macedonico** III 541, 33. **plantago**
gerolionis III 564, 60. genotobis id est gerobon III 583, 6/5. gerelion
III 591, 11; 612, 30 (plantaginis). ierelion III 591, 53; 613, 7.
gerobone III 546, 45. ierebon III 625, 25. gerolion III 624, 51
(plantagine). gerelione gerebon uel septineruia et **plantago** III 583,
9. **plantago** ennobron III 562, 48. ineploides III 565, 44.
ierobotanis III 591, 42. erobotano III 546, 7. ierobotana III 612, 67
(plantaginis). ierobana III 625, 13 (*item*). *Cf.*
