ἀνθράκιος III 367, 54. ἀνθρακίσκιον III 324, 31; 507, 81. antracas III
543, 22; 551, 32; 597, 29 (= ἄνθραξ?). ἄν­θραξ, III 552, 53. mala
pustella qui dicitur clauus III 599, 4. *Cf.* agratimata icl est
**carbunculus** siue furunculus III 551, 33. acidiua **carbunculus**
quae in pectore (*vel* corpore) coquitur quasi aqua calida III 597, 16.
**carbunculus** spryng (*AS.*) V 349, 46. **carbunculi** gemmae; hos
Graeci σπινθῆρες uocant IV 29, 43. *V.* arunculeus, papula, calculus.

**Carcer** φυλακή II 473, 47; 490, 48; 510, 2; III 80, 47; 196, 31.
*Cf.* lax **carcer** III 353, 48; 498, 66 (φυλακή? λαξευτήριον *Vulc.
Cf.* calce λάξ). εἱρκτή, φυλακή II 97, 51; III 306, 26; 523, 39.
δεσμωτήριον II 268, 42. locus inclusionis IV 213, 52. metallum,
ergastulum, custodia IV 316, 35. φυλακή, ἀφετηρία II 537, 28. φυλακή,
ἀφετηρία καὶ κιγκλίς II 549, 43. **carcerem** (?) ubi rei clauduntur IV
*praef.* XLII. **carcere** ubi rei clauduntur V 444, 50 (*cf. GR. L.*
IV 197, 32). statione V 444, 46. **carceres** (pluraliter tantum
declinabitur) ἀφετηρίαι II 252, 51 (*GR. L.* V 428, 6). ἀφετηρίαι II 97,
41. ἱππάφεσις II 97, 52. καμπτῆρες III 240, 67; 372, 9. καμπτός, νύσσα,
καμπτήρ III 240, 33. ἱππαφίδες (-φέσεις? *cf.* caballi ammissi) III 11,
1; 84, 34; 302, 55; 173, 50 (ipparis). unde quadrigae emittuntur V 444,
49; IV *praef.* XLII. *Cf. Serv. in Aen.* I 54; V 145; *Isid.* V 27,
13; XVIII 32. *V.* pedatum.

**Carcerarius** φυλακίτης II 98, 7; 473, 48 (φυλακτήρ *Vulcanius*).
custos carceris *b* II 571, 37. *V.* cloacarius.

**Carcer uentorum** spelunca IV 433, 10 (carcer spel. u. *codd. cf.
Verg. Aen.* I 141).

**Carchesium** εἶδος ποτηρίου II 97, 58. **carcessium** est in summo
malo na\<uis\> IV 29, 35. **carchesia** genus poculorum IV 29, 22; 433,
11 (*Verg. Aen.* V 77: *cf. Serv. h. l. et Georg.* IV 379); 492, 39.
genus poculi IV 213, 38. generatio (!) poculorum V 550, 36. uas uinarium
uel tonna V 617, 10. uasa pontificum circa mediam partem coangustata
dependentibus ansis a summo usque ad infimam partem V 585, 2. species
uel acumen oculorum V 564, 19 (species poc. *et* acumen oc.: *cf.*
acies), summitas mali V 353, 2 (*cf. Isid.* XIX 2, 9). sunt loca in
cacumine arboris nauis ubi funes stant ad trahendum V 617, 19.

**Carchiis** uitus, et dicitur haec uitus feminino genere V 653, 44
(*ubi* cachrys *Wirz*, canthus *W. Heraeus Arch.* IX 595; *Fleckeiseni
Ann.* 1897 *p.* 362. *Cf. Pers.* V 71).

**Carcire** abicere IV 214, 48 (arcere? coercere? *cf. tamen* carrio).

**Cardam omum** *v.* cicer erraticum, eruca.

**Cardamum** *in his subest glossis:* **cartamis** icl est agrione (?)
III 537, 70. **cartamo** lypbcorn (*AS.*) V 354, 4.

**Cardella** thistil (*AS.*) V 353, 51. *V.* carduus.

**Cardellus** genus auis cristati V 274, 31. *V.* acalanthis et
carduelis. *Cf. Goetz Comment. Woelfflin, p.* 127.

**Cardiacus** καρδιακός II 338, 55 (cordiacus *praeter ae*); III 207,
22. passio cordis, non stomachi III 598, 38. diaforasin III 599, 52.
sincopen III 606, 17 (*cf. Cass. Fel.* LXIV). **cordiatus** καρδιακός
III 444, 16; 484, 14 (*add.* leg. cardiacus).

**Cardinales** κλίματα III 426, 30.

**Cardinarius** arcarius uel primarius (arc. uel *om. Epini*) V 352,
7.

**Cardine[m] rerum** extremitate[m] rerum IV 433, 12 (*Verg. Aen.*
1 672 *et Serv.*): *cf.* haud tanto cess. c. r. *et* extremitatem
rerum.

**Cardineum** supraliminarium IV 491, 44. **kardineam** pagani dixerunt
in domorum ostiis cardinalem deam *Mai* VII 565 (*cf.* Carda *in*
*lexicis*).

**Cardo** στρόφιγξ II 97, 56; 439, 8; III 365, 19. στροφεύς III 19, 52;
91, 44 (strothos). κέντρον III 292, 63; 524, 38. uertebrum ostei II 572,
3. summa pars ostei IV 31, 29. ubi uertitur ianua V 274, 6. extrema pars
ualuae IV 433, 13 (*Verg. Aen.* I 449). ima pars horti (ostii?) V 444,
43. origo, radix, stirs (= stirps) V 444, 48 (*pf.* cardinem rerum).
**cardines** κυλάδες III 190, 55. στρόφιγγες III 312, 45. κυλάδες,
στροφεῖς III 268, 66.

**Cardo** cinarios (*pro* κινάρα?) III 555, 61 (cardos); 620, 19.
cinario III 544, 17; 581, 1; 631, 43. cinargio III 589, 25; 610, 38.
enarra III 545, 39. eranara III 545, 70. enangra III 590, 24; 611, 48
(enanagra); 623, 60. erattilidus (ἀτρακτυλίδος *Stadler: cf.Diosc.* III
97) III 546, 15. erapticlilos III 538, 52. tibia III 630, 7. erugines
(ἠρύγγιον?) III 562, 74.

**Cardo fullonicius** amilia III 586, 30 (folinicius); 607, 21; 616, 31.
*Cf. v. Fischer-Benzon p.* 122.

**Cardo maior** amiliunta III 587, 15; 608, 14; 617, 5.

**Cardo niger** scoliesmus (*cf. Mai* VII 454: σκόλυμος?) III 595, 17;
629, 9. dat foca (= dipsacon) III 589, 46; 610, 52 (cardus); 623, 10.
