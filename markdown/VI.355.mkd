*Cf. Plaut. Curc.* 424. *V.* disicio, clispargo.

**Dissidens** ἀποκεχωρισμένος II 51, 58. **dissidentes** discordantes IV
230, 4. **dissidentia** distantia, differentia IV 331, 1.

**Dissideo** διχοστατῶ. Cicero pro Marco Marcello (*c.* 10): sed armis
etiam \<et\> castris dissidebamus II 279, 28. διαφέρομαι II 275, 32.
**dissidet** διαχωρίζεται II 51, 59. discordat IV 54, 43; 229, 11; V
408, 67. discordat, dissentit IV 56, 22; V 285, 59. dissentit, discordat
IV 332, 35. desentit V 287, 20. dissentit, distat, discordat IV 506, 13.
**dissident** discordant V 285, 7. **dissidebat** discordabat V 356, 31;
408, 11. *V.* dissentio.

**Dissignat** ordinat, distribuit IV 57, 26; V 451, 46; 497, 14. *V.*
designo.

**Dissilio** διαρήγνυμαι II 273, 56. **dissiluit** ἐλάκησεν II 52, 34.
ἐρράγη, ἐλάκησεν II 53, 3. discrepuit siue descendit (*v.* desilio) V
285, 43 (*cf. Oros.* IV 20, 35). **dissiluisse** aperuisse, crepasse IV
55, 28 (*Verg. Aen.* III 416). *V.* desilio, dissoluo.

**Dissiliunt utres** rumpuntur *Plac.* V 18, 4 = V 62, 14.

**Dissimilis** ἀνόμοιος II 52, 62; 228, 24. **dissimile** dispar,
disparile IV 332, 36. **dissimilem** ἀνόμοιον II 52, 3. **dissimili**
dispari V 533, 4 (*Ter. Ad.* 41). discrepanti IV 57, 29.

**Dissimilis sum** ἀνόμοιός εἰμι II 228, 26.

**Dissimilitudo** ἀνομοιότης II 228, 27.

**Dissimilo** *v.* disto, dissimulo.

**Dissimulanter** προσπεποιημένως II 422, 43.

**Dissimulata** παραπροσποιηθέντα II 52, 11.

**Dissimulatio** μετεωρισμός II 370, 5; 494, 56. παρενθύμησις,
παραλογισμός II 51, 33. προσποίησις II 422, 51. παραπροσποίησις II 396,
8. ὑπόκρισις II 466, 43. **dissimulatione** tinctione, celatione V 451,
18.

**Dissimulator** ὑποκριτής II 466, 44.

**Dissimulo** ὑποκρίνομαι II 466, 42. πλάττομαι II 408, 63. ῥᾳθυμῶ II
427, 20. προσποιοῦμαι II 422, 52. παραπροσποιοῦμαι II 396, 9.
παρενθυμοῦμαι III 154, 13. ἀφοσιοῦμαι II 253, 45. **dissimilo** ἀνομοιῶ
II 228, 25. **dissimulo** praetereo V 286, 47. **dissimulat**
παραλογίζεται, παραπροσποιεῖται II 52, 6. conticiscit, praeterita
neglegit V 410, 44 (*cf. can. conc. Afric.* 93; *decr. Fel.*).
praeterita neglegit (*reg. Bened.* 2, 56?) V 413, 9. miđiđ (*AS.*) V
408, 15. **dissimulent** παρενθυμοῦνται, ὑποκρίνονται, προσποιοῦνται II
52, 2 (dissimulant *e*). cessent (celent?) IV 438, 49 (*Verg. Aen.* IV
291). **dissimulauit** distulit IV 58, 11. **dissimulatur**
ἀποπροσποιεῖται II 51, 34. σιωπηθῇ II 52, 7. **dissimulari** ἀφοσιοῦσθαι
II 52, 9.

**Dissinus** *v.* Dossenus.

**Dissipatio** διασπασμός II 274, 19; 494, 57.

**Dissipatum** καταλυθέν II 52, 31 (distipulum *cod. corr. c*); 35.

**Dissipiscit** *v.* desipiscit.

**Dissipo** διασπῶ II 274, 22. διασκορπίζω II 274, 14. διασκεδάζω II
274, 11. σκεδαννύω II 432, 52. **dissipat** παρακούει (παρακρούει *apud.
Labb.* = desipit?), παρατάσσεται (*ad* dirigit 6?) II 53, 7 (desipat g).
διασκεδαννύει II 52, 29. disturbat (*vel* det.) IV 54, 41; 507, 28; V
407, 60. **dissipa** disrue, disperge IV 332, 38.

**Dissire** desuere (dissuere *Loewe GL. N.* 108. desiuare desinere
*Bugge Fleckeiseni ann.* 1872 *p.* 95) IV 332, 39. *V.* cuso, dissuo,
resuit.

**Dissociata** disiuncta V 546, 47 (*Ovid. Met.* I 25).

**Dissocio** διαλύω κοινωνίαν II 272, 34. κοινωνίαν διαλύω II 351, 56.
διαλύω II 535, 30. διαχωρίζω II 276, 4. **dissociat** διαλύει κοινωνίαν
II 52, 27.

**Dissologia** duplex locutio IV 56, 20.

**Dissolutio** διάλυσις II 272, 31; III 135, 62; 338, 10; 446, 27.
διευλύτωσις II 276, 42.

**Dissolutus** ἔκλυτος II 291, 34; 36. διαλελυμένος II 272, 16; III
136, 1. **dissoluta** conuulsa IV 438, 46 (*cf. Verg. Aen.* II 507 *et*
conuulsa). infecta, fracta IV 332, 42.

**Dissolutus** διάλυσις II 272, 31.

**Dissoluo** καταλύω III 76, 43. διαλύω II 272, 33; III 135, 59. παραλύω
II 395, 38. dirrumpo V 551, 31. **dissoluis** διαλύεις III 135, 60.
**dissoluit** διαλύει II 52, 4; III 135, 61. exoluit IV 332, 41.
**dis\<s\>oluerat** ascaeltte (? *AS.*) V 409, 11 (*cf.*
*Wright-Wuelcker* 385, 30). **dissoluisse** (dissiluisse *Nettleship
'Journ. of Phil.'* XIX 120, *recte*) contremuisse IV 57, 37.
in\<h\>iasse (inaniasse *a*), crepuisse IV 56, 10 (*contam. cf.*
dissilio). *V.* desoluo.

**Dissona** dissimilia V 628, 7.

**Dissono** ἀπηχῶ II 235, 8. διαφωνῶ II 275, 49. **dissonat** per
diuersa sonat IV 56, 24. non consentit IV 332, 43. non conuenit IV 507,
21. diuisi (? *an contam. cum* dissortes?) IV 507, 26. **desonuit**
desentit V 405, 59. *Cf.* distonat, disdonat, dissulto.

**Dissortes** διακληρωθέντες II 51, 24. dis\<s\>ociat\<i\>, sine sorte V
287, 35. desociati, diuisi, sine sorte sociati V 628, 8. *Cf. Isid.* X
51.
