**Audax** τολμηρός II 457, 2; III 177, 46; 250, 18; 372, 66. θρασύς III
145, 36; 334, 61; 338, 48; 439, 81; 504, 52; 522, 61. θρασύς, τολμηρός
II 26, 13. αὐθάδης III 129, 6. temerarius IV 22, 53; 471, 13. fortis IV
84, 56; 523, 39. qui periculum non timet IV 312, 8 (*cf. Isid. Diff.*
59). **audaces** τολμηροί II 26, 7. *V.* atrox.

**Audax facinus** inprobum factum V 531, 22 (= *Ter. Eun.* 644).

**Audentia** (augentia *cod.*) τόλμα. Cicero in Pisonem (XVI 37):
'confer, si audes, ausentiam (absentiam *codd. Cic.* audentiam *e*) tuam
cum mea' II 457, 1.

**Audens** εὔτολμος II 319, 62. **audentes** τολμῶντας II 26, 10.
**audentior** audacior V 441, 57 (*Verg. Aen.* VI 95). praesumptior IV
23, 47.

**Audenter** confidenter IV 209, 14. audentius, constantius IV 312, 9.

**Audeo** τολμῶ II 26, 6; 457, 4. θαρρῶ II 326, 33; III 75, 39. audaciam
sumo IV 23, 39. ausim IV 312, 10. *Cf.* **audet** ausus sum IV 23, 20.
**audet** ausus est IV 312, 11. confidit uel ausus est IV 484, 11.
**aude** τόλμησον II 26, 8. **ausim** uerbum est promissiui modi (huius
modi *G*), ut si dicas: ausim dicere, ausim recitare (a cecitate *R*):
cuius est uerbi prima (est prima uerbi *G*) persona audeo, et
dicit\<ur\> (*corr. nonnulla exempl. lib. gloss.*) ausim, ausis, ausit
uel audet (audebit *Deuerling*) *Plac.* V 4, 7 = V 49, 9 = V *praef.*
XVI (declinabitur *pro* dicit, *fortasse recte*). audeo IV 209, 41.
audacter IV 22, 54. audaciter V 269, 35; IV 479, 10. audeo uel audaciter
IV 312, 36. ausus sum IV 23, 28. ausus sum uel audeo V 442, 15. audacter
uel audeo V 346, 36. **audeatur** τολμηθείη II 26, 11.

**Audiens** *v.* dicto audiens.

**Audientia** ἀκρόασις II 223, 52; 534, 14. auditus II 568, 44.

**Audio** ἀκούω II 223, 30; III 73, 8; 123, 6; 337, 24; 398, 6 (*cf.*
οὐκ ἀκούω non **audio** III 5, 73). **audis** ἀκούεις III 73, 9; 123, 7;
337, 25. **audit** ἀκούει II 25, 53; III 73, 10 (*contam.*); 123, 8;
337, 26. **odit** audit ut froger fruges (= frodes fraudes), clodus,
claudus *Plac.* V 89, 7 = V 125, 26. **audimus** ἀκούομεν III 337, 30.
**auditis** ἀκούετε III 123, 13; 337, 31. **audiunt** ἀκούουσιν III 73,
14; 123, 14; 337, 32; 398, 10. **audi** ἄκουε II 25, 54. ἄκουσον III 73,
11 (?); 123, 9; 337, 27; 398, 5. **audite** ἀκούσατε III 73, 13; 398, 8.
**audiam** ἀκούσω III 73, 12; 123, 10; 337, 28. **audiet** ἀκούσει III
123, 11; 337, 29. **audibimus** ἀκούσομεν III 123, 12. **audiui** ἤκουσα
III 440, 1. **audisti** ἤκουσες (!) III 398, 7. **audiit** ἤκουσεν III
4, 58. **audiuimus** ἠκούσαμεν III 398, 9. **audierunt** ἤκουσαν III
398, 11. **audierat** cognouerat IV 485, 40. **audior** ἀκροῶμαι II 224,
10. **audiatur** (?) ἀκουσθήσεται II 26, 12. *V.* dicto audiens.

**Audita** cognita IV 485, 39.

**Auditio** ἄκουσμα II 223, 29.

**Auditor** ἀκροατής II 223, 53. **auditores** ἀκροαταί III 24, 47; 199,
13; 351, 70; 352, 3; 395, 24; 400, 41; 440, 2.

**Auditorium** ἀκρωτήριον (!) II 26, 3. ἀκροατήριον II 223, 54; III 24,
46; 198, 29; 352, 2 (*cf.* πρὸς τον ακουατεριον ad **auditorium** III
115, 22/23 = 644, 26). augurium (*aut male versum aut corruptum*) II
568, 45. ἀκρόασις II 528, 61. **auditorio** scola legentium (*Euseb.
eccl. hist.* XI 9?) V 421, 52 = 430, 35.

**Auditus** ἀκοή II 509, 11; 537, 13; 549, 18 (*cf.* **auditus**, auris
ἀκοή II 222, 57; III 247, 30); III 25, 19; 175, 10; 348, 67; 350, 39
(ἀκοαί); 506, 37; 551, 24. ἀκρόασις II 223, 52; 487, 29.,

**Aufero** ἀποκομίζω II 237, 48. ἀποφέρω II 242, 25. ἀφαιροῦμαι II 252,
21. ὑφαιροῦμαι II 468, 58. **aufert** ἀποφέρει II 26, 16; 27, 38.
tollit, abducit IV 312, 14. **aufer** ἄφελε II 252, 44. **auferre**
ἀντανελεῖν (ἀνελεῖν *H.*) II 26, 19. **auferes** interem[i]as V 442,
5. **abstulit** ἀφείλετο II 252, 42. ἀφείλατο (!) II 5, 49. eripuit IV
302, 29.

**Aufertice** ablatiuus IV 22, 22 (ἀφαιρετική *Housman 'Journ. of
Phil.'* XX 49, *Hessels p.* XIII, *recte*).

**Aufertus** *v.* abrogatus.

**Aufugit** abscedit IV 23, 32.

**Augeo** αὔξω II 251, 16. αὐξάνω II 251, 10. **auget** αὔξει II 26, 23.
προστίθησι II 26, 28. crescit (*cf. Roensch 'Beitr'* III 9), ampliat IV
22, 56; 312, 15 (cupit *add.: v.* auet); 473, 36; 484, 12. **auge**
πρόσθες III 154, 8. **augere** αὐξῆσαι II 26, 26. προσθεῖναι III 154, 9.
ampliare IV 405, 48; V 346, 19 (arguere).

**Augescit** αὔξεται II 251, 11.

**Augificat** auget V 638, 28 (= *Non.* 76, 1).

**Augmentarius** αὐξητικός II 251, 14.

**Augmentatur** crescit IV 312, 17.

**Augmentum** αὔξησις II 251, 12; 528, 51; 546, 52. προσθήκη II 421, 40;
502, 50. προσθήκη, ἐπιθεματισμός, πρόσοδος II 26, 20. incrementum, ab
augendo V 442, 8. **augumentum** profectus V 590, 35.

**Augur** οἰωνοσκόπος II 381, 27; III 10, 10; 83, 63; 171, 26; 238, 9;
302, 4; 362, 55. ὀρνεοσκόπος II 387, 11. οἰωνοσκό-
