**ἄζυμος** sine fermento (*cf.* fermentum), -ον azyma, infermentum, non
fermentatum.

**ἄζωστος** ὁ μὴ ἔχων ζώνην discinctus 1.

**ἀηδής** deformis, insolens, insuavis, litigiosus, malchio, odiosus,
perosus, taediosus.

**ἀηδία** *et* **ἀηδεία** lis, rixa, taedium.

**ἀηδίζομαι** taedeo, taedesco.

**\*ἀήδονος** insuavis.

**\*ἀηδοποιός** litigiosus.

**ἀηδών** luscinia *et* -ius *sim.*, philomela. *Cf.* querquedula.

**ἀηδῶς** taediose.

**ἀήθεια** insolentia.

**ἀήθης** insolens, insuetus.

**ἀήρ** aer, aether, - οὔριος circius.

**ἀήττητος** inexsuperabilis, invictus.

**ἀθαμαντικόν** meum.

**ἀθανασία** immortalitas.

**ἀθάνατος** immortalis. *Cf.* athanatus, θεός.

**ἀθέατος** invisibilis.

**ἀθέμιστος** incestus.

**\*ἀθεμιτοποιός** infanda faciens, infanda gerens.

**ἀθέμιτος** illicitus, infandus, infaustus, nefandus, nefarius,
profanus, -ον illicitum, infas, nefas.

**ἄθεος** *cf.* atheus, theos.

**ἀθεράπευτος** incurabilis, insanabilis.

**ἀθέριστος** insecta.

**ἀθέρωμα** *v.* ἀθήρωμα.

**ἄθεσμος** illicitus.

**ἀθέσμως** illicite.

**ἀθέτησις** deceptio.

**ἄθετος** inhabilis. *Cf.* intercalaris.

**ἀθετῶ** decipio, refello, reprobo.

**Ἀθηνᾶ** Minerva. *V.* ξόανον.

**Ἀθηναῖος** Atheniensis.

**ἀθήρ** *et* - σίτου arista, - οἰνάδος crupula.

**\*ἀθήριον** *vel* ἀθέριον (?) *cf.* character.

**ἀθήρωμα** *vel* ἀθέρωμα *cf.* atheroma.

**ἄθικτος** illibatus, incontaminata, intactus, intaminatus,
intemeratus.

**ἀθλητής** athleta. *Cf.* μαλλός.

**ἄθλιος** calamitosus, miser, nequior (-issimus), -ώτατος aerumnosus.

**ἀθλιότης** miseria.

**ἆθλον** missum. *Cf.* athlum.

**ἄθραυστος** illibatus, infractus, inquassatus, inviolabilis.

**ἀθροίζω** aduno, congrego, glomero, -ει gregat, -ομαι glomero,
ἠθροισμένος adunatus.

**ἄθροισις** adunatio, coacervatio, coaggeratio, conventus.

**ἄθροισμα** caterva, cohors, globus.

**ἀθροιστικῶς** catervatim.

**ἀθρόος** frequens.

**ἀθρόως** passim, [proclivus (-e)], repentim, summatim.

**ἀθυμία** animi depressio, anxietas, devotio, exanimatio.

**ἀθυμῶ** deficio, -εῖ despondit animo.

**ἀθύμως** taedium (-o).

**ἀθῶος** innocens.

**ἀθώως** innocenter.

**Αἰακίδης** Aeacides.

**Αἰακός** Aeacus.

**Αἴας** Aiax.

**αἶγα** capra.

**\*αἰγάγρειον** capriolina caro.

**αἰγάγριον** capreola, caprifer (-ra), ibex.

**αἴγαγρος** caper, caprifer, ibex.

**Αἰγαῖον πέλαγος** Aegeum mare.

**\*αἰγάριον** (*Eins.*) capella.

**αἴγειον** *et* **αἰγεία** (-εια) caprina, caprina caro. *Cf.* κόπρος.

**αἴγειρος** alnus, populus, III 264, 37 *sine interpr.*

**αἰγελάτης** caprarius.

**αἰγιαλός** litus. *Cf.* litus atrum.

**αἰγιαλώδης** litoralis.

**αἰγίθαλλος** parra, parra maior.

**αἴγιθος** parra. *Cf.* αἴγινθος.

**\*αἰγίκερον (?)** fenum graecum.

**αἰγικόν** gramen.

**αἰγίλιψ** *cf.* aegilipon saxum.

**αἰγιλώπιον** fistula. *Cf.* aegilopium.

**αἰγίλωψ** avena, fistula (*cf.* tolas).

**αἴγινθος μικρός** parra modica. *Cf.* αἴγιθος.

**αἰγίπαν** semicaper.

**αἰγίς** *cf.* aegis.

**\*αιγιτιπος** semicaper.

**αἴγλη** fulgor, iubar, nitor.

**\*αἰγοβοσκός** caprarius.

**αἰγόκερως** capricornus.

**αἰγονόμος** caprarius, opilio.

**αἰγοπόλος** caprarius.

**\*αἰγοστάσιν** (= -σιον) caprile.

**Αἰγύπτιον** Aeguptium.

**Αἴγυπτος:** *v.* ἔπαρχος.

**\*αἰγών** ὁ τόπος ἤτοι ἡ μάνδρα caprile.

**αἰδέσιμος** honestus, -ώτατος praedicabilis.

**αἰδεστικός** pudicus.

**αἰδεστικῶς** pudenter.

**αἰδημόνως** pudenter, pudice.

**αἰδήμων** modestus, probus, pudens, pudicus, pudoriosus, verecundus.

**ἀΐδιος** *v.* ἀείδιος.

**αἰδοῖον** penis, pudendum, sira, veretrum, -α genitalis (-ia),
verenda. *Cf.* ἀπόκοπος, ἐκτέμνω, σαύρα.

**αἰδοῦμαι** pudet me, verecundor, vereor, μὴ αἰδεσθείς vesania.

**αἰδώ** pudor, religio.

**αἰδώς** pudicitia, pudor.

**αἰθάλη** favilla, fuligo.

**αἴθε** utinam.

**αἰθήρ** aether.
