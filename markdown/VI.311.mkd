IV 53, 12. inhonestatio V 188, 10. inhonesta\<tio\>, dehonestatio IV 51,
8. *V.* decoloratio.

**Dedecoratus** deturpatus IV 409, 17. **dedecoratius** inhonestatius V
188, 11; IV 504, 11.

**Dedecore** ἀπρεπῶς II 243, 9.

**Dedecoro** ἀδοξίῳ ἐμβάλλω II 218, 61. καταισχύνω II 341, 1.
**dedecorant** ἀποκοσμοῦσιν, α\<τι\>μάζουσιν II 39, 44.

**Dedecus** ἀδοξία II 218, 60; 506, 58. ἀκοσμία, αἶσχος, ἀδοξία II 39,
46. ἀκοσμία II 223, 24. αἶσχος II 221, 6. ἀπρέπεια II 243, 7; 529, 57.
αἰσχύνη, ἀπρέπεια II 547, 16. crimen IV 225, 15. scelus IV 49, 7. macula
V 405, 64. uitium, macula, nota IV 327, 51. turpe, inhonestum IV 327,
52. **dedecora** turpia V 627, 54. *V.* dedecor.

**Dedeum** φυνικιον II 38, 4; 39, 48 (*ubi* Tyrium Φοινίκιον *Nettleship
Arch.* VI 150: didaeum φ. *h: Ed. Diocl.* 16, 89 δηδιου *pro* απλιου
*scribens confert Buech.*).

**Dedicatio** καθιέρωσις (catherisis *cod.*) III 148, 26.
**dedicationes** encaenia IV 328, 2. *V.* tabernaculorum dedicatio, in
dedicationem uenit, encaenia.

**Dedicatum** καθωσιωμένον II 40, 10; 14. καθιερωμένον III 148, 27.
denotatum, sacrosanctum IV 327, 54. deuotum, consecratum IV 502, 18.
*V.* dicatus.

**De dichoto\<me\>matibus** de coaetaneis (?) V 405, 31; 356, 29 (*cf.
Hieron. de vir. ill.* 57 *et Gen.* 15, 9).

**Dedico** καθιερῶ II 335, 18; III 148, 25 (cathero). ἀφιερῶ II 253, 17.
καθοσιῶ II 335, 37. **dedicat** καθοσιοῖ, ἀφιεροῖ, ἀφοσιοῖ II 40, 12;
13. καθιδρύει, ἀφιεροῖ II 39, 47. consecrat IV 52, 28; 225, 41.
consecrat, perficit IV 327, 53. **dedicare** pro religione offerre IV
502, 19 (*v. ab* IV 225, 41). *V.* delicare.

**De dictae mao** (*vel* de dictemao) de exductione exitus V 356, 12.
exitus de exductione V 405, 23 (*obscura*).

**De die** mature, ante hora\<m\> cenandi V 533, 17 (*Ter. Ad.* 965).

**Dedignata** indignata IV 437, 16 (*Verg. Aen.* IV 536). non dignata
IV 53, 1.

**Dedigno** (-or *ae*) ἀπαξιῶ II 233, 11. **dedignatur** ἀναξιοπαθεῖ II
39, 50. non dignat, spretat (*vel* spernit), contemptat (*vel*
contemnit) V 449, 24 (**dedignata** non digna, spreta, contempta
*Buech.*).

**De dimenso tuo** a me[n]tiendo dictum [a mentiendo dictum] IV 50,
24 (uel ab eo quod in mense uno acceperit uel a m. d. *a*)*. Cf. Ter.
Phorm.* 43 (*cum Donato*).

**Dedisco** ἀπομανθάνω II 239, 1. dedi\<di\>ci, nescio V 449, 23.
**dediscere** nescire V 284, 1; 406, 16; IV 504, 12. est amittere quod
di\<di\>ceris. Lucanus (late *codd.*): longeque togae tranquillior usui
De\<di\>dicit iam pace ducem, hoc est amisit (*Phars.* I 130) V 188, 13.
**dedicit** εμβαθεν (didicit ἔμαθεν *vel* dedidicit ἀπέμαθεν) II 40, 11.
*V.* desciscit.

**Dedita opera** ἐπίτηδες II 311, 58. ualde data IV 226, 16; 328, 1; V
284, 29; 405, 8. mala (= ualde) data IV 51, 11 (*Ter. Eun.* 841); 504,
25. *V.* deditus. *Cf. Festus Pauli p.* 70, 5.

**Dediticius** ἔκδοτος εἰς κόλασιν II 40, 15; 289, 39. ἑαυτὸν παραδοὺς
ἐν πολέμῳ II 283, 2 ἐκδεδομένος II 289, 15. ἔκδοτος, κατάκριτος II 40,
7. tormentis redditus II 576, 28. damnaticius IV 328, 3. pro obside
datus IV 51, 28. qui de sua prouincia ad aliam se tradet IV 226, 9
(dedicius *cod. Sangall. cf. Arch.* XI 85). **dediticius** (*vel*
dedicius) si barbarus tradat se Romanis IV 51, 10; 504, 27; V 188, 14.
**dediticii** οἱ μετὰ τὸ ληφθῆναι τῇ τῶν νικησάντων πολεμίων ἐξουσίᾳ
ἑαυτοὺς ἐπι[σ]τρέψαντες, κατάκριτοι II 39, 52. *Cf. Arch.* V 423; 429;
IX 370; XI 82 *sqq. V.* daticius.

**Deditio** ἔκδοσις εἰς κόλασιν ἤ πόλεμον II 289, 38. ἔκδοσις II 535, 1.
donum II 576, 38 (*transtulit* δόσις). traditio spontanea IV 51, 9; 328,
4; 504, 26; V 284, 28; 496, 37; 547, 35 (sp. trad.). sui traditio IV
228, 10. hondgong (*AS.*) V 356, 44. traditio, handgang (*AS.*),
spontane\<a\> V 405, 43. *V.* in deditione uenit, dedo.

**Deditus** ἔκδοτος II 39, 53; 535, 2. promissus IV 225, 34 (perm. ?).
traditus, subiectus IV 226, 15. intentus, subditus IV 51, 4. subiectus
IV 328, 5. subiectus, seruiens, int\<ent\>us, subditus IV 504, 24.
**dediti** traditi IV 48, 52. **dedi[c]ta** ἐπίτηδες (*scil.* dedita
opera) καὶ ιδοτα (ἔκδοτα) II 39, 51. **dedita** διαδοθέν\<τα\> II 39, 49
(didita?). *V.* noxae deditus.

**Dedo** ἐκδίδωμ[α]ι II 289, 25. ἐκδίδωμι εἰς κόλασιν II 289, 27.
trado IV 226, 14. humilio, deputo, obligo, trado IV 50, 42. **dedo,
dedit** trado, tradit se V 284, 50 (*v.* de gente). **dedunt** tradunt
IV 225, 38; 504, 23; V 283, 54; 406, 13. **dedunt se** subcumbunt aut
arma tradunt IV 49, 27; 504, 22. **dedere** est a deditione dictum,
**deditio** enim dicitur quando se uicti aut uincendi hostes uictoribus
tradunt *Plac.* V 18, 5 = V 60, 25 (*cf.* V *praef.* XVI, *Isid.* IX 4,
49). **dedere** tradere, deputare IV 50, 35. **dedam** ἐκδώσω II 37, 60.
**dedas** ἐκδῷς II 37, 59. tradas, des IV 226, 17.

**Dedoceo** ἀποδιδάσκω II 236, 31. **dedocet** ἀποδιδάσκει II 40, 8.
