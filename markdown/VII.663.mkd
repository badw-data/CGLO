**τριτέγγονος** *cf.* τριυιωνός.

**\*τριτημέρᾳ** die tertio.

**τριτημόριον** triens.

**τρίτομον** tricodatum.

**τρίτος:** -η tertia, -ον triens, -ον (*adv.*) ter, tertia (-o), -ης
ἡμέρας nudiustertius, -ῃ ἡμέρᾳ die tertio, -ῃ περιόδῳ tertio pedato, ἡ
-η τῶν γάμων repotia, εἰς -ην perendie, ἐκ -ου ex tertio, tertio pedato,
ἕως -ου usque tertio.

**\*τριυιωνός** ἤτοι τριέγγονος trinepos.

**τρίφαλλος** *cf.* triphallus (*expl.* Πρίαπος).

**τριφυής** tricapitinus, triceps.

**τρίφυλλον** medica, trifolium.

**τρίχαπτον** *cf.* trichaptum (*expl.* subtilissimum vestimentum ad
instar capillorum).

**\*τριχεα** trafan.

**τριχθά** *cf.* trafan.

**τριχίας** sardina.

**\*τριχινοσεν** trumen. *V.* τρυχινόεις.

**τρίχινος** *cf.* tricinum.

**τριχολαβίον** vulsella.

**\*τριχολαβίς** forficula, tonsilla 1., vulsella.

**\*τριχολάβον** vulsella.

**τριχολογεῖ** piligerat.

**τριχομανές** capillus Veneris.

**\*τριχοφόρος** saetigerus, saetosus.

**τριχοφυῶ** oppilasco.

**τριχῶ:** τετριχωμένος saetosus.

**τριχώδης** capillosus, pilosus, villosus.

**τρίχωμα** capillamentum.

**τρίχωρον** *cf.* trichorum (*expl.* locus prandii qui et sima dicitur;
tres cameras sive tres absidas).

**τριχῶς** trifariam.

**τρίχωσις** capillamentum, pilositas.

**τριώνυμος** *cf.* trionymus (*expl.* qui tria nomina habet).

**τροιά** filum, - ἡ κρόκη subtemen.

**τρομητός** tremibundus.

**τρομικός** tremibundus.

**τρομοποιῶ** tremefacio.

**τρόμος** timor, tremor.

**τρόπαιον** tropaeum.

**τροπαιοφόρον** *cf.* tropaeophorum (*expl.* quo tropeum portant).

**τροπή** consternatio, demutationes (-αί), - ἡλίου *et* κυμάτων (=
κλιμ.) solstitium, ἐαρινή - II, 282, 61 *s. interpr.*, - θερινή
solstitialis, solstitium, - χειμερινή bruma (*cf. p.* 435), brumalis
(*ubi adde* τροπὴ χ. *post* II, 460, 8).

**τροπικός:** - θερινός solstitialis, - χειμερινός brumalis. *Cf.*
tropica (*expl.* splendida, moralia).

**τρόπις** *et* - πλοίου carina.

**τροπολογία** *cf.* tropologia (*expl.* moralis intellegentia).

**τρόπος** genus, ister, modulus, modus, mos, pactum, secta, sectum,
*cf.* tropus (*expl.* locutionis modus, sonus, cantus, mensura
dictionis), τούτου τοῦ τρόπου huiuscemodi, huiusmodi, οὐδενὶ -ῳ
haudquaquam, nullatenus, nullo modo, παντὶ -ῳ omnifariam, omnimodo, ποίῳ
-ῳ quomodo, quo pacto, τινὶ -ῳ quodammodo, εἴ τινι -ῳ siquo modo,
τοιούτῳ -ω eo modo, ᾧ δήποτε -ῳ quocumque modo, ὃν *et* τίνα -ον
quemadmodum, τοιοῦτον -ον eo more, ἐκ παντὸς -ου omni modo, κατὰ -ον per
modum, pro modo, καθ' οὐδένα -ον nequaquam.

**τροπωτήρ** strepus.

**τρούβλιον** *v.* τρύβλιον.

**τρούλλα** (-ας *cod.*) trulla.

**τροφεῖον** alimentum, alimonium, nutrimentum, -α alimentum (*et* -a),
cibarium (-a).

**τροφεύς** alerius, altor, alumnus, educator, nutricius, nutritor.

**τροφή** alimonium, alitudo, cibus, esca, pastus 1., victus 2., -αί
cibarium (-a). *Cf.* χορτασία.

**τρόφιμος** ὁ τραφεὶς ὑπό τινος alumnus, nutricius.

**τροφός** nutricula, nutrix. *Cf.* τιθηνός.

**τροφώ** nutrio.

**τροχαλός** loquax, velox.

**τροχαλῶς** ἐπὶ δρόμου cursim.

**τροχάς** gallicula.

**\*τροχεῖον** (τρόχιον?) rotella.

**τροχηλάτης** ἵππος currilis equus.

**τροχηλατῶ** roto.

**τροχίζω** roto (*et* -or).

**τροχιλλέα** *et* -ιλέα torquilla, tortula, trogala. *Cf.* troclea.

**\*τροχίλος** fulica, III, 258, 20 s. *interpr.*

**τρόχιον** *cf.* τροχεῖον.

**τροχίσκος** pastillus, rotulus, trochiscus. *Cf.* calamus, pilulas.

**τρόχμαλα (?)** *cf.* trogala (*expl.* χάλιξ).

**τροχός** rota, *cf.* trochus (*expl.* rotae genus ad lusum). *Cf.*
ἀγκύριον, κανθός, κνημός, ὄχημα, χάραξις, χοινικίς.

**τρύβλιον** *et* **τρούβλιον** parapsida *sim.*, *cf.* trublium.

**\*τρυγάνη** ἡ τὸν σῖτον ἀλοῶσα tribula (*ibidem* τρικάνη). *Cf.*
τυκάνη.

**τρύγη** vindemia (*et* -ae).

**τρυγήσιμος** vindemialis.

**\*τρυγητήριον** torcular.

**τρυγητής** vindemiator.

**\*τρυγητικός** vindemialis.

**τρύγητος** vindemia (*et* -ae).

**τρυγία** faex, - γάρου fraces (frax), - ἐλαίου amurca, faex (faeces),
fraces, - οἴνου faex.

**τρυγίας** faex.
