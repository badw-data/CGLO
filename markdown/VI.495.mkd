ritas *add. c d*); 604, 37; V 364, 5. of­fusio oculorum, nebula terrae
(?) V 298, 48. *Cf. Aldhelm. p.* 320.

**Glau\<co\>matici** *cf. Loewe GL. N.* 120.

**Glaucos** (glancos *codd.*) diuus marinus IV 443, 51; 597, 38 (*cf.
Serv. in Ecl.* VI 74). *V.* glaucus color.

**Glaucus** γλαῦκος ὁ ἰχθύς II 263, 21.

**Glaucus** equs ueluti pintos (!) oculos habens et quodam splendore
perfusos: nam glaucum ueteres dicunt \<splendiclum\> *Plac.* (*sub
finem*). albus et uiridis uel uiride, pressum IV 604, 35 (*cf. Serv. in*
*Georg.* IV 335). **glauca** alba uel uiridis IV 597, 39. uiridis IV
537, 10. flaua aut rubea IV 83, 7; 521, 50; V 458, 57. rubea, flaua V
502, 32. alba uirida (uiridia *f*; uiridis *de: cf. Verg. Georg.* II 13)
IV 443, 527 **glaucia** (!) uiridis IV 347, 8; 604, 36; V 502, 34
(uiridia); 600, 53. **glaucum** κυανοῦν. Virgilius lib. VIII (33):
glauco uelabat amicto (!) II 356, 18. χλοερόν II 477, 28; III 272, 19.
hauui (*vel rectius* heuui) uel grei (*AS.*) V 363, 25. **glauco**
uiridi, presso IV 242, 25; 587, 11 (*cf. Serv. plen. ad Verg. Georg.*
III 83). *V.* glaucia.

**Glaucus color** interdum pro uiridi ponitur et qui admixtum habet
uirori alborem. nam Virgilius hoc sciens 'glaucas salices' (*Georg.* IV
182) et 'oliuas glaucas' dixit (ulua *Aen.* VI 416. *cf. Verg. append.
catal.* II 9). item (nam *Maius*) in equis aut hominibus 'glauci oculi'
pro splendidi ponuntur, legimus nonnumquam et maris colorem glaucum
dici, sed tunc quando canescit fluctibus, unde **Glaucus** deus maris
senior fingitur a canitie fluctuum *Plac.* V 24, 1 = V 72, 4 (*cf.*
glaucus) = V 106, 22 = V *praef.* XIX. *Cf. Gell.* II 26, 17.

**Gleba** βῶλος γῆς II 260, 52. ἡ βῶλος, ὁ ὑμήν, ὁ βῶλος II 553, 10
(*Buecheler Arch.* I 289). βῶλος II 518, 48; 540, 49; III 199, 68; 261,
7; 449, 73; 469, 41; 476, 49; 490, 68. cespes durus IV 588, 35. sepes
(cespes *b*) durus cum herba IV 522, 33. cespis durus IV 83, 21; 242, 27
(cispis *cod. Sangall.*). cespis durus cum herba leuatur [glebo] IV
83, 8 (*secl. Nettleship 'Journ. of Phil.'* XIX 124: glebator glebo *W.*
*Heraeus Arch.* X 511; glebo *ad* 9 *spectare videtur.* leuatus?).
**gleba** [glea] cespes durus V 502, 36. terra pinguis et ponitur pro
pane V 502, 37. **glebam** cespitem durum IV 604, 40. cespitem V 419,
63; 428, 49 (*Euseb. eccl. hist.* II 17). **glebae** βῶλοι III 356, 64.
**glebas** cispites duri V 298, 38. *V.* fecundi cespites, glebo.

**Glebalis summa** ἡ τῆς γλα\<ί\>βας ὁλκή III 481, 50.

**Glebo** βῶλος II 34, 12 (*tibi* gleba *Vulc.*). **glouo** (*corr.*
*c*) arator IV 83, 9. **glebo** arator V 205, 29. glebra arator lingua
gallica V 364, 8. **glebo** rusticus V 364, 10. rusticus, arator IV 83,
22; 242, 26; 588, 34; V 205, 30; 298, 39; 600, 40. rusticus arator aut
arator IV 521, 40. rusticus arator, stiuarius IV 604, 39. arator
rusticus IV 347, 11; 597, 40. *Cf.* gleba.

**Glebula** βωλάριον II 260, 49; III 261, 9.

**Glepas** (? χλεύας *Buech.*) dissensiones V 548, 16.

**Glicissida** *v.* hastula quae nocte lucet.

**Glidium** παγῶδες II 34, 18 (gelidum? *ubi* glandium *Vulc.*).

**Glimerit** προστίθησιν II 34, 17 (*ubi* glomerat *Vulc. probabiliter.*
glumeritio *c.* προστίθησις *e*).

**Glirius** somnolentus *Scal.* V 601, 6 (*Osb. p.* 259). **glirium**
torpentem, stupidum V 614, 32.

**Glis** μυοξός II 507, 34 (*cf. GR. L.* I 546, 22). **glis guris** (=
gliris) μυοξὸς ὁ μῦς ὁ εἰς τὰ δένδρα II 374, 4. **glis** ὁ μυοξός II
518, 52; III 259, 40. egilae (*vel* eglae, *AS.*) V 363, 20. **glis**
nomina\<ti\>bus V 642, 24 (*Non.* 119, 20). **gliris** (*GR. L.* I 42,
3) μυοξός III 18, 58; 189, 44; 431, 48. μυωξός III 469, 42. **glires**
μυωξοί III 469, 43. *V.* glix, cliris, glitis.

**Glis glitis** humus tenax *Scal.* V 601, 7 (*cf. Osb.* 259, 264 *et
Loewe GL. N.* 120; *AHD. GL.* III 241, 17).

**Glisco** αὐξάνω II 251, 10. αὐξάνομαι II 251, 9. αὔξω II 251, 16.
προβαίνω ὅ ἐστιν αὔξω II 416, 22. **gliscit** αὔξει II 34, 19. increscit
IV 347, 12. ardescit aut crescit IV 522, 41. ascendit (accenditur
*Nettleship 'Journ. of. Phil.'* XVII 121) V 364, 11. pinguescit, crescit
IV 589, 7. ardescit, increscit V 205, 34. crescit, pinguescit IV 604,
41. ardescit IV 83, 12 (*Verg. Aen.* XII 9). crescit V 298, 37.
**cliscit** crescit V 278, 9. **gliscet** crescet V 205, 33. **cliscet**
crescet IV 216, 35 (*Serv. in Aen.* XII 9). **glescit** pinguescit,
crescit IV 347, 10. pinguescit, crescit, increscit uel adolescit IV 597,
41. crescit IV 83, 10; V 205, 31. **clescit** crescit V 627, 6.
**clisset** criscet V 633, 27. **gliscit** crescit, au\<g\>mentatur aut
quod ex tota uirtute procedit V 205, 35. **gliscere** crescere IV 83,
11; 522, 40 (*cf. Isid.* XII 3, 6). **glescere** crescere, adolere IV
597, 50. *Cf. Festus Pauli p.* 98, 9; *Non.* 22, 10.
