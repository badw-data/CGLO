**ἀστεϊσμός** *cf.* astismos.

**ἀστείως** festive.

**ἄστεκτος** intolerabilis.

**ἀστέριον** astereon.

**ἀστερίσκος** signum. *Cf.* asteriscus.

**ἀστεροπή** marrubium.

**ἀστερωτός** stellatus.

**ἀστήρ** aster, astrum, sidus, stella (*cf. p.* 438), stilla marina,
terra Samia, - ἑωθινός, ἑωσφόρος, φωσφόρος lucifer. *Cf.* γῆς.

**\*ᾄστης** cantator, cantor.

**\*αστια** *cf.* ἑορτή.

**\*ᾀστικός** canorus.

**ᾀστικός** urbanus.

**ἁστίλιον (?)** origanum.

**ἄστομος** insulsus, vanus.

**ἄστοργος** inamabilis, sine affectione.

**ἀστός** civis.

**ἀστράβη** *cf.* astraba.

**ἀστραγάλεια** talare (-ia).

**ἀστραγαλῖνος** (ὁ στρουθός) carduelis, II 248, 46 *s. interpr.*

**ἀστραγαλίσκος** parra, taxillus, τοῖς -οις τοῦ χάρτου capitulariis.

**ἀστράγαλος** talus.

**ἀστραγαλωτόν** talare.

**ἀστραπεύς** fulgurator.

**ἀστραπή** coruscatio (*et* scor.), coruscum, dium, fulgor, fulgur,
fulguratio.

**\*ἀστραπίς** fulgur.

**\*ἀστραποειδής** coruscus.

**ἀστράπτω** corusco, fulgeo, fulgero, mico, -ει fulgurat. *Cf.* Ζεύς.

**ἀστρολογία** *cf.* astrologia.

**ἀστρολόγος** astrologus.

**ἄστρον** astrum, sidus, signum, stella, - Ὠρίωνος iugula.

**ἀστρονομία** *cf.* astronomia.

**\*ἀστροπλήξ** siderosus.

**ἄστυ** civitas, urbs. *Cf.* asty.

**ἀστυγείτων** confinis, proximus urbis.

**ἀστυνόμος** iecorus (= viocurus).

**ἀσυγγενής** sine cognatione, sine cognatis.

**ἀσυγκέραστος** intemperatus.

**ἀσύγκριτος** incomparabilis.

**ἀσυγκρότητος** sine sustentatione.

**ἀσυκοφάντητος** sine calumnia.

**ἄσυλος** intemeratus, -ον asylum.

**ἀσυμβούλευτος** inconsul, -ον insuasum.

**\*ἀσύμβουλος** inconsultus.

**ἀσύμμετρος** immoderatus.

**ἀσυμπάθεια** inclementia.

**ἀσυμπαθής** immitis, inclemens.

**ἀσύμφορος** incommodus, inutilis.

**ἀσυμφόρως** inutiliter.

**ἀσυμφωνία** inconsensus.

**ἀσύμφωνος** abhorrens, incongruens, incongruus, inconsonans,
inconveniens.

**ἀσυμφώνως** absurde.

**ἀσυνειδησία** inconscientia.

**ἀσυνείδητος** inconsciens.

**ἀσύνετος** imprudens (*cf.* impudicus), insensibilis.

**\*ἀσυνηγόρευτος:** πράγματος -ου re ignorata.

**ἀσυνήθεια** desuetudo.

**ἀσυνήθης** insolens, insuetus.

**ἀσύνθετος** incompositus.

**ἀσυνθέτως** incomposite.

**\*ἀσυνίστωρ** inconscius.

**ἀσύντακτος** indispositus.

**ἀσυντελής:** -ῆ χωρία immunes fundi.

**ἀσύντριπτος** inquassatus.

**ἀσύστατος** inconstans.

**ἀσύφηλος** absurdus.

**ἀσφάλαξ** *et* **ἀσπάλαξ** (μῦς ἀρουραῖος) mus caecus, talpa (*cf.*
asphalaga). *Cf.* σφάλαξ.

**ἀσφάλεια** cautela, cautio, diligentia, munitio, praesidium,
stabilitas, tuitio, tutela. *Cf.* ἐγγύη.

**ἀσφαλής** astutus, cautus, enixus, tutus.

**ἀσφαλίζομαι** caveo, communio, munio, tueor, tutor, -εται emunit;
ἠσφαλισμένος cautus, -ον encautum.

**ἀσφάλισμα** cautela, - πλοίου saburra, -ατα instrumentum (-a), III
277, 69 *s. interpr.*

**ἀσφάλτιον** polygonos, quinquefolium, trifolium.

**ἄσφαλτον** *et* **ἄσφαλτος** bitumen. *Cf.* aplustria.

**ἀσφαλῶς** certe, enim vero, tute *et* tuto.

**ἀσφόδελος** albucium, asphodelus, ebulus, hastula regia, II 249, 24
*s. interpr.*

**ἀσχημονήσας** deformatus.

**ἄσχημον** *cf.* aschemon (*expl.* inhonestum).

**ἀσχημοσύνη** foeditas, inhonestitas.

**ἀσχήμων** ignominiosus, inhonestus. *Cf.* aschemon.

**ἀσχολία** negotium, occupatio, -αι feriae.

**ἀσχολῶ** avoco, -οῦμαι occupo (-atus sum), otior, -ηθείς destrictus.

**ἀσώματος** *cf.* asomata (*expl.* incorporea).

**ἀσωτεύομαι** helluor, lustro, luxo, luxurior, prodigo.

**ἀσωτία** (*et* **ἀσωτεία**) ganeum, lascivia, luxum, luxuria, luxus,
nepotatus.

**ἄσωτος** balatro, exoletus, ganearius, helluo, lascivus, luxuriosus
(*cf.* asotus), prodigus, profusus, sumptuosus, λάθρα - nebulo. *Cf.*
κατωφερής.

**ἄτακτος** immoderatus, inordinatus, inquietus, lascivus, passivus,
petulans, tumultuosus.

**ἀτακτῶ** tumultuo.

**ἀτάκτως** inordinate.

**ἀταξία** enormitas, tumultus.

**ἀτάρ** at.

**ἀτάραχος** intrepidus.

**ἄταφος** inhumatus, insepultus.
