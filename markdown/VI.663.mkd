**Lustrabilis** περίβλεπτος II 402, 14. qui a multis desideratur (?) ut
rex II 586, 54.

**Lustrago** *v.* uerbena.

**Lustralis** πενταετηρικός II 401, 12. καθαρτικός II 334, 37; III 239,
18. τέλειος III 485, 19. **lustrale** πενταετηρικόν *post* II 125, 27.
quinquennale II 586, 56.

**Lustramentum** περικάθαρμα II 402, 53. purgatio II 586, 49. faetor,
putor, oletum, faetulentia *Scal.* V 603, 55 (*Osb.* 329).

**Lustrandum** uidendum IV 111, 35.

**Lustratio** καθαρμός II 125, 29; 334, 41; 495, 9; 541, 1; 553, 35; III
150, 45/46; 171, 6/5; 239, 11. ἁγνισμός II 216, 43.

**Lustrato stipite** circuito ligno V 368, 45.

**Lustrator** περιοδευτής II 403, 36; III 260, 55.

**Lustratum** circuitum IV 111, 25; 535, 36; V 308, 52. **lustrato**
inspecto aut uisitato IV 112, 19; 5 (specto); V 219, 36. **lustrata**
circuita V 554, 15. *V.* et per hostiam lustratum.

**Lustratus** luxuriosus V 643, 66 (*Non.* 135, 7).

**Lustro** uagator *Scal.* V 603, 1 (*Osb.* 327).

**Lustro** περινοστῶ II 403, 28; III 260, 56. περιέρχομαι II 402, 41.
κυκλεύω II 356, 30. ἀβοτεύομαι (ἀσωτεύομαι? *v.* luxo. *cf. Loewe Prodr.
p.* 275) III 485, 23. circumio IV 535, 37. circumspicio V 309, 12.
circuminspicio V 114, 26. **lustrat** περιέρχεται, περικαθαίρει II 125,
27. circuit IV 415, 19. circuit, peragrat IV 256, 12. peragrat IV 362,
28. perambulat, peragrat IV 535, 34; V 308, 45. circuit, peragrat uel
inuisit IV 112, 16. oculis circumspicit uel circumit IV 451, 2 (*Verg.
Aen.* I 453?). **lustrare** circuire, peragere (peragrare?) uel expiare
IV 111, 37. circuire IV 535, 35. *Cf. Serv. in Ecl.* V 75.

**Lustrum** φωλεός II 474, 18; 500, 50; III 260, 49. **lustra** φωλεοί
II 125, 26. latibula ferarum uel turpium hominum IV 362, 27; V 528, 3
(*cf. Serv. in Aen.* IV 151; *GR. L. suppl.* 276, 17). cubilia ferarum
V 308, 49 (*Isid.* XIV 8, 29). ferarum cubilia IV 111, 26; 535, 39.
ferarum cubicula (*vel* cubilia) IV 451, 1 (*Verg. Aen.* III 646/7:
*cf. Serv.*). cubilia aprorum in siluis IV 111, 17. **lustris** locis
abditis, in quae potandi libidinanclique causa secedebant *Plac.* V 29,
45 (libidinumque) = V 82, 12 = V 114, 31.

**Lustrum** πενταετηρίς, πενταετηρικόν II 125, 28. πενταετηρίς (*vel*
πεντετηρίς) II 401, 11 (*GR. L.* I 554, 18); 504, 20; 547, 56; III 242,
36; 455, 21; 476, 5. καθαρμός II 334, 41; 544, 15; III 54, 57; 56, 13;
107, 19; 496, 48. ἁγιασμός II 216, 24 (*GR. L.* I 553, 38; lustratio
*a*). quinquennium IV 111, 28 (unum lustrum facit annos quinque *add.
a*); 256, 11; 362, 30; V 114, 24. quinquennium tempus IV 535, 38.
quinquenni temporis IV 451, 3 (*Verg. Aen.* I 283); V 308, 47.
quinquennii tempus aut lumen IV 111, 16. quinquennium uel lumen V 572,
10. quinque (quinque annium *de:* quinquenne?) temporis partium
(spatium?), πεντετηρίς IV 415, 20. illuminatio V 369, 40; (*de lib. rot.
= Isid. de nat. rer.* VI 6) V 415, 43; 425, 16. **lustro** τῷ καθαρμῷ
III 56, 19; 107, 42. **lustrum** καθαρμόν III 107, 37. *V.* diem lustri.

**Lustrum conditur** καθαρμός κτίζεται III 55, 65/66; 56, 7/8; 107,
33/34. *Cf.* **lustro conditur** καθαρμῷ κτίζεται III 54, 56; 55, 52;
107, 18.

**Lusus** παίγνιον II 391, 57; 515, 57. παιδιὰ τὸ παίγνιον II 392, 11;
400, 18. παιδιά II 488, 62. παιγνιά II 538, 40; 550, 54. iocus,
cachinnus IV 362, 26. **lusum** παίγνιον II 125, 30. *V.* ad lusum.

**Lusus gratiam** (!) παιδιᾶς (παιδείας *cod.*) χάριν II 125, 31.

**Lusus manibulus** *v.* plausus.

**Luta de collo tauri** colotaurium III 558, 59.

**Luteolum** ἴον ξανθὸν ἤτοι κροκοειδές II 332, 37; III 266, 39.
κροκοειδές II 355, 35. **luteola** crocei coloris IV 110, 44; V 220, 3;
464, 16. crocei coloris, rubicunda V 506, 58.

**Luteres** conchae uel canthari aqua­rum IV 256, 14; V 506, 59. in
regnorum libro concae uel canthari aquarii, sed cantharus graecum est
nomen *lib. gloss.* (*= Eucher. instr. p.* 147, 10).

**Lutescit** lutea fit V 643, 53 (*Non.* 133, 23).

**Luteus** κροκοειδής III 272, 24. ὑπόχλωρος II 468, 35. croceus color
IV 535, 44. **lutea** κροκειδής II 355, 34. rosea IV 535, 42. rufea (?)
V 308, 42. rosea, rubea, rubicunda V 464, 15. rosea aut rubea IV 112, 20
(*Verg. Aen.* VII 26). **luteum** color croceus IV 362, 29; V 528, 4.
crocei coloris IV 535, 43; V 308, 46; 505, 43 (*v. Serv. in Aen.*
VII 26). *Cf. Wessner Comm. Ien.* VI 2, 101, 8; 127; *Isid.* XIX 28, 8.
*V.* lacteus.

**Lŭteus** πήλινος II 125, 33; 407, 25; III 455, 22; 478, 13. sordidus
IV 415, 25. **luteum** πήλινον III 322, 68. πήλινον, χρῶμα II 125, 32
(*contam.*).

**Lutifer** (-ci-?) est lutum (lucem?) ferens V 621, 10.

**Lutifex** *v.* coactiliarius.
