**Serraculum** πηδάλιον II 407, 17. *Cf. Cuiac. Observ.* XIV 13; *Not.
Tir.* 112, 73.

**Serrarius** πρίστης III 309, 14.

**Serrarius lapidarius** λιθοπρίστης II 360, 62.

**Serratorium** (= serat.) μοχλός III 23, 45.

**Serro** γριπός (γρυπός *e*) II 265, 17.

**Serta** *v.* sertum, corona.

**Sertat** στεφανοῖ [στέμματα *v.* sertum] II 182, 51 (*nisi* sertae
στέφανοι *subest*). *Cf. gloss. lat. ar. p.* 466 *Seyb.* (serto compono
coronas repleo).

**Serticulum** δρέπανον χορτοκοπικόν II 183, 2. serpic. *Salmas.*
serric. *De-Vit. Cf. Plin.* XVIII 177.

**Sertor** cultor IV 169, 31; 284, 10; 391, 10; V 149, 19; 331, 47; 636,
61. **sector** cultor *a post* IV 170, 3. *V.* sator, sector, sultor.

**Sertorium** qui nominat (sem.?) V 332, 6; 395, 6 (sent.).

**Sertorum nexibus retitum** (irr.?) coronatum, nodis circumdatum IV
423, 6.

**Sertula Campana** id est melilota III 576, 19 (μελίλωτος *apud
Hesychium est*).

**Sertum** στέφανος II 544, 38. corona est ex floribus facta IV 170, 17
(coronam---factam); 566, 18; V 332, 4. **serta sertum** στέφανος II 437,
38. **serta** στέφανος, στέμματα II 182, 50/51 (*v.* sertat). **steria**
(*corr. g.* seria *H. v.* series) ὁρμαθός II 188, 22. **serta** corona
reptilis (sertilis?) ex floribus facta IV 391, 9. corona florum IV 171,
3 (sertam *cod. Va­tic.*); V 149, 20. texta corona IV 566, 19; V 331, 40.
texta V 331, 44. στέμματα (singularia non habet) II 437, 5 (*GR. L.* I
34, 7). coronae ex floribus IV 283, 33. **sertae** (syptae *cod.*)
στέφανοι III 301, 54 (*corr. Boucherie*). **sertis** στέμμασιν II
183, 1. coronis IV 463, 42 (*Verg. Aen.* I 417? uictis uel ardua loca
siue rocce in mare *add. e f: quae ex contam. repe­tenda sunt:* sertis
uietis, Syrtes ardua *e. q. s. cf. Loewe GL. N.* 106). coronis
subtilibus (sertilibus? *v. supra*) IV 566, 21; V 331, 49. coniunctis
V 331, 54. coniugis (*vel* coniugiis) V 394, 55. floribus, coronis IV
566, 20. coronis V 390, 6. *V.* corona.

**Serula** (serola *codd.*) fisalidus (φυσαλίδος) III 563, 54; 595, 24
(fisalidus); 629, 25 (fisalidis). *V.* fistolidia.

**Serum** ὀρὸς τοῦ γάλακτος τῶν \<βοῶν\> II 183, 7. ὀρὸς γάλακτος
**seru** (-um *e*) indeclinabile est II 387, 22 (*GR. L.* I 31, 7; 36,
1; V 414, 14). **serum** ἀφρὸς γάλακτος III 315, 16. liquor casei V 331,
45. liquor care (casei?) IV 566, 10. liquor casei, id est huaeg (*vel*
huuaeg, *AS.*) V 394, 52. **seru** huaeg (*AS.*) V 393, 10. **serum**
est mesgus V 623, 18 (*cf. Osb.* 557 serum mesga. *v. Stokes apud
Fickium* II 215). *V.* seros.

**Serus** βραδύς II 259, 54. ὄψιμος III 244, 16 (*unde*?). tardus II
593, 8. **sera** ἑσπέρα II 315, 21 (*Arch.* V 466). βραδεῖα II 259, 51.
tarda V 456, 22 (fera *vel* feo *codd.*). **serum** post uesperum IV
463, 36 (*Verg. Aen.* VI 764; XII 864). **seram** tardam, nouissimam V
149, 13. **seras** tardas uel unde ianuae muniuntur IV 390, 56 (*v.*
sera 2). serotinas, se enim syllaba in huiusmodi nominibus producitur. †
almar (aliter?) enim serum ex lacte dicimus V 244, 19. **serius**
βράδιον II 182, 48. *V.* serius, tam sera, serum *= soir v.* Hesperus.

**Serua** δούλη II 280, 31; III 304, 72; 463, 57; 515, 1. παιδίσκη III
305, 1. ancilla IV 169, 45; 283, 39.

**Seruans** φυλάσσων καὶ φύλαξ II 183, 3. animo continens IV 463, 38
(*Verg. Aen.* I 36).

**Seruans aequi** φύλαξ τοῦ δικαίου II 183, 4. *Cf. Verg. Aen.* II 427.

**Seruatam** in custodiam positam IV 463, 39 (*Verg. Aen.* V 283).

**Seruator** φύλαξ II 473, 51. τηρητής II 455, 15. **seruatores**
τηρηταί III 417, 59.

**Seruatorium** φυλακτήριον II 473, 49. τηρητ\<ή\>ριον III 196, 48.
custodiatorium II 593, 2.

**Seruilis** δουλικός II 280, 32. δουλότροπος III 334, 30; 492, 41; 515,
9. **seruilis mores** (moris?) δουλότροπος III 334, 31. **seruilis
se\*\*\*\*** II 559, 29. **seruile** a seruo deriuatum IV 423, 4.

**Seruilitas** δουλεία II 280, 29.

**Seruio** δουλεύω II 280, 30; III 416, 72; 463, 53; 492, 59. λατρεύω II
358, 55. **seruit** δουλεύει II 183, 5; III 416, 74. **seruite**
δουλεύσατε III 416, 73. **seruiui** ἐδούλευσα III 416, 78.
**seruis\<ti\>** ἐδούλευσες (!) III 416, 77. **seruiuimus** ἐδουλεύσαμεν
III 416, 80. **seruierunt** ἐδούλευσαν III 417, 1.

**Seruitas (!)** infirmitas II 592, 59.

**Seruitium** δουλεία II 280, 29; 505, 34; 531, 60; 532, 13; 548, 42;
III 135, 32; 338, 6; 463, 54. multitudo seruorum uel ingeniorum
obsequium V 389, 53; 611, 1 (ingenuorum: recte?). *Cf. GR. L.* I 76, 16;
*suppl.* 276, 19.

**Seruitudo** δουλεία II 280, 29.

**Seruitum** seruitium IV 169, 52; 463, 43 (*cf. Verg. Aen.* II 786:
*an* seruītum in seruitium?).

**Seruitus** δουλεία, λατρεία II 183, 6. δουλεία II 280, 29; III 463,
55; 492, 62. condicionis nomen V 389, 52. *V.* in seruitutem redigo.
