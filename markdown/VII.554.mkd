**\*κατάπαις** pueralis, puerarius.

**καταπαλαίω** eluctor.

**καταπατῶ** conculco, inculco, proculco.

**καταπαύω** compesco, reparo, requiesco (requio), -παυσθήσεται
dissedabitur.

**\*καταπειράτης** *cf.* cataporates (*expl.* linea cum massa plumbea,
qua maris temptatur altitudo).

**καταπείρω:** καταπεπαρμένος confixus.

**καταπέλτης** ballista.

**καταπέμπω** demitto, -πεμφθείς directus. *Cf.* digero.

**καταπεπαίνω** commitigo.

**καταπέπτω** digero.

**καταπέτασμα** velum.

**καταπήγνυμι** *vel* -πήσσω defigo, καιαπεπηγός praefixum. *Cf.*
digero.

**καταπηδῶ** desilio, insilio.

**καταπήξ:** καταπῆγες οἱ έν ποταμῷ sublices.

**καταπήσσω** *v.* καταπήγνυμι.

**καταπιέζω** deprimo.

**καταπίνω** deduco, devoro, voro, - ὕδωρ imbibo.

**καταπίπτω** concido, decido, occubo, occumbo, procubo, κατέπεσεν
occĭdi (-it).

**καταπιστεύω** committo, confido.

**κατάπλασμα** fascimentum, *cf.* cataplasma (*expl.* medicamentum).

**καταπλέω** deveho (-or). *Cf.* κατάγω, καταγωγή.

**καταπληκτικός** torvus.

**κατάπληξις** stupor.

**καταπλήσσω** obstupeo, stupefacio, -ομαι obstupeo, obstupesco,
stupesco.

**κατάπλους** adventus navium, *cf.* cataplus.

**καταπλύνω** deluo, proluo.

**καταπνεόμενος** spirans.

**καταπολεμῶ** debello, oppugno.

**\*καταπολιός** canosus.

**καταπόνησις** defatigatio, delassatio, lassitudo.

**καταποντίζω** demergo, perundo, pessumdo, praecipito.

**καταποντισμοί** mensura (-ae).

**καταπονῶ** afficio, afflicto, affligo, conficio, confligo, fatigo,
profligo, vexo, πρὸς τὸ - εῖν ad fatigandum, -οῦμαι afflicto,
καταπονηθείς profligatus, vexatus, καταπεπονημένος afflictus, confectus,
defatigatus, defectus *et* defessus, lassatus, profligatus.

**\*καταπορθῶ** devasto.

**\*καταπορίζω** remeo.

**καταπορνεύω** fornico.

**\*καταπόσια** ludi floralis.

**κατάποσις** vorago.

**καταπότης** devorator, glutto, helluo, vorator, vorax.

**καταπότια** pilulas. *Cf.* catapota.

**καταπράϋνσις** mitigatio.

**καταπραΰνω** compesco, lenio, mansuefacio, mitigo, mollio, mulceo,
permulceo, sedo.

**καταπτοῶ** deterreo, perterreo, proterreo.

**κατάπτυσμα** putacilla.

**κατάπτυστος** contemptibilis.

**κατάπτωσις** ruina.

**κατάπυγος** depugis.

**καταπύγων** depugis.

**κατάρα** devotio, exsecratio, maledictio, -αι dirae.

**καταράσσω** *cf.* diverbero.

**κατάρατος** derectarius, devotus, exsecrabilis, maledictus.

**καταργῶ** avoco, intrico.

**\*κατάρδευτος** irriguus.

**καταριθμῶ** denumero.

**καταρπαγή** *v.* καθαρπαγή.

**καταρράκτης** cataracta, obex.

**καταρράπτω** consuo.

**\*κατάρρειθρον (?)** endoriguum.

**καταρρέπω** praepondero.

**καταρρέω** defluo, destillo, subsideo, τὰ -έοντα collapsus (-a).

**καταρρήγνυμι** *v.* χεῖλος.

**καταρητορεύω** peroro.

**κατάρρους** catarrhus, destillatio, gravido.

**καταρρυπῶ:** κατερρυπωμένος destitutus.

**κατάρρυτος** delibutus 1., irriguus.

**καταρτίζω** conficio, construo, instruo, perficio, struo,
κατηρτισμένος *etc.* aptata, confectus, constructus, instructus.

**κατάρτιος** arbor.

**καταρτίσματα** instructus (-a).

**κατάρτυσις** confectio.

**καταρχἡ** inceptio, origo.

**κατάρχομαι** inchoo.

**καταρῶμαι** devoveo, exsecror, imprecor, insecro (-or), κεκαταραμένος
inexsecrabilis, maledictus (κεκατηρ.).

**κατασβεννύω** exstinguo.

**κατασκάπτω** confodio.

**κατασκαφή** eversio, excidium.

**κατασκευάζω** colloco, comparo, condo, conficio, conflo, construo,
fabrico, struo, - ὅ ἐστιν ἐπιβουλεύω concinno. *Cf.* ποιητός.

**κατασκεύασμα** *v.* ποίημα.

**κατασκευαστής** instructor, - ὁ κακοπράγμων calumniator, concinnator.
*Cf.* ποιητής.

**κατασκευή** confectio, factura, supellex, *cf.* catasceue (*expl.*
destructio, instructio), - οἰκοδομῆς exstructio. *Cf.* ποίησις.

**κατάσκιος** amoenus, opacus, umbrosus.

**κατασκοπή** speculatio, specus.

**κατασκόπησις** exploratio.

**κατάσκοπος** explorator, speculator.
