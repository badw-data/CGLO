498, 37; 528, 21. χρυσίον II 27, 18; 478, 62. *V.* zaab.

**Aurum coronarium** quia imperatoribus coronatis offerebatur V 562, 8
(*cf. Serv. in Aen.* VIII, 721).

**Aurum signatum** χρυσοῦν νόμισμα II 479, 8.

**Aurunca** Italia V 652, 34 (*Iuvenal.* I 20; *cf.* II 100 et *Serv.
in Aen.* VII 206). ciuitas Hispaniae V 652, 35: *ubi* Asturia *Wirz*
(*cf. Iuvenal.* III 212), Hesperiae *H.*

**Aurunci** οἰκήτορες Ἰταλίας II 27, 11.

**Aurunculus** *v.* aurunculeius.

**Auruncus** θεὸς ἀποτρόπαιος II 27, 15; 327, 37. **Aruncus** deus fugae
II 568, 20 = 569, 4 (*cf. GL. N. praef. p.* XII). *Cf. Loewe Prodr.*
365. *V.* auerruncus.

**Auscul[t]ari** pro osculari quod est os cum ore conferre V 562, 9
(*cf. Festus Pauli p.* 28, 9).

**Ausculatus** osculatus *Plac.* V 7, 42 = V 49, 7. *Cf. Festus Pauli
p.* 28, 9.

**Auscultatio** ἀκρόασις II 223, 52.

**Auscultator** ἐπακροατής II 305, 27. κατακουστής II 341, 28.

**Ausculto** ἀκούω II 223, 30. **ausculto, auscultor** ἐπακροῶμαι II
305, 26. **ausculto** κατακροῶμαι II 341, 38. **auscultat** ἀκροᾶται,
ὠτακουστεῖ II 27, 20. **auscultatur** ἐπακροᾶται II 27, 21.
**auscultat** aduertit intente IV 21, 43; 302, 16 (abscultat: *cf.
Roensch Coll. phil.* 311); V 169, 10 (asc.). aduertit, intendit IV 485,
36. **auscultem** obtemperem V 529, 11 (*Ter. Andr.* 209).
**ausculta** audi, animaduerte V 530, 25 (*Ter. Andr.* 536).
**auscultare** (auscultari *G*) parere, obsequi, obedire *Plac.* V 7, 9
= V 49, 8.

**Auser** est proprium nomen fluminis (*Gregor, dial.* III 9) V 423, 23.

**Auson** *v.* ab Ausonio.

**Ausonia** Italia dicta IV 23, 11. Italia IV 486, 16; V 269, 57.

**Ausonium** Italicum IV 406, 45; V 492, 18. *Adde* **Auxinium** Romanum
uel Latinum IV 486, 24; V 270, 4; V 590, 14. *Cf. Loewe Prodr.* 424.

**Auspex** οἰωνοσκόπος II 27, 24; 381, 27. pronubus, παράνυμφος II 27,
31 (*cf. Iuvenal. ed. Friedlaender p.* 108). anibus augurians utrum
dextera an sinistra sint II 569, 16. qui auspicia exercet V 338, 8. qui
auium augurium exercet (*vel* intendit) V 338, 9. *Cf.* **arrux**
aruspex IV 20, 25 (*quamquam* ariolus *potius Nettleship* ῾*Journ. of
Phil'* XIX 115; Arruns *H. coll. Verg.* XI 759). **auspice** auctore,
fautrice II 27, 25 (*Horat. ep.* I 3, 13). **auspices** οἰωνοσκόποι II
27, 29. **hauspices** aruspices, diuini V 107, 32. *V.* haruspex.

**Auspicalis** οἰωνοσκοπικός II 381, 28. ὀρνεοσκόπος II 387, 11.
οἰωνιστικός II 27, 30.

**Auspicantes** initiantes V 543, 8. **auspicantia** inchoantia V 269,
50.

**Auspicato procedit** V 660, 12.

**Auspicatus** εὐοιώνιστος II 318, 44. adorsus IV 22, 61. adorsus,
incipiens uel incipit IV 485, 50. *V.* haud auspicato.

**Auspicium** οἰωνοσκοπία II 27, 27; 381, 26; 528, 52. ὀρνεοσκοπία II
502, 52. οἰωνός II 525, 26. οἰωνισμός, σύμβολον, ἀρχή II 27, 26. initium
actionis IV 406, 46; V 342, 10. auis signum uel initium mensuum (!) V
170, 3. est initium alicuius rei, quo primo sumit exordium, id est
inchoatur ut fiat, est et (et est *G*) auspicium quod aues uel animalia
(aues animalibus *R*) paganis auguribus (auribus *G*) demonstrant, unde
futura noscuntur, sunt igitur bona auspicia quae cogunt res inchoare,
sunt mala quae prohibent *Plac.* V 4, 5 = 49, 10 = *praef.* XVI
(*contracta*). initium cuiuscumque rei IV 312, 38. id est ab
inspiciendo auis nuntium, quod in aue aspicitur uel uotum uel augurium V
269, 40. auis nuntium, quod in aue aspiciatur IV 209, 32. dispositio,
augurium V 549, 44. **auspicio** principio IV 485, 51 (*v.* bono
auspicio). **auspicia** sunt quae iter facientes obseruant. dicta sunt
autem auspicia quasi auium ospicia. duo sunt autem genera auspiciorum:
unum ad oculos, alterum ad aures pertinen[te]s, ad oculos scilicet
uolatus, ad aures uox auium V 169, 43. signa quod (!) per aues
ostenditur quasi auigeria V 170, 1 (*cf.* augurium). portenta, prodigia
IV 24, 2. auguria V 269, 49. somnia IV 209, 8. signa uel initia,
auguria, somnia IV 485, 58. cantiones (*vel* cantationes) auium V 340,
57. **auspiciis** οἰωνοσκοπίοις II 27, 22. in[d]iciis uel potestate IV
23, 60. [in] initiis uel potestate V 170, 2. potestate IV 431, 29
(auspicit). qui (quia?) aues inspiciunt uel homines obuiantes (*Euseb.
eccl. hist.* IV 26) V 418, 34; 427, 4. **arit** auis signum (*cont. ex*
haurit aperit; auspicium a. s.: *cf. c*) IV 22, 58. *V.* ducto auspicio.
*Cf. Isid.* VIII 9, 19.

**Auspicor** οἰωνίζομαι II 381, 31; III 238, 13. ὀρνεοσκοπῶ II 387, 12.
**auspicatur** ἀπάρχεται, ἄρχεται II 27, 23. incipit IV 22, 59.
**auspicari** somnia inquirere IV 209, 44. **auspicare** requirere IV
312, 37. **auspicati (*vel* auspicei) sunt** consecuti sunt IV 209,
9.

**Auster** νότος II 27, 33; 377, 12; III 11, 21; 295, 20; 354, 13; 395,
70; 400, 56;
