**Expilatam** aritrid (? *AS.* ārýhid *Sievers*) V 357, 60.

**Expilatio** γύμνωσις III 447, 66; 480, 29. furtum sacrilegorum II 579,
9.

**Expilatores** alienae hereditatis subreptores IV 339, 25; V 292, 36;
598, 34 alienae hereditatis, subuersores *Plac.* V 67, 20; IV 70, 26;
512, 9; V 454, 46. alienae hereditatis subtractores (*vel* subreptores)
IV 235, 7.

**Expilo** ἀποσυλῶ II 241, 10. συλῶ II 441, 39. **expilat** ἀποδύει,
λῃστεύει, συλᾷ, λυμαίνεται, ἐκπορθεῖ, τοιχωρυχεῖ II 65, 58. occulte
exterminat V 499, 52. occulte exterminat uel elapidat IV 339, 24.
occulte exterminat [seu purgat] V 454, 45 (*cf.* expio).

**Expio** ἐξιλάσκομαι II 303, 34. ἐξιλεοῦμαι II 303, 35. ἐξευμενίζομαι
II 303, 13. ἀποτροπιάζω II 242, 7. mundo V 618, 48. **expiat**
ἀποτροπιάζει, ἐξιλε\<ο\>ῦται, ἐξευμενίζεται II 65, 55. abluit, mundat IV
66, 46; V 292, 29 (exdat). purgat, mundat IV 234, 12. abluit, purgat,
mundat, exorat IV 339, 21. **expiare** emundare IV 67, 30. propitiare V
293, 2. **expiabat** emundabat V 292, 19. exigebat (*v.* exposcit) IV
71, 55; 512, 39. **expiatur** uindicatur IV 339, 23. subplicium (-io?)
purgatur IV 512, 40 (*v.* expiatus). **expiari** emundari V 292, 18.

**Expiscabar** capiebam (*Euseb. eccl. hist.* III 39) V 420, 24 = 429,
7.

**Explacato** uehementer placato V 196, 39.

**Explanatio** ἔκθεσις II 290, 16.

**Explano** διασαφῶ II 274, 3. σαφηνίζω II 430, 10. **explanat**
σαφηνίζει, διασαφεῖ II 66, 1. edisserit IV 441, 25. exaequat IV 67, 1.
ab iracundia exiluit IV 339, 26 (explacatur *Hildebrand; v.*
excandesco).

**Explebilis** πληρωτικός II 410, 2. complens II 579, 14.

**Explen explenis** palatium V 619, 17 (splen splenis?).

**Expleo** ἐκπληρῶ II 292, 12. **explet** ἀναπληροῖ II 66, 2. perficit
IV 235, 42. **expleam** abundare faciam V 533, 29 (*Ter. Andr.* 339).
**explere** finire aut satiare IV 68, 22 (*Serv. in Aen.* VI. 545).
**expleri** repleri IV 339, 27.

**Expletio** satisfactio IV 339, 28.

**Expletiuum** παραπληρωματικόν II 66, 5.

**Explicabilis** perfector omni\<s\> rei V 454, 48. patrator, perfector
V 499, 53.

**Explicatio** εὐλύτωσις II 318, 20. ἐκπλοκή II 292, 15. ἀποπλήρωσις II
239, 54.

**Explicit** ad librum refertur, **explicuit** autem et **explicauit**
ad hominem, ut si dicamus 'explicit liber', 'explicuit homo opus suum'
*Plac.* V 20, 27 = V 67, 21. **explicit** (sermo) ἐπληρώθη (λόγος) III
166, 9. **explicit** apertiste (ἀπήρτισται) III 405, 75. *Cf. Keller
'Volkset.' p.* 147.

**Explicitus** expeditus IV 69, 22; V 454, 49; 634, 17.

**Explico** ἀπευλυτῶ II 234, 52. ἀποπληρῶ II 239, 53. εὐλυτῶ II 318, 19.
ἐκπλέκω III 141, 19. **explicat** ἀπαλλάσσει II 66, 4. **explicant**
ἐκπλέκουσιν III 44, 12; 97, 4. **explicabo** narrabo IV 71, 37 (*v.*
exemplicabo). **explicuimus** ἐξεπλέξαμεν III 56, 41. **explicassem**
ἐξεπλεξάμην III 212, 57 = 228, 53 (ἐξέπλησα ἄν expleuissem) = 649, 5
(*cf.* III *praef. p.* XXXV). **explicantur** V 413, 26. **explicatum
est** ἐκπλέκει III 114, 26 = 643, 23 (*cf. Ind. Ien. a.* 1892 *p.* 16).

**Explodens** elidens IV 514, 19; V 196, 40; 291, 44; 628, 48.

**Explodit** expellit, uituperat IV 234, 35. uituperat IV 339, 30.
excludit V 358, 49 (*cf. Roensch Coll. phil.* 53). **explodere**
excludere IV 513, 39. **exprodere** excludere IV 71, 12. **exploderem**
(*vel* expr.) excluderem IV 66, 34; V 292, 20. **exproderem** excluderem
V 196, 44. **explodam** euertam IV 235, 12.

**Explodita** exclusa IV 234, 53; V 499, 54. **exprodita** exclusa IV
66, 41; 339, 40; V 196, 45; 292, 26; 597, 66. *V.* explosus.

**Explorandum** *v.* ad explorandum.

**Explorans (explosas *GR. L.* VII 427, 9) comoedias** V 661, 50.

**Exploratio** κατασκόπησις II 343, 54.

**Explorator** κατάσκοπος II 66. 6; 343, 53; III 353, 8. speculator IV
339, 31. inquisitor V 196, 41. **exploratores** inquisitores IV 234, 14.

**Exploratus** ἀπευλυτωμένος II 234, 51 (*v.* exploro, explosus).
secretus, oportunus IV 69, 53. **explorata** exquisita, conperta V 533,
59 (*Ter. Eun.* 603). **explorato** probato et praemitato
(praemeditato?) V 547, 45.

**Exploro** ἀπευλυτῶ (*v.* expedio; an explodo? *v.* explosus) II 234,
52. κατασκοπῶ II 343, 55. περισκοπῶ II 404, 28. **explorat** ἐκζητεῖ II
66, 9. auscultat, inquirit IV 66, 47; 513, 37. inquirit uel auscultat IV
339, 29. **explorare** exquirere IV 441, 26; 513, 36 (*Verg. Aen.* I
77). **exploramur** σκοπούμεθα II 66, 7.

**Explosus** ἐκβεβλημένος II 288, 62. ἀπευλυτωμένος II 234, 51. ἐκριφείς
II 292, 35. exutus (expulsus?) IV 71, 13. excussus V 196, 42. eiectus
aut exclusus uel foris missus V 196, 43. **explosa**
