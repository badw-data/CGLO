iubet ac sanctum facit IV 422, 35. confirmat, decernit, statuit, dicit
IV 388, 43. statuit, confirmat IV 281, 36. statuit aut sanctum facit aut
condit aut dicit IV 166, 13. statuit aut sanctum facit aut condit aut
dicit aut decernit IV 564, 9. confirmat *Plac.* V 97, 21. dicit, statuit
V 330, 22; 394, 19. considerat V 330, 29. statuit, con[ci]dit, sanctum
facit V 146, 19. **sancire** indicare, confirmare IV 564, 11. confirmare
V 330, 13. **sanxit** consecrauit, iussit IV 388, 48. definiuit,
diiudicauit IV 281, 40. iussit uel definiuit IV 166, 14. iussit aut
tribuit uel iudicauit siue definiuit IV 564, 10; V 412, 12. tribuit,
definiuit uel iudicauit uel iussit V 329, 57. iussit V 146, 20. tribuit
V 394, 20. **sanciuit** statuit V 146, 18. *Cf. Serv. in Aen.* XII 200.

**Sancio legem** νομοθετῶ II 376, 65; III 276, 29. **sancit legem**
κυροῖ νόμον \<δι'\> ὅρκου II 178, 16 (*add. Buech.*).

**Sancta scriptura** ἁγιόγραφα II 554, 8 (*margo*).

**Sancta trinitas** ἁγία τριάς III 278, 20 (*unde*?).

**Sancti dies** ἅγιαι ἡμέραι III 295, 7; 490, 4; 506, 48.

**Sanctificatio** ἁγιασμός II 216, 24; 495, 34. ἁγίασμα II 216, 23.
ἁγιωσύνη II 216, 27.

**Sanctificetur nomen tuum** ἀγιασθήτω τὸ ὄνομά σου III 507, 19
(*Matth.* VI 9).

**Sanctifico** ἁγιάζω II 216, 22. **sanctificat** purificat IV 281, 37.

**Sanctimonium** ἁγίασμα II 553, 46. ἁγιασμός II 216, 24; 501, 36.
ἁγιότης II 216, 26. **sanctimonia** ὁσιότης II 388, 8. ἁγιότης II 216,
26. σεβασμιότης II 430, 19.

**Sanctio** νομοθεσία II 376, 64: III 276, 30. **sanctiones**
indicationes uel difinitiones V 412, 9 (*de canon. concil.*).

**Sanctitas** ὁσιότης II 388, 8. ἁγιωσύνη II 216, 27. ἁγιότης II 216,
26.

**Sanctitudo** cognitio, statutio II 592, 2. sanctitas V 647, 45 (*Non.*
173, 26).

**Sanctuarium** θυσιαστήριον III 238, 38 (*unde*?). locus uel cubiculum
ubi sanctae res geruntur *Plac.* V 42, 6 = V 97, 22 (cubiculus) = V 146,
24 (*item*). *V.* promptuarium.

**Sanctus** ὅσιος, ἅγιος II 178, 19. ἁγνός, ἅγιος III 330, 58. ἅγιος II
216, 25. ὅσιος II 388, 7; III 417, 68; 472, 62; 506, 47. sacratissimus
IV 463, 6 (*Verg. Aen.* I 426?). a ueteri consuetudine appellatus eo
quod hi qui purificari uolebant, sanguine hostiae tangebantur (agebantur
*codd i*) \<et\> ex hoc sanctionem (sancti nomen *Isid.*) acceperunt V
242, 20 (*cf. Isid.* X 241; *Serv. in Aen.* XII 200). **sancta** ἱερά II
178, 29; III 418, 26 sancta, purchra (!) III 509, 61. **sanctum** ἁγνόν,
κεκυρωμένον, ἅγιον II 178, 18. ἁγνόν III 302, 11. ἅγιον III 170, 35.
ἱερόν III 75, 67; 146, 70. θεῖον, ἄγιον III 171, 29. θεῖον III 238, 40.
ὅσιον III 238, 39. θυσία III 418, 27. augustum, uenerabile, religiosum
IV 388, 44. canum, pulchrum, candidum IV 388, 45. diuinum, cultu
consecratum IV 281, 39. **sanctae** τῆς ἁγίας III 423, 1. **sanctam**
ἁγίαν III 508, 19. **sanctissimus** ἱερώτατος, θειότατος, ὁσιώτατος II
178, 17. ἁγιώτατος II 216, 28. *V.* scriptura s.

**Sandalia** calciamenta quae non habent desuper corium V 391, 37.

**Sandapila** νεκροφόριον II 178, 22. feretrum uel loculum, in quo
defuncti portantur: sic enim de Domitiano imperatore legimus (*Oros.*
VII 10) 'cuius cadauer populari sandapila per uespelliones reportatum
(exp. *Oros. et Suet. Dom.* 17) atque ignominiose sepultum est' *Plac.*
V 97, 23. feretrum V 654, 21 (*Iuvenal.* VIII 175); 585, 24 (sind.);
623, 33 (send.). feretrum, id est locus ubi portantur gladiatores
mortui, non in quo nobilium corpora sed in quo plebeiorum atque
damnatorum V 578, 28. feretrum, quo mortuorum corpora ad sepeliundum
eiciuntur pauperum sine ditiori apparatu, dicta sandapila quasi sine
dapila II *p.* XII. ubi portantur gladiatores IV 281, 43. *Cf. Wessner
Comm. Ien.* VI 2 *p.* 88, 6; 103; *Roensch Coll. phil. p.* 289.

**Sandapilo** νεκροθάπτης II 178, 21 (sandapelo).

**Sandara** Cappadocum lingua Martius mensis dicitur V 242, 21, *V.*
menses.

**Sandaraca** χρυσίτης III 194, 49; 273, 74. genus coloris IV 166, 18;
564, 17; V 481, 17 (*cf. Fest. Pauli* 324, 6; *Isid.* XIX 17, 12). auri
pigmentus mundus IV 281, 42. pi\<g\>menta orientalis III 594, 62; 628,
48. pigmentum auri in robore (= rubore; *cf. Diosc.* V 121) III 577, 28.
auropigmenta in robore III 586, 15.

**Sandocus** (= Santonicus) *v.* absinthium.

**Sandonicum** θηριοκτόν\<ον\> (*add. e*) βοτάνη II 178, 23. *V.*
santonica. *Cf. Iuvenal. ed. Friedlaender praef. p.* CX; *Hermae vol.*
XXXIII *p.* 413.

**Sandyx** βόμβυξ II 258, 42; III 256, 63 (*in cap. de sec. mensa, inter
pisces*). fuci genus est V 330, 38. frugi (!) genus V 394, 37. uueard
(uuad *Sievers, AS.*) V 391, 24. herba apta tincturae, quam uulgus
uuarantiam uocant II *p.* XIII.
