III 24, 30; *sat.* I, 64) V 623, 1. quo tenore V 477, 50. *Cf. Arch.* V
400.

**Quater** τετράκις II 454, 10.

**Quaternio** τετράδιον II 454, 4; 516, 32. τετράς II *praef. p.*
XXXVII; 454, 26; 513, 10. *Cf.* **quaternio** (*uel* quaterno) quatern
(*AS.*) V 386, 3.

**Quatio** concutio IV 276, 33. **quatit** σείει, τινάσσει II 166, 48.
concutit IV 157, 15; 460, 46 (*gl. Verg.*); V 139, 30; 326, 35. concutit
uel commouet IV 157, 49; 383, 26 (*cf. Serv. in Aen.* II 611). **cutit**
mouet, concutit, cillet (!) V 283, 11. **quatere** peruertere IV 421, 9.
percutere, contundere (*uel* tundere) IV 383, 25. **quatare** commonere
V 326, 21. **quatitur** concutitur IV 157, 13; 558, 52; V 326, 29; 386,
8. concutitur \[ue coniunctio\] IV 383, 27 (*v.* que). *Cf. Festus p.*
261, 15.

**Quattuor** τέσσαρες II 453, 35. τέσσαρα *b e post* IV 166, 42. τὰ
τέσσαρα III 426, 29. **quattu\[o\]r** latinum \<non\> est, sed quattuor
V 326, 34. *Cf. Loewe Prodr.* 423, *GL. N.* 146. **quattuor** quattuor
species diuidit (diuidunt *codd.*) Virgilius boum (bonum *codd.*):
maritos gregis, uictimas, aratores, armentiuos, et equorum quattuor:
currules, dorifarios (dorsuarios?), admissarios, armentiuos V 238, 3.

**Qua uectus** qua portatus IV 157, 21; 460, 47 (*Verg. Aen.* I 121).

**Quaxillus** *v.* quasillus.

**Que** τέ, καί II 166, 49. τέ II 452, 27. καί II 335, 50. coniunctio IV
383, 27 (*cf. GR. L. suppl.* 275, 4). ue \[etsi licet\] IV 383, 38.
*Cf.* etsi, quatio.

**Quem ad finem** quatenus IV 383, 29.

**Quemadmodum** τίνα τρόπον II 166, 51. ὃν τρόπον II 384, 30. πῶς ἄν II
427, 3. quomodo, sicut IV 276, 55. sicut V 140, 27.

**Queo** δύναμαι II 166, 57; 281, 28. possum IV 157, 30; 558, 59; V 140,
30. posso IV 383, 31 (corr. *b*). ualeo, posso (*uel* possum) IV 277, 1.
**quis** potes V 327, 4. **quimus** possumus V 539, 4 (*Ter. Andr.*
805). **queunt** possunt IV 157, 44; 277, 6; 383, 39; 558, 60; V 140,
31. **queam** possim IV 158, 9; 383, 28; V 140, 14; 538, 68 (*Ter.
Andr.* 270). **queat** possit *a post* IV 158, 10; V 140, 19 (*Ter.
Andr.* 394). **queamus** possimus IV 157, 31; 558, 61; V 140, 4.
**queant** possint IV 383, 41. **quiui** potui IV 159, 7; 559, 56; V
140, 52; 327, 11. potui, consensi (*ad* quieui?) IV 277, 7. **quiuit**
ἠδυνήθη II 167, 52. potuit IV 559, 57; V 327, 1; 478, 1; 539, 17; 577,
21. **quiuerunt** potuerunt IV 159, 8; 277, 21; 559, 58; V 529, 20.
**quiueris** potueris V 529, 22. **quiuerit** (*uel* queuerit) potuerit
IV 383, 40; V 478, 2 (querit); 577, 20. **queuerint** potuerint IV 276,
51. **quiuissent** potuissent V 326, 39. **queuero** potuero V 512, 8.
*V.* quis, non queo.

**Quercetum** δρυμών II 281, 13. **questetum** dirmon II 527, 19 (*corr.
a e*).

**Quercinus** δρύϊνος II 281, 11.

**Quercus** δρῦς II 166, 58; 281, 15; III 264, 40; 358, 35 (quercum);
64; 397, 24. diris id est **quercu** III 538, 27. diris **querco** sunt,
ilice III 545, 28. drius id est arbor rubor (= robur) uel **querquus**
III 623, 11. **quercus** πρῖνος III 428, 37. ilicis similis an ilex *a
post* IV 157, 48. genus ligni IV 559, 25; V 326, 47. arbus V 140, 17.
*V.* aeriae quercus, quernum, ramnus, lingua clandis.

**Querela** μέμψις ματαία II 166, 59. μέμψις III 339, 33; 461, 76.
**querella** μέμψις II 367, 42; III 77, 26. αἰτία II 221, 20.
**quaerelia** questibus uel clamor IV 157, 39; 559, 8. **quaerilla**
quae habet quaestionem IV 421, 14. culpae accusatio IV 276, 44.
**querella** accusatio IV 383, 32. **querela** est uox (mox *cod.*) muta
(?) II 591, 3. **quaelellae** accusationes V 326, 44. *V.* sine
querella.

**Querellantem** querellas afferentem IV 460, 37 (*cf. Serv. in Ecl.
prooem. p.* 3, 11). **querelantem** querelas afferentem *Scal.* V 609,
8.

**Querellator** *v.* quiritator.

**Querellatus** questus IV 383, 33. *V.* quiritat.

**Querens** quaerulans (= querellans) uel deprecans IV 157, 50; 460, 38
(*cf. Verg. Aen.* I 385).

**Quereta** *v.* quae fata.

**Querimonia** μέμψις II 166, 61; 367, 42. αἰτία II 221, 20. frequens
querella IV 383, 34. grauis querella uel accusatio V 326, 31.
\[maleficus arteficus\] contestatio V 477, 51. querella aut quaestio IV
559, 5. *Cf.* **quaerimonis** (*uel* -iis) grauis quaerela V 386, 9.
**quaerimoniam** querellam grauem (*can. conc. Carth.* 11) V 411, 49.
**quaeremoniae** quaerellae IV 559, 7. quaerellae graues IV 157, 37
(quaerim.); 559, 6. accusationes V 386, 17 (quaerim.). **quaerimoniae**
querellae graues V 140, 9. *V.* caerimonia. *Cf. GR. L.* VII 523, 3.

**Querimonia puplica** querellae in publico prouulgatae IV 421, 15.
**caerimonia puplica** scripta puplica IV 407, 44.

**Querito** *v.* quiritat.
