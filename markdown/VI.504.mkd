**Grauidauit** impleuit V 642, 13 (*Non.* 118, 9).

**Grauidinosi** (grandinos *cod.*) a morbo V 641, 76 (*Non.* 115, 16).

**Grauido** (-ed- *e*) κατάρρους II 36, 4.

**Grauipes** *v.* auis tarda.

**Grauis** βαρύς II 255, 60; III 129, 49; 330, 53. ἁδρός II 34, 61; 35,
60. στερρός II 437, 34. ἐπίτοκος II 312, 14. ἐγκύμων II 284, 10. ἔγκυος
II 284, 9. βαρεῖα II 35, 30; 255, 55. uenerabilis, unde et
contemptibiles leues dicimus (*cf. Serv. in Aen.* I 151; *Is.* X 112).
grauis pro consilio et substantia dictus, quia dissilit (desilet *a.*
qui haud des. *Buech.*), sed fida constantiae grauitate consistit IV
599, 8. **graue** βαρύ II 34, 60; 36, 1; 256, 2; III 73, 47. *Cf.*
**graue** βαρύν III 129, 48. **graue** στιβαρόν II 437, 56. ἐπαχθές II
306, 21. quod per naturam est ponderosum V 552, 28 (*v.* granatus).
**grauem** βαρύν II 35, 31. ponderosam, obrutam IV 347, 47; 599, 6.
**grauibus** fortibus, modestis IV 599, 7 (*Serv. in Aen.* V 114; X
207). **grauissimus** στερρότατος II 437, 35. **grauissima** βαρυτάτη II
35, 61. **grauissimum** βαρύτατον II 35, 57. **grauissimo** ἐντιμοτάτῳ
II 35, 56. *V.* cras, gratus, non grauis.

**Grauissime** βαρυτάτως II 35, 62.

**Grauitas** βαρύτης II 256, 4. στερρότης II 36, 6 (grautates *cod.
corr. a e: nisi pluralis praeferendus*); 437, 36; 561, 17 (*suppl.
Boysen*). **grambas** αναστυσια (*ubi* grauitas ἀναισθησία *c*; crambas
ἀναστύσια *Buech.*) II 35, 53. **grauitas** firmitas, modestia IV 599,
9; 605, 30. modestia V 413, 43 (*reg. Bened.* 7, 136; 22, 12?).
**grauitates** στερρότητες II 35, 59.

**Grauiter** βαρέως II 34, 41; III 129, 55. uehementer aut moleste
(*Verg. Aen.* VII 753. *v.* grauis, grauitas) IV 83, 51; 521, 47.
uehementer IV 598, 54. *Cf. Donat. in Hec.* V 1, 32.

**Grauiter commotus** uehementer iratus IV 444, 6 (*Verg. Aen.* I
126); 84, 10; 599, 4.

**Grauo** βαρῶ II 256, 5. βαρύνω III 73, 46; 129, 50. **grauas**
βαρύνεις III 129, 51. **grauat** βαρύνει III 129, 52; 450, 8.
**grauauit** (grauabit?) portauit IV 444, 5 (*Verg. Aen.* II 708?).
onerauit uel portauit IV 599, 3. onerauit IV 521, 23. **grauor**
βαρύνομαι III 129, 53. **grauaris** βαρύνει[ς] III 159, 54.
**grauatur** βαρεῖται II 34, 42.

**Grauosus** *v.* onustus.

**Gredinunda** βαδίζουσα II 36, 10 (*ubi* gradibunda *c d g: at cf.
Lindsay p.* 472).

**Gregalis** ἀγελαῖος II 216, 12; III 488, 14 (gracilis agilius). pastor
II 581, 48 (*male versum*; an = gregarius?). uilis, po­pularis IV 84, 4.
mediocris IV 84, 20; 605, 34; V 364, 13. *Cf.* **gregalis** multitudo
mediocris V 299, 10 (*v.* IV 84, 19 + 20).

**Gregarius** ἀγελάρχης, ποιμήν II 35, 63. ἀγελαῖος II 216, 12.
ἀγελάρχης II 216, 13; III 358, 2. χυδαῖος ἄνθρωπος II 479, 21.
ἀγελάρχης, βουκολικός (βουκονκος gregaris *cod.*) III 432, 18. popularis
II 581, 42. dux militum V 362, 53. **gregarii** ἀγελάρχαι II 36, 11.
**gregariorum** aedilra (*der edeln, AS.*) V 363, 32. **gregariis**
uulgaribus IV 242, 22; 588, 38; 599, 13; 605, 33. *Cf.* **gregaris**
βουκολια (βουκολικός? *cf. adnot.*) III 432, 17. *V.* regaliolus.

**Gregarius homo** dux IV 84, 21; *a post* 605, 32 (de grege *add. m.*
2). dux uel miles unius annonae V 299, 11.

**Gregat** ἀγελίζει, ἀθροίζει II 34, 59 (ἀγελάζει *Vulc.*).

**Gregatim** ὁμοθυμαδόν II 383, 17. *Cf.* ἀγεληδόν \<**gregatim**
Ἀγένεια\> ignobilitas II 216, 15. globatim IV 84, 3; V 106, 38.

**Gregorius** uigilans V 106, 36.

**Gregoro** (gregosto *cod.*) uigilo graece V 106, 36. **gregoras**
uigilas V 106, 37.

**Gremia** *v.* cremium, gremium.

**Gremio fouet** qui sinu[m] sustinet IV 84, 8 (*Verg. Aen.* I 718).

**Gremiones** (= cremia?) *in hac est glossa:* fragmenta gremiones III
199, 33. *sequitur:* chamata fugu: *ubi* κλάσματα fragmenta, ἀκρεμόνες
*latere videtur: etsi quid* fugu *esse dicam nescio* (φρύγανα *H.*
fragmenta γῆ μείων ἢ σκάμματα ζυγοῦ *'ludibundus' Buech.*). *V.*
gemiones.

**Gremium** ἀγκάλη II 36, 7; 216, 29; 503, 62; 530, 27. **gremius**
ἀγκάλη II 216, 29; 488, 42; 511, 24; 538, 16; 550, 29 (*cf. GR. L.* I
552, 8). sinus IV 522, 32; V 299, 5. **gremium** sinus V 617, 35.
[graece] sinum V 299, 9. sinum IV 522, 31. signum (*h. e.* sinum) IV
84, 16 (*Verg. Aen.* XI 744). sinus et praefectura, sedes IV 242, 9;
588, 36 (sedis). sinum uel sedile IV 347, 49; 605, 35; V 502, 55. sinum
uel sedile, sedis perfecta (praefecta?) uel praefectoria IV 599, 15.
ulnae (!) II 581, 40. *Cf.* **gremia** δράγματα III 200, 11. *V.* in
gremio, cremium, grumus. *Cf. Arch.* II 135; VIII 191, 448.

**Gressi** incedentes IV 599, 17.

**Gressu\<m\> dirigebat** iter tendebat IV 84, 11 (intendebat *a*).

**Gressus** πορεία, βάσις, βάδισμα II 36, 5. βάδισμα II 255, 17; 491,
29; 515, 33; 542, 43. **gressum** τὸ βάδισμα II 36, 8. βῆμα III 305, 69;
511, 21. **gressus** βάσις III 490, 28. βάσις, βῆμα III 469, 51.
ἐπίβασις II 307, 5. *Cf.* II 511, 29. ambulatio IV 84, 17. *V.*
incessus, grassus.
