C\<l\>ytium infelix, noua gaudia, Cydon V 178, 6 (*Cf. Goetz
'Sitzungsber. der K. S. Ges. d. W.'* 1896 *p.* 88). **cidones** puerorum
amatores V 595, 27. *Cf. Osb. p.* 152.

**Cydoneum** (citoneum *cod.*) κυδώνιον III 358, 76. **cydonea** κυδώνια
III 192, 16; 264, 19. **cidoni** (? κυδώνια) id est **cydonia** III 537,
39. **cidon (?)** κυδώνια, χρυσόμηλα III 428, 25. cidon **cidonia** III
555, 50; 620, 8. *V.* cana mala, cicocia, cotoneum.

**Cydonium (cithonia *cod.*) malum** κυδώνιον μῆλον II 356, 25.
**mala cydonia** κορωνόπους III 538, 15; 544, 24; 558, 40; 622, 38; 72;
631, 52. coronopodium III 558, 34. coronopodia III 581, 32. *Cf. v.
Fischer-Benzon p.* 147. codonius (cotonium?) III 588, 20; 592, 54; 610,
5; 614, 32. *Cf.* III 588, 38; 48; 609, 29; 626, 31. *V.* corona poia.

**Cygn-** *v.* cycn.

**Cylindrus** κύλινδρος III 200, 16; 262, 59. lapis uoluilis IV 34, 59
(uolubilis *cd*). est fustis quo aratores glebas cam­porum quassant V
617, 49. **cylindrum** lapis uolubilis IV 495, 20 (*Serv. in Georg.* I
178). semicolumnium IV 35, 7; 495, 21; V 595, 46; 633, 24. **cylindri**
margaritae rotundae V 653, 37 (*Iuvenal.* II 61). *V.* clinorum.

**Cyllaris (!)** equus Pollucis V 178, 10 (*cf. Verg. Georg.* III 89
*sq.*).

**Cyllenius** mons Arcadiae uel Mercurius ab ipso monte IV 436, 44
(*Verg. Aen.* IV 252; 276; *Serv. in Aen.* VIII 138). **Cyllenus** (!)
mons Arcadiae V 550, 34. **Cyllenius** Mercurius IV 219, 8. *Cf. Festus
Pauli p.* 52, 3.

**Cylleus** *in hac est glossa*: βόρβορος hoc caenum, **cylleus**,
plurale non habet caenum II 258, 45 (*ubi nescio an* caenus *lateat*,
colluuies *H.* κοίλειος *Buech., a* κοιλία = βόρβορος).

**Cyma** ὄρμενον III 88, 41; 185, 49; 218, 53 = 233, 50 = 653, 11; 359,
15; 397, 58. **cuma** ὄρμενον II 387, 1. ἀσπάραγος, κραμβασπάραγος,
ἀροτρίασις (? *an ad* 53 *spectat?*) II 118, 54. ἀσπάραγος II 492, 53;
540, 7; 552, 32; III 444, 55; 484, 30. **cyima** ἀσπάραγος III 317, 26.
**cima** ἀκρεμών II 223, 43. **cuima** culicli (*v.* cauliculus)
κραμβασπάραγος III 317, 27; 526, 46. **cyma** cucumola V 596, 38.
cocumula V 595, 45; 633, 21. **cimae** ἀσπάραγοι III 430, 23. *Cf.*
**cymaculo** mola V 543, 25 (*an in his omnibus latet* cyma culiculi?
cucuma cucumula *H.*). *Cf. porro* isparagia id est **cimas** III 565,
39. ὄρμενον id est **siamo** (cyamos *Stadler*) III 571, 35. *V.*
cauliculus.

**Cymba** nauis V 354, 23. **cumba** ἀκάτιον II 222, 35. σκάφη τὸ
πλοιάριον II 432, 44. σκάφος, πλοιάριον II 521, 57. locus (iouis *R*)
nauis *Plac.* V 60, 5 = V 12, 29 (*cf. Isid.* XIX 2, 1: *ubi* locus imus
n.). nauis IV 29, 37 (camba cauis *cod. em. Nettleship 'Journ. of
Phil.'* XIX 117); 46, 23; V 282, 51. nauiculae genus V 446, 52. nauicula
modica IV 224, 44; V 282, 47; 566, 35. nauicula aut nauis IV 501, 30.
nauis non grandis IV 326, 38. species nauis non grandis V 448, 50. genus
est nauis, id est pilatica (piratica?) V 448, 41. est fundus nauis V
617, 28.

**Cymbalissare** cymbala quatere V 639, 48 (= *Non.* 90, 21).

**Cymbalum** κύμβαλον II 356, 46. **cym­bala** κύμβαλα III 171, 13; 204,
3; 238, 64; 325, 32; 527, 27.

**Cymbia** species poculorum IV 34, 48; 436, 45 (*Verg. Aen.* III 66;
V 267). poculorum genera IV 218, 51; V 277, 16; 446, 40. species
crocorum (poculorum? caucorum?) V 494, 40. poculorum sunt genera ex
quibus cymbia pocula dicta sunt ex similitudine cumbae nauis V 178, 13
(*cf. Serv. in Aen.* III 66; V 267; *Isid.* XX 5, 44; *Festus Pauli p.*
51, 10). uasa quae in modum cauci fiunt *lib. gloss. = Mai* VII 555.
*V.* catinus.

**Cyminatum** (cum. *cod.*) κυμινᾶτον III 315, 6.

**Cyminum** κύμινον II 356, 47; III 186, 9; 266, 1; 359, 79; 430, 71;
491, 30. **cuminum** κύμινον III 555, 32; V 448, 53. *Cf.* ciminon id
est **cimino** III 537, 16. ciminus id est **cominus** III 609, 56.
basilicon quiminon **cuminum** III 554, 44; 618, 74. cymon **ciminum** V
178, 14. *V.* ameus, careo.

**Cyminum aethiopicum** *v.* ameus.

**Cynici** philosophi sunt a canibus uitam ducentes IV 219, 28 (*cf.
Isid.* VIII 6, 14). philosophi filo (κύων *Nettleship 'Journ. of Phil.'*
XIX 293: *v.* cynus) enim graece canis dicitur IV 143, 28. **quinici**
philosophi sunt a canibus uitam ducentes: quinos (!) enim graece dicitur
canis *a post* IV 153, 25 (filo *pro* quinos); V 238, 15; 327, 10; 386,
30/31. philosophi sunt a canibus uitam ducentes; philosophi enim graece
canes dicuntur IV 559, 41 (*cf. ac post* IV 158, 16). *Cf. Loewe Prodr.*
376, *Birt Mus. Rhen.* LI 98. *V.* cynus.

**Cynico more** canino more V 277, 28.

**Cynocaumaticis** *v.* dies caniculares.

**Cynoglossa** ribbae (*AS.*) V 354, 5. *Cf.* canis lingua.
