**Communio** κοινωνία III 442, 70.

**Communis** κοινός II 104, 17; 24; 105, 31; 351, 54; III 332, 19; 442,
71; 527, 52. simul, in se IV 36, 20 (*v.* comminus). humanus, iucundus
IV 497, 40 (*cf. Serv. in Aen.* VIII 275). κοινωνικός III 373, 60.
κοινὴ ὄνομα II 351, 49. **commune** κοινόν II 351, 53; III 147, 38.
inquinatum, inmundum V 521, 27 (*cf. Vulg. Act.* 10, 14). inmundum
dicitur et (*vel* eo) commune V 182, 11. **com­munem** inmundum IV 499,
21. *V.* in commune, comis.

**Communis uictor (!)** σύγκοινος III 6, 36.

**Communiter** κοινῇ ἐπίρρημα II 351, 48.

**Communitorium** munitionem (*de lib. off.*) V 415, 19. *Cf.*
commonitorium.

**Commutatio** ἀντάλλαγμα II 228, 42. *Cf. post* II 105, 7.

**Commuteo** (conmutuo *cod.*) *et* **commutesco** φιμοῦμαι II 472, 9
(*Arch.* II 469). **commutescit** reticet, conticiscit IV 320, 58.

**Commuto** ἐναλλάσσω II 297, 23.

**Como** κομῶ II 353, 2. **comat** frondet IV 36, 11; 220, 47; 320, 30;
499, 11; V 279, 9; 594, 45. ornat (*v.* comit), frondiat (!) V 495, 12.

**Comoedia** κωμῳδία III 375, 69. storia comoediae IV 408, 19. historia
comoedi graece IV 220, 35. significatio morum singulorum cum detractus
quis fit in cerco (!) IV 221, 27. historia V 596, 20. historia,
tragoedia V 181, 5. est quae res priuatorum et humilium personarum
conprehendit non tam alto ut tragoedia stilo, sed mediocri et dulci
*Plac.* V 56, 11. est quae priuatorum hominum continet acta, comoediam
autem inuenisse Thaliam, unam ex Musis, poetae finxerunt, dicta autem
comoedia siue a loco, quia circum pagos agebatur, quos Graeci κώμας
uocant, siue a commessatione. solebant enim post cibum homines ad eos
audiendos uenire. sed prior ac uetus comoedia ridicularis exstitit,
postea ciuiles uel priuatas adgressa[s] materias in dictis atque gestu
uniuersorum delicta corripiens in scenam proferebat, nec uetaba\<n\>tur
poetae pessimum quemque discrimine (scr. describere *ex Isid.*) uel
cui\<us\>libet peccata moresque reprehendere, auctor eius \<Sus\>arion
(*suppl. Usener*) traditur, sed in fabulas primi eam contulerunt Magnes
\*\* (Magnes \*\* *Buech.* magnes *vel* magnas *libri*), ita ut non
excederent in singulis uersus tricenos, postea autem omissa maledicendi
libertate priuatorum hominum uitam cum hilaritate imitabant ammonentes,
quid adpetendum quidue cauendum esset. Romae tragoedias comoediasque
primus egit idemque etiam composuit Liuius Andronicus duplici toga
inuolutus . aput Romanos quoque Plautus comoediae choros exemplo
Graecorum inseruit V 181, 7. *Cf. Isid.* VIII 7, 6 *sq.; Usener Mus.
Rhen.* XXVIII *p.* 418, *Sabbadini Stud. Ital.* II 40. **commoediae**
cantica agrestia graece V 181, 6. *Cf.* scena.

**Comoedus** κωμῳδός II 357, 43; III 172, 50; 240, 8. **comoedi**
κωμῳδοί III 10, 42; 84, 19; 302, 34; 371, 73; 525, 22. sunt qui uanorum
hominum acta et amores meretricum cantabant V 566, 5 (*cf. Isid.*
XVIII 46). *Cf.* **comoedis** inde nomen est castellis et cantilena V
181, 8 (*mutilata*).

**Comopolis** municipium graece V 182, 7. *V.* conciliabulum.

**Comosus** *v.* comatus.

**Compactio** (*vel* conp.) συνθήκη II 446, 32. σύνταξις ἐπὶ δύο τινῶν
ἀλλήλοις συνθεμένων II 448, 4.

**Compactum** συνδοιασμός II 499, 62; III 484, 40. σύμφωνον ἤτοι συνθήκη
II 443, 24 (*v.* compactus).

**Compactum (conp.) facit** συνδοιάζει 110, 39. συνδυάζει II 108,
16.

**Compactus** (*vel* conp.) σύμφωνος II 443, 26. συνηρμοσμένος II 446,
24. συνηρμολογημένος II 110, 45. **compactum** (*vel* conp.)
συνηρμοσμένον II 105, 57. συμπεπηγός II 110, 26. coniunctum IV 39, 6;
496, 35. **conpacta** coniuncta, conposita IV 223, 9.

**Compages** (*vel* conp.) ἁρμογή II 245, 8 (compagies). γόμφος ὁ
καλούμενος ἐπίουρος II 264, 30 (compagies). ἐπίουρος II 310, 9 (*item*).
coniunctio II 574, 33. ἁρμογαί, ἁρμοί II 110, 38. iuncturae tabularum IV
434, 44 (*Verg. Aen.* I 122). iuncturae membrorum V 280, 61.
coniunctiones IV 220, 37. **conpagibus** iuncturis IV 496, 37; V 184, 1
(*Verg. Aen.* I 293). iuncturis, alligamentis V 447, 38. tabularum
texturis V 183, 50.

**Compaginatam** coniunctam (*Euseb. eccl. hist.* IX 10) V 421, 50; 430,
33.

**Compaginatio** (*vel* conp.) ἁρμογή II 245, 8. ἅρμοσις II 245, 20.
ἁρμολόγησις II 245, 16.

**Compaginatus** συνάρμοσις II 444, 22.

**Compagino** συναρμόζω II 444, 20. συναρμολογῶ II 444, 21.
**conpaginauit** coniunxit IV 496, 42; V 279, 39; 627, 31. *V.*
compingo.

**Compago** (*vel* conp.) ἁρμονία μελῶν ἀνθρώπου II 245, 17. corporis
commissura IV 41, 7. coniunctio V 184, 3; 282, 3. copula, coniunctio IV
323, 6. iunctura IV 496, 39; V 184, 4. kapita sunt ossuum, dicta eo quod
sibi con-
