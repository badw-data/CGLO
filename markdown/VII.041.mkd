(= uelum) III 92, 58 (*cf. Bluemner ῾Maximaltarif' p.* 154). ἔνδυμα III
323, 14; 493, 31; 518, 66. ἁπλο\<ΐ\>διον III 323, 17. φάρος III 471, 48.
yrsos (ὑσσός pilum?) III 208, 61. *V.* cidarim.

**Pallor** ὠχρίασις II 141, 30; 483, 7; 490, 13; 512, 57; 538, 71; 551,
23; III 603, 39.

**Pallor (?)** ἀποχλωριῶ II 242, 40.

**Palma** χεὶρ καὶ βαΐς καὶ φοῖνιξ καὶ παλάμη II 141, 1. νίκη, φοῖνιξ II
141, 22. φοῖνιξ II 472, 41; 493, 49; 541, 18; III 26, 23, 191, 39; 264,
2; 358, 26; 397, 15. fnix III 563, 38. fenice III 563, 10 (*cf.*
**palmone** φοῖνιξ III 358, 48). fenu nuce III 539, 19. βραβεῖον II 259,
45; 546, 9; III 173, 6; 240, 19. παλάμη II 392, 45. σπιθαμή II 435, 43
(*v.* palmus). παλαιστής III 311, 16. παλαιστὴς χειρός II 392, 40.
παλαιστής, σπιθαμή, δοχμή III 248, 9 (*unde*?). genus uictoriae IV 373,
21. uaticae diuinationis IV 373, 22 (*ubi* uictoriae donatio *Loewe GL.
N.* 114: *cf. Roensch Coll. phil. p.* 301. *de* παλμοσκοπία *cogitat
Buech.*). **palmae** (*de secunda mensa*) φοίνικες III 15, 47; 316,
10; 372, 34. ἀδελφίδες III 185, 9. **palmas** uictorias IV 266, 40.
manus uel uictorias IV 135, 28; 548, 9; V 128, 31. *V.* abies, palmus.

**Palma cum dactulis** φοῖνιξ III 582, 54; 590, 43; 611, 61; 624, 8.
*Cf.* dactylus.

**Palma nucaria** φοῖνιξ καρυη\<ρ\>ά III 428, 17 (*suppl. David*).

**Palmatio** *v.* segestrum.

**Palmatus** laureatus uel coronatus IV 135, 27; 548, 10; V 128, 32;
230, 16. coronatus IV 266, 28. *V.* toga palmata.

**Palmes** κλῆμα τὸ τοὺς βότρυας ἔχον συνεκκοπέντας II 141, 31. κλῆμα
βότρυς ἔχον II 141, 3; 350, 37. ὄσχος II 141, 23. pars uitis unde uua
nascitur V 382, 55. pars uitis est ubi uua nascitur, quasi palma V 230,
18. uitis est ubi uua nascit, ut palma V 128, 30. in uitibus, uelut
principia flagellorum, generis feminini, ut palmae (palma *cod. alter*)
fecunda V 230, 19 (*v.* palmes laetus). **palmites** ἔρνη III 427, 47.
sarmentum IV 547, 17. sarmenta V 472, 11; 525, 20. *Cf. Festus Pauli
p.* 220, 8.

**Palmes laetus** uitis brachiolum quo unae sunt, masculini generis.
Virgilius (*Georg.* II 363): dum se laetus (*uel* lectus) ad auras
Palmes agit V 230, 17.

**Palmetum** (*uel* palmentum) φοινικών II 141, 17; 12 (φοινικεών); 472,
39; 42; 500, 60.

**Palmidae** ἀδελφίδες III 256, 12 (*unde*? *cf.* palma).

**Palmosus** uictoriosus *Scal.* V 607, 65 (*Osb.* 482).

**Palmula** πλάτη ἐν ᾗ διαπλέομεν II 408, 61. φοῖνιξ, θρῖναξ (*v.* pala)
II 140, 56 (δράξ *e*). τὸ θέναρ τὸ πλατὺ μέρος τῆς κώπης II 524, 2 + 3.
extrema pars re\<mo\>rum IV 456, 45 (*Verg. Aen.* V 163). extrema pars
nauis IV 266, 36; V 509, 16. **palmulae** φοίνικες III 88, 10.

**Palmularium** φοινίκιον II 472, 38.

**Palmus** παλάμη III 351, 28. παλαιστής II 141, 7; 18; III 175, 51.
παλαιστὴς χειρός II 392, 40. παλαιστής, σπιθαμή, δοχμή III 248, 9
(*unde*?). **palmum** ψιμύθιν II 141, 29 (σπιθαμήν *Vulc.*, *nisi*
plumbum *subest*).

**Palor** ῥέμβομαι III 458, 77; 486, 75. **palatur** ῥέμβεται II 141,
26. **palantur** uagantur IV 136, 3; 267, 8. **palare** errare IV 266,
37.

**Palpabunda** blandiens V 636, 21.

**Palpantia** trementia IV 373, 23. **palpitia** trementia \[reliquo ad
huspia (haruspic- *Buech.*)\] V 128, 34.

**Palpebra** βλέφαρον II 497, 44; 523, 55; 546, 11; III 554, 9; 618, 37
(*GR. L.* I. 553, 26). **palpebrae** βλέφαρα II 140, 53; 258, 11; III
175, 3 (-bra); 247, 24; 310, 30; 349, 28; 350, 29; 394, 33; 414, 72;
491, 3; 511, 50. κανθοί III 12, 13; 85, 35.

**Palpitans** brocdaettendi (*uel* brogdaet hendi, *AS.*) V 378, 14. est
qui animam trahit: nam palpitrans (*uel* palpitans) non est latinum:
potest tamen a palpebris uenire *Plac.* V 37, 35 = V 90, 21 = V 130, 16.
*Cf. Deuerling praef.* XX, *Roensch Coll. phil. p.* 261, *Diez* I
palpebra.

**Palpitatio** haesitatio, dubitatio V 509, 19. *V.* sine palpitatione.

**Palpito** πάλλω II 393, 6. σκαρίζω II 432, 40. σπαίρω ἐπὶ τοῦ σκαρίζω
II 435, 10. tempto V 509, 17. **palpitat** σφύζει II 449, 48. σπαίρει,
πάλλει II 141, 8. temptat IV 267, 7.

**Palpo** ψηλαφῶ II 480, 32; III 164, 37. foueo aut blandio aut
pertracto IV 135, 21; 548, 3; V 128, 33 (blandeo). **palpas** ψηλαφᾷς
III 164, 38. **palpat** ψηλαφᾷ III 164, 39. blanditur aut manu
contractat IV 135, 22; 548, 4. fouet, blanditur IV 266, 30. **palpare**
ψηλαφῆσαι III 164, 40. adtrectare uel blandire IV 373, 24. blandire
(*uel* -ri) uel adtractare IV 548, 5; V 317, 43. blandire IV 136, 5.
*Cf. Festus Pauli p.* 220, 14.

**Palpo** \<a\> palpando V 646, 38 (*Non.* 163, 10). **palponem**
adolatorem V 509, 11; 574, 56. **palpones** qui ducunt caecos *Scal.* V
607, 62. *Cf. Osb.* 427; *Loewe Prodr.* 51.

**Palpo equo** κατακροτῶ τοῦ ἵππου II 141, 34 (*cf. GR. L.* VII 283, 13;
425, 2).
