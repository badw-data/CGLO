sabina (= auena) siluatica III 580, 16. zizania id est **lolium** id est
aduena (= auena) siluatica, id est aneronis III 630, 60. *Cf.* era
(αἷρα) **lupus** III 545, 38. **lolium** atte (*vel* atae, *AS.*) V 369,
33. malus terrae, tubercula, IV 361, 45 (*ubi* mala terrae herbuscula
*Hildebrand*). malus terrae, tubercula uel spina V 629, 50.

**Lolligo** τευθίς II 124, 18; III 17, 13; 89, 28; 186, 57; 257, 20;
317, 65; 355, 34; 69; 396, 43; 409, 77; 436, 60. **loligo** fructus seu
aues, quae per sex penses piscees, per sex fiunt aues, per sex alii
pisces V 554, 14. **solligina** τευθίς II 454, 42 (lolligina *e*). *Cf.
Isid.* XII 6, 47.

**Lomentarius** σμηγματοπώλης II 434, 43.

**Lomento** σμήχω II 434, 46. zimizo (σμήχω?) III 142, 62 (lam.).

**Lomentum** σμῆγμα II 434, 42; III 142, 61 (lam.); 430, 12 (γαλιγνα).
farina uiua, mulieres in faciem mittunt V 308, 39. fomentum, ablutio V
463, 45; 506, 38; 572, 5. nitrum V 544, 20; 602, 61. **lomenta** σμῆγμα,
σῆμα (σμῆμα *H.*) II 124, 20 (σμήγματα *e, margo*). **lumentum** ἐρεγμοῦ
λέπος II 314, 2. id est leuamen de erba escumaria III 613, 31; 625, 42
(scuminaria). **lumentus** leuamen de scumaria erba III 591, 72.
**lomentum de faba** oromum (ἐρεγμόν) III 570, 47.

**Lomerato** *v.* glomerati.

**Longaeuus** μακρόβιος, γεραιός II 124, 21 (*cf. margo*). μακρόβιος II
364, 10. μακροχρόνιος II 364, 24; 557, 18. πολυχρόνιος II 413, 19.
πολυετής II 412, 38. γεραιός II 262, 51. γηραλέος II 263, 11. πρεσβύτης
II 415, 28; III 328, 68. longus IV 415, 11 (longeus). longae aetatis uel
senior IV 110, 28. ualde senex IV 535, 9; V 308, 34 (longeus).
**longaeuo** seni IV 110, 25. seni uel deo facto. hi[c] (*del. Buech.
'nisi* Stoici *reponendum'*) enim longaeuos dixerunt deos, id est longa
aetate, non perpetuos, qui dicunt cum suo mundo esse casuros V 219, 3
(*cf. Verg. Aen.* VI 764; *Gell.* II 16). **longeuum** aeternum IV 450,
26 (*Verg. Aen.* II 525).

**Longa intercapedine** longo interstitio, longo interuallo IV 361, 46.
longo interuallo, longo interstitio *cd ante* IV 110, 34; V 219, 2; 463,
46. longo interstitio.

**Longa nauigatio** μακρὸς πλοῦς II 364, 20; 495, 10.

**Longanimis** μακρόθυμος *post* II 124, 23; 364, 14 (-us); III 454, 77.
*V.* durabilis.

**Longanimitas** μακροθυμία II 364, 13; 561, 33 (*suppl. Boysen*); III
424, 31; 454, 78; 499, 71.

**Longao** κολέντερον II 124, 22 (*cf. margo.* longabo *c*, longano
*vel* longabo *Herald.*). **longaonem** τενεσμόν III 602, 28. *V.*
chordapsus.

**Longa ualitudo** μακρονοσία III 206, 6.

**Longe** ἄπωθεν II 243, 31. μακράν II 364, 8; III 454, 79; 470, 52.
μακρόθεν II 364, 12. λίαν, μακρόν (-άν *e*) II 124, 23. [manifeste
aut] ualde uel nimium satis *Plac.* V 30, 19 (*cf.* libare) = V 81, 17
(*cf. Serv. in Aen.* I 13; II 711; V 406; *GR. L.* I 203, 17; *Non.*
339, 9). aliquotiens pro ualde accipiendum, ut: longe distat ab illo
sapiente \<i\>ste indoctus *Plac.* V 81, 16 = V 113, 27. procul IV 361,
47. maxime IV 450, 27 (*Serv. in Aen.* I 252); 535, 10; V 536, 56
(*Ter. Ad.* 65). multum, ualde [longe] V 308, 41. **longius**
μακροτέρω II 364, 23. ἄπωθεν II 243, 31. plus *Plac.* V 81, 19.
**longissime** μακρότατα II 364, 21. *V.* haud longe.

**Longe iaculans** ἑκηβόλος III 494, 15.

**Longe lateque** ex utraque parte IV 110, 30; 450, 28 (*Verg. Aen.*
VI 378).

**Longinquitas** μακρότης II 364, 22; 561, 32 (*suppl. Boysen*).
μακροχρονιότης II 364, 25 (longaeuitas?).

**Longinquus** ὁ μακρὰν ὤν II 382, 51. μακρὰν ὥν II 364, 9. ἐπιμήκης II
309, 44. **longinquius** longius, ulterius *Plac.* V 30, 1 = V 81, 18.
*V.* ex longinco.

**Longiscere** longum fieri V 643, 60 (*Non.* 134, 17): *ubi*
languescere *Hug.*

**Longisecus** πόρρωθεν II 414, 5. ἄπω­θεν II 243, 31.

**Longitudo** μῆκος II 124, 24; 498, 51; III 180, 11; 252, 26; 470, 53.
μῆκος τὸ τοῦ ἀνθρώπου II 370, 53. μακρότης II 364, 22; 557, 30; III 454,
80. proceritas IV 361, 49.

**Longiturni** longaeui, id est longi temporis V 506, 39. longaeui IV
255, 44.

**Longo** μηκύνω II 370, 56.

**Longo interuallo** V 662, 58. ex longo tempore, sed a locis tractum
est, inter murum et fossatum locus in medio interuallum dicitur: hoc iam
translatum est et ad tempus IV 110, 34 (*Verg. Aen.* V 320); V 219, 4;
308, 38; 463, 47.

**Longo limite** prolixo ducto (-tu *H.*) IV 450, 29 (*Verg. Aen.* II
697). longo ordine IV 255, 45.

**Longo luctu** a[c] bello decenni temporis IV 450, 30 (*Verg. Aen.*
II 26).

**Longurio** longus V 643, 38 (*Non.* 131, 27).
