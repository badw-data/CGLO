γνῶσις, πρόνοια II 163, 51. ἀγχίνοια II 217, 47. δεινότητος II 160, 16
(-ae *e*).

**Prugella** *v.* prunella.

**Pruina** πάχνη II 164, 1; 400, 4; III 245, 4; 294, 15; 347, 46; 425,
60. paline (πάχνη?) II 519, 57. κρύσταλλος, πάχνη II 541, 19 (*GR. L.* I
553, 9). παγετός II 391, 41; III 245, 2. nix et **pruina** νιφετός III
244, 69 (*unde*?). **pruina** frigor IV 558, 24. glacies V 321, 38
(pluina *cod.*). dicitur aqua alba hiemalis, quae ante lucanum cadit
super herbas V 237, 24. aqua gelata, uel tempestas, turbo, glacies IV
382, 17. genus (= gelus) niualis uel aqua gelata V 325, 46. **pruinas**
πάχνας II 163, 54.

**Pruna** ἀνθράκιον \[κοκκύμηλον *v.* prunum\] II 163, 55. ἀνθρακιά III
245, 19 (*unde*?). **pruina** ἄνθραξ II 519, 51 (*corr. bce. GR. L.* I
553, 8). **pruna** δαλός II 541, 23. carbones uiui IV 382, 18; V 237,
25. **prunam** ἀνθρακιάν III 215, 22 = 231, 1 = 650, 9. **prunae**
ἄνθρακες II 541, 22. carbones uiui IV 557, 36. **prunas** carbones uiui
IV 275, 55. *V.* ramalis pruna.

**Pruna cerea** pomi genus V 237, 26 (*Verg. Ecl.* II 53).

**Prunarius** *v.* gumen.

**Prunella** *in his est gl.*: **prugella** siue **prunella** uquimela
(κοκκύμηλα) III 632, 69. **progella** aquimilla III 536, 34. **prunellas
de uisco** ioquimella III 566, 52. **pumella euisco** iuquimella III
591, 44. **pumella uisco** iusquimella III 612, 69. iuquemella III 625,
15. **pumella de uisco** utquimella III 579, 24. *V.* acacia, occa.

**Prunellatum** *v.* bacatum.

**Prunum** κοκκύμηλον II 352, 1 (pronum *cod. corr. e*).
κοκκύμηλ\[ε\]α III 358, 78 (*del. Vulc.*). *Cf.* \[μηλέα\] κοκκύμηλον
(-io *cod.*) \[melaria\] **pronum** III 428, 19. **prunum** et **pruna**
poma IV 152, 20; 558, 34. **pruna** \[ἀνθράκιον *v.* pruna\], κοκκύμηλον
II 163, 55. κοκκύμηλα III 15, 52; 88, 16; 185, 24; 192, 5; 256, 27
(prunae); 316, 27; 358, 19; 372, 39; 397, 8; 415, 42; 497, 75; 526, 32;
544, 68. βερέκοκκα II 257, 16. ἀχράς (? pirum?) III 316, 23; 509, 53.
*Cf.* eg \*\* serabro semen a **pruno** III 545, 66. **pruna** uallis IV
558, 28 (*obscura*: *v. sub* pruina. *de* prona ualle *cogitat
Buech.*).

**Prunum praec\<oqu\>um** κοκκύμηλον (*suppl. Keil*) II 548, 6.

**Prunus** δαμασκηνόν II 266, 21. arbor IV 152, 19; 558, 33. plumae
(*AS.*) V 382, 45. lignum, **prunum** fructus id est plun (*AS.*) *gloss.
Werth. Gallée* 343 (*GR. L. suppl.* 238, 23; *v. suppl.*).

**Prurientes** detrahentes V 512, 3; 577, 14. scalpientes (!) V 607, 25;
630, 59.

**Prurientes auribus** qui libenter desiderant de alio mali aliquid
audire *Scal.* V 608, 46 (II *Timoth.* 4, 3: *cf. 'Litt. Centralblatt'*
1877 *p.* 695).

**Prurigo** κνησμὸς ἤτοι κνησμονή II 351, 28. κνησμός II 541, 21; III
76, 17. κνήφη II 163, 56. desiderium scalpendi II 590, 17 (*Gallée* 363.
*v. suppl.*). scabies V 325, 47. scalpitio *Scal.* V 607, 24; 630, 58
(prurio). gycinis (*AS.*) V 381, 36. **prorigo** πίτυρον κεφαλῆς II 498,
62. **prurigo** uel **porigo** (*v.* porrigo) myrinicia (μυρμηκιά?) III
569, 24. **prorigo** urido cutis V 426, 54 = uriclo cutis, id est gyccae
(*AS.*) V 418, 12 (*Euseb. eccl. hist.* I 8). **proriginem** euismonen
(κνησμονήν?) III 600, 12. bloot (? *AS.*) *gl. Werth. Gallée* 343 (*v.
suppl.*). **prurigine** scalpitudine V 512, 4.

**Prurio** καπρῶ II 338, 46; III 461, 39; 486, 74. **prurit** κνηφη
(κνήθει? κνηφεῖ *e*) II 163, 57. **plire** saltare V 655, 24 (plurire =
pr.? *cf. Iuvenal.* XI 163).

**Prydanis** prudentia V 379, 19 (*cf. Verg. Aen.* IX 767).

**Prytaneum** *v.* brittaneum.

**Psades aturafa** (*uel* psade paturafa) incerta et de octaua egregium
V 379, 14 (*obscura*; *fortasse ad Hieron. de vir. ill.* 32
'ψευδεπίγραφα' *spectat*). *Cf.* hyades.

**Psallere** *v.* blaterat.

**Psalmista** ψάλτης III 472, 14. plastes (= psaltes) III 502, 79.

**Psalmizo** ψάλλω II 480, 1 (*Arch.* VIII 384: *v.* nablizo).

**Psalmus** ψαλμός II 480, 2; III 472, 15. cantus V 414, 23 (*reg.
Bened.* 9, 6; 12, 4). *Cf.* ψαλμόν **psalmosaltrum** (psalmum saltrum?)
III 57, 50.

**Psalta** ψάλτης II 480, 4. **psaltes** cithara V 420, 34 = 429, 16
(*Euseb. eccl. hist.* IV 18). *Cf.* psalmista.

**Psalterium** laus V 379, 37. triangulum est *gl. Werth. Gallée* 343
(*v. suppl.*).

**Psaltria** cantatrix V 383, 9 (psallia). **psalt\[e\]ria** cantrix V
525, 6. **psaltriae** cantores siue cantrices in conuiuiis cantando
saltantes II *p.* XIV.

**Pseud\[o\]epigrapha** falsa superscripta V 377, 39 (*Hieron. de vir.
ill.* 32).

**Pseudolus** est falsus V 622, 39.

**Pseudomeni** dicuntur fallaces a graeco (*add. G*), qui rem aliquam
mentitionibus conantur adserere, ut dicimus (diximus *codd.*) de
philosophis qui dicunt: si dico \<me\> mentiri et mentior, uerum dico
*Plac.* V 38, 18 (pensomeni) = V 95, 14 (me *suppl. ex Gellio* XVIII 2,
10). *Cf. Cic. Div.* II 4, 11. *V. Buecheler Mus. Rhen.* XXXV 405.
