III 555, 60. **oua seruilia** (sorbilia?) carofita (ὠὰ ῥοφητά?) III 620,
18.

**Ouatio** minor triumphus V 555, 26 (*cf. Serv. in Aen.* IV 543).

**Ouicula** προβάτιον, ὄϊς III 432, 40. προβάτιον III 571, 27.

**Ouicularia** calemos (?) III 555, 39.

**Ouifer** πρόβατον ἄγριον II 416, 25; 492, 6. προβατάγριον III 90, 45;
189, 23. **oilifera** προβατάγριον III 18, 33. *Cf. Brugmann Mus. Rhen.*
XLIII 404, *Roensch Coll. phil. p.* 274.

**Ouile** ἔπαυλις, αιλεαι (αὖλαι? *de* caprile αἰγών *cogitat Vulc.*)
III 357, 78. ἔπαυλις II 306, 11; III 260, 26. ποίμνιον II 411, 34.
μάνδρα II 506, 46; III 313, 5; 530, 26. caula V 509, 5. *V.* ab
ouilibus.

**Ouilis** ab ouibus nomen accepit IV 455, 26 (*Verg. Aen.* IX 59).

**Ouilium** ποίμνη II 411, 33.

**Ouilla** προβατ\<ε\>ία III 316, 50.

**Ouina** (*sc.* caro) προβάτειον III 88, 28.

**Ouina pellis** ἀρνακίς III 273, 10 (*unde*?). *Cf.* pellis lanata.

**Ouis** πρόβατον II 499, 16; 523, 46; 545, 74 (*GR. L.* I 553, 24); III
18, 31; 90, 43; 189, 15; 320, 35; 361, 46. πρόβατον, ὄϊς III 259, 4
(*unde*?). *Cf.* πρόβατον uerbella, **ouis**, uerbex \[bala\<n\>tum\],
uides (bidens?). \[ouium\] II 416, 24. **oues** πρόβατα III 432, 39.
bidentes IV 455, 20 (*cf. Serv. in Aen.* VI 39; *Isid.* XII 1, 9).
uindentes (!) V 544, 52.

**Ouispex** ouium inspector *Scal.* V 606, 29 (*cf. Osb.* 401, *ubi*
ouinspex *editur*).

**Ouo** χαίρω II 474, 36. παιανίζω II 391, 55. **ouatur** χαίρει II 140,
20. **ouat** bacchatur, exilit, excrescit V 229, 37 (*Verg. Aen.* X
500?).

**Ouum** ὠόν II 482, 21; III 165, 45; 315, 9; 471, 47; 501, 19. **oua**
ὠά III 14, 58; 81, 50; 87, 47; 165, 44; 343, 42; 458, 67. **ouorum** ὠῶν
II 559, 19. **ouis** ὠοῖς II 559, 18 (ωουις *cod.*).

**Ouum piscis** κύημα ἰχθύος II 356, 26.

**Oxalmen** acetum et sal mixtum III 603, 44. *Cf. Diosc.* V 23.

**Oxantus** id est carpo et alius ciricen III 585, 9. Oxiacantus . . .
carpos et alias miricen *Stadler.*

**Oximinum** (= oxycyminum) aceto mixto cum cumino *lib. gl. Cf. W.
Heraeus 'Spr. des Petr.' p.* 18.

**Oxines** uappa graece V 128, 9; *lib. gl.*

**Oxogarum** ὀξόγαρον III 287, 39 = 658, 17; 521, 36.

**Oxymelle** confectio cum aceto et melle III 603, 34. *Cf. Diosc.* V
22.

**Oxypolyfagia** multum comedentes et cito digerentes (deg. *cod.*) III
603, 30.

**Ozasanga** militum calciamenta V 375, 19 (oza sanga *e. q. s. Roensch
Coll. phil.* 298). *V.* ocrea. *Cf. AHD. GL.* III 620, 53.

**Ozenas** (= ozaena) mala habitudo in naribus III 603, 38. *V.*
polypus.

**Ozias** fortitudo domini V 375, 31 (*cf. Isid.* VII 6, 71; *Onom.
sacr.* 50, 21).

**Ozirat** Syrorum lingua Iunius mensis dicitur V 229, 38.
