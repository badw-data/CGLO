per mendacium (*Euseb. eccl. hist.* II 18) V 419, 65 = 428, 51.

**Periscelis** περισκελίς III 324, 10. **periscelides** περίσφυρα III
22, 35; 93, 44. χρυσοψέλια III 203, 1. quae in tibiis habentur: peris
graece circum, celia tibia *a post* IV 153, 25. **periscelidae** crurum
ornamenta aput feminas (= *Eucher. instr. p.* 157, 4) IV 271, 52 (*cf.
Isid.* XIX 31, 19). **priscelli** feminarum crurum ornamenta V 382, 31.
**periscelides** armillas in pedibus V 381, 41. armillas pedum muliebris
IV 142, 46. *Cf. Funck Philol.* LIII p. 128.

**Peristereon** ἱεροβοτάνη III 565, 14. columbina III 557, 40 (columbina
*etiam Pseudapul.* IV). emagallis III 561, 54; 562, 39 (αἷμα γαλῆς
*Pseudap.* 65, *Diosc.* IV 60). aelius (*Pseudap.* 65) III 550, 43.
bonion (*Pseudap.* 65) III 554, 43; 618, 12; 618, 73. ferraria (*cf.
Pseudap.* 65, *Diosc.* IV 60) III 563, 25. exupra matricalis (*Pseudap.*
65) III 561, 56. thiabsenti (*Pseudap.* 65) III 578, 41. peltoclotis
(peltodotes *Pseudap.* 65) III 573, 47 (perstrion). *V.* crista
gallinacia, uerbena.

**Peristola** *v.* chrysocanthus.

**Peristromata** (*uel frequentius* perstromata) tegmina, accubitus IV
272, 11; 376, 7; 553, 7; V 233, 3; 473, 14; gemina (*scr.* tegmina),
accubitus V 319, 23. gemina IV 142, 1; 376, 39; V 132, 15; 233, 4.
tegmina V 320, 6; 381, 6 (protegmina *cod.*). gemina stifadii IV 140,
14; 138, 43; V 473, 32; 510, 13 (perstromea *ut* 473, 32). ornamenta
staefadbrum (= stibadiorum) V 385, 12. ornamenta stafediorum V 636, 23.
*V.* gemina. *Cf. Loewe Prodr.* 347; 290 et *de* stefadium *W. Heraeus
'Spr. des Petr.'* 15.

**Peristu\[l\]lium** columpnarum circumitus II 589, 22.

**Peristylum** περίστυλον II 147, 17.

**Peritia** πεῖρα, γνῶσις II 147, 15. εἴδησις II 285, 13. ἐμπειρία II
296, 21. ἐπιστήμη II 311, 15. doctrina IV 142, 36; V 132, 2.

**Peritia imbuti** doctrina instructi V 473, 13.

**Peritomen** (*uel* pentomen) circumcisio IV 139, 31; 270, 38; 549, 52;
V 131, 9; 319, 65; 385, 20.

**Peritos** (*uel* Peritios) Macedonum lingua Februarius mensis dicitur
V 232, 39. *V.* menses.

**Peritus** ἔμπειρος II 147, 11 (*cf. margo*); 296, 22; III 177, 23; 249,
65; 374, 33. *Cf.* ἔνπειρος φρόνιμος prudens **peritus** III 331, 41;
519, 29. πολυμαθής II 412, 44. ἐπιστήμων II 311, 16. scientia plenus IV
141, 26; V 132, 1. consultus, sciens, doctus, prudens, sapiens IV 376,
8. **perita** ἐμπειρική III 206, 26. **periti** docti IV 271, 23. *V.*
bono (*uel* bonis) peritus (*ubi* bonisperius *Buech.*). rerum
peritus, iuris peritus.

**Periurium** ἐπιορκία II *p.* XXXVII; 310, 6; 504, 57 (εφ.); 531, 39;
548, 2.

**Periuro** ἐπιορκῶ II 310, 8; III 138, 47; 459, 65. ἐφιορκῶ (!) II 321,
6. ὁρκίζω III 78, 26. **periuras** ἐπιορκεῖς III 138, 48. **periurat**
ἐπιορκεῖ III 138, 49. qui mendo (*uel* mendacio) iurat V 473, 15. *V.*
abiuro, peiero.

**Periurus** ἐπίορκος II 147, 12; 310, 7; III 138, 50; 179, 28; 251, 54;
374, 34; 459, 64; 494, 37. nomen habet R, **peiuro** uerbum non habet V
575, 42. nomen habet R V 525, 7 (*cf. Serv. in Aen.* II 195; *Isid.* X
222; *GR. L.* VII 307, 32). **periuri** false iurantis IV 458, 10 (*Verg.
Aen.* II 195). *V.* peiero.

**Perizoma** corporis obs\<c\>ena tegit gloss. *Werth. Gallée* 341 (*v.
suppl.*). **perizomatum** lumbare uel succinctorium V 510, 2.
**perizomaton** lumbare uel succinctoria IV 140, 7. **perizomata**
praecinctoria IV 553, 3; V 319, 12. cinctoria uel minores bracas V 379,
38. tunica ex foliis consuta, alii lumbarem dicunt V 231, 16 (par.).
*Cf. Isid.* XIX 22, 5.

**Perlabor** διολισθαίνω II 278, 17. **perlabitur** decurrit IV 271, 11.
percurrit IV 553, 11; V 132, 5 (*Verg. Aen.* I 417); 319, 36.

**Perlata** tolerata IV 141, 37; 271, 51; 373, 55; 550, 44; V 320, 29.
perportata V 132, 4.

**Perlateo** διαλανθάνω II 272, 11.

**Perlauia** ποδάνιπτρα (*ubi* ποδόνιπτρα *Vulc.*) II 147, 23. *V.* peluis,
perluuium.

**Per libellum** διὰ βιβλιδίου III 34, 6; 387, 22; 388, 48. διὰ βιβλίου
III 478, 18. δι' ἐπιστολιδίου III 31, 45.

**Per litteras** *v.* promitto, κατὰ στοιχεῖον II 344, 11.

**Perluceo** διαφαίνω II 275, 27. **perlucet** διαυγάζει II 275, 22.
διαφαύει II 275, 29. **perlucere** (perculere?) percutere IV 142, 39.

**Perlucescit** διαφαύει II 147, 26; 275, 29.

**Perlucidum** διαυγές, διαφανές II 147, 21. διαυγές II 275, 23; III
315, 29; 514, 38. διαφανές III 269, 9. **perlucida** διαφανῆ II 147, 22;
III 369, 58.

**Perluo** διακλύζω II 147, 25.

**Perlusio** διαπαιγμός II 273, 14.

**Perlustrat** περισκοπεῖ II 147, 24. peruidet IV 141, 29; 271, 30; V
473, 18.

**Perluuium** ποδονιπτήρ (-της *cod.*) II 410, 53. *V.* perlauia, peluis.
