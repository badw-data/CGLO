**Amphiscii** biumbres V 591, 38; 626, 1 (*cf. Eustath. Hexaem.* VI 8).

**Amphitape** genus uestimenti utrimque uillosum V 339, 56. *Cf. Isid.*
XIX 26, 5; *Non.* 540, 25.

**Amphitheatrum** ἀμφιθέατρον III 11, 11; 84, 45; 173, 9; 302, 71; 372,
15. circumspectaculum V 342, 28; 439, 39. locus spectaculi III 488, 32;
509, 70.

**Amphitrite** dea maris III 488, 33; 509, 71 (dicunt *add.*). est dea
maris, matrona Neptuni *Plac.* V 4, 10 (amphitrites) = V 47, 5. mare IV
16, 32; 308, 29; 477, 50; V 265, 40; 57; 345, 38; 339, 51. *Cf.*
**Amphitrites** oceanum mare (*Ovid. Met.* I 14) V 546, 34.

**Amphitryoniades** Herculis ab Amphitryone uitrico suo IV 481, 3; V
265, 38. **Amphitrides** Hercules, Alcides (arcites *vel* orcidis) V
439, 38; 560, 45.

**Amphora** κεράμιον II 16, 28; 347, 60; 496, 21; 544, 68; III 24, 6;
326, 30; 368, 40; 369, 12; 556, 12; 620, 51. ἀμφορεύς II 492, 33 (*GR.
L.* II 156, 6). ἄμφορον II 521, 32. modii tres III 488, 34; 509, 72. IV
modios tenet V 340, 55.

**Amphrysia** (afri ysia *cod.*) Sibylla V 438, 21 (*Verg. Aen.* VI
398).

**Ampiles** Tuscorum lingua Maius mensis dicitur V 166, 13.

**Ampla manu** V 660, 31.

**Amplector** ἀσπάζομαι II 248, 11. περιλαμβάνω ἐπὶ ἀνθρώπου II 403, 18.
περιπλέκομαι II 404, 10. περιπτύσσομαι II 404, 19. **amplectitur**
amplexat IV 16, 6. amplexat uel adiuuat, complectitur IV 308, 50.

**Amplexabilis** ἁβρότονον II *p.* XII.

**Amplexatus** *v.* amplexus.

**Amplexo** περιπλέκω II 404, 9. futo (= futuo) IV 308, 51.

**Amplexus** participium a uerbo amplector et amplexatus uenit ab
amplexor uerbo passiuo V 520, 3. **amplexa** continens V 266, 32.
**amplexus** autem (*om. G*) et **amplexatus** recte dicimus. nam ab eo
quod est uerbum commune 'amplector' participium facit amplexus
praeterito tempore et ab eo quod est (commune --- est *om. R*)
'amplexor' amplexatus sum facit, ut singula participia uerba sua
separata habeant *Plac.* V 6, 2 = 47, 8.

**Amplexus** περιπλοκή II 404, 11; 487, 21; 509, 5; 537, 5; 549, 14.
συμπλοκή II 443, 1. περιπλοκή, συμπλοκή II 16, 44. περίληψις II 403, 22.
ἀσπασμός II 248, 12.

**Ampliare iudicium** V 660, 18.

**Ampliatus** ηὐξημένος II 16, 41.

**Amplica** *v.* accommodo.

**Amplicitus (!)** κεκολλημένος II 16, 50.

**Amplifico** μεγαλοποιῶ II 365, 50. μεγαλύνω II 366, 12. πληθύνω II
409, 37. **amplificat** dilatat IV 308, 53.

**Amplificus** μεγαλοφυής II 16, 42. **amplificum** ἐξοχώτατον II 304,
35. ὑψηλότατον II 469, 24.

**Amplio** μεγαλύνω II 366, 12. **amplo** ὑψῶ II 469, 31 (*quod tutatur
Woelfflin Arch.* VIII 412: amplio *vulgo*). **ampliat** ὑπερτίθεται II
16, 40. adcumulat IV 308, 52. **ampliari** δειναυξῆσαι II 16, 49.

**Amplitudo** μέγεθος II 16, 43; 366, 17; 498, 30. πλεονασμός II 409,
26. ὕψος II 469, 30.

**Amplius** ἐπὶ πλέον II 310, 18. πλέον II 409, 23. περαιτέρω II 401,
60. saepius, plus IV 308, 54.

**Amplius sapio** ὑπερφρονῶ II 465, 3.

**Amplo** *v.* amplio.

**Amplus** μέγας II 366, 16. ὑψηλός II 469, 23. ὕψιστος II 469, 29.
ὑπερφυής II 465, 2. ὐπερμεγέθης II 464, 32. ἔξοχος II 304, 33. ἄπλατος
II 235, 16. **amplum** εὐμέγεθες II 16, 45. ἔξοχον II 304, 34. ὑψηλόν II
469, 22. magnum, grandem IV 308, 55. patulum, patens, apertum IV 308,
56. **amplam** latam (*Euseb. eccl. hist.* X 12) V 419, 28 = 428, 6.
**ampla** lata, spatiosa IV 471, 23. grandia, magna IV 308, 49.
**amplis** magnis, ingentibus IV 429, 34 (*Verg. Aen.* III 353).
**amplissimus** μέγιστος II 366, 18. ἔξοχος III 275, 42. *V.* amplius.

**Ampulla** λήκυθος τὸ βησίον II 360, 15. λήκυθος II 16, 48; III 197,
38; 270, 45; 326, 64; 65; 368, 41; 439, 36. χερνίβιν II 521, 31. dicta
quasi ampla bulla: similis est enim rotunditate bullis quae ex ispumis
aquarum fiunt atque ita inflantur uento V 166, 14 (= *Isid.* XX 5, 5).
**ampullam** λήκυνθον III 216, 20 = 231, 50 = 651, 10.

**Ampullarius** ληκυθοποιός II 360, 16; III 201, 49; 271, 49; 308, 39;
499, 26; 528, 59. ampullarum factor II 566, 24. *Cf. Plauti Rud. v.* 756.

**Amputatio** κλάδευσις II 350, 2. περικοπή II 403, 13. una lectio,
fiit, (= fitt, *AS.*) II 566, 25: *ubi* uuae sectio *Stowasser Arch.* II
319. *Cf. Schade Ad. Wb. sub* fizza; *Ind. Ien.* 1888/89 *p.* VI.

**Amputator** praecisor ramusculorum siue uineae II 566, 26 (*cf.* II
566, 25).

**Amputo** ἀφαιρῶ II 252, 20. περιαιρῶ II 402, 5. ἐκτέμνω II 292, 51.
περικόπτω II 403, 14. περιτέμνω II 405, 17. **amputat** περιαιρεῖ,
κλαδεύει, καθαίρει, περικόπτει II 16, 46. **amputauit** ἐκλάδευσεν,
περιεῖλεν II 16, 47.

**Amsancti** undique sancti V 338, 26. *Cf. Serv. in Aen.* VII 125; 565.

**Amsedentes** *v.* arsedentes.

**Amterminus** διχόθεν ὅρος II 16, 35.
