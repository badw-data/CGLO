**συγγνώμη** ignorantia, venia.

**συγγομφῶ:** συγγεγομφωμένος condentalis.

**σύγγονος** II, 439, 59 *s. interpr.*

**συγγραφεύς** auctor, conscriptor.

**συγγραφή** conscriptio, syngraphum, *cf.* syngrapha *et* syngraphae
(*expl.* cautiones, cautio vel scriptio, subscriptiones), -αὶ ἀρχαῖαι
monimentum (-a).

**συγγράφω** conscribo.

**συγκάθεδρος** assessor, consessor.

**συγκαθέζομαι** assideo, consideo.

**συγκαθεύδω:** συνεκαθεύδησεν concumbit (concubuit).

**συγκαίω** conflagro.

**συγκαλύπτω** obnubo.

**συγκαλῶ** cieo, cogo, concieo, concito, conclamo, convoco, percito,
ὅταν εἰς πόλεμον συγκαλέσωνται procinctum.

**συγκάμπτω** concurvo.

**συγκατάθεσις** assensio, assensus, consensio, consensus.

**συγκατανεύω** assentio, consentio.

**συγκατατίθημι** assentio, -εμαι assentio, consentio.

**\*συγκαττυστής** concinnator.

**συγκαττύω** concinno.

**συγκείμενος** *etc.* compos, compositus, comptus, concepta, initum, -
τὸν διάνοιαν, ὁ μὴ μεμηνώς compos mentis, διηγηματικῶς -ας facto
conceptas.

**συγκειμένως** contexte.

**\*συγκεκραμένως** attemperate, temperatissime.

**συγκεράννυμι** tempero, συγκεκερασμένος temperatus. *Cf.*
συγκεκραμένως.

**συγκέρασμα** temperamentum.

**\*συγκερασμός** temperies.

**συγκεραστός** temperatus, - όν aqua calda, calda aqua, caldativa,
temperatus (-um).

**συγκεφαλαίωσις** aequatio.

**συγκίνησις** exagitatio.

**συγκινῶ** commoveo, concieo, concito, -ηθείς concitatus.

**συγκιρνῶ** commisceo, tempero.

**συγκλείω** *et* **συγκλήω** concludo.

**συγκληρονόμος** coheres.

**σύγκληρος** consors.

**συγκλήρωσις** consortium.

**συγκλητικός** senator, senatorius, *cf.* syncletus (-ticus), - βίρρος
ἔχων πορφύραν paenula, -ὸν ἔνδυμα praetexta, -ὸν φόρημα toga, -ὸν φόρημα
ὁλοπόρφυρον praetexta, -ὸν ὑπόδημα cothurnus.

**σύγκλητος** convocatio, senatus (*cf* syncletus), -ου δόγμα senatus
consultum.

**συγκλῄω** *v.* συγκλείω.

**συγκλίνω:** συνεκλίθη concumbit (concubuit).

**σύγκλυδες** οἱ ἐκ πολλῶν συλλεγέντες τόπων II, 440, 32 *s. interpr.*

**συγκλῶ** confrango.

**σύγκοινος** communis victor.

**συγκοιτάζω:** συνεκοιτάσθησαν concumbit (concubuissent).

**\*σύγκοιτις** concubina.

**σύγκοιτος** concuba, concubina.

**συγκόλλησις** conglutinatio.

**συγκολλῶ** conglutino, συγκεκόλλημαι cohaereo, συγκεκολλημένος
cohaerens.

**συγκομιδή** ἡ συνάθροισις cotlectus 1.

**συγκοπή** collisio, *cf.* syncope (*expl.* defectio stomachi,
detractio). *Cf.* cardiacus.

**σύγκοπτα** faratalia.

**συγκόπτω** concīdo, condico, contamino, contundo, decīdo, tundo.

**σύγκρασις** temperamentum, temperantia, ᾠδῆς - concentus.

**συγκρίματα** concretiva.

**συγκρίνω** comparo, confero.

**σύγκρισις** cerniculum, collatio, comparatio, - δείπνου collatio.

**συγκριτικόν** comparativum, **συγκριτικῶς** comparative.

**συγκρότησις** favor, sustentatio.

**συγκροτῶ** concinno, condenso, confero, confligo, contineo, sustento,
ventilo, -ηθέν confotum, συγκεκροιημένος *etc.* agitatus, conflatam.
*Cf.* ἀκρόασις.

**σύγκρουσις** conflictio, confligatio.

**συγκρούω** concito, confligo.

**σύγκτησις** compossessio, massa, saltus.

**\*συγκτήτωρ** compossessor.

**συγκυρῶ** attingo.

**\*συγνωσαι** (\*συχνῶσαι *Buech.*) degunere.

**συγξύω** corrado.

**συγχαίρω** congaudeo, - *et* -ομαι gratulor.

**\*συγχέζω (?)** *cf* concacasti.

**συγχέω** confundo, permisceo, συγκεχυμένος confusus.

**συγχρονίζω** commoro (-or).

**σύγχρονος** coaevus.

**συγχρῶμαι** *et* **συγκέχρημαι** coutor.

**σύγχυσις** colluvium, confusio, - ὁράσεως caligo, - ὁρκίων nuncupatio,
εἰς -εις ad confundendum.

**συγχωνεύω** conflo.

**συγχωννύω** obruo.

**συγχώρησις** concedentia, concessio, inclulgentia, indutia, remissio,
κατὰ ἀξίωσιν καὶ συγχώρησιν precario.

**σύγχωρος** confinis, conforaneus.

**συγχωρῶ** concedo, indulgeo, remitto, sino, -ηθείς remissus.

**σύειον** suilla (-um).

**συζευγνύω** *vel* -υμι (*vel* συνζ.)^1^ conecto, coniungo, copulo, -
αἵματι coniungo,
