liber (conprehensus: *om. cd*) IV 43, 55. **commenticias**
adinuenticias IV 221, 3. adinuentas V 447, 37 (*v.* commentaticias).

**Commentior** καταψεύδομαι II 345, 16.

**Commentor** σοφίζομαι II 435, 1. τεχνάζομαι II 454, 46. ὑπομνηματίζω
II 467, 6. **commentatur** σοφίζεται II 104, 51. commemoratur IV 500,
13. **conmentabar** commemorabar (!) V 279, 52. **conmentare** (*vel*
comm.) conponere, adinuenire IV 43, 9. conponere aut inuenire IV 500, 8.
**commentatus est** mentitus est (*v.* commentum) IV 221, 16.

**Commentor** καταψεύστης III 334, 69; 497, 6; 527, 60. expositor (*v.*
commentator) IV 408, 16.

**Commentum** (*vel* comm.) τέχνασμα II 454, 47. σόφισμα II 104, 13
*mrg.*; 104, 52; 435, 3. ποίημα II 411, 15. ἐνθύμημα, σόφισμα II 104,
13. ἐνθύμημα II 105, 21. ἐπίνοια II 105, 43; 310, 1; 503, 6. cogitatio,
uersutia II 574, 23. **cementum** mendacium, cogitatum IV 33, 44; 218,
6; 317, 28; V 275, 62. mendacium V 494, 18. **commentum** (*vel* conm.)
commune mendacium IV 35, 40. commune mendacium et librorum expositio IV
221, 19. quod fi\<n\>xit mendacium IV 320, 48. quod confi\<n\>xit
mendacium V 280, 46. mentitus est (*contam. v.* commentor) uel commune
mendacium, quod confinxit mendacium V 279, 8. commune mendacium,
excogitatio uel conpositum IV 500, 9. con­cinnatum IV 42, 16. figmentum V
531, 39 (*Ter. Andr.* 225). molitum, machinatum IV 500, 10.
excogitatum IV 38, 51. argumentum, similitudo IV 220, 29. librorum
expositio IV 434, 42. commonitum V 639, 31 (= *Non.* 88, 22).
**comnientus** cogitatio IV 320, 49. **commento** τεχνάσματι II 106, 21.
**commenta** plura significat, dicimus enim comminisci 'crimen
confingere' (figere *G*). dicimus et commenta interpretationes
commentariorum, ut commenta iuris, commenta Virgilii *Plac.* V 13, 15 =
V 56, 14 (*cf. Isid.* VI 8, 5). astutiae, machinationes IV 35, 41; V
279, 10. astuta, macinationes IV 500, 6; fraudes IV 43, 53; 44, 7
(*abc*); 221, 13; 320, 43; V 279, 23. fraudes, argumenta IV 500, 7.
argumenta V 279, 32. excogitata (*Euseb. eccl. hist.* I 6) V 419, 45 =
428, 29. **commentorum** διανοημάτων II 105, 20. **commentis** fraudibus
IV 44, 43. searuum uel ordoncum (*AS.*) V 354, 3.

**Commentus** excogitatus IV 500, 11.

**Commeo** διοδεύω II 278, 7. συνοδεύω II 446, 60. συμφοιτῶ, φοιτῶ II
105, 28. **commeat** συμφοιτᾷ, φοιτᾷ II 105, 8. uenit IV 43, 12. simul
se (?) pergit IV 320, 41. simul ambulat IV 220, 48. iter agit IV 220,
45. **commeet** φοιτάτω, ἐρχέσθω, κατατρεχέτω II 105, 16. **conmeare**
(*vel* comm.) ambulare, uenire IV 42, 1. simul ambulare IV 44, 41. iter
agere IV 220, 46.

**Commercantur** merces coemunt IV 38, 33; 40, 10. mercibus merces
coemunt V 183, 41.

**Commercator** συνέμπορος II 445, 21.

**Commerciarius** συνωνητής II 448, 49.

**Commercium** (*vel* conm.) συναλλαγή, πανήγυρις II 104, 43. συναλλαγή,
ἐπιμιξία II 110, 23. συνωνή, ἐπιμιξία II 503, 8. ἐπιμιξία II 309, 45;
547, 7. συνάλλαγμα πρὸς πολεμίους γινόμενον II 444, 1. συνάλλαγμα III
478, 50. συνωνή II 448, 48. quod est negotium per duo mm scribendum
*Plac.* V 56, 15. commutatio mercis IV 434, 43 (*cf. Verg. Aen.* X 532).
commutatio mercium V 279, 19. mutatio IV 220, 43. commutationem V 495,
61. **commercia** id est mysteria (ministeria?) V 547, 19.

**Commergo** (comm.) βυθίζω II 260, 40.

**Commerui** (*vel* conm.) male merui aut offendi IV 37, 41. male egi V
550, 43 (*Ter. And.* 139). offendi IV 39, 8; 501, 3.

**Commerus** (?) puplice V 279, 31 (commune? comminus *H.*).

**Commetare** (conm.) conmeare V 639, 42 (*Non.* 89, 28).

**Commic\<t\>ilis** (conm.) conpositi V 638, 69 (*Non.* 83, 3).

**Commigro** (conm.) μετοικῶ II 370, 12.

**Commiles** (conm.) συνσρατιώτης II 447, 52. *V.* auxiliarius.

**Commilito** (conm.) συνστρατιώτης III 6, 38. compar II 575, 14.
militiae socius V 183, 42.

**Commilito** (conm.) συνστρατεύομαι II 447, 51. conmilitat
συνστρατεύεται II 110, 30.

**Comminando** intentando IV 320, 50; V 596, 17.

**Comminatio** ἀπειλή II 106, 22; 234, 1.

**Comminator** diabulus, adcontrarius (ac contr. *a*, id est contr.
*H.*) II 574, 31.

**Comminicipes** (conm.) ciues V 183, 43. *Cf. Loewe Prodr.* 93.

**Comminiscor** (conm.) ἐννοοῦμαι II 299, 46. **comminisco** σοφίζομαι
II 435, 1. ὑπομιμνήσκω II 467, 4. **comminiscitur** (*vel* conm.)
σοφίζεται II 110, 27. confingit IV 37, 48; 39, 36. commemoratur,
recordatur aut confingitur IV 37, 55; 500, 47 (confidetur). recordatur,
commemoratur *cod. Werthin.* (*cf. suppl.*) *post* V 279, 9.
commemoratur IV 221, 17. recordatur IV 322, 52. **comminiscit**
