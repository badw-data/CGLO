ἡ ὑφή, ἡ τάξις (eife et axis *cod.*) II 513, 60. uirtus II 595, 23. ordo
sermonis IV 183, 35; V 156, 49. textus epistulae IV 396, 38; V 398, 9.
mensura, ordo IV 291, 7. **tenorem** σχέσιν II 196, 46. **tenore**
περιοχῆς, δυνάμεως II 196, 47. ordine uel statim [tricennalibus: *novae
gl. initium*?] V 397, 32.

**Tensa** θρόνος II 196, 50 (*cf. Festus p.* 364, 10). genus uehiculi IV
183, 10; 291, 3; 573, 14; V 248, 13. **thensae** ἅρμα θεῶν II 198, 18
(*Arch.* XI 323). **tensae** θρόνοι II 196, 51.

**Tensore** *praef. anthol.* (V *praef.* VI: tems.). *V.* tentores.

**Tensus** tesus IV 182, 27; 573, 9; V 485, 53; 516, 32. *V.* taesus
*et* tentus 1.

**Tenticum** sprindil (*AS.*) V 397, 2 (tendiculum *Schlutter referens
ad Aldhelm. de laud. virg.* XIII *p.* 14, 8).

**Tentigo** τόνος, τάσις II 196, 49. libido V 655, 3 (*Iuvenal.* VI
129). tenacitas uentris V 426, 57 (*Euseb. eccl. hist.* I 8). tenacitas
uentris, id est ebind (*AS.*) V 418, 15. infirmitas uentris V 418, 15.
infirmitas uentris V 395, 28. **tentigine** τῇ διατάσει II 196, 48 (*cf.
Hor. sat.* I 2, 118). *Cf.* timaci. *V.* teritigo, testricum.

**Tentio** τάσις II 196, 52; 451, 58.

**Tentipellilim** φάρμακον πρὸς ῥυτίδας II 196, 54. *Cf. Fest. p.* 364,
16.

**Tento** συνέχω II 445, 56. τείνω II 452, 34. τείνομαι II 452, 33. *V.*
tempto.

**Tentores** ἀφέται II 196, 53. *V.* tensore.

**Tentorium** σκηνή, σκήνωμα II 196, 55. σκήνωμα *margo* II 196, 55.
σκηνή II 433, 22; 505, 63; 532, 47. casa militaris uel tabernaculum IV
182, 26 (*cf. cod. Werth. qui addit* quod dicunt mil\<it\>es papiliones
*Gallée* 354; *v. suppl.*). casa militis IV 396, 43. papilionem V 395,
22. **tentoria** σκηνοπήγια στρατιωτικά II 433, 26. tabernacula IV 466,
31 (*v.* tabernaculum *et Verg. Aen.* I 469). papiliones IV 182, 25;
291, 4; V 156, 41. papiliones, tabernacula IV 396, 42. tabernacula,
papiliones uel casa militaris uel tabernacula IV 573, 8.

**Tentus** τεταμένος II 453, 39. **tenta** extensa V 648, 24 (*Non.*
181, 22).

**Tentus** κατασχεθείς II 344, 23. κατεχόμενος, II 346, 3.

**Tenui hauena** fistula: uulgo fiscla dicitur V 248, 14 (*Verg. Ecl.*
I 2).

**Tenuis** ἰσχνός II 197, 1. λεπτός II 359, 42; III 329, 47; 473, 57;
499, 8; 529, 44. ἀραιόθριξ II 196, 43 (tenius *inter* teni *cod.*).
leuis IV 184, 1. subtilis, tener IV 396, 39. gracilis IV 291, 6.
fragilis IV 183, 47. subtilis IV 183, 36. λεπτή II 359, 36. **tenue**
ψιλόν II 481, 1 (*v.* babylonicum). **tenues** ἰσχνούς, ἀσθενεῖς II 196,
56. **tenuia** λεπτά III 369, 54. tenuissima. Virgilius (*Georg.* I
397): 'tenuia nec lanae per caelum uellera ferri'. item (II 121):
'uelleraque ut foliis depectant tenuia Seres' V 156, 43; 248, 15.

**Tenuis res** dubias uel infirmas res IV 466, 29 (*Verg. Aen.* V
690).

**Tenuitas** λεπτότης ἐπὶ σώματος II 359, 43. fame\<s\>, ieiunia IV 396,
40.

**Tenum** *v.* telum.

**Tenuo** λεπτύνω II 359, 47.

**Tenus** πέρας, μέχρι II 197, 2. ἄχρι II 254, 42. μέχρι II 370, 28.
usque uel fine IV 396, 44. usque, aliter fine IV 182, 47; 573, 20;
*gloss. Werth. Gallée* 353 (*v. suppl.*). usque uel iuxta IV 183, 15;
573, 19; V 156, 48. usque IV 291, 2. usque nunc uel finis (*scr.* fine)
V 485, 47; 516, 28 (telurum *codd.*). praepositionem Virgilius metri
necessitate genitiuo plurali iunxit, ut (*Georg.* III 53): crurum tenus
a mento palearia pendent V 248, 16. *V.* aliquatenus. *Cf. GR. L.* VII
354, 15 *sqq.*; *Woelfflin Arch.* IX 15.

**Tenus** extrema pars arcus V 396, 22. laqueus dictus quod teneat uel
quod intendatur V 651, 48 (*Non.* 6, 12).

**Tepefacio** χλιαίνω II 477, 24. **tepefio** χλιαίνομαι II 477, 23.

**Tepeo** χλιαίνομαι II 477, 23.

**Tepesco** χλιαίνομαι II 477, 23. **tepescit** χλιαίνεται II 197, 4.

**Tepido** χλιαίνω II 477, 24.

**Tepido lacte** recenti IV 466, 32 (*Verg. Aen.* III 66).

**Tepidarium** χλιαροψύχιον II 477, 26. **tepidaria\<m\>** (*scil.*
cellam) προπνιγέα (*scil.* οἶκον) III 217, 2 = 652, 10 (προπνιγεῖα
tepidaria *H.*).

**Tepidus** χλιαρός III 164, 15; 255, 47; 336, 23; 498, 34; 562, 17
(exiliaron). **tepidum** χλιαρόν II 197, 5; 477, 25; III 15, 15; 81, 3;
87, 63; 218, 12 = 233, 21 = 653, 11; 315, 32; 364, 44; 378, 65; 398, 30;
420, 8; 465, 28; 491, 35. **tepida** χλιαρά III 420, 9. χλιαρόν (*scil.*
aqua ὕδωρ) III 184, 32.

**Tepor** χλιαρότης II 539, 42; 551, 65. calor IV 466, 33 (*gl.
Verg.*?). **tepore** calore IV 574, 5; *gl. Monac.* (tefore *cod. cf.
suppl.*).

**Ter** τρὶς τὸ ἐπίρρημα II 459, 43. τρίτον II 459, 52.

**Teramne** (= Therapne) locus est V 581, 1.

**Teraphim** figurae III 504, 24; 521, 27 (= *Eucher. instr. p.* 146,
7). *Cf. Onom. sacr.* 48, 4.
