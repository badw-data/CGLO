**δίσκος** discus, lanx, satura. *Cf.* ποιητός.

**δισκοφόρος** dapifer.

**δισσολογία** *cf.* dissologia.

**δισσολογῶ** bis verbum.

**δισσός** *et* **διττός** bifax, bilinguis, dubienus, geminus, -οὶ θεοί
geminus (-i), δισσὰ τοῦτ' ἔστι δύο bina. *Cf.* dissum.

**δισσῶς** *et* **διττῶς** bifariam, varie.

**δισταγμός** dubitatio, dubius (-um), haesitatio, varicatio, ἄνευ -οῦ
sine cunctatione, sine dubitatione.

**διστάζω** dubito, haesito, retracto, -ει ambigit, οὐκ ἐδίστασα non
dubitavi.

**\*δίσταθμος** dupondium (dipundius).

**διστακτικός** dubitans.

**διστακτικῶς** dubio, dubitanter.

**διστεγἑς** οἴκημα superficium.

**δίστομος** anticeps. *V.* ἀξίνη, πέλεκυς (= bipennis).

**δισχίλιαι** γυναῖκες duo milia mulierum.

**δισχίστως** bipertito.

**δίσωμος** bimembris.

**δίτροχον** birotus.

**\*δίτρυποι** bifores.

**\*δίτρωτοι** bivolnes.

**δίυγρος** humidus, liquidus, madidus, roscidus.

**διυλίζω** defaeco, delinquo, eluo, faeco, percōlo, sacco, -ει
deliquat, διυλισμένον siccatio.

**διυλιστήρ** qualus.

**διυπνίζω** expergefacio, exsomnio, suscito, *-*ομαι, expergiscor,
exsomnio.

**διφασίως** bifariam.

**διφθέρα** membrana, pellis, segestrum, -αι pellicula, -α τὸ βιβλίον
caudex (codex), membrana, - πλοίου segestrum.

**\*διφθεράρι(ο)ς** membranarius.

**\*διφθεροποιος** membranarius.

**δίφθογγον** duplum.

**δίφορος** bifer.

**δίφραξ** soliar.

**διφρί(ο)ν** sella.

**δίφρος** sella, sella curulis, solium, ἀγκυλόπους δ. βασιλικός curulis
sella, δίφρῳ σύρεται curru trahitur. *Cf.* diphrum (*expl.* tribunal
eburneum).

**διφροφόρος** sellarius.

**διφρυγές** *cf.* defriges (*expl.* confectio).

**διφυής** biceps, geminus.

**δίχα** absque, citra, praeter. *V.* μερίζω, ὄχημα.

**\*διχαίτης** disulcis.

**διχαλ(λ)ον** furcilla.

**\*διχάνοικτος (?)** bipatens.

**\*δίχειλον** bilabrum.

**δίχηλος** (ὀρνέου εἶδος) turbelix.

**διχογνωμονῶ** delibero.

**διχόθεν** *v.* ἀνοίγω, ὄρος.

**\*διχονοητικῶς** discordale.

**διχόνοια** discessio, discidium, discordia (*cf.* discors), dissensio,
dissensus, distantia, dividia.

**διχόνους** discors.

**διχονοῶ** discordor, dissentio, -ῶν discors, distans.

**διχοστασία** discessio, discordia, dissensio.

**διχοστατῶ** dissideo.

**διχοτόμημα** *cf.* de dichotomematibus. **δίχρους** discolor.

**δίχρωμος** bicolor, bifax, discolor.

**\*δίχρωτος** bicolor.

**διχῶς** bifariam, bifarie, bipertito, difariam, dupliciter, varie.
*Cf.* μερίζω.

**δίψα** sitis.

**δίψακος** cardo niger, dictamnus, siticula.

**διψαλέος** siticulosus.

**διψάς** siticula, *cf.* dipsas (*expl.* serpens *sim.*). *Cf.* ὕδρα,
ὕδος.

**διψῶ** sitio.

**διψώδης** torridus.

**διώβολον** dupondium, -ου diobolares.

**διωγμός** grassus, indago, persecutio.

**διώκτης** persecutor, *cf.* dioctes (*expl.*
operis impulsor). *Cf.* ἐργοδιώκτης.

**διώκω** insequor, sector, sequor, - κατ' ἔχθραν persequor, -ων actor.

**διώνυμος** *cf.* dionymus (*expl.* qui duo nomina habet).

**δίωξις** persecutio.

**διωρία** intercapedo, spatium, - διαπόντιος transmarina latio (=
dilatio).

**διώροφον** οἴκημα duorum pavimentorum domus.

**διωρυγή** *sim. v.* διορ-.

**διῶρυξ** canalis, cuniculus, traductio.

**\*δλειμα** vulnus.

**δόγμα** consultatio, consultum, decretum, dogma, scitum, secta,
suffragium, - δήμου plebiscitum, populiscitum, - συγκλήτου senatus
consultum.

**δογματίζω** censeo, consulto, decerno. *Cf.* dogmatizo.

**δογματικός** consultus. *Cf.* dogmatica.

**δοθιήν** aurunculeus, furunculus.

**δοθιών** furunculus.

**\*δοιάς** duitas.

**δοίδυξ** pistillum.

**δοκάριον** assarium, contus, - κεκαπνισμένον insuasum.

**δόκησις** existimatio, visio.

**δοκιμάζω** aestimo, approbo, comprobo, examino, expendo, inspecto,
perpendo, probo, τὸ -ον τὸν χρυσὸν ὅταν παρατρίβηται obsus, -όμενος
spectatus, δοκιμασθείς *etc.* examinati, perpensas, temptatum, trutinata
(*cf. etiam* inspecto), δεδοκιμασμένος probatus.

**δοκιμασία** aestimatio, approbatio, ar-
