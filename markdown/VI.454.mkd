553, 3. σωλήν, pandiros (= πανδουρίς?) II 518, 36. σωλήν, σῦριγξ,
φυσητήρ II 72, 22. σῦριγξ III 79, 25; 171, 16; 204, 6; 207, 24; 238, 65;
368, 36; 469, 13. ciringos III 537, 44. cirizis III 556, 22. curizi III
621, 5. σῦριγξ ὁ αὐλός II 448, 57. ὄργανον II 386, 20. οὐρήθρας ὀπή II
390, 13. σῦριγξ οὐρήθρας II 448, 58. egilopas (αἰγίλωψ) **fistula**
secus oculum nata III 600, 49. eolopia **fistula** III 600, 40.
cadeinopio (?) **fistula** III 598, 21. **fistula** pictis (πηκτίς
*Buech.*) cyris (σῦριγξ?), id est musa V 456, 62. *V.* tenui hauena.

**Fistularius** ὀργανάριος II 386, 18. **fistolarii** *v.* auceps.

**Fistulator** συριστής II 448, 59; III 10, 48 (fisc.); 84, 23; 302, 40;
371, 78. *Cf.* II 580, 22 (fisculator).

**Fistulo** sibilo V 501, 8. **fistulor** sibilo V 201, 7; 599, 50.
sibilor IV 238, 37; 519, 6. sifilor IV 75, 41; 77, 2. sibilor [fibus
sol] V 201, 8 (*cf.* Phoebus).

**Fisus** ualde fidus IV 519, 11. ualde fidens V 294, 49. *Cf.* fidus.

**Fitilium** aeger gelu (*AS.*) *Epin. post* V 360, 49 (*scr.*
uitellum).

**Fitilla** olla, χύτρα II 479, 31 (*ubi* fictile *e*: fritilla?).

**Fitilla** (*ita a*, fid. *reliqui*) cibi genus ex farre IV 76, 47 (uel
olera, pultes cum larido in sacris gentilium *add. a*). *Cf. Arnob.* II
21; VII 24; *Arch.* X 513.

**Fixio** πῆξις II 407, 32. καθήλωσις II 335, 7.

**Fixor** *v.* fictor.

**Fixus** πῆξις II 407, 32.

**Fixus** πηκτός II 407, 21. immobilis siue stabilis IV 76, 31 (*Verg.*
*Aen.* VII 291?). fixum πεπηγὸς ἐπὶ ξύλου ἢ ἄλλης ὕλης II 401, 35.
inmobilem, stabilitum IV 76, 37. firmum IV 343, 12.

**Flabanus** suan (*AS.*) V 361, 4.

**Flabarius** custos porcorum (?) V 600, 26 (*= Osb.* 246): *ubi* custos
tororum *Graevius*, flabrarius custos corporis *De-Vi*t.

**Flabello** ῥιπιζω II 428, 18; III 157, 58; 269, 72.

**Flabellum** ῥιπίς II 503, 56; III 21, 4; 157, 59; 197, 58; 269, 70;
321, 49. ῥιπιστήρ II 543, 58; III 366, 26. ῥιπιστήριον II 72, 23; 428,
19; III 269, 71/70. uenticapium IV 77, 34 (*Ter. Eun.* 595); V 457,
17; 501, 16. muscarium V 295, 44; 628, 72. *V.* muscarium.

**Flabra** *v.* flagrum.

**Flabrum** φύσημα II 474, 5; III 448, 64; 481, 61. aurae incitamentum
IV 77, 7. aurae incitamentum aut aura IV 517, 10. praecipitium,
incitamentum aurae V 501, 9. praecipitium V 456, 63. **flabra** φυσήματα
III 426, 37. φυσήματα uel uenti IV 237. 58; V 569, 6. **flabri**
fabulosi (flabrosi?) uenti IV 517, 8. fabulosi, uentosi IV 237, 43.
uentosi, fabulosi V 499, 70. fabulosi, [in]uentosi V 599, 56.
flabulosi, uentosi [perfecti] V 599, 28 (*v.* fabrum). fabulosi,
uentosi [perfecti] V 628, 70. **flabris** famulas (flabellis?) aut
uentis IV 517, 9. fabulas (flabellis?) V 295, 20. uentis, tempestatibus
V 295, 17; 501, 9. uentis temperantibus (*vel* tempestatibus) IV 77, 28
(*Verg. Aen.* III 199?). uentis temperantibus IV 77, 9; 517, 6
(flagris). *Cf.* **fuluum** uestis temperantibus V 297, 7. **flabris**
uentis siccis (= calabris?) IV 237, 44; V 569, 7; 628, 71.

**Flaccentia** contracta V 360, 59. ar[d]entia, sicca, absque humore V
501, 18. are sicca, id est sicca (= arentia, id est sicca), absque
humore V 569, 8.

**Flaccesco** μαραίνομαι II 364, 57.

**Flaccet** languet V 641, 41 (*Non.* 110, 11).

**Flaccida** putrida farina uel marcida V 457, 1. **flaccidum**
contractum V 360, 18. seruum V 416, 32 (*de verb. interpret. = Hieron.
in Matth.* 13, 32).

**Flaccus** γλίσχρος III 334, 17; 512, 50. ωτοσκλαδιας (ὠτοκλαδίας?) III
330, 45. *V.* auriflaccus.

**Flagellaticius** μαστιγίας II 365, 10. flagellis deditus II 580, 29.

**Flagellator** *v.* quaestionarius.

**Flagello** uerbero μαστιγῶ II 365, 11. **flagellat** uerberat uirgis
IV 343, 16.

**Flagello** copidermos V 457, 2. copidermos, uerbero V 501, 10 (*cf.*
flagriones). *V.* casabus.

**Flagellum** μάστιξ II 365, 14; 503, 55; 547, 33; III 24, 28; 174, 6;
194, 36; 241, 13; 273, 28; 326, 72; 339, 25; 370, 41; 448, 65; 499, 64;
530, 45. **flagelli** μάστιγες II 72, 24. **flagella** μάστιγες III 407,
9. *V.* flagrum.

**Flagias** *v.* flagrum.

**Flagitatione** (flaratione *vel* flagratione *codd.*) petitione V 361,
18.

**Flagit\<at\>or** ἐκζητητής II 290, 11 (*suppl. a e*).
**flagitatores** exactores V 295, 36.

**Flagitias** *v.* flagrum.

**Flagitiata** (!) scelerata V 457, 3.

**Flagitiosus** ἄτοπος II 250, 21. φιλοδάρτης plagosus, **flagitiosus**
(? *cf.* flagrum; flagellosus *Hagen progr. Bern.* 1877 *p.* 13) III
336, 13. **flagitiosus** criminosus IV 237, 45. uitiosus, libidinosus,
turpis IV 343, 22. inhonestus, dolosus, malitiosus IV 343, 23.
inuicliosus, cri-
