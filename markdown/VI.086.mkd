**Aquam manibus** ὕδωρ πρὸς χεῖρας III 439, 56; 474, 2. **aqua\<m im\>
manus** νίψασθε (-σθαι *Scal.*) II 20, 26 (*add. Buech.*). *Cf.* δὸς
ὕδωρ εἰς χεῖρας da aquam ad manus III 514, 71.

**Aqua mellita** ὑδρόμελι III 255, 53. **aqua mel\<lit\>a** ὑδρόμελι III
184, 58.

**Aqua mulsa** ὑδρόμελι II 462, 16; 496, 23; 521, 33; III 315, 47. *V.*
mulsum, hydromeli.

**A quanam** *v.* quisnam.

**Aqua naturalis** arcia clitteca (?) III 597, 11.

**Aqua paucum** (aquae?) ὕδωρ ὀλίγον III 110, 14 = 640, 7 (aquam
paucam).

**A quapiam** *v.* quispiam.

**A quaqua** *v.* quisquis.

**A quaquam** *v.* quisquam.

**A quaque** *v.* quisque.

**Aquarioli** βαλλάδες II 20, 30. V ouariolus, bacario.

**Aquarium** ὑδραγώγιον II 20, 24 (aquagium *Scal.*); 462, 5. ἐπίχυσις
II 502, 42. lauatio (*male vers.*) II 567, 21. *V.* aquale.

**Aquarius** ὑδροφόρος II 462, 22; 559, 2. ὑδροχόος III 400, 53. *sign.*
*c.*: III 72, 44; 170, 27; 242, 6; 292, 30; 425, 38. *animal:* III 260,
1 (*add.* ὕδρος); 376, 37. ὑδρηχόος II 462, 13. *sign. c.*: III 30,
12. *animal*: ὕδρος III 190, 5; 305, 24. *Cf.* **aquarius** ὑδρηχόος,
ὑδροφόρος II 20, 25. aquar\<i\>um III 193, 68/9. melastinus
(mediastrinus *Keil*) V 652, 23 (*Iuvenal.* VI 332).

**Aqua salsa** ὑδράλμη II 462, 8; III 184, 15; 245, 53. almedis
(ἁλμῶδες?) III 551, 6.

**Aqua siderizusa** id est aqua ferruginea III 536, 20; 550, 15.

**Aqua stypterizusa** id est aqua quo (!) alumen lauatur III 550, 21;
*cf.* III 536, 27.

**Aqua sub cute** ὕδρωψ II 20, 29.

**Aqua suppellis** aquaticus serpens II 567, 26 (*cf.* intercus et
*Loewe GL. N.* 16).

**Aquatam** po[si]tionem recte dicimus quae aquam aliunde recipiat,
**aquosum** autem locum qui ex se aquam fundit *gl. apud Loewe GL.*
*N.* 154 (*ubi* aqua populus *ex Eucher. form.* 20, 11 *illatum est*).
*Cf. GR. L.* VII 264, 27; *Isid. Diff.* 41.

**Aquatica** ὕδρος III 19, 18; 91, 11. ὕδρος ἡ διψάς III 433, 12
(sitiuncula *add. m. 2*). **aquaticus** hydrus IV 310, 13; V 590,
47.

**Aquatilis** ἔνυδρος II 300, 59. ὕδρος ὁ ὄφις II 462, 19. **aquatile**
ἔνυδρον II 300, 60.

**Aquatio** ὑδρεία II 20, 27; 462, 14.

**Aquator** ὑδρευτής II 462, 12.

**Aquatus** ὑδαρής II 461, 59. potio aquatica II 567, 23 (*cf. Loewe
GL. N* 16). **aquatum** ὑδαρές II 461, 58; III 15, 22; 87, 70; 184,
60; 255, 40; 315, 39; 364, 61; 378, 72; 398, 32 (ydarce). ὑδαρῆ
**aquatum** III 364, 46. *V.* aquatam, aquosus.

**A quibus** in ablatiuo casu numero hominum IV 406, 6. *V.* quis.

**A quibusdam** *v.* quidam.

**A quibusnam** *v.* quisnam.

**Aquiductium** ὑδραγώγιον II 462, 5. ἀγωγός III 326, 71.

**Aquila** ἀετός II 20, 34; 219, 14; 492, 35; 517, 19; 540, 2; 552, 27;
553, 55; III 17, 38; 89, 58; 170, 10 (*sign. caeli*); 187, 59; 241, 48
(*s. c.*); 257, 42; 293, 34 (*s. c.*); 318, 66; 360, 11; 40; 397, 28;
400, 69; 435, 28; 487, 26; 507, 11; 551, 37. **aquilae** segnas (*AS.*)
V 341, 22.

**Aquila marina** ἀετὸς θαλάσσιος II 20, 36.

**Aquilex** ὑδραγωγός III 309, 51; 439, 57; 478, 29. homo aquarius II
567, 27. aquam colligens V 637, 6 (= *Non.* 69, 14). **aquilices** οἱ τὰ
πάρυγρα (πυραγρα *cod. corr. H.*) ἐργαζομενοι, οἷον ὀχετάριοι,
ποταμῖται, ὑδροσκόποι II 18, 45 (οἱ τὰ ὑδραγώγια ἐνεργαζόμενοι, οἱ
ἀνοχετάριοι *Scal. ad Fest. v.* aquaelicium). **aquilici** scrutatores
aquarum V 342, 5. *V.* harenulcaes.

**Aquiliata** βοόφθαλμος III 437, 15 (*inter pisces*). *Cf. Plin.*
XXXII 145.

**Aquilifer** ἀετοφόρος II 20, 31; 219, 15; III 208, 17; 353, 20; 498,
74. id est aquilae signum alegin (*ubi* a legione *m. rec.*) V 267,
11. aquiliferi ἀετοφόροι II 20, 38; III 298, 31.

**Aquilinus** ἀετώδης, γρυπός II 20, 32 (*cf. Loewe Prodr.* 271).

**Aquilo** βορρᾶς II 20, 35 (aquilio); 258, 47. βορρᾶς, βορέας II 18,
28. βορέας III 11, 19; 84, 57; 172, 8; 295, 13; 354, 15; 395, 69; 400,
57; 426, 46; 511, 15. uentus IV 19, 37 (= *Eucher. form.* 11, 16).
ἀπαρκτίας III 245, 39.

**Aquilus** φαιός II 469, 44. color niger (?) proximus V 632, 30 (*GR.*
*L.* VII 108, 1 *ubi* nigro). **aquiluus** fuluus (= furuus) bruun
(*AS.*) locar (?) II 567, 22: *cf. Loewe GL. N.* 16. **aquilum**
μελανόν ὡ\<ς\> Λουκίλλιος II 20, 37 (*inc. fr.* LXXIX *L. M.*).
fuscum, nigrum IV 19, 36 (aquileum *cod.*); 208, 26; 310, 14; 480, 32; V
167, 42 (aquileum); 267, 9; 541, 9. fuscum, nigrum, atrum V 440, 54
(aquileum). fuscum uel subnigrum V 338, 38 (aquilium). *Cf. Loewe
Prodr.* 273, 296, 402, *Festus Pauli p.* 22, 2. aquileum = aquilium?
*cf. Ott Fleckeiseni Ann.* CXVIII *p.* 423. *V.* di aquili.

**Aquilo corpore** nigro corpore V 167, 43. *Cf. Plaut. Poen.* 1112.

**Aquiminale** χέρνιβον II 18, 46. trulleum et gutum et **aquiminale**
χέρνιβον, ξεστόν (ξέστην *d.* χερνιβόζεστον *Mau Pauly-Wissowa sub*
aquaeman.) II 202, 31.
