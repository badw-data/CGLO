**Prolictabimini** dicitur et multa significat: nam prolici est
persuaderi *Plac.* V 38, 10 (prolictabimi) = V 95, 2 (uerbum autem eius
est prolector *add.*) = V 138, 35 (*item*). *Cf. W. Heraeus Arch.* VI 555
(**prolecta** bene dicitur et inuitata sign.) et *Stowasser 'Zeitschr.
f. östr. Gymn* V XLII *p.* 96. **prolicta** carmine d. et multa (=
mulcta, mulsa) *Buech.* **prolicit amor uini** dicit et inuitat s.
*Bugge Op. ad Madv. p.* 177 *coll. Plaut. Curc. v.* 97.

**Prolixe** προελκυσμένως, αὐτάρκως II 161, 18. προελκομένως III 461,
22. δαψιλῶς II 266, 54. **et prolixius** καὶ προηλκυσμένως III 3, 7.
**prolixius** προελκυσμένως (-ενῶ *cod.*) II 161, 17.

**Prolixitas** μῆκος χρόνου II 370, 55. ἐπέκτασις II 306, 34. ἅπλωσις II
235, 36 (prolixi). **prolixitate** μήκει, μεγέθει, διαστήματι II 161,
16.

**Prolixitudinem** a prolixo V 646, 15 (*Non.* 160, 11).

**Prolixus** ἐπιμήκης II 309, 44. τετανόθριξ II 161, 25 (prolicinus
*Vulc.*). **prolixa** longa IV 148, 16; 275, 13; 381, 11; 557, 27; V
137, 50. qui patre longius peregrinante narrant (nascitur?) V 576, 50
(*v.* proculus). **prolixas** ἐπιμήκεις II 161, 19.

**Prologus** πρόλογος II 419, 1. sequentis operis praefatio IV 148, 42;
556, 16; V 137, 48. praefatio *gloss. Werth. Gallée* 343 (*v.*
*suppl.*). **prologis** principiis IV 151, 28 (*Ter. Andr.* 5).

**Proloquo\<r\>** προλαλῶ II 418, 43. **proloqui** φράζειν (φρασοεν
*cod.* φράσαι *e*) II 161, 20. dicere IV 150, 18 (*Ter. Andr.* 256
*etc.*).

**Prolubium** uoluntas V 538, 48 (*Ter. Ad.* 985).

**Proludere** prospicere seu promeditare V 476, 28 (*v.* promeditare).

**Proluo** κατακλύζω II 341, 18. καταπλύνω II 343, 4. **proluit**
ἐπέκλυσεν II 161, 21. perduxit. \<Pl\>aut\<us\> in Curculione (*v.* 123:
curcula *codd.*): prolue cluacam (claucam *codd.*) V 236, 14. perfundit,
lauit IV 459, 51 (*Verg. Aen.* I 739?). perfudit IV 150, 50; V 476,
31.

**Proluuies** κοιλίας ὑπερέκχυσις II 161, 22. κοιλίας ἔκχυσις II 161,
26. inmu\<n\>ditia II 590, 29; *Gallée* 363 (*v. suppl. cf. Non.*
373, 11). sordis effusio V 556, 33 (*cf. Serv. in Aen.* III 217. *v.*
profluuies). sordes V 325, 20. squalores, sordes IV 556, 11. squalor uel
sordes IV 148, 38. squalores IV 459, 50 (*Verg. Aen.* III 217); 557,
18. *Cf.* praeluuium, profluuies.

**Proma** ταμιεῖον τὸ ἰδιωτικόν II 451, 25. *V.* cella (*an cum* cella
*coniungendum*, cella proma? *cf. tamen lexica*). **proma** cella,
prumptuaria, hordren (*AS.*) *gloss. Werth. Gallée* 362 (*v. suppl.*).

**Promagistratus** ἀντάρχων III 297, 36; 507, 67. ἄνταρχος III 488, 71.

**Promaritima** (*uel* perm. *an* ora m.?) saegesetu (*AS.*) V 378, 4.

**Promatertera** soror auiae (*vel* proauiae) IV 275, 27. soror proauiae
IV 381, 12; V 539, 12. *Cf. Isid.* IX 6, 27.

**Promatum** (primatum? stromatum *Buech.*) lectorum (electorum?) V
381, 37.

**Pro mediocritate ingenii mei** V 663, 71. *Cf.* **pro uirili
portione** V 663, 68 (*Arch.* IX 143).

**Promeditare** prospicere V 476, 32.

**Promentarius** ταμιοῦχος III 305, 11. **promentarium** ταμιεῖον III
313, 48. prumptuarium *gloss. Werth. Gallée* 362 (*v. suppl.*).

**Promerenda** ad laudandam (adulanda *H.*) V 511, 24.

**Promerentem** (properantem *cod.*) dignum V 538, 42 (*Ter. Ad.* 681).

**Promereor** praesto\[r\], bene ago V 556, 35 (*cf. Serv. in Aen.* IV
335). **promeretur** impetrat IV 149, 5. **promeruit** dignus est V 538,
37 (*Ter. Ad.* 201). **promeruerunt** ambulauerunt (impetrauerunt?
ambiuerunt *Buech.*) V 476, 33; 511, 19. **promeruisse** εὖ πεποιηκέναι
II 161, 28.

**Promeritus** bene meritus IV 275, 18. apud antiquos dicebatur qui quid
bene fecisset: **promerita\<m\>** profuisse, praestitisse V 137, 54;
236, 15 (corr. *adscito Verg. Aen.* IV 335 *H.*).

**Prometheus** a prouidentia dictus V 379, 4 (*cf. Serv. Ecl.* VI 42).

**Prominentes** rupes montium super alios eminentes IV 148, 48; V 476,
36.

**Promineo** προέχω II 417, 43. **prominet** προκύπτει, προκόπτεις (!)
II 161, 30. incubat IV 420, 43.

**Promiscue** μίγδην, κοινῇ 161, 33. ἀδιαφόρως II 161, 34 (*cf.
margo*). ἀναμίξ, χύδην II 161, 40. passim IV 381, 14.

**Promiscuus** ἐπίκοινος II 308, 54 (praem. *cod. corr. a*: *cf. Funck
Arch.* IX 304); III 461, 23; 494. 42. ἀδιάφορος II 218, 40. μεμιγμένος
II 367, 32. **promiscuum** ἐπίκοινον II 308, 55. **promiscua** confusa,
diuersa uel uaria siue commixta *c* IV 274, 48. confusa IV 557, 26.
**promiscuis** diuersis IV 557, 25; V 323, 49.

**Promissio** ὑπόσχεσις II 468, 9. ἐπαγγελία II 305, 3; III 138, 22.
κατεπαγγελία II 345, 38.

**Promissor** ἐπαγγελτής II 305, 5. sponsor IV 381, 16. *V.* fidei
promissor.

**Promissum** ὑπόσχεσις II 548, 21 (*GR.*
