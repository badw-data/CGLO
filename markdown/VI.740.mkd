**Nisi forte** εἰ μὴ κατὰ τύχην II 286, 10. εἰ μὴ τυχόν III 141, 40.
quamuis etiam IV 541, 59.

**Nisi non** εἰ μή II 286, 8.

**Nisi si** εἰ μή τι II 286, 12. εἰ μὴ ἄρα III 141, 39. nisi quia IV
368, 43. *Cf.* ni.

**Nisison ani** id est anio sangũs r̃ (nausiosin ani i. ano sanguis
rumpitur *Buech. dubitans*) III 570, 38.

**Nisuper** *v.* insuper.

**Nisus** σπουδή III 486, 18 (*cf.* II 512, 31). **nisu** conatu IV 124,
46 (*vel* nixu); 454, 34 (*gl. Verg.: cf. Aen.* III 37; V 437). conatu
uel conamine V 121, 23. conamine uel conatu IV 125, 7; 541, 35.

**Nisus** *v.* nixus.

**Nisus** (nysus) Νύσος (Νῖσος*?*) III 257, 69 (*avis*; *unde?
sequiturr* scylla).

**Nit** *v.* neo.

**Nitalmus** (= nyct.) est qui noctibus non uiclet V 621, 40. *Cf. Is.*
IV 8, 8.

**Nitela** δενδροβάτης II 133, 54. *V.* netila.

**Nitela** Solinus (ΧΧII 5: *qui locus a Mommseno damnatur*): nam
praecipua uiris gloria est in armorum nitela V 121, 24 (rex Solinus:
*ubi* lux *pro* rex *Landgraf. Arch.* IX 399. = require. *H.* res.
*Buech.*); 226, 23. **nitellae** nitoris diminutio IV 123, 43 (netelle).
nitores diminutiue IV 541, 56; V 313, 42; 374, 59. nitores parui IV 262,
6; 368, 46; V 468, 20; 528, 38. nitores parui, nitores deminuti\<ue\>
*Scal.* V 605 45 (*cf. Hagen Grad. ad cr.* 72). **nitille** nitores IV
125, 25.

**Nĭtens** λάμπουσα, στίλβουσα II 134, 6. nitidus IV 125, 10 (*Verg.
Aen.* VI 895); 541, 44; V 121, 28. incumbens (*v.* nītens) et splendidus
(exsplendens *a b*) IV 262, 5. **nitentem** candidum uel nitidum IV 125,
13 (*Verg. Aen.* III 120); 541, 49. **nitentes** nitidos IV 125, 29; V
121, 27 (*Verg. Aen.* I 228).

**Nītens** conans IV 125, 26; 454, 35 (*gl. Verg.: Aen.* IV 252?).
ambulans IV 125, 14 (*Verg. Aen.* II 380). incumbens IV 368, 47
(*Non.* 353, 13); V 313, 41; 468, 21. ambulans uel incumbens IV 541, 43
(*Non.* 353, 15). contendens IV 368, 48. participium est a uerbo quod
est nitatur (!) V 121, 26. *V.* humi nitens, nĭtens.

**Niteo** λάμπω II 358, 37. στίλβω II 438, 1. [σπένδω] III 159, 25.
**nitet** στίλβει II 134, 8. **nitit** [σπένδει] III 159, 24.
**nitet** splendet IV 368, 50; V 313, 52. splendet, lucet IV 125, 11;
262, 12; 541, 48 (nitit); V 121, 29; 313, 51.

**Nitescit** splendescit V 630, 8.

**Nitidant** albent V 644, 57 (coluent *cod.*; *cf. Non.* 144, 12:
abluunt).

**Nitiditate** pro nitore V 644, 53 (*Non.* 143, 27).

**Nitidus** λαμπρός III 329, 23; 529, 42. linis (leuis *Hildebrand*) uel
accuratus IV 368, 51. splendidus V 121, 30. **nitidos** λάμποντας II
134, 7. *V.* uuidus.

**Nitilla** ὀρίγανον II 386, 46 (*v.* nepeta montana).

**Nitit** *v.* neo.

**Nitor** αἴγλη II 538, 57. αἴγλη ἢ λαμπηδών II 551, 8. ἡ λαμπηδών II
512, 29. splendor IV 125, 15; 261, 46; 541, 50. **nitore** quod nitidum
est IV 124, 43. odore, splendore (*v.* nidor) V 468, 5. lucore (? luce?
liquore?), splendore V 468, 19.

**Nitor** ἐπερείδομαι II 306, 45 (*GR. L.* IV 572, 5). conor uel
splendor (*v.* nitor) IV 368, 52. **nititur** conatur IV 124, 45; 125,
23. conatur aut laborat IV 541, 37. laborat IV 125, 12. pugnat, conatur,
temptat IV 261, 55. **nituntur** conantur IV 125, 20 (*Verg. Aen.* II
443). **nitar** coner, moliar V 121, 25. **niti** conari IV 368, 49.
conare, temptari V 313, 62. **nitent** incumbent IV 125, 24. *V.*
enixus.

**Nitrum** νίτρον (νιπτον *cod. corr. e*) II 376, 40; III 195, 2; 273,
57. **nitros** λίτρον III 566, 69. **nitrus** λίτρον III 584, 8; 592, 34
(lintron); 613, 65 (lintro); 626, 11 (*item*). **nitrus** sal
Alexandrinus III 593, 9; 626, 67. beronegario III 587, 57. rocia (?) III
594, 50. racia III 628, 44. *V.* uermicarium, lomentum.

**Nitrus albus** ricius III 575, 43.

**Nitrus niger** (nitro nigro *cod.*) lutrus III 547, 27.

**Nitrus rubeus** rica III 575, 25. ricus III 575, 42. ricior III 585,
42. ricor III 594, 46. rigor III 628, 33. **nitro rubio** riteur III
632, 51.

**Nitta** (?) laxa (?) III 602, 30.

**Nitum** νῆμα III 471, 24; 477, 5. *V.* netum.

**Nitura** genitura IV 125, 9; 541, 42; V 468, 22; 605, 29 (nisura
*Graevius*). **nictura** (*vel* nect.) genitura IV 262, 2 (natura?
gannitura? *Warren*).

**Niuarius** splendidus V 375, 2. **nibarus** splendidus IV 124, 49;
261, 41; 541, 25; V 226, 11; 313, 47; 605, 28. niueus, splendidus
*gloss. post Salom. Cf. Loewe Prodr.* 427. *A* Nibaro *monte*?

**Niuata aqua** ex niuibus facta IV 124, 48; 541, 24 (nubibus); V 226,
12; 313, 61; 374, 58 (niue); 605, 27 (nubibus).

**Niuauit** *v.* ninguit.

**Niueis uelis** candidis uelaminibus IV 454, 36 (*Verg. Aen.* I 469).

**Niuet** χιονίζει III 347, 44. νίφει II 134, 9.

**Niueus** candidus IV 261, 48. **niuea** χιονώδης II 477, 15.
**niueum** plenum \<niue\> V 313, 60 (splendidum *H.*).

**Niuitor** χιονίζομαι II 477, 14. **neuitor** χενίζομαι III 486, 5.
*Cf.* neuicare *Italorum.*
