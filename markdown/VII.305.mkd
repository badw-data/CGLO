γάγετο II 190, 18. subdidit, deuicit IV 288, 3. coegit IV 177, 15; 569,
61; V 153, 6. dominauit, sub iugum misit V 484, 19. **subegerunt**
dominauerunt (*vel* donauerunt) V 484, 18; 21, *V.* subago.

**Subinde** ὑπὸ χεῖρα, πολλάκις II 190, 34. παρ' ἕκαστα II 397, 38
(παρεσακτ *cod.* πάρεξ *e*). frequenter IV 177, 17; 569, 62; V 153, 9.
**subind[i]e** assidue V 484, 24. **subindius (!)** frequentius V 484,
25; 153, 17.

**Subinducatus** subintroductus II 594, 34.

**Sub ingenti** sub altissimo *a post* IV 181, 7 (*Verg. Aen.* I 453).

**Subinsanus** παρεμμανής, παρακεκακωμένος, cerritus, garriosus
(cererosus *Haupt Opusc.* II 367) II 562, 18. παρακεκομμένος *Bernd
collato Suida. Cf.* cerritus.

**Subintroductus** *v.* subinducatus.

**Sub irco** (subirico *cod.*) μάλη ἀνθρώπου II 364, 33 (*v.* sub ala).
*Cf. Is.* XI 1, 65.

**Subitaneus** αἰφνίδιος II 221, 32.

**Subitanum** σπιλλας (σπιλάς *David*) III 426, 58 (*de ventis*).

**Subitator** annuens puellis (subitare = subare? subigitator?) II 594,
26.

**Subitillus** ἔνθυτος (ἐγχυτος? ἔνθρυπτον?) III 372, 23 (*cf. not.
Tir.* 109, 35), *V.* sibitillus. *Cf. W. Heraeus 'Spr. des Petr.'* 29.

**Subito** ἐξαίφνης II 190, 33; 301, 38. ἐξαπίνης II 301, 57. αἰφνιδίως
II 221, 33; III 127, 47; 142, 5. αἰφνίδιον II 221, 31.

**Subitus** inprouisus IV 465, 20 (*cf.* improuisus). **subitum**
repentinum V 153, 16. **subiti** repentini V 647, 44 (*Non.* 173, 24).

**Subiuga** ζεύγλη II 321, 63. **subiugum** ὑποζύγιον II 466, 15.

**Subiugium** seruitus II 594, 59.

**Subiugo** ὑποβάλλω II 465, 24.

**Subiunctiuus** συνζευκτικός II 445, 61. **subiunctiuum** ὑποτακτικόν
II 468, 11. **subiunctiua** ὑποτακτικά II 190, 35.

**Subiungo** ὑποζευγνύω II 466, 14.

**Sublabrare** labris sumere V 647, 10 (*Non.* 170, 6).

**Sublapsa** diminuta IV 288, 26 (*cf. Verg. Aen.* II 169; XII 686); V
580, 3 (sublaxa).

**Sublato genitore** portato patre V 153, 23 (*Verg. Aen.* II 804).

**Sublatus** exemptus IV 394, 19. natus II 190, 40. **sublatum**
subtractum IV 177, 24; 570, 7; V 153, 19. tultum V 580, 8. raptum IV
287, 53. **sublato** tulto, eleuato V 484, 27.

**Sublauo** ὑποπλύνω III 161, 51.

**Sublego** ὑπαναγιγνώσκω II 463, 17. rapio V 557, 14. **sublegit**
subtrahit, colligit IV 177, 25; 288, 29; 570, 8; V 153, 20; *cod. Monac.
v. suppl.* subtrahit IV 394, 20. **sublegi** est de (te *codd.*) legente
insidiando furatus sum, ut si quis solus legat et alius ex occulto uerba
eius furetur et ea prodat *Plac.* V 41, 4 = V 100, 13 = V 155, 2. *Cf.
Serv. in Ecl.* IX 21. *V.* sacrilegium.

**Sublestia** infirmitas, tristitia *Scal.* V 611, 58.

**Sublestis** infirmus, tristis *Scal.* V 611, 57. **sublestum** leue V
647, 66 (*Non.* 177, 8). *Cf. Loewe Prodr.* 264; *Festus Pauli p.* 295,
5.

**Subleuis** (-us *cod. corr. e*) ὑπόκουφος II 466, 41.

**Subleui** subsignani ut releui resignaui. **\<subleui\>** hoc est
subsignaui, si (subsignare *codd.*) qui alio signante iterum
[re]signat, resigna\<ui\> est quod dicitur releui, si ea quae signata
sunt aperiuntur, item sublitum \<os\> (sublini *Deuerling*) dicitur cui
(qui *codd.*) imposturae (-ae *vel* -as *codd.*) fiunt, qui decipitur
(decipiuntur *codd.*), cui uerba dantur uel qui non satis doctus est, ut
si dicas: 'sublitum mihi est os' non bene doctum uel indoctum \<te
significes\> *Plac.* V 41, 3 = V 100, 15 = V 154, 37 (doctum bene *cod.
suppl. Buech.*). **subleuit** subiunxit (subunxit *Quicherat, Roensch
Fleckeiseni ann.* CXVII 797: subinunxit *vel* subunxit *Georges 'Berl.
phil. W.'* IV 1578) a liniendo *Plac.* V 40, 7 = V 100, 14 = V 154, 34.
illusit V 654, 38; 647, 31 = 651, 40 (*Non.* 45, 18).

**Subleuo** ὑποκουφίζω II 466, 40.

**Sublica** ἐπενδύτης III 323, 9; 518, 63. *Cf. Loewe GL. N.* 169 *et*
σουβρικός *GR. L.* IV 578, 9. *V.* subricula.

**Sublices** καταπῆγες οἱ ἐν τῷ ποταμῷ II 185, 22 (soblices *cod.*).
**subliges** κατα­πῆγες οἱ ἐν ποταμῷ τήν γέφυραν ὑποβαστάζοντες II 190,
46. *Cf. GR. L.* VII 544, 24.

**Sublicius** pons, qui\<a\> inter eius materiem (materie ẽ *codd.*)
aqua subliquitur IV 179, 16 (*cf. Nettleship 'Journ. of Phil.'* XX 61).

**Subligaculum** διαζώστρα II 190, 41. περιζώστρα II 190, 42.

**Subligar** (sublegar *cod.*) inguinar\<i\>um, bu[m]bonaria V 484, 28
(*Iuv.* VI 70?).

**Sublimatus** exaltatus IV 177, 23; 570, 5; V 153, 18. honore exaltatus
IV 288, 9.

**Sublim\<in\>are** limen II 594, 44. *V.* superliminare.
