564, 50. **satyricus** morum disscriptor V 631, 28. **satirici** poetae
a saturitate dicti et copia, eo quod pleni sunt omni facundia V 513, 27.
*Cf. Isid.* VIII 7, 8.

**Saturitas** χορτασία II 179, 14; 478, 4; III 343, 38; 463, 18.
πλησμονή II 410, 8; III 463, 17.

**Saturnalia** Κρόνια (singularia non habet) II 355, 38 (*GR. L.* I 34,
7). Κρόνια III 10, 20 (κρονιαι); 83, 74; 171, 41; 239, 27; 294, 57; 371,
55; 491, 39; 524, 52. **Saturnalibus** Κρονίοις II 179, 6.

**Saturnia** Iuno, Saturni filia V 146, 39. a Tritone palude ubi dicitur
nata IV 422, 42 (Tritonia?). Italia a rege Saturno dicta IV 282, 10
(*Serv. in Aen.* VIII 328).

**Saturnia arua** italica, quod illic Saturnus regnauerat V 146, 40
(*Verg. Aen.* I 569).

**Saturnus** Κρόνος II 179, 12; 355, 39; III 8, 27 + 28; 72, 27; 150,
47; 167, 35; 242, 14 (*stella*); 289, 55; 342, 13; 343, 48; 348, 10;
393, 30; 418, 12; 463, 19; 524, 28. **Saturnus pater** Κρόνος μέγιστος
III 167, 34; 236, 34. **Saturnus** Caeli Terraeque filius IV 422, 43.
Graecorum rex V 416, 4 = 426, 31 (*vita S. Anton. interpr. Euagr.* 46).
apostaticus, id est dipositor (!) et destructor fatorum V 513, 26.
**Saturni** Χρόνου III 292, 48 (*stella*). *V.* Incubo, Satyrus.

**Saturo** χορτάζω II 478, 3; III 163, 51. κορεννύω II 353, 40.
**saturas** χορτάζεις III 163, 52. **saturat** χορτάζει III 163, 53.

**Saturo exercitum commeatus** χορτάζω τὸ στράτευμα ἐπιτηδείων II 179,
11 (*GR. L.* VII 425, 16, *ubi* commeatu).

**Saturus** *v.* pinguis.

**Satus** σπαρείς II 435, 21. **satum** σπαρέν II 179, 15. **sata**
σπόριμα II 178, 56. seminata IV 463, 11 (*Verg. Aen.* II 306); 282, 4;
389, 1. seminata, conlocata uel mensura, id est modium unum semis (semit
*cod.*) V 481, 26 (*v.* satum). messis IV 167, 5. messis aut semina IV
564, 21. segetes V 146, 28.

**Satus** (satu?) semine V 647, 46 (*Non.* 174, 11).

**Satyriasis** impetus desiderii circa ueretrum sine mensura cum dolore
et pruritu III 605, 3.

**Satyricus** *v.* saturicus.

**Satyrion** (*vel* saturion *vel* satirion) cynos (gynos *cod. Vrat.
Pseudapul. c.* XVI. *cf.* κυνὸς ὄρχις *Diosc.* III 131) III 557, 64.
ginos III 564, 29. cinos III 622, 19. emtacon (etaticum *cod. Vrat.
Pseudapul. =* ἐντατ.) III 561, 65. ureir (οὐρητήρ *confert Buech.* ura
*Pseudapul.*) III 579, 26. orcis (orchis *Pseudapul.*) III 570, 49.
Siculi orchis III 633, 23 (*cf.* 21). naemo (neme *cod. Vrat.
Pseudapul.*) III 570, 31. priapiscus (*ita Pseudapi*) III 572, 14; 595,
21; 629, 13; 633, 22 (*cf.* 21); 548, 57. turmentalis (torminalem
*Pseudapul.*) III 578, 63. testiculus leporinus III 633, 24 (*cf.
Pseudapul.*). testiculus leporis III 577, 17. testiculum leporinum III
578, 29. cinticoron (cynocoron *cod. Vrat. Pseudap. cf. Diosc.* II 171)
id est **satiflon** III 621, 70. cinocoron id est **satyrion** III 557,
65.

**Satyrus** Σάτυρος II 430, 1. **Satirum** Incubum V 423, 5 (*Clem. Rom.
rec.* X 18, *ubi* Saturnus). **Satyri** Σάτυροι III 236, 55. **Satiri**
dicuntur fa\<n\>tasiae eo quod saturos homines opprimant V 556, 62.
*Cf.* **satur** saturi V 393, 5. *V.* pilosus.

**Sauciatus** τρωθείς II 460, 57. uulneratus uel fatigatus IV 389, 9.
**seuiati** tetromenta (sauciati τετρωμένοι?) III 209, 20.

**Saucilia** (saucia ilia *Buech. coll. Aen.* VII 499) uulnera V 481,
32.

**Saucio** τιτρώσκω II 456, 34. τραυματίζω 458, 21. **sauciari**
uulnerari IV 167, 11; 564, 34.

**Saucius** τετρωμένος II 454, 36. τραυματίας II 179, 16; 458, 20.
uulneratus IV 282, 15; 564, 35; V 390, 44. grauissime uulneratus IV 166,
30. uulneratus, semianimis V 146, 48. semiuiuus V 146, 46. **saucios**
uulneratos V 330, 2.

**Saucomaria** (= sac.) est uasculum de cucurbita V 623, 45. *Cf.
Hieron. Ion.* IV 6.

**Saucus** sambucus III 629, 43. **cortex sauci** fleo III 590, 51.
**cortex sauco** fleo III 611, 66. **corti arde sauco** fleo III 624,
16. astra passa **flores de sauco sicco** III 586, 8; 607, 6 (flos);
616, 9 (*item*). *Cf. Arch.* V 454. *V.* sambucus.

**Saulus** temptatio uel securitas (scuritas *cod. Sangall.* saturitas
*Warren*) IV 282, 20. *Cf. Isid.* VII 6, 63.

**Saumarius** (= sagm.) *v.* cantherius, ueterina bestia.

**Saura** (satura *cod.*) genus lacertorum V 578, 37. *Cf. Is.* XII 4,
37.

**Sauricem** soricem, antiqui si\<c\> dicebant V 146, 47. **sauricem**
soricem: antiqui enim saurice (!) dicebant quod nunc soricem appellamus
V 242, 33. *Cf. Serv. in Georg.* II 30; III 278; *Isid.* XII 3, 2.
**saures** sorices IV 389, 15; V 609, 62; 481, 35. surices IV 282, 8;
V 394, 30. *Cf. Loewe Prodr.* 345. *V.* lacertus.

**Sauromatae** genus et nomen barbae (gentis est nomen barbarae?) V 394,
22. **Saurom\<at\>ae** nomen gentis barbarae IV 166, 27. *Cf.* Sarmatae
*et Loewe Prodr.* 407.
