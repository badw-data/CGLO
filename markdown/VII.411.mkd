**Ueteris Bacchi** ueteris uini IV 469, 20; 578, 33 (*cf. Verg. Aen.* I
215). ueteris uini. Bacchum autem metonymicos ait uinum ab inuentore V
253, 20. *V.* Bacchi *et* Bacchus. **Bacchi ueteris** uini ueteris
(uinum uetus *codd.*) IV 188, 26.

**Ueteris belli** diu gesti [una simul pariter] IV 469, 18 (*Verg.*
*Aen.* I 23. *v.* una).

**Ueteris flammae** pristini amoris IV 469, 19 (*Verg. Aen.* IV 23).

**Ueternosus** ὑδρωπικός II 462, 23. φθισικός, ὑδρωπικός II 207, 46
(*cf. margo*). aquaticum uentrem habens II 596, 52. hydropicus *post* V
539, 25 (*Ter. Eun.* 688). callidus IV 191, 12 (*cf.* ueterator). *V.*
ueternum.

**Ueternum** ὕδρωψ II 462, 24. φθίσις II 533, 10. morbus pigritiae uel
humoris foedi, unde et idropes ueternosos dicimus V 335, 43 + 44 (*cf.
Serv. in Ge­org.* I 124). morbus inueteratus, hydrops V 489, 31
(ueternus). **ueterno** otio et situ IV 192, 14.

**Ueternus** oblitus (λήθαργος *male ver­sum*) II 596, 54. **ueternum**
antiquum, uetus, uetere (! *v.* uetustus), priscum IV 469, 22 (*Verg.*
*Georg.* I 124?). anticum, uetustum IV 295, 40. uetus uel antiquum IV
190, 10.

**Uetero** παλαιῶ II 392, 44. **ueterauit** antiquauit IV 578, 28. *Cf.
Landgraf Arch.* IX 440 (*ad Hebrae.* 8, 13).

**Uetitum** prohibitum IV 191, 14.

**Ueto** κωλύω II 357, 34; III 147, 60; 421, 16; 466, 25; 497, 77. εἴργω
II 286, 25. caueo IV 401, 13. **uetas** κωλύεις III 147, 61; 421, 17.
**uetat** κωλύει II 207, 41; III 5, 16: 147, 62; 421, 18. **uetamus**
κωλύομεν III 421, 19. **ueta** κώλυσον III 421, 15. **uetare** κωλῦσαι
III 147, 63. **uetor** prohibeor IV 192, 4 (*Verg. Aen.* I 39).
**uetantur** κωλύονται III 421, 20. **uetati sunt** ἐκωλὐθησαν III 421,
21.

**Uettonica** (bett.) cristris (cestros *Diosc.* IV 1) III 537, 17.
**uetonica** cestros III 558, 41. cistros III 555, 33; 619, 59.
**betonica** cistrus III 589, 6; 609, 47. siderites (sideritis *Pseudap.
Ackermanni*) III 554, 61. **uetonica** (*vel* uett. *vel* uitt.)
pandonia (pandiona *Pseudap.*) III 573, 49. tia rica (thia rhiza
*Pseudap.*) III 578, 12. theripo (dyprinion *Pseudap.*) III 578, 15.
cyroei (hiera coryne *Pseudap.*) III 557, 49; 622, 11. genera carine III
564, 32. peropion (pheriponion *Pseudap.*) III 573, 48. indicena
(indicem *Pseudap.*) III 546, 73. hindicis III 583, 38 (uitt.). seraula
(serrata *Pseudap.* serratula *Plin.* XXV 84) III 576, 41 (uitt.).
**betonicae semen** ifidici (?) semen III 565, 55. **betonica** κεστός
III 265, 53 (*unde*?). ἱεροβοτάνη III 540, 4. aturlađe (*AS.*) V 402,
51. *V.* brittannica, *'Realencycl.' ed. Pauly-Wissowa sub* betonica.

**Uetulco** uetusta, uetrana V 489, 32 (uetula?).

**Uetulus** παλαιὸς ὑποκοριστικῶς II 392, 33. γέρων III 466, 27.
ueteranus V 489, 33. diminutiue quasi minus uetere V 335, 47.

**Ueturnus** uetustus IV 190, 12. *V.* uetustus.

**Uetus** παλαιός II 392, 32; III 184, 42; 255, 33; 364, 37; 398, 23;
501, 73. olim conditus IV 578, 26. παλαιά II 392, 30. παλαιόν III 15,
20; 87, 68; 315, 37; 357, 39; 364, 59; 466, 26. antiquum IV 191, 5.
antiquum, ueteratum IV 401, 14. **uetera** antiqua IV 578, 29. *De*
uetere *cf.* uetustus, ueternus.

**Uetustas** παλαιότης II 207, 50; 392, 38. ἀρχαιότης II 246, 31.
sapientia V 648, 53 (*Non.* 184, 23). *V.* meratrum.

**Uetustus** ἀρχαῖος, παλαιός II 207, 48. **ueturnus** et **uetustus**
παλαιός II 207, 49. **uetustus** ἀρχαῖος II 246, 27. παλαιός II 392, 32.
**uetusta** ἀρχαία II 246, 28. **uetustum** ueturnum, antiquum, uetere
(! *v.* ueternus) IV 401, 15. **uetustam** ueterem V 415, 8 (*serm. de
regul.*). **uetusta** antiqua, uetula IV 469, 23 (*Verg. Aen.* IX
284). olitana (*v.* olitanus) V 400, 4. **uetustissimus** ἀρχαιότατος
III 508, 50. **uetustissimus ab origine** ἀρχαιότατος ἀπ' ἀρχαιογονίας
III 285, 21/22 = 656, 7.

**Uexatio** σκυλμός II 207, 54; 434, 22; III 159, 7; 205, 63. κακοπάθεια
II 336, 53.

**Uexatus** σκυλείς II 434, 17. καταπονηθείς, σκυλ[λ]είς II 207, 55.
id est portatus a ueho V 582, 42 (*cf. Isid.* X 280). periclitatus IV
401, 17.

**Uexillarius** φαρσοφόρος II 470, 15. μαππάριος III 174, 14. portitor
crucis II 596, 51.

**Uexillatio** βηξιλλατίων II 257, 13. numerus militum cum uexillis V
335, 48; 400, 41.

**Uexillifer** βηξιλλοφόρος II 257, 14. signifer II 596, 62.

**Uexillum** σημεῖον τάγματος στρατιωτικοῦ II 430, 59. τάγματος
στρατιωτικοῦ σημεῖον II 451, 9. ῥούσιον (= russeum) φάρσος II 428, 48.
faros III 209, 1 (φάρσος?). signum II 596, 41. signum militare IV 295,
50; 426, 5. signum militare, id est belli signum seu crux aut paxillum V
489, 34. a uehendo (aduentum *vel* aduertendum *cod.*) dictum
portatorium IV 191, 24. **uexilla** et **lab\<a\>rum**
