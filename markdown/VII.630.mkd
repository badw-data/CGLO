**πρωτεῖον** primarium.

**πρωτεύω** principatum gero, II, 424, 58 *s. interpr.*, -ων
principalis.

**πρώτιστον** primarium.

**πρωτόγαλα** colostra.

**πρωτογενής** primitivus. *Cf.* τύχη.

**πρωτογέννητος** primo genitus.

**πρωτογεύστης** gustator.

**πρωτόγονος** primitivus.

**\*πρωτοδέκανος** primus virgariorum.

**\*πρωτοκατασκεύαστος:** εἶδος μονοξύλου πλοίου -ον linter.

**πρωτόκτιστος** primitivus.

**\*πρωτοπειρία** rudimentum, tirocinium.

**πρωτόπειρος** II, 425, 6 *s. interpr.*

**πρωτόπλαστος** *cf.* protoplastus (*expl.* primitus figuratus, primo
genitus *sim.*), -ον primo finctum.

**\*πρωτόπλοια** primum navigium.

**πρωτοπολίτης** princeps, princeps civitatis, principalis, -αι
optimates, procer (proceres).

**πρωτοπραξία** privilegium.

**\*πρωτοραβδοῦχος** primivirgius.

**πρῶτος** primus, prior, -οι primates, primor (-ores), - ἐν δόρατι
primipilarius, πρῶτον prime, primus (-um, *cf.* proton), ἤδη πρῶτον iam
primum, ἐν πρώτοις apprime, in primis, principium (-o). *Cf.* δόρυ,
καρπός, οἶκος, πρώτιστον.

**\*πρωτοστασία** principatus.

**πρωτοστάτης** princeps, -αι primor (-es).

**πρωτότοκος** primigenus (*et* -a), primitivus, primo genitus.

**πρωτότυπος** primitivus, principalis, -ον originalis (-e), -a
praedicamenta.

**πρωτοτύπως** principium (-o). **πρωτόχρονος** primaevus.

**πρώτως** primo.

**πταίρω** *et* -ομαι sternuto.

**πταῖσμα** culpa, delictum, noxa, usurpatio.

**πταίω** pecco, titubo, trepido. *Cf.* furcifer.

**\*πταρες** rivus.

**πταρμικόν** sternutatio naris.

**πταρμός** sternumen *et* sternumentum, sternutum.

**\*πτάρνεται** sternuto (-at).

**πτελέα** ulmus.

**\*πτελεών** (ὁ τόπος) ulmetum.

**\*πτερίδιον** (? \*πτερίχνιον *Buech.*) filix *sim.*

**πτέριον** filix *sim.*

**πτέρις** (βοτάνη) filix *sim. Cf.* cerrus, ptereas.

**πτέρνα** *et* -η calcaneum, calx, planta. *Cf.* culdex; teterna
(*expl.* calcitratio).

**πτερνίζω** supplanto.

**\*πτέρνος** culdex.

**πτερόν** ala, axila, penna, pinna (*et* pinnula), pluma, - πλοίου
aplustria. *Cf.* ἶβις.

**πτερόπους** alipes.

**πτεροφόρος** aliger, penniger.

**πτερύγιον** lingula, penna.

**πτέρυξ** ala, pinna.

**πτερύσσεται** frontinet.

**πτερῶ** alipinno (= alo, pinno?), pinno, -ωθείς pennatus.

**πτερωτός** alatus.

**πτηνός** volucer, -όν ales, auca, volucer (*et* -cris *sim.*), -ά
volatilis (-ia), εἶδος πτηνῶν merops.

**πτῆσις** volatus.

**πτιλλός** lippus.

**πτίλον** pluma.

**πτισάνη** *et* **τισάνη** ptisana (*et* tisana). *Cf.* oryza.

**πτίσσω** *cf.* ptisso (*expl.* tundo), -ει pisat.

**πτόησις** horror, pavor, stupor, territus 1., terror, trepidatio.

**πτόρθος** ramus.

**πτοῶ** absterreo, paveo, terreo, territo, -οῦμαι expavesco, paveo,
pavesco, trepido, -ηθείς obstupefactus 1., ἐπτοημένος pavidus.

**πτύγματα** *cf.* ptygmata (*expl.* causiae).

**πτυκτόν** plectibile, plectile.

**πτύξ** tabella.

**πτύον** delabrum, pala, paleta, ventilabrum.

**πτύρω** terreo, -ομαι consternor, terrefio, trepido.

**πτύσμα** sputum.

**πτύσσω** complico, plico.

**πτυχή** plix. *Cf.* pulex.

**πτύω** spuo.

**πτῶμα** cadaver, - νεκροῦ cadaver, - ἐπὶ πάλης II, 425, 53 *s.*
*interpr.*

**πτώσιμος** caducus.

**πτῶσις** casus, - μεγάλη ruina, - σώματος strues, - σωμάτων strages, -
ἐπὶ οἰκοδομῆς rues, ruina.

**πτωτικός** caducus.

**πτωχεία** *et* -χία egestas, inopia (*cf.* ptochias), mendicitas,
pauperia, περὶ -ας de paupertate.

**πτωχεῖον** *cf* ptochium (*expl.* hospitium).

**πτωχός** egenus, mendicus, pauper, πτωχότατος pauperculus.

**πτωχοτροφεῖον** *cf.* ptochotrophium (*expl.* locus venerabilis in quo
pauperes et infirmi homines pascuntur).

**πύαλος** *v.* πύελος.

**Πυανεψιών** *v.* menses.

**πύγαργος** *et* -ον damma, *cf.* pygargus (*expl.* ovis cum albis
clunibus).

**πυγή** anus 2., culus, natica, natis. *Cf.* clunis.

**πυγίζω** percido, πεπυγισμένος pedicatus, percisus.

**\*πυγί(ο)ν (?)** clunis.

**πυγμή** dimicatio, pugna.
