**Chelys** cithara quae (!) canitur III 528, 15; IV 32, 46; 493, 35.
cithara IV 216, 29; 318, 14; V 276, 35; 446, 22.

**Cherea** manualis V 276, 24 (χειρία?).

**Cherub** plenitudo, singulare III 528, 17. **cherubim** χερουβείμ II
559, 11; III 279, 15. **cherubin** plenitudo scientiae III 528, 18.
scientiae multitudo IV 32, 55; 493, 11; V 353, 44. *Cf. Isid.* VII 5,
22; *Roensch Mus. Rhen.* XXXI 458; *Eucher. instr. p.* 146, 13.

**Chiliarchus** tribunus qui mille contribulibus \<praeest\> IV 218, 54
(*cf.* Warren 198, 302). qui mille praeest V 349, 27 (*Isid.* IX 3, 30).

**Chilo** *v.* cilo.

**Chimaera** Typhonis et Echidnae filia prima leo, postrema draco, media
capra, quam Bellerofantes occidit IV 436, 46 (*cf. Verg. Aen.* VI 288 et
*Serv.*). **Cymira** Typhonis (*mutilata*) V 543, 26. **Chimaera**
bestia IV 219, 16. leo triformis V 445, 19; 276, 40 (lector formis).
*V.* in Thracia.

**Chionia uitis** uuas grandis magis quam multas habet: nomen autem inde
hoc sumens quod multum uini fluat *lib. gloss.* (*cum Vergilii nota*);
*Mai* VII 555; *Isid.* XVII 5, 28. *Cf.* spionia *et* chironia *in
lexicis.*

**Chiragra nodosa** V 653, 48 (*cf. Hor. Ep.* I 1, 31).

**Chiragricus** χείραγρος III 330, 33.

**Chirographum** γραμματεῖον II 264, 50. δάνειον II 266, 25; III 277,
26. manuscriptum eo quod propria manu sit (*vel* fit) II 573, 7.
manuscriptio V 350, 13. propria scriptura IV 318, 15; *p.* XLIII (*cf.*
XLII). cautio propria manu scripta IV 216, 46. **chirographus** cautio
IV 34, 56 (*cf. Isid.* V 24, 22).

**Chiron** Χείρων III 241, 59.

**Chironeus** (*ita e*, chicr. *codd.*) παναξιδος II 100, 20 (*ubi*
πανακίδος *d.* ὀπόπαναξ, εἶδος βοτάνης *c: immo* πάναξ εἶδος β.).

**Chironomia** *v.* gesticulum.

**Chirurgia** χειρουργική III 206, 28. ferramentorum incisio III 599,
21. *Cf. Isid.* IV 9, 3.

**Chirurgicus** χειρουργός III 296, 31.

**Chlamydem coccineam** id est clamidem rufam V 564, 46.

**Chlamys** χλαμύς II 477, 19; III 272, 65; 323, 21. χλαῖνα III 370, 13.
**c[a]lamida** uestis quaedam regalis II 573, 20 (*cf. Loewe GL. N.
p.* 32). **chlamys** ex una parte induitur non consuta, sed fibula
infrenata (-tur?) V 564, 47 (*cf. Isid.* XIX 24, 2). **clamide** χλαμύς
III 193, 21. *V.* diploide.

**Chlorus** χλωρὸς ὁ στρουθός II 477, 34.

**Choerogryllus** \<e\>ricius V 565, 39.

**C[h]oeus** et **Enceladus** gigantes, terrae filii IV 434, 33
(*Verg. Aen.* IV 179).

**Choicus** terrenus uel puluis seu malus V 494, 71 (*cf. GR. L.
suppl. p.* 185, 15).

**Cholera** χολή III 246, 55. uentris solutio IV 496, 32; V 276, 33.
umores V 351, 34. *Cf.* astian **colera** cum scara (= eschara) III 597,
44. *V.* fel rufum, chroma, bilis.

**Cholericus** passio acuta quae latenter uenit et subito occidit, aut
ipsa die periet uel liberabitur III 598, 11.

**Choragium** ornatus mimicus (*vel* mimici) [hoc est timorum] IV 397,
9 (timorum = mimorum *Loewe putat GL. N.* 222, *qui glossema delet*,
thymelicorum? id est tiara *cod. a exhibet*), ornatus mimicus IV 293,
1; 575, 37; V 486, 16. *Cf. com. ed. min. Ribb. p.* 382. **coragio**
pars est in ludis quando prouerbia dicuntur IV 45, 6 (*om.* in); 500, 28
(*item*); V 185, 24 (*de qua glossa obscura cf. Loewe l. s. s.:* pars
est funeris quando deuerbia dicuntur, *vix recte*, diuerbia desierunt
*Maehly Phil.* XLVIII *p.* 646). **coragio** puer\<orum\> est ludus
quando prouerbia dicunt. Alibi legitur: **coragium** pars est in ludis
quando prouerbia dicuntur V 593, 49. prouerbium *est* adagio. *Cf.
Festus Pauli p.* 52, 10; *Wessner Comm. Ien.* VI 2 *p.* 121.

**Choras** *v.* batrachium.

**Choraules** χοραύλης III 172, 53; 240, 1. **c\<h\>oraula** mimus IV
44, 40 (*cf. com. ed. min. Ribb. p.* 382). iocularius (!) IV 325, 31;
V 594, 58. iocularis V 596, 30. princeps chori ludorum (ludiorum?), quo
nomine potest dici totus chorus V 593, 22. cantator *Plac.* V 12, 32 = V
58, 36. **choraulae** χοραῦλαι III 10, 41; 302, 33; 525, 21. *Aut huc
aut ad singu­larem spectant:* **coraule** χορ[ο]αυλη III 371, 72;
coraule **coraule** III 84, 18.

**Chorda** χορδὴ τὸ ἔντερον II 477, 59. χ. ἡ νευρά II 477, 60. χορδή III
14, 48. *Cf.* sparruius (spasmus?) **cordarum tensio** III 605, 39.
**chorda** corda III 491, 25. πλεκτή III 184, 1. **cordam ex aqua**
πλεκτὴν ἐξ ὕδατος III 218, 32 = 653, 11. *Cf. Krumbacher Comm. in hon.
Christii p.* 361.

**Chordapsus** flegma frigida longaone illigata III 598, 33.

**Chorea** graece saltatio cum cantilena classium concinentium V 352,
45 + 46 (cf. *Isid.* VI 19, 6). **coreae** cantus cantantum V 185, 29.
**coreas** sonus[in] ludorum, a choro dictum V 185, 26. cantica, a
choro tractum. Vergilius (*Aen.* VI 644): pars pedibus plaudunt coreas
