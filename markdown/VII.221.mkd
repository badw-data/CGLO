πλοίου II 249, 20. ermos (ἕρμα?) III 205, 5. fundamentum II 591, 52.
onus IV 388, 17; V 545, 38. onus quo[d] uacuas naues stabiliunt V 480,
57. onus quodlibet quo naues uacuae stabiliuntur V 394, 24; IV 564, 41.
onus quodlibet V 330, 14. sonus liquis libet (onus quodlibet?) V 580, 5.
arena V 580, 9. arena, id est onus quo[d] uacuae (-uas *codd.*) naues
stabiliuntur IV 281, 24. harena et petra, sabburra, sarcina, suppellex V
654, 19 (*contaminata*? sagma?). lapis magnus V 388, 26. dicitur quando
lapides et ligna mittunt in nauem, qui non habent alia onera V 390, 24.
*V.* subura.

**Saccarius** σακκοπλόκος II 429, 36. σακκοϋφάντης III 309, 35. saccorum
factor II 592, 11.

**Saccatum uinum** expressum per saccum: inde nomen accepit graece V
145, 44; 242, 10. *Cf. Isid.* XX 3, 11.

**Saccellarius** βαλλαντιοτόμος II 255, 41/40. βαλλαντιοφύλαξ 255, 41.

**Saccellus** βαλλάντιον II 176, 36; 543, 13. **saccellum** βαλλάντιον
III 194, 34. βαλλαντίδιον II 255, 39. μαρσύπιον III 115, 3 = 643, 25;
369, 69. **saccellus** μαρσύπιον (marsupium *cod.*) III 499, 54.
**sacellus** marsippo III 626, 50. marsicpo III 614, 46. marsippus III
592, 73. *De* sacellus *pro* sacc. *cf. Haupt Op.* III 402. *V.* melo 2.

**Saccia** σάκκος II 429, 35; 493, 66. saccus, marsupium II 591, 51.

**Sacco** διηθῶ II 277, 9. διυλίζω II 279, 12. **saccat** ὑλίζει II 176,
35.

**Sacculus** βαλλάντιον II 255, 38; 491, 59; III 273, 25. **sacculum**
βαλλάντιον II 255, 38. marsuppium IV 388, 18; V 545, 39. *V.* sarculus.

**Saccus** σάκκος II 429, 35. ὑλιστήρ II 462, 48. σάγος, σακάλιον,
σάκκος, βαλάντιον III 269, 43 (*unde?*). μάρσιπος III 327, 63; 530, 50.
**saccum** μαρσίπιον III 327, 62; 530, 49.

**Saccus uinarius** ὑλιστήρ II 177, 1.

**Sacelliones** sacculos V 513, 3 (*v.* saccellus). *Cf. Osb.* 192.

**Sacellum** ναϊσκάριον II 374, 54; III 238, 31. templum modicum V 146,
30. **sacella** loca sacra IV 165, 46; 281, 22. **sacellae** loca sacra
V 330, 36.

**Sacer** ἅγιος, ἱερός II 176, 37. ἱερός II 331, 25; III 462, 70; 496,
4. **sacrus sacer** θεῖος τόπος II 327, 7. **sacer** sacratus IV 462, 46
(*Verg. Aen.* V 761?). consecratus IV 165, 43; V 330, 26. sanctus IV
281, 11; 565, 1. **sacra** ἱερόν II 497, 60. **sacrum** ἱερόν, θεῖον II
176, 47. τὸ[ν] σεπτόν II 176, 51. θεῖον II 327, 8; III 238, 40. ἱερόν
II 331, 21; III 9, 70; 301, 31; 362, 7; 462, 77; 496, 5. θυσία III 75,
35. ἱερεῖον II 331, 17. μυστήριον III 418, 24. media res est, et bonum
et malum, nam dicimus 'sacer est\<o\>' [id est] alicui quem nolumus
execrari aut occidi. et item consecrari (?) ad bonum ut 'sacra mari
colitur medio gratissima tellus' (*Verg. Aen.* III 73). ad malum 'auri
sacra fames' (*Verg. Aen.* III 57). et 'sacrae panduntur portae'
(*Verg. Aen.* VI 573/4). unde et ignis sacer dicitur ulcus horribile
*Plac.* V 40, 10 = V 97, 16 = V 146, 51. *Cf. Isid. Diff.* 498; *Serv.
in Aen.* I 632; III 57. sanctum, diuinum IV 166, 28. sanctum, diuinum
uel aliter malum ex[a]orabile (*cf. Serv. in Georg.* II 395, *al.*
exacrabile?) V 330, 12. **sacra** ἱερά, ἀπόρρητα, μυστήρια II 176, 39.
ἱερά II 331, 7; III 170, 31. θεῖα II 326, 59. consecranda uel sancta IV
167, 14. consecrata, diuina IV 281, 20. horrenda uel detestabilia V 391,
2. **sacra et diuina** τέρα (ἱερά?) καὶ θεῖα III 422, 71. *V.* in
sacris.

**Sacerdos** ἱερεὺς καὶ ἱέρεια II 176, 38. ἱερεύς II 331, 12; 556, 28;
III 10, 3; 75, 69; 83, 58; 146, 68; 171, 21; 237, 67; 301, 65; 332, 11;
341, 53; 348, 22; 362, 17; 393, 42; 418, 15; 462, 71; 496, 6. ἱέρεια II
331, 16. antistes IV 388, 20. **sacerdotes** ἱερεῖς III 146, 69. *V.*
antistes, primus sac, summus sac.

**Sacerdos rusticus** *v.* bucco, egones.

**Sacerdotalis** ἱερώτατος III 332, 12. **sacerdotale** *v.* ereon. *V.*
sacramentum.

**Sacerdotium** ἱερωσύνη II 331, 35; 505, 24; 548, 44; III 462, 72.
ἱερατία II 331, 11. ierosinium (= ἱερωσύνιον?) III 496, 7.

**Sacer ignis** *v.* ignis sacer.

**Sacer morbus** ἱερὰ νόσος II 331, 9; 489, 39. *V.* morbus s.

**Sacmina** θαλλός, σπονδαὶ ἀρχῆς (σπονδαῖα *h.* sagmina … σπονδαῖα
ἀρχαίως *vel* sagmina θαλλός, σπονδεία, sagminarius σπονδειάρχης
*Scaliger*) II 176, 34.

**Sacomaria** *v.* saucom.

**Sacra fames** (*vel* famis) execranda cupiditas IV 281, 13; 422, 34;
462, 47 (*Verg. Aen.* III 57). *V.* sacer.

**Sacra iura** sacramenta militaria IV 166, 26 (*Verg. Aen.* II 157?).

**Sacralis** καθιερωμένη III 170, 38.

**Sacramentum** ὁρκωμόσιον II 176, 43. ὅρκος στρατιωτικὸς II 176, 48;
386, 57; 501, 18. στρατιωτικὸς ὅρκος II 438, 49. ὅρκος II 386, 56; III
462, 73. παραβόλιον (παρεμβ*. H.*) ὃ καὶ ὅρ\<κ\>ος στρατικὴ συνστεμα
(στρατιωτικὸς ἢ σύστημα?)
