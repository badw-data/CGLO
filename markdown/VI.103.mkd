*Varro de l. l.* V 104). μυάκανθος (asparage) III 553, 7.
**asparagum** ἕλειον III 16, 18 (*cf.* **sparagun** elion III 185, 50).
**asparagi** ἀσπάραγοι III 88, 42; 400, 72. μυάκανθοι III 359, 56. *Cf.*
esparagus cyprinus id est **sparagos** III 561, 22 et **sparagus
siluaticu\<s\>** μυάκανθος III 540, 62. **isparagas** id est
**sparagus** III 565, 72. isparagos nostros (!) id est **sparagus** III
565, 73. **sparagos** agantelos (*cf. Pseudapul.* LXXXIV) III 535, 12;
549, 22; 617, 30. μυάκανθος III 595, 20 (sparoga); 569, 51 (sparagus);
629, 12 (sparago); 548, 11 (sparago); 569, 74 (sperago). **sparigus**
(*sine interpr.*) III 543, 11. libycum id est **sparagus** III 567, 69.
*Cf.* κήπεια ἕλεια **escarii asfaragi** III 430, 19 (*David Comm. Ien.* V
233). *Cf.* allium.

**Aspectans** uidens IV 475, 29.

**Aspectio** (adsp.) θεωρία II 24, 13.

**Aspecto** θεωρῶ II 328, 15. ὁρῶ II 387, 44 **aspectat** (*vel* adsp.)
aspicit IV 10, 24; 22, 9; 475, 28. **aspecta** contra aspice V 441, 38.
**aspectare** uoluptuose intueri IV 208, 38; V 441, 39 (intuere); IV
311, 26 (intendere).

**Aspectus** πρόσοψις, ὁμιλία II 21, 53. ὄψις ἡ πρόσοψις II 391, 33.
πρόσοψις II 422, 36; 487, 27; 537, 15 (*cf.* II 549, 19); III 174, 69;
247, 14. βλέμμα II 258, 7; III 511, 27; 310, 66. ὅρασις, βλέμμα,
πρόσοψις III 328, 42. θέα II 509, 9. ἄποψις hic **aspectus**,
conspectus, prospectus, suspectus ut Virgilius lib. VI (579): quantum
(!) ad aetherius (!) caeli suspectus Olympum II 242, 50. *Cf.* filargia
**aspectus** II *p.* XXXVII (*contam.?*). **aspectus** uultus, facies
IV 311, 27.

**Aspeleo bethlem** sarculum (?) (*cf. Cass. inst.* IV 31: a spelaeo
*etc.*) V 425, 30.

**Aspellens** expellens *Plac.* V 5, 3 = V 48, 33.

**Aspellit** ἀπωθεῖ II 24, 18. **apellit** ἀπολακτίζει, ἀπωθεῖται II 21,
44. *Cf.* **apellit** uetat, prohibet IV 207, 20 (appolit); V 267, 8.

**Asper** τραχύς II 24, 21; 458, 28; III 372, 64; 467, 34. ἀπότομος τῷ
ἤθει II 241, 50. τραχύς, ἀπότομος III 335, 73. ταραχώδης II 451, 50.
αὐστηρός inde austerus, id est durus, asper *marg.* II 554, 8. ἀφειδής,
ὁ ὀργίλος II 252, 39. obscurus (durus?), aridus (arduus?) uel districtus
IV 21, 49. durus IV 208, 31. lapidosus V 268, 42. tortus (toruus?),
bellicosissimus IV 311, 28. **aspera** τραχεῖα II 458, 23; III 209, 46
(*cf.* III 427, 15). petrosa IV 473, 27 (*v.* asperata). iracunda, ferox
IV 473, 28. hirta (erta) uel ferox *Plac.* V 48, 34. **asprum, asperum**
τραχύ II 458, 27. **asprum** natura uel actu fit, **asperum** gustu
probatur V 561, 52. **asperum** τραχύ III 79, 63; 322, 57. **asprum**
ἔκλευκον II 291, 24. δηνάριον, τραχὺ ἤ ἔκλευκον II 269, 57. asperum II
568, 27. **asperrima** ferocissima V 268, 52. saeuissima, ferocissima,
acerbissima IV 473, 29. **asperrimum** τραχύτατον II 458, 31. V asper
tactu.

**Aspera hiems** uiolenta tempestas IV 22, 3; 473, 30.

**Asperata** quasi petrosa V 492, 2. *V.* asper.

**Aspera uox** est rauca et quae dispergitur per minutos et indissimiles
pulsus *a post* IV 6, 38.

**Aspergine** περικλύσματι II 24, 20. aspersio est V 632, 36.
**aspargine** asparsione IV 22, 1 (= *Verg. Aen.* III 534).
περιραντηρ\<ίῳ\> II 404, 22.

**Aspergo** ῥαίνομαι, ῥαντίζω III 244, 57; 246, 39. **aspargo** reni
(ῥαίνω?) III 79, 9. roro V 268, 47. **aspargis** (-es *c*) ῥαντιεῖς II
24, 17. **asparge** ῥᾶνον III 79, 10. **asperserit** προσκλύσῃ II 24,
16. *V.* aspargere uoces.

**Asperitas** ἀγριότης II 217, 21. ἀγριότης, ὠμότης II 560, 16 (*ex
Boyseni supplemento*). τραχύτης II 458, 32. **asperitates** τραχύτητες
II 24, 19.

**Aspernanda** neganda II 21, 54 (*Verg. Aen.* XI 106). contempnenda
II 21, 51. reicienda IV 22, 7.

**Aspernatus** contemptus IV 5, 2 (absp.).

**Aspernit** contemnit, dispicit (de- *b*), recusat IV 473, 32.
**absternit** abicit, repellit V 613, 1 (*nisi* absterret *subest*).

**Asperno** καταφρονῶ II 345, 2. ἐξουδενῶ II 304, 23. ἐκμυκτηρίζω II
291, 48. **aspernatur** ἀπαναίνεται, ἀπαρνεῖται II 21, 52. ἐξουθενίζει,
ἀναξιοπαθεῖ II 21, 43. contempnit V 259, 35 (absp.); 161, 18 (absp.).
contemnit, despicit IV 22, 13; 208, 32 (disp.); 311, 29 (depegit. *ubi*
uel dedignat *ac add.*). despicit, detestatur, contempnit IV 21, 45.
detestatur, dedignatur IV 473, 31. fastidit, contempnit V 268, 48. *Cf.
GR. L.* II 383, 3.

**Aspero** τραχύνω II 458, 30.

**Asperrima \<belli\>** bellicosissima IV 430, 47 (*Verg. Aen.* I 14).
*Cf.* bellicosus.

**Aspersum** respersum IV 311, 30.

**Aspersus** εἶδος ἰχθύος καταστίκτου II 24, 8 (*Iuvenal.* V 104: *cf.
ed. Friedlaenderi p.* 107).

**Asper tactu** ἄγριος τῇ ἁφῇ II 217, 20 (*Hor. carm.* III 2, 10?).

**Asphalaga** (asfalaga *cod.* = ἀσπάλαξ) est talpa V 616, 8; *gloss.
Sal. V.* talpa.

**Asphaltiis** *v.* bitumen.

**Asphodelos** (*cf. Pseudapul. c.* XXXIII) *in his latet glossis:*
arapdion **asfodillum**
