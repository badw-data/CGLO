um *Deuerling. om. R*) releget aut in longinqua transmittat (transmittit
*R*) *Plac.* V 5, 15 = V 46, 22. *Cf. Serv. in Aen.* III 50.

**Amandula** Θασία III 578, 2. *Cf. Plin.* XV 90 *et von
Fischer-Benzon p.* 159. *V.* amygdala.

**Amandus** φιλητός III 333, 1.

**Amaneo** ἀποκοιτῶ II 237, 46. **amanet** extra manet IV 16, 7; 308,
10; V 265, 21; 345, 18. amansit ἀπεκοίτησεν II 15, 50. expectauit IV
405, 35; V 591, 11. spectauit seu expectauit V 439, 7.

**Amans** προσφιλής ἀρρενικῶς II 423, 29. ἀγαπητός II 215, 48.
**amantissimus** προσφιλέστατος II 423, 31; 534, 2. φίλτατος II 472, 3.
ἀγαπητός II 215, 48. *Cf.* III 108, 3 = 638, 1; 111, 21 = 641, 12.

**Amanuensis** προχειροφόρος, προχειράριος II 15, 49. qui se audaciter
inscribit (se inducit in scribis *a*) alicui rei II 566, 27
(*interpretamentum obscurum: male vertit ex graeco qui collegit*).

**Amara** ὑπόνομος II 467, 20. *V.* ἀμάρα, camara.

**Amaracus** genus floris IV 16, 5; V 265, 24. genus floris iucundi IV
429, 21 (= *Verg. Aen.* I 693). genus floris unde et amaracinum
unguentum fit V 165, 38.

**Amarantus** ἀμάραντον III 266, 44. **amarantum** αμάραντον III 192,
31.

**Amaratha** sic dic (syriace dicitur *Roensch*) perditio in aduentum
domini V 265, 39. *Cf. Roensch Mus. Rhen.* XXXI 456. *V.* maranatha.

**Amare** *v.* amale.

**Amare focos** domus construere IV 472, 40 (*cf. Verg. Aen.* III 134).

**Amaresco** *v.* acesco.

**Amaricat** exacerbat, prouocat IV 308, 11. **amaricauerunt**
irritauerunt V 439, 8. *V.* aceo.

**Amaricatum** incitatum IV 429, 22.

**Amarina** genus uirgulti amari *lib. gloss. = Mai* VII 551 (*v.*
amera).

**Amaritas** πικρία γεύσεως II 15, 52.

**Amaritia** πικρία II 407, 50.

**Amaritudo** πικρία II 407, 50; III 439, 28. iracundia V 439, 9.

**Amarola** *v.* chamaedrys, myrobalanus.

**Amarthath** Cappadocum lingua Augustus mensis dicitur V 165, 39. *Cf.
Ideler* I 422.

**Amarus** πικρός II 407, 52; III 255, 52; 335, 47. crudelis uel saeuus
IV 471, 7. pygras (πικράς?) id est **amara** uel pygra III 572, 64.
**amarum** πικρόν II 16, 1; III 184, 52; 439, 27; 502, 24.

**Amaryllis** Roma; antiqui ita appellauerunt V 115, 29 (*cf. Serv. in
Ecl.* I 5, 36). **Amaryllida** Roma V 439, 5. **Amaryllidis** Romana (?)
V 439, 6.

**Amasco** amare cupio *Scaliger* V 589, 2 *ex Osb. p.* 7; 43 (incipio).
*Cf. GR. L.* I *p.* 343, 11.

**Amasius** est pronus ad amorem V 615, 25. amatus puer *b* II 566, 16.
**amasios** amatores V 560, 47. *Cf. Plaut. Truc.* 658.

**Amathus** et **Paphus** insulae et ciuitates in Cypro V 549, 23 (*cf.
Serv. in Aen.* X 51 Cypri insulae ciuiates sunt).

**Amatio** amor V 637, 15 (*=Non.* 70, 22).

**Amator** ἐραστής II 313, 48; III 4, 27; 137, 4/5; 178, 60; 251, 17.
φιλητής II 471, 31; III 372, 60. φιλητής, ἐραστής III 336, 11; 439, 30.
**amatoribus** φιληταῖς III 30, 33.

**Amatores riuales** duo qui amant unam, quasi qui de uno riuo bibunt *a
post* IV 16, 32. duo qui amant unam quasi de uno riuo V 165, 41. duo qui
amant V 265, 17. **amatores** duo qui unam amant quasi de uno riuo IV
308, 13. qui amant unam quasi de uno riuo V 344, 23. quod amant quoniam
(duo unam?) quasi de uno riuo *cod. Epin. post* V 345, 27. *Cf. Donat.
ad. Ter. Eun.* V 9, 42. *V.* riualis, spudasten.

**Amator iactantiae** φιλόπομπος II 559, 6 (*margo*).

**Amatorium** φίλτρον II 15, 47; 472, 4. χαριτήσιον (χαριστήριον *e*) II
475, 48.

**Amatorius** ἐρωτικός II 15, 48; 315, 1. puellarum amator II 566, 22.

**Amator pecuniae** φιλάργυρος III 335, 65. πλεονέκτης III 335, 66.

**Amator seruorum** φιλόδουλος II 471, 35.

**Amator uerborum** φιλόλογος II 471, 39.

**Amat perditim** et **perdite** V 660, 15.

**Amatus** ἐραστός III 439, 31. **amatos** φιλητούς III 400, 15.

**Amaus** populus abiectus IV 472, 39. *De* Ammaus *cf. Onom. sacr. p.*
64, 8.

**Amazones** gens Scytharum III 509, 64. *Cf.* Penthesilea.

**Ambactus** δοῦλος μισθωτός ὡς Ἔννιος II 16, 3 (*inc. sed.* XX *ed. L.
M.*). seruus gallice V 439, 11. **amb\<act\>i** serui *Plac.* V 7, 43 =
V 47, 2 (*suppl. Koch*); V 616, 10. **ambacti** serui, coloni, lixae seu
circum est V 439, 10 (*cf.* ambedo). *Cf. Festus Pauli p.* 4, 13. *Non
recte* anculi *apud Placidum proponit O. Muellerus.*

**Ambages** πλάναι, περίοδοι II 16, 4. circuitus uerborum IV 471, 26.
circuitus uerborum uel anf\<r\>actus IV 15, 42. circuitus uerborum,
obliquitates IV 429, 24 (*Verg. Aen.* I 342; VI 29; 99). circuli,
circuitiones IV 309, 2. circuli uel cir-
