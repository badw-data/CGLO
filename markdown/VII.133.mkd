(intemptanter); 475, 56. inprobiter V 475, 52. superbe V 414, 21 (*reg.
Bened.* 3, 9).

**Procantat** προᾴδει II 159, 41.

**Procanus (?)** ornatus aedificiorum IV 147, 23; 150, 27; V 324, 27;
383, 28; 475, 55. πρόδομος *Nettleship 'Journ. of Phil.'* XIX 294.
Plocamus *Helmreich Arch.* VII 275. Procinctus *H. V.* procerus.

**Procapis** proximus IV 557, 48; V 607, 60; 630, 52. **procapibus**
proximis IV 557, 49; V 607, 59. *Cf. Loewe Prodr. p.* 351; *Festus Pauli
p.* 225, 4.

**Pro captu** (*uel* perc.) faengae (*AS.*) V 378, 3 (*cf. Oros.* IV
*praef.* 5).

**Procare** non (nunc *H.* enim *Buech.*) est poscere V 475, 53; 511, 3.
pascere V 630, 51. *Cf. Isid.* IX 7, 7; X 214; *Festus Pauli p.* 224,
17; *Varro de l. l.* VII 80; *Serv. in Aen.* I 536.

**Procas** qui post mortem patris nascitur IV 148, 2; 274, 27; V 511, 5
(procax). *V.* postumus. *Glossa ex his contaminaia:* Procas \<rex fuit
Albae Longae, Postumus\> qui e. *q. s. Cf. Loewe Prodr.* 395.

**Procastrium** ὁ πρὸ τῆς παρεμβολῆς τόπος II 385, 67. **procastria**
(singularia non habet) οἱ πρὸ τῶν παρεμβολῶν τόποι II 381, 13 (*GR. L.*
I 550, 12) οἰκήματα πρὸ παρεμβολῆς II 159, 42. aedificia ante castra uel
quae ante portas sunt V 475, 54. aedificia quae sunt ante castra uel
extra portam V 525, 27; 575, 57 (praeestria). quae ante castra sunt V
576, 42. aedificia ante castra V 511, 4. procestria *tutatur coll. Festo
Pauli p.* 225, 12 *Landgraf Arch.* IX *p.* 415. *V.* proastion. *Cf. GR.
L.* VII 283, 22.

**Procax** προπετής II 419, 50; III 335, 53; 54 (porcax). αὐθάδης,
ἰταμός, προπετής II 159, 45. ἰταμός II 333, 57. ἅρπαξ II 245, 50.
κωμαστής III 251, 2 (*unde*?). celerrimus IV 459, 33 (*Verg. Aen.* I
536). uerbosus IV 151, 10. uerbosus, ingratus (?) IV 380, 19. uerbosus
uel inprobus aut superbus IV 555, 42. audax uel frontosus aut inportunus
IV 147, 26; V 475, 57. inportunus, inprobus IV 274, 24; 380, 20.
superbus, audax, frontuosus V 323, 44. proteruus, inportunus aut
chromaticus IV 150, 8 (*Ter. Hec.* 159. procax pr, inp, procurator
pragmaticus *Nettleship 'Journ. of Phil.'* XIX 294, non recte), petax V
556, 32 (*cf. Don. in Hec.* 12, 84; *Serv. in Aen.* I 536). **procaces**
uehementer flantes IV 420, 47. **procacibus** improbe flantibus IV 150,
52; V 475, 58 (*v.* procacibus austris). **procacioribus**
inpudic\[it\]ioribus V 324, 12.

**Procedens** *v.* progenicultor.

**Procedo** πρόειμι II 418, 5. προχωρῶ II 424, 46. προέρχομαι II 417,
35; III 152, 57. προκόπτω II 418, 35. ἐκπορεύομαι II 292, 24.
**procedit** προέρχεται III 152, 58. προάγει II 159, 50. προκόπτει ἐπὶ
τάξει II 159, 43. **procedimus** προέλθωμεν II 417, 29 (-amus *e*).
**procedere** ut decedere V 646, 22 (*Non.* 161, 8. pertidere ut
distidere *Luc. Mueller*). **processit** προῆλθεν III 152, 59; 339,
49; 461, 6. προέκοψεν ἐν στρατιᾷ, προελήλυθεν II 159, 47.
**processerat** προεληλύθει, προκεχωρήκει II 160, 5. *V.* praecedo.

**Procella** καταιγίς II 159, 46; 340, 63; III 169, 58; 172, 17/16; 354,
23. συνστροφὴ ἀνέμου II 447, 57. σπιλάς II 435, 44; III 294, 12. θύελλα
II 329, 42; III 245, 49. λαῖλαψ II 358, 9. καταιγὶς ἡ ἄελλα III 244, 63
(*unde*?). καταφορὰ τῶν ἀνέμων III 294, 13; 524, 46. tempestas IV 151,
17; 380, 21. tempestas, uis uentorum IV 459, 35 (*Verg. Aen.* I 102;
VII 594). tempestas nenti (*uel* uis uenti) subita IV 274, 31. pluuia
aut tempestas IV 555, 39. tempesta (!) aquae siue uentorum IV 420, 45.
impetus uenti et fluctus V 137, 23 (procellis). pluuia cum tempestate V
137, 17; 235, 40. pluuia cum tempestate IV 147, 29. pluuia
tempestiua (!), caput uenti V 324, 47. subita uis uentorum cum pluuiis V
235, 41 (*cf. Serv. in Aen.* I 85). **procellae** καταιγίδες III 295,
27; 524, 54. καταιγίδες, καταφορ\<αὶ ἀνέμων\> III 295, 28 (καταφορεων
*cod.*). **procellas** tempestiui uenti IV 555, 40. *V.* totegis.

**Procello** προφθάνω, ὑπερέχω II 159, 44. **procellit** προέχει, ἐξέχει
II 159, 49. **procellit** praeuertit V 475, 58. **proculit** prostrauit,
elisit V 476, 2. **proculsit** mactauit, immolauit IV 380, 33; V 476, 3.
percussit, turbauit, terruit V 476, 4 (perc.?). \[filia neptis\] V 576,
46 (*v.* procul sitas nationes). *Cf. Roensch Coll. phil.* 229. *V.*
praecello.

**Procellosus** καταιγιζόμενος II 340, 64.

**Proceres** ἔξαρχοι, πρωτοπολῖται II 160, 1. ἔξαρχοι II 302, 19 (*cf.*
princeps); III 275, 62. πρωτοπολῖται (singulare non habet) II 425, 4
(*GR. L.* I 33, 1). magnifici, potestates IV 420, 34. primati (!),
nobiles IV 274, 32. priores, principes, primates IV 380, 22. primates,
uiri electi aut principes IV 147, 28. primates, uiri electi aut
principes, priores uel iusti IV 555, 37. principes V 137, 15. primores
IV 151, 11. priores, principes V 324, 2. *sine interpr.* V 428, 14 (de
*Euseb.*). sunt capita tri
