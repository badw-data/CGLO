ci\<n\>a IV 362, 41. *Cf. GR. L. suppl.* 215, 3. *V.* carnificina.

**Macellus** macilentus V 644, 6 (*Non.* 136, 27).

**Macer** λεπτός, ἰσχνός II 125, 43. λεπτός II. 359, 42; III 181, 3;
252, 53; 567, 58. ἰσχνός II 333, 40; III 329, 46. tenuis, subtilis IV
362, 42.

**Maceratum** τετηγμένον II 125, 44.

**Macere** macie infestari (infestare *libri omnes: corr. Maius*)
*Plac.* V 33, 26 (macie maciei) = V 82, 15 = V 116, 11 (macere macere).

**Macerefallio (?)** locus circumueniendi *Scal.* V 604, 46
(machaeropolion 1. cultrum uendendi *H.* ματρυλεἴον *Buech.* machinator
falsilocus? *idem*).

**Maceria maceries** τριγχός II 458, 59. **maceria** τριγχός II 495, 11;
519, 31; III 199, 49; 300, 24; 355, 13. θριγκός III 455, 28; 485, 75.
θριγκός, τριγχός III 262, 18. **maceries** περίβολος III 312, 33.
τριγχός II 125, 42 (θριγχός *cod.* θριγκός *e*); 519, 37 (ὁ θριγκός);
III 365, 27. **maceria** lapis tantum V 371, 39. **maceriam** saepem IV
114, 40. structilem saepem V 537, 16 (*Ter. Ad.* 908). **maceriem**
saepem V 114, 46. **maceriae** aedificia sine cemento V 507, 1.
**maceries** parietes V 644, 36 (*Non.* 141, 18).

**Macer\<i\>atio** τρίγχωσις II 458, 60 (*suppl. e*).

**Maceries** maceratio V 644, 18 (*Non.* 138, 10).

**Macero** macies II 587, 10 (macerio maceriarum constructor *commemorat
De-Vit: cf. Osb. p.* 348: *nisi alius error subest.* macor *H.*). *V.*
machiones.

**Macero** λεπτύνω II 359, 47. μαραίνω II 364, 56. τήκω II 455, 4
(marceo *e*). **macerare** angere V 537, 8 (*Ter. Andr.* 685).
**ma[r]ceror** μαραίνομαι II 364, 57 (*corr. a e*).

**Macetae** (-ti *codd.*) Macedones IV 256, 31.

**Machaera** gladius IV 536, 50; V 114, 49 (*GR. L.* VII 305, 4).

**Machanios** Byzantinorum lingua December mensis dicitur V 220, 5. *Cf.
C. F. Hermann Phil.* II 263. *V.* menses.

**Machaon** Aesculapii filius, medicorum maximus IV 451, 14. Scholapii
filius, medicorum \<maximus\> V 544, 27 (*Verg. Aen.* II 263: *cf.
Serv.*).

**Machina** μηχανή II 371, 26. fabrica V 114, 37. syllaba secunda per h
V 114, 42 (*GR. L.* VII 304, 34). magnitudo IV 115, 2 (*ubi de
contaminatione cogitat Nettleship 'Journ. of Phil.'* XIX 188: *cf.*
maiestas). **machinis** argumentis IV 114, 32; 535, 47. *V.* machinor.

**Machinamentum** triumphum (*cf.* IV 398, 24 tropaeum) IV 362, 43; V
544, 28.

**Machinantem** struentem IV 536, 36.

**Machinarius** μηχανικός II 371, 25. μηχανοποιός III 308, 54; 500, 16;
530, 7.

**Machinatio** dolus, excogitatio V 310, 6. **machinationes** commenta
astutiae IV 257, 20. *Cf.* **machinicium** (machinatione?) excogitatione
operis V 310, 51.

**Machinor** μηχανῶμαι II 371, 28. **machinatur** parat, instruit
[machinis argumentum] IV 112, 26 (*v.* machina). parat, instruit IV
257, 11; V 114, 43. cogitat IV 114, 25. cogitat[ur] IV 536, 37. mala
cogitat IV 362, 44. **machinat** conficit V 114, 48. **machinari**
cogitare IV 113, 8. **machinabantur** mol\<i\>ebantur V 544, 29.

**Machiones** constructores parietum V 220, 6 (*cf. Isid.* XIX 8, 2).
*V.* macio *sub* architectus.

**Machomenus** (*sine interpretamento*) V 411, 15.

**Macies** ἰσχνότης II 333, 41. λεπτότης ἐπὶ σώματος II 359, 43. tabes
IV 451, 15 (*Verg. Aen.* III 590). exilitas corporis IV 113, 3; 536,
39; V 309, 25. exiguitas corporis IV 256, 30. *V.* macero *subst.*,
tabes.

**Macilentus** λεπτόχρως III 252, 54 (*unde?*). macer IV 257, 12.
spilodis III 181, 7 (σπιλώδης *est* maculosus).

**Macir** *v.* cortex mali punici.

**Macio** *v.* architectus, machiones.

**Macius** *v.* mature.

**Macore** macie V 644, 7 (*Non.* 136, 29).

**Macritas** λεπτότης II 125, 45. *V.* emacitas.

**Macritudinem** maciem V 643, 72 (*Non.* 136, 2).

**Macrochir** graece, latine longimanus V 554, 23.

**Macrologia** longa oratio graece *Plac.* V 82, 16 = V 114, 40 (graece
*om. cf. GR. L.* I 271, 12).

**Macros** longus graece V 114, 39.

**Mactator** *v.* haruspex.

**Mactatus** immolatus (*vel* inm.) IV 113, 14; V 464, 18. **mactatos**
immolatos IV 451, 16 (*Verg. Aen.* II 667).

**Macto** θύω II 330, 11. σφαγιάζω II 449, 9. **mactat** σφαγιάζει, θύει
II 125, 46. immolat, delet, deruit IV 362, 45. **mactare** immolare V
114, 38 (*cf. Non.* 341, 27). **mactabam** immolabam IV 114, 11
(*Verg. Aen.* III 21). **mactaui** caesi, uel alias augere IV 114, 10
(auget *a*); V 464, 19 (*cf. Serv. in Aen.* IV 57; VI 248; VIII 85).
**mactauit** immolauit IV 114, 37 (*Verg. Aen.* III 118). immolauit,
perculsit (!) IV 363, 1. **mactari** ἀναιρεθῆναι II 125, 47.
