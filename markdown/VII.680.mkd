ἀριστερός, ἄφεσις, βραχίων, δάκτυλος, δράξ, ἐπάνω, ἐπιλαμβάνομαι,
θέναρ, καρπός, κατοχή, κοῖλος, κόνδυλος, μάχη, μισθός, μῦς, νίπτω,
παλαιστής, παρέχω, ὕδωρ.

**χείραγρος** chiragricus.

**\*χειραλυσίδιον** copula.

**\*χειράλυσις** manica, χειροάλυσις copula.

**\*χειραφεσία** emancipatio.

**χειράφετος** emancipatus.

**\*χειραφετῶ** emancipo.

**χειρεκμαγεῖον** *et* **χειρεκμάγεον** mantela *sim.*, mappa.

**χειρίδιον** manica, manicillium.

**\*χειριδῶ:** κεχειριδωμένος manicatus.

**χειριδωτός** manicatus.

**χειρίζω** gero, seco, tracto, troito; χειρισθείς *etc.* attaminatus,
gestum.

**χείριος:** χειρία *cf.* cherea (*expl.* manualis). *An nomen proprium*
Chaere a *Terentii Eunuchi explicatura*

**χειρίς** manica, manicium, manuela, manulea.

**χειριστής** gerulus, gerro, gestor.

**χείριστος** *v.* χείρων.

**\*χειροάλυσις** *v.* χειράλυσις.

**\*χειροβαλλίστρα** falarica.

**χειρόγραφον** cautio, cautus (-um), *cf.* chirographum (*expl.* cautio
propria manu scripta, manuscriptum *sim.*).

**χειρογραφῶ** caveo.

**\*χειρόδεσμος** copula, manica.

**\*χειροδόσιον** manupretium.

**χειροήθη** mansuefactum.

**χειροθεσία** gestus.

**χειρόμακτρον** mantela *sim.*, manutergium, mappa.

**χειρόμυλον** mola manualis.

**χειρόμυλος** mola manualis.

**χεῖρον** *v.* χείρων.

**χειρονομία** gesticulum.

**χειρονομῶ:** - ἐπὶ ὀρχηστοῦ II, 476, 36 *s. interpr. Cf.* σχεδιάζω.

**\*χειρόπεδον** manica ferrea.

**χειροπληθής** corpulentus, -ές manupretium (-pletium).

**χειροποίητος** manufactus.

**χειροσκόπος** manuinspex.

**χειροτέχνης** opifex.

**χειροτεχνία** opificium.

**χειροτονητής** creator.

**χειροτονία** creatio, creatura, nuncupatio, ordinatio, procreatio,
suffragium.

**χειροτονῶ** creo, nuncupo, -ηθείς *et* κεχειροτονημένος designatus.

**χειροτριβεῖ** ruspinat (*cf. p.* 438).

**χειρουργία** *cf.* chirurgia (*expl.* ferramentorum incisio).

**χειρουργικός** venarius (*cf.* chirurgicus), -ή chirurgia.

**χειρουργός** chirurgicus, opifex.

**χειρουργῶ** opifico, suspino (= ruspino).

**χείρων** deterior, peior, pessimus, χεῖρον (*praeter alia sub
adiectivis*) secus (sequius), χείριστος deterior, pessimus.

**Χείρων** Chiron. *Cf.* κλῆμα.

**χειρώνειος** *cf.* chironeus, -ειον panax, -εία centauria maior.

**\*χελιδονιαῖος** badius.

**χελιδόνιος** *cf.* chelidonia.

**χελιδών** hirundo (*cf.* chelidon), διὰ τῶν -όνων hirundo (medicamen
ex hirundinibus factum).

**χελλών** *cf.* helio (*piscis*).

**\*χελύδριον (?)** anguis.

**χέλυδρος** *cf.* chelydrus (*expl.* ὄφις, serpens aquaticus).

**χελύνιον** rostrum.

**χέλυς** cithara (*cf.* chelys).

**χελώνη** golaia, testudo.

**χελώνιον** claustrum (-ῶνι), pessulus.

**χελώνιος:** -α *cf.* testudia.

**\*χελώτρα** stillicidium.

**χερνίβι(ο)ν** ampulla, aquiminale.

**χέρνιβον** aquiminale. *Cf.* χερνιβόξεστον.

**χερνιβόξεστον** *rectius quam* χέρνιβον, ξεστόν *codicis lectionem cum
Du Cangio et Mauio interpreteris s. v.* aquiminale, gutus, trulleum
(*ubi errore* χερνιβόκεστρον).

**χερουβείμ** cherub (-bim).

**χερρόνησος** *et* **χερσόνησος** paeninsula.

**χερσαῖος** terrenus, terrester. *Cf.* βδέλλα, ἐχῖνος.

**χερσόνησος** *v.* χερρόνησος.

**χέρσος** incultus (-a), -η γῆ ἢ -ος τόπος dumus, inculta terra, -ος
τόπος glarea.

**χέρσυδρος** boa, chelydrus, coluber (*etiam* - ὁ ὄφις).

**χερσώδης:** -ες dumosum, -εις τόποι dumosa loca.

**\*χέσμα** fimus.

**χεῦμα** unda, χεύματα fluenta.

**χέω** fundo 2, χυθείς *et* κεχυμένος fusus 1. *Cf.* χύννω.

**χηλή** disulcis (-ca). *Cf* chelae (*expl.* brachia scorpionis).

**χήμη** peloris.

**χήν** anser (*cf. p.* 435), χῆνες ἐκβοῶσιν gingriunt, φωνὴ χηνός
gingrum.

**χηναλώπηξ** ganta.

**χήνειον** *et* -α anserina.

**χηνοβοσκός** anserarius (*et* pastor a.).

**χήρα** orbus (-a), vidua. *Cf.* φθορά.

**χηρεία** *v.* χηρία.

**χηρεύω** viduor.

**χηρία** (= χηρεία) viduitas, - ἀνδρός caelibatus, - γυναικός viduitas,
πρόστιμον χηρείας viduvium.

**χηρῶ** orbo.

**χήρωσις** caelibatus, viduitas.

**χθαμαλός** humilis. *Cf* χαμαλόν? **χθαμαλότης** humilitas.
