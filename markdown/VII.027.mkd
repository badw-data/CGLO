**Oppilago** curcilla IV 372, 24; *Scal.* V 606 11. quicquid in ore
ponitur ad ipsum os obturandum *Osb.* 402. *V.* curcilla.

**Oppilasco** τριχοφυῶ II 459, 59.

**Oppilat** (*uel* obp.) obturat (*uel* obd.), obcludit IV 265, 31; 133,
9; 545, 7; V 229 18. in claudit, obstruit, obturat IV 372, 25. claudit
IV 130, 27. cludit V 315, 28; 376, 30. **oppillauit** gigiscdae (ḡ scdae
*cod. Ampl.*, *AS.*) V 375, 34.

**Oppilo** τίλλω II 455, 34.

**Oppitandum** *praef. cod. Salm.* (V *praef.* V). *V.* horitandum.

**Oppito** sospito, ualde saluto *Scal.* V 606, 37. *V.* horitandum.

**Oppleo** ἐκπληρῶ II 292, 12. repleo V 574, 22. **opplet** complet IV
129, 10; 543, 45 (*Ter. Heaut.* 306). **opplere** obliuisci ad plenum
IV 133, 27; 372, 17; V 316, 46; 377, 15 (*quae explicanda ex* oppletum
oblitum usque ad plenum).

**Oppletum** (*uel* obpl.) oblitum usque ad plenum *Plac.* V 36, 21 = V
90, 6 = V 127, 5. **oppleta** inpleta (*Cassian. inst.* XI 15) V 417,
52.

**Opploro** (obp.) κατακλαίω II 341, 11.

**Oppono** ἀντιτίθημι II 230, 50. παρατίθημι III 155, 16. **opponere**
obicere IV 372, 26. **obponi** ἀντιστῆναι II 136, 32.

**Opportun-** *v.* oportun-

**Oppositio** ἀντίθεσις II 229, 34.

**Oppositus** ἀντίθεσις II 229, 34.

**Oppressio** καταβάρησις II 339, 45.

**Oppressus** καταθλιψις II 340, 57.

**Oppressus** βεβαρημένος III 130, 4. necatus IV 544, 24. **oppressi**
βεβαρημένοι III 130, 5. **obpressis** afflictis IV 455, 38 (*cf. Verg.
Aen.* I 129).

**Opprimo** συνέχω II 445, 56. καταθλίβω II 340, 56 (oppremo *cod. corr.
a*). συνλαμβάνομαι ἐπὶ τοῦ συνέχω II 446, 51. **obpressit** \<ob\>repsit
V 470, 15. **opprimor** συνέχομαι II 445, 55. **opprimar** συνσχεθῶ II
139, 17.

**Opprobrium** (*uel* nt *saepissime* obp.) ὄνειδος II 139, 16; 384, 6;
557, 52; III 458, 42. αἶσχος II 221, 6. ὀνειδισμός II 384, 5.
improperium II 588, 37; IV 546, 12. maledictum IV 128, 39; 370, 30.
crimen turpe IV 370, 31. maius crimen IV 129, 12. malum crimen IV 265,
25. maius crimen uel maledictus (!) V 124, 5.

**Opprobro** (obp.) ὀνειδίζω II 384, 4.

**Oppugnatio** καταμάχησις II 342, 15. πολιορκία II 412, 1.

**Oppugno** καταμάχομαι II 342, 16. καταπολεμῶ II 343, 5. **oppugnat**
πορθεῖ II 139, 18.

**Ops** Ῥέα II 139, 13 (δεα *cod.*); 427, 41; 508, 13; III 168, 6.
**Opis** (*cf. Festus Pauli p.* 187, 15) Ῥέα III 8, 60; 236, 64; 291,
26; 348, 32; 393, 52; 413, 22; 503, 11 (*cf.* Ῥέα **Ops, Opi\[u\]s** III
83, 15). **Ops** Terra, uxor Saturni V 555, 17. terra V 375, 40 (*cf.
Varro l. l.* V 64). **opis** auxilii IV 133, 32. **opem** βοήθειαν,
ἐπικουρίαν II 138, 51. auxilium IV 132, 49; 265, 2; 372, 3 (*v.* opem
fero). **ope** auxilio IV 544, 8 (*v.* ope eius); V 126, 26. **opes**
(pluralia\<!\> non habet) βοήθεια II 258, 24. περιουσία\<ι\> (singularia
non habet) II 403, 44 (*cf. GR. L.* I 33, 17). περιουσίαι, χρήματα,
εὐπορίαι II 139, 3. ὕπαρξις II 463, 28. diuitiae IV 132, 46; 265, 22;
543, 50. census, diuitiae V 555, 16. facultates IV 456, 11 (*Verg.
Aen.* II 4). *Cf.* **opis nostrae** facultatis nostrae singulari numero
V 126, 30 (*Verg. Aen.* I 601). **opis** possibilitas V 524, 21.
**opibus** copiis IV 133, 31 (*Verg. Aen.* VIII 171). *V.* diues opum,
famosae opes. *Cf. Serv. in Aen.* VI 325; VIII 377.

**Opsonium** *v.* obson.

**Optabile** εὐκταῖον II 317, 60.

**Optata arena** uotis desiderata terra uel litora IV 456, 15 (*Verg.
Aen.* I 172). non quod Africam uenire obtauerunt, set quia nauigantes
qua\<m\>cumque terra\<m\> uidere exobtant V 124, 35.

**Optatiuus** εὐκτικός II 318, 3.

**Optato** (obtate *cod.*) optate (obtare *cod.*), ex uoto V 537, 63
(Ter. *Andr.* 533).

**Optatus** εὐκταῖος II 317, 59. ἀσπάσιος III 125, 30/29. **optata**
(obt.) desiclerata IV 131, 10; V 124, 34. **obtatum** desideratum IV
131, 9; V 124, 33 (*Ter. Heaut.* 611). **optatis** desideratis IV 543,
22. **obtatius** obtabilius IV 130, 21; V 124, 32. *V.* ualde optatus.

**Optimates** (singulare non habet) ἄριστοι II 244, 37. (singulare non
habet: dicitur et optumas) κράτιστοι II 354, 50 (*GR. L.* I 548, 33;
VII 282, 23). πρωτοπολῖται II 139, 21. primarii uiri V 126, 43. *Cf.*
optimatus optimarum \<p\>artium auctor uel princeps IV 265, 13.

**Optime** ἄριστα II 244, 36. κάλλιστα II 337, 33. καλλίστως III 150,
52; 437, 38. **optume** oportune V 537, 60 (*Ter. Andr.* 335).
**optime** unice, praeclare V 126, 47 (*nisi vocativum esse sumes*).

**Optime factum est** ὡραίως ἐγένετο III 288, 10 = 658, 18. *Cf. W.
Heraeus 'Spr. des Petr.' p.* 35.

**Optimus** *v.* bonus.

**Optio** ἐπιλογή, ἐκλογή II 139, 19. ἐπιλογή II 309, 27. αἵρεσις II
220, 47; III 458, 44; 486, 39. electio, potestas, arbitrium IV 265, 19.
electio IV 130, 14; V 470, 58. electio uel optatio V 124, 43; 315, 22.
optatio, electio IV 372, 30. libera uoluntas aut electio IV
