201, 23; 301, 32 (abiurgat *codd.*); V 262, 15. reprobat IV 485, 17.
reprobat aut negat uel plorat (periurat *Nettleship 'Journ. of Phil.'*
XIX 113) IV 3, 22. **abiurant** abnuunt, negant V 259, 49.
**abiur[g]are** negare IV 5, 10. **abiurare** est rem creditam negare
periurio V 161, 3 (= *Serv. in Aen.* VIII 263). **abiurari** ab iure
ecclesiae abicere (*Euseb. eccl. hist.* VII 26) V 421, 3; 429, 50.
**abiurasse** intercepisse periurio IV 4, 30; V 547, 13.

**Ablacta** id est quod purgaturia similat III 616, 11. id est
purgaturia similat III 607, 8. id est purgaturia simulae (!) III 586,
10.

**Ablacto** est a lacte separo V 620, 51 (*cf. Isid.* X 11).

**Ablaqueata** διορυχθέντα II 4, 3.

**Ab latere** longe IV 471, 5; V 259, 15; 343, 39; 625, 8.

**Ablatio** ἀφαίρεσις II 252, 17. *V.* apocope, aphaeresis.

**Ablatiuus** ἀφαιρετικός III 382, 60. ἀποκομιστικός III 376, 22.
ἀφαιρετική II 252, 19.

**Ablatum** absconsum IV 202, 5 (abditum *c*, *recte*, ut uid.*).
**ablata** binoman (*AS.*, binumini *cod. Ep.* = ablata) V 341, 37.
**ablata** ἀφαιρεθέντα II 4, 8.

**Ablaua minor** *v.* chamaemela.

**Ablauo** ἀπονίπτω II 239, 25.

**Ablegatio** ἀποπομπή II 240, 4. legatio II 563, 44.

**Ablegatus** condemnatus IV 4, 47.

**Ablegmina** partes extorum quae prosegmina dicuntur *Scal.* V 589, 28.
*Cf. Festus Pauli* 21, 7.

**Ablego** ἀποπέμπομαι II 239, 42 (ablebo *cod.*). ἀποποιονμαι II 240,
3. **ablegat** ἀποπέμπει, ἀποποιεῖ II 4, 9. ἀποποιεῖ II 554, 40. abrogat
IV 301, 33. **ablegare** id est a suo loco commendare V 265, 15. a loco
ali\<e\>nare, unde et legati et legatio dicta V 649, 2 (= *Non.* 32, 1).
**ablegatur** condemnatur IV 202, 9.

**Ablicit** (ablicet *codd.*) abdicet (abducit?), abstrahit, subducit V
490, 5; 435, 23 (*om.* subducit): *contam.?*

**Abligur\<r\>igine** uoracitate V 161, 6 (*Loewe GL. N.* 163).

**Abligurrio** κατεσθίω II 345, 44. **abligurrit** degustat V 161, 5
(ableg. *codd.*); IV 476, 37 (adl.); IV 14, 19; V 165, 20 (alleg.);
342, 56 (all.). **abligur\<r\>ire** plurima consumere (*cf. Schlee
Schol. Ter.* 46), id \<est\> suspensis digitis leniter cibum tangere
(non plurima *Warren 'on lat. Gloss' 190*: *non recte*) IV 201, 42.
**abligur\<r\>ire** (-ri *codd.*) gluttire, sorbere uel deuorare V 161,
7. **abligurrierat** deuorauerat V 531, 10 (*Ter. Eum 235*). *V.*
ligurrio.

**Ab limite** ὁροθέσιον, id est terminus V 435, 24; 559, 13 (termini).
*Glossa contracta.*

**Ablumentum** κάθαρσις II 528, 45.

**Ablunda** *V.* apluda.

**Abluo** ἀπολούω II 4, 12; 238, 45. ἀποκλύζω II 237, 43. ἀπονίπτω II
239, 25. ἀποπλύνω II 239, 59. **abluit** ἀπονίπτεται, ἀπόκλύζει καὶ
απέκλυσεν II 4, 11. emundat IV 202, 8; 486, 10. mundat uel lauat IV 3,
25. expiat, purgat, mundat IV 301, 35. lauit, detersit uel lauat IV 301,
34. **abluero** lauero IV 427, 19 (*= Verg. Aen.* II 720).

**Abluta** diligenter lota IV 201, 38; 404, 8. **abluti** mundati V 547,
3.

**Ablutione** emundatione V 161, 8.

**Abluuione** inundatione sordium IV 4, 29; V 161, 9 (*de* adluuione
*vix est cur cogitemus*).

**Abmatertera** (aba mat. *codd.*) soror auiae IV 301, 4; *cf. Isid.* IX
6, 27.

**Abnatare** ἐκκολυμβῆσαι II 4, 13.

**Abnegatio** ἀπάρνησις II 233, 25. ἀποστέρησις II 240, 55.

**Abnegator** ἀπαρνητής II 233, 26. ἀποστερητης II 240, 56. negator II
563, 49.

**Abnegito** abnego saepius (abnegotio *codd.*) V 435, 28. saepius
abnego V 559, 2 (abnegatio *cod.*).

**Abnego** ἀπαρνοῦμαι II 4, 15; 233, 27. ἀποστερῶ II 240, 54. **abnegat**
plus quam negat IV 201, 43. infitiatur, plus quam negat IV 301, 36; V
435, 27.

**Abnepos** ἀπέγγονος II 533, 33; III 254, 17. filius nepotis IV 3, 13.
**abnepus** ἀπέγγονος II 233, 45. ἀπόγονος III 375, 34. filius nepotis
IV 301, 37; 474, 33. qui nascitur de pronepote IV 202, 12. filius
pronepotis, id est nepus nepotis IV 301, 38. qui natus de pronepote V
342, 53. extra nepus IV 404, 9. extraneus (!) V 435, 26.

**Abniso** *v.* abnuo.

**Abnormis** ἄρυθμος II 246, 25. innumerabilis II 564, 1.

**Abnormitas** ἀρυθμία II 246, 24. amaritudo II 563, 46 (*contam.*
abnormitas enormitas et amaritas amaritudo: *cf. Loewe GL. N.* 3).

**Abnumero** ἀπαριθμῶ II 233, 21.

**Abnuo** ἀπαρνοῦμαι II 233, 24. ἀπονεύω II 239, 23. **abnuit**
ἀνανεύει, ἀπαρνεῖται II 4, 10; 554, 41. rennuit IV 202, 10. negat uel
recusat IV 3, 19. negat uel consentit (*contam.: cf.* annuo) IV 4, 22;
301, 40. denegat, contradicit, negat, non sentit (!) IV 487, 1. negauit,
