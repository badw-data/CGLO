200, 29; 262, 15. locus ubi poma sunt IV 146, 7. ubi poma ponuntur IV
274, 7; V 474, 22 (= *Isid. Diff.* 465. *GR. L.* I 109, 28; 140, 15).
locus ubi sunt pomae (!) IV 554, 34. *V.* pomerium, malarium.

**Pomarius** ὀπωροπώλῃς II 153, 7; 386, 6. pomorum uenditor II 589, 60
(*Gallée* 361. *v. suppl.*).

**Pomas (!) iuniperi** artiotitas III 542, 16 (ἀρκευθίδας).

**Pomatium** mollis et liquidus cibus ex pomis *Scal.* V 606, 39 (*Osb.
p.* 479).

**Pomeos** Ellenorum lingua Octuber mensis dicitur V 234, 25. *Cf. C. F.
Hermann Phil.* II 265 (Ῥωμαῖος). *V.* menses.

**Pomerium** ὁ ἐντὸς ἢ ἐκτὸς τείχους κῆπος II 379, 32 (pomarium). ὁ περὶ
τὸ τεῖχος τόπος II 153, 10 (pomoerium). περὶ τὸ τεῖχος τόπος II 405, 21.
locus proximus muris (*uel* maris *uel* mari) IV 274, 8; 378, 14 (pomarium);
V 474, 23 (pomerius et muri). locus proximus mari uel muri (*scr.* muris).
ubi arbores pomorum V 576, 33 (*v.* pomarium). ipse locus arborum IV
146, 8 (pometum *Nettleship 'Journ. of Phil.'* XIX 294); 554, 35; V
234, 27. regio determinata certis signis IV 147, 8. quod est iuxta murum
IV 554, 37 (*Varro de l. l.* V 143). spatium quod circa muros est IV
554, 36; V 321, 56; 383, 8. quod est circa muros V 234, 26. spatium
circa muros V 380, 12. *V.* pomarium.

**Pometarius** ὀπωροπώλης III 371, 37.

**Pome\[n\]tum** παράδεισος II 394, 32 (*corr. e*); 501, 4. pomarium II
589, 58 (*Gallée* 362. *v. suppl.*). **pometum** παράδεισος II 153,
11.

**Pomifer** ὀπωροφόρος II 386, 7.

**Pomiferat** ὀπωροτροφεῖ II 153, 13.

**Pomonum** huius **Pomoni\[s\]** apud Latinos nihil est, sed **Pomona**
est (*om. P*) dea pomorum (*cf. Serv. in Aen.* VII 190). interdum pro
ubertate pomorum metonymicos ponitur, quod si est, haec Pomona, Pomonae,
Pomonam et ab hac Pomona dea facit, pluralem non habet *Plac.* V 93, 25
= V 135, 5. *Cf.* **Pomones** pomorum custodes *Plac.* V 93, 26 = V 135,
6.

**Pomorum custos** ὀπωροφύλαξ II 386, 8.

**Pompa** πομπή II 413, 24; III 10, 60; 84, 33; 302, 52; 372, 8.
fallacia *gloss. Werth. Gallée* 342 (*cf. suppl.*). **pompam** risionem
V 421, 54 = 430, 37 (*Euseb. eccl. hist.* V 3). **pompas** obsequia
funeris IV 146, 9 (*Verg. Aen.* V 53?); V 474, 25. ludorum imagines V
474, 24.

**Pompulentus** pompa plenus IV 146, 14; 273, 45; 378, 16; 554, 44; V
234, 28; 322, 25; 384, 5.

**Pomum** ὀπώρα II 386, 5; 504, 63; 548, 20; III 256, 2 (*cf. GR. L.* I
554, 25). **poma** ὀπώρα II 153, 9 (οπωρω *cod. corr. e*); 12; III 15,
41; 88, 4; 184, 66; 316, 5; 372, 28; 460, 27; 501, 39. ἀκρόδρυα III 256,
3 (*unde*?). ὀπώραν (-ον *cod.*) III 571, 24. *Cf.* δὸς ὀπώραν **da
poma** III 288, 59 = 659, 20; 514, 74. ἀκρόδρυον, ὀπώρα **pomum** turion
(= turio) III 428, 5. **poma** generaliter dicuntur *gloss. Werth.
Gallée* 342 (*cf. suppl.*). quod potu indigeant, id est umore\[m\]
maiore\[m\] V 651, 12 (*Non.* 61, 28). *V.* malum, pepo.

**Pomum copressi** capton (κοπτόν? capito? κῶνον?) III 581, 40. copton
**poma copresso** III 544, 38. cypressus **pomum de cupresso** III 581,
7. ciparissum **poma copressi** III 544, 66.

**Ponderator** σταθμιστὴς καὶ ζυγοστάτης II 153, 19. σταθμιστής II 436,
29. ζυγοστάτης III 74, 56 (pondus *cod.*); 142, 70.

**Ponderatorium** ζυγοστάσιον III 196, 32; 267, 58. *Cf. Funck Arch.*
VIII 383.

**Ponderatura** σταθμός II 436, 30.

**Ponderitatem** a pondere V 645, 64 (*Non.* 156, 3).

**Pondero** σταθμίζω II 436, 28. παρασταθμίζω II 396, 22 (\<p\>ondo
*cod.*). ζυγοστατῶ III 74, 55. **ponderat** σταθμίζει II 153, 18.
**pondera** στῆσον, σήκωσον II 153, 15. **ponderare** σταθμίσαι, στῆσαι
καὶ σταθμίσασθαι II 153, 27.

**Ponderositas** *v.* hernia.

**Ponderosus** grauis *gloss. Werth. Gallée* 362 (*v. suppl.*).
**ponderosum** ἐπιβαρές, βαρὺ καὶ στιβαρόν II 153, 17. grauem IV 378,
15.

**Ponderum baiulus** φορτιαφόρος III 309, 56.

**Pondo** *v.* duo et dena pondo. **pondo** libras XII (*scil. unc.*?) IV
274, 15. **pondo et pondus** σταθμός II 153, 20. *V.* dupondium.

**Pondus** ὁλκή, σταθμός II 153, 14. στάθμη, σταθμόν, ὁλκήν II 153, 26.
ὁλκή II 382, 20 (*GR. L.* I 554, 25); 506, 60; 548, 22. σταθμός II 436,
30; 502, 17; 527, 33; 544, 33. βάρος II 255, 57; III 471, 75; 490, 32.
σταθμίον II 558, 53; III 270, 4. βαρύσταθμος II 256, 3. **pondere**
grauidine V 630, 45. **pondera** σταθμία καὶ βάρη II 153, 16. σταθμία
III 204, 26; 321, 29; 366, 58; 504, 4. ἡμίλιτρον III 197, 63. *V.*
pondo, ponderator.

**Pone** \[θὲς καὶ\] ὄπιθεν καὶ ὀπίσω II 153, 21 (*v.* pono). ὄπιθεν ἐπὶ
τοῦ ὀπίσω II 385, 12. ὀπίσω II 385, 19. post V 134, 37. a posteriore
parte V 134, 36. prope, iuxta et propter (?) IV 555, 30;
