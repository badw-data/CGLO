IV 403, 1. **uolumine** longo tractu IV 580, 11. **uolumina** orbes IV
426, 38. *Cf. Isid.* VI 13, 2.

**Uoluntarie** ἑκουσίως II 291, 60.

**Uoluntarie peccantium** ἀναμαρ\<τ\>ήτων (*contam.*?) III 510, 7.

**Uoluntarius** ἑκούσιος II 291, 59. αὐθαίρετος II 211, 24; 250, 43.
ἐθελοντής II 284, 50. αὐτοπροαίρετος II 251, 51.

**Uoluntas** βούλησις II 259, 25. θέλησις III 145, 34; 466, 63. θέλημα
III 279, 38 (*unde*?). δόξα III 279, 37 (*unde*?). προαίρεσις II 416, 7.
πρόθεσις ἡ προαίρεσις II 417, 54. σκοπὸς ἡ προαίρεσις II 433, 53. mens,
θέλημα IV 426, 34. mens IV 296, 42. *V.* aduersa u.

**Uolup** uoluptuosum V 648, 68 (*Non.* 187, 6). **uolupest**
uoluptuosus est IV 195, 33 (*Ter.*); V 490, 2; 519, 43.

**Uoluptarius** ἡδύβιος III 334, 58. ἡδυπαθής III 334, 59. ἐπιθυμητικός
II 308, 20. lasciuus II 597, 36.

**Uoluptas** ἡδονή, τέρψις II 211, 27. ἡδονή II 323, 35; III 466, 64.
ἀπάτη (*ex contaminatione*?) III 291, 43 (*inter deos*) = 508, 27.
concupiscentia IV 296, 46; 426, 36. oblectatio IV 580, 12.

**Uolutabrum** κύλινδρος II 356, 36. συφεός III 258, 59 (*unde*?).
κυλίστρα ζῴων II 211, 43. **uolutabra** loca in quibus se porci uolutant
IV 296, 47 (*cf. Serv. in Georg.* III 411). lacunae in quibus iumenta
(*vel* -tae) uolutantur IV 195, 39. ubi apri porcique se uoluunt V
255, 1. *V.* caenum.

**Uolutans** cogitans IV 195, 22; 470, 31 (*Verg. Aen.* I 50?); 579,
49.

**Uolutatorium** ὑλιστήριον (εἰλ.? κυλ.?) III 306, 14.

**Uoluto** κυλίομαι II 356, 38 (uolito); III 148, 42. εἰλοῦμαι II 286, 2
(uolito). ἐνθυμοῦμαι II 299, 24. **uolutas** κυλίεσαι (!) III 148, 43.
**uolutat** κυλίει II 211, 42. κυλίεται III 148, 44 (*cf.* cylosin
**uolutat** III 148, 53: κυλίουσι uolutant?). cogitationem repetit IV
195, 30; 403, 3 (cogitationes); 579, 50; V 336, 36. **uolutant**
uoluuntur IV 470, 32 (*Verg. Aen.* I 725: *cf. Non.* 420, 25).

**Uoluentibus annis** transcurrentibus aeuis IV 470, 33 (*Verg. Aen.*
I 234). *Cf.* **uoluentibus aeuis** transcurrentibus annis IV 195, 25; V
519, 42.

**Uoluere fumum** agere nebulam uelut fumum IV 470, 34 (*Verg. Aen.*
III 206).

**Uoluo** ἑλίσσω II 211, 25. κυλίω II 211, 26; 356, 39. εἰλῶ II 286, 4.
**uoluit** refouet IV 426, 43. pectore cogitat IV 296, 44. **uoluunt**
inpellunt IV 579, 52. **uoluere** cogitare *c post* IV 195, 22; 195, 34;
579, 51. nunc[u] pati, alias cogitare IV 470, 28 (*Verg. Aen.* I 9;
22). **uoluit** traxit IV 195, 24 (*Verg. Aen* I 101).

**Uoluola** (= conuoluulus) uuidubindae (*AS.*) herba similis
hederae, quae uitibus et frugibus circumdari solet V 398, 54.

**Uomer** ὕνις II 31, 8 (bomer); 211, 28; 462, 65; 490, 35; III 23, 41;
195, 61; 204, 55 (ynyx); 299, 69; 325, 47; 357, 81; 369, 1; 466, 65;
482, 11. **uomis** et **uomer** ὕνις II 514, 19; 539, 50. **uomes** et
**uomer** II 552, 6. **uomis** uomer II 597, 37. **uomer** uomis IV 402,
62. **uomis** uomer aratri V 519, 44. **uomis** et **uomer** dicitur
*Plac.* V 103, 7. **bomer** scar (*vel* scaer, *AS.*) II 570, 20. *Cf.
GR. L.* II 249, 18; VII 544, 12. *V.* cucumis, uomis aratri.

**Uomica** ἀπόστημα II 241, 2; 525, 2. φύμα II 473, 62; 498, 26
(uimica). tumor, duritia uomitus II 597, 33. caua (concaua?) V 648, 65
(*Non.* 186, 28). **uomicae** stomachi dissolutio V 656, 30 (*luvenal.*
XIII 95).

**Uomis aratri** tofus (!) ferreus quem et dentem dicunt V 255, 2.

**Uomitina** ἔμετος II 211, 29.

**Uomitoria** ἔξοδος III 173, 15. ἔξοδοι III 240, 41.

**Uomitum** ἔμετος II 296, 5; 501, 51; III 600, 39.

**Uomo** ἐμῶ περὶ ἐμέτου II 297, 9.

**Uopiscus** qui fratre gemino abortiuo legitimus nascitur V 583, 4. qui
ex geminis in utero remanet altero aborto V 519, 45. est tarde natus uel
qui ex duobus conceptis uno abortu excluso alter ad partum legitimum
deducitur teste Marcello (*Non.* 557, 3) V 625, 8. nominatus uno aborto
filio alter qui legitime natus fuerit V 255, 3. *Cf. Isid.* IX 5, 21.

**Uorago** χάσμα II 209, 42; 211, 30; 476, 3. fantasma (χάσμα?
*contam.*?) II 525, 20. χάρυβδις II 475, 56. κατάποσις II 343, 10. χάος
III 434, 44. βόθρος II 258, 30. summersio terrae IV 403, 4. obsorsio (=
obsorptio) IV 195, 49. obsorsio et fossa profunda et terrae hiatus IV
296, 50. hool (*AS.*) V 399, 18. **uoraginem** baratrum IV 195, 50.

**Uorator** *v.* ardalio. καταπότης III 251, 56.

**Uorax** καταπότης II 343, 11. καταφαγᾶς II 344, 48. sorbens IV 195,
38; 403, 6; 580, 2; V 336, 41.

**Uoro** ῥοφῶ II 428, 51; III 255, 29. καταπίνω II 342, 61. **uorat**
καταπίνει II 211, 31. gluttit, sorbit IV 403, 5. ingluttit (*vel*
gluttit) IV 195, 40. gluttit
