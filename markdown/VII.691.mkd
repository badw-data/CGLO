**a** - *cf. etiam* an -, on -, ond-, un-.

**a: a fordh** (E, a forthe A *minus recte*) in dies crudesceret. *Cf.*
gisettan *s. v.* gisettae.

**aac** color (*error videtur*); robor (= robur).

**aad** rogus. *Cf.* beel vel aad.

**aastc** *v.* aesc.

**achlocadum** effossis *s. v.* effossa.

**acuaerna vel sciron** disperdulus. *Cf.* aqueorna.

**adesa** *cf.* aetsa, etsa.

**adexa *et* adexe** (*illud L habet* V, 418, 66, *non* adexe)
lacerta.

**aduinendanan, afulodan, asuundnan** tabida et putrefacta. *Cf.* afulat
ond asuunden.

**aebitu** *v.* aelbitu.

**aebordrotae** colicum, **eborthrotae** scasa (?).

**aec þan** (E, aec don A) quin etiam.

**aecsa** *v.* etsa.

**aectath** (E, aechtath A) perpendit *s. v.* perpendo.

**aedilra** gregariorum *s. v.* gregarius.

**aegan** *v.* unþyotgi egan.

**aegergelu** fitilium (= vitellum).

**aeggimang** (E, -mong A) ogastrum (*vocem celticam agnoscit
Schlutter*).

**aegnan, aegrihan** *v.* aehrian.

**aegur** (A, egur C) dodrans.

**aehnan** *v.* aehrian.

**aehrian** (E, aegrihan A, *i. e.* aehnan *vel* aegnan) quisquiliae.

**aelbitu** (E, aebitu A) olor.

**aelding** dilatio.

**aeldrum** *v.* bituicn aeldrum.

**aelifnae** alumnis E *post* V, 343, 3 (*deest in Thes. s. v.* alumnus,
*ad* alumen *alii referunt obloquende Schluttero in Anglia* N. F. XIV,
297).

**aemil** curculio; gurgulio *s. v.* curculio.

**aend suilcae** (E, end suilce A) atque.

**aengiþinga** (E, -dinga A) quoque (A, quoquo E) modo.

**aenid** (E, aenit A) aneta *s. v.* anas 1.

**aeohed** eviscerata *s. v.* evisceratus (athed *Gl. Corp. Chr.*)

**aerngeup** *v.* earngeat.

**aeruuica** *v.* earuuigga.

**aesc** (E, aastc A) fraxinus, **aesc** (E, esc A) praxinus *s. v.*
fraxinus, **aesc** cerciclus *s. v.* cercurus.

**aesc** (= aex) *v.* braedlaestu aesc.

**aesctbrotae** (E, -drotae A) ferola *s. v.* ferula.

**aesil** *v.* haesl.

**aespae** *v.* espae.

**aetgaeru** (E, aetgaru A) framea.

**aethm** *v.* ethm.

**aetrinan** *v.* tha uuannan aetrinan *s. v.* uuannā.

**aetsa** (= adesa) dolatorium, **etsa** (*et* aecsa) ascia.

**aex** axis. *Cf.* braedlaestu aesc.

**aferidae** *v.* an uueg aferidae *s. v.* an.

**afigaen** (E, afigen A) frixum.

**a fordh** *v.* a.

**afulat ond asuunden** (A, aduinendanan, afulodan, asuundnan E) tabida
et putrefacta.

**afyred olbenda** (C, afyrid obbenda A) dromidus.

**a gisettan** *cf.* gisettae.

**agnaettae** (E, agnetae A) usurpavit *s. v.* usurpo.

**agnidinne** (A, -dine C) de triturigine (Oros. VII, 25, 10 detrita
rubigine *con­fert Schlutter. v. p.* 437).

**alaer** (E, aler A) alnus.

**aldot** alveolum.

**aldur** dictatorem s. *v.* dictator.

**aler** (A, alaer E) alnus.

**alerholt** (A, alterholt E) alneta.

**algiuueorc** (E, algiuerc A) ignarium *s. v.* igniarius.

**alterholt** *v.* alerholt.

**am** *v.* haam.

**ambaer** (E, ombar A) urna, **ainbaer** (E, enƀ A) situla, **ambras**
cados *s. v.* cadus.

**ambect** (E, ambaet A) rationator (rationato *codd., unde alii*
rationatio), **ambechtae** collatio.

**ambras** *v.* ambaer.

**amprae** (E, omprae A) varix.

**an** - *cf. etiam* a -, and-, on -, ond-, un-.

**an: an landae** (A, on laste *Gl. Corp. Chr. m. 1*) e vestigio, **an
uueg aferidae** (E, anuoega ueridae A) avehit *s. v.* aveho, **an ba
halbae** (E, on ba halbe A) altrinsecus.
