**ὑφελκύζω** *v.* ὑφέλκω.

**\*ὑφελκυσμός** reductio, subtractio.

**ὑφέλκω** *et* **ὑφελκύζω** decedo (*et* discedo), subduco, subtraho.

**ὑφέν** *v.* ὑπό.

**ὑφεξαιρ-** *v.* ὑφεπαιρ-.

**ὑφέρπω** subrepo.

**ὑφή** tenor, textura, textus, - τοῦ πράγματος liniamentum (-a causae).

**ὑφηγεῖται** praesipit (-itur).

**ὑφίημι:** ὑφειμένος nixus. *Cf.* ὑφειμένως.

**ὑφίσταμαι** subsisto, ὑποσταίη exsistit (exstiterit), τὸ μὴ ὑφεστός υ.
ἀνυπόστατος. *Cf.* ὑποστατέα.

**ὑφορῶμαι** vereor.

**ὕφος** contextus, suggestum, textum, textura. *Cf.* astantus.

**ὕφυγρος** *et* **ὑπόϋγρος** subhumidus.

**ὑψηλοπετής** illustris, praepes.

**ὑψηλός** acclivis, aeria (-ή), altus, amplus, arduus, celsus,
excellens, excelsus, sublimis, summus, -ότατος amplificus, excelsus,
praecelsus. *Cf.* μετέωρος, ὄρος, τόπος.

**ὑψηλότης** eminentia, excelsitas.

**ὑψιπετής** sublimis.

**ὕψιστος** altus (altissimus), amplus, arduus, celsus, excelsus,
summus, superus, θεοὶ -οι dii summi, Ὑψίστη Suadala.

**ὑψόθεν:** - ἀποβλέπω despicio.

**ὕψος** altitudo, amplitudo, suggestum, τὸ μεταξὺ τῶν αὐλάκων - porca
2.

**ὑψῶ** alto, amplio (amplo), exalto, extollo, sublimo, -οῦμαι exorior,
-ωθείς elatus.

**ὕψωσις** exaltatio.

**ὕω** pluo.

**φάβα** faba, faberrae, φάβατος κόκκος fabatrum.

**φαγεδαινικός** *cf.* fagedinicus (*expl.* -ὄρεξις, βούλιμος). *Cf.*
podex.

**φαγεῖν** *v.* ἐσθίω.

**\*φαγνος** salvia.

**\*φαγολοίδορος** *cf.* fagolidoros (*expl.* manducans senecias vel
sinancias; manducans maledicta vel quae non decent).

**φαγός** edax.

**\*φαγος** *cf.* phagos (*expl.* cibus).

**φάγρος** barbulus, dentatus (- ὁ ἰχθύς), dentex, rubellius.

**φάγω** *cf.* phago (*expl.* manduco).

**φαιδρός** faustus, festus, laetus.

**φαιδρότης** festivitas.

**φαιλόνη** *v.* φαινόλης.

**φαινόλης** (πενόλης, φελόνης, φαινόλη, φαιλόνη, φελόνη *sim.*) lacerna,
paenula (*ibid.* τὸ καλούμενον φελόνιον), λευκὴν -ην albam paenulam.

**φαίνω** alluceo, declaro, luceo, pando, pareo, -ομαι, appareo, consto,
dinosco (-or), exsisto, pando (-or), pareo, perspicio, polleo, video
(-or), φανείς pollens, αἰτίας φανείσης causa cognita.

**φαιός** aquilus, -όν aeguptium, pulla (-um), -ὸν τὸ μέλαν pulla.

**\*φακαρίς** fulica. *Cf* φαλακρίς.

**φακῆ** lens, lenticula. *Cf.* χιτών.

**φακός** lens, lenticula, lentigo, - προσώπου naevus.

**\*φάκοψις** lentiginosus, sparso ore.

**φακώδης** lentiginosus.

**φαλάγγιον** *cf.* sphalangius *sim.* (*expl.* musca venenosa).

**φάλαγξ** acies, legio. *Cf.* coniveo (cohors), phalanga (*expl.*
fustis super cui aliquid ligatur), palangae (*expl.* fustes).

**φαλάκρα** calvaria (*et* -um).

**φαλακρός** calvus.

**φαλακροῦμαι** decalvo.

**φαλάκρωμα** calvitium.

**φάλαρα** phalera.

**φαλαρίς** querquedula (φυληρις), *cf.* phalaris (*expl.*
λευκομέτωπος). *Cf.* φακαρίς, φυληρίς.

**φαλλός** habus.

**φαλός** *cf.* habus.

**\*φάναξ** (*Eins.*) lanterna.

**\*φανεροποίησις** declaratio, manifestatio.

**φανεροποιῶ:** -ῆσαι certiorare.

**φανερός** apertus, certus, clarus, evidens, manifestus, perspicuus,
-όν ἐστι consto (constat), pateo (patet).

**φανερῶ** claresco, declaro, illustro, intimo, manifesto, pando,
patefacio, -οῦμαι claresco, illuceo (-esco), -ωθείς *cet.* designatus,
expressum, πεφανερωμένον patefactum.

**φανερῶς** aperte, clare, coram, disceptim, evidenter, palam (*cf. p.*
438), proterve, providenter.

**φανέρωσις** declaratio, insinuatio, manifestatio, ventilatio.

**φανός** candela, lanterna (*et* -όν?). *Cf.* fanum.

**φάνσις** crepusculum.

**φαντάζομαι** imaginor.

**φαντασία** imago, phantasia (*expl. etiam* ostentum, ostensio praeter
consuetudinem), vana visio, vanitas, visibilitas, visio.
