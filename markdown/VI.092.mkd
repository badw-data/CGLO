Vergilius (*Georg.* I 110): scatebris querens quam temperat arua V 168,
11.

**Areo** ξηραίνομαι II 378, 8. **aret** ξηραίνεται II 24, 34. ab
ariditate IV 20, 40. **arebant** siccabantur IV 477, 30; V 168, 10.
siccabant V 168, 9. *Cf.* **arfet** animi intercelat (intus calet *H.*)
uel siccum est IV 406, 15 (ardet *et* aret?).

**Areola** ab area diminutiue II 567, 29. **areda** canale V 491, 49.
**arecla** est riga (= striga) \<h\>ortorum V 615, 15. **areolae**
πρασιαί (πρασεια *cod.*) III 430, 16. id est collectiones aquarum quae
in hortis deriuantur pro utilitate olerum *a post* IV 4, 18. **areoli**
(*scr.* areolae) **aromatum** horti V 338, 29 (*Cant. cant.* V 13).
sceabas (*AS. = Getreidebündel*) V 339, 15.

**Areopagita** (*vel* ariop.) ἀρεοπαγίτης II 244, 26. curialis IV 208,
6. graece, locus in Athenis V 268, 12. locus Martis Athenis V 346, 20.
archisynagogus V 338, 2; V 420, 3 (amopaga: *cf. Euseb. eccl. hist.* III
4); 428, 63 (*item Euseb.*). **areopagitae** curiales IV 20, 14
(-tes); 480, 14; V 168, 19. *Cf.* **ariopagita** ara paganorum (!) V
168, 18.

**Areopagus** nomen curiae V 342, 8 (*cf.* astismos). curiae aput
Athenienses nomen est, quae a Marte nomen accepit V 168, 20 (= *Eucher.
instr. p.* 152, 20). *V.* areopagita.

**Areos** (= Areios) Bithyniensium lingua Iulius mensis dicitur V
168, 12. *Cf. Ideler* I 421.

**Arepennis** *v.* iugerum.

**Aresco** ξηραίνομαι II 378, 8. **arescit** ξηραίνεται II 24, 44.
siccat V 441, 10 (*vel* seccat); 491, 52; 561, 37.

**Arete[n]** uirtus V 552, 30.

**Arethusa** nympha Syracusae (!) IV 406, 16. fons Siciliae V 268, 8.
una ex nymfis IV 430, 27 (*Verg. Aen.* III 696).

**Aretillum** ἀμπελόπρασον II 24, 33 (αρωπελο πρασον *cod. corr. chg*;
ἄρον et ἀμπελόπρ. *d*); III 88, 54; 186, 17; 266, 14; 317, 48; 535, 35;
543, 14; 549, 43. porrus per se (*vel* pro se: πράσον?) in campo uel in
uinea (*ubi* crescens *pro* per se, *nisi id in fine potius
interciderit, Loewe GL. N.* 17) II 568, 5. μάραθρον III 16, 32
(*errore*). *Huc spectat* scordio\<n\> id est **orotillo** III 575, 52.
*Aliquoties* **arotillum** *invenitur* (*cf.* III 535, 35; 549, 43).

**Arferia** uas uinarium cum quo uinum ad aras ferebant IV 208, 11. uasa
cum quibus uinum ferebant ad aras V 491, 55. uas uinarium quod uinum ad
ara\<s\> fert V 441, 29. *Cf. Festus Pauli p.* 11, 14; *Loewe Prodr* 13.
*Adde* **adferial** ὕδωρ τὸ τοῖς νεκροῖς σπενδόμενον II 462, 26. aqua
qua mortuus lauatur II 564, 48 (quae mortuis libatur *Loewe Prodr.*
127). *Cf. Nettleship 'Contr.' p.* 270.

**Arfet** *v.* areo.

**Argata** ualtae (*AS.*) V 268, 30 (*cf. Diefenbach nov. gl. p.* 33;
*Diez* I árgano. artata uincta uel ancta *Hildebrand p.* 20). *V.*
ergata.

**Arg\<e\>i** simulacra IV 207, 49. *V. Festus Pauli p.* 15, 12; 19,
10. *Cf.* **argiarra** ἀφιδρύματα II 19, 15 (*ubi* argei *dg*, arae g);
*Dammann Comm. Ien.* V 44; *O. Mueller ad Festum Pauli* 19, 10.

**Argemonia** (*pro qua forma plerumque* agrimonia *legitur*) *in his
glossis exstat:* accella (= arsella *Pseudapul. c* XXXII, *ubi* aptela
uel acela *ed. Rom.*) III 549, 48. accela III 535, 43. abcella III 552,
48. sarcola (= sarcocolla *Pseudapul.*) id est eopaturion (= εὐπατώριον)
id est radicis **agrimonia** III 629, 7. **agrimonia** id est sarcocula
III 580, 19. sarcola id est **amomia** III 629, 17. *Cf.* sarcocolla
eupaturium III 595, 15. eopaturio radicis **agrimina** III 632, 14.
eopaturio id est radix **agrimoniae** III 623, 21. ipaturio radicis
**argimoniae** III 632, 24. ipaturium radices **argemonio** III 547/7.
hispaturio id est radix **agrimoniae** III 583, 33. eupaturio radix
**agrimoniae** III 589, 58. **agrimonia** concordialis III 557, 27; 621,
46. libornia (*Arch.* XI 110) III 567, 48. bebalis (?) maior III 553,
74. berbahs maior III 618, 24. raminalis III 575, 16. dometrix galica (?)
III 559, 57. rucilia (?) III 575, 17. *V.* lappa inuersa, aristolochia.
*Cf. von Fischer-Benzon p.* 77.

**Argentarium** ἀργυρωματοφυλάκιον III 313, 1.

**Argentarius** ἀργυροφύλαξ II 244, 14. κομάκτωρ (μ *del. d*) II 19, 22.
ἀργυροκόπος II 19, 18; 244, 9; III 201, 12; 271, 17; 307, 2; 366, 74;
489, 39; 508, 75. *V.* faber arg., creta arg., coactor.

**Argenteclum** (?) est argenti theca V 616, 9 (*cf.* argentarium,
argentithecium?).

**Argenteus** ἀργυροῦς II 244, 12. albus IV 406, 17. **argentea** ἀργυρᾶ
III 202, 43; 203, 16; 274, 26. ἀργύρεα III 367, 71. **argenteae**
ἀργύρεα III 370, 57. **argenteos** ἀργυροῦς III 93, 51. *V.* spuma
argentea.

**Argenti \<bi\>gati** ubi erat biga caelata V 561, 38.

**Argentifodina** *v.* Scaptensulas.

**Argenti susceptor** ἀργυρο\<υ\>ποδέκτης II 244, 13.

**Argentilla** *v.* apiata.

**Argentum** ἄργυρος II 499, 46; 525, 23; 534, 21; III 22, 40; 434, 47.
ἄργυρος hoc **argentum** pluraliter non declinabitur
