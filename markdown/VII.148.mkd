**Prorogator** δαπανητής III 334, 22. *V.* praerogator; *cf. schol.
Pers.* II 59.

**Prorogatum** prolongatum IV 147, 17 (porrogatum); *a c post* IV 149,
28 (*item*); IV 556, 54. praefixum IV 381, 45. *V.* praerogatum.

**Pro rostris** publice IV 557, 32; V 476, 58 (prostris *cod. corr. a*).
*Cf. Festus Pauli p.* 226, 3. *V.* pro nostra.

**Prorsus** παντάπασιν II 393, 37. παντελῶς II 393, 40. καθάπαξ II 334,
32. καθ' ὅλου II 335, 30. τὸ σύνολον II 457, 40. omnino IV 148, 21; *a
post* 151, 13; 381, 46; V 138, 16. specialiter, certe IV 275, 44. uere,
specialiter, certe IV 556, 60. plane, etiam aut procul dubio IV 149, 32;
556, 59 (-um). plane, procul dubio, uere (*can. apost.* 17) V 411, 29.
sine dubio uel omnino IV 150, 5 (*Ter. Andr.* 435 etc.). immo uel tofus
siue ultra V 324, 14. **prosus** longa (-e?) V 577, 1.

**Prorsus nequaquam** οὐδὲ ὅλως II 389, 7 (*cf.* οὐδέ ὄντως \*\*\* ram
II 389, 8: '*nisi est* οὐ δεόντως perperam' *Buech.*).

**Prorumpo** ἀπορηγνύω II 240, 15. **prorumpi\<t\>** κυπρίζει III 429,
33. **prorupit** (*uel* prorumpit) floruit IV 149, 30; 556, 58 (*ubi
non* recte proruit *Nettleship* 'Journ. *of Phil.'* XIX 294).
**prorupisse** festinus egredi IV 151, 22 (*v.* prouersa).

**Prorup\[t\]ia** ἀπόκρημνα II 162, 40 (corr. *g*: *cf. Roensch Coll.
phil.* 153).

**Proruptum** effuse fluens V 556, 31 (*cf. Serv. in Aen.* I 246).

**Proruptus** ἀπόρρηξις II 240, 23.

**Prorusu lembo** prorusu insula \[proprium lembo, a quo ibi faciunt
illa uestimenta\] V 416, 53 (*scr.* pro rufo limbo pro rufa infula *coll.
Hieron. in Matth.* 27, 27: pro rufo limbo). *V.* russo limbo.

**Proruunt** cadunt IV 148, 13. **praeruunt** procurrunt V 538, 64 (*Ter.
Eun.* 599). **proruere** expellere IV 149, 29; 556, 57. *V.* probruunt.

**Prosa** καταλογάδην II 341, 58. uerba sine metro IV 275, 52. oratorum
(*uel* oratorium) dicta V 324, 63; 383, 20. communis uel pedestris
locutio V 381, 50. praefatio V 379, 26. est producta oratio: prosum
antiqui productum et rectum dicebant, unde Varro 'prosis lectis' posuit
V 576, 62 (*cf. Isid.* I 37, 1). **prosum** antiqui dicebant productum
et solutum V 511, 36. productum V 237, 22. rectum V 237, 23.

**Prosadis** (Pleiades?) stellae IV 557, 43.

**Prosaltor** ὁ ἐν τοῖς ἱεροῖς προορχούμενος II 162, 41.

**Prosa oratio** πεζὸς λόγος II 400, 26; 495, 25 (ratio); III 375, 70;
461, 32; 478, 16.

**Prosa pexa tunica** πεξὸν ἱμάτιον II 162, 43 (*cf. Salmas. ad Solin.
p.* 391): ubi prosa *delet Dammann Comm. Ien.* V *p.* 25 *ad Hor. Epi.*
I 1, 95 *referens.*

**Prosapies** γένους καταγωγή II 162, 42. **prosapia** ἐκ προγόνων
εὐγένεια καὶ συγγένεια II 162, 44. ἡ ἐκ προγόνων εὐγένεια II 323, 47. ἐκ
προγόνων εὐγένεια II 292, 27; III 253, 68 (*GR. L.* I 548, 9). εὐγένεια
(singulariter tantum declinabitur) II 316, 23. εὐγένεια III 275, 15.
διάδοχοι γένους III 303, 14 (*unde*?). origo, generatio IV 275, 40.
genus uel progenies IV 149, 38. progenies siue propago *a post* IV
151, 13; V 576, 61. progenies siue propagando (?) ut rustici V 476, 60
(prosagia *cod.*). genus, origo, progenies, stirps, radix IV 381, 47.
genus uel progenies aut origo IV 556, 12. generis origo V 138, 21. genus
V 237, 10. progenies V 237, 11; 419, 43 = 428, 24 (*Euseb. eccl. hist.*
I 1). prosaga futurorum uel genus *gloss. Werth. Gallée* 343 (*v.*
*suppl. cf.* praesaga). **prosapiam** originem V 325, 22. *Cf. Festus
Pauli p.* 225, 14.

**Prosator** genitor IV 557, 37; V 380, 6. \[praenuptus diuisus\]
genitor V 324, 18 (praeruptus).

**Prosatrix** genitrix *Scal.* V 608, 44.

**Prosatus** progenitus V 237, 12.

**Proscariose** (*ex* πρὸς χάριν *nescio a quo fictum*) affabiliter uel
iucunde: cari enim graece gratia dicitur V 324, 61. effabiliter (!)
ueliocunde IV 460, 13 (*gl. Verg.*?).

**Proscenium** tabulae ubi mimi aguntur V 476, 62; 511, 37. tabulae ubi
mimi IV 420, 46. **protinium** theatrum V 477, 4; 511, 43 (*em. Hagen
Grad. ad cr.* 64). **proscenium** quod ante scenam, id est in theatro,
unde exeunt striones V 324, 62. **proscenia** pars theatri V 381, 15.
pulpita V 556, 30 (*Serv. in Georg.* II 381; *GR. L.* VII 104, 10).
theatralia. Virgilius (*Georg.* II 381): et ueteres ineunt proscenia
ludi V 138, 18; 237, 13. *V.* stega.

**Proscholium** *cf.* **in proscolio** ἐν τῷ προσχολίῳ III 380, 66. *Cf.
Not. Tir.* 101, 8, *W. Heraeus 'Spr. des Petr.'* 23.

**Proscindo** προσχίζω II 423, 45.

**Proscribendo** damnando V 421, 12 = 429, 56 (-da -da *cod. cf. Euseb.
eccl. hist.* VIII 17).

**Proscribo** προγράφω III 461, 33; 486, 71. δημεύω II 269, 24. ταμιεύω
II 451, 27. **proscripsit** infiscauit, titulum posuit V 325, 25.
**proscribantur** porro uel palam scribantur V 411, 28 (*can. conc.
Antioch.* 24?). *V.* perscribo.
