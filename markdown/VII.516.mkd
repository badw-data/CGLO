**ἐνοχλῶ** convicior, inquieto, molesto (*et* -or).

**\*ἐνοχοποιόν** obligatorium.

**ἔνοχος** obligatus, obnoxius, reus. *Cf.* superobligatus, χάλαζα.

**ἐνράπτω** insuo, ἐνερραμμένος inserticius, insutum.

**ἐνράσσω** *cf.* illido.

**\*ἔνρειθρον** endoriguum.

**ἐνρήγνυμι** *vel* **ἐνρήσσω** irrumpo, ἐνραγείς elisus.

**\*ἔνρηξις** illisio.

**ἐνρήσσω** *v.* ἐνρήγνυμι.

**ἐνσείω** incutio.

**\*ἔνσηστρον** cerniculum.

**ἔνσπονδος** foederatus.

**ἔνστασις** instantia, intentio.

**\*ἐνστοιβάζει** instipat.

**ἐνστρέφω** intorqueo, ἐνεστραμμένον intortum.

**ἐνστύφω** imbuo, ἐνεστυμμένος infectus.

**ἐνσχίζω** infindo.

**\*ἐνσχισμοί** insectiones.

**ἔνταλμα** mandatum.

**\*ἐντάξιμος** inserticius.

**ἔντασις** ὀμμάτων obtutus.

**ἐντάσσω** indo, insero.

**ἐντατικόν** magisnon. *Cf.* entaticum (*expl.* quae humanum sperma
diregunt).

**ἐντάττω** *v.* ἐντάσσω.

**ἐνταῦθα** hic 1. *et* 2., huc, istuc.

**ἐνταφιάζω** funero, pollingo.

**ἐνταφιαστής** libitinarius, pollinctor.

**ἐντάφιον** libitina.

**ἐνταφιοπώλης** libitinarius.

**ἐντελής** perfectus, plenus.

**ἐντέλλομαι** mando 1., -όμενος mandator.

**ἐντέμνω** incido.

**ἐντεροκήλη** ramex *sim.*

**\*ἐντεροκηλήτης** ramicosus.

**ἐντεροκηλικός** ruptura (intrinsecus rupturas).

**ἔντερον** chorda, γῆς - lumbricus terrae,
-α burbalia, iecur (iocinera), interanea, intestinum (-a, stentinae
*sim.*), -α λεπτά (τῶν ἐρίφων) lactes.

**ἐντεῦθεν** exin, hinc, indidem, istinc, - ἤδη iam inde, iam nunc, -
ἤδη λοιπόν iam iamque.

**ἐντευκτής** interpellator.

**ἔντευξις** aditio, aditus, conventus, interpellatio, postulatio.

**ἐντεχνής** affaber.

**ἐντίθημι** impono, indo, insero, interpono, ἐντεθῇ inditum (-a),
ἐνθείς indens, ἐντεθειμένον appositicium. *Cf.* ἔγκειμαι.

**ἔντιμος** gravis, honestus, honoratus, honorificus, praeclarus,
praetextatus, - κατὰ στρατιωτικοὺς καμάτους emeritus.

**ἐντίμως** honorifice.

**ἐντινάσσω** incutio.

**ἔντοκον** δάνειον fenus.

**ἐντολεύς** procurator.

**ἐντολή** commissum, mandatum.

**ἔντομα** inferiae.

**ἔντονος** intentus.

**ἐντόπιος** gentilis, II, 300, 44 *sine interpr.*

**ἐντός** cis, indu, infra, intra, intro 1., intus, νάρθηκος τὸ - hilum.
*Cf.* εἰσέρχομαι.

**ἐντρέπομαι** revereor, venero (-or), verecundor, vereor.

**ἐντρέχεια** experientia, instantia, sollertia, strenuitas, κατὰ τὴν
ἰδίαν *-*αν pro sua scientia.

**ἐντρεχής** navus, sollers, strenuus, vegetis (-tus, *ubi lege* ἐντρ.
*pro* εὐτρ.).

**ἐντρεχῶς** citate, gnare.

**ἔντριτος** sequester.

**ἔντριχος** pilosus.

**ἐντροπή** elementicium, respectus, reverentia, verecundia.

**ἐντρυφῶ** luxurior, sulto.

**ἐντύβι(ο)ν** intubus.

**ἔντυβον** indivia, intubus *sim. Cf.* ἴντ.

**ἐντυγχάνω** interpello, -ων adiens, ἐντευχθείς aditus.

**\*ἐντυπή** ἄρτου exagies.

**ἐντυπῶ:** ἐντετυπωμένος informatus.

**\*ἐντυρίτης** libus, sibitillus.

**ἐντυχία** interpellatio.

**Ἐνυάλιος** Quirinus.

**ἔνυγρος** madidus.

**ἔνυδρις** *cf.* enhydris (*expl.* colubra in aqua vivens).

**ἔνυδρος** aquatilis.

**ἐνυπνιάζομαι** somnio, -όμενος somniator.

**ἐνύπνιον** insomnium, somnium, visio, visum, visus 1.

**ἐνυφαίνω** contexo.

**Ἐνυώ** Bellona.

**ἑνῶ:** ἑνοῦμαι unit (unior), ἥνωμαι haereo, ἡνωμένος· continuatus,
haerens.

**ἐνώπιον** coram, in conspectum.

**ἐνωρίτερον** *et* **ἐνωρότερον** temperi (-ius).

**ἑνωσις** coadunatio, unitas, - σιδήρου ἐκτὸς μολίβδου ferruminatio, -
σιδήρου διὰ μολίβδου plumbatura.

**ἐνώτιον** auris, inauris.

**ἐξ** *v.* ἐκ.

**ἕξ** sex, χαλκοῖ - sesquas.

**ἐξαγγέλλω** enuntio, pronuntio.

**ἐξαγιάζω** examino, perpendo.

**ἐξάγιον** pensatio.

**ἐξαγοράζω** redimo.

**\*ἐξαγορασία** redemptio.

**ἐξαγορεύω** enuntio. *Cf.* ἔξεῖπον.

**ἐξαγριῶ** effero 2., exaspero.
