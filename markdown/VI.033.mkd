**Aeneator** σαλπικτής II 12, 3. tubarum factor II 565, 22 (*ubi* cantor
*Hildebrand p.* 5. *male versum*). **aenatores** κυμβαλοκροῦσται II
12, 4. **aeneatores** tubicines IV 11, 47; 12, 3; 204, 13; V 163, 48;
262, 48; 266, 47; 338, 42. **aenatores** cornicines IV 12, 11.
**aeneatores** cornic\<in\>es, liticines, tubicines IV 475, 33.
corni\<cin\>es, liticines V 262, 33. cornicines, liticines, id est corno
(cornu *de*) uel calamo canentes IV 306, 1; V 437, 46 (cornu). corno
(cornu *ab*) uel calamo cantantes IV 204, 18. *Cf. Festus Pauli* 20, 7.

**Aeneum** χάλκειον II 474, 56. χαλκοῦν II 475, 6. aereum IV 204, 24;
306, 2; V 262 31. aereum, tissum (nasum?) IV 12, 8. **aenea** χάλκεα III
325, 2. χαλκᾶ II 474, 54. **aeneis** aereis IV 474, 15. **aeneum** λέβης
III 368, 26. *V.* aenum.

**Aenigma** obscura intellegentia IV 63, 23; 511, 41. obscura parabola
*a* IV 63, 11. obscura pars (parabola?) V 357, 70. obscuriloquium IV
410, 22; V 598, 46. similitudo V 359, 16. similitudo uel obscuriloquium
IV 336, 8. figura siue typus uel species V 590, 36. **aenigmate**
similitudo V 289, 50. **aenigmata** similitudo uel imago IV 63, 2.
similitudines aut imagines IV 511, 42. *Cf. Isid.* I 37, 26.

**Aenigmaneum** genus masculinum *Plac.* V 64, 18. *V.* poemaneum. *Cf.
temptamenta Stowasseri Arch.* II 608.

**Aenigmatista** est qui figuraliter loquitur V 618, 52.

**Aenis foribus** aereis ianuis IV 428, 21 (*Verg. Aen.* I 449). *Cf.*
**aenis for[t]ibus** ut Virgilius: uinctus (uictus *cod.*) aenis Pos
tergum nodis (*Aen.* I 295) V 560, 22.

**Aenobarbus** flaua barba uel dura V 339, 34; 439, 53. flaua barba V
491, 29. **aenobarbius (?)** flaua barba uel uaria IV 405, 9. *Cf. Loewe
Prodr.* 392.

**Aenulum** (enulum) caldarium *Scal.* V 597, 50. *Cf. Festus Pauli p.*
28, 4; *Osb. p.* 192. **enunum** cetil (*AS.*) V 357, 33.

**Aenum** aeneum V 560, 19. caldariam dicimus, quia de aere est
*Papias.* **aenus** λέβης III 498, 75. **aena** λέβης II 12, 2. **aeni**
Vergilius: e foliis undam trepidi despumat aeni (*Georg.* I 296) V 163,
50. **aeni (?)** ollas quas [c]aulas dicimus V 163, 49. **aena** uasa
aerea V 163, 44. ollas aeneas V 163, 45. **aenis** aeneis IV 11, 38.
*V.* ab aenis.

**Aeolia** insula in ora Siciliae IV 428, 22 (*Verg. Aen.* X 38).
patria uentorum IV 476, 20.

**Aeolus** rex uentorum IV 12, 7; 232, 23; 476, 21; V 263, 10; 290, 13.
**Aeorus** uentus, quem regem uentorum appellant IV 11, 50.

**Aeolus** uarius II 62, 10.

**Aeonas** saecula IV 233, 32.

**Aequabilis** εὐώμαλος II 12, 8. δίκαιος II 277, 23. **aequabiles**
aequales *Plac.* V 6, 15 = V 45, 13.

**Aequabiliter** δικαίως II 277, 27. aequaliter IV 306, 4; V 437, 48.

**Aequa dies** ἰσημερία II 333, 12; III 242, 48.

**Aequaeuus** ἰσόχρονος II 12, 13; 333, 24. eiusdem aetatis IV 12, 26; V
262, 32. unius aetatis IV 204, 15; 12, 34. **aequaeuum** unius aetatis
IV 474, 7. **aequaeui** coëtanei IV 11, 28; 474, 11; V 262, 47.
coëtanei, id est eiusdem aetatis IV 306, 5.

**Aequa lance** ἴσῳ ζυγῷ II 333, 51. aequali diuisione, conpensatio V
437, 50. simili diuisione IV 203, 50. aequo pondere, aequa lance V 194,
8. aequa rectitudine, aequa temperatione V 194, 9.

**Aequa libra** *v.* sub aequilibra.

**Aequali ligno** aequali robore IV 428, 24. *V.* cauo robore.

**Aequalis** χῶρος (χορος *cod.* aqualis *adscr. m. rec.*) III 11, 26.
χῶρος III 354, 27 (*in capite de ventis hic et illic*).

**Aequalis** ἴσος II 333, 21; III 5, 5; 177, 32; 372, 54; 447, 13; 467,
13. πάρισος II 398, 41. ὁμῆλιξ II 383, 2. συνηλικιώτης, φιλάργυρος (!)
II 12, 6. δίκαιος II 277, 23. ἰσότιμος III 147, 19; 399, 25. unius
aetatis IV 12, 27. unius aetatis, formae uel meriti IV 474, 3.
**aequalem** ἴσον III 399, 24. ἰσότιμον III 447, 14.

**Aequalitas** ἰσότης II 333, 22; III 439, 5. ἰσοτιμία III 147, 20.
ὁμαλότης II 382, 55. ὁμοιότης ἡ ἰσότης II 383, 18.

**Aequaliter** ἴσως, ὅ ἐστιν ἐφ' ἴσης II 333, 52.

**Aequamentum** ἰσότης II 333, 22; 502, 39; 528, 56. aequalitas II 565,
6.

**Aequanimitas** εὐθυμία II 317, 23. fauor uel aequitas V 530, 35 (=
*Ter. Ad.* 24).

**Aequanimo** προθύμως II 12, 5. *V.* aequo a.

**Aequanimus** εὔθυμος II 317, 24. εὔψυχος II 320, 44.

**Aequargentus** am IV 203, 48 (*del. m. 1 postea.* aequator argenti
*H.*).

**Aequatio** συγκεφαλαίωσις II 12, 17. ἴσωσις II 333, 54.

**Aeque** ὁμοίως II 383, 22. ἴσως, ὅ ἐστιν ἐφ' ἴσης II 333, 52. δικαίως
II 277, 27. iuste IV 203, 49; 476, 51 (iusti). similiter IV 11, 44; V
530, 30 (= Ter. *Andr.* 702). similiter. Lucanus (VII 17): uenerabilis
aeque V 164, 10.
