IV 477, 20. a patria exulare V 265, 27; 625, 44. **amendabit** abscondit
V 166, 4. *V.* amando.

**Amens** ἔκφρων II 293, 49; III 334, 40; 519, 60. ἄνους II 228, 36.
ἀχανής III 129, 12; 251, 50. ἀχανής, τολμηρός III 179, 24. ἀπόπληκτος
III 333, 65. qui mentem non habet IV 16, 33. qui mentem non habet sanum
(!) V 265, 19. stupidus, lentus, segnis IV 523, 41. *Cf.* **habens**
ahamens, stupidus IV 523, 42 (hebes *H.*). **amentium** sine mente,
insanorum V 529, 12 (= *Ter. Andr.* 218). *V.* infrunitus.

**Amentauit** iaculatus est V 439, 23. *Cf. Loewe Prodr.* 370. *V.*
coniecit iaculum.

**Amentia** ἀφροσύνη II 254, 3. παράνοια II 395, 44. *V.* cluamentia.

**Amento** ἀπονοῶ II 239, 26.

**Amentum** ligamentum IV 16, 24. ligamentum hastae V 166, 6. ligamentum
quod est in media hasta V 625, 43. est iaculorum uinculum V 615, 43.
**ammentum** ἅμμα λόγχης, λῶρόν ἐστιν ὅθεν κατέχεται ἡ λόγχη II 16, 29 +
51 (amurca). βεροῦτα, εἶδος ἀκοντίου II 257, 15. **armentum** ἅμμα τῶν
ἀκοντίων II 25, 34. **ammentum** ligamentum inmissile V 265, 12.
**admentum** ligamentum IV 304, 28. ligamentum missile V 437, 12.
ligamentum missile, id est quod in medio lanciae ligatur V 437, 14.
corrigia lanceae, quae etiam ansula est ad iactandum IV 476, 30 (ad
lantiam); V 262, 6 (loncae); 632, 5 (que est a.). **amenta** ligamenta
ad messem (*h. e.* missilia) IV 476, 29. **agimenta** augmenta (*vel*
adm.) hastarum IV 13, 42. **amentis** sceptloum (*AS.*, *dat. plur.*) V
341, 43. *Cf. Loewe Prodr.* 368, *Nettleship 'Contr.'* 41. admentum *ex
scriptura* amm *natum est. Cf. Festus Pauli p.* 12, 1; *Isid.* XVIII 7,
6.

**Amera** genus salicis V 342, 4 (*scr.* amerina: *cf. Serv. plen. in
Georg.* I 265). amer\<in\>a ora (*h. e.* lora) *lib. gloss.: cf. Serv.
l. s. s. V.* amarina.

**Ameus** pes miluinus III 549, 46. pedemelbinu III 535, 38. berola III
631, 23. herba, semen eius quasi apii semen III 586, 6. id est herba,
semen, eius similat apii semen III 607, 5; 616, 7. baselice id est
**ameos** III 554, 45; 618, 75. semen nuclei III 535, 16. nuclei[le]
semen III 549, 25. cuminum Aethiopicum id est **ameos** III 558, 62;
622, 49. arterus ameus (= ἄνθος ἄμμιος) flores de **ameo** III 543, 3.
*Cf.* arterus flores de **ameo** III 580, 26. *Cf.* III 543, 71.
**ameus** lino III 543, 25 (lino = mo\<rula\> *Loewe Hermae vol.* XVIII
530, *vix recte*; cimino *Stadler*). *Cf. von Fischer-Benzon* 66.

**Amfariam** *v.* ambifariam.

**Amfractum** περιφερές II 21, 27. iter difficile, tortuosum IV 17, 29;
471, 35; V 265, 48. iter difficile uel circuitus IV 309, 9.
circumfractum, id est flexuosum V 439, 24. confractum IV 471, 37.
**anfracto** per flexum (perplexum?), adeunti difficile IV 17, 16.
**anfracta** intertortuosa (!) et difficilia IV 206, 39/40 (itinera
tortuosa?). *V.* alipes, amfractus.

**Amfractus** περίοδος II 403, 35; 509, 13; III 260, 54. κάμψις ὁδοῦ II
338, 19. διασφαγή II 487, 20; 274, 41; III 260, 53. περιοδεύσιμος τόπος
II 403, 33. διασφαγή † ποταμιας, περικατεαγώς (ποταμία *e.* ἀποτομάς
*c*) II 16, 27. circuitus IV 18, 24; V 266, 16; 345, 29. rima duarum
rerum termin[i]um unum \<habentium\> II 566, 39 (*suppl. Deycks:
vertitur quasi* amfractus *sit commissura errat Hildebrand p.* 11).
circuitus, alias (alius) flexus IV 471, 34. **amfractibus**
circumflexionibus IV 16, 26. *Scribitur et* anf. *et* amf. *V.*
labyrinthus, affretus.

**Amfragosus** locus qui ex multis partibus perambulari potest II 566,
23.

**Amfridis** uersiculis V 338, 41. Amphrysiis *H.*

**Amiantus** genus ligni (!) III 509, 74. *Cf.* III 536, 8

**Amibibus** madidus διάβροχος II 270, 23 (humidus? = qui amas bibit
*Buech.*).

**Amica** φίλη II 471, 27; III 7, 1; 28, 43; 162, 41; 375, 1; 400, 48;
439, 34; 494, 77. φιλητὴ καὶ ἐρωμένη III 304, 35. φίλη, ἐρωμένη III 182,
14. ἐρωμένη III 253, 62. ἑταίρα, πόρνη III 272, 8.

**Amicabiliter** est amiciter V 615, 19.

**Amicalis** φιλικός II 471, 34.

**Amicantur** amicitia sociantur V 439, 29.

**Amicinus** ἀσκοῦ στόμα II 16, 23; 248, 4; 490, 65. os utris II 566,
20. *Cf. Festus Pauli p.* 15, 2.

**Amicio** περιβάλλω ἱμάτιον (ειματιω *cod.*) II 402, 8. περιβάλλομαι
ἱμάτιον II 402, 9. **amicit** uestit IV 16, 15; 478, 19; V 265, 10; 439,
25; 626, 2. uestit eo quod amictum (*truncata*) IV 308, 30. **amicitur**
tegitur aut uelatur IV 15, 45; V 632, 21. induitur V 439, 26.
inuoluitur, tegitur, uestitur IV 478, 20. **amiciantur** operiantur
(amictantur *cod.*, *ut in eis quas attuli saepius est* amictit, *mera
opinor corruptela: cf. tamen GR. L.* VII 433, 9, *nisi ibi quoque*
amicior *scribendum est*) V 439, 27. **amiciti erunt** (amicuerunt?)
texerunt V 632, 20.

**Amiciter** amicabiliter (amicaliter *G*) *Plac.* V 5, 4 = V 47, 6
(*cf. Plaut. Pers. v.* 255). *V.* amicabiliter.
