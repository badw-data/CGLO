*cf.* II 457, 5). **decidit** διακόπτει, συνκόπτει II 38, 28. concidit,
battuit IV 327, 35 (*v.* battuit).

**Deciduum** quod cito cadit V 283, 48; 406, 9. quod octiditur (= cito
occ.) V 633, 38. cito descendet (!) IV 53, 37. **decidium** quod cito
cadit IV 505, 23. qui (!) cito decidit IV 228, 14; 327, 36; V 543, 37.
qui cito cecidit V 496, 5. quod cito decidit V 187, 36; 633, 43. quod
uelociter cadit V 187, 37. **decidua** occidua V 640, 46 (= *Non.* 100,
6). *V.* desiduus.

**Decies** δεκάκις II 38, 33.

**Decima** δεκάτη II 38, 31; III 426, 24. **decimum** δέκατον II 38, 32.

**Decim annis** *v.* bilustrum.

**Decimanum** ouum et fluctum dicimus, quia semper decimum ouum et
decima unda maior est V 566, 49 (*v. Festus Pauli p.* 71, 15; *cf.* 4,
7). **decumano** (-ne *R*) maximo, a fluctu (*vel* fructu) decimo
*Plac.* V 16, 19 = V 60, 23 (*cf. Ovid. Trist.* I 2, 49; *schol.
Lucan.*V 672).

**Decimatio** (*vel potius* decum.) δεκασμός II 267, 44.

**Decimatum** (*vel potius* decum.) δόκιμον, ἐπίλεκτον II 39, 30.

**Decimo** (decerno *cod.*) δεκατεύω II 267, 45. **decimat**
ἐπιδεκατεύει, δεκατοῖ II 38, 29. **decimantur**
ἐπιδεκατεύονται, ἀποδεκατεύονται II 38, 38. **decumantur**
ἐπιδεκατεύονται II 39, 29. *V.* decurio *verbum.*

**Decimus lapis** δέκατον μίλιον III 445, 12; 479, 25.

**Decingo** ἀποζωννύω II 236, 52.

**Decipio** ἐνεδρεύω, ἐπιβουλεύω, ἀθετῶ II 38, 34. σφάλλω II 449, 19;
III 79, 46. ἀπατῶ II 233, 41. ἐξαπατῶ II 301, 56. προδιδῶ III 155, 68.
**decipit** seducit, fallit, circumuenit IV 327, 37. **decepit** ἀθετεῖ,
ἐνεδρεύει, ἠπάτησεν II 38, 9. **decipiunt** ἐνεδρεύονται II 38, 39.
**decepi** ἠθέτησα III 445, 10. **decepisti** προέδωκες (!) III 156, 1.
**deceperunt** ἠθέτησαν II 38, 27. **deceptus sum** ἀπέτυχον II 38, 21.
**deceptus est** προδέδοται II 38, 18. *Cf.* **decepta** ἐνεδρεύει,
ἀπατᾷ (*ubi* deceptat *e: an* decepit = decipit?) II 38, 17. *V.*
deinceps.

**Decipula** laqueus IV 50, 13; 226, 5; 503, 18. laqueus, musci\<p\>la V
187, 40. deceptio V 187, 39. tendicula, laqueus, muscipula V 543, 38.
tendicula, laqueus, muscipula uel pedica V 627, 53. peduca, laqueus, a
decipiendo V 187, 41. bisuicfalle (*AS.*) V 405, 6. **decipulum**
deceptionem IV 50, 23. **decipulam** tendiculam qua aues capiuntur V
416, 37 (*de verb. interpr. = Hieron. in Matth.* 19, 1).

**Decisio** τομὴ πράγματος II 457, 5. κατατομή II 344, 39. κατακοπή II
341, 24. διακοπή II 271, 54. διάλυσις II 272, 31. ἀποκοπὴ χρέους II 237,
53. χρεοκοπία II 478, 20. **decissio** incisio, cassus (*male versum*)
II 576, 29. **decisiones** χρεῶν ἀποκοπαί II 38, 37.

**Decitiuis** de insanis (inter *lemmata graeca: de verb. interpr. =
Hieron. in Matth.* 15, 31, *quo loco collato scr.* de κυλλοῖς) V 417,
13.

**Declamanda** ad laudem pertinet V 404, 58.

**Declamat** μελετᾷ ῥήτωρ II 39, 31. ἀναφωνεί, ἀποφωνεῖ II 38, 40.
**declamauit** ἀνεφώνησεν II 38, 41.

**Declamatio** ἀναφώνησις III 24, 40; 199, 12; 351, 65; 395, 19. μελέτη
ῥήτορος II 38, 42; 366, 58.

**Declamatorium** altum V 567, 5.

**Declaratio** δήλωσις II 38, 48. διασάφησις II 274, 2. φανέρωσις II
470, 3. φανεροποίησις II 469, 55.

**Declaratiuus** δηλωτικός II 269, 17. **declaratiuum** δηλωτικόν II 38,
49.

**Declaro** δηλῶ II 269, 15. διασαφῶ II 274, 3. ἀποδεικνύω II 236, 13.
φανερῶ II 470, 1. **declarat** ἀναγορεύει, σαφηνίζει, δηλοῖ καὶ φανεροῖ
καὶ λαμπρύνει II 38, 46. demonstrat IV 437, 11 (*Verg. Aen.* V 246).
**declara** delucida IV 502, 26. **declarare** σημῆναι, φανερῶσαι II 38,
45. **declarari** ἀνασαφισθῆναι (*ubi* ἀνασαφηνισθῆναι *e*) II 38, 47.
ἀνασαφηνισθῆναι II 39, 24. **declaratum est** πέφ\<ανται\> II 406, 38.

**Declinatio** κλίσις II 38, 43; 350, 62. παρακμή III 205, 68; 572, 72.
ὑπολήγουσα III 363, 13.

**Declino** κλίνω II 350, 61. ἐκκλίνω II 290, 43. κατακλίνω II 341, 15;
III 260, 41. παρακλίνω II 395, 15. **declinat** παρεκκλίνει II 562, 12.
deuitat IV 437, 12 (*Verg. Aen.* IV 185). derelinquit IV 504, 39.
derelinquit aut uitat IV 51, 35. euitat, obtundit (= hebetat) IV 327,
38. **declinare** ἐκκλῖναι II 38, 44. **declinauit** peccauit IV 52, 25.
**declina[bi]tur** κλίνεται II 350, 57. *V.* deplicare.

**Decliuis** κατάντης II 342, 41; III 445, 13; 480, 14. *Cf.* II 576,
55. inclinatus locus IV 226, 10; 327, 40; V 540, 27. **declinius**
inclinatus V 406, 23. **decliue** κατωφερές II 38, 50. contrarium II
576, 60 (*male versum*). **decliuia** incurua IV 327, 39. **decliuum**
κάταντες II 342, 39; III 260, 40. κατωφερές II 346, 51. **declinium**
procliuium II 576, 22.

**Decliuus** κατάβασις II 38, 51; 339, 36; 46; 488, 21; 510, 36; III
260, 39.

**Decoctio** ἀποκοπὴ χρέους II 237, 53.

**Decoctor** φυρατής III 445, 15; 479, 32. χρεοκόπος II 478, 21.
χρεοκόπος, φυρα-
