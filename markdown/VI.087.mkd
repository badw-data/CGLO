χερνίβιον III 203, 41. **aquiminalium** ubi aqua pisilo (= pessulo
*Buech.*) dimitti et obstrui potest ad lauandas (lauandus *Ampl.*) manus
II 567, 19 (*ubi* pistomio = epistomio *pro* pisilo *Loewe GL. N.* 15:
epitoni *Roensch cum Schmitzlo Coll. phil. p.* 120).

**Aquimitti** (?) inrequieta V 267, 12 (acimete *h. e.* ἀκοίμητος?).

**Aquipedum** *v.* acupedium.

**A quo** *v.* quis.

**Aqu[e]o** et **aquor** ὑδρεύομαι II 462, 11.

**A quocumque** *v.* quicumque.

**A quodam** *v.* quidam.

**A quonam** *v.* quisnam.

**A quopiam** *v.* quispiam.

**A quoquam** *v.* quisquam.

**A quoque** *v.* quisque.

**A quoquo** *v.* quisquis.

**Aquosa loca** (*v.* pratum) λειμών III 499, 21. *V.* aquatam, locus
aquosus.

**Aquosus** ὑδατώδης II 462, 2. pluuiosus V 267, 10. **aquosum** et
**aquatum** ὑδαρές, ὑδατῶδες II 18, 48. *V.* aquatam.

**Ar** apud Hebraeos uocatur mensis secundus V 167, 45. *Cf. Ideler* I
*p.* 510.

**Ara** βωμός II 18, 49; 261, 1; 492, 36; 517, 16; 539, 62; 552, 20; III
9, 71; 83, 50; 129, 19; 171, 31; 238, 37; 301, 37; 362, 9; 400, 39; 439,
58; 490, 63; 511, 19. θυσιαστήριον III 241, 61 (*sign. caeli*). a
precibus dicitur, quas Graeci ἀράς uocant V 549, 35 (*cf. Serv. in
Aen.* II 515). altare IV 207, 38; V 267, 32. **aras** altaria IV 20, 13.
*V.* area, ansa.

**Arachne** uirgo lanificii peritissima V 652, 24 (*Iuvenal.* II 56).

**A radice** radicitus IV 310, 15.

**Aram sepulchri** rogum IV 430, 12 (= *Verg. Aen.* VI 177).

**A ramulariis** ἀπὸ τῶν ἀντεπιτρόπων liber de officio proconsulis II
18, 52 (*ubi* rauulariis *d cum Cuiacio. Cf. Rudorff 'Abh. d.*
*Berl. Ac.*' 1865 *p.* 266).

**Arandum** sulcandum, nauigandum V 626, 20. *V.* aequor arandum.

**Aranea** ἀράχνη II 243, 51; III 320, 51; 431, 56. bambis (= bombyx) IV
310, 16. **aranea** et **araneum** ἀράχνη II 18, 50. **araneum** est
quod textum est, **aranea** ipsum animal V 561, 46 (*cf. GR. L.* VII
522, 31). **arania** er\<y\>sipela minor milio similis in cute III 596,
10. erpinas (= derbitas) id est **aranea** III 600, 23. *v.* araneus,
bubestris.

**Araneus** ἀράχνη II 18, 51; III 19, 24; 91, 16; 188, 36. masculino
genere animal, **aranea** feminino genere tela araneorum V 652, 25
(*Iuvenal.* XIV 61: *cf. Serv. in Georg.* IV 247). δράκων, ἰχθύος
γένος II 18, 55. δράκων III 17, 28; 89, 48; 318, 15. *V.* mus araneus.

**Arar** fluuius Germaniae IV 208, 8 (*immo* Galliae: *cf. Serv. ad
Verg. Ecl.* I 62).

**Arase** lauari V 440, 57; 561, 35. ἆρσαι lanare *Buech. Cf.*
arseuerse auerte *Fest. Pauli p.* 18, 15.

**Aras tenentem** apud aras (amparas *codd.*) agentem IV 430, 13 (=
*Verg. Aen.* IV 219).

**Aratio** ἀροτρίασις II 245, 41; III 199, 62; 261, 13. ab arando II
568, 15.

**Aratiunculas** modicas fossas *gloss. Salom.* (*cf. Loewe Prodr.*
284: *quam glossam non tam ad Plautum* \<*Truc.* 148\> *quam ad* 3
*Reg.* XVIII 32 *pertinere Loewe postea vidit*).

**Arator** ἀροτήρ II 245, 39; III 439, 59. ἀρότης II 18, 53. ζευγηλάτης
III 261, 17. rusticus IV 310, 17.

**Aratrum** ἄροτρον II 18, 54; 245, 40; III 27, 32; 262, 52; 299, 68;
357, 32; 467, 26; 508, 68. *Cf.* II 546, 50, *ubi Keil* aratrum
*scribit: at* rutrum *verum est.*

**Aratura** ἀροτρίασις II 245, 41. ab arando dicitur II 567, 30.

**Arax** ἄραξ (= ἄρακος) τὸ ὄσπριον II 243, 49.

**Araxis** fluuius Armeniae IV 406, 7 (*cf. Serv. in Aen.* VIII 728).
fluuius orientis IV 472, 25; 28; V 268, 26. fluuius Armeniae in
oriente[m], ibi et ararim (Ararat?) V 440, 56. *V.* Arar.

**Arbatae** (?) sibaed (*AS.* = gesiebt?) V 340, 54. *V.* artaba.

**Arbellum** (?) βασανιστήριον II 19, 2.

**Arbetes** *v.* arbor.

**Arbiter** μεσίτης II 25, 11; III 298, 13; 375, 46; 439, 60; 475, 23.
μεσίτης, διαιτητής II 19, 5. διαιτητής II 271, 28. διαγνώμων II 270, 29.
διαιτητής, αὐτόβουλος II 25, 12. χαμαιδικαστής II 475, 15 (*v.* pedaneus
iudex). mediator II 568, 17. iudex a duobus electus IV 207, 40; 480, 44.
iudex priuatus IV 311, 15. iudex probatus (?) seu pacificus IV 19, 43.
iudex quem lex non praefinit quid iudicet, a duobus \<electus\> V 267,
40. quem lex non definit quid iudicet IV 480, 45. iudex eo quod arbitrio
causa dicitur V 546, 3. censor, iudex, testis IV 480, 41. gratiator,
unitor, foederator, pacis factor *cod. Leid.* 67 *E* (*Loewe Prodr.*
410). **arbitrum** indicem uel medi[c]um V 530, 39 (= *Ter. Ad*.
123).

**Arbiterium** arbitrium II 567, 37. collegio (*vel* collectio)
arbitr[i]orum multorum, id est ipsa consensio (consessio?) IV 311, 16;
V 590, 56 (collectio). collegio arbitrorum V 632, 32. collegium (*vel*
collegio) arbitrorum multorum, id est ipsa consensio ipsorum IV 208, 16.
*V.* arbitrium.

**Arbitrans** ratus, aestimans IV 311, 17.
