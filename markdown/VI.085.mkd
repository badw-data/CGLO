**aptare** conponere uel reparare IV 19, 10; 471, 3. ἥρμοσεν III 437,
55. **aptauit** comparauit IV 471, 2; V 259, 16; 343, 40. *Cf.* apio.

**Aptotus** ubi nulla inflexio casu\<u\>m *a ante* IV 5, 27.

**Aptra** ἀμπελόφυλλα ὡς Τιτίννιος (*p.* 187 *ed. min. Ribb.*) II
18, 34. **abtra** folia uitea IV 202, 22; V 440, 51; 490, 10 (uitis).
apiastra et μελίφυλλα *Salmasius ad Plin. p.* 101: *cf. Klein Mus.*
*Rhen.* XXIV *p.* 295.

**Aptum (*vel* apertum) est** expedit IV 310, 11.

**Aptus** ἁρμόδιος II 245, 9. ἁρμοστός II 245, 21. ἁρμόζων III 372, 62.
εὐάρμοστος II 316, 19. εὔθετος II 317, 18. ἐπιτήδειος II 311, 56.
utilis, necessarius IV 19, 3; 484, 23. habilis, idoneus, commodatus,
compositus IV 310, 12. **aptum** ἡρμοσμένον II 20, 14; III 143, 14.
ἁρμόδιον II 245, 10. ἁρμοστόν II 245, 22. ἁρμόζον III 423, 57.
ἐπιτήδειον, εὐάρμοστον II 20, 21. compositum (*cf. Non.* 234, 16) IV
19, 11; 484, 24. conexum, congruum IV 430, 8 (*Aen.* IV 482 = VI 797 =
XI 202). **aptam** congruam IV 207, 5. **aptius** ἐπιτηδειότερον II 20,
20. **aptissimum** commodatissimum, necessarium IV 310, 10.

**Apua** ἀφύη II 254, 7 (*v.* mel marinum); III 89, 11; 186, 68; 355,
55. piscis minutus *Plac.* V 7, 29 = V 48, 12. *Cf.* ἡ ἀφύα apyia,
balaena III 256, 66: *quae glossa vix est vestusta: praeterea contam.*
*Cf. Festus Pardi p.* 22, 18.

**Apud** παρά II 394, 5. πρός II 420, 15. παρά, πρός II 20, 15. **ape**
παρά II 21, 40. ad *Plac.* V 6, 29 = V 48, 13. iuxta V 637, 2 (*=Non.*
68, 11; *cf.* 522, 22).

**Apud acta** ἐπὶ πράξεως II 20, 11.

**Apud me** παρ' ἐμοί II 18, 39. **apud te** παρὰ σοῦ (*scr.* σοί) II
18, 40. **apud se** παρ' ἑαυτῷ (εαυτων *cod.*) II 18, 27. **apud nos**
παρ' ἡμῖν II 18, 25. **apud uos** παρ' ὑμῖν II 18, 41.

**Apud** (*vel* aput) **te sies** constanti animo V 530, 18 (*Ter.*
*Andr.* 408).

**A pueritia** (apuerilla *cod.*) παιδιόθεν II 392, 13.

**Apulia** dicta est Messapia, Peucetia, Daunia V 549, 29 (*Serv. in
Aen.* VIII 9).

**Apyretus** (*ita Warren:* aperetus *cod.* = apoeretus) sine febre IV
207, 14.

**A qua** ἀπὸ τίνος θηλυκῶς II 241, 34. ἀπὸ ποίας II 239, 64. ἀφ' ἧς II
253, 5.

**Aqua** ὕδωρ II 20, 23; 462, 25; 496, 22; 521, 20; 544, 63; III 87, 61;
184, 28; 244, 37; 255, 43; 315, 26; 398, 20; 505, 29. nero (νερόν) II
563, 17 (*cf. Buecheler Fleckeiseni Ann.* CXI *p. 310* sg.).
**acua** ὕδωρ III 467, 6. **aquam** ὕδωρ III 15, 13; 364, 53. **acuae** ὕδατα III 467, 7. *Cf.* **aqua** minutium V 167, 38 (?). *Petr.* 47 *confert Buech. V.* niuata aqua.

**Aqua calida** συγκεραστόν III 184, 33; 315, 35 (calda). **aquae
calidae** θερμαί III 306, 20; 522, 26. V calda aqua.

**Aqua cum naturali colore** officidata (oxidiata *Buech.*) III 603, 36.

**A quacumque** *v.* quicumque.

**A quadam** *v.* quidam.

**Aquae cursus** ῥεῦμα II 427, 46.

**Aquaeducta** *v.* nymphaea.

**Aquaeductium** *v.* aquiductium.

**Aquaeductor** ὑδραγωγός II 462, 6; III 439, 55; 482, 7. deriuandi
aquam peritus II 567, 24.

**Aquaeductus** ἀγωγός II 20, 28; 218, 3; 525, 30; III 487, 39; 507, 7
(aquae ductum). ὁλκός II 382, 21. ὑδρα\<γω\>γεῖον III 196, 53.
ὑδραγωγός, ὑδραγώγιον III 246, 43. cursus aquarum V 440, 55.
**aquaeducti** ὑδραγωγοί III 433, 39. *V.* canalis.

**Aquaemola** ὑδρόμυλον II 521, 19.

**Aquaemolina** ὑδρομύλη II 462, 17. **aquaemoliuus** ὑδραλέσιο\<ν\> III
306, 53

**Aquaemolus** ὑδραλέτης II 462, 7.

**Aquae mons** magnitudo IV 472, 44; V 262, 26. fluctum uel mons
(fluctuum mons? maris *Buech.*) IV 430, 9 (*Verg. Aen.* I 105).

**Aquae portator** ὑδροφόρος II 462, 22. qui manibus portat aquam II
567, 25.

**Aquae potor** ὑδροπότης II 462, 18.

**Aquagium** ὑδραγώγιον II 462, 5. *Cf. Festus Pardi p.* 2, 11. *V.*
aquagium.

**Aquale** ὑδροχοεῖον III 368, 47. ἐπίχυσις III 324, 57.

**Aqualegellae** ποδόκοιλον II 410, 52 (aquale, gello *Ducangius*).

**A quale, a quali, a qualibus** *v.* qualis.

**A quali\<bu\>scumque** *v.* qualiscumque.

**Aqualiculus** γαστρίον II 514, 29. uentricolus V 549, 31. κοιλία III
248, 33. **aqualiculum** κοιλίδιον ἐκζεστόν II 351, 43. κοιλία χοίρου
(aqualicum *cod.*) II 351, 42. κοιλίδιον (*vel* κοιλίδιν) III 87, 38;
218, 31 = 653, 11; 314, 50; 526, 24. uentriculum IV 19, 35; 208, 25;
472, 4; V 167, 39; 440, 53. uentriculus aqua plenus II 567, 20. taurus
(*v.* taurus) **aquacaleculus** III 577, 60. **aquiliculum** pingui dum
super umbilicum V 167, 44. *V.* aqualis.

**A qualicumque** *v.* qualiscumque.

**Aqualis** aqualiculus V 167, 40 (alicuius) *et* 41.

**Aqualium** summa pars capitis *Scal.* V 589, 43 (Aequilibrium *et*
scapi *Semler*). *Cf. lib. gloss.* (aqualiam) *et Papias*
(aquilium). *Videtur contam.* (apex).

**Aquam ingere** *cod. Leid.* 67 *E* (*cf. Loewe GL. N.* 154 et
*Plaut. Pseud. v.* 157).
