<div type="section" xml:id="XYZ"><pb n="VII.431" facs="7.431.jpg"></pb>
**Xanctum bibissent** (*Verg. Aen.* I 473) prodesco (pro de sancto =
Xantho?) *c post* IV 197, 11.

**Xanticos** Macedonum lingua Aprilis mensis dicitur *lib. gloss. V.*
menses

**Xenium** graece, ab ammiratione (? animi ratione *Buech.* ammigratore
*Schoell*) IV 197, 12. **xenia** munera, dona IV 197, 11. quod sunt
praemia uel dona, per x scribendum *Plac.* V 103, 13. *V.* epimenia.

**Xenizo** hospitor IV 197, 9. **xenizor** hospitor V 583, 21.

**Xenocrates** ostii (hospitii?) rector III 505, 21.

**Xenodochium** susceptio peregrinorum III 505, 20; V 424, 20
(xenodochiorum *cod. de dial.*). receptatio peregrinorum V 359, 49
(exen.). locum uenerabilem in quo peregrini suscipiuntur IV 298, 21.
hospitium, locus uenerabilis in quo peregrini suscipiuntur IV 197, 8.
**xenodochiorum** collectionum V 401, 4; 416, 58 (*de verb. interpr.*).
*Cf. Is.* XV 3, 13. *V.* receptaculum, synodiciis.

**Xenodochus** susceptor peregrinorum III 505, 19.

**Xerophagia** herbae quae comeduntur incoctae V 417, 30 = V 426, 41
(*Cassian. inst.* IV 21).

**Xiflon** id est sifion III 579, 37. fiscanion (φασγάνιον) id est
**sifion** III 563, 22. exsiphea id est **exsifion** III 561, 44.
actorion (ἀορίον? *at cf. Diosc.* IV 20 ἀνακτόριον) **exilio** III 550,
37. arion id est **exfiοn** III 550, 38. *V.* gladiolus.

**Xosfer** Tureorum (*h. e.* Tuscorum) lingua October mensis dicitur V
255, 18. *V.* menses.

**Xozibus (?)** mendacium V 519, 56.

**Xyla** graece, latine lingua (= ligna) IV 197, 10.

**Xylobalsamum** lignum est balsami III 560, 51 (exhilobalsami); 579, 36
(xirobals. *cod.*). **xilobalsamo** id est lignum balsami III 630, 53.
*Cf. Isid.* XVII 8, 14; *Serv. in Georg.* II 119.

**Xystarcha** substantiae (?) princeps IV 470, 52 (*gl. Verg.*).
**xystarches** athletarum princeps *c post* IV 197, 11; V 255, 16.

**Yalium** *v.* hyalinum.

**Yar** Syrorum lingua Maius mensis dicitur V 255, 20. **Yer**
Hebraeorum lingua Iunius mensis dicitur V 255, 23. *V.* menses.

**Yerana** Hebraeorum lingua Iulius mensis dicitur V 255, 24. *V.*
menses.

**Yripeon** *v.* haeresis.

<pb n="VII.432" facs="7.432.jpg"></pb>
**Zaab** (hebr.) genus auri III 506, 6. genus auri, obryzi III 521, 11.

**Zaba** indumenti genus V 519, 58. munimentum uirorum fortium III 505,
74. munimentum III 521, 12. munimentum in proelio uirorum fortium
*Plac.* V 104, 13; V 655, 11 (taba). *Cf. Ducange.*

**Zaber** *v.* rosmarinus.

**Zaberna** ubi uestes ponuntur III 506, 7; 521, 13 (*cf. a post* IV
427, 13). ubi uestes ponuntur aut quodlibet aliud *Plac.* V 104, 14. ubi
uestes portantur (?) uel quodlibet aliud IV 197, 31; V 519, 57 (quid
libet); 583, 24 **zabarras** arcas V 401, 15. *V.* pera, gaberina,
gabarnas. *Cf. Ducange.*

**Zabulon** habitaculum fortitudinis V 401, 17 (*cf. Roensch Mus.
Rhen.* XXXI 464; *On. sacra* 11, 29).

**Zabulus** Satan III 505, 75; 521, 14.

**Zaccheus** humanum genus *e post* IV 198, 6.

**Zacynthos** insula Achaiae III 506, 8; 521, 15; *c post* IV 198, 6.
insula *c ibidem.*

**Zame** fons in Africa III 521, 16. *Cf. Isid.* XIII 13, 2.

**Zanga** pellis. Acro: nigris medium impediit crus pellibus *Scal.* V
613, 45 (*cf. Loewe Prodr.* 50). sunt ossa (!) V 625, 11. *V.* caliga,
ozasanga.

**Zar** apud Hebraeos uocatus mensis secundus, id est Aprilis (*Eucher.
instr. p.* 153, 18?) V 255, 26. *V.* menses.

**Zare** oriens uel ortus III 607, 7 (*Isid.* VII 6, 41; *Onom. sacra*
21, 14).

**Zarnabo** id est careo III 630, 59. *V.* careo.

**Zaziton (?)** omnia condimenta III 596, 38. **zazituri (?)**. id est
omnia condimenta III 630, 58.

**Zeduar** *v.* cidoar.

**Zelatus** *v.* inuidus, inuisus.

**Zelo** ζηλοτυπῶ III 74, 62. **zelat** ζηλοῖ III 467, 8.

**Zelocius** (zeloticus? zelotypus?) *v.* pelicator.

**Zelotes** ζηλώτης II 556, 14.

**Zelotypus** ζηλότυπος III 4, 42; 142, 43; 373, 15; 467, 9. ζηλώτης III
338, 21. aemulus formae III 505, 68.

**Zelus** ζῆλος II 322, 14. **zelos** gelos (= ζῆλος; *cf. W. Heraeus
'Spr. d. Petr.'* 17) III 74, 63. **zelus** (*vel* zelum) aemulatio,
inuidia, liuor IV 403, 48. **zelus** aemulatio, inuidia, feruor,
indignatio, ira IV 197, 30. aemulatio IV 298, 31. est siue cum coniugis
adulterio turbatus animus contabescit siue cum seruandae pudicitiae
coniugalis diligens custodia adhibetur IV 197, 29.

**Zema** sucus IV 197, 34. **zima** (*i. e.* ζύμη) fermentum V 583, 27.
**zimi** frumentum IV 198, 5. **zema** ulla (= olla) IV 427, 12.
**zima** olla IV 298, 34. **zema** ulla V 545, 69. **zima** uilla (!) IV
198, 4; V 583, 26. *Diversa coniunxi.*

**Zephyris felicibus** prosperis uentis *c post* IV 198, 6 (*Verg.*
*Aen.* III 120).

**Zephyrus** ζέφυρος III 245, 37. uentus III 521, 17; IV 298, 35; *c
post* IV 198, 6. nomen uenti *c post* IV 198, 6. fauonius uentus *c
post* IV 198, 6. uentus fauonius IV 403, 47. *Cf. Serv. in Georg.* II
330; *Is.* XIII 11, 8.

**Zernam** inpetigo IV 298, 30. **zernae** impetudines (= impetigines)
ex flegmate falso IV 197, 35 (*cf. a* IV 298, 20). **zernas** licinas
(λειχῆνας) III 602, 38. *V.* impetigo (*ubi* sannam), derbitas, sterna.

**Zeta** graece cubiculum V 337, 26. *Cf.* **dieta** zeta III 191, 1.
**zetas** aedes V 583, 25. domus V 337, 28 (*cf. Aldhelm de laud.
virginit.* 33). id est sedes hiemales quae calidae fiunt obducto igne IV
197, 32 (*v.* zetas aestiuales).

**Zetarius** custos palatii V 519, 59. mansionarius V 519, 60
(zotarius).

**Zetas aestiuales** quae frigidae fiunt obducta aqua IV 197, 33. *V.*
zetas hiemales.

**Zetas hiemales** sunt domus hiemales quae calidae efficiuntur
subductis flammis, quod uero est melius **zetae hiemales** britischae
(*Diez* I bertesca) austro appositae, **zetae aestiuales** britischae
aquiloni appositae, a similitudine zetae litterae V 586, 8.

**Zibei** florentes V 401, 16 (*cf. Roensch Mus. Rhen.* XXXI 464).
**zippei** germinantes V 583, 28. Ziphaei *Onom, sacra* 37, 1.

**Zibina** (zip. *cod.*) est lancea V 625, 10. **zibinarum aculeis**
lancearum *c post* IV 198, 6; V 255, 28 (zybinn.). **zybinnas** lanceas
V 255, 27. *Cf. Iulian. Antec. Const.* 79, 4. *V.* uenabulum.

**Zima, zimi** *v.* zema.

**Zimzario** κάνθαρος III 17, 45. *V.* zinzalario.

**Zingiber** *v.* gingiber.

**Zinzala** *v.* culex, sciniphes.

**Zinzalario** (*vel potius* tzintzalario) *v.* culiculare, zimzario.

<pb n="VII.433" facs="7.433.jpg"></pb>
**Zippei** *v.* zibei.

**Zira** παλίουρος III 192, 7 (piliuros *cod. cf.* paliurus, *ubi* zura
*est: item Diosc. lat.*).

**Zirotha** series IV 298, 33.

**Zita (?)** thimonia (θημωνιά) III 200, 13. zeta?

**Zizania** lolium IV 198, 6; 298, 32 (*cf. Eucher. instr. p.* 148,
16; *Isid.* XVII 9, 106). **zizanium** (-ia *d e*) lolium IV 427, 13.
*V.* lolium.

**Zizeria** *v.* gequaria.

**Ziziphu\<m\>** ζίζιφον III 429, 12. **zizifa** σηρικά (σιρ. *cod.*) II
431, 50. *Cf. Bluemner 'Maximaltarif' p.* 94.

**Zizyga** rustice galla V 337, 27. *V.* galla.

**Zmaragdum** lapis pretiosus *Plac.* V 104, 15 (*v.* smar).

**Zodiacus** animalis V 337, 29; 401, 13 (zoziacus). XII signae (!)
continens V 401, 14 (*item*). **zodiacum** signiferum IV 198, 2.
**zoziacum** sideralem V 401, 18; 415, 40; 425, 13 (*lib. rot.* = *Isid.
de nat. rer.* IV 1).

**Zoelet** tractum uel purgatum III 607, 8. *Cf. Onom, sacra* 44, 3.

**Zomos** ius IV 198, 3. *V.* ius 2.

**Zona** ζώνη III 74, 50. cingulum II *p.* XIII; IV 197, 36. cingulum,
balteum IV 403, 49. cingulum uel balteus III 506, 4. *Cf.* ζώνη
semicinctum, id est **zona** IV 198, 1. **zone** cinctore (?) *c post*
IV 198, 6. *V.* aona.

**Zona pellicia** στρόφιον (στοφιος *cod.*) II *p.* XIII.

**Zonatim** ingenuorum (in goerum?) V 649, 17 (*Non.* 189, 24).

**Zopyrion** *v.* azofirion.

**Zoroastres** (zoro asrtes *cod.*) ipse est magister Babylonis V 337,
25.

**Zozum** (= zodion) graece animal V 337, 30. **zozia** signa *c
post* IV 198, 6.

**Zura** iaceu (!) III 539, 67. panatros III 541, 52. *Cf.* paliurus,
zira.

**Zythum** ζύθος III 315, 66.
</div>