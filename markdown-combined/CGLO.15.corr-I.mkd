<div type="section" xml:id="corr-I"><pb n="VI.praef_IX" facs="6_praef.9.jpg"></pb>
**abhorresco]** *adde* διαφωνῶ II 275, 49.

**abigeus]** *adde post* V 492, 15 (*contam. cum* auidus?).

**abomi­nandus]** *adde post* II 242, 12 ἀπευκτόν II 234, 49.

**abscidio]** *scr.* ἀποτομή II 241, 48.

**absorbuit]** absorta est = *Vulg. Cor.* I 15, 54 (*cf. Lachm. ad
Lucr.* p. 254).

**abstrudo]** *scr.* ἐξώθησεν.

**abusat]** *cf.* abusiue.

**abutor]** II 242, 44 *est* ἀποχρῶμαι.

**acatus]** *V.* carabus.

**accedo]** *ad* accedunt *cf. GR. L.* VII 264, 20 (accidunt).

**accidentia]** *V.* addictio.

**accitus]** *adde* uocatus IV 5, 41 *et* IV 302, 54 *ante* V 625, 17.

**aceo]** acie turba *H. pro* acet curuat.

**acerrale]** λαβίς *H.* 'λαβῆς δ. ab acore, quantum digito prendas'
*Buech.*

**acisculus]** axinoryx *pro* axedonis *H.*

**acronyctae]** *cf. Firm. math. p.* 50 *ed. Sk. et K.*

**acuto murice]** *Verg. Aen.* V 205.

**acutor]** *del.* **auctor.**

**acutum** **cerno]** *v.* certum non cerno.

**adhibeo**] *scr.* παραλαμβάνομαι (*pro* -ω).

**adhibitis**] *scr.* συνπαραληφθέντων.

**admissarius**] *del. V.* amissarius.

**adoleo**] *del.* = oleo.

**adulo]** *post* II 313, 63 *adde* σαίνω II 429, 34.

**adumbro]** describit *pro* discrepat *H.*

**Aeneadae]** *v.* Eneades.

**aenigma]** *v.* inaegnea.

**aera]** *cf. Isid.* VI 15, 4. 5.

**aeriae uirgae]** *scr.* **uerua** uirgae f. *H. V.* ueru.

**affabilis]** *v.* effabilis.

**affector]** offector *priore loco H.*

**affrutabulum]** *scr. afr.* **aggaudeo** suppl. *H.* II 423, 40.

**agitata]** *de* mensura *cf. Vulg. Luc.* 6, 38 coagitata mensura.

**agnina]** *pro* ἀρνεία *scr.* ἄρνια.

**Agrantos]** *scr.* Agrianios. *v.* menses.

**allido] adlectat** adlicit *nunc H. Adde* Almaciuum *v.*
Dalmatinum.

**almus ager]** *del.* (*v.* aruum). *Adde* alucus *v.* ales, ulucus.

**Amaryllis]** *v.* Maryllis.

**am­bactus]** *scr.* δοῦλος, μισθωτός.

**ambiguus** hoc amb. et haec amb. *adde* V 300, 61.

**ambuuia]** *cf.* Georges ambubaia. 'Wegewarte' *confert Buech.*

*Adde* **amenticus** *v.* dementicus.

**amfragosus]** περιοδεύσιμος τόπος II 403, 33.

**amicio]** περιβάλλομαι *est* **amicior**.

**amplexabilis]** *scr.* Ἁβρότονον: nam nomen est.

**animulus]** *v.* papilio (V 318, 33).

**antescholanus]** *Cf.* III 277, 32.

**antlia]** *cf.* sentina.

**aphrodin]** *recte, cf. Isid.* VIII 11, 76.

**apogeum]** *cf. Isid.* XV 3, 12.

**aqua]** *v.* ex aqua.

**aquagium]** *scr. V.* aquarium.

**aqualis]** *adde* ὕδρωψ II 462, 24.

**ara]** *v.* hasenam.

**arcumterebra]** ὄρυξ *H.*

**arcus]** *post* **arcus caeli** *adde* ἶρις.

**argenti bigati]** *Liv.* XXXIII 23, 7.

**arra]** *v.* masculinus.

**as]** chus congius *H.*

**asper** et **aspritudo]** *dicendum erat et respiciendum* ἄσπρος
*Neograecorum* = albus *esse*; *cf.* candor.

**asser]** *cf. Isid.* XIX 19, 7.

**atrium]** *scr.* πύλη. *v.* hastarium.

**Attali]** *cf. Isid.* XIX 26, 8.

**attenso]** οὐδέτερον neutrum *H.*

**audio]** odit ut sorex saurex *H. coll. Isid.* XII 3, 2.

**aurunculus]** *v.* arunculeus.

**auspicium]** *cf. Serv. in Aen.* VII 257.

**azyma]** ἄλεισον = zema *H.*

**Bachium]** *cf. Liv.* XXXVII 21, 7.

**baium]** *dele* prasum *ante H.*

**barbiton]** *de* barbita *cf. Isid.* III 21, 3.

**baro]** *adde* baro fortis in laboribus *gloss. arabicol.*

**batutus]** ἀναυδῆ (*et* battulum) *H.*

**bellum intestinum]** *adde* πόλεμος ἐμφύλιος II 411, 58.

**bene uertat]** *Liv.* XXXIV 34, 2.

**bestiarius]** θηρευτής *pro* θηριοτης *H.*

*Adde* **biduuium** *v.* uid.

**bigener]** *v. Isid.* XVIII 4, 4.

**bilis]** *V.* sine bile.

**bilustrum]** *scr.* decim annis.

**bissum]** *v.* dissum.

**bonorum cedo]** ἐξίσταμαι II 303, 41 (*v.* cedo).

**bradigabo]** **bradigabo** feldhoppo *'Iourn. of Phil.'* X 96; *cf.*
*AHD. GL.* IV 245^b^ 45 (*Kluge*).

**bucinum]** *v.* bigener.

**bulbus]** *v.* uulbos.

**Burrae Vatr.]** *scr.* Burra

**caduceator]** *Liv.* XXXII 32, 5.

**caedo]** *v.* pugnis caedo.

**caelatum]** *adde* caelatus τετορνευμένος II 453, 54.

**calamus]** *cf. Vulg. Ex.* 25, 33; calamiscus.

**caldarius]** *cf.* aenulum.

**calumnia]** *cf. Isid.* V 26, 8.

**calx]** *v.* carcer.

**canier]** ganeus *H.*

**caniles]** τηγανίτης *H.*

**Canopus]** *v.* excetra.
<pb n="VI.praef_X" facs="6_praef.10.jpg"></pb>
**cantherius]** *cf.* Isid. XIX 19, 15.

**cardus]** *de* cardum *adde GR. L.* I 75, 1 *sq.*

**carticula]** *GR. L.* II 213, 4 confert *H.*

*Adde* **catagoga** *v.* ocimum *et* **cataegis** *v.* totegis.

**cataphractus]** *Liv.* XXXVII 40, 11?

**caupulus]** *adde* (*v.* capulum) post locellum.

**Celaena]** *Liv.* XXXVIII 13, 5.

**cerbarii]** *cf.* V 481, 30 (satellites).

**cereacas]** cloacas secessus *H.*

**cetratos]** *cf. Liv.* XXXI 36, 1; XXXIII 4, 4.

**ceruicosus]** αὐχενίας **ceruiciosus** *cod*., *fortasse recte*.

**Chimaera]** *cf.* Isid. I 39, 4.

**cinctor]** ξωστης *cod.*, *recte*.

**cingillum]** *cf.* redimiculum.

**cistophori]** *cf. Liv.* XXXVII 46, 3.

**citatus in lingua]** *Vulg. Eccl.* 4, 34.

*Adde* **clausa** cl[a]usa, negata Plac. V 55, 19.

**clipeus]** *v.* calbae, pluteus.

**co]** *v.* quo.

*Adde* **colligatio** σύνδεσμος II 444, 41; III 442, 46.

*Adde* **collis iugum** *v.* iugum.

**colobistae]** *cf. GR. L.* suppl. p. 95, 10.

**colurnum]** λεπτοπάρυον *est* **colurna**.

**comitium facio]** *v.* conuicior.

**compaginatus]** *adde* compagitus ἁρμολόγησις II 245, 16.

**concauae]** *v.* poples.

**conci­torem]** *Liv.* XXXVII 45, 17.

**congrego]** *adde* **congregat** συναθροίζει, συμφορεῖ II 109, 21.

**congregatus] consartum** consutum *Hildebrand*.

**congruenter paren­tibus]** *cf. GR. L.* I 314, 11 (congruus
patribus).

**considunt] considere** conruere *H. coll. Verg. Aen.* II 624.

**Copsa]** *cf. GR. L.* II 77, 13.

**Corax]** *cf. Liv.* XXXVI 30, 4.

**coria]** *interpretamentum merum est: v.* portarum indumenta
(*itaque scr.* portae indutae).

**Coronides]** *cf. GR. L.* II 63, 18.

**cylleus]** culleus *latet*: *expli­candus error ex GR. L.* I 553, 34
(culleus ὁ ταύρειος, caenum βόρβορος *H.*).

**Cydnus]** *cf. GR. L.* II 42, 18 Cydnus, Ariadne (*H.*).

**cultellus]** *post* **cultellum** *adde* μαχαίριον.

**cumba]** *verba* a curando *ad glossam sequentem* (curator) *refert*
*H.*

**cupidus]** *adde* φιλάργυρος II 471, 25.
</div>