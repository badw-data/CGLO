from unicodedata import normalize

def normalize_file(infile):
    with open(infile, "r", encoding="utf-8") as f:
         text = f.read()

    # Perform a canonical decomposition and recomposition.
    text = normalize("NFKC", text)

    with open(infile, "w", encoding="utf-8") as f:
         f.write(text)
