# Disambiguate references assigned to multiple glossaries, e.g. @addName = 'hermen. frg. Brux. / hermen. frg. Leid.'
# Relies on a CSV file 'data/ambiguous_glossaries.csv' formatted as follows:
# In each case of overlap, one of the two ambiguous glossaries is named.
# The beginning of the gloss. has the suffix "inc.", the end "fin.".
    # This must be entered below!
# Each rows gives some item of text content that can be used to identify
# the lemma/gloss pair. The rows must be *exactly* in the order of the glossary.
# The name in the first row must be *exactly* as found in gloss_names.xlsx.
# (Everything else is assumed to belong to the other gloss)

import pandas as pd
import re
from lxml import etree

AMBIG_FILE = 'data/ambiguous_glossaries.csv'
XML_FILE = 'tests/CGLO.99.combined-notes-defs.xml'
df = pd.read_csv(AMBIG_FILE)

def disambig_ref(ref):
    glossary = ref.get("addName") or ''
    name1, _, name2 = glossary.partition(' / ')

    if name2 == '':  # glossary is not ambiguous; nothing to do
        return None

    # match = set([name1, name2]).intersection(df.keys())  # check if there is intersection between name1, name2 and ambiguous glossaries

    cref = ref.get("cRef")
    vol = cref.split('.')[1]
    line = int(cref.split('.')[3].partition('/')[0])  # get the line number, drop anything after '/' and turn to int
    lemma_list = ref.xpath("preceding-sibling::form[1]")
    if not lemma_list:
        print(f"No preceding lemma found ({cref}:", etree.tostring(ref))
        return None
    lemma = lemma_list[0].text  # get preceding lemma or ''
    gloss_list = ref.xpath("preceding-sibling::def[1]")
    gloss = gloss_list[0] if gloss_list else ''  # get the preceding definition
    gloss_text = etree.tostring(gloss, encoding="unicode", method="text")



    # Abav. mai. occurs twice in CGL: once in vol. IV as a specimen, once in
    # vol. V as excerpta. We need to disambiguate these for purposes of search.
    if "Abav. mai." in name1 and vol == "IV":
        name1 = "Abav. mai. spec."
    if "Abav. mai." in name2 and vol == "IV":
        name2 = "Abav. mai. spec."

    name1 = name1 + ' fin.'
    name2 = name2 + ' inc.'
    matches = []
    for n in [name1, name2]:
        if n in df.keys():
            matches.append(n)
        else:
            non_match = n

    if matches == []:  # neither name is in the ambiguous list
        print(f"\tCouldn't find either {name1} or {name2} ({glossary}) in the ambiguous glossary table.")
        return None

    results = []  # list to store results (usually only one result, if there are two we have a problem!)
    for match in matches:  # if both names are on the list we need to check both columns
        if line > len(df[match].dropna()):  # line number is higher than rows in the csv column
            continue  # no result here

        combined_text = lemma + ' ' + gloss_text  # combine lemma and gloss for searchability

        table_text = df[match].dropna()[line - 1]
        # print("Comparing table_text", table_text, "to combined gloss text", combined_text)
        if table_text.lower() in combined_text.lower():  # there is textual overlap
            results.append(match)  # add match to results list!
            continue
        else:  # no match, return the other of the two names
            continue

    if len(results) == 1:  # easiest case: a single result
        results[0] = results[0].removesuffix(' inc.')
        results[0] = results[0].removesuffix(' fin.')
        results[0] = results[0].replace(" spec.", "")  # for Abav. mai.
        ref.set("addName", results[0])
        # print(f"Lemma ({lemma}: {gloss_text} at loc {cref}) disambiguated to match {results[0]}")
        return ref
    elif len(results) == 0:  # no results, action depends on whether we have one or two matches originally
        if len(matches) == 1:  # only one of the two ambiguous glossaries in the list, and it wasn't matched, so assign the unmatched name
            non_match = non_match.removesuffix(' inc.')
            non_match = non_match.removesuffix(' fin.')
            non_match = non_match.replace(" spec.", "")  # for Abav. mai.
            ref.set("addName", non_match)
            print(f"Lemma ({lemma}: {gloss_text} at loc {cref}) disambiguated to non-match {non_match}")
            return ref
        elif len(matches) == 2:  # this shouldn't happen!
            print(f"Could not disambiguate lemma ({lemma}: {gloss_text} at loc {cref}): both names in ambiguous glossary list ({glossary}), but no match!")
        return None
    elif len(results) == 2:
        print(f"Could not disambiguate lemma ({lemma}: {gloss_text} at loc {cref}: both names in ambiguous glossary list ({glossary}!")
        return None

def disambig_file():
    tree = etree.parse(XML_FILE)
    root = tree.getroot()
    # refs = root.xpath('//ref')
    ambig_refs = tree.xpath("//ref[contains(@addName, '/')]")
    count = 0
    for ref in ambig_refs:
        res = disambig_ref(ref)
        if res: count += 1

    print(f"{count} of {len(ambig_refs)} ambiguous references disambiguated.")

    tree.write(f'{XML_FILE.removesuffix('.xml')}-disamb.xml', encoding='utf-8')
    print("New tree saved as:", f"{XML_FILE.removesuffix('.xml')}-disamb.xml")

    return

if __name__ == "__main__":
    disambig_file()
