# Syntax of corrigenda files:

# **αἷμα]**                 # normal corrigenda
# **germen] gemina proles** # correct lemma german to gemina proles
# **asper** et **aspritudo]**  # corrigenda variation
# **bradigabo]** **bradigabo** ... # unclear why the repetition
# Adde **λάμυρος**          # addenda
# Adde: ***σεκορις** ...    # addenda with colon
# Adde post **ἀγνότης ** ... # addenda with variation
# Dele **κοιράδες** ...     # delenda

# NB: some refs to GL look like refs to CGL

from mkd_to_xml import *
