The scripts in this directory convert markdown texts of the Corpus Glossariorum
Latinorum, Index glossarum (volumes VI and VII), into XML with TEI tags.
Attribution: Adam Gitner and Andreas Consalvi, 2023

More information: https://publikationen.badw.de/en/cglo/index