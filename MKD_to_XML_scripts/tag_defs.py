# Looks for definitions in each <entryFree> and adds numbered <def> tags.
# Not fully functional.

from lxml import etree
# from pathlib import Path
import os
import re
# import logging


# Add <note> tags around all brackets in the filename (including across line breaks!)
def tag_notes(filename):
    with open(filename, 'r', encoding='utf-8') as f:
        text = f.read()

    text = text.replace('(', '<note>(')
    text = text.replace(')', ')</note>')

    savefile = filename.removesuffix('.xml') + '-notes.xml'
    with open(savefile, 'w', encoding='utf-8') as f:
        f.write(text)

# Broken version with re.sub: mysteriously only tags the first 16 instances!!!
# def tag_notes(filename):
#     with open(filename, 'r', encoding='utf-8') as f:
#         text = f.read()
#
#     text = re.sub(r'\((.*?)\)', r'<note>(\1)</note>', text, re.DOTALL)
#
#     savefile = filename.removesuffix('.xml') + '-notes.xml'
#     with open(savefile, 'w', encoding='utf-8') as f:
#         f.write(text)

def tag_defs_file(filename):
    tree = etree.parse(filename)
    root = tree.getroot()

    for entry in root.xpath("//entryFree"):
        tag_defs_entry(entry)

    filename = filename.removesuffix('.xml')
    tree.write(f'{filename}-defs.xml', encoding='utf-8')


# Procedure: iterate through the text elements to find the first non-italic text (skip over contents of note, emph)
# When tail of emph element (but also specific content that marks a pseudo-citation: "gloss.", are there others?) contains a dot, start a new def.
# extend the def to the next "." (outside of contents of note or emph) or to the next ref
def tag_defs_entry(entry):
    forms = entry.xpath("form")
    refs = entry.xpath("ref")
    if forms == [] or refs == []:
        return False

    current_tag = forms[0]  # Advance counter to the first form tag (just in case a <pb> comes first)
    if entry.index(current_tag) > entry.index(refs[0]):  # Make sure that the first form precedes the first ref (sometimes a parenthesis begins with a ref before a new form!)
        refs = [r for r in refs if entry.index(r) > entry.index(current_tag)]

    while current_tag is not None:  # iterate through the elements until we reach the end
        # step through siblings until we get non-italic text (skip over note, emph)
        if is_start_of_def(current_tag):
            new_def = create_def_from_tail(current_tag)
            insert_def(current_tag, new_def, entry)  # insert def after the current tag
        else:
            current_tag = current_tag.getnext()
            continue  # restart the while loop

        # now we have the start of a new_def and need to find where it ends
        for sibling in etree.SiblingsIterator(new_def):
            if sibling.tag == "form":  # there is a form tag without any intervening ref
                # look for a dot in the preceding def text and if we find it, end the def before the form
                # TODO: expand to find a dot in any intervening tags between the def and the form
                if entry.index(sibling) == entry.index(new_def) + 1:
                    # print("New def and form adjacent.", new_def.text, ",", new_def.tail)
                    m = re.search(r"[\w]{3,}\.", new_def.text)  # period must be preceded by at least two alphanumeric characters to avoid matching abbreviations
                    if m:  # if the sibling immediately adjacent to the def AND we have a match
                        # print("Def with dot conditions met.")
                        new_def.text = new_def.text[:(m.span()[1] - 1)]  # move the dot to the tail
                        new_def.tail = "." + new_def.text[m.span()[1]:]
                    break

            if is_end_of_def(sibling):
                current_tag = new_def  # move the pointer to the new_def
                rstrip_def(new_def)  # moves any white space at the end of the def tag to the tail
                break
            else:
                new_def.append(sibling)

        current_tag = current_tag.getnext()

    # check if any note tags contain any refs: if so, recursively tag the defs
    notes_with_refs = entry.xpath("note[ref]")
    for note in notes_with_refs:
        tag_defs_entry(note)

    return entry

# checks whether the current tag might be the valid start of a new def
def is_start_of_def(current_tag):
    if current_tag.tag == "form":
        return True
    elif current_tag.tag in ["note", "emph", "ref"]:  # check if tail contains start of def
        return tail_has_dot(current_tag)

# Tests a tag to see if the tail contains start of a def.
def tail_has_dot(tag):
    tail = tag.tail or ''
    if re.search(r'^[.\s]*$', tail):
        return False # if the tail only has white space and/or a dot, return False
    if "Cf." in tail:
        return False
    if "." not in tail:
        return False
    else:
        return True

# tests to see whether the current tag completes the definition
def is_end_of_def(current_tag):
    if current_tag.tag == "ref":  # any time we hit a ref, the def is over
        return True
    elif current_tag.tag == "emph":
        for word in ["gloss.", "GR. L.", "Papias"]:  # italicized "gloss." or "GR. L." signal end of def also
            text = current_tag.text or ""
            if word in text:
                return True
    else:
        return False


# moves any white space at the end of the def tag to the tail
def rstrip_def(tag):
    # if def has any descendents, ignore
    if tag.getchildren() != []:
        return None

    m = re.search(r"\s+$", tag.text)
    if m:
        tag.text = tag.text.rstrip()
        if tag.tail == None: tag.tail = ''
        tag.tail = m.group() + tag.tail
        return tag
    else:
        return None

# return a string from start tag to end tag. Doesn't fully work.
def tags_to_text(start, end):
    siblings = start.xpath("following-sibling::*")
    if end not in siblings:
        return None  # end tag not a sibling of start tag!

    siblings.insert(0, start)  # insert first tag to start of siblings list
    s = ''

    for tag in siblings:
        s += etree.tostring(tag, method="text")
        if tag == end:
            break

    return s


def find_next_def(current_tag, entry):
    # Iterate through siblings until we reach the start of a def.
    try:  # UNCLEAR WHY THIS SOMETIMES FAILS
        next_ref = current_tag.xpath("following-sibling::ref")[0]
    except:
        return None

    while current_tag != next_ref:  # loop until we hit the next ref tag
        if current_tag.tag == "form":  # automatically start a new def
            new_def = etree.Element("def")
            if current_tag.tail.isspace():
                new_def.text = ""
            else:
                match = re.search(r'^\s+', current_tag.tail)
                if match:
                    new_def.text = current_tag.tail.lstrip()
                    current_tag.tail = match.group()
                else:
                    new_def.text = current_tag.tail
                    current_tag.tail = ''

            insert_def(current_tag, new_def, entry)
            return new_def

        elif current_tag.tag == "note":  # check if tail contains start of def
            # if test_note(current_tag):
            if test_ref(current_tag):
                new_def = create_def_from_tail(current_tag)
                insert_def(current_tag, new_def, entry)
                return new_def

        elif current_tag.tag == "emph":  # check if tail contains start of def
            if test_ref(current_tag):
                new_def = create_def_from_tail(current_tag)
                insert_def(current_tag, new_def, entry)
                return new_def

        elif current_tag.tag == "ref":  # check if tail contains start of def
            if test_ref(current_tag):
                new_def = create_def_from_tail(current_tag)
                insert_def(current_tag, new_def, entry)
                return new_def

        current_tag = current_tag.getnext()

    return None  # no def found before we reached the next ref


def get_next_ref(current_tag):
    return current_tag.xpath("following-sibling::ref")[0]

# The tail of the current tag contains start of a def: return a new_def, with the tail text correctly split
def create_def_from_tail(current_tag):
    new_def = etree.Element("def")
    if current_tag.tag == "form":
        if current_tag.tail.isspace():
            new_def.text = ""
        else:
            match = re.search(r'^\s+', current_tag.tail)
            if match:
                new_def.text = current_tag.tail.lstrip()
                current_tag.tail = match.group()
            else:
                new_def.text = current_tag.tail
                current_tag.tail = ''

    else:  # we have to split the tail around the dot
        if re.search(r"(?<!\.)\. ", current_tag.tail):  # if there is a dot that isn't ellipsis (...)
            predot, postdot = split_tail(current_tag.tail)
            current_tag.tail = predot
            new_def.text = postdot
        else:  # move entire tail of current tag to new_def
            new_def.text = current_tag.tail
            current_tag.tail = ''

    return new_def


# Tests a note tag to see if tail contains start of a def.
# Different from a ref: if there is any alphabetic character in the tail, return true (even without dot)
def test_note(note):
    tail = note.tail or ''
    if any(c.isalpha() for c in tail):
        return True
    else:
        return False

# Splits the tail into sequence before the first "." and everything after
def split_tail(tail):
    # s = tail.split('.')
    # predot = s[0] + "."
    # postdot = ".".join(s[1:])
    end, start = re.search(r"(?<!\.)\. ", tail).span()
    predot = tail[:end] + ". "
    postdot = tail[start:]
    # move any initial spaces from post-dot to end of pre-dot
    # match = re.search(r'^\s+', postdot)
    # postdot = postdot.lstrip()
    # predot = match.group() + predot

    return predot, postdot

def insert_def(previous_tag, new_def, entry):
    previous_tag_pos = entry.index(previous_tag)
    entry.insert(previous_tag_pos + 1, new_def)
    return new_def

# Takes a new def and appends all intervening tags until we reach the next ref
# Make any intervening siblings (pb, note, emph) children of <def>
# NB: currently this loop includes form tags within a def tag (e.g. Auerto / auertit)
def extend_def_to_ref(new_def):
    for sibling in etree.SiblingsIterator(new_def):
        if sibling.tag == "ref":
            current_tag = sibling  # advance current_tag pointer to the ref
            break
        else:
            new_def.append(sibling)

    return sibling  # returns the adjacent ref that follows the end of new_def

TEST_FILE = "tests/test_B_notes.xml"
tag_defs_file(TEST_FILE)

#
# tree = etree.parse(TEST_FILE)
# root = tree.getroot()
# entries = root.xpath("//entryFree")
